#IOI EXCEL INGEST

1. [Configuration Documentation](#configuration)
2. [Installation](#installation)
2. [Developer Documentation](#developer)
3. [Feature Requests](#feature-requests)
4. [TODOS](###TODOS)


###Configuration

- censhare Specific Configurations

    - Module was built to mimic standard censhare configuration to the action
itself.  Module title, description, and labels can all be configured through
the properties files within the directory 'cscs/app/modules/ioi/lib/ioiExcel/ingest/'.

    - Currently there is no html5 client interface built in to perform the action through
the web.  This can be done by creating the necessary assets and tying them to the command.
Documentation for this to be done can be found in censhare's partner git documentation.

- Module Specific Configurations

    - All module specific configurations can be found under the label title 'Excel Configuration'.  
    
    - This module will generate .xlsx documents and is built using apache's poi library
    
    - The configuration for this module ultimately builds an xml structure that is used to provide the functionality
    with a backbone to map feature data into cells in the spreadsheet.
    
    - You are able to create workbooks and multiple sheets within those workbooks.  The name fields for workbook and sheet 
    are used to name their respected objects.  Within these fields you can use both hardcoded text as well as, by surrounding
    the property in '{}' evaluate xpath to fill in dynamic text.  These two methods can be used within the same field to generate
    names that have hardcoded text combined with text that is generated at the time of creation.  A good example is delivered by
    default and is to pull the current date/time and insert into the name of the object.  The property you insert within the {} will
    need to map directly to an attribute set on the mappings node found on the xml structure that can be opened by clicking the 
    XPath mapping button.  Currently these properties must be added manually to the xml due to the limitations delivered by 
    censhare's XMLEditor technology(There is no way to currently enter a dynamic attribute name).
    
            <mappings date="format-date(current-date(), &apos;[M01]/[D01]/[Y0001]&apos;)">
            
    - To Configure the column mapping you can add as many columns as required by the export.  The ui provides a way to 
      map header names to header styles.  To extract data from the assets selected there are three fields needed.  A checkbox named
      Map Feature.  This check box is used to display a dropdown that makes system features available to map into.  For more advanced
      mapping you can leave the check box unchecked to display an editfield named Column Value.  This Column value will map to a 'map'
      element's property, configured in the mapping accessable through the XPath mappings, and must be an exact match.  This allows the
      admin to enter xpath asset_feature path's (nested features and concatinations of data).  The last field is the column
      style and is used how the header styles is and will map to the excel styles configuration to export that specific cell with a 
      style configured in that xml.
            
        
    <map property="descde" valuepath="concat(asset_feature[@feature=&apos;censhare:description&apos; and @language=&apos;de&apos;][1]/@value_string, &apos; (&apos;, asset_feature[@feature=&apos;censhare:description&apos; and @language=&apos;de&apos;][1]/@language, &apos;)&apos;)"/>
                
                
###Installation

To install this module the ioiExcel module will need to be installed in cscs/app/modules/ioi/lib/.  As well as the ioiExcel
module the helper package will need to be installed as well and should be placed in the same directory.
There are paths in the java that require this location.  If there is ever the request to move this module then the java files
will need to be refactored to match the new path.

Along with the module the appropriate jar's will need to be installed on censhare in censhare's recommended location.
The Apache POI version 3.15 lib is used for this module.  All poi jar files will be needed as well as their dependencies
which are packaged together and can be found on apache's website.

###Developer

The module has a few files that are necessary to provide the action with its funcitonality
IoiExcelIngest.java and IoiHelper which is found in the helper package.  The helper package is used to perform any
queries needed by the action(currently by ID, ID_EXTERN, NAME).  These methods
inputs are as follows:
- getAssetByIdExtern(TransactionManager tm, String idExtern)
- getAssetById(TransactionManager tm, String idExtern)
- getAssetByIdName(TransactionManager tm, String idExtern)

###Feature Requests

Currently there are no feature requests.

###TODOS

Currently there are no todos.
