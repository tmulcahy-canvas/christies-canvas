package modules.ioi.lib.helper

import com.censhare.model.corpus.impl.Asset
import com.censhare.model.corpus.impl.AssetFeature
import com.censhare.server.manager.CommonServiceFactory
import com.censhare.server.manager.DBTransactionManager
import com.censhare.support.model.PersistenceManager
import com.censhare.support.xml.AXml

/**
 * @author Cameron Cramsey
 *         6/19/17.
 */
class IoiHelperTest extends GroovyTestCase {
    void testFindFeatureChangesByXml() {

        DBTransactionManager tm;
        PersistenceManager pm;

        tm = CommonServiceFactory.createDBTransactionManager("AssetAutomation");
        pm = tm.getDataObjectTransaction().getPersistenceManager();

        IoiHelper help = new IoiHelper();
        Asset a1 = Asset.newInstance("text.", "Test 1");
        pm.makePersistentRecursive(a1);
        AssetFeature ausMarket = a1.createFeature("s44:pim.acc.market").setValueKey("australia");
        ausMarket.createFeature("s44:pim.acc.labour-rate-jaguar").setValueDouble(142.0).setValueUnit("AUD");
        ausMarket.createFeature("s44:pim.acc.labour-rate-landrover").setValueDouble(140.0).setValueUnit("AUD");
        ausMarket.createFeature("s44:pim.acc.local-tax").setValueDouble(1.2).setValueUnit("AUD");
        ausMarket.createFeature("s44:pim.config.currency").setValueString("AUD");
        ausMarket.createFeature("s44:pim.config.language").setValueString("aue");

        AssetFeature ukMarket = a1.createFeature("s44:pim.acc.market").setValueKey("united-kingdom");
        ukMarket.createFeature("s44:pim.acc.labour-rate-jaguar").setValueDouble(142.0).setValueUnit("AUD");
        ukMarket.createFeature("s44:pim.acc.labour-rate-landrover").setValueDouble(140.0).setValueUnit("AUD");
        ukMarket.createFeature("s44:pim.acc.local-tax").setValueDouble(1.2).setValueUnit("AUD");
        ukMarket.createFeature("s44:pim.config.currency").setValueString("AUD");
        ukMarket.createFeature("s44:pim.config.language").setValueString("aue");

        Asset a2 = Asset.newInstance("text.", "Test 2");
        pm.makePersistentRecursive(a2);
        AssetFeature ausMarket2 = a2.createFeature("s44:pim.acc.market").setValueKey("australia");
        ausMarket2.createFeature("s44:pim.acc.labour-rate-jaguar").setValueDouble(143.0).setValueUnit("AUD");
        ausMarket2.createFeature("s44:pim.acc.labour-rate-landrover").setValueDouble(140.0).setValueUnit("AUD");
        ausMarket2.createFeature("s44:pim.acc.local-tax").setValueDouble(1.1).setValueUnit("AUD");
        ausMarket2.createFeature("s44:pim.config.currency").setValueString("AUD");
        ausMarket2.createFeature("s44:pim.config.language").setValueString("aue");
        AXml test = help.findFeatureChangesByXml(a1, a2, "s44:pim.acc.market");

        System.out.println(test.toStringPretty());
    }
}
