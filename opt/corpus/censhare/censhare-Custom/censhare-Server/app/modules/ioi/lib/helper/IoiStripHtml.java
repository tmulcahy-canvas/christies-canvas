package modules.ioi.lib.helper;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.Deflater;

import com.censhare.model.corpus.impl.StorageItem;
import com.censhare.support.io.FileFactoryService;
import com.censhare.support.io.FileLocator;
import com.censhare.support.model.DataObject;
import com.censhare.support.service.ServiceLocator;
import modules.ioi.lib.ioiParser.Element;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;

import com.censhare.support.xml.AXml;
import com.censhare.support.xml.CXml;
import com.censhare.support.xml.CXmlUtil;
import org.apache.commons.lang.math.IntRange;

/**
 *
 */
public class IoiStripHtml {

    private static final Pattern LINE_BREAK = Pattern.compile("<br/>");
    private static final Pattern TAG = Pattern.compile("</?([^<]*)>");
    private static final Pattern FULL_TAG = Pattern.compile("<(\\S+?)(.*?)>(.*?)</\\1>");
    private static final String PARAGRAPH_TAG = "<paragraph>";
    private static final String PARAGRAPH_TAG_CLOSE = "</paragraph>";

    private static final List<String> keepers = Arrays.asList("paragraph", "bold", "italic", "bold-italic", "br");

    private static final Map<String, Map<String, String>> LOT_TAGS = createLotTagMap();
    private static final Map<String, String> LOT_TAGS_EXCEL = createLotTagMapForExcel();
    private static FileFactoryService ffs = ServiceLocator.getStaticService(FileFactoryService.class);

    /**
     * @param article
     * @param assetID
     * @param replaceLotSymbols
     * @param addLineBreaks
     * @return
     * @throws Exception
     */
    public static AXml prepareArticle(AXml article, String assetID, boolean replaceLotSymbols, boolean addLineBreaks) throws Exception {

        return processArticleByFile(article, assetID, addLineBreaks, replaceLotSymbols, Mode.NONE);

    }

    /**
     * @param textString
     * @param replaceLotSymbols
     * @param addLineBreaks
     * @return
     * @throws Exception
     */
    public static AXml prepareArticleFromString(String textString, boolean replaceLotSymbols, boolean addLineBreaks) throws Exception {
        AXml article = new CXml("article");
        AXml text = article.create("content").create("text");

        // String paragraphText = StringEscapeUtils.unescapeXml(textString).trim();
        String paragraphText = textString.trim().replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&amp;gt;", ">").replaceAll("&amp;lt;", "<");

        if(addLineBreaks) {
            paragraphText = LINE_BREAK.matcher(paragraphText).replaceAll("\n");
        }

        Matcher m = TAG.matcher(paragraphText);
        paragraphText = cleanParagraphText(paragraphText, m);

        AXml updated = getUpdatedXml(paragraphText);
        if(replaceLotSymbols)
            convertLotTags(updated);



        text.removeAllChildNodes();
        text.appendChild(updated);


        return article;
    }

    /**
     * @param paragraphText
     * @param m
     * @return
     */
    private static String cleanParagraphText(String paragraphText, Matcher m) {
        while(m.find()) {
            String tag = m.group();

            String cleanTag = tag.replace("/", "").replace("<", "").replace(">", "");
            if(!LOT_TAGS.containsKey(cleanTag)  && !keepers.contains(cleanTag))
                paragraphText = paragraphText.replace(tag, "");
        }
        return paragraphText;
    }

    /**
     * @param article
     * @param assetID
     * @param replaceLotSymbols
     * @param addLineBreaks
     * @return
     * @throws Exception
     */
    public static AXml prepareArticleForExcel(AXml article, String assetID, boolean replaceLotSymbols, boolean addLineBreaks) throws Exception {

        return processArticleByFile(article, assetID, addLineBreaks, replaceLotSymbols, Mode.EXCEL);
    }

    /**
     * @param textString
     * @param addLineBreaks
     * @return
     * @throws Exception
     */
    public static AXml prepareArticleForExcelFromString(String textString, boolean addLineBreaks) throws Exception {
        AXml article = new CXml("article");
        AXml text = article.create("content").create("text");

        // String paragraphText = StringEscapeUtils.unescapeXml(textString).trim();
        String paragraphText = textString.trim().replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&amp;gt;", ">").replaceAll("&amp;lt;", "<").replaceAll("&amp;", "&");
        if(addLineBreaks){
            paragraphText = LINE_BREAK.matcher(paragraphText).replaceAll("\n");
        }
        Matcher m = TAG.matcher(paragraphText);
        paragraphText = cleanParagraphText(paragraphText, m);

        for(Map.Entry<String, String> entry : LOT_TAGS_EXCEL.entrySet()) {
            String replace = entry.getKey();
            String with = StringEscapeUtils.escapeXml(entry.getValue());

            paragraphText = paragraphText.replaceAll(replace, with);
        }

        AXml updated = getUpdatedXml(paragraphText);

        text.removeAllChildNodes();
        text.appendChild(updated);

        return article;
    }

    /**
     * @param paragraphText
     * @return
     */
    private static AXml getUpdatedXml(String paragraphText) {
        AXml updated;
        try {
            updated = CXmlUtil.parse("<paragraph>" + paragraphText.trim() + "</paragraph>");
        } catch (Exception e) {
            updated = new CXml("paragraph");
            AXml err = updated.create("error_processing_article");
            err.setTextValue(StringEscapeUtils.escapeXml("Error occurred processing asset: " + paragraphText + " - " + e));
        }
        return updated;
    }

    /**
     * @param article
     * @param assetID
     * @param addLineBreaks
     * @param replaceLotSymbols
     * @param mode
     * @return
     * @throws Exception
     */
    private static AXml processArticleByFile(AXml article, String assetID, boolean addLineBreaks, boolean replaceLotSymbols, Mode mode) throws Exception {

        FileLocator fL = getFileLocatorFromStorageItem(article);
        fL = ffs.resolveUseLocalRepl(fL);

        Reader reader = new InputStreamReader(fL.getInputStream(FileLocator.STANDARD_FILE, Deflater.NO_COMPRESSION));


        String paragraphText, articleString;

        String xml = IOUtils.toString(reader);

        try {
            AXml parsedXml = CXmlUtil.parse(xml);
            AXml paragraphXml = parsedXml.find("content.text.paragraph");
            paragraphText = paragraphXml.toString();
            paragraphXml.remove();
            articleString = parsedXml.toString();
        } catch(Exception e) {
            paragraphText = xml.substring(xml.indexOf(PARAGRAPH_TAG), xml.lastIndexOf(PARAGRAPH_TAG_CLOSE));
            articleString = xml.substring(0, xml.indexOf(PARAGRAPH_TAG)) + xml.substring(xml.lastIndexOf(PARAGRAPH_TAG_CLOSE) + PARAGRAPH_TAG_CLOSE.length());
        }

        if(addLineBreaks){
            paragraphText = LINE_BREAK.matcher(paragraphText).replaceAll("\n");
        }

        Matcher m = TAG.matcher(paragraphText);
        while(m.find()) {
            String tag = m.group();
            String tagStr = tag.replace("/", "").replace("<", "").replace(">", "");
            if(!LOT_TAGS.containsKey(tagStr) && !keepers.contains(tagStr) )
                paragraphText = paragraphText.replaceAll(tag, "");
        }

        AXml updated = null;
        try {
            // String escaped = StringEscapeUtils.escapeXml(paragraphText).replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&amp;gt;", ">").replaceAll("&amp;lt;", "<");
            updated = CXmlUtil.parse(paragraphText);
            paragraphText = processByMode(replaceLotSymbols, mode, paragraphText, updated);
        } catch(Exception e) {
            // we need to try and fix.
        }

        if(updated == null) {
            Element elem = new Element(paragraphText, keepers);

            // String escaped = StringEscapeUtils.escapeXml(elem.getFixedString()).replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&amp;gt;", ">").replaceAll("&amp;lt;", "<");

            try {
                updated = CXmlUtil.parse(elem.getFixedString());
            } catch (Exception e) {
                updated = new CXml("paragraph");
                AXml err = updated.create("error_processing_article");
                err.setTextValue(StringEscapeUtils.escapeXml("Error occurred processing asset: " + assetID + " - " + e));
            }
        }

        AXml articleXml = CXmlUtil.parse(articleString);

        articleXml.findX("content/text").appendChild(updated);

        return articleXml;
    }

    /**
     * @param replaceLotSymbols
     * @param mode
     * @param paragraphText
     * @param updated
     * @return
     */
    private static String processByMode(boolean replaceLotSymbols, Mode mode, String paragraphText, AXml updated) {
        if (mode == Mode.EXCEL) {
            for (Map.Entry<String, String> entry : LOT_TAGS_EXCEL.entrySet()) {
                String replace = entry.getKey();
                String with = StringEscapeUtils.escapeXml(entry.getValue());

                paragraphText = paragraphText.replaceAll(replace, with);

            }
        } else {
            if (replaceLotSymbols) {
                convertLotTags(updated);
            }
        }
        return paragraphText;
    }

    /**
     * @param storageItem
     * @return
     * @throws IOException
     */
    private static FileLocator getFileLocatorFromStorageItem(AXml storageItem) throws IOException {
        if (storageItem.getDataInterface() instanceof StorageItem)
            return ((StorageItem) storageItem.getDataInterface()).getFileLocator();

        String fsName = storageItem.getAttr(StorageItem.ATTR_FILESYS_NAME);
        if (fsName == null)
            fsName = storageItem.getAttr(DataObject.CORPUS_PREFIX + StorageItem.ASSET_TEMP_FILESYSTEM);

        String relPath = storageItem.getAttr(StorageItem.ATTR_RELPATH);
        if (relPath == null)
            relPath = storageItem.getAttr(DataObject.CORPUS_PREFIX + StorageItem.ASSET_TEMP_FILEPATH);

        FileLocator fileLocator = ffs.get(fsName, relPath);
        fileLocator.setMimetype(storageItem.getAttr(StorageItem.ATTR_MIMETYPE));
        fileLocator.setContentHashCode(storageItem.getAttr(StorageItem.ATTR_HASHCODE));
        return fileLocator;
    }

    /**
     * @param xml
     */
    private static void convertLotTags(AXml xml) {
        for(AXml element : xml.findAll()) {
            String localName = element.getLocalName();
            Map<String, String> valueMap = LOT_TAGS.get(localName);

            if(valueMap != null) {
                String val = valueMap.get(element.getTextValue());
                if(val != null) {
                    element.setTextValue(val);
                }
            }
        }
    }

    public static AXml processSymbolConvertedString(String s) throws Exception {

        Map<String, LotSymbolMapTag> mappingMap = new TreeMap<>();
        mappingMap.put("<S>**</S>", new LotSymbolMapTag("&#x2217;", "SS"));
        mappingMap.put("<S>⊕</S>", new LotSymbolMapTag("&#x2295;", "SS"));
        mappingMap.put("<S>a</S>", new LotSymbolMapTag("&#x03B1;", "SS"));
        mappingMap.put("<S>D</S>", new LotSymbolMapTag("&#x0394;", "SS"));
        mappingMap.put("<S>F</S>", new LotSymbolMapTag("&#x03A6;", "SS"));
        mappingMap.put("<S>l</S>", new LotSymbolMapTag("&#x03BB;", "SS"));
        mappingMap.put("<S>P</S>", new LotSymbolMapTag("&#x03A0;", "SS"));
        mappingMap.put("<S>q</S>", new LotSymbolMapTag("&#x03B8;", "SS"));
        mappingMap.put("<S>W</S>", new LotSymbolMapTag("&#x03A9;", "SS"));
        mappingMap.put("<S>Ψ</S>", new LotSymbolMapTag("&#x03A8;", "SS"));
        mappingMap.put("<Z>n</Z>", new LotSymbolMapTag("&#x25A0;", "Z"));
        mappingMap.put("<Z>u</Z>", new LotSymbolMapTag("&#x25C6;", "Z"));
        mappingMap.put("○<Z>u</Z>", new LotSymbolMapTag("&#x0B00;", "Z"));
        mappingMap.put("σ", new LotSymbolMapTag("&#x03C3;", "SS"));
        mappingMap.put("†", new LotSymbolMapTag("&#x2020;", "AB"));
        mappingMap.put("‡", new LotSymbolMapTag("&#x2021;", "AB"));
        mappingMap.put("*", new LotSymbolMapTag("&#x002A;", "AB"));
        mappingMap.put("#00B0", new LotSymbolMapTag("&#x00B0;", "AB"));
        mappingMap.put("○", new LotSymbolMapTag("&#x00B0;", "AB"));
        mappingMap.put("•", new LotSymbolMapTag("&#x2022;", "AB"));
        mappingMap.put("+", new LotSymbolMapTag("&#x002B;", "AB"));
        mappingMap.put("~", new LotSymbolMapTag("&#x007E;", "AB"));
        mappingMap.put("□", new LotSymbolMapTag("TBD", "SS"));
        mappingMap.put("ƒ", new LotSymbolMapTag("&#x0192;", "AB"));
        mappingMap.put("!", new LotSymbolMapTag("&#x0021;", "AB"));
        mappingMap.put("∇", new LotSymbolMapTag("&#x2007;", "SS"));
        mappingMap.put("‡*", new LotSymbolMapTag("&#x2021;", "AB"));
        mappingMap.put("++", new LotSymbolMapTag("&#x002B;", "AB"));
        mappingMap.put("«", new LotSymbolMapTag("&#x2605;", "Z"));

        AXml article = new CXml("article");
        AXml text = article.create("content").create("text");

        // String paragraphText = StringEscapeUtils.unescapeXml(textString).trim();

        String tempText = StringEscapeUtils.unescapeXml(s);
//        System.out.println("Original Lot Symbol - " + tempText);
        Map<Integer, LotSymbol> symbolMap = new TreeMap<>();

        Matcher tagMatcher = FULL_TAG.matcher(tempText);

        while(tagMatcher.find()) {
            LotSymbol ls = new LotSymbol(tagMatcher);
            symbolMap.put(ls.startPosition, ls);
        }

        for(int i = 0; i < tempText.length(); i++) {
            if(!inTag(i, symbolMap)) {
                symbolMap.put(i, new LotSymbol(Character.toString(tempText.charAt(i)), i));
            }
        }

//        for(Map.Entry<Integer, LotSymbol> entry : symbolMap.entrySet()) {
//            System.out.println("Symbol found at position: " + entry.getKey());
//        }
        
        StringBuilder sb = new StringBuilder();

        for(Map.Entry<Integer, LotSymbol> entry : symbolMap.entrySet()) {
            LotSymbol symbol = entry.getValue();
            String symbolString = symbol.symbol;
            LotSymbolMapTag mappedSymbol = mappingMap.get(symbolString);
            if(mappedSymbol != null) {
                sb.append("<" + mappedSymbol.mapTag + ">" + mappedSymbol.mapValue + "</" + mappedSymbol.mapTag + ">");
            }
        }
//        System.out.println("Replaced Lot Symbol - " + sb.toString());
        String paragraphText  = PARAGRAPH_TAG + sb.toString() + PARAGRAPH_TAG_CLOSE;

        AXml data = CXmlUtil.parse(paragraphText);

        text.appendChild(data);

        return article;
    }

    private static boolean inTag(int position, Map<Integer, LotSymbol> lotSymbolMap) {
        boolean inTag = false;
        for(Map.Entry<Integer, LotSymbol> entry : lotSymbolMap.entrySet()) {
            LotSymbol existingLotSymbol = entry.getValue();
            if(existingLotSymbol.type == LotSymbolType.TAG && existingLotSymbol.range.containsInteger(position)) {
                inTag = true;
                break;
            } else if(existingLotSymbol.type == LotSymbolType.CHARACTER && existingLotSymbol.startPosition == position) {
                inTag = true;
                break;
            }
        }
        return inTag;
    }

    /**
     * @return
     */
    private static Map<String, String> createLotTagMapForExcel() {

        Map<String, String> tempMap = new HashMap<>();

        tempMap.put("<P>1</P>", "1/16");
        tempMap.put("<P>2</P>", "3/16");
        tempMap.put("<P>3</P>", "5/16");
        tempMap.put("<P>4</P>", "7/16");
        tempMap.put("<P>5</P>", "9/16");
        tempMap.put("<P>6</P>", "11/16");
        tempMap.put("<P>7</P>", "13/16");
        tempMap.put("<P>8</P>", "15/16");
        tempMap.put("<P>Q</P>", "1/20");
        tempMap.put("<P>R</P>", "1/24");
        tempMap.put("<P>S</P>", "1/25");
        tempMap.put("<P>T</P>", "1/32");
        tempMap.put("<P>U</P>", "1/36");
        tempMap.put("<P>V</P>", "1/72");
        tempMap.put("<P>W</P>", "1/74");
        tempMap.put("<P>X</P>", "1/96");
        tempMap.put("<P>Y</P>", "1/100");
        tempMap.put("<P>Z</P>", "1/200");

        tempMap.put("<E>¡</E>", "3/8");
        tempMap.put("<E>¿</E>", "1/8");
        tempMap.put("<E>¬</E>", "5/8");
        tempMap.put("<E>√</E>", "7/8");
        tempMap.put("<E>≈</E>", "2/3");
        tempMap.put("<E>æ</E>", "3/4");
        tempMap.put("<E>Ç</E>", "&#00C7;");
        tempMap.put("<E>ƒ</E>", "1/3");
        tempMap.put("<E>º</E>", "1/4");
        tempMap.put("<E>Ø</E>", "&#00F8;");
        tempMap.put("<E>Ω</E>", "1/2");

        tempMap.put("<Z>n</Z>", "&#25A0;");
        tempMap.put("<Z>u</Z>", "&#25C6;");

        return tempMap;

    }

    /**
     * @return
     */
    private static Map<String, Map<String, String>> createLotTagMap() {

        Map<String, Map<String, String>> tempMap = new HashMap<>();
        Map<String, String> pMap = new HashMap<>();
        Map<String, String> eMap = new HashMap<>();
        Map<String, String> zMap = new HashMap<>();

        pMap.put("1", "&#x0031;");
        pMap.put("2", "&#x0032;");
        pMap.put("3", "&#x0033;");
        pMap.put("4", "&#x0034;");
        pMap.put("5", "&#x0035;");
        pMap.put("6", "&#x0036;");
        pMap.put("7", "&#x0037;");
        pMap.put("8", "&#x0038;");
        pMap.put("Q", "&#x0051;");
        pMap.put("R", "&#x0052;");
        pMap.put("S", "&#x0053;");
        pMap.put("T", "&#x0054;");
        pMap.put("U", "&#x0055;");
        pMap.put("V", "&#x0056;");
        pMap.put("W", "&#x0057;");
        pMap.put("X", "&#x0058;");
        pMap.put("Y", "&#x0059;");
        pMap.put("Z", "&#x005A;");

        eMap.put("¡", "&#x00A1;");
        eMap.put("¿", "&#x00BF;");
        eMap.put("¬", "&#x00AC;");
        eMap.put("√", "&#x221A;");
        eMap.put("≈", "&#x22F2;");
        eMap.put("æ", "&#x00E6;");
        eMap.put("Ç", "&#x00C7;");
        eMap.put("ƒ", "&#x0192;");
        eMap.put("º", "&#x00BA;");
        eMap.put("Ø", "&#x00F8;");
        eMap.put("Ω", "&#x2126;");

        zMap.put("n", "&#x25A0;");
        zMap.put("u", "&#x25C6;");

        tempMap.put("P", pMap);
        tempMap.put("E", eMap);
        tempMap.put("Z", zMap);

        return tempMap;

    }

    public static class LotSymbolMapTag {
        public String mapValue;
        public String mapTag;

        public LotSymbolMapTag(String mapValue, String mapTag) {
            this.mapValue = mapValue;
            this.mapTag = mapTag;
        }
    }

    public static class LotSymbol {

        public LotSymbolType type;
        public String symbol;
        public int startPosition;
        public int endPositon;
        public IntRange range;

        public LotSymbol(Matcher tagMatcher) {
            this.type = LotSymbolType.TAG;
            this.symbol = tagMatcher.group(0);
            this.startPosition = tagMatcher.start();
            this.endPositon = tagMatcher.end() - 1;
            this.range = new IntRange(this.startPosition, this.endPositon);
        }

        public LotSymbol(String symbol, int position) {
            this.type = LotSymbolType.CHARACTER;
            this.symbol = symbol;
            this.startPosition = position;
        }

        @Override
        public String toString() {
            return "LotSymbol{" +
                    "type=" + type +
                    ", symbol='" + symbol + '\'' +
                    ", startPosition=" + startPosition +
                    '}';
        }
    }

    /**
     *
     */
    private enum Mode {
        EXCEL,
        NONE
    }

    private enum LotSymbolType {
        CHARACTER, TAG
    }

}
