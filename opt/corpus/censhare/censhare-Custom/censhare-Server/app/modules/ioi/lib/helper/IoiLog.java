package modules.ioi.lib.helper;

import com.censhare.support.util.logging.SimpleFormatter;

import java.util.logging.FileHandler;
import java.util.logging.Logger;

/**
 * @author Cameron Cramsey
 *         4/7/17.
 */
public class IoiLog {

    private final Logger logger = Logger.getLogger(IoiLog.class.getName());

    private FileHandler fh = null;

    public IoiLog() {

    }

    public Logger getLogger(String path) {

        try {
            fh = new FileHandler(path
                    + "-ioi" + ".log", 5000000, 20);
        } catch (Exception e) {
            e.printStackTrace();
        }

        fh.setFormatter(new SimpleFormatter());
        logger.addHandler(fh);

        return logger;
    }

}
