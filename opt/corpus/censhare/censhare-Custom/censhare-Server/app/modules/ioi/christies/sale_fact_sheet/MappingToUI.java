package modules.ioi.christies.sale_fact_sheet;

import java.io.IOException;
import java.io.Writer;
import java.util.logging.Logger;

import com.censhare.manager.assetmanager.AssetManagementService;
import com.censhare.model.corpus.impl.Asset;
import com.censhare.model.corpus.impl.ResourceAsset;
import com.censhare.model.corpus.impl.StorageItem;
import com.censhare.server.kernel.Command;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.cache.CacheService;
import com.censhare.support.io.FileLocator;
import com.censhare.support.model.PersistenceManager;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.xml.AXml;
import com.censhare.support.xml.CXmlUtil;
import com.censhare.support.xml.Node;
import com.censhare.support.xml.xpath.XPathContext;
import com.censhare.support.xml.xpath.XslExprImpl;
import com.censhare.support.xml.xpath.XslParser;

public class MappingToUI {

    private Command command;
    public Logger logger;

    public DBTransactionManager tm;
    public PersistenceManager pm;
    public AssetManagementService am;
    public CacheService cacheService;

    private XslExprImpl.XslStylesheet xslStylesheet = null;
    
    public MappingToUI(Command command) {
        this.command = command;
        logger = command.getLogger();

        tm = command.getTransactionManager();
        pm = tm.getDataObjectTransaction().getPersistenceManager();
        am = ServiceLocator.getStaticService(AssetManagementService.class);
        cacheService = ServiceLocator.getStaticService(CacheService.class);
    }
    
    public int generateUI() throws Exception {

        AXml cmdXml = (AXml) command.getSlot(Command.XML_COMMAND_SLOT);

        logger.info("   ###   UI GENERATION - Command - " + command.getClientType()); // Client5
        
        AXml dialog = null;

        AXml commandStepXml = command.getTarget();
        if (commandStepXml == null) {
            throw new IllegalStateException("Current command step not found");
        }
        
        AXml uiSettings = cmdXml.find(commandStepXml.getAttr("key"));
        String path = uiSettings.getAttr("path", null);
        String resourceKey = uiSettings.getAttr("resourceKey", null);
        String partID = uiSettings.getAttr("partId", null);
        
        CacheService.CachedTable ct = cacheService.getCachedTable(ResourceAsset.ELM_NAME, command.getLocale());
        ResourceAsset trafo = (ResourceAsset) ct.getEntry(ResourceAsset.ATTR_KEY, resourceKey);
        if (trafo != null) {
            Asset trafoAsset = am.cacheGetAssetNoAttach(tm, trafo.getAssetId());
            if (trafoAsset != null) {
                StorageItem master = trafoAsset.struct().getStorageItem(StorageItem.StorageItemKey.MASTER.getDBValue());
                if (master != null) {
                    AXml trafoXml = CXmlUtil.parse(master.getFileLocator().getInputStream(FileLocator.STANDARD_FILE, 0));
                    xslStylesheet = new XslParser().parse(trafoXml);
                    
                    XPathContext context = new XPathContext(cmdXml.getDocumentNode());
                    context.setVariable("cmd", cmdXml);
                    context.setVariable("partID", partID);
                    context.setVariable("mappingFile", path);
                    context.setVariable("clientType", command.getClientType());

                    // output xsl:message to log
                    context.messageWriter = new Writer() {
                        @Override
                        public void write(char[] cbuf, int off, int len) throws IOException {
                            logger.info(new String(cbuf, off, len));
                        }

                        @Override
                        public void flush() throws IOException {
                        }

                        @Override
                        public void close() throws IOException {
                        }
                    };

                    // perform xsl translation
                    Object result = xslStylesheet.eval(context);
                    logger.info("Script result is " + result);

                    dialog = (AXml) result;
                    if (dialog.getNodeType() == Node.DOCUMENT_NODE)
                        dialog = dialog.getFirstChildElement();
                    
                    if(dialog != null) {
                        cmdXml.appendChild(dialog.cloneMutableDeep());

                        command.setSlot(Command.XML_COMMAND_SLOT, cmdXml);
                    }
                }
            } else {
                logger.warning("Resource asset not found! - " + resourceKey);
            }
        }
        
        return Command.CMD_COMPLETED;
        
    }
    

}
