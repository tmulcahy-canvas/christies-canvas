package modules.ioi.lib.helper;

import com.censhare.manager.assetmanager.AssetManagementService;
import com.censhare.model.corpus.impl.Asset;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.model.PersistenceManager;
import com.censhare.support.transaction.TransactionManager;

import java.util.logging.Logger;

import static com.censhare.support.service.ServiceLocator.getStaticService;

/**
 * @author Cameron Cramsey
 * 8/24/17.
 */
public class IoiTransactionManagement {

    private Logger logger;

    private DBTransactionManager tm;
    private PersistenceManager pm;
    private AssetManagementService am;
    private int transactionsMade = 0;
    private int transactionLimit;

    private static final String LOG_PREFIX = "   ###   IOI TM - ";

    public IoiTransactionManagement(DBTransactionManager transactionmanager, int tl, Logger logger) {
        this.logger = logger;
        transactionLimit = tl;
        tm = transactionmanager;
        pm = tm.getDataObjectTransaction().getPersistenceManager();
        am = getStaticService(AssetManagementService.class);
        try {
            beginTransaction();
        } catch(Exception e) {
            logger.warning(LOG_PREFIX + "Initial begin failed!");
        }
    }

    public void checkInNew(Asset asset) {
        checkTransactionLimit();
        try {
            beginTransaction();
            pm.ensurePersistenceRecursive(asset);
            am.checkInNew(tm, asset);
        } catch(Exception e) {
            logger.warning(LOG_PREFIX + "Error creating asset - " + asset + "   -   " + e);
        }
        transactionsMade++;
    }

    public void update(Asset asset) {
        checkTransactionLimit();
        try {
            beginTransaction();
            am.update(tm, asset);
        } catch(Exception e) {
            logger.warning(LOG_PREFIX + "Error updating asset - " + asset);
        }
        transactionsMade++;
    }

    public Asset cacheGetCurrent(Long id) {
        Asset asset = null;

        try {
            beginTransaction();
            asset = am.cacheGetAssetCurrent(tm, id);
        } catch(Exception e) {
            logger.warning(LOG_PREFIX + "Error getting asset with id - " + id);
        }

        return asset;
    }

    private void beginTransaction() throws Exception {
        if (tm.getStatus() != TransactionManager.STATE_BEGUN) {
            tm.begin();
        }
    }

    public void endOrCommit() throws Exception {
        endOrCommit(false);
    }

    public void endOrCommit(boolean rebeginTm) throws Exception {
        if(transactionsMade <= transactionLimit) {
            tm.end();
        } else {
            tm.commit();
            pm.reset();
        }

        if(rebeginTm)
            beginTransaction();
    }

    public void ensureEnd() {
        if (tm.getStatus() != TransactionManager.STATE_ENDED || tm.getStatus() != TransactionManager.STATE_ENDING
                || tm.getStatus() != TransactionManager.STATE_COMMITTING
                || tm.getStatus() != TransactionManager.STATE_COMMITTED) {

            try {
                endOrCommit();
            } catch(Exception e) {
                logger.warning(LOG_PREFIX + "Could not end transaction!");
            }
        }
    }

    private void checkTransactionLimit() {
        if(transactionsMade > transactionLimit) {
            try {
                endOrCommit(true);
            } catch(Exception e) {
                logger.warning(LOG_PREFIX + "Error committing transaction");
            }
        }
    }
}
