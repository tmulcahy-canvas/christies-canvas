<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
	<xsl:param name="censhare:command-xml"/>
	<xsl:template match="/">
		<cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"></cs:command>
		<xsl:variable name="languages" select="string-join($censhare:command-xml/settings/language/@value, ',')"/>
		<xsl:variable name="ext-condition" select="$censhare:command-xml/settings/@externalCondition"/>
		<xsl:variable name="img-condition" select="$censhare:command-xml/settings/@imageStatus"/>
		<!-- rg <xsl:message> ##### R.G External Condition / <xsl:value-of select="$ext-condition" /></xsl:message> rg -->
		<xsl:variable name="excludes">
			<excludes>
				<xsl:for-each select="$censhare:command-xml/settings/excludes/@*">
				<!-- rg 	<xsl:variable name="toExclude" >
						<xsl:value-of select="." />
					</xsl:variable> rg -->
					<xsl:if test=". = 'true' or . = '1'">
						<!-- rg <xsl:message> ##### R.G Excludes Maker node  / <xsl:value-of select="." /></xsl:message> rg -->
						<exclude value="{local-name()}"/>
					</xsl:if>
				</xsl:for-each>
			</excludes>
		</xsl:variable>
		<xsl:variable name="sale-asset" select="$censhare:command-xml/assets/asset[1]"/>
		<xsl:variable name="pdf-name" select="concat($sale-asset/@id, '.pdf')"/>
		<xsl:variable name="dest" select="concat($out, $pdf-name)"/>
		<xsl:variable name="fop">
			<cs:command name="com.censhare.api.transformation.FopTransformation">
				<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=ioi:sale-fact-sheet-xslfo/storage/master/file'"/>
				<cs:param name="source" select="$sale-asset"/>
				<cs:param name="dest" select="$dest"/>
				<cs:param name="xsl-parameters">
					<cs:param name="languages" select="$languages"/>
					<cs:param name="use-external-condition" select="$ext-condition"/>
					<cs:param name="use-image-status" select="$img-condition"/>
					<cs:param name="excludes" select="$excludes"/>
				</cs:param>
			</cs:command>
		</xsl:variable>

		<!-- nh variables for the file to download pdf with legal page nh -->
		<xsl:variable name="pdf-legal-name" select="concat($sale-asset/@id, '-legal.pdf')"/>
		<xsl:variable name="pdf-legal-path" select="concat($out, $pdf-legal-name)"/>
		<xsl:variable name="pdf-legal-relpath" select="concat('file:', substring-after($out, 'censhare:///service/filesystem/temp/'), $pdf-legal-name)" />
		<xsl:variable name="filesysname" select="substring-before(substring-after($out, 'censhare:///service/filesystem/'), '/')" />
		<xsl:variable name="targetname" select="concat($sale-asset/@name, '-Fact Sheet.pdf')" />


		<xsl:variable name="renderResult">
			<cs:command name="com.censhare.api.pdf.CombinePDF">
				<cs:param name="dest" select="$pdf-legal-path"/>
				<cs:param name="sources">
					<source href="{$dest}"/>
					<source href="censhare:///service/assets/asset;censhare:resource-key=christies:legal-page/storage/master/file"/>
				</cs:param>
			</cs:command>
		</xsl:variable>

		<download>
			<file filesys_name="{$filesysname}" relpath="{$pdf-legal-relpath}" target_name="{$targetname}"/>
		</download>
	</xsl:template>
</xsl:stylesheet>










	<!-- nh <xsl:variable name="legalPage" select=":cs:asset()[@censhare:resource-key = 'christies:legal-page']/@id" />
		<xsl:message>### NH LEGAL PAGE - <xsl:value-of select="$legalPage" /></xsl:message> nh -->