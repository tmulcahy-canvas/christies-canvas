package modules.ioi.lib.helper;

import com.censhare.db.query.model.AssetQBuilder;
import com.censhare.manager.assetquerymanager.AssetQueryService;
import com.censhare.model.corpus.generated.FeatureBase;
import com.censhare.model.corpus.impl.Asset;
import com.censhare.model.corpus.impl.Feature;
import com.censhare.model.corpus.impl.QAsset;
import com.censhare.model.corpus.impl.QAssetFeature;
import com.censhare.support.cache.CacheService;
import com.censhare.support.model.QSelect;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.transaction.TransactionException;
import com.censhare.support.transaction.TransactionManager;
import com.censhare.support.xml.AXml;
import com.censhare.support.xml.CXml;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 * @author Cameron Cramsey
 *         3/16/17.
 */
public class IoiHelper {

    private AssetQueryService aqs;

    public IoiHelper() {
        aqs = ServiceLocator.getStaticService(AssetQueryService.class);
    }

    public Asset getAssetByIdExtern(TransactionManager tm, String idExtern) throws TransactionException {

        Asset asset = null;

        AssetQBuilder qb = aqs.prepareQBuilder();
        qb.setSqlFallback(false);
        QAsset tAsset = (QAsset) qb.getMainTable();
        QSelect select = qb.select(tAsset).where(qb.and(qb.expr(tAsset.ID_EXTERN, idExtern)));

        for (Iterator<Asset> it = aqs.queryAssets(select, tm).iterator(); it.hasNext();) {
            asset = it.next();
        }

        return asset;
    }

    public Asset getAssetById(TransactionManager tm, String id) throws TransactionException {

        Asset asset = null;

        AssetQBuilder qb = aqs.prepareQBuilder();
        qb.setSqlFallback(false);
        QAsset tAsset = (QAsset) qb.getMainTable();
        QSelect select = qb.select(tAsset).where(qb.and(qb.expr(tAsset.ID, id)));

        for (Iterator<Asset> it = aqs.queryAssets(select, tm).iterator(); it.hasNext();) {
            asset = it.next();
        }

        return asset;
    }

    public Asset getAssetByName(TransactionManager tm, String name) throws TransactionException {

        Asset asset = null;

        AssetQBuilder qb = aqs.prepareQBuilder();
        qb.setSqlFallback(false);
        QAsset tAsset = (QAsset) qb.getMainTable();
        QSelect select = qb.select(tAsset).where(qb.and(qb.expr(tAsset.NAME, name)));

        for (Iterator<Asset> it = aqs.queryAssets(select, tm).iterator(); it.hasNext();) {
            asset = it.next();
        }

        return asset;
    }

    public Asset getAssetByIDAndVersion(TransactionManager tm, String id, String version) throws TransactionException {

        Asset asset = null;

        AssetQBuilder qb = aqs.prepareQBuilder();
        qb.setSqlFallback(false);
        QAsset tAsset = (QAsset) qb.getMainTable();
        QSelect select = qb.select(tAsset).where(qb.and(qb.expr(tAsset.ID, id), qb.expr(tAsset.VERSION, version)));

        for (Iterator<Asset> it = aqs.queryAssets(select, tm).iterator(); it.hasNext();) {
            asset = it.next();
        }

        return asset;
    }

    public AXml findFeatureChangesByXml(Asset oldAsset, Asset newAsset, String featureKey) {

        AXml result = new CXml("result");

        AXml newOrChanged = new CXml("changed");
        AXml deleted = new CXml("deleted");

        String valTypeAttr = getAttrByValueType(featureKey);

        AXml oldAssetXml = oldAsset != null ? oldAsset.xml() : null;
        AXml newAssetXml = newAsset.xml();

        newAssetXml.findAllX("asset_feature[@feature='" + featureKey + "']").forEach((AXml newFeature) -> {
            if(oldAsset != null) {
                String val = newFeature.getAttr(valTypeAttr);
                AXml oldFeature = oldAssetXml.findX("asset_feature[@feature='" + featureKey + "' and @" + valTypeAttr + "='"
                        + val + "']");
                if (oldFeature == null) {
                    newOrChanged.appendChild(newFeature.cloneMutableDeep());
                } else {
                    boolean changesExist = hasChildFeaturesChanged(newFeature, oldFeature);
                    if (changesExist) {
                        newOrChanged.appendChild(newFeature.cloneMutableDeep());
                    }
                }
            } else {
                newOrChanged.appendChild(newFeature.cloneMutableDeep());
            }
        });

        if(oldAsset != null) {
            oldAssetXml.findAllX("asset_feature[@feature='" + featureKey + "']").forEach((AXml oldFeature) -> {
                String val = oldFeature.getAttr(valTypeAttr);
                AXml newFeature = newAssetXml.findX("asset_feature[@feature='" + featureKey + "' and @" + valTypeAttr + "='"
                        + val + "']");
                if (newFeature == null) {
                    //must be deleted
                    deleted.appendChild(oldFeature.cloneMutableDeep());
                }
            });
        }

        result.appendChild(newOrChanged.cloneMutableDeep());
        result.appendChild(deleted.cloneMutableDeep());
        return result;
    }

    public AXml findFeatureChangesByXmlS44(Asset oldAsset, Asset newAsset, String featureKey) {

        AXml result = new CXml("result");

        AXml newOrChanged = new CXml("changed");
        AXml deleted = new CXml("deleted");

        String valTypeAttr = getAttrByValueType(featureKey);

        AXml oldAssetXml = oldAsset != null ? oldAsset.xml() : null;
        AXml newAssetXml = newAsset.xml();

        newAssetXml.findAllX("asset_feature[@feature='" + featureKey + "']").forEach((AXml newFeature) -> {
            String val = newFeature.getAttr(valTypeAttr);
            String marketVal = featureKey != "s44:pim.acc.semcon-fitting-time-hours" ?
                    newFeature.find("asset_feature").getAttr("value_key") : null;
            if(oldAsset != null) {
                AXml oldFeature = featureKey != "s44:pim.acc.semcon-fitting-time-hours" ?
                        oldAssetXml.findX("asset_feature[@feature='" + featureKey + "' and @" + valTypeAttr + "='"
                                + val + "' and asset_feature[@feature='s44:pim.acc.market']/@value_key='" + marketVal + "']")
                        : oldAssetXml.findX("asset_feature[@feature='" + featureKey + "' and @" + valTypeAttr + "='"
                        + val + "']");
                if (oldFeature == null) {
                    newOrChanged.appendChild(newFeature.cloneMutableDeep());
                }
            } else {
                newOrChanged.appendChild(newFeature.cloneMutableDeep());
            }
        });

        if(oldAsset != null) {
            oldAssetXml.findAllX("asset_feature[@feature='" + featureKey + "']").forEach((AXml oldFeature) -> {
                String val = oldFeature.getAttr(valTypeAttr);
                String marketVal = featureKey != "s44:pim.acc.semcon-fitting-time-hours" ?
                        oldFeature.find("asset_feature").getAttr("value_key") : null;
                AXml newFeature = featureKey != "s44:pim.acc.semcon-fitting-time-hours" ?
                        newAssetXml.findX("asset_feature[@feature='" + featureKey +
                                "' and asset_feature[@feature='s44:pim.acc.market']/@value_key='" + marketVal + "']")
                        : newAssetXml.findX("asset_feature[@feature='" + featureKey + "' and @" + valTypeAttr + "='"
                        + val + "']");
                if (newFeature == null) {
                    //must be deleted
                    deleted.appendChild(oldFeature.cloneMutableDeep());
                }
            });
        }

        result.appendChild(newOrChanged.cloneMutableDeep());
        result.appendChild(deleted.cloneMutableDeep());
        return result;
    }

    private boolean hasChildFeaturesChanged(AXml newFeature, AXml oldFeature) {

        boolean exists = false;

        for(AXml newSubFeature : newFeature.findAllX("asset_feature")) {
            String subFeatureKey = newSubFeature.getAttr("feature");
            String subValTypeAttr = getAttrByValueType(subFeatureKey);
            String subVal = newSubFeature.getAttr(subValTypeAttr);
            AXml oldSubFeature = oldFeature.findX("asset_feature[@feature='" + subFeatureKey + "' and @"
                    + subValTypeAttr + "='" + subVal + "']");
            if(oldSubFeature == null) {
                exists = true;
                break;
            } else {
                exists = hasChildFeaturesChanged(newSubFeature, oldSubFeature);
            }
        }

        return exists;

    }

    public String getAttrByValueType(String featureKey) {

        CacheService cs = ServiceLocator.getStaticService(CacheService.class);
        Feature feature = cs.getEntry(Feature.newPk(featureKey));
        FeatureBase.FeatureValueType featureValType = feature.getValueType();

        switch (featureValType.getDBValue()) {
            case 1:
            case 2:
                return "value_key";
            case 3:
            case 8:
            case 6:
            case 27:
                return "value_long";
            case 4:
                return "value_string";
            case 5:
            case 13:
            case 25:
            case 21:
            case 23:
            case 15:
            case 17:
            case 19:
            case 9:
            case 14:
            case 26:
            case 22:
            case 24:
            case 16:
            case 18:
            case 20:
                return "value_timestamp";
            case 7:
            case 12:
                return "value_double";
            case 10:
                return "value_asset_id";
            case 28:
                return "value_string";
            case 29:
                return "value_asset_key_ref";
            case 11:
                return "xmldata";
            default:
                return null;
        }
    }

    public Asset getAssetByResourceKey(TransactionManager tm, String resourceKey) throws Exception {

        Asset asset = null;

        AssetQBuilder qb = aqs.prepareQBuilder();
        qb.setSqlFallback(false);
        QAsset tAsset = (QAsset) qb.getMainTable();
        QAssetFeature tAssetFeature = qb.table(QAssetFeature.class);
        QSelect select = qb.select(tAsset).where(qb.and(qb.feature("censhare:resource-key", resourceKey)));

        for (Iterator<Asset> it = aqs.queryAssets(select, tm).iterator(); it.hasNext();) {
            asset = it.next();
            break;
        }

        return asset;
    }
}
