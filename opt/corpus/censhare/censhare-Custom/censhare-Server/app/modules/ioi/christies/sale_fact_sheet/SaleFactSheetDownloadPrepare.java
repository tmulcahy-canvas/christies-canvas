package modules.ioi.christies.sale_fact_sheet;

import com.censhare.server.kernel.Command;
import com.censhare.support.xml.AXml;

/**
 * @author Cameron Cramsey
 * 1/11/18.
 */
public class SaleFactSheetDownloadPrepare {

    private Command command;

    public SaleFactSheetDownloadPrepare(Command command) {
        this.command = command;
    }

    public int prepare() {

        AXml cmdXml = (AXml) command.getSlot(Command.XML_COMMAND_SLOT);

        cmdXml.appendChild(cmdXml.findX("generate/download").cloneMutableDeep());

        command.setSlot(Command.XML_COMMAND_SLOT, cmdXml);

        return Command.CMD_COMPLETED;
    }

}
