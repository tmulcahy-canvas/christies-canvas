package modules.ioi.lib.ioiExcel.export;

import com.censhare.manager.assetmanager.AssetManagementService;
import com.censhare.model.corpus.generated.FeatureBase;
import com.censhare.model.corpus.impl.Asset;
import com.censhare.model.corpus.impl.AssetTyping;
import com.censhare.model.corpus.impl.ChildAssetRel;
import com.censhare.model.corpus.impl.Feature;
import com.censhare.server.kernel.Command;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.cache.CacheService;
import com.censhare.support.io.FileFactoryService;
import com.censhare.support.io.FileLocator;
import com.censhare.support.model.DataObjectIterable;
import com.censhare.support.model.PersistenceManager;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.xml.AXml;
import com.censhare.support.xml.CXml;
import com.censhare.support.xml.Node;
import com.censhare.support.xml.xpath.XPathContext;
import com.censhare.support.xml.xpath.XPathExpr;
import com.censhare.support.xml.xpath.XPathParser;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.WorkbookUtil;
import org.apache.poi.xssf.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Cameron on 1/24/17.
 */


public class IoiExcelGenerator {

    private final Command command;
    private final Logger logger;

    private XPathParser xp;
    private FileFactoryService fileFactory;
    private DBTransactionManager tm;
    private PersistenceManager pm;
    private AssetManagementService am;
    private static final Pattern NAME_REGEX = Pattern.compile("\\{[a-zA-z1-9-_]*}");

    private Map<Long, Asset> assets2Update;
    private Map<Long, List<Long>> loopMap;

    public IoiExcelGenerator(Command command) throws Exception {
        this.command = command;
        logger = command.getLogger();
        xp = new XPathParser();
        fileFactory = ServiceLocator.getStaticService(FileFactoryService.class);
        tm = command.getTransactionManager();
        pm = tm.getDataObjectTransaction().getPersistenceManager();
        am = ServiceLocator.getStaticService(AssetManagementService.class);
        assets2Update = new HashMap<>();
        loopMap = new TreeMap<>();
    }

    public int ioiExcelExportPrepare() throws Exception {
        AXml cmdXml = (AXml) command.getSlot(Command.XML_COMMAND_SLOT);

        // var to hold config / mappings from command xml
        AXml config = cmdXml.find("excelConfig");
        AXml mappings = cmdXml.find("mappings");
        AXml assets = null;
        if(config.getAttrBoolean("processOnChildren", false)) {
            assets = gatherChildAssetsAXml(cmdXml.find("assets"), config.find("childSettings"));
        } else {
            assets = cmdXml.find("assets");
        }
        AXml styling = cmdXml.find("excelStyles");

        AXml update = cmdXml.find("updateWorkflow");

        // Prepare the worksheet and export
        if (config == null)
            throw new Exception("Command does not contain configuration");
        AXml result = exportExcel(config, mappings, assets, styling, update);

        // Get element to return data from excel generation to.
        AXml commandStepXml = command.getTarget();
        if (commandStepXml == null)
            throw new IllegalStateException("Current command step not found");
        String elementKey = commandStepXml.getAttr("key", null);
        AXml dataExchangeElement = cmdXml.find(elementKey);

        // if there is a result append it to the data exchange element
        if (result != null) {
            AXml resultXml = result;
            if (resultXml.getNodeType() == Node.DOCUMENT_NODE && resultXml.getFirstChildElement() != null)
                resultXml = resultXml.getFirstChildElement(); // select root element

            dataExchangeElement.removeAllChildNodes();
            dataExchangeElement.appendChild(resultXml.cloneMutableDeep());
        }

        AXml downloadFile = cmdXml.create("download.file");
        downloadFile.put("@filesys_name", result.find("file").getAttr("filesys_name"));
        downloadFile.put("@relpath", result.find("file").getAttr("relPath"));
        downloadFile.put("@target_name", result.find("file").getAttr("target_name"));
        logger.info("Prepared result for download: fs=" + result.find("file").getAttr("filesys_name") + ", relPath=" + result.find("file").getAttr("relPath"));

        tm.begin();
        for(Map.Entry<Long, Asset> asset4update : assets2Update.entrySet()) {
            am.update(tm, asset4update.getValue());
        }
        tm.end();

        return Command.CMD_COMPLETED;
    }

    private AXml gatherChildAssetsAXml(AXml assets, AXml processChildConfig) {
        // AXml obj to return
        AXml assets2Return = new CXml("assets");

        // Settings for gathering children
        List<String> relsToFollow = new ArrayList<>();
        for(AXml rel : processChildConfig.findAll("rel")) {
            relsToFollow.add(rel.getAttr("followRelation", "*"));
        }
        boolean recursive = processChildConfig.getAttrBoolean("recursive", false);
        boolean filterDupes = processChildConfig.getAttrBoolean("filterDupes", false);

        // Iterate over selected assets and gather their children
        for(AXml asset : assets.findAll("asset")) {

            Asset currAsset = am.cacheGetAssetCurrent(tm, asset.getAttrLong("id"));
            AXml gatheredAssets = new CXml("assets");
            for(ChildAssetRel childRel : getRelIter(currAsset, relsToFollow)) {
                Asset childAsset = am.cacheGetAssetCurrent(tm, childRel.getChildAsset());
                AXml gathered = processAssetForGather(childAsset, relsToFollow, recursive, currAsset.getId());
                // logger.info("GATHERED - " + gathered.toStringPretty());
                for(AXml foundAsset : gathered.findAll("asset")) {
                    gatheredAssets.appendChild(foundAsset.cloneMutableDeep());
                }
            }

            List<Long> foundIds = new ArrayList<>();

            for(AXml returnedAsset : gatheredAssets.findAll("asset")) {
                if(filterDupes) {
                    if(foundIds.indexOf(returnedAsset.getAttrLong("id")) == -1){
                        assets2Return.appendChild(returnedAsset.cloneMutableDeep());
                        foundIds.add(returnedAsset.getAttrLong("id"));
                    }
                } else {
                    assets2Return.appendChild(returnedAsset.cloneMutableDeep());
                }
            }
        }

        return assets2Return;
    }

    private AXml processAssetForGather(Asset currAsset, List<String> relToFollow, boolean recursive, Long parId) {

        // AXml obj to return
        AXml a2Return = new CXml("assets");

        boolean noDupe = true;

        if(!loopMap.containsKey(parId)) {
            List<Long> childs = new ArrayList<>();
            childs.add(currAsset.getId());
            loopMap.put(parId, childs);
        } else {
            List<Long> childs = loopMap.get(parId);
            logger.info("TEH CHILDS - " + childs.toString());
            if(!childs.contains(currAsset.getId())) {
                childs.add(currAsset.getId());
            } else {
                noDupe = false;
            }
        }

        if(noDupe) {
            logger.info("PROCESSING - " + currAsset);
            a2Return.appendChild(currAsset.xml().cloneMutableDeep());

            if(recursive) {
                for(ChildAssetRel childRel : getRelIter(currAsset, relToFollow)) {
                    Asset childAsset = am.cacheGetAssetCurrent(tm, childRel.getChildAsset());
                    AXml assets = processAssetForGather(childAsset, relToFollow, recursive, currAsset.getId());
                    for(AXml asset : assets.findAll("asset")) {
                        a2Return.appendChild(asset.cloneMutableDeep());
                    }
                }
            }
        } else {
            logger.info("NOT PROCESSING - " + currAsset + " - IT IS A DUPE!");
        }

        return a2Return;
    }

    private List<ChildAssetRel> getRelIter(Asset currAsset, List<String> relToFollow) {

        List<ChildAssetRel> relItrTemp = new ArrayList<>();

        if(relToFollow != null && !relToFollow.isEmpty() && !relToFollow.contains("*")) {
            for(String rel : relToFollow) {
                for(ChildAssetRel cRel : currAsset.struct().getChildAssetRelIter(AssetTyping.AssetRelSubType.dbValueOf(rel))) {
                    relItrTemp.add(cRel);
                }
            }
        } else {
            for(ChildAssetRel cRel : currAsset.struct().getChildAssetRelIter()) {
                relItrTemp.add(cRel);
            }
        }

        return relItrTemp;
    }

    public AXml exportExcel(AXml config, AXml mappings, AXml assets, AXml styling, AXml update) {

        // Node to return
        AXml result = new CXml("result");

        AXml wb = config.find("workbook");

        XSSFWorkbook workbook = new XSSFWorkbook();
        String workbookName = generateName(wb.getAttr("name"), mappings, assets);

        // Generate Fonts and assign to fonts arraylist
        Map<String, XSSFFont> fonts = generateFonts(styling, workbook);

        // Generate Styles and assign to styles arraylist
        Map<String, XSSFCellStyle> styles = generateStyles(styling, fonts, workbook);
        
        // Is a normal or horizontal Sheet
        if(config.getAttrBoolean("horizontal", false)) {
            // Special sheet where header is in column A
            String orderOn = config.getAttr("orderOn", "id");
            generateSheetsHorizontal(workbook, wb, mappings, assets, styles, update, orderOn);
        } else {
            // Generate Sheets
            generateSheets(workbook, wb, mappings, assets, styles, update);
        }

        //Export File
        try {
            FileLocator tempDir = fileFactory.createAutoDeleteTempDirectory(tm, "", "");
            String childPath = "work/temp/" + tempDir.getFileName();
            File home = new File(System.getProperty("user.home"), childPath);
            FileOutputStream fileOut = new FileOutputStream(home + File.separator + workbookName + ".xlsx");
            workbook.write(fileOut);
            fileOut.close();

            AXml file = new CXml("file");
            file.setAttr("fileLoc", (childPath + "/" + workbookName + ".xlsx"));
            file.setAttr("filesys_name", tempDir.getFsName());
            file.setAttr("relPath", (tempDir.getRelURL() + "/" + workbookName + ".xlsx"));
            file.setAttr("target_name", (workbookName + ".xlsx"));
            result.appendChild(file.cloneMutableDeep());
        } catch (Exception e) {
            logger.info("   ###   IOI EXCEL - error encountered exporting file - " + e.getMessage());
        }

        return result;
    }

    private Map generateFonts(AXml styling, XSSFWorkbook workbook) {
        Map<String, XSSFFont> fonts = new HashMap<>();
        for(AXml f : styling.findAllX("fonts/font")) {
            XSSFFont font = workbook.createFont();
            for (Node attr : f.getAttributes()) {
                String attrName = attr.getNodeName();
                String attrValue = attr.getNodeValue();
                if(attrName != "name" && !attrName.equals("corpus:dto_flags")) {
                    FontSetters.valueOf(attrName).execute(attrValue, font);
                }
            }
            fonts.put(f.getAttr("name"), font);
        }
        return fonts;
    }

    private Map generateStyles(AXml styling, Map<String, XSSFFont> fonts, XSSFWorkbook workbook) {
        Map<String, XSSFCellStyle> styles = new HashMap<>();
        for(AXml s : styling.findAllX("styles/style")) {
            XSSFCellStyle cs = workbook.createCellStyle();
            for (Node attr : s.getAttributes()) {
                String attrName = attr.getNodeName();
                String attrValue = attr.getNodeValue();
                XSSFFont font = null;
                if (attrName == "font" && attrValue != null) {
                    font = fonts.get(attrValue);
                }
                if(attrName != "name" && !attrName.equals("corpus:dto_flags")) {
                    StyleSetters.valueOf(attrName).execute(attrValue, cs, font);
                }
            }
            styles.put(s.getAttr("name"), cs);
        }
        return styles;
    }

    private void generateSheets(XSSFWorkbook workbook, AXml wb, AXml mappings, AXml assets,
                                Map<String, XSSFCellStyle> styles, AXml update) {
        for(AXml s : wb.findAllX("sheet[@enabled=1]")) {
            // instantiate rowCount at 0 for header row and then will increment every iteration over assets
            int rowCount = 0;
            String sheetName = generateName(s.getAttr("name"), mappings, assets);
            XSSFSheet sheet = workbook.createSheet(sheetName);

            // create header row
            XSSFRow headerRow = sheet.createRow(rowCount);
            int headerCellCount = 0;
            for (AXml row : s.findAllX("map")) {
                String header = row.getAttr("header");
                String headerStyle = row.getAttr("headerStyle");
                String xpathResult;

                String mappingPath = "map[@property='" + header + "']";
                AXml pathNode = mappings.findX(mappingPath);
                XPathContext context = new XPathContext(pathNode);
                if(pathNode != null) {
                    String path = pathNode.getAttr("headerpath");
                    XPathExpr expr = xp.parse(path);

                    xpathResult = expr.eval(context).toString();
                } else {
                    xpathResult = header;
                }
                XSSFCellStyle cs = null;
                if(headerStyle != null) {
                    cs = styles.get(headerStyle);
                }
                XSSFCell cell = headerRow.createCell(headerCellCount);
                cell.setCellValue(xpathResult);
                if(cs != null) {
                    cell.setCellStyle(cs);
                }
                headerCellCount++;
            }
            // increment header row
            rowCount++;

            // update asset data
            int updateWorkflowID = update != null ? update.getAttrInt("workflow", -1) : -1;
            int updateWorkflowStep = update != null ?  update.getAttrInt("workflowStep", -1) : -1;
            boolean performUpdate = update != null && update.getAttrBoolean("enabled", false);

            // Loop assets and create data rows.
            for(AXml asset : assets.findAllX("asset")) {

                XPathContext context = new XPathContext(asset);

                // create asset row
                XSSFRow assetRow = sheet.createRow(rowCount);
                int assetCellCount = 0;
                for (AXml row : s.findAllX("map")){

                    boolean mapFeat = row.getAttrBoolean("featUsed");
                    String rowStyle = row.getAttr("style");
                    XSSFCell cell = assetRow.createCell(assetCellCount);
                    XSSFCellStyle cs = null;

                    // use feature dropdown
                    cell.setCellValue(getValueFromAssetWithMapping(row, mapFeat, context, mappings));

                    if(rowStyle != null) {
                        cs = styles.get(rowStyle);
                    }
                    if(cs != null) {
                        cell.setCellStyle(cs);
                    }

                    assetCellCount++;
                }
                //increment rowCount
                rowCount++;

                if(performUpdate) {
                    try {
                        tm.begin();
                        Asset assetToUpdate = am.cacheGetAssetCurrent(tm, asset.getAttrLong("id"));
                        assetToUpdate.setWfId(updateWorkflowID);
                        assetToUpdate.setWfStep(updateWorkflowStep);
                        if(!assetToUpdate.isPersistent())
                            pm.makePersistentRecursive(assetToUpdate);
                        // add asset to map
                        assets2Update.put(asset.getAttrLong("id"), assetToUpdate);
                        tm.end();
                    } catch(Exception e) {
                        logger.warning("Unable to update asset - " + e);
                    }
                }
            }
        }
    }
    
    private void generateSheetsHorizontal(XSSFWorkbook workbook, AXml wb, AXml mappings, AXml assets,
            Map<String, XSSFCellStyle> styles, AXml update, String orderOn) {
        
        // update asset data
        int updateWorkflowID = update != null ? update.getAttrInt("workflow", -1) : -1;
        int updateWorkflowStep = update != null ?  update.getAttrInt("workflowStep", -1) : -1;
        boolean performUpdate = update != null && update.getAttrBoolean("enabled", false);
        
        // bool used to ensure each asset being queried has the orderOn attr.  If one does not meet then ID will be used.
        boolean orderOnCheck = true;
        
        List<AXml> assetsList = new ArrayList<>();
        
        for(AXml assetXml : assets.findAll("asset")) {
            orderOnCheck = assetXml.getAttr(orderOn) != null;
            assetsList.add(assetXml);
        }

        String orderOnString = orderOnCheck ? orderOn : "id";
        
        Collections.sort(assetsList, new AssetCompare(orderOnString));
        
        for(AXml s : wb.findAllX("sheet[@enabled=1]")) {
            
            // instantiate rowCount at 0 for row and then will increment every iteration over configured header
            int rowCount = 0;
            String sheetName = generateName(s.getAttr("name"), mappings, assets);
            XSSFSheet sheet = workbook.createSheet(sheetName);
         
            // Create one row per header map.
            for (AXml row : s.findAllX("map")) {

                int cellCount = 0;
                XSSFRow excelRow = sheet.createRow(rowCount);
                
                String header = row.getAttr("header");
                String headerStyle = row.getAttr("headerStyle");
                String xpathResult;

                String mappingPath = "map[@property='" + header + "']";
                AXml pathNode = mappings.findX(mappingPath);
                XPathContext context = new XPathContext(pathNode);
                if(pathNode != null) {
                    String path = pathNode.getAttr("headerpath");
                    XPathExpr expr = xp.parse(path);

                    xpathResult = expr.eval(context).toString();
                } else {
                    xpathResult = header;
                }
                XSSFCellStyle cs = null;
                if(headerStyle != null) {
                    cs = styles.get(headerStyle);
                }
                XSSFCell cell = excelRow.createCell(cellCount);
                cell.setCellValue(xpathResult);
                if(cs != null) {
                    cell.setCellStyle(cs);
                }
                // increment cell count
                cellCount++;
                for(AXml assetXml : assetsList) {

                    XPathContext assetContext = new XPathContext(assetXml);
                    boolean mapFeat = row.getAttrBoolean("featUsed");
                    String rowStyle = row.getAttr("style");
                    XSSFCell assetCell = excelRow.createCell(cellCount);
                    XSSFCellStyle assetCellStyle = null;
                    
                    // use feature dropdown
                    assetCell.setCellValue(getValueFromAssetWithMapping(row, mapFeat, assetContext, mappings));
                    
                    if(rowStyle != null) {
                        assetCellStyle = styles.get(rowStyle);
                    }
                    if(assetCellStyle != null) {
                        assetCell.setCellStyle(assetCellStyle);
                    }
                    
                    // increment cell count
                    cellCount++;
                    
                    if(performUpdate) {
                        try {
                            tm.begin();
                            Asset assetToUpdate = am.cacheGetAssetCurrent(tm, assetXml.getAttrLong("id"));
                            assetToUpdate.setWfId(updateWorkflowID);
                            assetToUpdate.setWfStep(updateWorkflowStep);
                            if(!assetToUpdate.isPersistent())
                                pm.makePersistentRecursive(assetToUpdate);
                            // add asset to map
                            assets2Update.put(assetXml.getAttrLong("id"), assetToUpdate);
                            tm.end();
                        } catch(Exception e) {
                            logger.warning("Unable to update asset - " + e);
                        }
                    }
                }

                // increment row
                rowCount++;
            }
        }
    
    }
    
    private String getValueFromAssetWithMapping(AXml row, boolean mapFeat, XPathContext context, AXml mappings) {
        
        String xpathResult = "";
        
        if(mapFeat) {
            String feat = row.getAttr("feature");
            Feature f = ServiceLocator.getStaticService(CacheService.class).getEntry(Feature.newPk(feat));
            // is feature an attribute?
            if(f.getAttributeMapping() != null) {
                String path = f.getAttributeMapping();
                XPathExpr expr = xp.parse(path);
                xpathResult = expr.eval(context).toString();
                if (xpathResult != null) {
                    return xpathResult;
                }
                // not an attribute
            } else {
                FeatureBase.FeatureValueType valType = f.getValueType();
                String attrVal = null;
                switch (valType) {
                    case HIERARCHICAL:
                    case ENUMERATION:
                        attrVal = "value_key";
                        break;
                    case STRING:
                        attrVal = "value_string";
                        break;
                    case BOOLEAN:
                    case INTEGER:
//                case INTEGER_PAIR:
                        attrVal = "value_long";
                        break;
                    case ASSET:
                        attrVal = "value_asset_id";
                        break;
                    case DOUBLE:
//                case DOUBLE_PAIR:
                        attrVal = "value_double";
                        break;
                    case TIMESTAMP:
                    case DATE:
                    case TIME:
                    case YEAR:
                    case YEAR_MONTH:
                    case MONTH_DAY:
                    case DAY:
                        attrVal = "value_timestamp";
                        break;
                    default:
                        break;
                }
                if (attrVal != null) {
                    String path = "asset_feature[@feature='" + feat + "'][1]/@" + attrVal;
                    XPathExpr expr = xp.parse(path);
                    xpathResult = expr.eval(context).toString().replace("[", "").replace("]", "");
                    if (xpathResult != null) {
                        return xpathResult;
                    }
                }
            }
        // use xpath configured
        } else {
            String mappingName = row.getAttr("path");
            String mappingPath = "map[@property='" + mappingName + "']";
            AXml pathNode = mappings.findX(mappingPath);
            if(pathNode != null) {
                String path = pathNode.getAttr("valuepath");
                XPathExpr expr = xp.parse(path);

                xpathResult = expr.eval(context).toString().replace("[", "").replace("]", "");
            } else {
                xpathResult = mappingName + " - No mapping configured with this mapping name.";
            }
            if(xpathResult != null || xpathResult != "") {
                return xpathResult;
            }
        }
        
        return xpathResult;
        
    }

    private String generateName(String name, AXml mappings, AXml assets) {

        String finalName = name;

        Matcher match = NAME_REGEX.matcher(name);
        
        while(match.find()) {
            String matchedGroup = match.group(0);
            String attrName = matchedGroup.replaceAll("\\{", "").replaceAll("}", "");
            XPathContext context = new XPathContext(mappings);
            
            context.setVariable("assets", assets);
            
            String path = mappings.getAttr(attrName);
            XPathExpr expr = xp.parse(path);

            String xpathResult = expr.eval(context).toString();
            if(xpathResult.startsWith("[")) {
                xpathResult = xpathResult.replace("[", "").replace("]", "");
            }

            String matchedGroupMod = "\\" + matchedGroup;
            finalName = finalName.replaceAll(matchedGroupMod, xpathResult);
        }

        return WorkbookUtil.createSafeSheetName(finalName);
    }

    public enum FontSetters {
        boldWeight() {
            @Override
            public void execute(String attr, XSSFFont font) {
                short s = Short.parseShort(attr);
                font.setBoldweight(s);
            }
        },
        charSet() {
            @Override
            public void execute(String attr, XSSFFont font) {
                // Leave out of UI unless needed
                byte b = Byte.parseByte(attr);
                font.setCharSet(b);
            }
        },
        color() {
            @Override
            public void execute(String attr, XSSFFont font) {
                int i = Integer.parseInt(attr);
                XSSFColor color = new XSSFColor(new java.awt.Color(i));
                font.setColor(color);
            }
        },
        fontHeight() {
            @Override
            public void execute(String attr, XSSFFont font) {
                short s = Short.parseShort(attr);
                font.setFontHeightInPoints(s);
            }
        },
        fontName() {
            @Override
            public void execute(String attr, XSSFFont font) {
                font.setFontName(attr);
            }
        },
        italic() {
            @Override
            public void execute(String attr, XSSFFont font) {
                Boolean bool = Boolean.parseBoolean(attr);
                font.setItalic(bool);
            }
        },
        strikeout() {
            @Override
            public void execute(String attr, XSSFFont font) {
                Boolean bool = Boolean.parseBoolean(attr);
                font.setStrikeout(bool);
            }
        },
        typeOffset() {
            @Override
            public void execute(String attr, XSSFFont font) {
                short s = Short.parseShort(attr);
                font.setTypeOffset(s);
            }
        },
        underline() {
            @Override
            public void execute(String attr, XSSFFont font) {
                byte b = Byte.parseByte(attr);
                font.setUnderline(b);
            }
        };

        public abstract void execute(String attr, XSSFFont font);
    }

    public enum StyleSetters {
        horizontalAlignment() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setAlignment(HorizontalAlignment.valueOf(attr));
            }
        },
        borderBottom() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setBorderBottom(BorderStyle.valueOf(attr));
            }
        },
        borderLeft() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setBorderLeft(BorderStyle.valueOf(attr));
            }
        },
        borderRight() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setBorderRight(BorderStyle.valueOf(attr));
            }
        },
        borderTop() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setBorderTop(BorderStyle.valueOf(attr));
            }
        },
        bottomBorderColor() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                int i = Integer.parseInt(attr);
                XSSFColor color =new XSSFColor(new java.awt.Color(i));
                cs.setBottomBorderColor(color);
            }
        },
        leftBorderColor() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                int i = Integer.parseInt(attr);
                XSSFColor color =new XSSFColor(new java.awt.Color(i));
                cs.setLeftBorderColor(color);
            }
        },
        rightBorderColor() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                int i = Integer.parseInt(attr);
                XSSFColor color =new XSSFColor(new java.awt.Color(i));
                cs.setRightBorderColor(color);
            }
        },
        topBorderColor() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                int i = Integer.parseInt(attr);
                XSSFColor color =new XSSFColor(new java.awt.Color(i));
                cs.setTopBorderColor(color);
            }
        },
        dataFormat() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                short s = Short.parseShort(attr);
                cs.setDataFormat(s);
            }
        },
        fillBackgroundColor() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                int i = Integer.parseInt(attr);
                XSSFColor color = new XSSFColor(new java.awt.Color(i));
                cs.setFillBackgroundColor(color);
            }
        },
        fillForegroundColor() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                int i = Integer.parseInt(attr);
                XSSFColor color = new XSSFColor(new java.awt.Color(i));
                cs.setFillForegroundColor(color);
            }
        },
        fillPattern() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setFillPattern(FillPatternType.valueOf(attr));
            }
        },
        font() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setFont(font);
            }
        },
        hidden() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                Boolean bool = Boolean.parseBoolean(attr);
                cs.setHidden(bool);
            }
        },
        indentation() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                short s = Short.parseShort(attr);
                cs.setIndention(s);
            }
        },
        locked() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                Boolean bool = Boolean.parseBoolean(attr);
                cs.setLocked(bool);
            }
        },
        rotation() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                short s = Short.parseShort(attr);
                cs.setRotation(s);
            }
        },
        wrapText() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                Boolean bool = Boolean.parseBoolean(attr);
                cs.setWrapText(bool);
            }
        },
        verticalAlignment() {
            @Override
            public void execute(String attr, XSSFCellStyle cs, XSSFFont font) {
                cs.setVerticalAlignment(VerticalAlignment.valueOf(attr));
            }
        };

        public abstract void execute(String attr, XSSFCellStyle cs, XSSFFont font);
    }

    private class AssetCompare implements Comparator<AXml> {
        
        private String attr;
        
        public AssetCompare(String orderOn) {
            super();
            this.attr = orderOn;
        }
        
        @Override
        public int compare(AXml a1, AXml a2) {
            return a1.getAttr(attr, "").compareTo(a2.getAttr(attr, ""));
        }
    }

}
