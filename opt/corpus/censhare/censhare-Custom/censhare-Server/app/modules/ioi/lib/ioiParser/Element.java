package modules.ioi.lib.ioiParser;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Element {

    private String origString;
    private String fixedString;
    private String elemName;
    private List<String> keepers;

    TreeMap<Integer, Tag> tagMapByEndPosition = new TreeMap<>();

    public Element(String element, List<String> keepers) {
        this.origString = element;
        this.keepers = keepers;
        if(keepers.contains("br")) {
            element = element.replace("<br/>", "\n");
        }
        this.fixedString = process(element, "");
    }

    private String process(String elementString, String space) {

        boolean inTag = false;
        boolean closeTag = false;
        String tagName = "";
        int openingTagStart = 0;


        for(int i = 0, length = elementString.length(); i < length; i++) {
            char c = elementString.charAt(i);
            if(inTag) {
                if(c == '>') {
                    if(!tagMapByEndPosition.containsKey(i)) {

                        Tag tag = new Tag(elementString.substring(i + 1), tagName, openingTagStart, i,
                                closeTag ? TagType.CLOSE : TagType.OPEN, true, closeTag? true : false);

                        if(!tagMapByEndPosition.containsKey(tag.tagPosition.closePosition))
                            tagMapByEndPosition.put(i, tag);

                        Tag close = tag.closingTag;
                        if(close != null && !tagMapByEndPosition.containsKey(close.tagPosition.closePosition))
                            tagMapByEndPosition.put(close.tagPosition.closePosition, close);
                    }
                    inTag = false;
                    closeTag = false;
                } else {
                    if(c != '/') {
                        tagName = tagName + c;
                    } else {
                        closeTag = true;
                    }
                }
            } else {
                if(c == '<') {
                    inTag = true;
                    tagName = "";
                    openingTagStart = i;
                }
            }
        }

        for(Map.Entry<Integer, Tag> entry : tagMapByEndPosition.descendingMap().entrySet()) {
            Tag curTag = entry.getValue();
            if(!curTag.isValid)
                elementString = elementString.substring(0, curTag.tagPosition.openPosition) + elementString.substring(curTag.tagPosition.closePosition + 1);
        }

        return elementString;
    }

    public String getOrigString() {
        return this.origString;
    }

    public String getElemName() {
        return this.elemName;
    }

    public String getFixedString() {
        return this.fixedString;
    }

    private class Tag {

        String restOfText;
        String tagName;
        TagPosition tagPosition;
        boolean isValid;
        TagType tagType;
        Tag closingTag;

        public Tag(String restOfText, String tagName, int startPosition, int closePosition, TagType type,
                   boolean fromProcess, boolean immediateInvalid) {

            this.restOfText = restOfText;
            this.tagName = tagName;
            this.tagPosition = new TagPosition(startPosition, closePosition);
            this.tagType = type;
            this.closingTag = null;

            if(type == TagType.OPEN)
                findClosingTag();

            if(immediateInvalid || (this.tagType == TagType.OPEN && this.closingTag == null)) // || (fromProcess && this.tagType == TagType.CLOSE)
                isValid = false;
            else
                isValid = true;
        }

        private void findClosingTag() {

            boolean inTag = false;
            boolean closeTag = false;

            String curTagName = "";
            int startingPosition = 0;

            for(int i = 0, length = this.restOfText.length(); i < length; i++){
                char c = this.restOfText.charAt(i);

                if (inTag) {

                    if (c == '>') {

                        int fullStartPosition = this.tagPosition.closePosition + startingPosition + 1;
                        int fullEndPosition = this.tagPosition.closePosition + i + 1;

                        if (curTagName.equals(this.tagName) && closeTag && !tagMapByEndPosition.containsKey(fullEndPosition)) {
                            if(this.closingTag == null) {
                                this.closingTag = new Tag(this.restOfText.substring(i + 1),
                                        curTagName, fullStartPosition, fullEndPosition, TagType.CLOSE, false, false);
                            } else {
                                this.closingTag.isValid = false;
                                this.closingTag =  new Tag(this.restOfText.substring(i + 1),
                                        curTagName, fullStartPosition, fullEndPosition, TagType.CLOSE, false, false);
                                break;
                            }
                        } else if (curTagName.equals(this.tagName) && !closeTag && !tagMapByEndPosition.containsKey(fullEndPosition)) {
                            if(this.closingTag == null) {
                                Tag badTag = new Tag(this.restOfText.substring(i + 1),
                                        curTagName, fullStartPosition, fullEndPosition, TagType.OPEN, false, true);
                                if(!tagMapByEndPosition.containsKey(badTag.tagPosition.closePosition)) {
                                    tagMapByEndPosition.put(badTag.tagPosition.closePosition, badTag);
                                    Tag closingTag = badTag.closingTag;
                                }

                            } else {
                                break;
                            }
                        }

                        inTag = false;

                    } else {

                        if (c != '/') {
                            curTagName = curTagName + c;
                        } else {
                            closeTag = true;
                        }

                    }

                } else {

                    if (c == '<') {
                        inTag = true;
                        curTagName = "";
                        startingPosition = i;
                    }

                }
            }
        }

        @Override
        public String toString() {
            return "Tag{" +
                    "tagName=" + tagName +
                    ", tagPosition=" + tagPosition +
                    ", isValid=" + isValid +
                    '}';
        }
    }

    private class TagPosition {

        int openPosition, closePosition;

        public TagPosition(int open, int close) {
            this.openPosition = open;
            this.closePosition = close;
        }

        @Override
        public String toString() {
            return "TagPosition{" +
                    "openPosition=" + openPosition +
                    ", closePosition=" + closePosition +
                    '}';
        }
    }

    private enum TagType{
        OPEN,
        CLOSE
    }
}