<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:ioi="http://www.iointegration.com" exclude-result-prefixes="xs html censhare cs io ioi">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
  <xsl:param name="censhare:command-xml"/>

  <xsl:template match="asset">

    <xsl:variable name="properties">
      <xsl:copy-of select="./cs:child-rel()[@key='*']/.[@type='product.']"/>
    </xsl:variable>
    <!-- nh <xsl:message> *+*+* PROPERTIES *****<xsl:value-of select="$properties/asset[1]/@id"/></xsl:message> nh -->

    <!-- cc - open vfs - cc -->
    <xsl:variable name="out"/>
    <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"></cs:command>

    <!-- cc - get christies lot card template - cc -->
    <xsl:variable name="template" select="cs:asset()[@censhare:resource-key='ioi:canvas_lot-card-template']"/>
    <xsl:variable name="tempFileName" select="concat($template/@id, '.indd')"/>

    <!-- cc - filesystem && relpath - cc -->
    <xsl:variable name="filesys" select="tokenize($out, '/')[6]"/>
    <xsl:variable name="relpath" select="concat('file:', substring-after($out, concat($filesys, '/')), $tempFileName)"/>
    
    <!-- cc - create url using temp vfs for the copy location - cc -->
    <xsl:variable name="tempCopyURL" select="concat($out, $tempFileName)"/>
    
    <!-- cc - copy template to vfs for use - cc -->
    <cs:command name="com.censhare.api.io.Copy">
      <cs:param name="source" select="$template/storage_item[@key='master']"/>
      <cs:param name="dest" select="$tempCopyURL"/>
    </cs:command>
    
    <!-- cc - get app version from the template - cc -->
    <xsl:variable name="appVersion" select="$template/storage_item[@key='master']/@app_version" />

    <!-- cc - get the property children - cc -->
    <!-- cc - count properties - cc -->
    <xsl:variable name="count" select="count($properties/asset)" as="xs:string"/>

    <xsl:variable name="script">
      <cs:command name="com.censhare.api.christiesLotCardScript.generate">
        <cs:param name="count" select="$count"/>
      </cs:command>
    </xsl:variable>

    <!-- nh <xsl:message><xsl:copy-of select="$script"/></xsl:message> nh -->

    <!-- cc - call the renderer - cc -->
    <xsl:variable name="renderer-result-xml">
      <cs:command name="com.censhare.api.Renderer.Render">
        <cs:param name="facility" select="concat('indesign-', $appVersion)"/>
        <cs:param name="instructions">
          <cmd>
            <renderer>
              <command method="open" asset_id="{$properties/asset[1]/@id}" document_ref_id="1"/>
              <command method="script" document_ref_id="1">
                <xsl:copy-of select="$script"/>
              </command>
              <command method="update-placeholder" asset_element_id="0" document_ref_id="1"/>
              <command method="save" document_ref_id="1"/>
              <command method="close" document_ref_id="1"/>
            </renderer>
            <assets>
              <asset>
                <xsl:copy-of select="$properties/asset[1]/@*"/>
                <storage_item app_version="{$appVersion}" asset_id="{./@id}" filesys_name="{$filesys}" key="master" mimetype="application/indesign" relpath="{$relpath}" url="{$tempCopyURL}"></storage_item>
                <xsl:copy-of select="$properties/asset[1]/node()"/>
                <xsl:for-each select="$properties/asset[1]/cs:child-rel()[@key='user.']/.[contains(@type, 'text.')]">
                    <xsl:copy-of select="ioi:get-xml-file-contents(.)"/>
                </xsl:for-each>
                <xsl:apply-templates select="$properties/asset[position() gt 1]" mode="trick"/>
              </asset>
            </assets>
          </cmd>
        </cs:param>
      </cs:command>
    </xsl:variable>

    <xsl:message> ***** RENDERER XML ***** <xsl:copy-of select="$renderer-result-xml"/></xsl:message>

    <xsl:copy-of select="$renderer-result-xml"/>
    
    <!-- cc - process renderer result and get url - cc -->
    <xsl:variable name="fileSystem" select="concat('censhare:///service/filesystem/', $renderer-result-xml/cmd/renderer/command[@method='save']/@corpus:asset-temp-filesystem, '/')" />
    <xsl:variable name="filePath" select="substring-after($renderer-result-xml/cmd/renderer/command[@method='save']/@corpus:asset-temp-filepath, 'file:')" />
    <xsl:variable name="newUrl" select="concat($fileSystem, $filePath)" />
    
    <!-- cc - check in a new lot card asset - cc -->
    <xsl:variable name="newLotCard">
      <xsl:variable name="to-checkin">
        <!-- cc - name of asset should be <@name>-Lot Card-<current-date> - cc -->
        <asset name="{concat(./@name, '_LotCard.indd')}" domain="{./@domain}" domain2="{./@domain2}" type="layout." application="default">
          <parent_asset_rel parent_asset="{./@id}" key="user."/>
          <asset_element key="actual." idx="0"/>
          <storage_item element_idx="0" key="master" mimetype="application/indesign" corpus:asset-temp-file-url="{$newUrl}" />
        </asset>
      </xsl:variable>
      <xsl:copy-of select="cs:checkin-new($to-checkin)"/>
    </xsl:variable>

    <!-- cc - close vfs - cc -->
    <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="close">
      <cs:param name="id" select="$out"/>
    </cs:command>
    
  </xsl:template>

  <xsl:template match="asset" mode="trick">
    <asset>
      <xsl:copy-of select="./@*"/>
      <xsl:copy-of select="./node()"/>
      <xsl:for-each select="./cs:child-rel()[@key='user.']/.[contains(@type, 'text.')]">
        <xsl:copy-of select="ioi:get-xml-file-contents(.)"/>
      </xsl:for-each>
    </asset>
  </xsl:template>

  <xsl:function name="cs:checkin-new">
    <xsl:param name="asset-xml" />
    <cs:command name="com.censhare.api.assetmanagement.CheckInNew">
      <cs:param name="source" select="$asset-xml" />
    </cs:command>
  </xsl:function>
  
  <xsl:function name="ioi:get-xml-file-contents">
      <xsl:param name="child-text-asset-xml"/>
      <xsl:if test="exists($child-text-asset-xml/storage_item)">
          <cs:command name="com.censhare.api.io.ReadXML" returning="content">
              <cs:param name="source" select="$child-text-asset-xml/storage_item[@key='master'][1]"/>
          </cs:command>
          <xsl:if test="exists($content/content)">
              <asset_feature feature="{$child-text-asset-xml/@name}">
                  <xsl:value-of select="$content/content/text/paragraph/text()"/>
              </asset_feature>
          </xsl:if>
      </xsl:if>
  </xsl:function>

</xsl:stylesheet>