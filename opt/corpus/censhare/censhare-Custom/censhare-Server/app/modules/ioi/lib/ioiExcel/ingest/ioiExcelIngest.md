#IOI EXCEL INGEST

1. [Configuration Documentation](#configuration)
2. [Installation](#installation)
2. [Developer Documentation](#developer)
3. [Feature Requests](#feature-requests)
4. [TODOS](###TODOS)


###Configuration

- censhare Specific Configurations

    - Module was built to mimic standard censhare configuration to the action
itself.  Module title, description, and labels can all be configured through
the properties files within the directory 'cscs/app/modules/ioi/lib/ioiExcel/ingest/'.

    - Currently there is no html5 client interface built in to perform the action through
the web.  This can be done by creating the necessary assets and tying them to the command.
Documentation for this to be done can be found in censhare's partner git documentation.

- Module Specific Configurations

    - All module specific configurations can be found under the label title 'Excel Ingest Config'.  
    
    - This module will accept .xlsx documents and is built using apache's poi library
    
    - The configuration for this module ultimately builds an asset xml "template" that
    is used to clone and insert data specific to the asset being created on a row by row
    basis.
    
        - The Button found under the title called "Edit Mappings" can be clicked to directly
        access the asset xml structure.  This mapping can be used to manually add the spreadsheet specific
        mappings.  It is not recommended to use this unless the format is familar to the admin making
        the configurations.  It is, however, recommended to use this to add asset attribute(name, domain, type, application)
        hardcoded values or hardcoded default feature values. 
        
        - The plus button is used to configure mappings based on a cell by cell basis.
        The cell configurations are empty by default and as many configurations can be added as neccessary.
          There are three checkboxes that appear as well as a feature dropdown.  The checkboxes work as described:
            
            - Required - this checkbox will force the action to list the row as an error if there was any
            issues found while trying to assign the value from the spreadsheet.
            
            - ID for update - this checkbox will force the action to perform asset updates by the censhare attribute 'id_extern'.
            The ID Extern attribute is a field used by censhare as a customizable unique identifier that
            the application itself can use to assign a unique attribute to the asset.  It is recommended to use
            this field to assign an unique identifier to the asset being ingested so that any future reimports can find
            the asset and make the appropriate updates.
            
            - Creation Relation from Column - this checkbox will force a new set of ui configurations to appear.
            When checked the action will take the configured set of data and create a relation to an existing asset.
            Ui works as follows:
                
                - Create Relation Using - This is the selection to choose what type of relation will be used to assign
                the new/updated asset to an existing asset.
                
                - Find Parent By - This is the selection to choose how the existing parent will be found.  The available
                options are id, id_extern, or name.  It is not recommended to use name as the configuration because it can
                provide unexpected relations due to name not being a protected field.  ID can be used if the id is known.
                id_extern is the most appropriate field to use because this is a field that is protected in the database 
                to be unique and is controlled by the import/asset structure creator module.
                
                - Header Map - this field is used to map the desired data to the field and should mirror the cells header
                text exactly.
                
             - If Creation Relation from Column is unchecked the ui functionality is as follows:
                
                - Feature - This is used to select the feature that will be used to map the data into.
                If the feature is actually an attribute the conversion will be made and the data will be mapped into the
                correct attribute of the asset.
                
                - Header Map - this field is used to map the desired data to the field and should mirror the cells header
                text exactly.
                
###Installation

To install this module the ioiExcel module will need to be installed in cscs/app/modules/ioi/lib/.  As well as the ioiExcel
module the helper package will need to be installed as well and should be placed in the same directory.
There are paths in the java that require this location.  If there is ever the request to move this module then the java files
will need to be refactored to match the new path.

Along with the module the appropriate jar's will need to be installed on censhare in censhare's recommended location.
The Apache POI version 3.15 lib is used for this module.  All poi jar files will be needed as well as their dependencies
which are packaged together and can be found on apache's website.

###Developer

The module has a few files that are necessary to provide the action with its funcitonality
IoiExcelIngest.java and IoiHelper which is found in the helper package.  The helper package is used to perform any
queries needed by the action(currently by ID, ID_EXTERN, NAME).  These methods
inputs are as follows:
- getAssetByIdExtern(TransactionManager tm, String idExtern)
- getAssetById(TransactionManager tm, String idExtern)
- getAssetByIdName(TransactionManager tm, String idExtern)

###Feature Requests

Currently there are no feature requests.

###TODOS

The update will need to be modified to include a version increase rather than a blind update.  This functionlity will need to
provide a checkout and then a checkin to achevie this.

Leverage import to create an excel styles import that will import styles configured on the spreadsheet into the xml styles configuration.
