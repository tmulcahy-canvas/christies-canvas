package modules.ioi.lib.helper;

import com.censhare.server.kernel.Command;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.io.FileFactoryService;
import com.censhare.support.io.FileLocator;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.xml.AXml;
import com.censhare.support.xml.CXml;
import com.censhare.support.xml.Node;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Cameron Cramsey
 *         4/20/17.
 */
public class CreateTextFileS44 {

    private Command command;
    private Logger logger;

    private DBTransactionManager tm;
    private FileFactoryService fileFactory;

    private static final String LOG_PREFIX = "   ###   Create Text File S44 - ";

    public CreateTextFileS44(Command command) {
        this.command = command;
        logger = command.getLogger();
        tm = command.getTransactionManager();
        fileFactory = ServiceLocator.getStaticService(FileFactoryService.class);
    }

    public int createLanguageTextFile() {

        AXml cmdXml = (AXml) command.getSlot(Command.XML_COMMAND_SLOT);

        AXml downloadFile = cmdXml.find("download");

        AXml result = generateTextFile(cmdXml);

        logger.info(LOG_PREFIX + " ORIG " + downloadFile.toStringPretty());
        AXml file = new CXml("file");
        file.put("@filesys_name", result.find("file").getAttr("filesys_name"));
        file.put("@relpath", result.find("file").getAttr("relPath"));
        file.put("@target_name", result.find("file").getAttr("target_name"));

        downloadFile.appendChild(file);

        logger.info(LOG_PREFIX + " NEW " + downloadFile.toStringPretty());
        return Command.CMD_COMPLETED;
    }

    private AXml generateTextFile(AXml cmdXml) {

        AXml temp = new CXml("result");
        FileLocator tempDir = null;
        try {
            tempDir = fileFactory.createAutoDeleteTempDirectory(tm, "", "");
        } catch(Exception e) {
            logger.info(LOG_PREFIX + "COULD NOT TEMP DIR");
        }

        String childPath = "censhare-Product/censhare-Server/work/temp/" + tempDir.getFileName();
        File home = new File(System.getProperty("user.home"), childPath);

        try {
            FileOutputStream fileOut = new FileOutputStream(home + File.separator + "languages.txt");
            PrintStream ps = new PrintStream(fileOut);
            List<String> langs = new ArrayList<>();
            for(AXml lang : cmdXml.findAllX("//excelConfig/workbook/sheet[@enabled='1']")) {
                langs.add(lang.getAttr("name"));
            }
            ps.print(String.join("\n", langs));

            ps.close();
            fileOut.close();

        } catch(Exception e) {
            logger.info(LOG_PREFIX + " Could not generate language text file...");
        }

        AXml file = new CXml("file");
        file.setAttr("fileLoc", (childPath + "/" + "languages.txt"));
        file.setAttr("filesys_name", tempDir.getFsName());
        file.setAttr("relPath", (tempDir.getRelURL() + "/" + "languages.txt"));
        file.setAttr("target_name", ("languages.txt"));
        temp.appendChild(file);

        return temp;
    }
}
