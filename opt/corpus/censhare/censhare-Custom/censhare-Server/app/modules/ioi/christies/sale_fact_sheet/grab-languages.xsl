<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus">
	<xsl:output method="xml"  version="1.0" encoding="utf-8" indent="yes"/>
	<xsl:param name="censhare:command-xml"/>

	<xsl:template match="/">
		<xsl:variable name="grab-asset" select="$censhare:command-xml/assets/asset" />
		<!-- rg <xsl:message> ##### R.G Working Asset <xsl:copy-of select="$grab-asset" /></xsl:message> rg -->

		<xsl:variable name="all-child-assets">
			<assets>
				<xsl:apply-templates select="$grab-asset/child_asset_rel" mode="get-product-assets" />
			</assets>
		</xsl:variable>
		<xsl:variable name="language-cache-lookup" >
			<test-langs>
				<xsl:copy-of select="cs:master-data('language_def')" />
			</test-langs>
		</xsl:variable>
		<xsl:variable name="languages-in-assets">
			<langs>
				<xsl:apply-templates select="$all-child-assets/assets/asset" mode="get-langs">
					<xsl:with-param name="languages-in-sys" select="$language-cache-lookup" />
				</xsl:apply-templates>
			</langs>
		</xsl:variable>

		<!-- rg <xsl:message> ##### R.G Please work <xsl:copy-of select="$languages-in-assets" /></xsl:message> rg -->
		<xsl:variable name="flat-langs">

			<xsl:for-each-group select="$languages-in-assets/langs/thelangs" group-by="@key">
				<xsl:copy-of select="." />
			</xsl:for-each-group>
		</xsl:variable>

		<!-- nh <xsl:message> ##### R.G Flat Langs <xsl:copy-of select="$flat-langs" /></xsl:message> nh -->
		<data>
			<xsl:copy-of select="$flat-langs" />
		</data>
	</xsl:template>

	<xsl:template match="child_asset_rel" mode="get-product-assets">
		<xsl:variable name="got-asset">
			<xsl:copy-of select="cs:asset()[@censhare:asset.id=./@child_asset][@censhare:asset.type = 'product.']" />
		</xsl:variable>
		<xsl:copy-of select="$got-asset" />
		<!-- rg <xsl:apply-templates select="$got-asset/child_asset_rel" mode="get-child-assets" /> rg -->
	</xsl:template>

	<xsl:template match="child_asset_rel" mode="get-child-assets">
		<xsl:copy-of select="cs:asset()[@censhare:asset.id=./@child_asset]" />
	</xsl:template>

	<xsl:template match="asset" mode="get-langs">
		<!-- rg <xsl:message> ##### R.G In Get Langs</xsl:message> rg -->
		<xsl:param name="languages-in-sys" />
		<!-- rg <xsl:message> ##### R.G All Langs  <xsl:copy-of select="$languages-in-sys" /></xsl:message> rg -->
		<xsl:for-each-group select="//asset_feature[@language]" group-by="@language">
			<!-- rg <xsl:message> ##### R.G for each </xsl:message> rg -->
			<!-- rg TODO: Fix This For Each Group loop rg -->
			<xsl:variable name="lang-key" select="./@language" />
			<!-- rg <xsl:message> ##### R.G Lang Key / <xsl:value-of select="$lang-key" /></xsl:message> rg -->
			<xsl:variable name="curr-lang-name">
				<xsl:value-of select="$languages-in-sys/test-langs/language_def[@id=$lang-key]/@name" />
			</xsl:variable>

			<thelangs key="{./@language}" name="{$curr-lang-name}"/>
		</xsl:for-each-group>
	</xsl:template>
</xsl:stylesheet>