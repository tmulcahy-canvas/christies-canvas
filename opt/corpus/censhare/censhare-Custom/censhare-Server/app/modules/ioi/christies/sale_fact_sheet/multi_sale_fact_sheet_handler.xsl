<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
  <xsl:param name="censhare:command-xml"/>
  <xsl:template match="/">
    <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
    <xsl:variable name="languages" select="string-join($censhare:command-xml/settings/language/@value, ',')"/>
    <xsl:variable name="ext-condition" select="$censhare:command-xml/settings/@externalCondition"/>
    <xsl:variable name="img-condition" select="$censhare:command-xml/settings/@imageStatus"/>
    <xsl:variable name="excludes">
      <excludes>
        <xsl:for-each select="$censhare:command-xml/settings/excludes/@*">
          <xsl:if test=". = '1' or . = 'true'">
            <!-- rg <xsl:message> ##### R.G MultiSale Fact Sheet Preview <xsl:value-of select="." /></xsl:message> rg -->
            <exclude value="{local-name()}"/>
          </xsl:if>
        </xsl:for-each>
      </excludes>
    </xsl:variable>
    <xsl:variable name="property-assets">
      <assets>
        <xsl:apply-templates select="$censhare:command-xml/assets"/>
      </assets>
    </xsl:variable>
    <xsl:variable name="property-assets-sorted">
      <assets>
        <xsl:for-each-group select="$property-assets/assets/asset" group-by="@id">
          <xsl:sort select="@name"/>
          <xsl:copy-of select="."/>
        </xsl:for-each-group>
      </assets>
    </xsl:variable>
    <xsl:variable name="pdf-name" select="concat($property-assets/assets/asset[1]/@id, '.pdf')"/>
    <xsl:variable name="dest" select="concat($out, $pdf-name)"/>
    <xsl:variable name="fop">
      <cs:command name="com.censhare.api.transformation.FopTransformation">
        <cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=ioi:multi-sale-fact-sheet-xslfo/storage/master/file'"/>
        <cs:param name="source" select="$property-assets-sorted"/>
        <cs:param name="dest" select="$dest"/>
        <cs:param name="xsl-parameters">
          <cs:param name="languages" select="$languages"/>
          <cs:param name="use-external-condition" select="$ext-condition"/>
          <cs:param name="use-image-status" select="$img-condition"/>
          <cs:param name="excludes" select="$excludes"/>
        </cs:param>
      </cs:command>
    </xsl:variable>

    <xsl:variable name="pdf-legal-name" select="concat($property-assets/assets/asset[1]/@id, '-legal.pdf')"/>
    <xsl:variable name="pdf-legal-path" select="concat($out, $pdf-legal-name)"/>
    <xsl:variable name="pdf-legal-relpath" select="concat('file:', substring-after($out, 'censhare:///service/filesystem/temp/'), $pdf-legal-name)" />
    <xsl:variable name="filesysname" select="substring-before(substring-after($out, 'censhare:///service/filesystem/'), '/')" />
    <xsl:variable name="targetname" select="concat($property-assets/assets/asset[1]/@name, '-Fact Sheet.pdf')" />


    <xsl:variable name="renderResult">
      <cs:command name="com.censhare.api.pdf.CombinePDF">
        <cs:param name="dest" select="$pdf-legal-path"/>
        <cs:param name="sources">
          <source href="{$dest}"/>
          <source href="censhare:///service/assets/asset;censhare:resource-key=christies:legal-page/storage/master/file"/>
        </cs:param>
      </cs:command>
    </xsl:variable>

    <download>
      <file filesys_name="{$filesysname}" relpath="{$pdf-legal-relpath}" target_name="Multi sale fact sheet.pdf"/>
    </download>



    <!-- nh <download>
      <file filesys_name="{substring-before(substring-after($out, 'censhare:///service/filesystem/'), '/')}" relpath="{concat('file:', substring-after($out, 'censhare:///service/filesystem/temp/'), $pdf-name)}" target_name="Multi sale fact sheet.pdf"/>
    </download> nh -->
  </xsl:template>
  <xsl:template match="asset[@type='sale.']">
    <xsl:apply-templates select="./cs:child-rel()[@key='*']"/>
  </xsl:template>
  <xsl:template match="asset[@type='product.']">
    <xsl:copy-of select="."/>
  </xsl:template>
  <xsl:template match="asset[@type='group.']">
    <xsl:apply-templates select="./cs:child-rel()[@key='*']"/>
  </xsl:template>
</xsl:stylesheet>