package modules.ioi.lib.ioiExcel.ingest;

import modules.ioi.lib.helper.IoiHelper;

import com.censhare.manager.assetmanager.AssetManagementService;
import com.censhare.model.corpus.impl.*;
import com.censhare.server.kernel.Command;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.cache.CacheService;
import com.censhare.support.model.DataObject;
import com.censhare.support.model.PersistenceManager;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.xml.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Cameron Cramsey
 *         2/2/17.
 */
public class IoiExcelIngest {

    private final Command command;
    private final Logger logger;

    private DBTransactionManager tm;
    private PersistenceManager pm;
    private AssetManagementService am;

    private IoiHelper ioiHelper;

    private String logPrefix = "   ###   IOI EXCEL IMPORT - ";

    private int created = 0, updated = 0, error = 0, transactionalerrors = 0;

    private Map<String, AXml> assetsToCommit;

    private String idExPrefix;

    public IoiExcelIngest(Command command) throws Exception {
        this.command = command;

        logger = command.getLogger();
        tm = command.getTransactionManager();
        pm = tm.getDataObjectTransaction().getPersistenceManager();
        am = ServiceLocator.getStaticService(AssetManagementService.class);

        ioiHelper = new IoiHelper();

        assetsToCommit = new HashMap<>();
    }

    public int ioiExcelIngestPrepare() throws Exception {
        // logger.info(logPrefix + "BEGIN IMPORT");
        // Get Command SLot && get data
        AXml cmdXml = (AXml) command.getSlot(Command.XML_COMMAND_SLOT);

        AXml assetMapping = cmdXml.find("mapping");
        idExPrefix = assetMapping.getAttr("id-external-prefix", "");

        if (assetMapping == null) {
            logger.warning(logPrefix + "Exception: Command does not contain asset configuration.");
            throw new Exception("Command does not contain asset configuration.");
        }
        // Get excel file
        String childPath = "work" + File.separator;
        //  JAVACLIENT
        AXml file = cmdXml.findX("file/file[1]");
        if(file == null) {
            // HTML5 Client
            file = new CXml("file");
            AXml uploadFile = cmdXml.findX("upload/file[1]");
            file.setAttr("corpus:asset-temp-filepath", uploadFile.getAttr("corpus:asset-temp-filepath"));
            childPath = childPath + uploadFile.getAttr("corpus:asset-temp-filesystem");
        } else {
            childPath = childPath + file.getAttr("corpus:asset-temp-filesystem");
        }
        String filename = file.getAttr("corpus:asset-temp-filepath").replace("file:", "");

        File assetTemp = new File(System.getProperty("user.home"), childPath);
        // logger.info(logPrefix + " Grabbing excel file.");
        InputStream input = new FileInputStream(assetTemp + File.separator + filename);
        // logger.info(logPrefix + " Generating excel workbook object");
        // Create Workbook object
        Workbook wb = new XSSFWorkbook(input);

        AXml result = importExcel(assetMapping, wb);

        // Get element to return data from excel generation to.
        AXml commandStepXml = command.getTarget();
        if (commandStepXml == null) {
            // logger.warning(logPrefix + "Exception: Current command step not found.");
            throw new IllegalStateException("Current command step not found");
        }
        String elementKey = commandStepXml.getAttr("key", null);
        AXml dataExchangeElement = cmdXml.find(elementKey);

        // if there is a result append it to the data exchange element
        AXml resultXml = result;
        if (resultXml.getNodeType() == Node.DOCUMENT_NODE && resultXml.getFirstChildElement() != null)
            resultXml = resultXml.getFirstChildElement(); // select root element

        dataExchangeElement.removeAllChildNodes();
        dataExchangeElement.appendChild(resultXml.cloneMutableDeep());

        // logger.info(logPrefix + "END IMPORT");

        return Command.CMD_COMPLETED;

    }

    private AXml importExcel(AXml assetMapping, Workbook wb) throws Exception {

        AXml result = new CXml("result");


        // logger.info(logPrefix + "Importing Sheets.");

        // Process Master Sheet First then loop remaining sheets.
        AXml masterSheetXml = assetMapping.findX("sheet[@masterSheet = 1]");
        if(masterSheetXml != null) {
            try {
                String masterSheetName = masterSheetXml.getAttr("sheetname");

                Sheet masterExcelSheet = wb.getSheet(masterSheetName);

                logger.info(logPrefix + "Working on Master Sheet - " + masterExcelSheet.getSheetName());

                AXml masterSheetResult = processSheet(masterExcelSheet, masterSheetXml.find("asset"));

                if(masterSheetResult.getNodeName().equals("errors"))
                    masterSheetResult.findAll("error").forEach(result::appendChild);
            } catch(Exception e) {
                throw new Exception("Cannot process master sheet.  Please check module configuration.");
            }
        }

        // Loop remaining non-master sheets.
        for(AXml nonMastersheet : assetMapping.findAllX("sheet[@masterSheet = 0]")) {
            AXml sheetResult;
            Sheet excelSheet = wb.getSheet(nonMastersheet.getAttr("sheetname"));
            if(excelSheet != null) {
                logger.info(logPrefix + " Working on Sheet - " + excelSheet.getSheetName());
                sheetResult = processSheet(excelSheet, nonMastersheet.find("asset"));
                // logger.info(logPrefix + "SHEET RESULT - " + sheetResult.toStringPretty());
                if(sheetResult.find("errors") != null) {
                    for(AXml error : sheetResult.findAll("errors.error")) {
                        result.appendChild(error.cloneMutableDeep());
                    }
                }
            }
        }
        logger.info(logPrefix + " Submitting " + assetsToCommit.size() + " assets to censhare.");
        // int counter = 1;
        assetsToCommit.forEach((String idExtern, AXml assetXml) -> {
            // for(Map.Entry<String, AXml> entry : assetsToCommit.entrySet()) {
            //     AXml assetXml = entry.getValue();
            // tm.begin();
            try {
                transactionHandler(assetXml);
            } catch(Exception e) {
                logger.warning(logPrefix + "Error submitting asset with id_extern - " + assetXml.getAttr("id_extern") + ".  " + e);
                transactionalerrors++;
                AXml error = new CXml("error");
                error.appendAttribute("msg",  "Transactional Error - " + e);
            }
            // if(counter % 100 == 0) {
            //     tm.commit();
            //     pm.reset();
            // } else {
            //     tm.end();
            // }
            // counter++;
        });

        result.appendAttribute("created", created);
        result.appendAttribute("updated", updated);
        result.appendAttribute("errors", error + transactionalerrors);
        result.appendAttribute("transactionalerrors", transactionalerrors);
        return result;
    }

    /*
    *
    *      Process Sheet passed in.
    *
    * */
    private AXml processSheet(Sheet sheet, AXml assetMapping) throws Exception {

        AXml result = new CXml("data");

        Iterator<Row> rows = sheet.rowIterator();

        ArrayList<String> headerNames = getHeaderNames(assetMapping);
        ArrayList<String> mappingOrder = processHeaderRow(sheet.getRow(0));
        if(!validateSheet(headerNames, mappingOrder)) {
            logger.warning(logPrefix + "Exception - Invalid Sheet Ingested.  Column headers do not match configuration!");
            throw new Exception("Invalid Sheet Ingested.  Column headers do not match configuration!");
        }

        Map<String, Integer> correctedHeaderMapping = getCorrectedHeaderMapping(headerNames, mappingOrder);

        while(rows.hasNext()) {
            Row row = rows.next();
            if(row.getRowNum() > 0 && !checkIfRowIsEmpty(row)) {
                logger.info(logPrefix + "Processing Row - " + (row.getRowNum() + 1));
                AXml assetXml = processRow(row, correctedHeaderMapping, headerNames, assetMapping);
                // logger.info("AssetXML " + assetXml);
                if (assetXml.getNodeName().equals("errors"))
                    result.appendChild(assetXml.cloneMutableDeep());
            }
        }

        return result;

    }

    private boolean checkIfRowIsEmpty(Row row) {
        if(row == null) {
            return true;
        }
        if(row.getLastCellNum() <= 0) {
            return true;
        }
        if(row.getPhysicalNumberOfCells() <= 0) {
            return true;
        }
        if((row.getRowNum() + 1) > 200000) {
            return true;
        }
        Cell cell = row.getCell(row.getFirstCellNum());

        return cell == null || (cell.getCellType() == Cell.CELL_TYPE_BLANK || cell.getCellType() == Cell.CELL_TYPE_FORMULA);
    }

    /*
    *
    *       Loops cells and replaces header name with value held in cell
    *
    * */

    private AXml processRow(Row row, Map<String, Integer> mappingOrder, ArrayList<String> headerNames, AXml assetMapping) {
        // First get id_extern to see if asset already has been put into queue
        AXml updateFeatNode = assetMapping.findX("asset_feature[@unique-id = 1]");
        String headerNameTemp = updateFeatNode.getAttr("value_string");
        String idEx = getIDExFromRow(headerNameTemp, updateFeatNode, row, mappingOrder);

        // clone asset xml from configured
        AXml assetXml = new CXml("asset");
        for(Node attr : assetMapping.getAttributes()) {
            assetXml.appendAttribute(attr.getNodeName(), attr.getNodeValue());
        }
        // prepare error node to log any errors that could come up looping cells.
        AXml errorXml = new CXml("errors");
        errorXml.appendAttribute("row", (row.getRowNum() + 1));

        boolean reqFieldMissing = false;
        boolean update = false;
        boolean parentMissing = false;
        boolean headerMappingError = false;

        //Loop asset_feature nodes and mapping to cells from there
        for(AXml featureNode : assetMapping.findAll("asset_feature")) {
            // logger.info(logPrefix + "Processing feature - " + featureNode.toStringPretty());
            AXml result = processFeature(featureNode, row, mappingOrder, assetXml, idEx);
            reqFieldMissing = result.getAttrBoolean("reqFieldMissing");
            update = result.getAttrBoolean("update");
            parentMissing = result.getAttrBoolean("parentMissing");
            headerMappingError = result.getAttrBoolean("headerMappingError");
            assetXml = result.find("asset");
            errorXml = result.find("errors");
        }

        if(headerMappingError) {
            error++;
            return errorXml;
        } else if(reqFieldMissing) {
            // logger.warning(logPrefix + "REQUIRED FIELD MISSING IN ROW " + row.getRowNum());
            error++;
            return errorXml;
        } else if(parentMissing) {
            // logger.warning(logPrefix + "PARENT NOT FOUND FOR ROW " + row.getRowNum());
            error++;
            return errorXml;
        } else {
            if(assetsToCommit.containsKey(idEx)) {
                AXml asset4AdditionalProcessing = assetsToCommit.get(idEx);
                for(AXml node : assetXml.findAllX("./node()")) {
                    if(node.getLocalName().equals("parent_asset_rel")) {
                        String parentId = node.getAttr("parent_asset");
                        if(asset4AdditionalProcessing.findX("parent_asset_rel[@parent_asset='" + parentId + "']") == null) {
                            AXml newParentNode = node.cloneMutableDeep();
                            asset4AdditionalProcessing.appendChild(newParentNode);
                        } else {
                            logger.info(logPrefix + " SKIPPING PARENT...Already exists!");
                        }
                    } else if(node.getLocalName().equals("asset_feature")) {
                        String feature = node.getAttr("feature");
                        Feature f = ServiceLocator.getStaticService(CacheService.class).getEntry(Feature.newPk(feature));

                        String featureValueType = ioiHelper.getAttrByValueType(feature);

                        // logger.info(logPrefix + " ADDING NODE - " + node.toStringPretty());
                        if(f.isMultivalue(false)){
                            AXml newNode = node.cloneMutableDeep();
                            if(newNode.getAttr("language") != null) {
                                String lang = newNode.getAttr("language");
                                Iterator<AXml> langNode = asset4AdditionalProcessing.findAllX("asset_feature[@feature='" + feature + "' and @language='" + lang + "']").iterator();
                                if(langNode.hasNext()) {
                                    if(!checkChildNodesForEquality(langNode, newNode)) {
                                        asset4AdditionalProcessing.appendChild(newNode);
                                    }
                                } else {
                                    asset4AdditionalProcessing.appendChild(newNode);
                                }
                            } else if(newNode.getAttr("value_unit") != null) {
                                String valUnit = newNode.getAttr("value_unit");
                                Iterator<AXml> unitNode = asset4AdditionalProcessing.findAllX("asset_feature[@feature='" + feature + "' and @value_unit='" + valUnit + "']").iterator();

                                if(unitNode.hasNext()) {
                                    if(!checkChildNodesForEquality(unitNode, newNode)) {
                                        asset4AdditionalProcessing.appendChild(newNode);
                                    }
                                } else {
                                    asset4AdditionalProcessing.appendChild(newNode);
                                }
                            } else {
                                String featVal = newNode.getAttr(featureValueType);
                                if(asset4AdditionalProcessing.findX("asset_feature[@feature='" + feature + "' and @" + featureValueType + "='" + featVal + "']") == null) {
                                    asset4AdditionalProcessing.appendChild(newNode);
                                }
                            }
                        } else {
                            if(asset4AdditionalProcessing.findAllX("asset_feature[@feature='" + feature + "']") == null) {
                                AXml newNode = node.cloneMutableDeep();
                                asset4AdditionalProcessing.appendChild(newNode);
                            }
                        }
                    } else {
                        AXml newNode = node.cloneMutableDeep();
                        asset4AdditionalProcessing.appendChild(newNode);
                    }
                }
                assetsToCommit.put(idEx, asset4AdditionalProcessing);
                return assetXml;

            } else {
                // logger.info(logPrefix + "MASTER SHEET XML - " + assetXml);
                assetsToCommit.put(idEx, assetXml);
                return assetXml;
            }
        }

    }

    private AXml processFeature(AXml featureNode, Row row, Map<String, Integer> mappingOrder, AXml assetXml, String idEx) {
        AXml result = new CXml("result");
        AXml errorXml = new CXml("errors");

        boolean reqFieldMissing = false, update = false, parentMissing = false, headerMappingError = false;

        String headerName = null;
        String attrName = null;
        for(Node attribute : featureNode.getAttributes()) {
            if(attribute.getNodeName().contains("value_") && !attribute.getNodeName().equals("value_unit")) {
                headerName = attribute.getNodeValue();
                attrName = attribute.getNodeName();
                break;
            }
        }

        if(headerName != null) {

            String val = null;

            Cell currentCell;
            if(headerName.startsWith("{") || headerName.startsWith("<")) {
                currentCell = row.getCell(0);
            } else {
                int cellIndex = mappingOrder.get(headerName);
                currentCell = row.getCell(cellIndex);
            }

            CellDataObject cdo = new CellDataObject(currentCell, featureNode);

            if(cdo.errorMsg != null) {
                reqFieldMissing = cdo.reqFieldMissing;
                errorXml.create("error").appendAttribute("msg", cdo.errorMsg);
            } else {
                if(headerName.startsWith("{")) {
                    String xPath = headerName.replaceFirst("\\{", "").substring(0, headerName.lastIndexOf("}") - 1);
                    Pattern regex = Pattern.compile("\\{[\\w ]*}");
                    Matcher m = regex.matcher(xPath);
                    while(m.find()) {
                        String group = m.group();
                        Cell cellTemp = row.getCell(mappingOrder.get(group.replace("{", "").replace("}", "")));
                        CellDataObject cdoTemp = new CellDataObject(cellTemp, featureNode);
                        xPath = xPath.replace(group, cdoTemp.val);
                    }
                    val = xPath;
                } else if(headerName.startsWith("<")) {
                    // logger.info(logPrefix + " < VAL - " + headerName);
                    val = headerName.replace("<", "").replace(">", "");
                } else {
                    if(cdo.val != null) {
                        if(!cdo.val.equals("")) {
                            val = cdo.val;
                        }
                    }
                }
            }
            if((!reqFieldMissing) && (val != null && val.trim().length() > 0)) {

                // Create Rel?
                if(featureNode.getAttrBoolean("createRel")) {
                    String parentFindBy = featureNode.getAttr("find-by");
                    String parentIdentifier = featureNode.getAttr("value_string");
                    String relType = featureNode.getAttr("rel-type");

                    try {
                        Long parentId = null;
                        switch(parentFindBy) {
                            case "id_extern":
                                String idEx2Find = idExPrefix + "." + val.replaceAll("[\\W]", "").toLowerCase();
                                parentId = ioiHelper.getAssetByIdExtern(tm, idEx2Find).getId();
                                break;
                            case "id":
                                parentId = ioiHelper.getAssetById(tm, val).getId();
                                break;
                            case "name":
                                parentId = ioiHelper.getAssetByName(tm, val).getId();
                                break;
                        }
                        if(parentId == null) {
                            parentMissing = true;
                            errorXml.create("error").appendAttribute("msg", "Could not find parent asset " + parentIdentifier + ".");
                        } else {
                            AXml testForDupeRel = assetXml.findX("parent_asset_rel[@parent_asset = '" + parentId + "']");
                            if(testForDupeRel == null) {
                                AXml parentRel = assetXml.appendNewChild("parent_asset_rel");
                                parentRel.appendAttribute("parent_asset", parentId);
                                parentRel.appendAttribute("key", relType);
                            }
                        }
                    } catch(Exception e) {
                        logger.warning(logPrefix + " Error querying for parent asset. Parent Id - " + parentIdentifier + "   On Sheet " + row.getSheet().getSheetName() + "     " + e);
                        parentMissing = true;
                        errorXml.create("error").appendAttribute("msg", " Error querying for parent asset. " + parentIdentifier);
                    }
                } else {
                    // Create Feature
                    if(!update)
                        update = featureNode.getAttrBoolean("unique-id");

                    boolean mapToIDExtern = featureNode.getAttrBoolean("unique-id");
                    String feature = featureNode.getAttr("feature");

                    Feature f = ServiceLocator.getStaticService(CacheService.class).getEntry(Feature.newPk(feature));

                    if(mapToIDExtern) {
                        assetXml.appendAttribute("id_extern", idEx);
                    } else {
                        if(f.getAttributeMapping() != null) {
                            String attributeName = f.getAttributeMapping().replace("@", "");
                            assetXml.appendAttribute(attributeName, val);
                        } else {
                            AXml assetFeature = processDataIntoFeature(val, attrName, featureNode, row, mappingOrder);
                            assetXml.appendChild(assetFeature.cloneMutableDeep());
                        }
                    }
                }
            }
        } else {
            // ERROR FOR NOT FINDING HEADER NAME!!!
            logger.warning(logPrefix + " - Couldn't resolve mapping for header - " + headerName + ".  On Sheet " + row.getSheet().getSheetName() + ".  Feature Node - " + featureNode.toString());
            headerMappingError = true;
            errorXml.create("error").appendAttribute("msg", "Couldn't resolve mapping for header - " + headerName + ".  On Sheet " + row.getSheet().getSheetName() + ".  Feature Node - " + featureNode.toString());
        }

        result.setAttrBoolean01("reqFieldMissing", reqFieldMissing);
        result.setAttrBoolean01("update", update);
        result.setAttrBoolean01("parentMissing", parentMissing);
        result.setAttrBoolean01("headerMappingError", headerMappingError);

        result.appendChild(assetXml.cloneMutableDeep());
        result.appendChild(errorXml.cloneMutableDeep());

        return result;
    }

    private AXml processDataIntoFeature(String val, String attrName, AXml featureNode, Row row, Map<String, Integer> mappingOrder) {
        AXml assetFeature = new CXml("asset_feature");
        // logger.info(logPrefix + " VALUE FOR FEATURE - " + val);
        assetFeature.appendAttribute(attrName, val);
        assetFeature.appendAttribute("feature", featureNode.getAttr("feature"));

        if(featureNode.getAttr("language") != null)
            assetFeature.appendAttribute("language", featureNode.getAttr("language"));

        if(featureNode.getAttr("currency") != null)
            assetFeature.appendAttribute("currency", featureNode.getAttr("currency"));

        if(featureNode.getAttr("value_unit") != null)
            assetFeature.appendAttribute("value_unit", featureNode.getAttr("value_unit"));

        if(featureNode.findAll("asset_feature") != null){
            for(AXml childFeature : featureNode.findAll("asset_feature")) {
                AXml formedChildFeat = processDataIntoChildFeature(childFeature, row, mappingOrder);
                assetFeature.appendChild(formedChildFeat.cloneMutableDeep());
            }
        }

        return assetFeature;
    }

    private AXml processDataIntoChildFeature(AXml childFeature, Row row,  Map<String, Integer> mappingOrder) {

        AXml childAssetFeature = new CXml("asset_feature");

        String val;
        String featureKey = childFeature.getAttr("feature");
        String headerName = null;
        String attrName = null;

        for(Node attribute : childFeature.getAttributes()) {
            if(attribute.getNodeName().contains("value_") && !attribute.getNodeName().equals("value_unit")) {
                headerName = attribute.getNodeValue();
                attrName = attribute.getNodeName();
                break;
            }
        }

        if(headerName.startsWith("{")) {
            String xPath = headerName.replaceFirst("\\{", "").substring(0, headerName.lastIndexOf("}") - 1);
            Pattern regex = Pattern.compile("\\{[\\w ]*}");
            Matcher m = regex.matcher(xPath);
            while(m.find()) {
                String group = m.group();
                Cell cellTemp = row.getCell(mappingOrder.get(group.replace("{", "").replace("}", "")));
                CellDataObject cdoTemp = new CellDataObject(cellTemp, childFeature);
                xPath = xPath.replace(group, cdoTemp.val);
            }
            val = xPath;
        } else if(headerName.startsWith("<")) {
            val = headerName.replace("<", "").replace(">", "");
        } else {
            int cellIndex = mappingOrder.get(headerName);
            Cell currentCell = row.getCell(cellIndex);
            CellDataObject cdo = new CellDataObject(currentCell, childFeature);

            val = cdo.val;
        }

        childAssetFeature.appendAttribute("feature", featureKey);
        childAssetFeature.appendAttribute(attrName, val);

        if(childAssetFeature.findAll("asset_feature") != null){
            for(AXml childSubFeature : childAssetFeature.findAll("asset_feature")) {
                AXml formedChildFeat = processDataIntoChildFeature(childSubFeature, row, mappingOrder);
                childAssetFeature.appendChild(formedChildFeat.cloneMutableDeep());
            }
        }

        return childAssetFeature;
    }

    private String getIDExFromRow(String headerNameTemp, AXml updateFeatNode, Row row, Map<String, Integer> mappingOrder) {
        String idEx;
        if(headerNameTemp.startsWith("{")) {
            String xPath = headerNameTemp.replaceFirst("\\{", "").substring(0, headerNameTemp.lastIndexOf("}") - 1);
            Pattern regex = Pattern.compile("\\{[\\w ]*}");
            Matcher m = regex.matcher(xPath);
            while(m.find()) {
                String group = m.group();
                int cellIndex = mappingOrder.get(group.replace("{", "").replace("}", ""));
                Cell cellTemp = row.getCell(cellIndex);
                CellDataObject cdoTemp = new CellDataObject(cellTemp, null);
                xPath = xPath.replace(group, cdoTemp.val.replaceAll("[\\W]", "").toLowerCase());
            }
            idEx = idExPrefix + xPath;
        } else {
            int header4ColIndex = mappingOrder.get(updateFeatNode.getAttr("value_string"));
            Cell updateCell = row.getCell(header4ColIndex);
            CellDataObject cdo4Update = new CellDataObject(updateCell, updateFeatNode);
            idEx = idExPrefix + cdo4Update.val;

        }
        return idEx;
    }

    /*
    *
    *   Get Header names to perform error checking on the configuration vs. excel header row.
    *
    * */
    private ArrayList<String> getHeaderNames(AXml config) {

        ArrayList<String> tempArr = new ArrayList<>();
        XmlIterator<AXml> headerCols = config.findAll("asset_feature");
        while(headerCols.hasNext()) {
            AXml col = headerCols.next();
            tempArr = findHeaders(tempArr, col);
        }
        return tempArr;
    }

    /*
    *
    *   Method for recursing asset_feature nodes to find header names
    *
    * */
    private ArrayList<String> findHeaders(ArrayList<String> tempArr, AXml col) {
        Iterable<Node> headerName = col.getAttributes();
        Iterator<Node> headerNameIter = headerName.iterator();
        List<String> colName = new ArrayList<>();
        while(headerNameIter.hasNext()) {
            Node thisNode = headerNameIter.next();
            if(thisNode.getNodeName().contains("value_") && !thisNode.getNodeName().equals("value_unit")) {
                String nodeVal = thisNode.getNodeValue();

                if(nodeVal.startsWith("{")) {
                    String xPath = nodeVal.replaceFirst("\\{", "").substring(0, nodeVal.lastIndexOf("}") - 1);
                    Pattern regex = Pattern.compile("\\{[\\w ]*}");
                    Matcher m = regex.matcher(xPath);
                    while(m.find()) {
                        String group = m.group();
                        String headName = group.replace("{", "").replace("}", "");
                        colName.add(headName);
                    }

                } else if(nodeVal.startsWith("<")) {
                    break;
                } else {
                    colName.add(nodeVal);
                }
                break;
            }
        }

        tempArr.addAll(colName);

        XmlIterator<AXml> headerCols = col.findAll("asset_feature");
        while(headerCols.hasNext()) {
            AXml thisCol = headerCols.next();
            findHeaders(tempArr, thisCol);
        }

        return tempArr;
    }
    /*
    *
    *    Mapping should always be based on order of header row.  We should test excel sheet for failure on the configured
    *    header2feature mapping and return information on information that is not found when analysing the row.
    *
    * */

    private ArrayList<String> processHeaderRow(Row row) {

        ArrayList<String> temp = new ArrayList<>();

        Iterator<Cell> cells = row.cellIterator();
        while(cells.hasNext()) {
            Cell thisCell = cells.next();
            CellDataObject cdo = new CellDataObject(thisCell, null);
            String headerName = cdo.val;
            temp.add(headerName);
        }

        return temp;
    }

    /*
    *
    *   Validate sheet has all columns configured
    *
    * */
    private boolean validateSheet(ArrayList<String> configHeaders, ArrayList<String> sheetHeaders) {
        boolean validate = true;

        for(String header : configHeaders) {
            if (!sheetHeaders.contains(header)) {
                if(!header.startsWith("{")){
                    validate = false;
                    break;
                }
            }
        }

        return validate;
    }

    /*
    *
    *       Correct header mapping by creating a map of index to string value
    *
    * */
    private Map<String, Integer> getCorrectedHeaderMapping(ArrayList<String> headerNames, ArrayList<String> mappingOrder) {

        Map<String, Integer> headerMap = new HashMap<>();

        int mappingOrderSize = mappingOrder.size();
        for(int i = 0; i < mappingOrderSize; i++) {
            String headerNameFromSheet = mappingOrder.get(i);

            if(headerNames.indexOf(headerNameFromSheet) != -1) {
                headerMap.put(headerNameFromSheet, i);
            }
        }

        return headerMap;
    }

    /*
    *
    *
    *
    * */
    private void transactionHandler(AXml assetXml) throws Exception  {
        Asset asset;
        String idExtern = assetXml.getAttr("id_extern");
        tm.begin();
        if(idExtern != null) {
            asset = ioiHelper.getAssetByIdExtern(tm, idExtern);
            // update asset
            if(asset != null) {
                updateAsset(assetXml, asset);
                // check in new asset clone corresponding to given xml
            } else {
                createNewAsset(assetXml);
            }
        } else {
            createNewAsset(assetXml);
        }

        tm.end();

    }

    private void updateAsset(AXml assetXml, Asset asset) throws Exception {

        asset = am.checkOut(tm, asset.getPrimaryKey()); // check out asset

        tm.end();
        tm.begin();

        for (Node attr : assetXml.getAttributes()) { // copy attribute nodes of assetXml
            DXml.setAttribute(asset.xml(), attr.getNodeName(), attr.getNodeValue());
        }
        for (AXml childXml : assetXml.findAll()) { // copy element nodes of assetXml
            if(childXml.getLocalName().equals("asset_feature")) {

                String featureKeyFromXml = childXml.getAttr("feature");
                String featureValueType = ioiHelper.getAttrByValueType(featureKeyFromXml);
                String featureValueFromXml = childXml.getAttr(featureValueType);
                String featureLanguageFromXml = childXml.getAttr("language", null);
                String featureValueUnitFromXml = childXml.getAttr("value_unit", null);

                boolean isMulti = ServiceLocator.getStaticService(CacheService.class).getEntry(Feature.newPk(featureKeyFromXml)).isMultivalue();

                boolean featureUpdated = false;
                for(AssetFeature af : asset.struct().getFeatures(featureKeyFromXml)) {
                    String featureValue;
                    switch(featureValueType) {
                        case "value_string":
                            featureValue = af.getValueString();
                            break;
                        case "value_key":
                            featureValue = af.getValueKey();
                            break;
                        case "value_long":
                            featureValue = Long.toString(af.getValueLong());
                            break;
                        case "value_double":
                            featureValue = Double.toString(af.getValueDouble());
                            break;
                        case "value_timestamp":
                            featureValue = af.getValueTimestamp().toString();
                            break;
                        case "value_asset_id":
                            featureValue = Long.toString(af.getValueAssetId());
                            break;
                        case "value_asset_key_ref":
                            featureValue = af.getValueAssetKeyRef();
                            break;
                        default:
                            featureValue = null;
                    }

                    boolean updateFeature = false;

                    if(isMulti) {
                        if(featureLanguageFromXml != null) {
                            if(af.getLanguage().equals(featureLanguageFromXml)) {
                                if(childXml.findAll() == null || checkChildNodesForEquality(af, childXml)) {
                                    updateFeature = true;
                                }
                            }
                        } else if(featureValueUnitFromXml != null) {
                            if(af.getValueUnit().equals(featureValueUnitFromXml)) {
                                if(childXml.findAll() == null || checkChildNodesForEquality(af, childXml)) {
                                    updateFeature = true;
                                }
                            }
                        } else {
                            if(featureValue.equals(featureValueFromXml)) {
                                if(childXml.findAll() == null || checkChildNodesForEquality(af, childXml)) {
                                    updateFeature = true;
                                }
                            }
                        }
                    } else {
                        updateFeature = true;
                    }

                    if(updateFeature) {
                        switch(featureValueType) {
                            case "value_string":
                                af.setValueString(featureValueFromXml);
                                break;
                            case "value_key":
                                af.setValueKey(featureValueFromXml);
                                break;
                            case "value_long":
                                af.setValueLong(Long.parseLong(featureValueFromXml));
                                break;
                            case "value_double":
                                af.setValueDouble(Double.parseDouble(featureValueFromXml));
                                break;
                            case "value_timestamp":
                                af.setValueTimestamp(new Date(featureValueFromXml));
                                break;
                            case "value_asset_id":
                                af.setValueAssetId(Long.parseLong(featureValueFromXml));
                                break;
                            case "value_asset_key_ref":
                                af.setValueAssetKeyRef(featureValueFromXml);
                                break;
                        }
                        featureUpdated = true;
                        break;
                    }

                }
                if(!featureUpdated) {
                    asset.addChild(pm.mapAndMakePersistentRecursive(childXml.cloneMutableDeep()));
                }
            } else if(childXml.getLocalName().equals("parent_asset_rel")) {
                Long parRelIdFromXml = childXml.getAttrLong("parent_asset");
                String parRelTypeFromXml = childXml.getAttr("key");
                boolean parentFound = false;
                // logger.info(logPrefix + " PARENT ASSET ID - " + parRelIdFromXml);
                // logger.info(logPrefix + " PARENT REL KEY - " + parRelTypeFromXml);
                for(ParentAssetRel rel : asset.struct().getParentAssetRelIter()) {
                    Long parentRelIdFromRel = rel.getParentAsset();
                    String parentRelTypeFromRel = rel.getKey();
                    if(parentRelIdFromRel.equals(parRelIdFromXml) && parentRelTypeFromRel.equals(parRelTypeFromXml)){
                        parentFound = true;
                        break;
                    }
                }
                if(!parentFound)
                    asset.addChild(pm.mapAndMakePersistentRecursive(childXml.cloneMutableDeep()));
            } else {
                asset.addChild(pm.mapAndMakePersistentRecursive(childXml.cloneMutableDeep()));
            }
        }
        asset = am.checkIn(tm, asset);

        updated++;
    }

    private boolean checkChildNodesForEquality(AssetFeature af, AXml updateXml) {

        boolean equal = true;

        for(AXml child : updateXml.findAll()) {
            String featureFromXml = child.getAttr("feature");
            String featureValTypeFromXml = ioiHelper.getAttrByValueType(featureFromXml);
            String featureValFromXml = child.getAttr(featureValTypeFromXml);
            AssetFeature childAF = (AssetFeature) af.getFeature(featureFromXml);
            String featureValue;
            switch(featureValTypeFromXml) {
                case "value_string":
                    featureValue = childAF.getValueString();
                    break;
                case "value_key":
                    featureValue = childAF.getValueKey();
                    break;
                case "value_long":
                    featureValue = Long.toString(childAF.getValueLong());
                    break;
                case "value_double":
                    featureValue = Double.toString(childAF.getValueDouble());
                    break;
                case "value_timestamp":
                    featureValue = childAF.getValueTimestamp().toString();
                    break;
                case "value_asset_id":
                    featureValue = Long.toString(childAF.getValueAssetId());
                    break;
                case "value_asset_key_ref":
                    featureValue = childAF.getValueAssetKeyRef();
                    break;
                default:
                    featureValue = null;
            }


            // logger.info(logPrefix + " CHECKING FOR EQUALITY - XML -" + featureValFromXml + " - ASSET - " + featureValue);
            // logger.info(logPrefix + " CHECKING FOR EQUALITY - " + childAF + " -  - " + updateXml);
            if(!featureValFromXml.equals(featureValue)) {
                equal = false;
                break;
            }
        }

        return equal;
    }

    private boolean checkChildNodesForEquality(Iterator<AXml> afs, AXml updateXml) {

        boolean equal = false;
        while (afs.hasNext()) {
            AXml af = afs.next();
            logger.info(logPrefix + af.toString());
            if(updateXml.findAll().hasNext()) {
                for(AXml child : updateXml.findAll()) {
                    String featureFromXml = child.getAttr("feature");
                    String featureValTypeFromXml = ioiHelper.getAttrByValueType(featureFromXml);
                    String featureValFromXml = child.getAttr(featureValTypeFromXml);
                    AXml childAF = af.findX("asset_feature[@feature='" + featureFromXml + "']");
                    String featureValue = childAF.getAttr(featureValTypeFromXml);

                    if(featureValFromXml.equals(featureValue)) {
                        equal = true;
                    }
                    logger.info(logPrefix + "ARE THEY EQUAL? - " + equal);
                }
            } else {
                equal = true;
            }
        }
        return equal;
    }

    private void createNewAsset(AXml assetXml) {
        Asset asset;

        asset = (Asset) pm.mapRecursive(assetXml.cloneMutableDeep());
        asset = asset.cloneAndMakeTransient(true);
        pm.makePersistentRecursive(asset);

        am.checkInNew(tm, asset);

        created++;
    }

    // Class to provide error handling functionality to extracting data from cell poi object
    private class CellDataObject {

        public Cell cell;
        public String val = null;
        public String errorMsg;
        private AXml featNode;
        private boolean reqFieldMissing;

        private CellDataObject(Cell cell, AXml featNode) {
            this.cell = cell;
            this.val = null;
            this.errorMsg = null;
            this.featNode = featNode;
            this.reqFieldMissing = false;

            if(cell != null) {
                switch(cell.getCellType()) {
                    case Cell.CELL_TYPE_NUMERIC:
                        if(DateUtil.isCellDateFormatted(cell)) {
                            val = cell.getDateCellValue().toString();
                        } else {
                            val = Double.toString(cell.getNumericCellValue());
                        }
                        break;
                    case Cell.CELL_TYPE_STRING:
                        val = cell.getStringCellValue();
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        val = cell.getCellFormula();
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        // Empty cell found...Is it reqd?
                        if(featNode != null) {
                            if(featNode.getAttrBoolean("required")) {
                                reqFieldMissing = true;
                                errorMsg = "Missing value for feature ( " + featNode.getAttr("feature") + " )";
                            }
                        }
                    case Cell.CELL_TYPE_BOOLEAN:
                        if(!cell.getStringCellValue().equals("")) {
                            val = Boolean.toString(cell.getBooleanCellValue());
                        } else {
                            val = null;
                        }
                        break;
                    case Cell.CELL_TYPE_ERROR:
                        // error in cell
                        if(featNode.getAttrBoolean("required")) {
                            reqFieldMissing = true;
                        }
                        errorMsg = "ERROR found in cell - " + (cell.getColumnIndex() + 1) + " - in row - " + (cell.getRowIndex() + 1) + ".";
                }
            }
            logger.info(logPrefix + "Processing Cell - " + (cell.getColumnIndex() + 1));
            logger.info(logPrefix + "[ val: " + val + "; feat: " + featNode + "; CELL - " + (cell.getColumnIndex() + 1) + " ROW - " + (cell.getRowIndex() + 1 ) + " ]");
        }
    }

}