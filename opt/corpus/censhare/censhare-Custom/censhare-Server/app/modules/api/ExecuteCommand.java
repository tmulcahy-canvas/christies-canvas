/**
*  Copyright (c) by censhare AG
*  SVN $Id$
*/

package modules.api;

import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.censhare.server.config.ConfigResolver;
import com.censhare.server.kernel.Command;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.server.manager.CommonServiceFactory;
import com.censhare.server.rmi.CommandData;

import com.censhare.support.transaction.CsContext;
import com.censhare.support.util.MiscUtilities;
import com.censhare.support.xml.AXml;




public final class ExecuteCommand {

   @Resource
   private DBTransactionManager tm; // injected TransactionManager


   @PostConstruct
   private void postConstruct() {
       if (tm == null)
           throw new IllegalStateException("TransactionManager not injected");
   }




   public Object execute(CsContext context, Map<String, Object> paramMap) throws Exception {
       try {
           AXml commandXml = (AXml) paramMap.get("command");
           return execute(context, commandXml);
       }
       catch (Exception e) {
           Io.logMethodError(context, e, "executeCommand", paramMap);
           throw e; 
       }
   }

    /**
    * Execute command
    *
    * <pre class="prettyprint" style="font-size:larger">
    * {@code
    * <cs:command name="com.censhare.api.ExecuteCommand.executeCommand">
    *   <cs:param name="command" select="$command-xml"/>
    * </cs:command>
    * }
    * </pre>
    * </p>
    *
    * <b>Examples:</b>
    * <pre class="prettyprint" style="font-size:larger">
    * {@code
    *  <xsl:variable name="command-xml">￼
    *    <cs:command name="com.censhare.api.io.ReadXML">
    *      <cs:param name="source" select="concat($config, 'modules/api/update-master-data-api.xml')" />
    *    </cs:command>
    *  </xsl:variable>
    *  <xsl:variable name="prep-command-xml">
    *    <cmd>
    *      <xsl:copy-of select="$command-xml/cmd/@*" />
    *      <xsl:copy-of select="$command-xml/cmd/node()" />
    *      <data>
    *        <event corpus:dto_flags="tn" description="tbtest1" description_de="tbtest1" enabled="1" event_key="censhare.CustomAssetEvent.tbtest1" eventsource="CustomAssetEvent" name="tbtest1" param3="0" />
    *      </data>
    *    </cmd>
    *  </xsl:variable>
    *  <cs:command name="com.censhare.api.ExecuteCommand.executeCommand">
    *    <cs:param name="command" select="$prep-command-xml" />
    *  </cs:command>
    * }
    * </pre>
    *
    * @param source  The command XML to execute.
    *
    */

   public AXml execute(CsContext context, AXml commandXml) throws Exception {

       AXml xmlResult;
       Logger logger = Io.logger(context);

       //  set correct locale
       AXml xmlInfoXml = commandXml.find(ConfigResolver.ELEM_XML_INFO);
       String localeString = xmlInfoXml.get("@locale", "__ALL");        
       Locale locale = MiscUtilities.stringToLocale(localeString);
       
       DBTransactionManager tm2 = CommonServiceFactory.createDBTransactionManager(tm.getTransactionContext(), tm.getDescription());
       tm2.getTransactionContext().setLocale(locale);

       // create and execute new command from xml
       CommandData cmdData = new CommandData();
       cmdData.setName(commandXml.get("cmd-info@name", "command-without-a-name"));
       cmdData.setTransactionContext(tm2.getTransactionContext());
       cmdData.setSlot(Command.XML_COMMAND_SLOT, commandXml);
       Command cmd = new Command(tm2, cmdData);

       // execute command            
       int result = cmd.executeSync(60 * 1000 /* 1 min */, true);
       if (result == Command.CMD_ERROR) {
           logger.log(Level.INFO, "Error in command:", cmd.getException());
           tm2.reset();
       } else {
           logger.info("executeCommand XSLT API call finished with state: "+Command.stateName[result]);
           tm2.commit();    
       }            

       return (AXml) cmd.getSlot(Command.XML_COMMAND_SLOT);

   }
}