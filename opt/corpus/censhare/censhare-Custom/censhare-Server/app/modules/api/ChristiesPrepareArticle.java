package modules.api;

import com.censhare.support.transaction.CsContext;
import com.censhare.support.xml.AXml;
import modules.ioi.lib.helper.IoiStripHtml;

import java.util.Map;
import java.util.logging.Logger;


public class ChristiesPrepareArticle {

    public AXml prepare(CsContext context, Map<String, Object> paramMap) throws Exception {

        Logger logger = Io.logger(context);

        AXml article = (AXml) paramMap.get("article");
        String fs = (String) paramMap.get("fs");
        String assetID = paramMap.get("id").toString();
        boolean replaceLotSymbols = paramMap.get("replaceLotSymbols") != null ? Boolean.parseBoolean((String) paramMap.get("replaceLotSymbols")) : false;
        boolean addLineBreaks = paramMap.get("addLineBreaks") != null ? Boolean.parseBoolean((String) paramMap.get("addLineBreaks")) : false;

        return prepare(article, fs, assetID, replaceLotSymbols, addLineBreaks, logger);
    }

    /**
     * Command to strip HTML tags and update Lot Symbols
     * Optionally you can pass booleans to replaceLotSymbols and addLineBreaks and defaults are false
     *
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.christiesPrepareArticle.prepare">
     *  <cs:param name="article" select="$article-xml"/>
     *  <cs:param name="replaceLotSymbols" select="true | false"/>
     *  <cs:param name="addLineBreaks" select="true | false"/>
     * </cs:command>
     * }
     * </pre>
     *
     * @return The AXml.
     */
    public AXml prepare(AXml article, String fs, String assetID, boolean replaceLotSymbols, boolean addLineBreaks, Logger logger) throws Exception {

        AXml updatedArticle = IoiStripHtml.prepareArticle(article, assetID, replaceLotSymbols, addLineBreaks);

        return updatedArticle;
    }

    public AXml prepareFromString(CsContext context, Map<String, Object> paramMap) throws Exception {

        Logger logger = Io.logger(context);

        String textString = paramMap.get("textString").toString();
        boolean replaceLotSymbols = paramMap.get("replaceLotSymbols") != null ? Boolean.parseBoolean((String) paramMap.get("replaceLotSymbols")) : false;
        boolean addLineBreaks = paramMap.get("addLineBreaks") != null ? (boolean) Boolean.parseBoolean((String) paramMap.get("addLineBreaks")) : false;

        return prepareFromString(textString, replaceLotSymbols, addLineBreaks, logger);
    }

    /**
     * Command to strip HTML tags and update Lot Symbols
     * Optionally you can pass booleans to replaceLotSymbols and addLineBreaks and defaults are false
     *
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.christiesPrepareArticle.prepare">
     *  <cs:param name="textString" select="$text-string"/>
     *  <cs:param name="replaceLotSymbols" select="true | false"/>
     *  <cs:param name="addLineBreaks" select="true | false"/>
     * </cs:command>
     * }
     * </pre>
     *
     * @return The AXml.
     */
    public AXml prepareFromString(String textString, boolean replaceLotSymbols, boolean addLineBreaks, Logger logger) throws Exception {

        AXml updatedArticle = IoiStripHtml.prepareArticleFromString(textString, replaceLotSymbols, addLineBreaks);

        logger.info("   ###   PREPARE ARTICLE - " + updatedArticle.toStringPretty());

        return updatedArticle;
    }

    public AXml prepareForExcel(CsContext context, Map<String, Object> paramMap) throws Exception {

        Logger logger = Io.logger(context);

        AXml article = (AXml) paramMap.get("article");
        String fs = (String) paramMap.get("fs");
        String assetID = paramMap.get("id").toString();
        boolean replaceLotSymbols = paramMap.get("replaceLotSymbols") != null ? Boolean.parseBoolean((String) paramMap.get("replaceLotSymbols")) : false;
        boolean addLineBreaks = paramMap.get("addLineBreaks") != null ? (boolean) Boolean.parseBoolean((String) paramMap.get("addLineBreaks")) : false;

        return prepareForExcel(article, fs, assetID, replaceLotSymbols, addLineBreaks, logger);
    }

    /**
     * Command to strip HTML tags and update Lot Symbols
     * Optionally you can pass booleans to replaceLotSymbols and addLineBreaks and defaults are false
     *
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.christiesPrepareArticle.prepareForExcel">
     *  <cs:param name="article" select="$article-xml"/>
     *  <cs:param name="replaceLotSymbols" select="true | false"/>
     *  <cs:param name="addLineBreaks" select="true | false"/>
     * </cs:command>
     * }
     * </pre>
     *
     * @return The AXml.
     */
    public AXml prepareForExcel(AXml article, String fs, String assetID, boolean replaceLotSymbols, boolean addLineBreaks, Logger logger) throws Exception {

        AXml updatedArticle = IoiStripHtml.prepareArticleForExcel(article, assetID, replaceLotSymbols, addLineBreaks);

        logger.info("   #### UPDATE ARTICLE - " + updatedArticle);

        return updatedArticle;
    }

    public AXml prepareForExcelFromString(CsContext context, Map<String, Object> paramMap) throws Exception {

        Logger logger = Io.logger(context);

        String textString = paramMap.get("textString").toString();
        boolean replaceLotSymbols = paramMap.get("replaceLotSymbols") != null ? Boolean.parseBoolean((String) paramMap.get("replaceLotSymbols")) : false;
        boolean addLineBreaks = paramMap.get("addLineBreaks") != null ? (boolean) Boolean.parseBoolean((String) paramMap.get("addLineBreaks")) : false;

        return prepareForExcelFromString(textString, replaceLotSymbols, addLineBreaks, logger);
    }

    /**
     * Command to strip HTML tags and update Lot Symbols
     * Optionally you can pass booleans to replaceLotSymbols and addLineBreaks and defaults are false
     *
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.christiesPrepareArticle.prepareForExcelFromString">
     *  <cs:param name="textString" select="$textString"/>
     *  <cs:param name="replaceLotSymbols" select="true | false"/>
     *  <cs:param name="addLineBreaks" select="true | false"/>
     * </cs:command>
     * }
     * </pre>
     *
     * @return The AXml.
     */
    public AXml prepareForExcelFromString(String textString, boolean replaceLotSymbols, boolean addLineBreaks, Logger logger) throws Exception {

        AXml updatedArticle = IoiStripHtml.prepareArticleForExcelFromString(textString, replaceLotSymbols);

        return updatedArticle;
    }

    public AXml prepareConvertedSymbols(CsContext context, Map<String, Object> paramMap) throws Exception {

        Logger logger = Io.logger(context);

        String textString = paramMap.get("textString").toString();

        return prepareConvertedSymbols(textString, logger);
    }

    /**
     * Command to strip HTML tags and update Lot Symbols
     * Optionally you can pass booleans to replaceLotSymbols and addLineBreaks and defaults are false
     *
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.christiesPrepareArticle.prepareConvertedSymbols">
     *  <cs:param name="textString" select="$text-string"/>
     * </cs:command>
     * }
     * </pre>
     *
     * @return The AXml.
     */
    public AXml prepareConvertedSymbols(String textString, Logger logger) throws Exception {

        AXml updatedArticle = IoiStripHtml.processSymbolConvertedString(textString);

        logger.info("   ###   PREPARE ARTICLE - " + updatedArticle.toStringPretty());

        return updatedArticle;
    }

}
