/*
 * Copyright (c) by censhare AG
 * SVN $Id:$
 */ 
package modules.api;

import java.util.Map;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import modules.asset_tree_sync.AssetSyncLib;

import com.censhare.manager.assetmanager.AssetManagementService;
import com.censhare.model.corpus.generated.AssetBase.AssetDeletionState;
import com.censhare.model.corpus.impl.Asset;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.model.DataObjectKey;
import com.censhare.support.model.DataObjectUtil;
import com.censhare.support.model.PersistenceManager;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.transaction.CsContext;
import com.censhare.support.transaction.TransactionManager;
import com.censhare.support.util.TypeConversion;
import com.censhare.support.xml.AXml;
import com.censhare.support.xml.DXml;

/**
 * @author jh
 *
 */
public final class Assetmanagement {
    
    @Resource
    private DBTransactionManager tm; // injected TransactionManager
    
    private PersistenceManager pm;
    private AssetManagementService am;
    
    @PostConstruct
    private void postConstruct() {
        if (tm == null)
            throw new IllegalStateException("TransactionManager not injected");
        
        pm = tm.getDataObjectTransaction().getPersistenceManager();
        am = ServiceLocator.getStaticService(AssetManagementService.class);
    }
    
    public Object cloneAndCleanAssetXml(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            AXml assetXml = (AXml) paramMap.get("source");
            return cloneAndCleanAssetXml(context, assetXml);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "cloneAndCleanAssetXml", paramMap);
            throw e;
        }
    }
    
    /**
     * Tries to construct an asset from the given XML, clones this asset and does some cleaning of the asset. 
     * The cleaning includes the deleting of feature nodes with key censhare:uuid,
     * all production flags, interface stuff and all parent asset element relations, for example. 
     * The corresponding XML of the resulting asset is stored in a "returning" attribute or is returned directly.
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CloneAndCleanAssetXml" returning=":resultVariable">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Example:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CloneAndCleanAssetXml" returning="cleanedAssetXml">
     *   <cs:param name="source" select="$assetXml"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The input XML to construct the asset of (required).
     * @return The corresponding XML of the resulting asset. If a "returning" attribute is present, the result
     *   is stored in a variable with the given name and can be used afterwards. It's also allowed to omit the
     *   "returning" attribute. In that case the result document is directly returned by the cs:command function.
     */
    public AXml cloneAndCleanAssetXml(CsContext context, AXml source) throws Exception {
        AXml assetXml = source;
        Logger logger = Io.logger(context);
        logger.info("### clone and clean asset xml");
        
        // duplicate asset and clean it (e.g. remove uuid)
        AssetSyncLib assetSyncLib = new AssetSyncLib(logger, tm, pm, am, false /*disableDBConstraintChecks*/);
        Asset rawAsset = (Asset) pm.mapRecursive(assetXml);
        Asset cleanedAsset = assetSyncLib.duplicate(rawAsset, null, false /*makePersistent*/, false /*convertChildRels*/);
        
        // return asset xml
        return cleanedAsset.xml();
    }
    
    public Object checkInNew(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            AXml assetXml = (AXml) paramMap.get("source");
            return checkInNew(context, assetXml);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "checkInNew", paramMap);
            throw e;
        }
    }
    
    /**
     * Tries to construct an asset from the given XML and to check in this new asset (with version 1) then. 
     * The XML of the checked in asset is stored in a "returning" attribute or is returned directly.
     * Usually the input asset XML was cleaned with the CloneAndCleanAssetXml-command (see 
     * {@link Assetmanagement#cloneAndCleanAssetXml(CsContext, AXml)}), but it is not necessary.
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning=":resultVariable">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Example:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CloneAndCleanAssetXml" returning="cleanedAssetXml">
     *   <cs:param name="source" select="$assetXml"/>
     * </cs:command>
     * <cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="checkedInAssetXml">
     *   <cs:param name="source" select="$cleanedAssetXml"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The input XML to construct and check in the new asset of (required).
     * @return The corresponding XML of the checked in new asset. If a "returning" attribute is present, the result
     *   is stored in a variable with the given name and can be used afterwards. It's also allowed to omit the
     *   "returning" attribute. In that case the result document is directly returned by the cs:command function.
     */
    public AXml checkInNew(CsContext context, AXml source) throws Exception {
        AXml assetXml = source;
        Logger logger = Io.logger(context);
        logger.info("### check in new asset");
        
        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        // check in new asset clone corresponding to given xml
        Asset asset = (Asset) pm.mapRecursive(assetXml);
        asset = asset.cloneAndMakeTransient(true);
        pm.makePersistentRecursive(asset);
        am.checkInNew(tm, asset);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
        
        // return asset xml
        return asset.xml();
    }
    
    public Object checkIn(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            Object source = paramMap.get("source");
            return checkIn(context, source);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "checkIn", paramMap);
            throw e;
        }
    }
    
    /**
     * Tries to construct an asset from the given XML and to check in this asset. 
     * The XML of the checked in asset is stored in a "returning" attribute or is returned directly.
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CheckIn" returning=":resultVariable">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="checkedInAssetXml">
     *   <cs:param name="source" select="$modifiedCheckedOutAssetXml"/>
     * </cs:command>
     * 
     * <cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="checkedInAssetXml">
     *   <cs:param name="source" select="10000"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The asset XML to construct and check in the asset of or the asset id of the corresponding asset (required).
     * @return The corresponding XML of the checked in asset. If a "returning" attribute is present, the result
     *   is stored in a variable with the given name and can be used afterwards. It's also allowed to omit the
     *   "returning" attribute. In that case the result document is directly returned by the cs:command function.
     */
    public AXml checkIn(CsContext context, Object source) throws Exception {
        Logger logger = Io.logger(context);
        logger.info("### check in asset");
        
        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        // get unmodified checked out asset and update it by mapping given plain XML to this asset
        DataObjectKey<Asset> assetKey = getAssetKeyFromSource(source, Asset.VERSION_CHECKEDOUT);
        Asset checkedOutAsset = am.cacheGetAsset(tm, assetKey);
        if (source instanceof AXml) {
            AXml assetXml = (AXml) source;
            if (checkedOutAsset == null)
                checkedOutAsset = (Asset) pm.mapRecursive(assetXml);
            else
                DataObjectUtil.updateDTO(assetXml, checkedOutAsset);
        }
        if (checkedOutAsset == null)
            throw new IllegalArgumentException("Asset to check in could not be found.");
        
        // check in updated asset (corresponding to given xml)
        Asset checkedInAsset = am.checkIn(tm, checkedOutAsset);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
        
        // return asset xml
        return checkedInAsset.xml();
    }
    
    public Object checkOut(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            Object source = paramMap.get("source");
            return checkOut(context, source);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "checkOut", paramMap);
            throw e;
        }
    }
    
    /**
     * Checks out the asset corresponding to the given source. 
     * The XML of the checked out asset is stored in a "returning" attribute or is returned directly.
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CheckOut" returning=":resultVariable">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="checkedOutAssetXml">
     *   <cs:param name="source" select="$assetXml"/>
     * </cs:command>
     * 
     * <cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="checkedOutAssetXml">
     *   <cs:param name="source" select="10000"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The asset XML to check out the asset of or the asset id of the corresponding asset (required).
     * @return The corresponding XML of the checked out asset. If a "returning" attribute is present, the result
     *   is stored in a variable with the given name and can be used afterwards. It's also allowed to omit the
     *   "returning" attribute. In that case the result document is directly returned by the cs:command function.
     */
    public AXml checkOut(CsContext context, Object source) throws Exception {
        Logger logger = Io.logger(context);
        logger.info("### check out asset");
        
        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        // get asset key to ckeck out
        DataObjectKey<Asset> assetKey = getAssetKeyFromSource(source, Asset.VERSION_CURRENT);
        
        // check out asset
        Asset checkedOutAsset = am.checkOut(tm, assetKey);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
        
        // return asset xml
        return checkedOutAsset.xml();
    }
    
    public void checkOutAbort(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            Object source = paramMap.get("source");
            checkOutAbort(context, source);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "checkOutAbort", paramMap);
            throw e;
        }
    }
    
    /**
     * Aborts the check out of the asset corresponding to the given source. 
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CheckOutAbort">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.CheckOutAbort">
     *   <cs:param name="source" select="$assetXml"/>
     * </cs:command>
     * 
     * <cs:command name="com.censhare.api.assetmanagement.CheckOutAbort">
     *   <cs:param name="source" select="10000"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The asset XML to check out abort the asset of or the asset id of the corresponding asset (required).
     */
    public void checkOutAbort(CsContext context, Object source) throws Exception {
        Logger logger = Io.logger(context);
        logger.info("### check out abort asset");
        
        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        // get asset key to ckeck out abort
        DataObjectKey<Asset> assetKey = getAssetKeyFromSource(source, Asset.VERSION_CHECKEDOUT);
        
        // check out abort asset
        am.checkOutAbort(tm, assetKey);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
    }
    
    public void delete(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            Object source = paramMap.get("source");
            Object state = paramMap.get("state");
            String stateString = null;
            if (state != null)
                stateString = state.toString();
            delete(context, source, stateString);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "delete", paramMap);
            throw e;
        }
    }
    
    /**
     * Deletes the asset corresponding to the given source and with the given deletion state. 
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.Delete">
     *   <cs:param name="source" select=":source"/>
     *   <cs:param name="state" select=":state"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.Delete">
     *   <cs:param name="source" select="$assetXml"/>
     * </cs:command>
     * 
     * <cs:command name="com.censhare.api.assetmanagement.Delete">
     *   <cs:param name="source" select="$assetXml"/>
     *   <cs:param name="state" select="'physical'"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The asset XML to check out abort the asset of or the asset id of the corresponding asset (required).
     * @param state The deletion state to delete the asset with. Possible values are 'none' (for not deleted), 
     * 'proposed' (for proposed for deletion), 'marked' (for marked for deletion or 'physical' 
     * (for physical deletion). This parameter is optional. If omitted the asset is proposed for deletion.
     */
    public void delete(CsContext context, Object source, String state) throws Exception {
        Logger logger = Io.logger(context);
        logger.info("### delete asset " + state);
        
        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        // get asset key to delete corresponding asset
        DataObjectKey<Asset> assetKey = getAssetKeyFromSource(source, Asset.VERSION_CURRENT);
        
        // get asset deletion state
        AssetDeletionState deletionState;
        if (state == null || state.equals("proposed"))
            deletionState = AssetDeletionState.PROPOSED;
        else if (state.equals("none"))
            deletionState = AssetDeletionState.NONE;
        else if (state.equals("marked"))
            deletionState = AssetDeletionState.MARKED;
        else if (state.equals("physical"))
            deletionState = AssetDeletionState.PHYSICAL;
        else
            throw new IllegalStateException("'" + state + "' is no valid asset deletion state. " + 
                    "Possible values are 'none', 'proposed', 'marked' or 'physical'.");
        
        // delete asset according to detected state
        am.delete(tm, assetKey, deletionState);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
    }
    
    public Object update(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            Object assetXml = paramMap.get("source");
            return update(context, assetXml);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "update", paramMap);
            throw e;
        }
    }
    
    /**
     * Tries to construct an asset from the given XML and to update this asset. 
     * The XML of the updated in asset is stored in a "returning" attribute or is returned directly.
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.Update" returning=":resultVariable">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.Update" returning="updatedAssetXml">
     *   <cs:param name="source" select="$changedAssetXml"/>
     * </cs:command>
     * 
     * <cs:command name="com.censhare.api.assetmanagement.Update" returning="updatedAssetXml">
     *   <cs:param name="source" select="10000"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The asset XML to construct and update the asset of or the asset id of the corresponding asset (required).
     * @return The corresponding XML of the updated asset. If a "returning" attribute is present, the result
     *   is stored in a variable with the given name and can be used afterwards. It's also allowed to omit the
     *   "returning" attribute. In that case the result document is directly returned by the cs:command function.
     */
    public AXml update(CsContext context, Object source) throws Exception {
        Logger logger = Io.logger(context);
        logger.info("### update asset");
        
        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        // get unmodified asset and update it by mapping given plain XML to this asset
        DataObjectKey<Asset> assetKey = getAssetKeyFromSource(source, Asset.VERSION_CURRENT);
        Asset asset = am.cacheGetAsset(tm, assetKey);
        if (source instanceof AXml) {
            AXml assetXml = (AXml) source;
            if (asset == null)
                asset = (Asset) pm.mapRecursive(assetXml);
            else {
                final Integer ccn = assetXml.getAttrInt(Asset.ATTR_CCN);
                final Integer tcn = assetXml.getAttrInt(Asset.ATTR_TCN);
                DXml.applyChangesRecursive(assetXml);
                DataObjectUtil.updateDTO(assetXml, asset);
                if (tcn != null) {
                    asset.setTcn(tcn);
                }
                if (ccn != null) {
                    asset.xml().setDataAttr(Asset.ATTR_CCN, ccn);
                }
            }
        }
        if (asset == null)
            throw new IllegalArgumentException("Asset to update could not be found.");
        
        // update asset (corresponding to given xml)
        if (asset.isNewDeletedOrModifiedRecursive())
            am.update(tm, asset);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
        
        // return asset xml
        return asset.xml();
    } 

    public Object updateWithLock(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            Object assetXml = paramMap.get("source");
            return updateWithLock(context, assetXml);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "updateWithLock", paramMap);
            throw e;
        }
    }
    
    /**
     * Tries to construct an asset from the given XML and to lock and update this asset. 
     * The XML of the updated in asset is stored in a "returning" attribute or is returned directly.
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.updateWithLock" returning=":resultVariable">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.assetmanagement.updateWithLock" returning="updatedAssetXml">
     *   <cs:param name="source" select="$changedAssetXml"/>
     * </cs:command>
     * 
     * <cs:command name="com.censhare.api.assetmanagement.updateWithLock" returning="updatedAssetXml">
     *   <cs:param name="source" select="10000"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The asset XML to construct, lock and update the asset of or the asset id of the corresponding asset (required).
     * @return The corresponding XML of the updated asset. If a "returning" attribute is present, the result
     *   is stored in a variable with the given name and can be used afterwards. It's also allowed to omit the
     *   "returning" attribute. In that case the result document is directly returned by the cs:command function.
     */
    public AXml updateWithLock(CsContext context, Object source) throws Exception {
        Logger logger = Io.logger(context);
        logger.info("### update with lock asset");
        
        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        // get unmodified asset and update it by mapping given plain XML to this asset
        DataObjectKey<Asset> assetKey = getAssetKeyFromSource(source, Asset.VERSION_CURRENT);
        // Asset asset = am.cacheGetAsset(tm, assetKey);
        Asset asset = am.cacheGetAsset(tm, assetKey);
        asset = am.assureUpToDateAndLock(tm, asset, true);
        if (source instanceof AXml) {
            AXml assetXml = (AXml) source;
            if (asset == null)
                asset = (Asset) pm.mapRecursive(assetXml);
            else
                DataObjectUtil.updateDTO(assetXml, asset);
        }
        if (asset == null)
            throw new IllegalArgumentException("Asset to update could not be found.");
        
        // update asset (corresponding to given xml)
        am.update(tm, asset);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
        
        // return asset xml
        return asset.xml();
    }


/*    public void commit (CsContext context, Map<String, Object> paramMap) throws Exception {
        Logger logger = Io.logger(context);        
        if (tm.getStatus() != TransactionManager.STATE_BEGUN) {
            logger.info("### commit current transaction");
            tm.commit();
        } else {
            logger.info("### no active transaction");
        }
    }
*/

    public boolean commit (CsContext context, Map<String, Object> paramMap) throws Exception {
        Logger logger = Io.logger(context);
		logger.info("------- Inside Commit ----- " + paramMap.size());

        boolean status = false;  
        try{   
        if (tm.getStatus() != TransactionManager.STATE_BEGUN) {
            logger.info("### commit current transaction");
            tm.commit();
            status = true;
        } else {
            logger.info("### no active transaction");
        }
        }catch(Exception e)
        {
        logger.log(Level.WARNING,"Exception in commit",e);
         status = false;
        }
		logger.info("------- Inside Commit Returning Status----- " + status);
        return status;
    }
    
    /** If tm is not begun tm begin is called and true returned, otherwise false is returned. */
    private boolean tryToBeginTransaction() throws Exception {
        if (tm.getStatus() != TransactionManager.STATE_BEGUN) {
            tm.begin();
            return true;
        }
        else
            return false;
    }
    
    /** If asset id can be extracted from source, asset key with this id is returned, otherwise exception is thrown. */
    private DataObjectKey<Asset> getAssetKeyFromSource(Object source, int defaultCurrversion) throws IllegalArgumentException {
        // get asset id (and currversion) from asset xml or try to use source directly as asset id 
        Long assetId = null;
        int currversion = defaultCurrversion;
        if (source instanceof AXml) {
            AXml assetXml = (AXml) source;
            assetId = assetXml.getAttrLong(Asset.ATTR_ID);
            currversion = assetXml.getAttrInt(Asset.ATTR_CURRVERSION, defaultCurrversion);
        }
        else
            assetId = TypeConversion.toLong(source);
        
        if (assetId == null)
            throw new IllegalArgumentException("Asset id cannot be extracted from source.");
        
        if (currversion > 0) 
            return Asset.newPk(assetId, currversion);
        
        return Asset.newUn(assetId, currversion);
    }
    
}
