package modules.api;

import com.censhare.manager.assetmanager.AssetManagementService;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.transaction.CsContext;
import com.censhare.support.xml.AXml;
import com.censhare.support.xml.xpath.XPathContext;
import com.censhare.support.xml.xpath.XPathExpr;
import com.censhare.support.xml.xpath.XPathParser;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Map;

/**
 * @author Cameron Cramsey
 * 10/30/17.
 */
public class IoiXPathEvaluate {

    private static final String XPATH_KEY = "xpath";

    @Resource
    private DBTransactionManager tm; // injected TransactionManager

    @Resource
    private Io io; // injected helper class Io

    @Resource
    private AssetManagementService am;

    @Resource
    private XPathParser xp;

    @PostConstruct
    private void postConstruct() {
        if (tm == null)
            throw new IllegalStateException("TransactionManager not injected");
        if (io == null)
            throw new IllegalStateException("Io not injected");
        if (am == null)
            throw new IllegalStateException("am not injected");
    }

    /**
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.ioiXPathEvaluate.evaluate">
     *   <cs:param name="xpath" select=":xpath"/>
     *   <cs:param name="{$context-variable-name}" select="{$context-variable-xml}"/>
     *   ...
     * </cs:command>
     * }
     * </pre>
     *
     * <b>Example:</b> Executes XPath and passes two variables to be used within the xpath.
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.ioiXPathEvaluate.evaluate">
     *   <cs:param name="xpath" select="concat($asset-xml/@id, $test/@test-str)"/>
     *   <cs:param name="asset-xml" select="$asset-xml"/>
     *   <cs:param name="test" select="$test"/>
     * </cs:command>
     * }
     * </pre>
     *
     * @param paramMap The map of the xpath statement and contextual variables and their xml.
     *
     * @return A String containing the result of the xpath
     */
    public String evaluate(CsContext csContext, Map<String, Object> paramMap) throws Exception {


        if(paramMap.containsKey(XPATH_KEY)) {

            String xpath = (String) paramMap.get(XPATH_KEY);

            XPathContext context = new XPathContext(csContext);
            for(Map.Entry<String, Object> c : paramMap.entrySet()) {
                if(!c.getKey().equals(XPATH_KEY) && (c.getValue() instanceof AXml || c.getValue() instanceof String)) {
                    context.setVariable(c.getKey(), c.getValue());
                }
            }


            XPathExpr expr = xp.parse(xpath);

            System.out.println("XPATH - " + xpath);

            String result = expr.eval(context).toString();

            if(result.startsWith("[")) {
                result = result.replaceFirst("\\[", "").replace("]", "");
            }

            System.out.println("Result - " + result);

            return result;

        } else {
            throw new Exception("   ###   No xpath provided!");
        }

    }

}
