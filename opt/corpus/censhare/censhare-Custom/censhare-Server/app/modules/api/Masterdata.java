/**
 *  Copyright (c) by censhare AG
 *  SVN $Id$
 */

package modules.api;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import com.censhare.server.manager.CommonServiceFactory;
import com.censhare.server.manager.DBTransactionManager;
import com.censhare.server.support.cache.CacheRefreshTransactionResource;
import com.censhare.support.cache.CacheService;
import com.censhare.support.model.DataObject;
import com.censhare.support.model.DataObjectKey;
import com.censhare.support.model.DataObjectXmlAttr;
import com.censhare.support.model.PersistenceManager;
import com.censhare.support.model.SchemaDefinition;
import com.censhare.support.model.SchemaDefinition.ColumnDefinition;
import com.censhare.support.model.SchemaDefinition.TableDefinition;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.transaction.CsContext;
import com.censhare.support.transaction.TransactionManager;
import com.censhare.support.util.MiscUtilities;
import com.censhare.support.xml.AXml;

public final class Masterdata {

    CacheService cacheService = ServiceLocator.getStaticService(CacheService.class);    

    private DBTransactionManager tm;
    
    private PersistenceManager pm;    

    SchemaDefinition schema;
    
    @PostConstruct
    private void postConstruct() {
        tm = CommonServiceFactory.createDBTransactionManager(getClass().getSimpleName());
	    pm = tm.getDataObjectTransaction(MiscUtilities.LOCALE_ALL).getPersistenceManager();       
        schema = pm.getSchema();
    }

     

    
    public Object injectMasterData(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            AXml masterDataXml = (AXml) paramMap.get("source");
            Object mode = paramMap.get("mode");
            String modeString = null;
            if (mode != null)
                modeString = mode.toString();
            return injectMasterData(context, masterDataXml, modeString);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "injectMasterData", paramMap);
            throw e;
        }        
    }

     /**
     * Imports master data 
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.masterdata.InjectMasterData">
     *   <cs:param name="source" select=":source"/>
     *   <cs:param name="mode" select=":mode"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <xsl:variable name="master-data-xml">
     *   <feature key="censhare:article-type" type="censhare:asset-feature" storage="0" name="Article type" name_de="Artikel-Typ" value_type="2" ismultivalue="0" isassetinfo="0" assetinfo_sorting="0" issearchable="1" sorting="1" language_type="0" color_type="0" bdb_key="302" enabled="1" has_relevance="0" target_object_type="asset" trait="content" trait_property="articleType" user_modifiable="1" is_xml_attr="1">
     *     <feature_value feature="censhare:article-type" is_hierarchical="0" value_key="standard" name="Standard" name_de="Standard" sorting="0" enabled="1" />
     *     <feature_value feature="censhare:article-type" is_hierarchical="0" value_key="static" name="Static" name_de="Statisch" sorting="0" enabled="1" />
     *     <feature_value feature="censhare:article-type" is_hierarchical="0" value_key="form" name="Form" name_de="Formular" sorting="0" enabled="1" />
     *   </feature>
     * </xsl:variable>  
     * <cs:command name="com.censhare.api.masterdata.InjectMasterData" returning="resultXml">
     *   <cs:param name="source" select="$master-data-xml"/>
     *   <cs:param name="mode" select="'both'"/>     
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The master data XML to inject into the master data.
     * @param mode This parameter is optional. If omitted the master data gets created and updated.
     * Available modes include 
     * 'import': only add non-existing records
     * 'change': only update existing records
     * 'both': default setting, add and update records
     */

    public AXml injectMasterData(CsContext context, AXml source, String mode) throws Exception {

        Set<String> involvedTables = new HashSet<String>(); 
        Map<String, ImportCounter> tableMap  = new HashMap<String, ImportCounter>();

        Logger logger = Io.logger(context);
        
        // flags for real import
        boolean importFlag = true;
        boolean changeFlag = true;
        //#2353775 if true then attributes with value null from import will be ignore
        boolean onlyNonEmptyAttrChange = false;

        if (mode == null || "both".equals(mode)) {
            importFlag = true;
            changeFlag = true;
        }
        else if ("import".equals(mode)) {
            importFlag = true;
            changeFlag = false;
        }
        else if ("change".equals(mode)) {
            importFlag = false;
            changeFlag = true;
        }
        if (logger.isLoggable(Level.FINE))
            logger.fine("Flags: import=" + importFlag + " change=" + changeFlag);

        ImportCounter sumIc = new ImportCounter();
        AXml infoXml = AXml.createElement("info-xml");

        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();

        //Iterate over nodes to import
        importNode(source, false, importFlag, changeFlag, sumIc, onlyNonEmptyAttrChange, logger, tableMap);
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
        //commit changes and refresh tables
        refreshCachedTables(cacheService, logger, tableMap);
        tm.commit();

        //insert final statistics
        infoXml.setAttr("updated", sumIc.updated);
        infoXml.setAttr("ignored", sumIc.ignored);
        infoXml.setAttr("changed", sumIc.changed); 

        // return result xml
        return infoXml;
        
    }

    private void importNode(AXml contentXml, boolean preRun, boolean importFlag, boolean changeFlag, ImportCounter sumIc, boolean onlyNonEmptyAttrChange, Logger logger, Map<String, ImportCounter> tableMap) throws Exception {

        for (AXml node : contentXml.findAll()) {

            //get statistics for updated table
            String table = node.getNodeName();

            ImportCounter ic = tableMap.get(table);
            if (ic == null) {
                //no statistics present make new one
                ic = new ImportCounter();
                tableMap.put(table, ic);
            }

            if(logger.isLoggable(Level.FINER))
                logger.finer("trying node: "+node.getNodeName());

            //fetch key for node to perform checks
            DataObject<?> importDTO = pm.mapRecursive(node.cloneMutableDeep());
            if (!(importDTO instanceof DataObjectXmlAttr)) {
                DataObjectKey<?> dok = importDTO.getPrimaryKeyUnchecked();
                DataObject<?> dto = null;
                //if DataObject has valid key look for corresponding DTO and changes
                boolean mustInsert = dok != null && dok.hasNullValue();
                if (!mustInsert) {
                    dto = updateDTO(dok,importDTO, onlyNonEmptyAttrChange, logger);
                    mustInsert = dto == null;
                }

                if(!mustInsert){
                    // node present => look for changes in the data

                    if (dto.isModified()) {  //null-warning is wrong (this here is so sophisticated indeed)
                        // if we're just looking if something changes, discard changes
                        // discard also if changes shouldn't be applied
                        if (preRun || !changeFlag)
                            dto.discardChanges();
                        if (changeFlag) {
                            ic.changed++;
                            sumIc.changed++;
                        }
                    }// DTO wasn't modified so ignore it
                    else {
                        ic.ignored++;
                        sumIc.ignored++;
                    }
                    // recurse to find new and modified child objects:
                    importNode(node, preRun, importFlag, changeFlag, sumIc, onlyNonEmptyAttrChange, logger, tableMap);
                }
                // key invalid or new node => insert this node as new
                // don't insert if nothing should be imported!
                else {
                    if (!preRun && importFlag) {
                        // insert new
                        pm.makePersistentRecursive(importDTO);
                    }
                    if (importFlag) {
                        ic.updated++;
                        sumIc.updated++;
                        collectChildTables(importDTO, sumIc, tableMap);
                    }
                }
            }
//            importNode(node, preRun, importFlag, changeFlag, sumIc);
        } //endfor
    }

    /**
     * Collect updated tables for cache refresh and count new objects.
     */
    private void collectChildTables(DataObject<?> dto, ImportCounter sumIc, Map<String, ImportCounter> tableMap) {
        for (DataObject<?> childDTO: dto.getAll().setFilterDeleted(false)) {
            //get statistics for updated table
            String table = childDTO.getBaseElementName();
            ImportCounter ic = tableMap.get(table);
            if (ic == null) {
                //no statistics present make new one
                ic = new ImportCounter();
                tableMap.put(table, ic);
            }
            ic.updated ++;
            sumIc.updated ++;
            collectChildTables(childDTO, sumIc, tableMap);
        }
    }

    /**
     * look for changes and transfer them from new DataObject to current DataObject
     * @param dok DataObjectKey that was created for the imported DataObject
     * @param importDTO DataObject that was created for the xml import data
     * @return null if no corresponding DataObject was found in database, or
     * the found DataObject with applied changes (if there are any).
     */
    private DataObject<?> updateDTO(DataObjectKey<?> dok, DataObject<?> importDTO, boolean onlyNonEmptyAttrChange, Logger logger) throws Exception {

        if(logger.isLoggable(Level.FINER))
            logger.finer("looking for Entry with key: "+dok);

        //using cached tables brought errors on writing changes to db
        //fetch object for given key
        DataObject<?> dto = cacheService.getEntry(dok);
        //DataObject<?> dto = pm.query(dok);
        //key ok but no object there, return to insert as new
        if(dto == null)
        	return null;

        dto = dto.cloneAndMakeTransient(true);
        pm.attach(dto);

        //now look for changes in the object's data
        TableDefinition table = schema.getTableDefinition(dto.getBaseElementName());
        for (ColumnDefinition column : table.getColumns()) {
            if (!column.isTCN() && !column.isReadOnly()) {
                if (column.isLocalized()) {
                    for (String locale : schema.getLocaleStrings()) {
                        String attr;
                        if (locale.equals(schema.getDefaultLocaleString()))
                            attr = column.getAttrName();
                        else
                            attr = column.getAttrName() + SchemaDefinition.LOCALE_INFIX + locale;
                        Object newVal = importDTO.xml().getAttrObject(attr);
                        //#2353775 if value of attribute is null then ignore it.
                        if (onlyNonEmptyAttrChange && newVal == null)
                            continue;
                        dto.xml().setAttr(attr, newVal);
                    }
                }
                else {
                    Object newVal = importDTO.xml().getAttrObject(column.getAttrName());
                    //#2353775 if value of attribute is null then ignore it.
                    if (onlyNonEmptyAttrChange && newVal == null)
                        continue;
                    dto.xml().setAttr(column.getAttrName(), newVal);
                }
            }
        }
        if(logger.isLoggable(Level.FINER))
            logger.finer("DTO is modified? "+dto.isModified()+" DTO transactional? "+dto.isTransactional()+" Flags: "+dto.getFlags());

        return dto;
    }

    private class ImportCounter {
        /** number of rows that have been changed */
        int changed = 0;
        /** number of rows that would be added in this update */
        int updated = 0;
        /** number of rows that well be ignored */
        int ignored = 0;
    }

    private void refreshCachedTables(CacheService cacheService, Logger logger, Map<String, ImportCounter> tableMap) throws Exception {
        
        boolean hasToTmEnd = tryToBeginTransaction();

        // execute after DB commit, since changes in current transaction are not yet visible
        CacheRefreshTransactionResource crtr = new CacheRefreshTransactionResource(logger);
        tm.enlistResource(crtr);

        for (Map.Entry<String, ImportCounter> e : tableMap.entrySet()) {
            String tableName = e.getKey();
            ImportCounter ic = e.getValue();
            if (ic.changed > 0 || ic.updated > 0) {
                logger.info("tbtest: " +tableName);
                crtr.addServiceName("cache/" + tableName);
                if(logger.isLoggable(Level.FINEST))
                    logger.finest("Added Service for: " + tableName);
            }
        }
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();
    }

    /** If tm is not begun tm begin is called and true returned, otherwise false is returned. */
    private boolean tryToBeginTransaction() throws Exception {
        if (tm.getStatus() != TransactionManager.STATE_BEGUN) {
            tm.begin();
            return true;
        }
        else
            return false;
    }
}
