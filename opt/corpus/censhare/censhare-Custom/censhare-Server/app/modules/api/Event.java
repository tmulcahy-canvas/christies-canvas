/**
 *  Copyright (c) by censhare AG
 *  SVN $Id$
 */

package modules.api;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.censhare.server.manager.DBTransactionManager;
import com.censhare.support.transaction.CsContext;
import com.censhare.support.transaction.TransactionManager;
import com.censhare.support.service.ServerEvent;
import com.censhare.support.service.ServiceLocator;
import com.censhare.support.xml.AXml;

public final class Event {

    @Resource
    private DBTransactionManager tm; // injected TransactionManager
    
    
    @PostConstruct
    private void postConstruct() {
        if (tm == null)
            throw new IllegalStateException("TransactionManager not injected");
    }

    
    public Object sendEvent(CsContext context, Map<String, Object> paramMap) throws Exception {
        try {
            AXml eventXml = (AXml) paramMap.get("source");            
            return sendEvent(context, eventXml);
        }
        catch (Exception e) {
            Io.logMethodError(context, e, "sendEvent", paramMap);
            throw e;
        }        
    }

     /**
     * Imports master data 
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.event.sendEvent">
     *   <cs:param name="source" select=":source"/>
     * </cs:command>
     * }
     * </pre>
     * </p>
     * 
     * <b>Examples:</b>
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <xsl:variable name="event-xml">
     *   <event target="AssetAutomation" method="preview_maker.aa-preview" param0="123456" param1="1" param2="0" param3="assets"/>
     * </xsl:variable>  
     * <cs:command name="com.censhare.api.event.sendEvent">
     *   <cs:param name="source" select="$event-xml"/>     
     * </cs:command>
     * }
     * </pre>
     * 
     * @param source  The master data XML to inject into the master data.
     * @param mode This parameter is optional. If omitted the master data gets created and updated.
     * Available modes include 
     * 'import': only add non-existing records
     * 'change': only update existing records
     * 'both': default setting, add and update records
     */

    public AXml sendEvent(CsContext context, AXml eventXml) throws Exception {

        Logger logger = Io.logger(context);        

        ServerEvent event = new ServerEvent(eventXml, ServiceLocator.getServerName());        

        // check if tm is begun (e.g. tm injected from asset automation) or not
        boolean hasToTmEnd = tryToBeginTransaction();
        
        tm.addEvent(event);        
        
        // call tm.end() only if tm was started here
        if (hasToTmEnd)
            tm.end();

        logger.info("created new event " + event);

        return event.getEventXml();
        
    }

    /** If tm is not begun tm begin is called and true returned, otherwise false is returned. */
    private boolean tryToBeginTransaction() throws Exception {
        if (tm.getStatus() != TransactionManager.STATE_BEGUN) {
            tm.begin();
            return true;
        }
        else
            return false;
    }
}
