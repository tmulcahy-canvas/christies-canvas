package modules.api;

import com.censhare.support.transaction.CsContext;
import com.censhare.support.util.logging.ContextLogger;
import com.censhare.support.xml.AXml;
import com.censhare.support.xml.CXml;
import com.ioi.renderer.scriptgen.ScriptGenerator;

import java.util.Map;
import java.util.logging.Logger;


public class ChristiesLotCardScript {
	
	private Logger logger(CsContext context) {
        Logger logger = (Logger) context.getVariable(Context.CONTEXT_VARIABLE_LOGGER);
        if (logger == null)
            logger = ContextLogger.getLogger(getClass());
        
        return logger;
    }

    public AXml generate(CsContext context, Map<String, Object> paramMap) throws Exception {

        String count = (String) paramMap.get("count");
        
        return generate(count);
    }

    /**
     * Command to insert a count into the static script so the renderer can read it.
     * 
     * <pre class="prettyprint" style="font-size:larger">
     * {@code
     * <cs:command name="com.censhare.api.christiesLotCardScript.generate">
     * 	<cs:param name="count" select=":v"/>
     * </cs:command>
     * }
     * </pre>
     * 
     * @return The AXml.
     */
    public AXml generate(String count) throws Exception {
    	
        AXml script = new CXml("script");
        script.appendAttribute("language", "javascript");
        
        script.setTextValue(ScriptGenerator.generate(count));
        
        return script;
    }

}
