<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:func="http://ns.censhare.de/functions"
  
  exclude-result-prefixes="xs"
  version="2.0">

  
  <!-- ############################################################ -->
  <!-- basic configuration -->

  <!-- relation type connecting properties to layout -->
  <xsl:param name="source-rel-type">target.autoplacer.</xsl:param>
  <!-- asset rel feature defining target layout group id  -->
  <xsl:param name="source-rel-groupdid-key">canvas:layout-group-id</xsl:param>
  <!-- key of layout group transformation to use for place collection -->
  <xsl:param name="layout-group-transformation">layout-group</xsl:param>
  <!-- picture placement method -->
  <xsl:param name="picture-placement-rule">top-left</xsl:param>
  <!-- key of script asset to excute after placement -->
  <xsl:param name="resourcekey-script">canvas:script-autoplacer-fitframes</xsl:param>  
  <!-- perform checkout: create new version of asset -->
  <!-- set to false when checkout is performed by AssetAutomation config! -->
  <xsl:param name="perform-checkout" as="xs:boolean" select="false()"/>
  
  
  <!-- ############################################################ -->
  <!-- starting on layout asset -->
  <xsl:template match="asset[@type/starts-with(., 'layout.')]">
    <xsl:variable name="layout-asset" select="." as="element(asset)"/>
    <auto-placer>
      <xsl:variable name="indesign-storage-item" select="$layout-asset/storage_item[@key='master' and @mimetype='application/indesign']"/>
      
      <xsl:if test="$indesign-storage-item">
        <xsl:variable name="app-version" select="concat('indesign-', $indesign-storage-item/@app_version)"/>                 
        <xsl:variable name="place-result-asset" as="element(asset)">
          <xsl:call-template name="place-properties">
            <xsl:with-param name="layout-asset" select="if ($perform-checkout) then func:checkout-asset($layout-asset) else $layout-asset"/>
            <xsl:with-param name="app-version" select="$app-version" />
          </xsl:call-template>
        </xsl:variable>
        
        <xsl:copy-of select="if ($perform-checkout) then func:checkin-asset($place-result-asset) else $place-result-asset"/>
      </xsl:if>
    </auto-placer>
  </xsl:template>
  
  
  
  <!-- place related properties into layout -->
  <xsl:template name="place-properties" as="element(asset)">
    <xsl:param name="layout-asset" as="element(asset)"/>
    <xsl:param name="app-version" />
    
    <xsl:message>--- auto-place-properties</xsl:message>    
    
    <!-- loop through child_asset_rel and create place commands -->
    <xsl:variable name="place-commands" as="element(command)*">
      <xsl:apply-templates select="$layout-asset/child_asset_rel[@key = $source-rel-type]" mode="get-place-commands"/>
    </xsl:variable>
    <!-- get all assets in use -->
    <xsl:variable name="placed-assets" as="element(asset)*">
      <xsl:for-each select="distinct-values($place-commands/@place_asset_id)">
        <xsl:copy-of select="cs:get-asset(.)"/>
      </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="place-box-elements" as="element(asset_element)*">
      <xsl:for-each select="distinct-values($place-commands/@group_id)">
        <xsl:sequence select="$layout-asset/asset_element[@key='actual.'][@kind='box'][xmldata/group/@group-id = current()]" />
      </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="place-image-boxes" as="element(asset_element)*" select="$place-box-elements[xmldata/box/@content = 'pict']" />
    <xsl:variable name="place-text-boxes" as="element(asset_element)*" select="$place-box-elements[xmldata/box/@content = 'text']" />
    
    <xsl:message>image boxes for script: <xsl:value-of select="$place-image-boxes/xmldata/box/@uid"/></xsl:message>
    <xsl:message>text boxes for script: <xsl:value-of select="$place-text-boxes/xmldata/box/@uid"/></xsl:message>
    
    <!-- execute InDesign script to adjust frame sizes to content -->
    <!-- insert uids of boxes filled by this process -->
    <xsl:variable name="script-command" as="element(command)?">
      <xsl:variable name="script-asset" select="cs:asset()[@censhare:resource-key = $resourcekey-script]"/>
      <xsl:if test="$script-asset">
        <xsl:variable name="script" select="$script-asset/storage_item[@key='master']/@url/unparsed-text(.)"/>
        <xsl:call-template name="render-command-script">
          <xsl:with-param name="script" select="func:multi-replace($script, 
            ('imageBoxes: \[\s*\]', 
             'textBoxes: \[\s*\]'), 
            (concat( 'imageBoxes: [', string-join( $place-image-boxes/xmldata/box/@uid/replace(., '^b', '') , ', '), ']'), 
             concat( 'textBoxes: [', string-join( $place-text-boxes/xmldata/box/@uid/replace(., '^b', '') , ', '), ']'))
            )"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:variable>
    
    <!-- call renderer with calculated command -->
    <xsl:variable name="render-result">
      <!-- Renderer -->
      <xsl:call-template name="execute-renderer-commands">
        <xsl:with-param name="app-version" select="$app-version"/>
        <xsl:with-param name="layout-asset" select="$layout-asset"/>
        <!-- calculated commands -->
        <xsl:with-param name="commands" select="$place-commands, $script-command"/>        
        <!-- add asset xmls -->
        <xsl:with-param name="assets" select="$placed-assets"/>        
        </xsl:call-template>
    </xsl:variable>

    <!-- save result -->
    <xsl:variable name="result-asset" as="element(asset)">
      <xsl:call-template name="save-render-result">
        <xsl:with-param name="layout-asset" select="$layout-asset"/>
        <xsl:with-param name="renderResult" select="$render-result"/>
      </xsl:call-template>
    </xsl:variable>
    
<!--    <xsl:message>-\-\-     auto-place-properties – result asset: <xsl:copy-of select="$result-asset"/></xsl:message>   -->
    <xsl:copy-of select="$result-asset" /> 
  </xsl:template>
  
  
  
  <xsl:template match="child_asset_rel" mode="get-place-commands">
    <xsl:variable name="layout-group-id" select="asset_rel_feature[@feature=$source-rel-groupdid-key]/@value_long" />
    <xsl:message>--- auto-place asset <xsl:value-of select="@child_asset"/> into group <xsl:value-of select="$layout-group-id"/></xsl:message>
    <xsl:if test="$layout-group-id">
      <xsl:call-template name="render-command-place-collection">
        <xsl:with-param name="place-asset" select="cs:get-asset(@child_asset)"/>
        <xsl:with-param name="group-id" select="$layout-group-id" />
        <xsl:with-param name="transformation-key" select="$layout-group-transformation" />
        <xsl:with-param name="picture-placement-rule" select="$picture-placement-rule" />
      </xsl:call-template>
    </xsl:if>    
  </xsl:template>


  <!-- ############################################################ -->
  <!-- renderer commands -->
 
  <!-- send commands to renderer, save and return result -->
  <xsl:template name="execute-renderer-commands">
    <xsl:param name="app-version"/>
    <xsl:param name="commands"/>
    <xsl:param name="layout-asset" as="element(asset)"/>
    <xsl:param name="assets" as="element(asset)*"/>
    
    <!-- Renderer -->
    <cs:command name="com.censhare.api.Renderer.Render">
      <cs:param name="facility" select="$app-version"/>
      <cs:param name="timeout" select="'6000'"/>      
      <cs:param name="instructions">
        <cmd>
          <renderer>
            <command method="open" asset_id="{$layout-asset/@id}" document_ref_id="1" report_mode="geometry"/>                
            <!-- needed for copied template: -->
            <!-- <command method="correct-document" document_ref_id="1" />-->
            
            <xsl:copy-of select="$commands"/>
                         
<!--              <command method="update-placeholder" document_ref_id="1" />-->
            <!--<command method="update-asset-element-structure" delete-target-elements="false" document_ref_id="1" force="true" sync-target-elements="false"/>-->                    
            <command method="save" document_ref_id="1" /> 
            <command method="preview" document_ref_id="1" resolution="72" /> <!-- ToDo: command optional? -->
            <command method="text-content" document_ref_id="1" /> <!-- ToDo: command optional? -->
            <!--<command method="idml" document_ref_id="1" />--> <!-- ToDo: command optional? -->
            <command method="close" document_ref_id="1" />           
            
            
            <!-- 
            appendOpenCommand(renderer, asset);
            appendScriptCommands(renderer, asset, ScriptResourceAsset.Event.OPEN_DONE);
            
            appendRendererCommands(renderer, subCmd, asset);
                // AAPlaceCollection:
                optional: check-missing-fonts
                optional: check-missing-links
                optional: check-modified-links
                asset.getIdOrg() > 0 : correct-document
            
            appendScriptCommands(renderer, asset, ScriptResourceAsset.Event.SAVE);
            appendSaveCommand(renderer, asset);
            
            if (appendPreviewCommands)
              appendPreviewCommand(renderer, asset);

            if (appendTextContentCommands)
                appendTextContentCommand(renderer, asset);
            
            if (appendIDMLCommand)
                appendIDMLCommand(renderer, asset);
            
            appendCloseCommand(renderer, asset);
             -->
          </renderer>
          <assets>
            <xsl:copy-of select="$layout-asset" />
            <xsl:copy-of select="$assets"/>
          </assets>
        </cmd>
      </cs:param>
    </cs:command>
  
  </xsl:template>  
 
  
  <!-- make json object from sequence of param elements:  -->
  <!-- element name > property name -->
  <!-- element content > property value -->
  <xsl:function name="func:make-json" as="xs:string">
    <xsl:param name="params" as="element()*"/> 
    <xsl:value-of select="concat('{', string-join($params/concat(local-name(.), ':', '''', string(.), ''''), ', '), '}')" />
  </xsl:function>
  
 
  <!-- renderer command: place assets -->
  <xsl:template name="render-command-place">
    <xsl:param name="layout-asset" as="element(asset)"/>
    <xsl:param name="uid" as="xs:string?"/>
    <xsl:param name="place-asset" as="element(asset)"/>
    <xsl:param name="transformation-key" as="xs:string?"/>
    
    <xsl:if test="$uid">
      <command method="place" document_ref_id="1" is_manual_placement="false" place_asset_id="{$place-asset/@id}" uid="{$uid}">
        <xsl:choose>
          <xsl:when test="$transformation-key">
            <transformation url="censhare:///service/assets/asset/id/{$place-asset/@id}/transform;key={$transformation-key};format=icml;mimetype=application%2Fvnd.adobe.incopy-icml;target-asset-id={$layout-asset/@id}" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:attribute name="place_asset_element_id">0</xsl:attribute>
            <xsl:attribute name="place_storage_key">master</xsl:attribute>
          </xsl:otherwise>
        </xsl:choose>
      </command>
    </xsl:if>
  </xsl:template> 
  
  <!-- renderer command: place collection -->
  <xsl:template name="render-command-place-collection">
    <xsl:param name="group-id" as="xs:long?"/>
    <xsl:param name="group-name" as="xs:string?"/>
    <xsl:param name="place-asset" as="element(asset)"/>
    <xsl:param name="picture-placement-rule" as="xs:string">top-left</xsl:param>
    <xsl:param name="transformation-key" as="xs:string?"/>
    <command method="place-collection" document_ref_id="1" place_asset_id="{$place-asset/@id}" picture_placement_rule="{$picture-placement-rule}" report_document="false">
      <xsl:choose>
        <xsl:when test="$group-id">
          <xsl:attribute name="group_id" select="$group-id" />
        </xsl:when>
        <xsl:when test="$group-name">
          <xsl:attribute name="group_name" select="$group-name" />
        </xsl:when>
      </xsl:choose>
      <xsl:if test="$transformation-key">
        <xsl:attribute name="transformation_key" select="$transformation-key" />
      </xsl:if>
    </command>        
  </xsl:template>  
  
  <!-- renderer command: execute script -->
  <xsl:template name="render-command-script">
    <!-- wrap script into variable, render command throws an error otherwise -->
    <!-- CDATA doesn’t work here either, so leave it plain, mask special characters (&lt; etc.) -->
    <xsl:param name="script-asset-id" as="xs:long?"/>
    <xsl:param name="script"/>
    <xsl:param name="script-language">javascript</xsl:param>

    <xsl:message>--- render script</xsl:message>
    <xsl:message><xsl:value-of select="$script"/></xsl:message>
    
    <command method="script" document_ref_id="1">
      <xsl:choose>
        <xsl:when test="$script-asset-id">
          <xsl:attribute name="script_asset_id" select="$script-asset-id"/>
        </xsl:when>
        <xsl:otherwise>
          <script language="{$script-language}">
            <xsl:value-of select="$script"/>
          </script>    
        </xsl:otherwise>
      </xsl:choose>      
    </command>
  </xsl:template>
  
  <!-- renderer command: create pdf -->
  <xsl:template name="render-command-pdf">
    <xsl:param name="pdfstyle"/>    
    <command method="pdf" document_ref_id="1" pdf_stylename="{$pdfstyle}"/>
  </xsl:template>
  
  
  <!-- ############################################################ -->
  <!-- save modified asset: new storage items, as returned by renderer -->  
  
  <xsl:template name="save-render-result" as="element(asset)">
    <xsl:param name="layout-asset" as="element(asset)"/>
    <xsl:param name="renderResult"/>
    <xsl:param name="pdf-storagekey">pdf</xsl:param>
    
    <xsl:variable name="renderedAsset" select="$renderResult/cmd/assets/asset[@id = $layout-asset/@id]"/>   
    <xsl:variable name="saveCmd" select="$renderResult/cmd/renderer/command[@method = 'save']"/>
    
    <xsl:message>--- save-render-result</xsl:message>
    
    <xsl:variable name="newAssetXml" as="element(asset)">
      <xsl:apply-templates select="$renderedAsset" mode="update-layout">
        <xsl:with-param name="new-storage-items" as="element(storage_item)*">
          <!-- Master  -->
          <xsl:call-template name="new-storage-item">
            <xsl:with-param name="cmd-return" select="$saveCmd"/>
            <xsl:with-param name="key">master</xsl:with-param>
            <xsl:with-param name="mimetype" select="$renderedAsset/storage_item[@key = 'master']/@mimetype" />
          </xsl:call-template>
          <!-- Preview -->
          <xsl:call-template name="new-storage-item">
            <xsl:with-param name="cmd-return" select="$renderResult/cmd/renderer/command[@method = 'preview']/file"/>
            <xsl:with-param name="key">preview</xsl:with-param>
            <xsl:with-param name="mimetype">image/jpeg</xsl:with-param>
          </xsl:call-template>
          <!-- PDF -->
          <xsl:call-template name="new-storage-item">
            <xsl:with-param name="cmd-return" select="$renderResult/cmd/renderer/command[@method = 'pdf']"/>
            <xsl:with-param name="key" select="$pdf-storagekey"/>
            <xsl:with-param name="mimetype">application/pdf</xsl:with-param>
          </xsl:call-template>
          <!-- contents (IDMS) -->
          <xsl:call-template name="new-storage-item">
            <xsl:with-param name="cmd-return" select="$renderResult/cmd/renderer/command[@method = 'text-content']/file"/>
            <xsl:with-param name="key">content</xsl:with-param>
  <!--          <xsl:with-param name="app-version" select="$renderResult/cmd/renderer/command[@method = 'save']/@corpus:app_version" />-->
          </xsl:call-template>          
        </xsl:with-param>
      </xsl:apply-templates>
    </xsl:variable>
    
<!--    <xsl:message>new asset xml:</xsl:message>
    <xsl:message><xsl:copy-of select="$newAssetXml" /></xsl:message>-->

    <!-- update asset -->
    <xsl:variable name="resultAssetXml" as="element(asset)" select="func:update-asset($newAssetXml)"/>

    <xsl:message>---   updated layout asset <xsl:value-of select="$renderedAsset/@id"/></xsl:message>
<!--    <xsl:message>result asset xml:</xsl:message>
    <xsl:message><xsl:copy-of select="$resultAssetXml" /></xsl:message>-->

    <xsl:copy-of select="$resultAssetXml" />
  </xsl:template>
  
    
  <!-- add modified or new storage_item returned by renderer -->
  <xsl:template name="new-storage-item">
    <xsl:param name="cmd-return" as="element()*"/>
    <xsl:param name="key" as="xs:string">master</xsl:param>
    <xsl:param name="mimetype" as="xs:string?"/>
    <xsl:for-each select="$cmd-return">
      <storage_item corpus:asset-temp-filepath="{@corpus:asset-temp-filepath}" corpus:asset-temp-filesystem="{@corpus:asset-temp-filesystem}" key="{$key}">
        <xsl:attribute name="element_idx" select="(@element_idx, 0)[1]" />
        <xsl:attribute name="mimetype" select="(@mimetype, $mimetype)[1]" />
        <xsl:if test="@corpus:app_version">
          <xsl:attribute name="app_version" select="@corpus:app_version" />
        </xsl:if>
      </storage_item>
    </xsl:for-each>
  </xsl:template>
    
    
  <!-- ######################################## -->
  <!-- mode = update-layout -->
  
  <!-- update layout asset for saving render results -->  
  
  <xsl:template match="@* | node()" mode="update-layout" priority="0.0">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="asset" mode="update-layout" priority="1.0">
    <xsl:param name="new-storage-items" as="element(storage_item)*"/>
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
      <xsl:copy-of select="$new-storage-items"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- remove existing storage_items -->  
  <xsl:template match="storage_item" mode="update-layout" priority="1.0" />  
  
  <!-- cleanup existing planning relations -->  
  <xsl:template match="child_asset_rel[@key = $source-rel-type]" mode="update-layout" priority="1.0" />    
  
  
  
  <!-- ############################################################ -->
  <!-- censhare API wrappers -->
  
  <xsl:function name="func:checkout-asset" as="element(asset)">
    <xsl:param name="asset" as="element(asset)"/>
    <xsl:variable name="result">
      <cs:command name="com.censhare.api.assetmanagement.CheckOut">
        <cs:param name="source" select="$asset"/>
      </cs:command>
    </xsl:variable>
    <xsl:sequence select="$result/asset"/>
  </xsl:function>


  <xsl:function name="func:checkin-asset" as="element(asset)">
    <xsl:param name="new-asset" as="element(asset)"/>
    <xsl:variable name="result">
      <cs:command name="com.censhare.api.assetmanagement.CheckIn">
        <cs:param name="source" select="$new-asset"/>
      </cs:command>
    </xsl:variable>
    <xsl:sequence select="$result/asset"/>
  </xsl:function>
  
  
  <xsl:function name="func:update-asset" as="element(asset)">
    <xsl:param name="new-asset" as="element(asset)"/>
    <xsl:variable name="result">
      <cs:command name="com.censhare.api.assetmanagement.Update">
        <cs:param name="source" select="$new-asset"/>           
      </cs:command>
    </xsl:variable>
    <xsl:sequence select="$result/asset"/>
  </xsl:function>
  

  <xsl:function name="func:multi-replace" as="xs:string">
    <xsl:param name="input"/>
    <xsl:param name="find"/>
    <xsl:param name="replace"/>
    
    <xsl:choose>
      <xsl:when test="(count($find) gt 1) and (count($replace) gt 1)">
        <xsl:variable name="one-replaced" select="replace($input, $find[1], $replace[1])"/>
        <xsl:value-of select="func:multi-replace($one-replaced, remove($find, 1), remove($replace, 1))"/>
      </xsl:when>
      <xsl:when test="(count($find) gt 0) and (count($replace) gt 0)">
        <xsl:value-of select="replace($input, $find[1], $replace[1])"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$input"/>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:function>
  

</xsl:stylesheet>