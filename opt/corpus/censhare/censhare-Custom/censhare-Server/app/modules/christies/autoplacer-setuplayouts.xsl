<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:func="http://ns.censhare.de/functions" exclude-result-prefixes="xs" version="2.0">
  
  <xsl:param name="only-simulate" as="xs:boolean" select="false()"/>
  
  <xsl:param name="target-rel-type">target.autoplacer.</xsl:param>
  <xsl:param name="target-rel-groupdid-key">canvas:layout-group-id</xsl:param>  
  
  <!-- wrap whole result -->
  <xsl:template match="/">
    <result>
      <xsl:variable name="asset" select="asset"/>
      <xsl:variable name="sale-assets-from-catelogue-level" select="if ($asset/@type = 'issue.') then for $section-asset in $asset/cs:child-rel()[@key='*']/cs:asset()[@censhare:asset.type='issue.section.'] return count($section-asset/cs:child-rel()[@key='user.']/cs:asset()[@censhare:asset.type='sale.']) else ()"/>
      <xsl:variable name="sale-assets-from-section-level" select="if ($asset/@type = 'issue.section.') then count($asset/cs:child-rel()[@key='user.']/cs:asset()[@censhare:asset.type='sale.']) else ()"/>
      
      <xsl:choose>
        <xsl:when test="sum($sale-assets-from-catelogue-level) = 0 or $sale-assets-from-section-level = 0">
          <xsl:call-template name="command-message">
            <xsl:with-param name="level">ok</xsl:with-param>
            <xsl:with-param name="title">No Sale Asset is available</xsl:with-param>
            <xsl:with-param name="message">Section needs to contain a Sale Asset. Please assign a Sale before proceeding.</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="sum($sale-assets-from-catelogue-level) gt count($sale-assets-from-catelogue-level) or $sale-assets-from-section-level gt 1">
          <xsl:call-template name="command-message">
            <xsl:with-param name="level">ok</xsl:with-param>
            <xsl:with-param name="title">More than one Sale Assets are available</xsl:with-param>
            <xsl:with-param name="message">Section contains more than one Sale Asset. Only one Sale per Section is allowed. Please remove the additional Sales before proceeding.</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates/>    
        </xsl:otherwise>
      </xsl:choose>
    </result>
  </xsl:template>

  <!-- fallback for not intended assets: do nothing -->
  <xsl:template match="asset"  priority="0.0"/>
  
  <!-- catalogue: process sections -->
  <xsl:template match="asset[@type = 'issue.']" priority="0.5">
    <xsl:apply-templates select="child_asset_rel[@key = 'target.']"/>
  </xsl:template>
  
  <xsl:template match="child_asset_rel">
    <xsl:apply-templates select="cs:get-asset(@child_asset)"/>
  </xsl:template>
  
  <!-- section: main routine, handle properties and connect them to the layout assets -->
  <xsl:template match="asset[@type = 'issue.section.']" priority="0.5">      
      <!-- get sales asset -->
      <!-- if more than one sales: error message -->
      <xsl:variable name="sale-assets" select="./cs:child-rel()[@key = '*']/cs:asset()[@censhare:asset.type = 'sale.']" as="element(asset)*"/>
      
      <xsl:choose>
        <!-- one sales asset: process -->
        <xsl:when test="count($sale-assets) = 1">
          
          <!-- get properties from sales assets (planning relation), sorted () -->
          <xsl:variable name="property-assets" select="func:get-properties-from-sale($sale-assets)" as="element(asset)*"/>
      
          <property-assets>
            <xsl:copy-of select="$property-assets"/>
          </property-assets>
          
          <!-- # precalculate property-group relations: -->
          <!-- process layout assets  -->
          <xsl:variable name="layout-assets" select="func:get-layouts-from-issue(.)" as="element(asset)*"/>

          <xsl:variable name="property-groups" as="element(groups)">
            <groups>
              <xsl:apply-templates select="$layout-assets" mode="property-groups"/>
            </groups>
          </xsl:variable>        
          
          <property-groups>
            <xsl:copy-of select="$property-groups"/>
          </property-groups>
          
          <!-- connect properties to property groups -->
          <!--   - property groups and property assets are filter, no skip already existing placements -->
          <!--   - create planning relations (maybe sub type) etc. from layouts to properties -->
          <!--   - remove existing planning relations -->
          <!-- trigger actual placement (event?) -->
          <xsl:variable name="processed-layouts">
            <xsl:apply-templates select="func:filter-property-groups($property-groups, $property-assets)" mode="plan-placements">
              <xsl:with-param name="property-assets" select="func:filter-property-assets($property-assets, $layout-assets)" tunnel="yes" />
              <xsl:with-param name="layout-assets" select="$layout-assets" tunnel="yes" />
            </xsl:apply-templates>
          </xsl:variable>
          
          <xsl:choose>
            <xsl:when test="$processed-layouts/asset[@type = 'layout.']">
              <xsl:call-template name="command-message">
                <xsl:with-param name="level">ok</xsl:with-param>
                <xsl:with-param name="title">Beginning to place properties in layout(s):</xsl:with-param>
                <xsl:with-param name="message"><xsl:value-of select="string-join(
                      for $l in $processed-layouts/asset[@type = 'layout.'] return concat('', $l/@name, ' (', $l/@id, ')'), 
                      ', ')"/>
                </xsl:with-param>
              </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="command-message">
                <xsl:with-param name="level">ok</xsl:with-param>
                <xsl:with-param name="title">No new properties are able to be placed. Please review and try again.</xsl:with-param>
                <xsl:with-param name="message"></xsl:with-param>
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
  
      <!-- for each layout count groups, determines count of properties to group -->
      
      <!-- more properties than groups in section: fill all groups, do not place the additional properties -->
      <!-- less properties than groups in section: leave rest of groups empty (remove placements??) -->
      
      <!-- # start placing: -->
      <!-- for every layout: call place-collection-into-group for every group/property-->        
          
        </xsl:when>
        <!-- no sales asset -->
        <xsl:when test="count($sale-assets) = 0"/>
        <!-- too many sales assets -->
        <xsl:otherwise>
          <xsl:call-template name="command-message">
            <xsl:with-param name="level">error</xsl:with-param>
            <xsl:with-param name="title">Section contains more than one Sale</xsl:with-param>
            <xsl:with-param name="message">Section: <xsl:value-of select="concat('', @name, ' (', @id, ')')"/>. Only one Sale per Section is allowed. Please remove additional Sales before proceeding. Section was skipped.</xsl:with-param>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
  </xsl:template>
  
  
  <!-- get target layouts for issue, sorted by paging -->
  <xsl:function name="func:get-layouts-from-issue" as="element(asset)*">
    <xsl:param name="issue-asset" as="element(asset)"/>
    <xsl:variable name="element-rels" as="element(child_asset_element_rel)*">
      <xsl:for-each select="$issue-asset/asset_element[@key = 'target.'][@paging]">
        <xsl:sort select="number(@paging)"/>
        <xsl:sequence select="$issue-asset/child_asset_element_rel[@key='target.'][@parent_idx = current()/@idx]"/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:for-each-group select="$element-rels" group-by="@child_asset">
      <xsl:copy-of select="cs:get-asset(@child_asset)[@type/starts-with(., 'layout.')]"/>
    </xsl:for-each-group>    
  </xsl:function>
  
  <!-- get property asset for a sale asset -->
  <!-- sort by asset relation features: 
        - canvas:jdeproperty.lotnumber
        - canvas:jdeproperty.lotsuffix
  -->  
  <xsl:function name="func:get-properties-from-sale" as="element()*">
    <xsl:param name="sale-asset" as="element(asset)"/>
    <!-- get all properties with necessary information from relation -->
    <xsl:variable name="properties" as="element()*">
      <xsl:for-each select="$sale-asset/child_asset_rel[@key = 'target.']">
        <property>
          <xsl:choose>
            <xsl:when test="number(asset_rel_feature[@feature = 'canvas:jdeproperty.lotnumber']/@value_long)">
              <xsl:attribute name="lotnumber" select="asset_rel_feature[@feature = 'canvas:jdeproperty.lotnumber']/@value_long"/>  
              <xsl:attribute name="nolotnumber" select="0"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="nolotnumber" select="1"/>  
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="asset_rel_feature[@feature = 'canvas:jdeproperty.lotsuffix']/@value_string">            
            <xsl:attribute name="lotsuffix" select="asset_rel_feature[@feature = 'canvas:jdeproperty.lotsuffix']/@value_string"/>
          </xsl:if>
          <xsl:copy-of select="cs:get-asset(@child_asset)[@type/starts-with(., 'product.')]"/>
        </property>
      </xsl:for-each>
    </xsl:variable>
    
    <!-- sort properties -->
    <xsl:for-each select="$properties[asset]">
      <xsl:sort select="number(@nolotnumber)" />
      <xsl:sort select="number(@lotnumber)" />
      <xsl:sort select="@lotsuffix" />
      <xsl:sort select="asset/@name" />
      <xsl:sort select="asset/@id" />
      <xsl:copy-of select="asset" />
    </xsl:for-each>    
  </xsl:function>
  
  
  <!-- 
  filter properties:
  - check actual-relation to layouts
  -->
  <xsl:function name="func:filter-property-assets">
    <xsl:param name="property-assets" as="element(asset)*"/>
    <xsl:param name="layout-assets" as="element(asset)*"/>
    <xsl:sequence select="$property-assets[not(parent_asset_rel[@key = 'actual.']/@parent_asset = $layout-assets/@id)]"/>
  </xsl:function>
  
  <!-- 
  filter property groups:
  - analyse child_asset_element_rel/@child_asset for property assets
    1. only groups with @transformation-key > picture placements are irrelevant
    2. first look in already found property assets (avoid asset queries for performance)
    3. asset search
  -->  
  <xsl:function name="func:filter-property-groups" as="element(groups)">
    <xsl:param name="property-groups" as="element(groups)"/>
    <xsl:param name="property-assets" as="element(asset)*"/>
    <groups>
      <xsl:for-each select="$property-groups/layout">
        <layout>
          <xsl:copy-of select="@* except @group-count"/>
          <xsl:variable name="groups" as="element(group)*">        
            <xsl:for-each select="group">
              <xsl:variable name="placed-ids" select="asset_element[@transformation-key]/child_asset_element_rel/@child_asset"/>
              <xsl:choose>
                <!-- a property of the current sale is placed already -->
                <xsl:when test="$placed-ids = $property-assets/@id"/>
                <!-- some other property is placed already -->
                <xsl:when test="exists(for $id in $placed-ids return (if (cs:get-asset($id)/@type = 'product.') then $id else ()))" />
                <!-- no property placement: use this group -->
                <xsl:otherwise>
                  <xsl:copy-of select="." />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </xsl:variable>
          <xsl:attribute name="group-count" select="count($groups)"/>
          <xsl:copy-of select="$groups"/>
        </layout>
      </xsl:for-each>
    </groups>
  </xsl:function>
  
  
  <!-- ######################################## -->
  <!-- mode = property-groups -->
  
  <!-- parse layout assets and get information about included layout groups -->
  <!-- where properties should be placed  -->
  <!-- sorted by:
        - spread
        - group name
  -->  
  <xsl:template match="asset[@type/starts-with(., 'layout.')]" mode="property-groups">
    <layout id="{@id}">
      <xsl:variable name="asset" select="."/>
      <xsl:variable name="pages" select="asset_element[@key = 'actual.'][@kind='page']"/>
      <xsl:variable name="groups" as="element(group)*">        
        <!-- handle by spreads -->
        <xsl:for-each-group select="$pages" group-by="floor(@paging/xs:long(.) div 2)">
          <xsl:sort select="number(@paging)"/>          
          <!-- groups of spread, sorted by group name --> 
          <!-- tb- exclude if text frame not empty (e.g. has a placement) -tb -->
          <xsl:for-each-group select="$asset/asset_element[@key='actual.'][@kind='box'][@parent_idx = current-group()/@idx][not(xmldata/box[@content='text']/element/@filepath)]" group-by="xmldata/group/@group-id">
            <xsl:sort select="xmldata/group/@group-name/number(replace(., '\D', ''))"/>
            <group>
              <xsl:copy-of select="xmldata/group/@group-id"/>
              <xsl:copy-of select="xmldata/group/@group-name"/>
              <xsl:for-each select="current-group()">
                <asset_element>
                  <xsl:copy-of select="@idx, @id_extern"/>
                  <xsl:copy-of select="xmldata/group/@search-transformation-key, xmldata/group/@transformation-key" />
                  <xsl:copy-of select="xmldata/box"/>
                  <xsl:copy-of select="parent::asset/child_asset_element_rel[@key = current()/@key][@parent_idx = current()/@idx]" />
                </asset_element>
              </xsl:for-each>          
            </group>
          </xsl:for-each-group>
        </xsl:for-each-group>        
      </xsl:variable>
      <!-- Handling of douple page spread -->
      <xsl:variable name="filtered-groups">
        <xsl:for-each-group select="$groups" group-adjacent="concat(@group-id, @group-name)">
          <group>
            <xsl:copy-of select="@*"/>
            <xsl:copy-of select="current-group()/*"/>
          </group>
        </xsl:for-each-group>
      </xsl:variable>
      <xsl:attribute name="group-count" select="count($filtered-groups/group)"/>
      <xsl:copy-of select="$filtered-groups"/>
      <xsl:message>--- <xsl:copy-of select="$filtered-groups"/></xsl:message>
    </layout>
  </xsl:template>

  
  <!-- ######################################## -->


  <!-- ######################################## -->
  <!-- mode = plan-placements -->
  
  <!-- connect properties to property groups -->
  
  <xsl:template match="groups" mode="plan-placements">
    <xsl:apply-templates mode="#current" />
  </xsl:template>
  
  <xsl:template match="layout" priority="0.5" mode="plan-placements">
    <xsl:param name="layout-assets" as="element(asset)*" tunnel="yes" />
    <xsl:variable name="layout-asset" select="$layout-assets[@id = current()/@id]"/>
    <xsl:message>--- working on layout <xsl:value-of select="$layout-asset/@id"/></xsl:message>
    <!-- get new child rels -->
    <xsl:variable name="new-child-rels" as="element(child_asset_rel)*">
      <xsl:apply-templates select="group" mode="#current"/>      
    </xsl:variable>
    <xsl:if test="$layout-asset and $new-child-rels">
      <xsl:variable name="modified-layout-asset" as="element(asset)">
        <xsl:apply-templates select="$layout-asset" mode="update-layout">
          <xsl:with-param name="new-child-rels" select="$new-child-rels"/>
        </xsl:apply-templates>
      </xsl:variable>
      <xsl:copy-of select="func:update-asset($modified-layout-asset)"/>
      <!-- ToDo: send events in later step, when no errors? -->
      <xsl:variable name="event-xml">
        <event target="CustomAssetEvent" method="layout-start-autoplace" param0="{$layout-asset/@id}" param1="{$layout-asset/@version}" param2="0" />
      </xsl:variable>
      <xsl:if test="not($only-simulate)">
        <cs:command name="com.censhare.api.event.sendEvent">
          <cs:param name="source" select="$event-xml" />
        </cs:command>
      </xsl:if>
    </xsl:if>    
  </xsl:template>
  
  <!-- skip layouts with no groups -->
  <xsl:template match="layout[not(group)]" priority="1.0" mode="plan-placements"/>
  
  
  <xsl:template match="group" mode="plan-placements">
    <xsl:param name="property-assets" as="element(asset)*" tunnel="yes" />
    <xsl:variable name="index" select="sum(
      (parent::layout/preceding-sibling::layout/@group-count/xs:int(.),
      position())
      )"/>
    <xsl:variable name="property-asset" select="$property-assets[$index]"/>
    <xsl:message>--- processing group with index <xsl:value-of select="$index"/> property asset: <xsl:value-of select="$property-asset/@id"/></xsl:message>
    <xsl:if test="$property-asset">      
      <xsl:variable name="placed-image-id" select="(asset_element[box/element/@type = 'pict']/child_asset_element_rel/@child_asset)[1]" />      
      <!-- skip if image belonging to this property is already placed -->
      <xsl:choose>
        <xsl:when test="$placed-image-id = $property-asset/child_asset_rel/@child_asset">
          <xsl:message>--- skipping property asset <xsl:value-of select="$property-asset/@id"/>, image already placed in right group.</xsl:message>
        </xsl:when>
        <xsl:otherwise>
          <xsl:message>--- creating relation for property asset <xsl:value-of select="$property-asset/@id"/></xsl:message>
          <child_asset_rel key="{$target-rel-type}" child_asset="{$property-asset/@id}" >
            <asset_rel_feature feature="{$target-rel-groupdid-key}" value_long="{@group-id}"/>
          </child_asset_rel>                
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:if>    
  </xsl:template>
  
  
  <!-- ######################################## -->
  <!-- mode = update-layout -->
  
  <!-- update layout asset -->  
  
  <xsl:template match="@* | node()" mode="update-layout" priority="0.0">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="asset" mode="update-layout" priority="1.0">
    <xsl:param name="new-child-rels" as="element(child_asset_rel)*"/>
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
      <xsl:copy-of select="$new-child-rels"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- remove existing planning relations -->  
  <xsl:template match="child_asset_rel[@key = $target-rel-type]" mode="update-layout" priority="1.0" />


  
  <!-- ######################################## -->


  <xsl:template name="command-message">
    <xsl:param name="title" as="xs:string"/>
    <xsl:param name="message" as="xs:string?"/>
    <xsl:param name="level">info</xsl:param>
    <message level="{$level}" title="{$title}">
      <xsl:value-of select="$message"/>
    </message>
  </xsl:template>  
  
  <xsl:function name="func:checkinnew-asset">
    <xsl:param name="new-asset" as="element(asset)" /> 
    <xsl:choose>
      <xsl:when test="$only-simulate">
        <xsl:copy-of select="$new-asset"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="result">
          <cs:command name="com.censhare.api.assetmanagement.CheckInNew">
            <cs:param name="source" select="$new-asset" />
          </cs:command>
        </xsl:variable>
        <xsl:sequence select="$result/asset" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="func:update-asset" as="element(asset)">
    <xsl:param name="new-asset" as="element(asset)"/>
    <xsl:choose>
      <xsl:when test="$only-simulate">
        <xsl:copy-of select="$new-asset"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="result">
          <cs:command name="com.censhare.api.assetmanagement.Update">
            <cs:param name="source" select="$new-asset"/>           
          </cs:command>
        </xsl:variable>
        <xsl:sequence select="$result/asset"/>
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:function>

</xsl:stylesheet>
