<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
    xmlns:func="http://ns.censhare.de/functions" exclude-result-prefixes="#all">
    
    
    <xsl:param name="censhare:command-xml"/>
    
    <xsl:template match="/">
        <xsl:variable name="asset-xml" select="asset"/>
        
        <xsl:message>
            select asset type: <xsl:value-of select="$asset-xml/@type"/>
            Search Mode: <xsl:value-of select="$censhare:command-xml//setup/@search_mode"/>
        </xsl:message>
        
        <xsl:choose>
            <xsl:when test="starts-with($asset-xml/@type,'issue.section.')">
                <xsl:variable name="current-layout-asset" select="$asset-xml/cs:child-rel()[@key='target.']/.[starts-with(@type,'layout.')][1]"/>
                <xsl:sequence select="cs:create-search-query($current-layout-asset,$censhare:command-xml//setup/@search_mode)"/>
            </xsl:when>
            <xsl:when test="$asset-xml/@type = 'layout.'">
                <xsl:sequence select="cs:create-search-query($asset-xml,$censhare:command-xml//setup/@search_mode)"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:function name="cs:create-search-query">
        <xsl:param name="asset-xml"/>
        <xsl:param name="search-mode"/>
        <xsl:variable name="sale-asset" select="$asset-xml/cs:parent-rel()[@key='target.']/.[starts-with(@type,'issue.section.')]/cs:child-rel()[@key='user.']/.[starts-with(@type,'sale.')]"/>
        <xsl:variable name="all-layout-asset-within-issue-section-asset" select="$asset-xml/cs:parent-rel()[@key='target.']/.[starts-with(@type,'issue.section.')]/./cs:child-rel()[@key='target.']/.[(starts-with(@type,'layout.') and asset_feature[@feature='chrisus:layout-section']/@value_key='PGS')]"/>
        <xsl:choose>
        <xsl:when test="$search-mode = 'property'">
            <xsl:variable name="all-property-assets" select="$sale-asset/cs:child-rel()[@key='target.']/.[starts-with(@type,'product.')]/@id"/>
            
            <xsl:variable name="placed-property-assets">
                <xsl:for-each select="$all-layout-asset-within-issue-section-asset">
                    <xsl:for-each select="current()/cs:child-rel()[@key='actual.']/.[starts-with(@type,'product.')]">
                        <property id="{@id}" name="{@name}"/>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:variable>
            <xsl:copy-of select="cs:create-query($all-property-assets,$placed-property-assets,$search-mode)"/>
        </xsl:when>
        <xsl:when test="$search-mode = 'image'">
            <xsl:variable name="all-image-asset">
                <xsl:for-each select="$sale-asset/cs:child-rel()[@key='user.']/.[starts-with(@type,'picture.')]">
                    <image id="{@id}"/>
                </xsl:for-each>
                <xsl:variable name="images-related-with-properties" select="$sale-asset/cs:child-rel()[@key='target.']/.[starts-with(@type,'product.')]/cs:child-rel()[@key='user.*']/.[starts-with(@type,'picture.')]/@id"/>
                <xsl:for-each select="$images-related-with-properties">
                    <image id="{current()}"/>
                </xsl:for-each>
            </xsl:variable>
                        
            <xsl:variable name="all-image-asset-ids" select="$all-image-asset//@id"/>
            
            <xsl:variable name="all-placed-images">
                <xsl:for-each select="$all-layout-asset-within-issue-section-asset">
                    <xsl:for-each select="current()/cs:child-rel()[@key='actual.']/.[starts-with(@type,'picture.')]">
                        <image id="{@id}"/>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:variable>
            <xsl:copy-of select="cs:create-query($all-image-asset-ids,$all-placed-images,'image')"/>
        </xsl:when>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="cs:create-query">
        <xsl:param name="all-assets"/>
        <xsl:param name="placed-assets"/>
        <xsl:param name="search-mode"/>
        
        <xsl:variable name="all-placed-asset-ids" select="distinct-values($placed-assets//@id)"/>
        <xsl:variable name="ids">
            <xsl:for-each select="$all-assets">
                <xsl:variable name="current-asset" select="xs:long(.)"/>
                <xsl:choose>
                    <xsl:when test="index-of($all-placed-asset-ids,$current-asset) &gt; 0"/>
                    <xsl:otherwise>
                        <xsl:value-of select="$current-asset"/>
                        <xsl:if test="position() != last()">
                            <xsl:value-of select="','"/>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>
        <or>
            <condition name="censhare:asset.id" op="IN" sepchar="," value="{$ids}"/>
        </or>
        <xsl:choose>
            <xsl:when test="$search-mode = 'property'">
                <sortorders>
                    <order ascending="true" by="@chrisus:jde.lot-number"/>
                </sortorders>
            </xsl:when>
            <xsl:otherwise>
                <sortorders>
                    <order ascending="true" by="@censhare:asset.name"/>
                </sortorders>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:function>
    
    
</xsl:stylesheet>
