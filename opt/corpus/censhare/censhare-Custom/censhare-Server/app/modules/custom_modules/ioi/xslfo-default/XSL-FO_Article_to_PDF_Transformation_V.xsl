<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:xe="http://www.censhare.com/xml/3.0.0/xmleditor"
  xmlns:my="http://www.censhare.com">

  <!-- The XSLT creates a PDF of an Articles Main Content in the selected language to be displayed in the 
       Artice or Content Editor -->

  <xsl:output method="xml" indent="yes" omit-xml-declaration="no" encoding="UTF-8"/>

    <!-- parameter -->
  <xsl:param name="transform"/>
  
  
  
  <!-- variables -->
  <xsl:variable name="language" select="$transform/@language"/>
  <xsl:variable name="mode" select="if ($transform) then $transform/@mode else ''"/>
  <xsl:variable name="app" select="if ($transform) then $transform/@app else 'none'"/>
  <xsl:variable name="scale" select="if ($transform) then $transform/@scale else 1.0"/>
    <xsl:variable name="urlPrefix" select="if ($mode = 'browser') then '/ws/rest/service/' else if ($mode='cs5') then '/censhare5/client/rest/service/' else '/service/'"/>
  <xsl:variable name="rootAsset" select="asset[1]"/>
  <xsl:variable name="mainContentAssets" select="my:getCheckedOutInsideAsset(my:getMasterContentAsset($rootAsset, $language))"/>

  <!-- root match -->
  <xsl:template match="/">
     <xsl:message><xsl:copy-of select="$mainContentAssets"/></xsl:message>
  	 <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out">
		 </cs:command>
  	
     <cs:command name="com.censhare.api.transformation.FopTransformation">
        <cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=censhare:pdf-preview-xslfo/storage/master/file'"/>
        <cs:param name="source" select="$mainContentAssets/storage_item[@key='master'][1]"/>
        <cs:param name="dest" select="concat($out, 'temp-preview.pdf')"/>
      </cs:command>
      
      <!-- Return the result (file locator) -->
      <cs:output href="concat($out ,'temp-preview.pdf')" media-type="'application/pdf'"/>

  </xsl:template>

  <!-- get checked out inside assets (checked out versions) of given asset -->
  <xsl:function name="my:getCheckedOutInsideAssets" as="element(asset)*">
    <xsl:param name="asset" as="element(asset)"/>
    <xsl:variable name="assetIDs" select="cs:asset()[@censhare:asset.checked_out_inside_id=$asset/@id]/@id"/>
    <xsl:for-each select="$assetIDs">
      <xsl:sequence select="cs:get-asset(., 0, -2)"/>
    </xsl:for-each>
  </xsl:function>

  <!-- get master content asset -->
  <xsl:function name="my:getMasterContentAsset" as="element(asset)?">
    <xsl:param name="asset" as="element(asset)"/>
    <xsl:param name="language" as="xs:string"/>
    <xsl:variable name="relatedAssets" select="if (exists($asset/child_asset_rel[@key='user.main-content.'])) then $asset/cs:child-rel()[@key='user.main-content.'] else (if (exists($asset/storage_item[@key='master' and @mimetype='text/xml'])) then $asset else ())"/>
    <xsl:if test="exists($relatedAssets)">
      <xsl:choose>
        <!-- take first main content asset, if no language is defined -->
        <xsl:when test="empty($language)">
          <xsl:sequence select="$relatedAssets[1]"/>
        </xsl:when>
        <!-- take main content asset with correct language, if language is defined -->
        <xsl:when test="$relatedAssets/@language = $language">
          <xsl:sequence select="$relatedAssets[@language = $language][1]"/>
        </xsl:when>
        <!-- take first main content asset with no language, if no main content asset with languages exists -->
        <xsl:when test="$relatedAssets[not(@language)]">
          <xsl:sequence select="$relatedAssets[not(@language)][1]"/>
        </xsl:when>
      </xsl:choose>
    </xsl:if>
  </xsl:function>

  <!-- get URL of given render command element -->
  <xsl:function name="my:getUrlOfRenderCmd" as="xs:string">
    <xsl:param name="pdfResult" as="element(pdfresult)"/>
    <xsl:variable name="fileSystem" select="$pdfResult/@corpus:asset-temp-filesystem"/>
    <xsl:variable name="filePath" select="$pdfResult/@corpus:asset-temp-filepath"/>
    <xsl:value-of select="concat('censhare:///service/filesystem/', $fileSystem, '/', if (starts-with($filePath, 'file:')) then substring-after($filePath, 'file:') else '')"/>
  </xsl:function>

  <!-- get checked out version of given assets or current version, if not exists -->
  <xsl:function name="my:getCheckedOutInsideAsset" as="element(asset)?">
    <xsl:param name="asset" as="element(asset)?"/>
    <xsl:variable name="currentAsset" select="cs:get-asset($rootAsset/@id, 0, -2)"/>
    <xsl:choose>
      <xsl:when test="string($app) = 'contenteditor' and exists($currentAsset)"><xsl:copy-of select="$currentAsset"/></xsl:when>
      <xsl:otherwise><xsl:copy-of select="if (exists($asset) and $asset/@checked_out_inside_id = $rootAsset/@id) then cs:get-asset($asset/@id, 0, -2) else $asset"/></xsl:otherwise>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>
    






