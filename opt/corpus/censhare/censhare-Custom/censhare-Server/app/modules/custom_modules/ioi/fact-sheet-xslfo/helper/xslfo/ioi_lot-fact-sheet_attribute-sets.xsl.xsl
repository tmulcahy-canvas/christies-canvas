<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:my="http://www.censhare.com/xml/3.0.0/xpath-functions/my"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:html="http://www.w3.org/1999/xhtml">


  <xsl:param name="ul-label-1">•</xsl:param>
  <xsl:param name="ul-label-2">-</xsl:param>
  <xsl:param name="ul-label-3">o</xsl:param>
  <xsl:param name="ol-label-1">1.</xsl:param>
  <xsl:param name="ol-label-2">a.</xsl:param>
  <xsl:param name="ol-label-3">i.</xsl:param>

<!-- cc - Lot Sheet Chinese Specific Attribute Sets - cc -->
  <xsl:attribute-set name="par-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-size">9.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="b-par-chinese">
    <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="lot-num-and-suffix-chinese">
    <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="maker-date-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="title-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="article-titles-chinese">
    <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-size">4.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="sale-loc-number-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="first-line-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="pre-lot-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="artist-date-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="picture-medium-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="light-small-paragraph-chinese">
    <!-- rg This set is used by 'Estimates, Size, Quantity, Picture Size, Weight, Other Details, Quantity Description, and All Related Text Assets' rg -->
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <!-- rg Text Alignmeent Test rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="strong-em-chinese">
    <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
  </xsl:attribute-set>

  <!-- cc - Attribute sets converted to named templates to be used dynamically - cc -->
  <xsl:template match="xsl:template[@name=&apos;par-chinese&apos;]" name="par-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-size">9.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;b-par-chinese&apos;]" name="b-par-chinese">
      <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="font-size">9pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;lot-num-and-suffix-chinese&apos;]" name="lot-num-and-suffix-chinese">
      <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="font-size">8.5pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;maker-date-chinese&apos;]" name="maker-date-chinese">
      <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
      <xsl:attribute name="font-size">8pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;title-chinese&apos;]" name="title-chinese">
      <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
      <xsl:attribute name="font-size">8pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;article-titles-chinese&apos;]" name="article-titles-chinese">
      <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="font-size">4.5pt</xsl:attribute>
      <xsl:attribute name="space-after">0.75em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;sale-loc-number-chinese&apos;]" name="sale-loc-number-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;first-line-chinese&apos;]" name="first-line-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;pre-lot-chinese&apos;]" name="pre-lot-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;artist-date-chinese&apos;]" name="artist-date-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;picture-medium-chinese&apos;]" name="picture-medium-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;light-small-paragraph-chinese&apos;]" name="light-small-paragraph-chinese">
    <xsl:attribute name="font-family">FZLanTingHei</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;strong-em-chinese&apos;]" name="strong-em-chinese">
      <xsl:attribute name="font-family">FZLanTingHei-Bold</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
  </xsl:template>


  <!-- cc - Lot Sheet Specific Attribute Sets - cc -->
  <xsl:attribute-set name="par">
    <xsl:attribute name="font-family">AtlasGrotesk-Regular</xsl:attribute>
    <xsl:attribute name="font-size">9.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="b-par">
    <xsl:attribute name="font-family">AtlasGrotesk-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="symbol-converted">
    <xsl:attribute name="font-family">AtlasGrotesk-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="lot-num-and-suffix">
    <xsl:attribute name="font-family">AtlasGrotesk-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="maker-date">
    <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
    <xsl:attribute name="font-style">italic</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="title">
    <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
    <xsl:attribute name="font-style">italic</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="P">
    <xsl:attribute name="font-family">BakedPie</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="E">
    <xsl:attribute name="font-family">SwisExpertBT, VPPExpertNEW</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="Z">
    <xsl:attribute name="font-family">ZapfDingbatsSTD</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="article-titles">
    <xsl:attribute name="font-family">AtlasGrotesk-Bold</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="font-size">4.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="sale-loc-number">
    <xsl:attribute name="font-family">AtlasGrotesk-Medium</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="first-line">
    <xsl:attribute name="font-family">AtlasGrotesk-Medium</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="pre-lot">
    <xsl:attribute name="font-family">AtlasGrotesk-Regular</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <!-- cc -<xsl:attribute name="space-after">4.25pt</xsl:attribute>- cc -->
    <!-- cc -<xsl:attribute name="text-align">start</xsl:attribute>- cc -->
  </xsl:attribute-set>
  
  <xsl:attribute-set name="artist-date">
    <xsl:attribute name="font-family">AtlasGrotesk-Medium</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="picture-medium">
    <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="light-small-paragraph">
    <!-- rg This set is used by 'Estimates, Size, Quantity, Picture Size, Weight, Other Details, Quantity Description, and All Related Text Assets' rg -->
    <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <!-- rg Text Alignmeent Test rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="lot-detail-group">
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="main-image">
    <xsl:attribute name="width">170mm</xsl:attribute>
    <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    <xsl:attribute name="height">140mm</xsl:attribute>
    <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
    <xsl:attribute name="scaling">uniform</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="main-image-green">
    <xsl:attribute name="width">170mm</xsl:attribute>
    <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    <xsl:attribute name="height">140mm</xsl:attribute>
    <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
    <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">green</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">green</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">green</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">green</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="main-image-red">
    <xsl:attribute name="width">170mm</xsl:attribute>
    <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    <xsl:attribute name="height">140mm</xsl:attribute>
    <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
    <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">red</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">red</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">red</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">red</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="main-image-small">
    <xsl:attribute name="width">85mm</xsl:attribute>
    <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    <xsl:attribute name="height">70mm</xsl:attribute>
    <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
    <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="main-image-green-small">
    <xsl:attribute name="width">85mm</xsl:attribute>
    <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    <xsl:attribute name="height">70mm</xsl:attribute>
    <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
    <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">green</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">green</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">green</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">green</xsl:attribute>
    <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="main-image-red-small">
    <xsl:attribute name="width">85mm</xsl:attribute>
    <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    <xsl:attribute name="height">70mm</xsl:attribute>
    <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
    <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">red</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">red</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">red</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">red</xsl:attribute>
    <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="lot-sheet-logo">
    <xsl:attribute name="width">40mm</xsl:attribute>
    <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
    <xsl:attribute name="height">5mm</xsl:attribute>
    <xsl:attribute name="content-height">scale-to-fit</xsl:attribute> 
    <xsl:attribute name="scaling">uniform</xsl:attribute>
  </xsl:attribute-set>
  <!-- cc - Standard Attribute Sets - cc -->
  <xsl:attribute-set name="root">
    <xsl:attribute name="writing-mode">
      <xsl:value-of select="$writing-mode"/>
    </xsl:attribute>
    <xsl:attribute name="hyphenate">
      <xsl:value-of select="$hyphenate"/>
    </xsl:attribute>
    <xsl:attribute name="text-align">
      <xsl:value-of select="$text-align"/>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="page">
    <xsl:attribute name="page-width">
      <xsl:value-of select="$page-width"/>
    </xsl:attribute>
    <xsl:attribute name="page-height">
      <xsl:value-of select="$page-height"/>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="detail-container">
    <xsl:attribute name="margin-bottom">1em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="lot-sheet-image">
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="span">all</xsl:attribute>
    <xsl:attribute name="margin-bottom">1.89em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="page-header">
    <xsl:attribute name="font-size">small</xsl:attribute>
    <xsl:attribute name="text-align">left</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="page-footer">
    <xsl:attribute name="font-size">8pt</xsl:attribute>
    <xsl:attribute name="font-family">SourceSansPro</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="font-weight">italic</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="h1">
    <xsl:attribute name="font-size">28pt</xsl:attribute>
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
    <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="h2">
    <xsl:attribute name="font-size">12pt</xsl:attribute>
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <xsl:attribute name="space-before">0.83em</xsl:attribute>
    <xsl:attribute name="space-after">0.83em</xsl:attribute>
    <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
    <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="h3">
    <xsl:attribute name="font-size">1.17em</xsl:attribute>
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
    <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
    <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="h4">
    <xsl:attribute name="font-size">1em</xsl:attribute>
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <xsl:attribute name="space-before">1.17em</xsl:attribute>
    <xsl:attribute name="space-after">1.17em</xsl:attribute>
    <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
    <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="txt-details">
    <xsl:attribute name="font-size">0.7em</xsl:attribute>
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
    <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
    <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="body">
    <xsl:attribute name="font-family">SourceSansPro-Regular</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="titles">
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="span">all</xsl:attribute>
    <xsl:attribute name="margin-bottom">0.75em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="p">
    <xsl:attribute name="font-size">10.5pt</xsl:attribute>
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="p-initial" use-attribute-sets="p"/>
  <xsl:attribute-set name="p-initial-first" use-attribute-sets="p-initial">
    <!--  initial paragraph, first child of div, body or td  -->
  </xsl:attribute-set>
  <xsl:attribute-set name="blockquote">
    <xsl:attribute name="start-indent">inherited-property-value(start-indent) + 24pt</xsl:attribute>
    <xsl:attribute name="end-indent">inherited-property-value(end-indent) + 24pt</xsl:attribute>
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="footnote">
    <xsl:attribute name="vertical-align">super</xsl:attribute>
    <xsl:attribute name="font-size">8pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="pre">
    <xsl:attribute name="font-size">0.83em</xsl:attribute>
    <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
    <xsl:attribute name="white-space">pre</xsl:attribute>
    <xsl:attribute name="white-space-collapse">false</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="address">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="hr">
    <xsl:attribute name="border">1pt</xsl:attribute>
    <xsl:attribute name="font-size">9.5pt</xsl:attribute>
    <xsl:attribute name="border-color">#000000</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="space-before">0.67em</xsl:attribute>
    <xsl:attribute name="space-after">0.67em</xsl:attribute>
    <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="ul">
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ul-nested">
    <xsl:attribute name="space-before">0pt</xsl:attribute>
    <xsl:attribute name="space-after">0pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ol">
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ol-nested">
    <xsl:attribute name="space-before">0pt</xsl:attribute>
    <xsl:attribute name="space-after">0pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ul-li">
    <!--  for (unordered)fo:list-item  -->
    <xsl:attribute name="relative-align">baseline</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ol-li">
    <!--  for (ordered)fo:list-item  -->
    <xsl:attribute name="relative-align">baseline</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="dl">
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="dt">
    <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
    <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="dd">
    <xsl:attribute name="start-indent">inherited-property-value(start-indent) + 24pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ul-label-1">
    <xsl:attribute name="font">1em serif</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ul-label-2">
    <xsl:attribute name="font">0.67em SourceCodePro-Regular</xsl:attribute>
    <xsl:attribute name="baseline-shift">0.25em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ul-label-3">
    <xsl:attribute name="font">bold 0.9em sans-serif</xsl:attribute>
    <xsl:attribute name="baseline-shift">0.05em</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ol-label-1"/>
  <xsl:attribute-set name="ol-label-2"/>
  <xsl:attribute-set name="ol-label-3"/>
  <xsl:attribute-set name="inside-table">
    <!--  prevent unwanted inheritance  -->
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
    <xsl:attribute name="end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-indent">0pt</xsl:attribute>
    <xsl:attribute name="last-line-end-indent">0pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="text-align-last">relative</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="table-and-caption">
    <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="table">
    <xsl:attribute name="width">
      <xsl:value-of select="concat($column_content_width,&apos;mm&apos;)"/>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="table-caption" use-attribute-sets="inside-table">
    <xsl:attribute name="text-align">center</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="table-column"/>
  <xsl:attribute-set name="thead" use-attribute-sets="inside-table"/>
  <xsl:attribute-set name="tfoot" use-attribute-sets="inside-table"/>
  <xsl:attribute-set name="tbody" use-attribute-sets="inside-table"/>
  <xsl:attribute-set name="tr">
    <xsl:attribute name="space-after">20pt</xsl:attribute>
    <!-- rg <xsl:attribute name="keep-with-next.within-page">always</xsl:attribute> rg -->
  </xsl:attribute-set>
  <xsl:attribute-set name="th">
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="border">1px</xsl:attribute>
    <xsl:attribute name="padding">1px</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="td">
    <xsl:attribute name="border">1pt</xsl:attribute>
    <xsl:attribute name="border-color">#000000</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="padding">1px</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="error-processing-article">
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <xsl:attribute name="color">red</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="b">
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="strong">
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="strong-em">
    <xsl:attribute name="font-family">AtlasGrotesk-Bold-Italic</xsl:attribute>
    <xsl:attribute name="font-style">italic</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="i">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="cite">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="em">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="var">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="dfn">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="tt">
    <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="code">
    <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="kbd">
    <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="samp">
    <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="big">
    <xsl:attribute name="font-size">larger</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="small">
    <xsl:attribute name="font-size">smaller</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="sub">
    <xsl:attribute name="baseline-shift">sub</xsl:attribute>
    <xsl:attribute name="font-size">smaller</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="sup">
    <xsl:attribute name="baseline-shift">super</xsl:attribute>
    <xsl:attribute name="font-size">smaller</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="s">
    <xsl:attribute name="text-decoration">line-through</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="strike">
    <xsl:attribute name="text-decoration">line-through</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="del">
    <xsl:attribute name="text-decoration">line-through</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="u">
    <xsl:attribute name="text-decoration">underline</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="ins">
    <xsl:attribute name="text-decoration">underline</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="abbr"/>
  <xsl:attribute-set name="acronym"/>
  <xsl:attribute-set name="q"/>
  <xsl:attribute-set name="q-nested"/>
  <xsl:attribute-set name="img">
    <xsl:attribute name="relative-position">relative</xsl:attribute>
    <xsl:attribute name="content-width">scale-down-to-fit</xsl:attribute>
    <xsl:attribute name="content-height">scale-down-to-fit</xsl:attribute>
    <xsl:attribute name="width">
      <xsl:value-of select="concat($column_content_width,&apos;mm&apos;)"/>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="img-link">
    <xsl:attribute name="border">2px solid</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="img-inline">
    <xsl:attribute name="relative-position">relative</xsl:attribute>
    <xsl:attribute name="content-width">scale-down-to-fit</xsl:attribute>
    <xsl:attribute name="content-height">scale-down-to-fit</xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="a-link">
    <xsl:attribute name="text-decoration">underline</xsl:attribute>
    <xsl:attribute name="color">blue</xsl:attribute>
  </xsl:attribute-set>

  <!-- cc - Attribute sets converted to named templates to be used dynamically - cc -->
  <xsl:template match="xsl:template[@name=&apos;par&apos;]" name="par">
      <xsl:attribute name="font-size">9.5pt</xsl:attribute>
      <xsl:attribute name="space-after">0.75em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;b-par&apos;]" name="b-par">
      <xsl:attribute name="font-family">AtlasGrotesk-Bold</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="font-size">9pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;lot-num-and-suffix&apos;]" name="lot-num-and-suffix">
      <xsl:attribute name="font-family">AtlasGrotesk-Bold</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="font-size">8.5pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;maker-date&apos;]" name="maker-date">
      <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
      <xsl:attribute name="font-style">italic</xsl:attribute>
      <xsl:attribute name="font-size">8pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;title&apos;]" name="title">
      <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
      <xsl:attribute name="font-style">italic</xsl:attribute>
      <xsl:attribute name="font-size">8pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;P&apos;]" name="P">
    <!-- cc -<xsl:attribute name="font-family">BakedPie</xsl:attribute>- cc -->
    <xsl:attribute name="font-size">9pt</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;E&apos;]" name="E">
    <!-- cc -<xsl:attribute name="font-family">SwisExpertBT</xsl:attribute>- cc -->
    <xsl:attribute name="font-size">9pt</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;Z&apos;]" name="Z">
    <xsl:attribute name="font-family">AtlasGrotesk-Regular</xsl:attribute>
    <xsl:attribute name="font-size">9pt</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;article-titles&apos;]" name="article-titles">
      <xsl:attribute name="font-family">AtlasGrotesk-Bold</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
      <xsl:attribute name="font-size">4.5pt</xsl:attribute>
      <xsl:attribute name="space-after">0.75em</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;sale-loc-number&apos;]" name="sale-loc-number">
    <xsl:attribute name="font-family">AtlasGrotesk-Medium</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;first-line&apos;]" name="first-line">
    <xsl:attribute name="font-family">AtlasGrotesk-Medium</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;pre-lot&apos;]" name="pre-lot">
    <xsl:attribute name="font-family">AtlasGrotesk-Regular</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">8.5pt</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <!-- cc -<xsl:attribute name="space-after">4.25pt</xsl:attribute>- cc -->
    <!-- cc -<xsl:attribute name="text-align">start</xsl:attribute>- cc -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;artist-date&apos;]" name="artist-date">
    <xsl:attribute name="font-family">AtlasGrotesk-Medium</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">7.5pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;picture-medium&apos;]" name="picture-medium">
    <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;light-small-paragraph&apos;]" name="light-small-paragraph">
    <xsl:attribute name="font-family">AtlasGrotesk-Light</xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-size">6.75pt</xsl:attribute>
    <xsl:attribute name="space-after">0.75em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;lot-detail-group&apos;]" name="lot-detail-group">
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;main-image&apos;]" name="main-image">
      <xsl:attribute name="width">170mm</xsl:attribute>
      <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
      <xsl:attribute name="height">140mm</xsl:attribute>
      <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
      <xsl:attribute name="scaling">uniform</xsl:attribute>
      
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;main-image-green&apos;]" name="main-image-green">
      <xsl:attribute name="width">170mm</xsl:attribute>
      <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
      <xsl:attribute name="height">140mm</xsl:attribute>
      <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
      <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">green</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">green</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">green</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">green</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;main-image-red&apos;]" name="main-image-red">
      <xsl:attribute name="width">170mm</xsl:attribute>
      <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
      <xsl:attribute name="height">140mm</xsl:attribute>
      <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
      <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">red</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">red</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">red</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">red</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;main-image-small&apos;]" name="main-image-small">
      <xsl:attribute name="width">170mm</xsl:attribute>
      <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
      <xsl:attribute name="height">140mm</xsl:attribute>
      <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
      <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;main-image-green-small&apos;]" name="main-image-green-small">
      <xsl:attribute name="width">85mm</xsl:attribute>
      <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
      <xsl:attribute name="height">70mm</xsl:attribute>
      <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
      <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">green</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">green</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">green</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">green</xsl:attribute>
    <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;main-image-red-small&apos;]" name="main-image-red-small">
      <xsl:attribute name="width">85mm</xsl:attribute>
      <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
      <xsl:attribute name="height">70mm</xsl:attribute>
      <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
      <xsl:attribute name="scaling">uniform</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">thick</xsl:attribute>
    <xsl:attribute name="border-bottom-color">red</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thick</xsl:attribute>
    <xsl:attribute name="border-top-color">red</xsl:attribute>
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-left-width">thick</xsl:attribute>
    <xsl:attribute name="border-left-color">red</xsl:attribute>
    <xsl:attribute name="border-right-style">solid</xsl:attribute>
    <xsl:attribute name="border-right-width">thick</xsl:attribute>
    <xsl:attribute name="border-right-color">red</xsl:attribute>
    <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;lot-sheet-logo&apos;]" name="lot-sheet-logo">
      <xsl:attribute name="width">40mm</xsl:attribute>
      <!-- <xsl:attribute name="content-width">scale-to-fit</xsl:attribute> -->
      <xsl:attribute name="height">7mm</xsl:attribute>
      <xsl:attribute name="content-height">scale-to-fit</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;root&apos;]" name="root">
      <xsl:attribute name="writing-mode">
          <xsl:value-of select="$writing-mode"/>
      </xsl:attribute>
      <xsl:attribute name="hyphenate">
          <xsl:value-of select="$hyphenate"/>
      </xsl:attribute>
      <xsl:attribute name="text-align">
          <xsl:value-of select="$text-align"/>
      </xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;page&apos;]" name="page">
      <xsl:attribute name="page-width">
          <xsl:value-of select="$page-width"/>
      </xsl:attribute>
      <xsl:attribute name="page-height">
          <xsl:value-of select="$page-height"/>
      </xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;detail-container&apos;]" name="detail-container">
      <xsl:attribute name="margin-bottom">1em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;lot-sheet-image&apos;]" name="lot-sheet-image">
      <xsl:attribute name="text-align">center</xsl:attribute>
      <xsl:attribute name="span">all</xsl:attribute>
      <xsl:attribute name="margin-bottom">1.89em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;page-header&apos;]" name="page-header">
      <xsl:attribute name="font-size">small</xsl:attribute>
      <xsl:attribute name="text-align">left</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;page-footer&apos;]" name="page-footer">
      <xsl:attribute name="font-size">8pt</xsl:attribute>
      <xsl:attribute name="font-family">SourceSansPro</xsl:attribute>
      <xsl:attribute name="text-align">center</xsl:attribute>
      <xsl:attribute name="font-weight">italic</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;h1&apos;]" name="h1">
      <xsl:attribute name="font-size">28pt</xsl:attribute>
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
      <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
      <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;h2&apos;]" name="h2">
      <xsl:attribute name="font-size">12pt</xsl:attribute>
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
      <xsl:attribute name="space-before">0.83em</xsl:attribute>
      <xsl:attribute name="space-after">0.83em</xsl:attribute>
      <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
      <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;h3&apos;]" name="h3">
      <xsl:attribute name="font-size">1.17em</xsl:attribute>
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
      <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
      <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;h4&apos;]" name="h4">
      <xsl:attribute name="font-size">1em</xsl:attribute>
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
      <xsl:attribute name="space-before">1.17em</xsl:attribute>
      <xsl:attribute name="space-after">1.17em</xsl:attribute>
      <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
      <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;txt-details&apos;]" name="txt-details">
      <xsl:attribute name="font-size">0.7em</xsl:attribute>
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
      <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;body&apos;]" name="body">
      <xsl:attribute name="font-family">SourceSansPro-Regular</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;titles&apos;]" name="titles">
      <xsl:attribute name="text-align">center</xsl:attribute>
      <xsl:attribute name="span">all</xsl:attribute>
      <xsl:attribute name="margin-bottom">2em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;p&apos;]" name="p">
      <xsl:attribute name="font-size">10.5pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;p-initial&apos;]" name="p-initial">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;p-initial-first&apos;]" name="p-initial-first">
      <!--  initial paragraph, first child of div, body or td  -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;blockquote&apos;]" name="blockquote">
      <xsl:attribute name="start-indent">inherited-property-value(start-indent) + 24pt</xsl:attribute>
      <xsl:attribute name="end-indent">inherited-property-value(end-indent) + 24pt</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;footnote&apos;]" name="footnote">
      <xsl:attribute name="vertical-align">super</xsl:attribute>
      <xsl:attribute name="font-size">8pt</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;pre&apos;]" name="pre">
      <xsl:attribute name="font-size">0.83em</xsl:attribute>
      <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
      <xsl:attribute name="white-space">pre</xsl:attribute>
      <xsl:attribute name="white-space-collapse">false</xsl:attribute>
      <xsl:attribute name="wrap-option">wrap</xsl:attribute>
      <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;address&apos;]" name="address">
      <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;hr&apos;]" name="hr">
      <xsl:attribute name="border">1pt</xsl:attribute>
      <xsl:attribute name="font-size">9.5pt</xsl:attribute>
      <xsl:attribute name="border-color">#000000</xsl:attribute>
      <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
      <xsl:attribute name="space-before">0.67em</xsl:attribute>
      <xsl:attribute name="space-after">0.67em</xsl:attribute>
      <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ul&apos;]" name="ul">
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ul-nested&apos;]" name="ul-nested">
      <xsl:attribute name="space-before">0pt</xsl:attribute>
      <xsl:attribute name="space-after">0pt</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ol&apos;]" name="ol">
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ol-nested&apos;]" name="ol-nested">
      <xsl:attribute name="space-before">0pt</xsl:attribute>
      <xsl:attribute name="space-after">0pt</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ul-li&apos;]" name="ul-li">
      <!--  for (unordered)fo:list-item  -->
      <xsl:attribute name="relative-align">baseline</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ol-li&apos;]" name="ol-li">
      <!--  for (ordered)fo:list-item  -->
      <xsl:attribute name="relative-align">baseline</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;dl&apos;]" name="dl">
      <xsl:attribute name="space-before">1em</xsl:attribute>
      <xsl:attribute name="space-after">1em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;dt&apos;]" name="dt">
      <!-- rg <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute> rg -->
      <!-- rg <xsl:attribute name="keep-together.within-column">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;dd&apos;]" name="dd">
      <xsl:attribute name="start-indent">inherited-property-value(start-indent) + 24pt</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ul-label-1&apos;]" name="ul-label-1">
      <xsl:attribute name="font">1em serif</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ul-label-2&apos;]" name="ul-label-2">
      <xsl:attribute name="font">0.67em SourceCodePro-Regular</xsl:attribute>
      <xsl:attribute name="baseline-shift">0.25em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ul-label-3&apos;]" name="ul-label-3">
      <xsl:attribute name="font">bold 0.9em sans-serif</xsl:attribute>
      <xsl:attribute name="baseline-shift">0.05em</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ol-label-1&apos;]" name="ol-label-1">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ol-label-2&apos;]" name="ol-label-2">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ol-label-3&apos;]" name="ol-label-3">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;inside-table&apos;]" name="inside-table">
      <!--  prevent unwanted inheritance  -->
      <xsl:attribute name="start-indent">0pt</xsl:attribute>
      <xsl:attribute name="end-indent">0pt</xsl:attribute>
      <xsl:attribute name="text-indent">0pt</xsl:attribute>
      <xsl:attribute name="last-line-end-indent">0pt</xsl:attribute>
      <xsl:attribute name="text-align">start</xsl:attribute>
      <xsl:attribute name="text-align-last">relative</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;table-and-caption&apos;]" name="table-and-caption">
      <xsl:attribute name="display-align">center</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;table&apos;]" name="table">
      <xsl:attribute name="width">
          <xsl:value-of select="concat($column_content_width,&apos;mm&apos;)"/>
      </xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;table-caption&apos;]" name="table-caption">
      <xsl:attribute name="text-align">center</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;table-column&apos;]" name="table-column">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;thead&apos;]" name="thead">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;tfoot&apos;]" name="tfoot">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;tbody&apos;]" name="tbody">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;tr&apos;]" name="tr">
      <xsl:attribute name="space-after">20pt</xsl:attribute>
      <!-- rg <xsl:attribute name="keep-with-next.within-page">always</xsl:attribute> rg -->
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;th&apos;]" name="th">
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
      <xsl:attribute name="text-align">center</xsl:attribute>
      <xsl:attribute name="border">1px</xsl:attribute>
      <xsl:attribute name="padding">1px</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;td&apos;]" name="td">
      <xsl:attribute name="border">1pt</xsl:attribute>
      <xsl:attribute name="border-color">#000000</xsl:attribute>
      <xsl:attribute name="border-right-style">solid</xsl:attribute>
      <xsl:attribute name="border-left-style">solid</xsl:attribute>
      <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
      <xsl:attribute name="border-top-style">solid</xsl:attribute>
      <xsl:attribute name="padding">1px</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;error-processing-article&apos;]" name="error-processing-article">
    <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
    <xsl:attribute name="color">red</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;b&apos;]" name="b">
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;strong&apos;]" name="strong">
      <xsl:attribute name="font-family">SourceSansPro-Bold</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;strong-em&apos;]" name="strong-em">
      <xsl:attribute name="font-family">AtlasGrotesk-Bold-Italic</xsl:attribute>
      <xsl:attribute name="font-style">italic</xsl:attribute>
      <xsl:attribute name="font-weight">bold</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;i&apos;]" name="i">
      <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;cite&apos;]" name="cite">
      <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;em&apos;]" name="em">
      <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;var&apos;]" name="var">
      <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;dfn&apos;]" name="dfn">
      <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;tt&apos;]" name="tt">
      <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;code&apos;]" name="code">
      <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;kbd&apos;]" name="kbd">
      <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;samp&apos;]" name="samp">
      <xsl:attribute name="font-family">SourceCodePro-Regular</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;big&apos;]" name="big">
      <xsl:attribute name="font-size">larger</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;small&apos;]" name="small">
      <xsl:attribute name="font-size">smaller</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;sub&apos;]" name="sub">
      <xsl:attribute name="baseline-shift">sub</xsl:attribute>
      <xsl:attribute name="font-size">smaller</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;sup&apos;]" name="sup">
      <xsl:attribute name="baseline-shift">super</xsl:attribute>
      <xsl:attribute name="font-size">smaller</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;s&apos;]" name="s">
      <xsl:attribute name="text-decoration">line-through</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;strike&apos;]" name="strike">
      <xsl:attribute name="text-decoration">line-through</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;del&apos;]" name="del">
      <xsl:attribute name="text-decoration">line-through</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;u&apos;]" name="u">
      <xsl:attribute name="text-decoration">underline</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;ins&apos;]" name="ins">
      <xsl:attribute name="text-decoration">underline</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;abbr&apos;]" name="abbr">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;acronym&apos;]" name="acronym">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;q&apos;]" name="q">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;q-nested&apos;]" name="q-nested">
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;img&apos;]" name="img">
      <xsl:attribute name="relative-position">relative</xsl:attribute>
      <xsl:attribute name="content-width">scale-down-to-fit</xsl:attribute>
      <xsl:attribute name="content-height">scale-down-to-fit</xsl:attribute>
      <xsl:attribute name="width">
          <xsl:value-of select="concat($column_content_width,&apos;mm&apos;)"/>
      </xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;img-link&apos;]" name="img-link">
      <xsl:attribute name="border">2px solid</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;img-inline&apos;]" name="img-inline">
      <xsl:attribute name="relative-position">relative</xsl:attribute>
      <xsl:attribute name="content-width">scale-down-to-fit</xsl:attribute>
      <xsl:attribute name="content-height">scale-down-to-fit</xsl:attribute>
  </xsl:template>
  <xsl:template match="xsl:template[@name=&apos;a-link&apos;]" name="a-link">
      <xsl:attribute name="text-decoration">underline</xsl:attribute>
      <xsl:attribute name="color">blue</xsl:attribute>
  </xsl:template>

</xsl:stylesheet>
