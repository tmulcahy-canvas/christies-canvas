<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet exclude-result-prefixes="xs html censhare cs io ioi" version="2.0" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:ioi="http://www.iointegration.com" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" indent="yes" method="xml" version="1.0"/>
    <xsl:param name="censhare:command"/>
    <xsl:template match="asset">

        <xsl:variable name="propertyIds">
            <root>
                <xsl:apply-templates select="./cs:child-rel()" mode="gather-children"/>
            </root>
        </xsl:variable>


        <xsl:variable name="stringJoinValues" select="string-join(distinct-values($propertyIds/root/asset/@id), ',')"/>

        <query xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xml:space="preserve">
            <and>
                <condition name="censhare:asset.type" value="picture.*"/>
                <condition name="censhare:asset.id" op="IN" sepchar="," value="{$stringJoinValues}"/>
            </and>
            <sortorders>
                <grouping mode="none"/>
                <order ascending="true" by="censhare:asset.name"/>
            </sortorders>
        </query>

    </xsl:template>

    <xsl:template match="asset[starts-with(@type, 'picture.')]" mode="gather-children">
        <xsl:variable name="wf-id" select="./@wf_id"/>
        <xsl:variable name="wf-name" select="cs:master-data('workflow')[@id=$wf-id]/@name"/>
        <xsl:variable name="wf-step-id" select="./@wf_step"/>
        <xsl:variable name="wf-step-name" select="cs:master-data('workflow_step')[@wf_id = $wf-id and @wf_step = $wf-step-id]/@name"/>
        <xsl:if test="$wf-step-name != 'Awaiting Image'">
            <asset id="{./@id}"/>
        </xsl:if>
    </xsl:template>

    <!-- cc - added template to recursively search asset tree for images - cc -->
    <xsl:template match="asset[not(starts-with(@type, 'picture.'))]" mode="gather-children">
        <xsl:apply-templates select="./cs:child-rel()" mode="gather-children"/>
    </xsl:template>
</xsl:stylesheet>