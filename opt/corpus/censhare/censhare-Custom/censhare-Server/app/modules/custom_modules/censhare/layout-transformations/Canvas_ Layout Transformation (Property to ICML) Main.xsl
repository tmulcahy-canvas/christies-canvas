<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="#all" version="2.0" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:my="http://www.censhare.com/xml/3.0.0/my-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:strip-space elements="*"/>
	
	<xsl:param name="transform" as="element(transform)?">
		<transform target-asset-id="220370"/>
	</xsl:param>

	<xsl:template match="asset">

		<!-- Aggregate Content -->
		<xsl:variable name="propertyInformation"/>
		<cs:command name="com.censhare.api.transformation.XslTransformation" returning="propertyInformation">
			<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.content-aggregator/storage/master/file'"/>
			<cs:param name="source" select="."/>
			<cs:param name="xsl-parameters">
				<cs:param name="params" select="$transform"/>
			</cs:param>
		</cs:command>
		
		<!-- 
        	Get Template according to object format of property
        -->
		<xsl:variable name="template">
			<xsl:choose>
				<!-- Applied Arts -->
				<xsl:when test="$propertyInformation/property/metadata[@type = 'objectformat']/@value = '2'">
					<xsl:copy-of select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.object-format.applied-art/storage/master/file')"/>
				</xsl:when>
				<!-- Pictures -->
				<xsl:when test="$propertyInformation/property/metadata[@type = 'objectformat']/@value = '3'">
					<xsl:copy-of select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.object-format.pictures/storage/master/file')"/>
				</xsl:when>
				<!-- Jewelry -->
				<xsl:when test="$propertyInformation/property/metadata[@type = 'objectformat']/@value = '4'">
					<xsl:copy-of select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.object-format.jewelry/storage/master/file')"/>
				</xsl:when>
				<!-- Books -->
				<xsl:when test="$propertyInformation/property/metadata[@type = 'objectformat']/@value = '5'">
					<xsl:copy-of select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.object-format.books/storage/master/file')"/>
				</xsl:when>
				<!-- General -->
				<xsl:when test="$propertyInformation/property/metadata[@type = 'objectformat']/@value = '6'">
					<xsl:copy-of select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.object-format.general/storage/master/file')"/>
				</xsl:when>
				<!-- Wine -->
				<xsl:when test="$propertyInformation/property/metadata[@type = 'objectformat']/@value = '7'">
					<xsl:copy-of select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.object-format.wine/storage/master/file')"/>
				</xsl:when>
				<!-- FALLBACK -->
				<xsl:otherwise>
					<xsl:copy-of select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.object-format.general/storage/master/file')"/>
				</xsl:otherwise>
			</xsl:choose>			
		</xsl:variable>

		<!-- Transform Template into Standard Structure using aggregated content-->
		<xsl:variable name="tempStructure"/>
		<cs:command name="com.censhare.api.transformation.XslTransformation" returning="tempStructure">
			<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.content-transformer/storage/master/file'"/>
			<cs:param name="source" select="$template"/>
			<cs:param name="xsl-parameters">
				<cs:param name="propertyContent" select="$propertyInformation/property"/>
			</cs:param>
		</cs:command>
		
		<!-- Render as ICML -->
		<cs:command name="com.censhare.api.transformation.XslTransformation">
			<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.content-renderer/storage/master/file'"/>
			<cs:param name="source" select="$tempStructure"/>
		</cs:command>
	</xsl:template>

</xsl:stylesheet>
