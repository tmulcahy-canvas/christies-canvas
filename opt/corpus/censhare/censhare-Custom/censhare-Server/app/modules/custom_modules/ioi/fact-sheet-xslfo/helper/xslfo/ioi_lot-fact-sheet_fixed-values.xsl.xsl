<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:my="http://www.censhare.com/xml/3.0.0/xpath-functions/my"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:html="http://www.w3.org/1999/xhtml">

  <!-- Fixed Values -->
  <!--  page size  -->
  <xsl:variable name="page-width">210mm</xsl:variable>
  <xsl:variable name="page-height">297mm</xsl:variable>
  <xsl:variable name="page-margin-top">17mm</xsl:variable>
  <xsl:variable name="page-margin-bottom">17mm</xsl:variable>
  <xsl:variable name="page-margin-left">15mm</xsl:variable>
  <xsl:variable name="page-margin-right">15mm</xsl:variable>
  <!--  page header and footer  -->
  <xsl:variable name="page-header-margin">10mm</xsl:variable>
  <xsl:variable name="page-footer-margin">10mm</xsl:variable>
  <xsl:variable name="title-print-in-header">false</xsl:variable>
  <xsl:variable name="page-number-print-in-footer">true</xsl:variable>
  <!--  multi column  -->
  <xsl:variable name="column-count">2</xsl:variable>
  <xsl:variable name="column-gap">8mm</xsl:variable>
  <!--  writing-mode: lr-tb | rl-tb | tb-rl  default is left to right and top to bottom-->
  <xsl:variable name="writing-mode">lr-tb</xsl:variable>
  <!--  text-align: justify | start  -->
  <xsl:variable name="text-align">justify</xsl:variable>
  <!--  hyphenate: true | false  -->
  <xsl:variable name="hyphenate">true</xsl:variable>
  <!-- Image and Table containers are always scaled up to the maximum row width -->
  <xsl:variable name="column_content_width" select="my:get_column_width()"/>

  <xsl:function name="my:get_value_as_number">
    <xsl:param name="input"/>
    <xsl:value-of select="number(substring-before($input, 'mm'))"/>
  </xsl:function>

  <xsl:function name="my:get_column_width">
    <xsl:value-of select="(my:get_value_as_number($page-width) - my:get_value_as_number($page-margin-left) - my:get_value_as_number($page-margin-right) - ($column-count * my:get_value_as_number($column-gap))) div $column-count"/>
  </xsl:function>
  
</xsl:stylesheet>