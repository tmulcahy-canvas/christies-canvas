<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">
  
  <xsl:param name="transform"/>
  
  <!-- Defines a search for the property primary image -->
  <xsl:template match="asset">
    <assets>
      <!-- debug -->
      <xsl:message><xsl:value-of select="concat('call ''canvas:search-property-primary-picture'' with asset ''', @name, ''' (ID: ', @id, ')')"/></xsl:message>      
      <!-- search -->    
      <xsl:variable name="primary-image" select="./cs:child-rel()[@key='user.main-picture.']/cs:asset()[@censhare:asset.type = 'picture.']" as="element(asset)*"/>      
      <xsl:if test="$primary-image">
        <xsl:copy-of select="$primary-image" />      		
      </xsl:if>
    </assets>
  </xsl:template>
  
</xsl:stylesheet>
