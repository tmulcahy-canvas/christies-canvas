<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:my="http://www.censhare.com/xml/3.0.0/xpath-functions/my" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:ioi="http://www.iointegration.com">
    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no"/>
    <!-- cc - Fixed Values - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:lot-fact-sheet-xslfo.fixed-values/storage/master/file"/>
    <!-- cc- Attribute Sets - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:lot-fact-sheet-xslfo.attribute-sets/storage/master/file"/>
    <!-- cc - Standard Article Templates - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:standard-article-templates/storage/master/file"/>
    <!-- cc - Property Text Handling Templates - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:process-property-text-feature-data/storage/master/file"/>
    <xsl:param name="languages"/>
    <xsl:param name="use-external-condition"/>
    <xsl:param name="use-image-status"/>
    <xsl:param name="excludes"/>
    <!-- cc - String for calling text extraction routine - cc -->
    <xsl:variable name="text-extraction-url" select="'censhare:///service/assets/asset;censhare:resource-key=ioi:gather-property-text-feature-data-with-languages/transform;key=ioi:gather-property-text-feature-data-with-languages;asset-id='"/>
    <!-- cc - GLOBAL FACT SHEET VARIABLES - cc -->
    <xsl:variable name="lot-fact-sheet-footer" select="'Other fees apply in addition to the hammer price- see the Conditions of Sale at the back of the Sale Catalogue.'"/>
    <!-- cc - templates stored so attribute sets can be applied dynamically - cc -->
    <xsl:variable name="templates">
        <xsl:variable name="content"/>
        <cs:command name="com.censhare.api.io.ReadXML" returning="content">
            <cs:param name="source" select="cs:asset()[@censhare:resource-key = 'ioi:lot-fact-sheet-xslfo.attribute-sets']/storage_item[@key='master'][1]"/>
        </cs:command>
        <xsl:copy-of select="$content"/>
    </xsl:variable>
    <xsl:template match="/">
        <xsl:variable name="rg-test" >
            <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xsl:use-attribute-sets="root">
                <xsl:call-template name="make-layout-master-set"/>
                <fo:page-sequence master-reference="all-pages">
                    <fo:static-content flow-name="page-footer">
                        <fo:block space-after.conditionality="retain" space-after="{$page-footer-margin}" xsl:use-attribute-sets="page-footer">
                            <xsl:if test="$page-number-print-in-footer = 'true'">
                                <xsl:value-of select="$lot-fact-sheet-footer"/>
                            </xsl:if>
                        </fo:block>
                    </fo:static-content>
                    <fo:flow flow-name="xsl-region-body">
                        <xsl:apply-templates/>
                    </fo:flow>
                </fo:page-sequence>
            </fo:root>
        </xsl:variable>
        <xsl:copy-of select="$rg-test" />
    </xsl:template>
    <xsl:template name="make-layout-master-set">
        <fo:layout-master-set>
            <fo:simple-page-master master-name="all-pages" xsl:use-attribute-sets="page">
                <fo:region-body margin-top="{$page-margin-top}" margin-right="{$page-margin-right}" margin-bottom="{$page-margin-bottom}" margin-left="{$page-margin-left}"/>
                <fo:region-before region-name="page-header" extent="{$page-margin-top}" display-align="before"/>
                <fo:region-after region-name="page-footer" extent="{$page-margin-bottom}" display-align="after"/>
                <fo:region-start extent="{$page-margin-left}"/>
                <fo:region-end extent="{$page-margin-bottom}"/>
            </fo:simple-page-master>
        </fo:layout-master-set>
    </xsl:template>
    <xsl:template match="asset">
        <xsl:variable name="asset-xml" select="."/>
        <!-- cc - Asset Feature Data Variables - cc -->
        <xsl:variable name="extracted-feature-data" select="doc(concat($text-extraction-url, ./@id, ';languages=',  $languages))"/>
        <!-- cc - LOGO - cc -->
        <fo:block span="all">
            <xsl:apply-templates select="cs:asset()[@censhare:resource-key = 'ioi:chr-logo-for-xslfo-png']" mode="process-logo"/>
        </fo:block>
        <!-- cc - Header Section - cc -->
        <fo:block span="all">
            <xsl:apply-templates select="$extracted-feature-data/data/feature[@position = 'header']" mode="language-header">
                <xsl:with-param name="asset-xml" select="$asset-xml"/>
            </xsl:apply-templates>
        </fo:block>
        <!-- cc - MAIN IMAGE - cc -->
        <fo:block xsl:use-attribute-sets="lot-sheet-image" span="all">
            <xsl:variable name="image-status-from-command" select="$use-image-status" as="xs:string"/>
            <xsl:variable name="img-status" select="if($image-status-from-command = '') 
                then 
                    asset_feature[@feature='canvas:image-status']/@value_long
                else 
                    $image-status-from-command" as="xs:string"/>
            <xsl:variable name="asset-id-max" select="round(max(./cs:child-rel()[@key='user.main-picture.']/@id))" />
            <xsl:if test="$asset-id-max">
                <xsl:apply-templates select="cs:asset()[@censhare:asset.id=$asset-id-max]" mode="process-image">
                    <xsl:with-param name="image-status" select="$img-status"/>
                </xsl:apply-templates>
            </xsl:if>
        </fo:block>
        <!-- cc - First Section - cc -->
        <xsl:variable name="first-section">
            <fo:block span="all">
                <fo:table>
                    <fo:table-column column-width="48.5%"/>
                    <fo:table-column column-width="3%"/>
                    <fo:table-column column-width="48.5%"/>
                    <fo:table-body>
                        <xsl:apply-templates select="$extracted-feature-data/data/feature[@position = 'first']" mode="table-row">
                            <xsl:with-param name="asset-xml" select="$asset-xml"/>
                        </xsl:apply-templates>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </xsl:variable>
        <xsl:if test="exists($first-section/fo:block/fo:table/fo:table-body/fo:table-row/node())">
            <xsl:copy-of select="$first-section"/>
        </xsl:if>
        <!-- nh Lot Symbol Converted nh -->
        <fo:block span="all">
            <xsl:variable name="feature-to-pass">
                <xsl:for-each select="$extracted-feature-data/data/feature[@position='converted']">
                    <feature>
                        <xsl:copy-of select="./@*" />
                        <xsl:copy-of select="./value[1]" />
                    </feature>
                </xsl:for-each>
            </xsl:variable>
            <xsl:apply-templates select="$feature-to-pass" mode="converted">
                <xsl:with-param name="asset-xml" select="$asset-xml"/>
            </xsl:apply-templates>
        </fo:block>
        <!-- cc - Second Section - cc -->
        /
        <xsl:variable name="second-section">
            <fo:block span="all">
                <fo:table>
                    <fo:table-column column-width="48.5%"/>
                    <fo:table-column column-width="3%"/>
                    <fo:table-column column-width="48.5%"/>
                    <fo:table-body>
                        <xsl:apply-templates select="$extracted-feature-data/data/feature[@position = 'second']" mode="table-row">
                            <xsl:with-param name="asset-xml" select="$asset-xml"/>
                        </xsl:apply-templates>
                    </fo:table-body>
                </fo:table>
            </fo:block>
        </xsl:variable>
        <xsl:if test="exists($second-section/fo:block/fo:table/fo:table-body/fo:table-row/node())">
            <xsl:copy-of select="$second-section"/>
        </xsl:if>
    </xsl:template>
    <xsl:template match="asset" mode="process-image">
        <xsl:param name="image-status"/>
        <xsl:variable name="wf-id" select="@wf_id"/>
        <xsl:variable name="wf-step" select="@wf_step"/>
        <xsl:variable name="wf-step-name" select="cs:master-data('workflow_step')[@wf_id = $wf-id and @wf_step = $wf-step]/@name" />
        <xsl:choose>
            <!-- cc - canvas:image-status - cc -->
            <xsl:when test="$image-status = '1' or $image-status = 'true'">
                <xsl:choose>
                    <xsl:when test="$wf-step-name = 'Passed4Print'">
                        <fo:external-graphic xsl:use-attribute-sets="main-image-green">
                            <xsl:attribute name="src">
                                <xsl:text>url('</xsl:text>
                                <xsl:value-of select="storage_item[@key='preview']/@url"/>
                                <xsl:text>')</xsl:text>
                            </xsl:attribute>
                        </fo:external-graphic>
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:external-graphic xsl:use-attribute-sets="main-image-red">
                            <xsl:attribute name="src">
                                <xsl:text>url('</xsl:text>
                                <xsl:value-of select="storage_item[@key='preview']/@url"/>
                                <xsl:text>')</xsl:text>
                            </xsl:attribute>
                        </fo:external-graphic>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <fo:external-graphic xsl:use-attribute-sets="main-image">
                    <xsl:attribute name="src">
                        <xsl:text>url('</xsl:text>
                        <xsl:value-of select="storage_item[@key='preview']/@url"/>
                        <xsl:text>')</xsl:text>
                    </xsl:attribute>
                </fo:external-graphic>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="asset" mode="process-logo">
        <fo:external-graphic xsl:use-attribute-sets="lot-sheet-logo">
            <xsl:attribute name="src">
                <xsl:text>url('</xsl:text>
                <xsl:value-of select="storage_item[@key='preview']/@url"/>
                <xsl:text>')</xsl:text>
            </xsl:attribute>
        </fo:external-graphic>
    </xsl:template>
</xsl:stylesheet>
