<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" 
  xmlns:censhare="http://www.censhare.com/xml/3.0.0/corpus" 
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
  exclude-result-prefixes="xs corpus censhare cs" version="1.0">

  <xsl:variable name="chart-type" select="'pie'" as="xs:string"/>
  <xsl:variable name="chart-legend" select="false()" as="xs:boolean"/>
  <xsl:variable name="chart-options" select="'labelType: percent;'" as="xs:string"/>

  <xsl:variable name="child-asset-type" select="'product.'" as="xs:string"/>
  <xsl:variable name="match-label" select="'Has primary image.'"/>
  <xsl:variable name="no-match-label" select="'No primary image.'"/>
  <xsl:variable name="no-match-with-image-label" select="'No primary image but has image assigned.'"/>

  <xsl:variable name="wf" select="'Image'"/>
  <xsl:variable name="wfs" select="'Awaiting Image'"/>
  <xsl:variable name="wfid" select="cs:master-data('workflow')[@name=$wf]/@id"/>
  <xsl:variable name="wfsid" select="cs:master-data('workflow_step')[@wf_id=$wfid and @name=$wfs]/@wf_step"/>

  <xsl:template match="asset">

    <xsl:variable name="children">
      <child-assets>
        <xsl:apply-templates select="./cs:child-rel()[@key='*']/.[@type=$child-asset-type]" mode="gather-children"/>
      </child-assets>
    </xsl:variable>

    <xsl:variable name="children-match-count" select="count($children/child-assets/child-asset[@main-image='1'])"/>
    <xsl:variable name="children-no-match-count" select="count($children/child-assets/child-asset[@main-image=0])"/>
    <xsl:variable name="children-no-match-with-image" select="count($children/child-assets/child-asset[@main-image='2'])"/>

    <xsl:variable name="title" select="'Properties with primary image.'" as="xs:string"/>
    <xsl:variable name="sub-title" select="concat($children-match-count, ' of ', count($children/child-assets/child-asset), ' properties have a primary image.')" as="xs:string"/>

    <result>
      <xsl:if test="$title">
        <title>
          <xsl:value-of select="$title"/>
        </title>
      </xsl:if>
      <subTitle>
        <xsl:value-of select="$sub-title"/>
      </subTitle>
      <type>pie</type>
      <legend censhare:_annotation.datatype="boolean">false</legend>
      <labelType censhare:_annotation.datatype="string">value</labelType>
      <showToolTipPercent censhare:_annotation.datatype="boolean">true</showToolTipPercent>
      <valueFormat censhare:_annotation.datatype="string">d3.format('.0f')</valueFormat>
      <noData censhare:_annotation.datatype="string">There is no data to display</noData>
      <data censhare:_annotation.arraygroup="true">
        <values>
          <!-- cc - NO PRIMARY IMAGE - RED - cc -->
          <key><xsl:value-of select="$no-match-label"/></key>
          <color>
            <xsl:value-of select="'#ff4e4f'"/>
          </color>
          <value censhare:_annotation.datatype="number"><xsl:value-of select="$children-no-match-count"/></value>
          <query>
            <ids censhare:_annotation.datatype="string"><xsl:value-of select="string-join($children/child-assets/child-asset[@main-image='0']/@asset-id, ',')"/></ids>
          </query>
        </values>
        <values>
          <!-- cc - HAS PRIMARY IMAGE - GREEN - cc -->
          <key><xsl:value-of select="$match-label"/></key>
          <color>
            <xsl:value-of select="'#6ae3a7'"/>
          </color>
          <value censhare:_annotation.datatype="number"><xsl:value-of select="$children-match-count"/></value>
          <query>
            <ids censhare:_annotation.datatype="string"><xsl:value-of select="string-join($children/child-assets/child-asset[@main-image='1']/@asset-id, ',')"/></ids>
          </query>
        </values>
      </data>
    </result>
  </xsl:template>

  <!-- cc - template applied to all children of contextual asset of a specific type
            Specific business logic can be applied here...Currently this will return
            a node called child-asset and an attribute containing a boolean value
            that signifies whether the asset in context has a related image asset with
            the relation type of 'user.main-picture.'
  - cc -->
  <xsl:template match="asset" mode="gather-children">
    <child-asset asset-id="{@id}" main-image="{if(exists(./cs:child-rel()[@key='user.main-picture.']/.[@wf_id=$wfid and @wf_step != $wfsid])) then '1' else '0'}"/>
  </xsl:template>

</xsl:stylesheet>
