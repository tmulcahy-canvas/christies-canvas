<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
	exclude-result-prefixes="xsl xs cs"
>
<!-- #########################################################################

	mwe@censhare.de

	version 1.1

##############################################################################

	include with:
	<xsl:include href="censhare:///service/assets/asset;censhare:resource-key=lib:assetmanagement/storage/master/file" />

	don't forget to set variable "VERBOSE"
	<xsl:variable name="VERBOSE" select="false()" />

##############################################################################

	EXAMPLEs

#### load asset-xml or create template ###
<xsl:variable name="_asset_loaded"
	select="cs:ASSET_getOrCreateByExternalID( 'foo:bar-test', 'Name of new Asset', 'text.', 'root.', 'default' )"
/>
<xsl:message>ASSET_management_TEST.extract_dir.asset_loaded: <xsl:copy-of select="$_asset_loaded" /></xsl:message>


#### update asset-xml ###
<xsl:variable name="_asset_updated">
	<asset
		name="{$_asset_loaded/@name}."
	>
		#### copy attributes ###
		<xsl:copy-of select="$_asset_loaded/@*[not(name()='name')]" />

		#### copy all nodes ###
		<xsl:copy-of select="$_asset_loaded/node()" />
		#### or only nodes we dont want to change ###
		<xsl:copy-of select="$_asset_loaded/node()[
			    not(local-name(.)='storage_item')
			and not(local-name(.)='asset_element')
			and not(@feature='my:feature')
		]" />

		#### link asset to some other asset (if relation not exists!)
		<xsl:if test="not($_asset_loaded/parent_asset_rel[@key='user.' and @parent_asset=$_parent_asset_id])">
			<parent_asset_rel
				key="user."
				parent_asset="{$_parent_asset_id}"
			/>
		</xsl:if>

	</asset>
</xsl:variable>

#### update asset-xml (set relation example) ###
<xsl:variable name="_asset_updated">
	<asset
		name="{$_asset_loaded/@name} - Edit"
	>
		<xsl:copy-of select="$_asset_loaded/@*[not(name()='name')]" />
		<xsl:copy-of select="$_asset_loaded/node()[not(
			    local-name(.)='parent_asset_rel'
			and @key='user.'
			and @parent_asset=$_parent_asset_id
		)]" />
		<xsl:if test="not($_asset_loaded/parent_asset_rel[@key='user.' and @parent_asset=$_parent_asset_id])">
			<parent_asset_rel
				key="user.edit."
				parent_asset="{$_parent_asset_id}"
			/>
		</xsl:if>
	</asset>
</xsl:variable>

<xsl:message>ASSET_management_TEST.extract_dir.asset_updated: <xsl:copy-of select="$_asset_updated" /></xsl:message>


#### write asset-xml back into system ###
<xsl:variable name="_asset_saved"
	select="cs:ASSET_insertOrUpdate( $_asset_updated )"
/>
<xsl:message>ASSET_management_TEST.extract_dir.asset_saved: <xsl:copy-of select="$_asset_saved" /></xsl:message>


########################################################################## -->


<xsl:variable name="VERBOSE" select="true()" />

<!-- ##################################################################### -->
<!-- ##################################################################### -->



<!-- ##################################################################### -->
<!-- ##  Asset-Management                                               ## -->
<!-- ##################################################################### -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


	<xsl:function   name="cs:ASSET_getByID">
		<xsl:param  name="_asset_id"     as="xs:string" />
		<xsl:copy-of select="cs:get-asset($_asset_id)" />
	</xsl:function>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!--
<xsl:variable
	name="_asset"
	select="cs:ASSET_getByExternalID( 'test:foo-bar' )"
/>
-->
	<xsl:function  name="cs:ASSET_getByExternalID">
		<xsl:param name="_id_extern"   as="xs:string" />

		<xsl:variable name="_asset"
			select="cs:asset()[@censhare:asset.id_extern=$_id_extern]"
		/>

		<xsl:if test="$VERBOSE=true()">
			<xsl:message>ASSET_getByExternalID._asset: <xsl:copy-of select="$_asset" /></xsl:message>
		</xsl:if>

		<xsl:copy-of select="$_asset" />
	</xsl:function>


	<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


<!--
<xsl:variable
	name="_asset"
	select="cs:ASSET_getOrCreateByExternalID( 'test:foo-bar', 'test-Asset', 'other.', 'root.', 'default' )"
/>
-->
	<xsl:function  name="cs:ASSET_getOrCreateByExternalID">
		<xsl:param name="_id_extern"   as="xs:string" />
		<xsl:param name="_name"        as="xs:string" />
		<xsl:param name="_type"        as="xs:string" />
		<xsl:param name="_domain"      as="xs:string" />
		<xsl:param name="_application" as="xs:string" />

		<xsl:variable name="_asset_loaded"
			select="cs:asset()[@censhare:asset.id_extern=$_id_extern]"
		/>
		<xsl:variable name="_asset_new" as="node()">
			<asset
				id_extern   = "{$_id_extern}"
				name        = "{$_name}"
				type        = "{$_type}"
				domain      = "{$_domain}"
				application = "{$_application}"
			/>
		</xsl:variable>

		<xsl:variable name="_asset" select="
			if ($_asset_loaded) then
				$_asset_loaded
			else
				$_asset_new
		" />
		<xsl:if test="$VERBOSE=true()">
			<xsl:message>ASSET_getOrCreateByExternalID._asset: <xsl:copy-of select="$_asset" /></xsl:message>
		</xsl:if>

		<xsl:copy-of select="$_asset" />
	</xsl:function>


	<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


	<xsl:function   name="cs:ASSET_insertOrUpdate">
		<xsl:param  name="_asset" as="node()" />
		<xsl:choose>
			<xsl:when test="$_asset/asset/@id">
				<xsl:if test="$VERBOSE=true()">
					<xsl:message>ASSET_insertOrUpdate.UPDATE: <xsl:copy-of select="$_asset" /></xsl:message>
				</xsl:if>
				<!--<xsl:copy-of select="cs:ASSET_update( $_asset )"/>-->
				  <xsl:variable name="checked-out-xml">
			        <xsl:copy-of select="cs:ASSET_checkout( $_asset )"/>
			    </xsl:variable>
			    <xsl:variable name="new-checked-out-xml">
			        <asset>
			            <xsl:copy-of select="$checked-out-xml/asset/@*"/>
			            <xsl:attribute name="name" select="$_asset/asset/@name"/>
			            <xsl:attribute name="domain" select="$_asset/asset/@domain"/>
			            <xsl:attribute name="domain2" select="$_asset/asset/@domain2"/>
			            <xsl:attribute name="modified_date" select="$_asset/asset/@modified_date"/>
			            <xsl:attribute name="modified_by" select="$_asset/asset/@modified_by"/>
			            <!--Added by FAC-08-06-18-->
			            <xsl:if test="$_asset/asset/@deletion">
			            <xsl:attribute name="deletion" select="$_asset/asset/@deletion"/>
			            </xsl:if>

			        	<!--Added by FAC-12-06-18-->
			        	<!-- 21-06-18 -->
			        	<!--<xsl:if test="$_asset/asset/@type='sale.'">
			        		<xsl:attribute name="type" select="$_asset/asset/@type"/>
			        		</xsl:if>-->

			        	<xsl:copy-of select="$_asset/asset/* except ($_asset/asset/parent_asset_rel[@key='actual.'] | $_asset/asset/parent_asset_element_rel)"/>
			        </asset>
			    </xsl:variable>
			    <xsl:copy-of select="cs:ASSET_checkin($new-checked-out-xml)"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$VERBOSE=true()">
					<xsl:message>ASSET_insertOrUpdate.INSERT: <xsl:copy-of select="$_asset" /></xsl:message>
				</xsl:if>
				<xsl:copy-of select="cs:ASSET_checkinnew( $_asset )"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->




<!-- ##################################################################### -->
<!-- ##  Low Level                                                      ## -->
<!-- ##################################################################### -->


	<xsl:function name="cs:ASSET_checkout">
		<xsl:param name="asset_xml"/>
		<cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="processed_asset_xml">
			<cs:param name="source" select="$asset_xml"/>
		</cs:command>
		<xsl:copy-of select="$processed_asset_xml"/>
	</xsl:function>


	<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


	<xsl:function name="cs:ASSET_checkin">
		<xsl:param name="asset_xml"/>
		<cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="processed_asset_xml">
			<cs:param name="source" select="$asset_xml"/>
		</cs:command>
		<xsl:copy-of select="$processed_asset_xml"/>
	</xsl:function>


	<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


	<xsl:function name="cs:ASSET_checkinnew">
		<xsl:param name="asset_xml" />
		<cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="processed_asset_xml">
			<cs:param name="source" select="$asset_xml"/>
		</cs:command>
		<xsl:copy-of select="$processed_asset_xml"/>
	</xsl:function>


	<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


	<xsl:function name="cs:ASSET_update">
		<xsl:param name="asset_xml"/>
		<cs:command name="com.censhare.api.assetmanagement.Update" returning="processed_asset_xml">
			<cs:param name="source" select="$asset_xml" />
		</cs:command>
		<xsl:copy-of select="$processed_asset_xml"/>
	</xsl:function>


	<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->


	<xsl:function  name="cs:ASSET_delete">
		<xsl:param name="asset_xml_or_id"/>
		<!--
			deletion_state-values:
				proposed : proposed for deletion
				none     : not deleted
				marked   : marked for deletion
				physical : physical deletion
		-->
		<xsl:param name="deletion_state" />

		<cs:command   name="com.censhare.api.assetmanagement.Delete">
			<cs:param name="source" select="$asset_xml_or_id"/>
			<cs:param name="state"  select="$deletion_state"/>
		</cs:command>
	</xsl:function>

	<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

	<xsl:function  name="cs:ASSET_delete">
		<xsl:param name="asset_xml_or_id"/>

		<xsl:copy-of select="cs:ASSET_delete($asset_xml_or_id, 'proposed')" />
	</xsl:function>


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<!-- ##################################################################### -->


	<xsl:variable name="CENSHARE_VALUETYPES">
		<!--  cs-Server v4.5  -  20120911  -->
		<value  id="0"   type=""                 quantity="1"  description="no value" />
		<value  id="1"   type="value_key"        quantity="1"  description="hierarchical attribute (string)" />
		<value  id="2"   type="value_key"        quantity="1"  description="enumeration (string)" />
		<value  id="3"   type="value_long"       quantity="1"  description="integer (long)" />
		<value  id="4"   type="value_string"     quantity="1"  description="string" />
		<value  id="5"   type="value_timestamp"  quantity="1"  description="timestamp" />
		<value  id="6"   type="value_long"       quantity="1"  description="boolean" />
		<value  id="7"   type="value_double"     quantity="1"  description="double" />
		<value  id="8"   type="value_long"       quantity="2"  description="integer pair" />
		<value  id="9"   type="value_timestamp"  quantity="2"  description="timestamp pair" />
		<value  id="10"  type="value_asset_id"   quantity="1"  description="asset reference" />
		<value  id="11"  type="xmldata"          quantity="1"  description="xml" />
		<value  id="12"  type="value_double"     quantity="2"  description="double pair" />
		<value  id="13"  type="value_timestamp"  quantity="1"  description="date" />
		<value  id="14"  type="value_timestamp"  quantity="2"  description="date pair" />
		<value  id="15"  type="value_timestamp"  quantity="1"  description="time" />
		<value  id="16"  type="value_timestamp"  quantity="2"  description="time pair" />
		<value  id="17"  type="value_timestamp"  quantity="1"  description="year" />
		<value  id="18"  type="value_timestamp"  quantity="2"  description="year pair" />
		<value  id="19"  type="value_timestamp"  quantity="1"  description="year month" />
		<value  id="20"  type="value_timestamp"  quantity="2"  description="year month pair" />
		<value  id="21"  type="value_timestamp"  quantity="1"  description="month" />
		<value  id="22"  type="value_timestamp"  quantity="2"  description="month pair" />
		<value  id="23"  type="value_timestamp"  quantity="1"  description="month day" />
		<value  id="24"  type="value_timestamp"  quantity="2"  description="month day pair" />
		<value  id="25"  type="value_timestamp"  quantity="1"  description="day" />
		<value  id="26"  type="value_timestamp"  quantity="2"  description="day pair" />
		<value  id="27"  type="value_long"       quantity="1"  description="duration" />
	</xsl:variable>


	<xsl:function name="cs:valueID_to_valueType" as="xs:string">
		<xsl:param name="_value_type_id" as="xs:string?"/>

		<xsl:choose>
			<xsl:when test="$_value_type_id">
				<xsl:value-of select="
					$CENSHARE_VALUETYPES/value[@id=$_value_type_id]/@type
				" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="''" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:function name="cs:get_value_definition">
		<xsl:param name="_value_type_id" as="xs:string?"/>

		<xsl:copy-of select="
			$CENSHARE_VALUETYPES/value[@id=$_value_type_id]
		" />
	</xsl:function>


	<xsl:function name="cs:to-cs-date" as="xs:string">
	<!--
		NEW censhare datetime format: yyyy-MM-ddTHH:mm:ssZ
	-->
		<xsl:param name="_d" as="xs:string?" />
		<xsl:param name="_t" as="xs:string?" />


		<xsl:variable name="_datum" select="
			if (matches($_d, '^\d{8}.*$')) then
				concat(substring($_d,1,4), '-', substring($_d,5,2), '-', substring($_d,7,2))
			else if (matches($_d, '^\d{2}.\d{2}.\d{4}.*$')) then
				concat(substring($_d,7,4), '-', substring($_d,4,2), '-', substring($_d,1,2))
			else if (matches($_d, '^\d{4}.\d{2}.\d{2}.*$')) then
				concat(substring($_d,1,4), '-', substring($_d,6,2), '-', substring($_d,9,2))
			else ''
		"/>

		<xsl:variable name="_zeit" select="
            if (matches($_t, '^\d{2}:\d{2}:\d{2}$')) then
            $_t
            else if (matches($_t,'^.{8}\d{2}:\d{2}:\d{2}.*$')) then
            concat(substring($_d,9,2), ':', substring($_d,12,2), ':', substring($_d,15,2))
            else if (matches($_t, '^\d{2}:\d{2}$')) then
            concat($_t, ':00')
            else if (matches($_t, '^\d{6}$')) then
            concat(substring($_d,1,2), ':', substring($_d,3,2), ':', substring($_d,5,2))
            else if (matches($_t, '^\d{4}$')) then
            concat(substring($_d,1,2), ':', substring($_d,3,2), ':00')
            else if (matches($_t, '^.{11}\d{2}.\d{2}.\d{2}.*$')) then
            concat(substring($_d,12,2), ':', substring($_d,15,2), substring($_d,18,2))
            else if (matches($_t, '^.{11}\d{2}.\d{2}.*$')) then
            concat(substring($_d,12,2), ':', substring($_d,15,2), ':00')
            else '00:00:00'
      "/>

		<xsl:sequence select="concat($_datum, 'T', $_zeit, 'Z')" />
	</xsl:function>


	<xsl:function name="cs:prepare-feature-value">
		<xsl:param name="_value" />
		<xsl:param name="value-definition" />

		<xsl:choose>
			<xsl:when test="$value-definition/@type='value_timestamp'">
				<xsl:value-of select="cs:to-cs-date($_value, $_value)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$_value" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

<!-- ##################################################################### -->
</xsl:stylesheet>
