function controller($scope, csApiSession) {

    var objDate1 = new Date($scope.asset.traits.jdesale.saledate.value[0].value);
    var objDate2 = new Date($scope.asset.traits.jdesale.saledate.value[0].value2);


    var strDate1 =
        objDate1.getUTCDate() + ' ' +
        objDate1.toLocaleString("en", { month: "long"  }) + ' ' +
        objDate1.toLocaleString("en", { year: "numeric"});
    var strDate2 =
        objDate2.getUTCDate() + ' ' +
        objDate2.toLocaleString("en", { month: "long"  }) + ' ' +
        objDate2.toLocaleString("en", { year: "numeric"});
    
    $scope.saleDate = strDate1 + ' - ' + strDate2;

    $scope.$watchGroup(
        [
            'asset.traits.jdesale.approver.value[0].value',
            'asset.traits.canvassale.productionAccountManager.value[0].value',
            'asset.traits.canvassale.saleCoordinator.value[0].value',
            'asset.traits.canvassale.headOfSaleManagement.value[0].value',

        ],
        function (newVal, oldVal) {

            $scope.party = [];

            csApiSession.masterdata.lookup({
                lookup: [{
                    table: 'party'
                }]
            }).then(function (result) {
                // console.log('RESULT');
                // console.log(result.records);
                $scope.party = result.records.record;
                // console.log(parseInt($scope.asset.traits.jdesale.approver.value[0].value));
                for (var i = 0; i < result.records.record.length; i++) {
                    if (parseInt($scope.asset.traits.jdesale.approver.value[0].value) === result.records.record[i].id) {
                        // console.log('HEY MY ID MATCHES');
                        // console.log(result.records.record[i].id);
                        $scope.approverDisplayName = result.records.record[i].display_name;
                        // console.log($scope.approverDisplayName);
                    }
                    if (parseInt($scope.asset.traits.canvassale.productionAccountManager.value[0].value) === result.records.record[i].id) {
                        // console.log('HEY MY ID MATCHES');
                        // console.log(result.records.record[i].id);
                        $scope.productionAccountManagerDisplayName = result.records.record[i].display_name;
                        // console.log($scope.approverDisplayName);
                    }
                    if (parseInt($scope.asset.traits.canvassale.saleCoordinator.value[0].value) === result.records.record[i].id) {
                        // console.log('HEY MY ID MATCHES');
                        // console.log(result.records.record[i].id);
                        $scope.saleCoordinatorDisplayName = result.records.record[i].display_name;
                        // console.log($scope.approverDisplayName);
                    }
                    if (parseInt($scope.asset.traits.canvassale.headOfSaleManagement.value[0].value) === result.records.record[i].id) {
                        // console.log('HEY MY ID MATCHES');
                        // console.log(result.records.record[i].id);
                        $scope.headOfSaleManagementDisplayName = result.records.record[i].display_name;
                        // console.log($scope.approverDisplayName);
                    }

                }
            });


        }, true);



}