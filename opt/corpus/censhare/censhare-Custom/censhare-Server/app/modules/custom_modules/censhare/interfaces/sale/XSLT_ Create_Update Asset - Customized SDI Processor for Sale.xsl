<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    exclude-result-prefixes="xsl xs cs"
    >
    <!-- #########################################################################
        
	resource-key = sdi:processor
	
TODOs:
- replace all features instead of updating them

########################################################################## -->
    
    <xsl:output
        method   = "xml"
        omit-xml-declaration = "no"
        encoding = "UTF-8"
        indent   = "yes"
    />
    
    <!--<xsl:variable name="VERBOSE" select="true()" />-->
    
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=lib:assetmanagement/storage/master/file" />
    
    <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="fs-ref" />
    
    <!-- ##################################################################### -->
    
    
    <xsl:template match="/" priority="1">
        <xsl:message>## sdi:processor ## BEGIN ##</xsl:message>        
        <result>
            <xsl:apply-templates select="asset" mode="file-processor">
                <xsl:with-param name="parent-asset-id" select="()"/>
            </xsl:apply-templates>
            <xsl:apply-templates select="assets/asset" mode="file-processor">
                <xsl:with-param name="parent-asset-id" select="()"/>
            </xsl:apply-templates>
        </result>
        
        <xsl:message>## sdi:processor ## END ##</xsl:message>
    </xsl:template>

    
    
    <xsl:template match="asset" priority="1" mode="file-processor">
        <xsl:param name="parent-asset-id" />
        <xsl:message>**** apa: asset input: <xsl:copy-of select="."/></xsl:message>
        <xsl:variable name="child-images" select="distinct-values(//child_asset_rel[@key='user.' or @key='user.main-picture.']/@child_asset)"/>
        
        <xsl:variable name="asset-input" select="." />
        
        <!-- load asset-xml or create template -->
        <xsl:variable name="asset-loaded"
            select="cs:ASSET_getOrCreateByExternalID(
            @id_extern,
            @name,
            @type,
            @domain,
            @application
            )"
        />
        <!--Ashutosh1-asset-loaded: <asset id_extern="269" name="Abstract" type="taxonomy." domain="root." application="default"/>-->
        <!--Ashutosh1-asset-loaded: <asset id_extern="16270" name="Closterman, school/style of John" type="taxonomy." domain="root." application="default"/>-->
        <xsl:message>**** apa: asset loaded: <xsl:copy-of select="$asset-loaded"/></xsl:message>
        
        <!-- update asset-xml (set relation example) -->
        <xsl:variable name="asset-updated">
            <asset>
                
                <!-- copy attributes from loaded asset, if they not exist on input-data -->
                <xsl:for-each select="$asset-loaded/@*">
                    <xsl:variable name="attribute-name" select="local-name(.)" />
                    
                    <xsl:if test="not($asset-input/@*[local-name(.)=$attribute-name])">
                        <xsl:attribute name="{local-name(.)}" select="." />
                    </xsl:if>
                </xsl:for-each>
                
                <!-- copy attributes from input-data -->
                <xsl:copy-of select="$asset-input/@*" />
                
                <!-- handle features -->
                <xsl:apply-templates select="$asset-loaded/node()" mode="node-processor-loaded">
                    <xsl:with-param name="features-input" select="$asset-input/node()" />
                    <xsl:with-param name="child-images" select="$child-images"/>
                </xsl:apply-templates>
                
                <xsl:apply-templates select="$asset-input/node()" mode="node-processor-input" />
                    
                <xsl:choose>
                    <xsl:when test="$asset-input/@type='product.' and count($asset-loaded/*)=0 and not(empty($parent-asset-id))">
                       
                        <parent_asset_rel
                            key="target."
                            parent_asset="{$parent-asset-id}"
                        />
                    </xsl:when>
                    <xsl:when test="starts-with($asset-input/@type,'text.') and count($asset-loaded/*)=0 and $asset-input/@language='en' and not(empty($parent-asset-id))">
                     
                        <parent_asset_rel
                            key="user."
                            parent_asset="{$parent-asset-id}"
                        />
                        
                        </xsl:when>
                    <xsl:when test="starts-with($asset-input/@type,'text.') and count($asset-loaded/*)=0 and $asset-input/@language!='en' and not(empty($parent-asset-id))">
                        
                        <parent_asset_rel
                            key="variant."
                            parent_asset="{$parent-asset-id}"
                        />
                        
                    </xsl:when>
                    
                    <xsl:when test="starts-with($asset-input/@type,'text.') and $asset-input/@language!='en'"/>
                    
                    <xsl:when test="not($asset-loaded/parent_asset_rel[@parent_asset=$parent-asset-id]) and not(empty($parent-asset-id))">
                        
                       
                        <parent_asset_rel
                            key="user."
                            parent_asset="{$parent-asset-id}"
                        />
                    </xsl:when>
                    <xsl:when test="$asset-input/parent_asset_rel[not(@parent_asset)] and string-length($parent-asset-id) &gt; 0">
                      
                        <parent_asset_rel
                            key="{$asset-input/parent_asset_rel/@key}"
                            parent_asset="{$parent-asset-id}"
                        />
                    </xsl:when>
                   <xsl:otherwise>
                       
                   </xsl:otherwise>
                </xsl:choose>
                <!--</xsl:if>-->
            </asset>
        </xsl:variable>
        
        <!--
        Ashutosh2-asset-updated: <asset name="Abstract" domain="root." type="taxonomy." id_extern="269" domain2="root." modified_date="2017-10-23T15:56:46.479Z" application="default">
                                    <asset_feature feature="canvas:coa.attributetype" value_key="4"/>
                                    <asset_feature feature="canvas:coa.attributeid" value_long="269"/>
                                 </asset>
            -->
        <xsl:message>**** apa: asset updated: <xsl:copy-of select="$asset-updated"/></xsl:message>
        <!--
	<xsl:message>sdi:processor #########################################################</xsl:message>
	<xsl:message>sdi:processor # ASSET-INPUT # <xsl:copy-of select="$asset-input" /></xsl:message>
	<xsl:message>sdi:processor # ASSET-UPDATED # <xsl:copy-of select="$asset-updated" /></xsl:message>
	<xsl:message>sdi:processor #########################################################</xsl:message>
-->
        
        
        <!-- write asset-xml back into system -->
        <xsl:variable name="asset-saved"
            select="cs:ASSET_insertOrUpdate( $asset-updated )"
        />
        
        <xsl:copy-of select="$asset-saved"/>
        <!--
        Ashutosh3-asset-saved: <asset corpus:dto_flags="p" id="18266" version="1" currversion="0" content_version="1" id_extern="269" name="Abstract" type="taxonomy." application="default" state="0" deletion="0" modified_by="10" domain="root." domain2="root." modified_date="2017-10-23T15:56:46Z" non_owner_access="0" iscancellation="0" has_master_file="0" has_update_geometry="0" storage_state="0" created_by="10" creation_date="2017-10-23T15:56:46Z" rowid="AAAOzKAAFAAAAVjAAE" usn="0" tcn="1" ccn="1">
                                 <asset_feature corpus:dto_flags="p" sid="72979" asset_id="18266" asset_version="1" feature="canvas:coa.attributetype" isversioned="1" timestamp="2017-10-23T15:56:46Z" party="10" value_key="4" rowid="AAAO3UAAFAAAF0fAAZ"/>
                                 <asset_feature corpus:dto_flags="p" sid="72980" asset_id="18266" asset_version="1" feature="canvas:coa.attributeid" isversioned="1" timestamp="2017-10-23T15:56:46Z" party="10" value_long="269" rowid="AAAO3UAAFAAAF0fAAa"/>
                                 <asset_feature corpus:dto_flags="p" sid="72981" asset_id="18266" asset_version="1" feature="censhare:uuid" isversioned="1" timestamp="2017-10-23T15:56:46Z" party="0" value_string="c5bfe200-b80a-11e7-80a2-acde48001122" rowid="AAAO3UAAFAAAF0fAAw"/>
                               </asset>
        
        -->
        <xsl:message>**** apa: asset saved: <xsl:copy-of select="$asset-saved"/></xsl:message>
    </xsl:template>
    
    
    <!-- ##################################################################### -->
    
    
    <xsl:template match="node()[not(self::asset_feature)]" mode="node-processor-loaded" priority="1">
        <xsl:param name="features-input" />
        <xsl:param name="child-images"/>

        <xsl:variable name="child-id" select="current()/@child_asset"/>
        <xsl:variable name="key" select="@key"/>
        <xsl:variable name="type" select="cs:get-asset(current()/@child_asset)/@type"/>
        <xsl:variable name="parent-asset-type" select="cs:get-asset(current()/@parent_asset)/@type"/>
        <xsl:variable name="feature-flag" select="if (starts-with($type,'picture.') and exists(current()/asset_rel_feature[@feature='canvas:jde.primaryimagerelation']/@value_long)) then current()/asset_rel_feature[@feature='canvas:jde.primaryimagerelation']/@value_long else 0"/>
        <xsl:variable name="image-flag" select="distinct-values(for $x in $child-images return if($x = $child-id) then 'true' else())"/>
          <xsl:choose>
            <xsl:when test="$key = 'master' or $key = 'actual.' or $key = 'text-preview'"/>
            <xsl:when test="$key = 'user.' and starts-with($type,'picture.') and $parent-asset-type != 'sale.'"/>
            <xsl:when test="$feature-flag = 0">
                <xsl:choose>
                    <xsl:when test="$parent-asset-type != 'sale.'">
                       <xsl:copy-of select="." />
                    </xsl:when>
                    <xsl:when test="empty($image-flag)">
                       <xsl:copy-of select="." />
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    
    <xsl:template match="asset_feature1111" mode="node-processor-loaded" priority="2">
        <xsl:param name="features-input" />
        
        <!--
		copy features from loaded asset,
		if they not exist on input-data
	-->
        <xsl:variable name="feature-key" select="@feature" />
        
        <xsl:choose>
            <xsl:when test="$feature-key = 'censhare:input-form'"/>
            <xsl:when test="starts-with($feature-key,'canvas:coa.')"/>
            <xsl:when test="not($features-input[@key=$feature-key]) and not(starts-with($feature-key,'chrisus:'))">
                <xsl:copy-of select="." />
            </xsl:when>
        </xsl:choose>
        
    </xsl:template>
    
    
    <!-- ##################################################################### -->
    
    
    <xsl:template match="node()" mode="node-processor-input" priority="1">
        <xsl:copy-of select="." />
    </xsl:template>
    
    <xsl:template match="asset" mode="node-processor-input" priority="2">
    </xsl:template>
    
    
    <xsl:template match="feature" mode="node-processor-input" priority="2">
        <xsl:variable name="feature-key" select="@key" />
        <xsl:variable
            name="feature-definition"
            select="cs:master-data('feature')[@key=$feature-key]"
        />
        
        <xsl:message>**** apa: feature definition: <xsl:copy-of select="$feature-definition"/></xsl:message>
        <!--
	<feature
		key="f01:collection-name"
		type="censhare:asset-feature"
		storage="0"
		domain="root."
		domain2="root."
		name="Collection Name"
		value_type="4"
		ismultivalue="0"
		isassetinfo="0"
		assetinfo_sorting="1"
		issearchable="1"
		sorting="1"
		language_type="0"
		color_type="0"
		bdb_key="461"
		enabled="1"
		has_relevance="0"
		tcn="0"
		rowid="AAAn2sAATAAAYE6AAR"
		corpus:dto_flags="p"
	/>
-->
        
        <xsl:variable
            name="value-definition"
            select="cs:get_value_definition($feature-definition/@value_type)"
        />
        <xsl:message>**** apa: value definition: <xsl:value-of select="$value-definition/@type"/></xsl:message>
        
        <xsl:variable name="value"  select="cs:prepare-feature-value(@value,  $value-definition)" />
        <xsl:variable name="value2" select="cs:prepare-feature-value(@value2, $value-definition)" />
        
        <asset_feature feature="{$feature-key}">
            <xsl:if test="string-length($value-definition/@type)>0">
                <xsl:attribute name="{$value-definition/@type}"  select="$value" />
            </xsl:if>
            
            <xsl:if test="$value-definition/@quantity='2'">
                <xsl:attribute name="{$value-definition/@type}2" select="$value2" />
            </xsl:if>
            
            <xsl:if test="@language">
                <xsl:attribute name="language"  select="@language" />
            </xsl:if>
            
            <!--
			if value=asset-relation,
			then also set value_long2 to 0
		-->
            <xsl:if test="$value-definition/@id='10'">
                <xsl:attribute name="value_long2" select="'0'" />
            </xsl:if>
            
            <xsl:if test="$feature-definition/@key='canvas:jdesale.saledate' and $value-definition/@id='14' and @value!='' and @value2!=''">
                <xsl:attribute name="value_timestamp" select="cs:format-date(xs:dateTime(@value),'yyyy-MM-dd''T''12:00:00''Z''')" />
                <xsl:attribute name="value_timestamp2" select="cs:format-date(xs:dateTime(@value2),'yyyy-MM-dd''T''12:00:00''Z''')" />
            </xsl:if>
            <xsl:if test="$feature-definition/@key='canvas:jdesale.salestarttime' and $value-definition/@id='15' and @value!=''">
                <xsl:attribute name="value_timestamp" select="cs:format-date(xs:dateTime(@value),'0000-00-00''T''HH:mm:ss''Z''')" />
            </xsl:if>
            
            <xsl:apply-templates select="node()" mode="node-processor-input" />
        </asset_feature>
    </xsl:template>
    
    
    <!-- ##################################################################### -->
    
    
    <!-- ##################################################################### -->
</xsl:stylesheet>