<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:canvas="http://integration.christies.com/Interfaces/DotCom/Notification"
  xmlns:corpus="http://www.censhare.com/corpus"
  exclude-result-prefixes="#all">
  
  <xsl:output method="xml" omit-xml-declaration="yes"/>
  
  <xsl:param name="data">
    <xsl:variable name="resource-key" select="asset//asset_feature[@feature='censhare:resource-key']/@value_string"/>
    <xsl:message>CoA bulk data process: Source file resource key: <xsl:value-of select="$resource-key"/></xsl:message>
    <xsl:variable name="resource-asset" select="concat('censhare:///service/assets/asset;censhare:resource-key=', $resource-key, '/storage/master/file')"/>
    <xsl:copy-of select="doc($resource-asset)"/>
  </xsl:param>
  
  <xsl:template match="/">
    
    <xsl:for-each-group select="$data//canvas:COAAttribute" group-ending-with="//canvas:COAAttribute[position() mod 1000 = 0]">
      
      <xsl:variable name="metadata">
        <SVCOutBoundChartOfArtExtract xmlns="http://integration.christies.com/Interfaces/DotCom/Notification">
          <Trace>
            <TraceStatusID>ea73acd1-22f7-4550-a9e1-95ee0b40a95f</TraceStatusID>
            <TraceMachineName>VPC054</TraceMachineName>
            <TraceAssemblyName>DTSXMLDumper</TraceAssemblyName>
            <TraceDateTime>2017-11-22T10:33:37.829331-05:00</TraceDateTime>
            <TraceDateTimeUTC>2017-11-22T15:33:37.829331-05:00</TraceDateTimeUTC>
          </Trace>
          <ChartOfArt>
            <xsl:copy-of select="current-group()"/>
          </ChartOfArt>
        </SVCOutBoundChartOfArtExtract>
      </xsl:variable>
                           
      <xsl:message>CoA bulk data process: Files metadata:<xsl:copy-of select="$metadata//canvas:SVCOutBoundChartOfArtExtract"/></xsl:message>
      
      <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
      
      <xsl:variable name="result-file" select="concat($out, 'product.xml')"/>
      <cs:command name="com.censhare.api.io.WriteXML">
        <cs:param name="source" select="$metadata"/>   
        <cs:param name="dest" select="$result-file"/>    
      </cs:command>
      
      <xsl:message>CoA bulk data process: File result:<xsl:copy-of select="$result-file"/></xsl:message>
      
      <xsl:variable name="asset_name">
        <xsl:variable name="file-name" select="'CoA file '"/>
        <xsl:variable name="position" select="position()"/>
        <xsl:value-of select="concat($file-name, $position)"/>
      </xsl:variable>
      
      <xsl:message>CoA bulk data process: asset name: <xsl:value-of select="$asset_name"/></xsl:message>
      
      <xsl:variable name="asset_resource_key" select="concat($asset_name, current-dateTime())"/>
      
      <xsl:message>CoA bulk data process: asset_resource_key: <xsl:copy-of select="$asset_resource_key"/></xsl:message>
      
      <cs:command name="com.censhare.api.assetmanagement.CheckInNew">
        <cs:param name="source">
          <asset application="default" name="{$asset_name}" type="text." domain="root." domain2="root.">
            <storage_item element_idx="0" key="master" mimetype="text/xml" corpus:asset-temp-file-url="{$result-file}"/>
            <asset_feature feature="censhare:resource-key" value_string="{$asset_resource_key}"/>
            <asset_element idx="0" key="actual." />
          </asset>
        </cs:param>
      </cs:command>
    </xsl:for-each-group>
  </xsl:template>
  
</xsl:stylesheet>
