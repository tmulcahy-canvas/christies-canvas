<?xml version="1.0" encoding="UTF-8"?>

<!-- 
#########################################################
censhare standard content Layout Transformation

Version 1.1 - August 16th 2013 
#########################################################
-->

<!-- changes: 
mhe 2013-08-16: 
  - refactoring of image captions per $category: moved to structure.xsl, to templates for <article/> and <print/>
  - added table column widths
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xi="http://www.w3.org/2001/XInclude" 
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:fn="http://www.w3.org/2005/xpath-functions" 
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:map="http://ns.censhare.de/mapping"
  xmlns:temp="http://ns.censhare.de/elements"
  xmlns:func="http://ns.censhare.de/functions"
  exclude-result-prefixes="xs xd fn xi cs map temp func"
  version="2.0">
  
  
  <xsl:param name="column-width-mm" as="xs:integer">88</xsl:param>
  <xsl:param name="std-table-width-mm" as="xs:integer" select="$column-width-mm"/>
  <xsl:param name="std-image-width-mm" as="xs:integer" select="$column-width-mm"/>
  
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- common elements  -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  
  <!-- main content -->
  <xsl:template match="article" priority="0.5">
    <xsl:choose>
      <xsl:when test="$category = 'default'">
        <xsl:apply-templates select="content" />        
      </xsl:when>
      <xsl:when test="$category">
        <xsl:apply-templates select="print" />
      </xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <!-- print specific: image captions if $category is set -->
  <xsl:template match="print" priority="0.5">
    <!-- find matching caption -->
    <xsl:variable name="sorted_content" as="element()*">
      <xsl:for-each select="//*[@placement-category=$category]">
        <xsl:sort select="number(@sorting)"/>
        <!-- copy parent element for better format mapping -->
        <xsl:copy>
          <xsl:copy-of select="caption"/>
        </xsl:copy>
      </xsl:for-each>
    </xsl:variable>
    <xsl:apply-templates select="$sorted_content[number($index)]"/>
  </xsl:template>
  
  <xsl:template match="content | text" priority="0.5">
    <xsl:apply-templates/>
  </xsl:template>
  
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- Christies related elements   -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  
  <xsl:template match="estimate-row">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="estimate">
    <xsl:call-template name="icml-inline">
      <xsl:with-param name="add-attributes" select="(@FillColor)"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="text[@type]">
    <xsl:apply-templates select="node()">
      <xsl:with-param name="type" select="@type" tunnel="yes"/>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="inline-quantity">
    <xsl:call-template name="icml-inline"/>
  </xsl:template>
  <xsl:template match="inline-quantitydesc">
    <xsl:call-template name="icml-inline"/>
  </xsl:template>
  
  <xsl:template match="lot">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="lot-number-only">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="lot-number-only/node()">
    <xsl:call-template name="icml-inline"/>
  </xsl:template>
  
  <xsl:template match="sizes">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="sizes/node()">
    <xsl:call-template name="icml-inline"/>
  </xsl:template>
  
  <xsl:template match="lot/node()">
    <xsl:call-template name="icml-inline">
      <xsl:with-param name="add-attributes" select="(@FillColor)"/>
    </xsl:call-template>
  </xsl:template>
  
  <!--
  <xsl:template match="lot/symbol/node()">
    <xsl:call-template name="icml-inline">
      <xsl:with-param name="add-attributes" select="(@FillColor)"/>
    </xsl:call-template>
  </xsl:template>
  -->
  
  <xsl:template match="lot/symbol/E | lot/symbol/P | lot/symbol/S | lot/symbol/Z">
    <xsl:call-template name="icml-inline">
      <xsl:with-param name="add-attributes" select="(@FillColor)"/>
    </xsl:call-template>
  </xsl:template>
  
    <xsl:template match="paragraph/b | paragraph/i | paragraph/bi | paragraph/sup | paragraph/sub | paragraph/sc | paragraph/E | paragraph/P | paragraph/S | paragraph/Z | paragraph/inline-quantitydesc/P | paragraph/inline-quantitydesc/E" priority="2">
    <xsl:call-template name="icml-inline">
      <xsl:with-param name="add-attributes" select="(@FillColor)"/>
    </xsl:call-template>
  </xsl:template>
  
  <!-- block elements -->
  
  <xsl:template match="strapline | title | subtitle | intro | paragraph | subheadline-1 | subheadline-2 | subheadline-3 | pre | quantity" priority="0.5">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="hdivider" priority="0.5">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="new-page" priority="0.5">
    <xsl:call-template name="icml-break">
      <xsl:with-param name="type" select="'NextPage'"/>
    </xsl:call-template>
  </xsl:template>
  
  <!-- paragraphs in special position -->
  
  <xsl:template match="text/paragraph[not(preceding-sibling::*)] | text/paragraph[preceding-sibling::*[1][self::intro]] | 
    callout/paragraph[not(preceding-sibling::*)] | group/paragraph[not(preceding-sibling::*)]" priority="0.8">
    <xsl:call-template name="icml-paragraph">
      <xsl:with-param name="name-postfix">.first</xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="text/paragraph[not(following-sibling::*)] | callout/paragraph[not(following-sibling::*)] | group/paragraph[not(following-sibling::*)]" priority="0.7">
    <xsl:call-template name="icml-paragraph">
      <xsl:with-param name="name-postfix">.last</xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="text/paragraph[preceding-sibling::*[1][self::subheadline-1 | self::subheadline-2]] | 
    group/paragraph[preceding-sibling::*[1][self::subheadline-1 | self::subheadline-2]]" priority="0.9">
    <xsl:call-template name="icml-paragraph">
      <xsl:with-param name="name-postfix">.aftersubhead</xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <!-- footnotes -->  
  
  <xsl:template match="footnote" priority="0.5">
    <xsl:call-template name="icml-footnote">
      <xsl:with-param name="content">
        <xsl:call-template name="icml-paragraph"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <!-- lists -->
  
  <xsl:template match="enumeration | bullet-list | item" priority="0.5">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="enumeration/item/paragraph" priority="1.0">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="enumeration/item[not(preceding-sibling::item)]/paragraph" priority="1.5">
    <xsl:call-template name="icml-paragraph">
    </xsl:call-template>
  </xsl:template>
  
  <!-- inline elements -->
  
  <xsl:template match="bold | italic | bold-italic | marker | sup | sub | underline" priority="0.5">
    <xsl:call-template name="icml-inline"/>
  </xsl:template>
  
  <xsl:template match="Br" priority="0.5">
    <xsl:call-template name="icml-break">
      <xsl:with-param name="type" select="'Br'"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="link[@url != '']" priority="1.0">
    <xsl:call-template name="icml-hyperlink">
      <xsl:with-param name="url" select="@url"/>
      <xsl:with-param name="name" select="@title"/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="link" priority="0.5">
    <xsl:call-template name="icml-inline"/>
  </xsl:template>
  
  <!-- tables -->
  
  <xsl:template match="table" priority="0.5">
    <xsl:apply-templates select="table-caption"/>
    <xsl:variable name="width">
      <xsl:choose>
        <xsl:when test="ancestor::table">
          <xsl:value-of select="func:mm2pt($std-table-width-mm div max(ancestor::table/row/count(cell)))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="func:mm2pt($std-table-width-mm)"/>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:variable>
  <!--  <xsl:variable name="col-widths">
    <xsl:for-each select="colspec/col">
          <xsl:value-of select="func:mm2pt(@width)"/>
    </xsl:for-each>      
    </xsl:variable>
  -->

    
    <xsl:call-template name="icml-table">
      <xsl:with-param name="paragraphstyle-group">table</xsl:with-param>
      <xsl:with-param name="paragraphstyle-name">table</xsl:with-param>    
      <xsl:with-param name="tablestyle-name" >default table</xsl:with-param>  
      <xsl:with-param name="header-rows" select="@header-row-count" />
      <xsl:with-param name="footer-rows" select="@footer-row-count" />
      <xsl:with-param name="table-width" select="$width" />
      <xsl:with-param name="content">
        <xsl:apply-templates select="row"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="table-caption" priority="0.5">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="row" priority="0.5">
    <xsl:call-template name="icml-tablerow"/>
  </xsl:template>
  
  <xsl:template match="cell" priority="0.5">
    <xsl:call-template name="icml-tablecell">
      <xsl:with-param name="cellstyle-name">default cell</xsl:with-param>
      <xsl:with-param name="col-span" select="if (@colspan != '') then @colspan else 1"/>
      <xsl:with-param name="row-span" select="if (@rowspan != '') then @rowspan else 1"/>
    </xsl:call-template>
  </xsl:template>
  
  
  
  <!-- callout -->
  
  <!-- anchored frame? -->
  <xsl:template match="callout" priority="0.5">
    <xsl:call-template name="icml-textframe">
      <xsl:with-param name="framewidth" select="func:mm2pt(88)" />
      <xsl:with-param name="frameheight" select="func:mm2pt(50)"/>
      <xsl:with-param name="objectstyle-name">callout</xsl:with-param>
      <xsl:with-param name="content">
        <xsl:apply-templates/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <!-- WORKAROUND: without anchored frame -->
  <xsl:template match="callout" priority="1.0">
    <xsl:apply-templates/>
  </xsl:template>

  
  
  <!-- images -->
  
  <xsl:template match="x-csdocu-icon" priority="0.5">
    <xsl:if test="string(@xlink:href)">
      <xsl:call-template name="icml-image">
        <xsl:with-param name="href" select="@xlink:href" />
        <xsl:with-param name="objectstyle-name">x-csdocu-icon</xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <!--  <xsl:template match="image-box" priority="0.5"/>-->
  
  
 <xsl:template match="image-box" priority="0.5">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="image" priority="0.5">
    <xsl:if test="string(@xlink:href)">
      <xsl:call-template name="icml-image">
        <xsl:with-param name="href" select="@xlink:href" />
        <xsl:with-param name="objectstyle-name">image</xsl:with-param>
        <xsl:with-param name="image-width" select="func:mm2pt($std-image-width-mm)"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="image-box/caption" priority="1.0">
    <xsl:apply-templates/>
  </xsl:template>
    
    
    
  <!-- interview --> 
  
  <xsl:template match="question/person | answer/person" priority="1.0"/>
  
  <xsl:template match="question/person | answer/person" mode="interview-item">
    <xsl:call-template name="icml-inline"/>
    <xsl:text>&#x2003;</xsl:text>
  </xsl:template>
  
  <xsl:template match="question/paragraph | answer/paragraph" priority="1.0">
    <xsl:call-template name="icml-paragraph"/>
  </xsl:template>
  
  <xsl:template match="question/paragraph[not(preceding-sibling::paragraph)] | answer/paragraph[not(preceding-sibling::paragraph)]" priority="1.5">
    <xsl:call-template name="icml-paragraph">
      <xsl:with-param name="insert-at-begin">
        <xsl:apply-templates select="preceding-sibling::person" mode="interview-item"/>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <!-- 
    colspec | col | interview | interview-item | group | additional-content | new-page | layout-1 | layout-2 | layout-3 | layout-4 | image-box | image | caption | video-box | video | audio-box | audio | slideshow-box | slideshow | interactive-element-box | interactive-element | image-reveal-box | image-reveal | turntable-box | turntable | image-range-box | image-range | layout | poster | x-csdocu-prereq | x-csdocu-result | anchor | link | x-csdocu-icon
  -->
  
  
  <!-- nicht print: -->
  <!--
    app | website | teaser | teaser-text | key-visual | key-visual-text | headline | 
  -->
  
  <!-- alle: -->
  <!-- 
    article | content | suptitle | title | subtitle | text | intro | paragraph | pre | subheadline-1 | subheadline-2 | callout |
    app | website | teaser | teaser-text | key-visual | key-visual-text | headline | 
    enumeration | bullet-list | item | table | table-caption | colspec | col | row | cell | interview | interview-item | question | answer | person | hdivider | group | callout-box | additional-content | new-page | layout-1 | layout-2 | layout-3 | layout-4 | image-box | image | caption | video-box | video | audio-box | audio | slideshow-box | slideshow | interactive-element-box | interactive-element | image-reveal-box | image-reveal | turntable-box | turntable | image-range-box | image-range | layout | poster | x-csdocu-prereq | x-csdocu-result | anchor | link | bold | italic | bold-italic | underline | sup | sub | em | strong | small | x-csdocu-icon
  -->
  
  <!-- eof: common elements  -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  
  
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- basic configuration          -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  
  <!-- ressource-key of an XSLT which returns page-references of an issue asset (for crossreferences across layouts) -->
  <xsl:param name="transform-key-referencetargets"/>
  <!-- use mapping ($PSMapping for paragraphs, $CSMapping for inline elements) as default -->
  <xsl:param name="default-use-mapping" select="true()"/>
  <!-- don’t create auto styles for elements without matching mapping -->
  <xsl:param name="default-use-autostyle" select="false()"/>
  
  <!-- only needed for auto styles -->
  <xsl:variable name="autostyle-path-strip">
    <map:entry/>
  </xsl:variable>

</xsl:stylesheet>