<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">

	<xsl:param name="system"/>
	<xsl:variable name="businessUnit" select="string-join(for $x in cs:get-asset(cs:master-data('party')[@id = system-property('censhare:party-id')]/@party_asset_id)/asset_feature[@feature='canvas:bu.filter.hvl']/@value_key return tokenize($x, '\.')[last()], ', ')" />
	<xsl:template match="/">
		<xsl:message> ----- XSLT EXEC ----- <xsl:value-of select="$businessUnit"/> </xsl:message>
		<query>
			<condition name="canvas:jdeproperty.department" op="IN" sepchar="," value="{$businessUnit}"/>
			<condition name="censhare:asset.modified_date" value="n-07.00.0000-00:00:00" op="&gt;"/>
			<sortorders>
				<order ascending="true"/>
			</sortorders>
		</query>		
	</xsl:template>
</xsl:stylesheet>
