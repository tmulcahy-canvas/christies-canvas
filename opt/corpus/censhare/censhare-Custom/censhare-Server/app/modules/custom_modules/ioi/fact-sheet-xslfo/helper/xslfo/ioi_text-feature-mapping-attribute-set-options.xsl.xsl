<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:my="http://www.censhare.com/xml/3.0.0/xpath-functions/my"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:html="http://www.w3.org/1999/xhtml">
  
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>

	<xsl:template match="/">
		<options>
			<xsl:apply-templates select="cs:asset()[@censhare:resource-key = 'ioi:lot-fact-sheet-xslfo.attribute-sets']"/>
		</options>
	</xsl:template>

	<xsl:template match="asset">
		<xsl:variable name="content"/>
	    <cs:command name="com.censhare.api.io.ReadXML" returning="content">
	      <cs:param name="source" select="storage_item[@key='master'][1]"/>
	    </cs:command>

	    <xsl:apply-templates select="$content/xsl:attribute-set"/>
	</xsl:template>
	
	<xsl:template match="xsl:attribute-set">
		<option name="{@name}" key="{@name}"/>
	</xsl:template>

</xsl:stylesheet>