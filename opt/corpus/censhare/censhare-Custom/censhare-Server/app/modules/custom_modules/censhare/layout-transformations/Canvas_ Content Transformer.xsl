<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="#all" version="2.0" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:func="http://www.censhare.com/xml/3.0.0/custom-functions" xmlns:xmlutil="java:com.censhare.support.xml.CXmlUtil" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:output indent="yes"/>
	
	<xsl:strip-space elements="*"/>
	
	<xsl:preserve-space elements="paragraph italic sup sub bold bold-italic"/>
	
	<xsl:param as="element(property)?" name="propertyContent">
		<!-- Predefined for development only. Will be replaced during execution by parent transformation -->
		<!--
		<property>
			<language_handling mode="english-only"/>
			<alternate_text_colour mode=""/>
			<sale_site code="CKS" name="CKS"/>
			<!-\-lot data-\->
			<lot number="3" symboldescription="Post Sale Offsite Storage Import under TI Reduced VAT" symbolconverted="&lt;Z&gt;n&lt;/Z&gt; *" symbolcode="S01 S17" suffix=""/>
			<!-\-estimates-\->
			<estimates count="0" lines="0">
				<estimatecurrency>GBP</estimatecurrency>
				<estimatecurrencysale>GBP</estimatecurrencysale>
				<estimateeur>
					<estimatecurrencyeur>EUR</estimatecurrencyeur>
					<estimatehigheur>120000</estimatehigheur>
					<estimateloweur>840</estimateloweur>
				</estimateeur>
				<estimategbp>
					<estimatecurrencygbp>GBP</estimatecurrencygbp>
					<estimatehighgbp>100000</estimatehighgbp>
					<estimatelowgbp>700</estimatelowgbp>
				</estimategbp>
				<estimatehigh>100000</estimatehigh>
				<estimatehighsale>100000</estimatehighsale>
				<estimatehkd>
					<estimatecurrencyhkd>HKD</estimatecurrencyhkd>
					<estimatehighhkd>1100000</estimatehighhkd>
					<estimatelowhkd>7700</estimatelowhkd>
				</estimatehkd>
				<estimatelow>700</estimatelow>
				<estimatelowsale>700</estimatelowsale>
				<estimateonrequest>0</estimateonrequest>
				<estimateusd>
					<estimatecurrencyusd>USD</estimatecurrencyusd>
					<estimatehighusd>140000</estimatehighusd>
					<estimatelowusd>990</estimatelowusd>
				</estimateusd>
			</estimates>
			<!-\-meta of property asset-\->
			<metadata type="cataloguewinecasequantity" value="0"/>
			<metadata type="cataloguewinequantity" value="0"/>
			<metadata type="details" language="en" value="The doors enclosing a fitted architectural interior with drawers and niches"/>
			<metadata type="estimatecurrency" value="GBP"/>
			<metadata type="estimatecurrencysale" value="GBP"/>
			<metadata type="estimateeur" value=""/>
			<metadata type="estimategbp" value=""/>
			<metadata type="estimatehigh" value="100000"/>
			<metadata type="estimatehighsale" value="100000"/>
			<metadata type="estimatehkd" value=""/>
			<metadata type="estimatelow" value="700"/>
			<metadata type="estimatelowsale" value="700"/>
			<metadata type="estimateonrequest" value="0"/>
			<metadata type="estimateusd" value=""/>
			<metadata type="firstline" language="en" value="A CHINESE GILT-METAL MOUNTED BLACK LACQUER CABINET-ON-STAND"/>
			<metadata type="makerdate" language="en" value="THE CABINET LATE 18TH/EARLY 19TH CENTURY, THE GILTWOOD STAND DUTCH AND LATE 17TH/EARLY 18TH CENTURY"/>
			<metadata type="objectformat" value="2"/>
			<metadata type="parcelnumber" value="0"/>
			<metadata type="parcelsequence" value="0"/>
			<metadata type="prelot" language="en" value="PROPERTY FROM THE COLLECTION OF THE GHEZZI FAMILY, SWITZERLAND (LOTS 1 - 16)"/>
			<metadata type="quantity" value="1"/>
			<metadata type="size" language="en" value="58&lt;E&gt;Ω&lt;/E&gt; in. (148.5 cm.) high; 36 in. (91.5 cm.) wide; 24 in. (61 cm.) deep"/>
			<metadata type="symbolconverted" value="&lt;Z&gt;n&lt;/Z&gt;"/>
			<metadata type="symbolconverted" value="*"/>
			<text type="prelot" language="en">
				<paragraph>PROPERTY FROM THE COLLECTION OF THE GHEZZI FAMILY, SWITZERLAND (LOTS 1 - 16)</paragraph>
			</text>
			<text type="lotdetails" language="en">
				<paragraph>The doors enclosing a fitted architectural interior with drawers and niches</paragraph>
			</text>
			<text type="externalcondition" language="en">
				<paragraph>Overall with nicks, marks and scratches consistent with age and use. &lt;br&gt;With some separation to the joins, as to be expected with age and use. &lt;br&gt;Structurally secure and ready to use.&lt;br&gt;&lt;br&gt;&lt;bold&gt;The cabinet:&lt;/bold&gt;&lt;br&gt;The decoration with areas of crazing, lifting, flaking, and small losses, with some associated refreshment to the decoration, as to be expected.&lt;br&gt;The backboards with some separation to the joins with later restoration.&lt;br&gt;The mounts with some tarnish. &lt;br&gt;&lt;br&gt;&lt;bold&gt;The stand: &lt;/bold&gt;&lt;br&gt;With later blocks. &lt;br&gt;Re-decorated. The giltwood with some scattered chips, losses to carving and gilt surfaces revealing gesso, bole and bare wood in areas as to be expected with age. With some possible areas of small restored breaks which have been concealed by the later decoration. &lt;br&gt;The left-facing putto to the apron with a loss to its proper left leg. The central figure possibly later. &lt;br&gt;&lt;br&gt;&lt;br&gt;</paragraph>
			</text>
			<text type="usernotes" language="en">
				<paragraph>HILLED 6 DECEMBER 2017 AG/SD/PG/FR/PU &lt;br&gt;-PG: Chinese in Japanese style&lt;br&gt;stand originally silvered&lt;br&gt;-later blocks&lt;br&gt;-thick regilt&lt;br&gt;&lt;br&gt;Hilled Japanese Department 5/1/18&lt;br&gt;-cabinet Chinese and 19th century, untraditional interior&lt;br&gt;&lt;br&gt;CY copied into AW email to Susannah Bury 5.2.18&lt;br&gt;&lt;br&gt;&lt;italic&gt;Orlando has just looked at the proof and is convinced that LOT 3 the cabinet should be described &lt;/italic&gt;&lt;italic&gt;&lt;bold&gt;as late 18&lt;/bold&gt;&lt;/italic&gt;&lt;italic&gt;&lt;bold&gt;th&lt;/bold&gt;&lt;/italic&gt;&lt;italic&gt;&lt;bold&gt;/early 19&lt;/bold&gt;&lt;/italic&gt;&lt;italic&gt;&lt;bold&gt;th&lt;/bold&gt;&lt;/italic&gt;&lt;italic&gt;&lt;bold&gt; century&lt;/bold&gt;&lt;/italic&gt;&lt;italic&gt;, the stand is as already described &lt;/italic&gt;&lt;br&gt;&lt;italic&gt;Please kindly amend&lt;/italic&gt;&lt;br&gt;&lt;italic&gt;Thanks &lt;/italic&gt;&lt;br&gt;&lt;br&gt;</paragraph>
			</text>
		</property>
		-->
	</xsl:param>
	
	<xsl:param name="output-mode" select="'default'"/>
	
	<!--xsl:strip-space elements="*"/-->
	
	<xsl:variable name="tag-mapping">
		<Tags>
			<!-- Not avilable in phase 2 mapping -->
			<tag name="&lt;i&gt;" element="i" type="element" closing="&lt;/i&gt;"/>
			<tag name="&lt;b&gt;" element="b" type="element" closing="&lt;/b&gt;"/>
			<tag name="&lt;V&gt;" element="sup" type="element" closing="&lt;/V&gt;"/>
			<tag name="&lt;-&gt;" element="sub" type="element" closing="&lt;/-&gt;"/>
			<tag name="&lt;BR&gt;" element="BR" type="empty"/>
			<tag name="&lt;P&gt;a&lt;/P&gt;" element="P" text="a" type="element"/>
			<tag name="&lt;P&gt;b&lt;/P&gt;" element="P" text="b" type="element"/>
			<tag name="&lt;P&gt;c&lt;/P&gt;" element="P" text="c" type="element"/>
			<tag name="&lt;P&gt;d&lt;/P&gt;" element="P" text="d" type="element"/>
			<tag name="&lt;P&gt;e&lt;/P&gt;" element="P" text="e" type="element"/>
			<tag name="&lt;P&gt;f&lt;/P&gt;" element="P" text="f" type="element"/>
			<tag name="&lt;P&gt;g&lt;/P&gt;" element="P" text="g" type="element"/>
			<tag name="&lt;P&gt;h&lt;/P&gt;" element="P" text="h" type="element"/>
			<tag name="&lt;P&gt;i&lt;/P&gt;" element="P" text="i" type="element"/>
			<tag name="&lt;P&gt;j&lt;/P&gt;" element="P" text="j" type="element"/>
			<tag name="&lt;P&gt;k&lt;/P&gt;" element="P" text="k" type="element"/>
			<tag name="&lt;P&gt;l&lt;/P&gt;" element="P" text="l" type="element"/>
			<tag name="&lt;P&gt;m&lt;/P&gt;" element="P" text="m" type="element"/>
			<tag name="&lt;P&gt;n&lt;/P&gt;" element="P" text="n" type="element"/>
			<tag name="&lt;P&gt;o&lt;/P&gt;" element="P" text="o" type="element"/>
			<tag name="&lt;P&gt;p&lt;/P&gt;" element="P" text="p" type="element"/>
			<tag name="&lt;E&gt;Ç&lt;/E&gt;" element="E" text="Ç" type="element"/>
			<tag name="&lt;E&gt;ø&lt;/E&gt;" element="E" text="ø" type="element"/>
			<!-- Available in phase 2 mapping -->
			<tag name="&lt;P&gt;Q&lt;/P&gt;" element="P" text="Q" type="element"/>
			<tag name="&lt;P&gt;R&lt;/P&gt;" element="P" text="R" type="element"/>
			<tag name="&lt;P&gt;S&lt;/P&gt;" element="P" text="S" type="element"/>
			<tag name="&lt;P&gt;T&lt;/P&gt;" element="P" text="T" type="element"/>
			<tag name="&lt;P&gt;U&lt;/P&gt;" element="P" text="U" type="element"/>
			<tag name="&lt;P&gt;V&lt;/P&gt;" element="P" text="V" type="element"/>
			<tag name="&lt;P&gt;W&lt;/P&gt;" element="P" text="W" type="element"/>
			<tag name="&lt;P&gt;X&lt;/P&gt;" element="P" text="X" type="element"/>
			<tag name="&lt;P&gt;Y&lt;/P&gt;" element="P" text="Y" type="element"/>
			<tag name="&lt;P&gt;Z&lt;/P&gt;" element="P" text="Z" type="element"/>
			<tag name="&lt;P&gt;1&lt;/P&gt;" element="P" text="1" type="element"/>
			<tag name="&lt;P&gt;2&lt;/P&gt;" element="P" text="2" type="element"/>
			<tag name="&lt;P&gt;3&lt;/P&gt;" element="P" text="3" type="element"/>
			<tag name="&lt;P&gt;4&lt;/P&gt;" element="P" text="4" type="element"/>
			<tag name="&lt;P&gt;5&lt;/P&gt;" element="P" text="5" type="element"/>
			<tag name="&lt;P&gt;6&lt;/P&gt;" element="P" text="6" type="element"/>
			<tag name="&lt;P&gt;7&lt;/P&gt;" element="P" text="7" type="element"/>
			<tag name="&lt;P&gt;8&lt;/P&gt;" element="P" text="8" type="element"/>
			<tag name="&lt;E&gt;æ&lt;/E&gt;" element="E" text="æ" type="element"/>
			<tag name="&lt;E&gt;º&lt;/E&gt;" element="E" text="º" type="element"/>
			<tag name="&lt;E&gt;Ω&lt;/E&gt;" element="E" text="Ω" type="element"/>
			<tag name="&lt;E&gt;¿&lt;/E&gt;" element="E" text="¿" type="element"/>			
			<tag name="&lt;E&gt;¡&lt;/E&gt;" element="E" text="¡" type="element"/>
			<tag name="&lt;E&gt;¬&lt;/E&gt;" element="E" text="¬" type="element"/>			
			<tag name="&lt;E&gt;√&lt;/E&gt;" element="E" text="√" type="element"/>
			<tag name="&lt;E&gt;ƒ&lt;/E&gt;" element="E" text="ƒ" type="element"/>
			<tag name="&lt;E&gt;≈&lt;/E&gt;" element="E" text="≈" type="element"/>
			<!--
			<tag name="&lt;S&gt;@&lt;/S&gt;" element="S" text="@" type="element"/>
			<tag name="&lt;S&gt;*&lt;/S&gt;" element="S" text="*" type="element"/>
			<tag name="&lt;S&gt;##&lt;/S&gt;" element="S" text="##" type="element"/>
			<tag name="&lt;S&gt;⊕&lt;/S&gt;" element="S" text="⊕" type="element"/>
			<tag name="&lt;S&gt;ó&lt;/S&gt;" element="S" text="ó" type="element"/>
			<tag name="&lt;S&gt;P&lt;/S&gt;" element="S" text="P" type="element"/>
			<tag name="&lt;S&gt;s&lt;/S&gt;" element="S" text="s" type="element"/>
			<tag name="&lt;S&gt;S&lt;/S&gt;" element="S" text="S" type="element"/>
			<tag name="&lt;S&gt;X&lt;/S&gt;" element="S" text="X" type="element"/>
			<tag name="&lt;S&gt;W&lt;/S&gt;" element="S" text="W" type="element"/>
			<tag name="&lt;S&gt;a&lt;/S&gt;" element="S" text="a" type="element"/>
			<tag name="&lt;S&gt;F&lt;/S&gt;" element="S" text="F" type="element"/>
			<tag name="&lt;S&gt;l&lt;/S&gt;" element="S" text="l" type="element"/>
			<tag name="&lt;S&gt;Y&lt;/S&gt;" element="S" text="Y" type="element"/>
			<tag name="&lt;S&gt;c&lt;/S&gt;" element="S" text="c" type="element"/>
			<tag name="&lt;S&gt;p&lt;/S&gt;" element="S" text="p" type="element"/>
			<tag name="&lt;S&gt;D&lt;/S&gt;" element="S" text="D" type="element"/>
			<tag name="&lt;Z&gt;n&lt;/Z&gt;" element="Z" text="n" type="element"/>
			<tag name="&lt;Z&gt;u&lt;/Z&gt;" element="Z" text="u" type="element"/>
			-->
			<!-- Added by Shail -->
			<tag name="&lt;S&gt;∇&lt;/S&gt;" element="S" text="∇" type="element"/>
			<tag name="&lt;S&gt;σ&lt;/S&gt;" element="S" text="σ" type="element"/>
			<tag name="&lt;S&gt;ó&lt;/S&gt;" element="S" text="⌠" type="element"/>
			<tag name="&lt;Z&gt;n&lt;/Z&gt;" element="Z" text="■" type="element"/>
			<tag name="&lt;Z&gt;u&lt;/Z&gt;" element="Z" text="◆" type="element"/>
			<!--<tag name="&lt;S&gt;q&lt;/S&gt;" element="S" text="q" type="element"/>-->
			<tag name="&lt;S&gt;q&lt;/S&gt;" element="S" text="θ" type="element"/>
			<tag name="&lt;S&gt;Ψ&lt;/S&gt;" element="S" text="Ψ" type="element"/>
			<tag name="&lt;S&gt;**&lt;/S&gt;" element="S" text="**" type="element"/>
			<tag name="&lt;S&gt;*&lt;/S&gt;" element="S" text="*" type="element"/>
			<tag name="&lt;S&gt;#&lt;/S&gt;" element="S" text="#" type="element"/>
			<tag name="&lt;S&gt;D&lt;/S&gt;" element="S" text="Δ" type="element"/>
			<tag name="&lt;S&gt;l&lt;/S&gt;" element="S" text="λ" type="element"/>
			<tag name="&lt;S&gt;F&lt;/S&gt;" element="S" text="Φ" type="element"/>
			<tag name="&lt;S&gt;W&lt;/S&gt;" element="S" text="Ω" type="element"/>
			<tag name="&lt;S&gt;a&lt;/S&gt;" element="S" text="α" type="element"/>
			<tag name="&lt;S&gt;⊕&lt;/S&gt;" element="S" text="⊕" type="element"/>
			<tag name="&lt;S&gt;@&lt;/S&gt;" element="S" text="≅" type="element"/>
			<tag name="&lt;S&gt;##&lt;/S&gt;" element="S" text="##" type="element"/>
			<tag name="&lt;S&gt;c&lt;/S&gt;" element="S" text="χ" type="element"/>
			<!--<tag name="&lt;S&gt;ó&lt;/S&gt;" element="S" text="N/A" type="element"/>-->
			<tag name="&lt;S&gt;P&lt;/S&gt;" element="S" text="Π" type="element"/>
			<tag name="&lt;S&gt;p&lt;/S&gt;" element="S" text="π" type="element"/>
			<tag name="&lt;S&gt;s&lt;/S&gt;" element="S" text="σ" type="element"/>
			<tag name="&lt;S&gt;S&lt;/S&gt;" element="S" text="Σ" type="element"/>
			<tag name="&lt;S&gt;X&lt;/S&gt;" element="S" text="Ξ" type="element"/>
			<tag name="&lt;S&gt;Y&lt;/S&gt;" element="S" text="Ψ" type="element"/>
		</Tags>
	</xsl:variable>
	
	<!-- Entry point -->
	<xsl:template match="/" priority="2">
		<xsl:variable name="language_handling" select="$propertyContent/language_handling/@mode" as="xs:string?"/>
		<xsl:variable name="alternate_text_colour" select="$propertyContent/alternate_text_colour/@mode" as="xs:string?"/>
		<content>
			<xsl:variable name="content">
				<xsl:choose>
					<xsl:when test="$output-mode = 'default'">
						<xsl:choose>
							<xsl:when test="$language_handling = 'english-only'">
								<xsl:apply-templates select="node()">
									<xsl:with-param name="target_language" select="'en'" tunnel="yes"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$language_handling = 'chinese-only'">
								<xsl:apply-templates select="node()">
									<xsl:with-param name="target_language" select="'chinese'" tunnel="yes"/>
								</xsl:apply-templates>
							</xsl:when>
							<xsl:when test="$language_handling = 'en-first'">
							  <property type="{property/@type}">
							    <xsl:apply-templates select="property/node()">
							      <xsl:with-param name="target_language" select="'en'" tunnel="yes"/>
							    </xsl:apply-templates>
							    <!-- paragraph containing single space to create an additional line break. Otherwise an empty paragraph would get removed by the framework -->
							    <paragraph>
							      <xsl:text> </xsl:text>
							    </paragraph>
							  	<xsl:apply-templates select="property/node()[not(local-name() = ('cs-estimates', 'cs-lot-data', 'cs-quantity'))]">
							      <xsl:with-param name="target_language" select="'chinese'" tunnel="yes"/>
							    </xsl:apply-templates>
							  </property>
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates select="node()">
									<xsl:with-param name="target_language" select="'en'" tunnel="yes"/>
								</xsl:apply-templates>
							</xsl:otherwise>
						</xsl:choose>		
					</xsl:when>
					<xsl:when test="$output-mode = 'lot-number'">
						<xsl:variable as="element(lot)" name="lot" select="$propertyContent/lot"/>
						<lot-number-only>
							<number>
								<xsl:value-of select="$lot/@number"/>
							</number>
							<suffix>
								<xsl:value-of select="$lot/@suffix"/>
							</suffix>
						</lot-number-only>
					</xsl:when>
				</xsl:choose>								
			</xsl:variable>
			<!-- Postprocessing...
				- inject ICML FillColor attribute. Add it to all elements
				- append quantity to content of its previous paragraph 
			-->
			<xsl:apply-templates select="$content" mode="postprocess">
				<xsl:with-param name="alternate_text_colour" select="$alternate_text_colour" tunnel="yes"/>
			</xsl:apply-templates>
		</content>
	</xsl:template>
	
	<xsl:template match="cs-lot-data">
		<xsl:variable as="element(lot)" name="lot" select="$propertyContent/lot"/>
		<xsl:variable as="xs:string" name="completeEscapedString" select="$lot/@symbolconverted"/>
		<lot>
			<symbol>
				<xsl:for-each select="tokenize($completeEscapedString, ' ')">
					<xsl:variable name="escapedString" select="current()"/>
					<xsl:choose>
						<!-- Convert string into XML node if possible-->
						<xsl:when test="starts-with($escapedString, '&lt;') or starts-with($escapedString, '°&lt;') or contains($escapedString, '&lt;')">
							<!-- Make use of censhare XSLT processor's ability to use static methods of java classes. Method 'parse' returns an XML node even for an escaped string. -->
							<!--<xsl:apply-templates mode="lot-symbol" select="xmlutil:parse($escapedString)" use-when="system-property('xsl:vendor')='censhare'"/>-->
							<xsl:copy-of select="func:get-converted-tag($escapedString)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$escapedString"/> 
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</symbol>
			<number>
				<xsl:value-of select="$lot/@number"/>
			</number>
			<suffix>
				<xsl:value-of select="$lot/@suffix"/>
			</suffix>
		</lot>
	</xsl:template>

	<!-- Not required anymore -->
	<!-- Replace lot symabol codes with hex code of corresponding character -->
	<!--
	<xsl:template match="node()" mode="lot-symbol">
		<xsl:copy>
			<xsl:choose>
				<xsl:when test="text() = 'D'">
					<xsl:text>&#x00394;</xsl:text>
				</xsl:when>
				<xsl:when test="text() = 'l'">
					<xsl:text>&#x003BB;</xsl:text>
				</xsl:when>
				<xsl:when test="text() = 'F'">
					<xsl:text>&#x003A6;</xsl:text>
				</xsl:when>
				<xsl:when test="text() = 'W'">
					<xsl:text>&#x003A9;</xsl:text>
				</xsl:when>
				<xsl:when test="text() = 'a'">
					<xsl:text>&#x003B1;</xsl:text>
				</xsl:when>
				<xsl:when test="text() = 'q'">
					<xsl:text>&#x003B8;</xsl:text>
				</xsl:when>
				<xsl:when test="text() = '@'">
					<xsl:text>&#x02245;</xsl:text>
				</xsl:when>
				<xsl:when test="text() = 'n'">
					<xsl:text>&#x025A0;</xsl:text>
				</xsl:when>
			</xsl:choose>
		</xsl:copy>
	</xsl:template>
	-->

	<xsl:template match="cs-estimates">
		<xsl:variable as="element(estimates)" name="estimates" select="$propertyContent/estimates"/>
		<xsl:variable as="xs:string?" name="sale_site_code" select="$propertyContent/sale_site/@code"/>
		<xsl:variable as="xs:string*" name="sorted_estimates">
			<xsl:choose>
				<xsl:when test="$sale_site_code = 'AMS'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'CKS'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'DUB'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'GNV'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'HGK'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'MIL'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'NYC'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimategbp, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'PAR'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimategbp, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'SHA'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'ZUR'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'ECO'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimategbp, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'MUM'">
					<xsl:value-of select="func:render-estimate($estimates/estimatecurrency, $estimates/estimatelowsale, $estimates/estimatehighsale, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
				</xsl:when>
				<xsl:when test="$sale_site_code = 'ELSE'">
					<xsl:value-of select="func:render-estimate($estimates/estimategbp, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateusd, $sale_site_code)"/>
					<xsl:value-of select="func:render-estimate($estimates/estimateeur, $sale_site_code)"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<xsl:choose>
			<xsl:when test="xs:boolean($estimates/estimateonrequest/text())">
				<estimate-row position="first">
					<estimate position="first">Estimate on Request</estimate>
				</estimate-row>
			</xsl:when>
			<xsl:when test="$estimates/@count = 0">
				<!-- Do not place any estimate -->
			</xsl:when>
			<xsl:when test="$estimates/@count = 1">
				<estimate-row position="first">
					<estimate position="first">
						<xsl:value-of select="$sorted_estimates[1]"/>
					</estimate>
				</estimate-row>
			</xsl:when>
			<xsl:when test="$estimates/@count = 2 and $estimates/@lines = 1">
				<estimate-row position="first">
					<estimate position="first">
						<xsl:value-of select="$sorted_estimates[1]"/>
					</estimate>
					<xsl:copy-of select="func:render-right-align-tab()"/>
					<estimate position="second">
						<xsl:value-of select="$sorted_estimates[2]"/>
					</estimate>
				</estimate-row>
			</xsl:when>
			<xsl:when test="$estimates/@count = 2 and $estimates/@lines = 2">
				<estimate-row position="first">
					<estimate position="first">
						<xsl:value-of select="$sorted_estimates[1]"/>
					</estimate>
				</estimate-row>
				<estimate-row position="second">
					<estimate position="second">
						<xsl:value-of select="$sorted_estimates[2]"/>
					</estimate>
				</estimate-row>
			</xsl:when>
			<xsl:when test="$estimates/@count = 3 and $estimates/@lines = 2">
				<estimate-row position="first">
					<estimate position="first">
						<xsl:value-of select="$sorted_estimates[1]"/>
					</estimate>
					<xsl:copy-of select="func:render-right-align-tab()"/>
					<estimate position="second">
						<xsl:value-of select="$sorted_estimates[2]"/>
					</estimate>
				</estimate-row>
				<estimate-row position="third">
					<xsl:copy-of select="func:render-right-align-tab()"/>
					<estimate position="third">
						<xsl:value-of select="$sorted_estimates[3]"/>
					</estimate>
				</estimate-row>
			</xsl:when>
			<xsl:when test="$estimates/@count = 3 and $estimates/@lines = 3">
				<estimate-row position="first">
					<estimate position="first">
						<xsl:value-of select="$sorted_estimates[1]"/>
					</estimate>
				</estimate-row>
				<estimate-row position="second">
					<estimate position="second">
						<xsl:value-of select="$sorted_estimates[2]"/>
					</estimate>
				</estimate-row>
				<estimate-row position="third">
					<estimate position="third">
						<xsl:value-of select="$sorted_estimates[3]"/>
					</estimate>
				</estimate-row>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:function as="xs:string" name="func:render-estimate">
		<xsl:param as="element()" name="combined_estimate"/>
		<xsl:param as="xs:string" name="sale_site_code"/>
		<xsl:value-of select="func:render-estimate($combined_estimate/node()[starts-with(local-name(), 'estimatecurrency')], $combined_estimate/node()[starts-with(local-name(), 'estimatelow')], $combined_estimate/node()[starts-with(local-name(), 'estimatehigh')], $sale_site_code)"/>
	</xsl:function>
	
	<xsl:function as="xs:string" name="func:render-estimate">
		<xsl:param as="xs:string" name="currency_code"/>
		<xsl:param as="xs:double" name="low"/>
		<xsl:param as="xs:double" name="high"/>
		<xsl:param as="xs:string" name="sale_site_code"/>
		
		<xsl:variable as="element(currency)+" name="currency_symbol_map">
			<currency code="USD" symbol="US$"/>
			<currency code="USD" sale_site="NYC" symbol="$"/>
			<currency code="HKD" symbol="HK$"/>
			<currency code="GBP" symbol="£"/>
			<currency code="EUR" symbol="€"/>
			<currency code="INR" symbol="IN₹"/>
		</xsl:variable>
		
		<xsl:variable as="element(currency)" name="currency_map_entry">
			<xsl:variable as="element(currency)*" name="matching_entries" select="$currency_symbol_map[@code = $currency_code]"/>
			<xsl:choose>
				<xsl:when test="$matching_entries[@sale_site = $sale_site_code]">
					<xsl:copy-of select="$matching_entries[@sale_site = $sale_site_code]"/>
				</xsl:when>
				<xsl:when test="exists($matching_entries)">
					<xsl:copy-of select="$matching_entries[1]"/>
				</xsl:when>
				<xsl:otherwise>
					<!-- Pass through three-letter currency code -->
					<currency symbol="{$currency_code}"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable as="xs:string" name="currency_code_formatted" select="$currency_map_entry/@symbol"/>
		<xsl:variable as="xs:string" name="low_formatted" select="format-number($low, '#,###')"/>
		<xsl:variable as="xs:string" name="high_formatted" select="format-number($high, '#,###')"/>
		
		<xsl:value-of select="concat($currency_code_formatted, $low_formatted, '-', $high_formatted)"/>
	</xsl:function>
	
	<xsl:template match="cs-text">
		<xsl:param as="xs:string" name="target_language" select="'en'" tunnel="yes"/>
		
		<xsl:variable as="xs:string" name="text_type" select="@type"/>
		
		<xsl:apply-templates mode="appendTextContent" select="$propertyContent/text[@type = $text_type and @language = (if ($target_language = 'chinese') then ('CS', 'CT') else $target_language)]/node()">
			<xsl:with-param name="textType" select="$text_type" tunnel="yes"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="cs-metadata">
		<xsl:param as="xs:string" name="target_language" select="'en'" tunnel="yes"/>
		
		<xsl:variable as="xs:string" name="type" select="@type"/>
		<xsl:apply-templates mode="appendMetadata" select="$propertyContent/metadata[@type = $type and (if (@language) then @language = (if ($target_language = 'chinese') then ('CS', 'CT') else $target_language) else false())]"/>
	</xsl:template>
	
	<xsl:template match="node()" mode="appendTextContent">
		<xsl:param name="textType" tunnel="yes"/>
		<xsl:param as="xs:string" name="target_language" select="'en'" tunnel="yes"/>
		
		<xsl:copy>
			<xsl:attribute name="text-type" select="concat($textType, '-', local-name(), if ($target_language != 'en') then '-SL' else '')"/>
			<xsl:call-template name="tag-in-text">
				<xsl:with-param name="str">
					<xsl:apply-templates mode="#current" select="@*|node()"/>
				</xsl:with-param>
				<xsl:with-param name="typ">
					<xsl:value-of select="$textType"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="cs-quantity">
		<xsl:param as="xs:string" name="target_language" select="'en'" tunnel="yes"/>
		
		<xsl:copy>
			<xsl:attribute name="value" select="func:render-quantity(., $target_language)"/>
			<xsl:attribute name="descvalue" select="func:render-quantity-desc(., $target_language)"/>
		</xsl:copy>				
	</xsl:template>
	
	<xsl:function name="func:render-quantity" as="xs:string?">
		<xsl:param name="context" as="element(cs-quantity)"/>
		<xsl:param name="target_language" as="xs:string"/>
		
		<xsl:variable name="quantity" select="$propertyContent/metadata[@type = 'quantity']" as="element(metadata)?"/>
		<xsl:variable name="quantity_description" select="$propertyContent/metadata[@type = 'quantitydesc' and (if (@language) then @language = (if ($target_language = 'chinese') then ('CS', 'CT') else $target_language) else false())]" as="element(metadata)*"/>
		
		<xsl:if test="number($quantity/@value) = number($quantity/@value) and number($quantity/@value) gt 1">
			<xsl:value-of select="concat('(', $quantity/@value, ')')"/>			
		</xsl:if>
	</xsl:function>
	<xsl:function name="func:render-quantity-desc" as="xs:string?">
		<xsl:param name="context" as="element(cs-quantity)"/>
		<xsl:param name="target_language" as="xs:string"/>
		
		<xsl:variable name="quantity" select="$propertyContent/metadata[@type = 'quantity']" as="element(metadata)?"/>
		<xsl:variable name="quantity_description" select="$propertyContent/metadata[@type = 'quantitydesc' and (if (@language) then @language = (if ($target_language = 'chinese') then ('CS', 'CT') else $target_language) else false())]" as="element(metadata)*"/>
		
		<xsl:if test="number($quantity/@value) = number($quantity/@value) and number($quantity/@value) gt 1">
			<xsl:value-of select="if ($context/@add-description = 'true') then $quantity_description/@value else ()"/>			
		</xsl:if>
	</xsl:function>
	
	<xsl:template match="b[i] | i[b]" mode="appendTextContent">
		<xsl:param name="textType" tunnel="yes"/>
		
		<xsl:variable as="element(bi)" name="node">
			<bi>
				<xsl:apply-templates mode="flatten" select="node()"/>
			</bi>
		</xsl:variable>
		
		<xsl:apply-templates mode="appendTextContent" select="$node"/>
	</xsl:template>
	
	<xsl:template match="b|i" mode="flatten">
		<xsl:apply-templates select="@*|node()"/>
	</xsl:template>
	
	<xsl:template match="metadata" mode="appendMetadata">
		<xsl:param as="xs:string" name="target_language" select="'en'" tunnel="yes"/>
		<xsl:variable name="type" select="@type"/>
		<xsl:variable name="value" select="@value"/>
		<xsl:variable name="heading" select="@heading"/>
		<xsl:choose>
			<xsl:when test="not($value='')">
				<xsl:if test="$heading">
					<subheadline-1>
						<xsl:attribute name="text-type" select="concat(@type, '-subheadline-1', if ($target_language != 'en') then '-SL' else '')"/>	<xsl:value-of select="$heading"/>
					</subheadline-1>
				</xsl:if>
				<paragraph>
					<xsl:attribute name="text-type" select="concat(@type, '-paragraph', if ($target_language != 'en') then '-SL' else '')"/>
					<xsl:call-template name="tag-in-metadata">
						<xsl:with-param name="str">
							<xsl:value-of select="@value"/>
						</xsl:with-param>
						<xsl:with-param name="typ">
							<xsl:value-of select="@type"/>
						</xsl:with-param>
					</xsl:call-template>
				</paragraph>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="tag-in-text">
		<xsl:param name="str"/>
		<xsl:param name="typ"/>
		<xsl:variable name="start" select="fn:substring-before($str,'&lt;')"/>
		<xsl:variable name="rest" select="fn:substring-after($str,'&lt;')"/>
		<xsl:variable name="fulltag" select="fn:substring-before($rest,'&gt;')"/>
		<xsl:variable name="tagparts" select="fn:tokenize($fulltag,'[  &#xA;]')"/>
		<xsl:variable name="tag" select="$tagparts[1]"/>
		<xsl:variable name="aftertag" select="fn:substring-after($rest,'&gt;')"/>
		<xsl:variable name="intag" select="fn:substring-before($aftertag,fn:concat(fn:concat('&lt;/',$tag),'&gt;'))"/>
		<xsl:variable name="afterall">
			<xsl:choose>
				<xsl:when test="$tag = 'Br/' or $tag = 'br/' or $tag = 'Br' or $tag = 'br'">
					<xsl:value-of select="$aftertag"/> 
				</xsl:when>
				<xsl:otherwise >
					<xsl:value-of  select="fn:substring-after($aftertag,fn:concat(fn:concat('&lt;/',$tag),'&gt;'))"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$start"/>
		<xsl:variable name="allowedtags"><italic/><sup/><sub/><bold/><bold-italic/><Br/><br/><a><href/></a><E/><P/>
		</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="$tag">
				<xsl:variable name="currtag" select="$allowedtags/*[$tag = local-name()]"/>
				<xsl:if test="$tag='Br/' or $tag='br/' or $tag = 'Br' or $tag = 'br'">
					<xsl:element name="Br"/>
				</xsl:if>
				<xsl:if test="$currtag">
					<xsl:element name="{$currtag/local-name()}">
						<xsl:attribute name="text-type">
							<xsl:value-of select="concat($typ,'-',$currtag/local-name())"/>
						</xsl:attribute>
						<xsl:for-each select="$tagparts[position()>1]">
							<xsl:variable name="anstring" select="fn:replace(.,'^([^ &#xA;=]*)=.*$','$1')"/>
							<xsl:variable name="antag" select="$currtag/*[$anstring = local-name()]"/>
							<xsl:if test="$antag">
								<xsl:attribute name="{$antag/local-name()}">
									<xsl:value-of select="fn:replace(.,'^.*[^ &#34;]*&#34;([^&#34;]*)&#34;.*','$1')"/>
								</xsl:attribute>
							</xsl:if>
						</xsl:for-each>
						<xsl:if test="$intag">
							<xsl:call-template name="tag-in-text">
								<xsl:with-param name="str">
									<xsl:value-of select="$intag"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:if>
					</xsl:element>
				</xsl:if>
				<xsl:if test="$afterall">
					<xsl:call-template name="tag-in-text">
						<xsl:with-param name="str">
							<xsl:value-of select="$afterall"/>
						</xsl:with-param>
						<xsl:with-param name="typ">
							<xsl:value-of select="$typ"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
		  <xsl:otherwise>
		    <xsl:apply-templates>
		      <xsl:with-param name="typ" select="$typ" tunnel="yes"/>
		    </xsl:apply-templates>
		    <!--<xsl:value-of select="$str"/>-->
		  </xsl:otherwise>
		</xsl:choose>
	</xsl:template>
  
  <xsl:template match="italic | sup | sub | bold | bold-italic | P | E | S | Z">
    <xsl:param name="typ" tunnel="yes"/>
    <!--Need to map-->
    <!--<a><href/></a>-->
    <xsl:element name="{local-name()}">
      <xsl:attribute name="text-type">
        <xsl:value-of select="concat($typ,'-',local-name())"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
	
	<xsl:template match="Br | br | BR">
		<Br/>
	</xsl:template>
	
	<xsl:template match="t">
		<xsl:param name="typ" tunnel="yes"/>
		<xsl:copy-of select="func:render-right-align-tab()"/>
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template name="tag-in-metadata">
		<xsl:param name="str"/>
		<xsl:param name="typ"/>
		<xsl:variable name="start" select="fn:substring-before($str,'&lt;')"/>
		<xsl:variable name="rest" select="fn:substring-after($str,'&lt;')"/>
		<xsl:variable name="fulltag" select="fn:substring-before($rest,'&gt;')"/>
		<xsl:variable name="tagparts" select="fn:tokenize($fulltag,'[  &#xA;]')"/>
		<xsl:variable name="tag" select="$tagparts[1]"/>
		<xsl:variable name="aftertag" select="fn:substring-after($rest,'&gt;')"/>
		<xsl:variable name="intag" select="fn:substring-before($aftertag,fn:concat(fn:concat('&lt;/',$tag),'&gt;'))"/>
		<xsl:variable name="afterall">
			<xsl:choose>
				<xsl:when test="$tag = 'Br/' or $tag = 'br/' ">
					<xsl:value-of select="$aftertag"/> 
				</xsl:when>
				<xsl:otherwise >
					<xsl:value-of  select="fn:substring-after($aftertag,fn:concat(fn:concat('&lt;/',$tag),'&gt;'))"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="$start"/>
		<xsl:variable name="allowedtags"><italic/><sup/><sub/><bold/><bold-italic/><Br/><br/><a><href/></a><E/><P/><t/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$tag">
				<xsl:variable name="currtag" select="$allowedtags/*[$tag = local-name()]"/>
				<xsl:if test="$tag='Br/' or $tag='br/'">
					<xsl:element name="Br"/>
					<!-- Currently this is commented because Christie's said we can ignore the issue of <br>tag for quantity description for now as there is no valid example. :) Nothing to do or deploy here. -->
					<!-- <xsl:if test="$typ='quantitydesc'">
    					<inline-quantity>
    						<xsl:copy-of select="func:render-right-align-tab()"/>
    					</inline-quantity>
    				</xsl:if>-->
				</xsl:if>
				<xsl:if test="$currtag">
				  <xsl:choose>
				    <xsl:when test="$currtag/local-name() = 't'">
				      <xsl:copy-of select="func:render-right-align-tab()"/>
				      <xsl:for-each select="$tagparts[position()>1]">
				        <xsl:variable name="anstring" select="fn:replace(.,'^([^ &#xA;=]*)=.*$','$1')"/>
				        <xsl:variable name="antag" select="$currtag/*[$anstring = local-name()]"/>
				        <xsl:if test="$antag">
				          <xsl:attribute name="{$antag/local-name()}">
				            <xsl:value-of select="fn:replace(.,'^.*[^ &#34;]*&#34;([^&#34;]*)&#34;.*','$1')"/>
				          </xsl:attribute>
				        </xsl:if>
				      </xsl:for-each>
				      <xsl:if test="$intag">
				        <xsl:call-template name="tag-in-metadata">
				          <xsl:with-param name="str">
				            <xsl:value-of select="$intag"/>
				          </xsl:with-param>
				        </xsl:call-template>
				      </xsl:if>
				    </xsl:when>
				    <xsl:otherwise>
				      <xsl:element name="{$currtag/local-name()}">
				        <xsl:attribute name="text-type">
				          <xsl:choose>
				            <xsl:when test="@text-type = 'otherdetails-paragraph' or @text-type = 'details-paragraph'">		
				              <xsl:value-of select="concat('quantitydesc-',$currtag/local-name())"/>
				            </xsl:when>
				            <xsl:otherwise>
				              <xsl:value-of select="concat(@type,'-',$currtag/local-name())"/>
				            </xsl:otherwise>
				          </xsl:choose>
				        </xsl:attribute>
				        <xsl:for-each select="$tagparts[position()>1]">
				          <xsl:variable name="anstring" select="fn:replace(.,'^([^ &#xA;=]*)=.*$','$1')"/>
				          <xsl:variable name="antag" select="$currtag/*[$anstring = local-name()]"/>
				          <xsl:if test="$antag">
				            <xsl:attribute name="{$antag/local-name()}">
				              <xsl:value-of select="fn:replace(.,'^.*[^ &#34;]*&#34;([^&#34;]*)&#34;.*','$1')"/>
				            </xsl:attribute>
				          </xsl:if>
				        </xsl:for-each>
				        <xsl:if test="$intag">
				          <xsl:call-template name="tag-in-metadata">
				            <xsl:with-param name="str">
				              <xsl:value-of select="$intag"/>
				            </xsl:with-param>
				            <!-- Currently this is commented because Christie's said we can ignore the issue of <br>tag for quantity description for now as there is no valid example. :) Nothing to do or deploy here. -->
				            <!--<xsl:with-param name="typ" select="if ($typ='quantitydesc') then 'quantitydesc' else ()"/>-->
				          </xsl:call-template>
				        </xsl:if>
				      </xsl:element>
				    </xsl:otherwise>
				  </xsl:choose>				  
				</xsl:if>
				<xsl:if test="$afterall">
					<xsl:call-template name="tag-in-metadata">
						<xsl:with-param name="str">
							<xsl:value-of select="$afterall"/>
						</xsl:with-param>
						<!-- Currently this is commented because Christie's said we can ignore the issue of <br>tag for quantity description for now as there is no valid example. :) Nothing to do or deploy here. -->
						<!--<xsl:with-param name="typ" select="if ($typ='quantitydesc') then 'quantitydesc' else ()"/>-->
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$str"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="cs-space">
		<xsl:value-of select="@char"/>
	</xsl:template>
	
	<xsl:template match="cs-tab">
		<xsl:value-of select="'&#9;'"/>
	</xsl:template>
	
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="paragraph[following-sibling::*[position()=1][local-name()='cs-quantity']] | sizes[following-sibling::*[position()=1][local-name()='cs-quantity']]" mode="postprocess" priority="2">
		<xsl:param name="alternate_text_colour" as="xs:string?" tunnel="yes"/>
		<xsl:copy>
			<xsl:if test="$alternate_text_colour">
				<xsl:attribute name="FillColor">
					<xsl:choose>
						<xsl:when test="$alternate_text_colour = 'alt1'">Color/Christie's Alt Colour 1</xsl:when>
						<xsl:when test="$alternate_text_colour = 'alt2'">Color/Christie's Alt Colour 2</xsl:when>
						<xsl:when test="$alternate_text_colour = 'alt3'">Color/Christie's Alt Colour 3</xsl:when>
						<xsl:when test="$alternate_text_colour = 'christiesred'">Color/CHRISTIE'S RED</xsl:when>
						<xsl:when test="$alternate_text_colour = 'paper'">Color/[Paper]</xsl:when>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="@*|node()" mode="#current"/>
			<xsl:variable name="quantity_string" select="following-sibling::cs-quantity/@value" as="xs:string?"/>
			<xsl:variable name="quantity_desc" select="following-sibling::cs-quantity/@descvalue" as="xs:string?"/>
			<xsl:if test="$quantity_desc or $quantity_string">
				<inline-quantity>
					<xsl:copy-of select="func:render-right-align-tab()"/>
				</inline-quantity>
			</xsl:if>
			<xsl:if test="$quantity_desc">
			  <inline-quantitydesc>
			    <!--xsl:value-of select="concat($quantity_desc, if($quantity_string) then(' ') else())"/-->
			    <!-- Currently this is commented because Christie's said we can ignore the issue of <br>tag for quantity description for now as there is no valid example. :) Nothing to do or deploy here. -->
			    <xsl:variable name="qty_desc" select="concat($quantity_desc, if($quantity_string) then(' ') else())"/>
			    <xsl:call-template name="tag-in-metadata">
			      <xsl:with-param name="str" select="$qty_desc"/>
			      <xsl:with-param name="typ" select="'quantitydesc'"/>
			    </xsl:call-template>
			  </inline-quantitydesc>
			</xsl:if>
			<xsl:if test="$quantity_string">
				<inline-quantity>
					<xsl:value-of select="$quantity_string"/>
				</inline-quantity>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="cs-quantity" mode="postprocess">
		<!-- remove -->
	</xsl:template>
	
	<xsl:template match="node()" mode="postprocess">
		<xsl:param name="alternate_text_colour" as="xs:string?" tunnel="yes"/>
		<xsl:copy>
			<xsl:if test="self::* and $alternate_text_colour">
				<xsl:attribute name="FillColor">
					<xsl:choose>
						<xsl:when test="$alternate_text_colour = 'alt1'">Color/Christie's Alt Colour 1</xsl:when>
						<xsl:when test="$alternate_text_colour = 'alt2'">Color/Christie's Alt Colour 2</xsl:when>
						<xsl:when test="$alternate_text_colour = 'alt3'">Color/Christie's Alt Colour 3</xsl:when>
						<xsl:when test="$alternate_text_colour = 'christiesred'">Color/CHRISTIE'S RED</xsl:when>
						<xsl:when test="$alternate_text_colour = 'paper'">Color/[Paper]</xsl:when>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="@*|node()" mode="#current"/>
		</xsl:copy>
	</xsl:template>	
	
	<xsl:template match="@*" mode="postprocess">
		<xsl:copy/>
	</xsl:template>
	
	<xsl:function name="func:render-right-align-tab" as="processing-instruction(ACE)">
		<xsl:processing-instruction name="ACE">8</xsl:processing-instruction>
	</xsl:function>
	
	<xsl:function name="func:get-converted-tag">
		<xsl:param name="data"/>
		
		<xsl:analyze-string select="$data" regex="(&lt;[ibV-]&gt;)(.*)(&lt;/[ibVBR-]&gt;)">
			<xsl:matching-substring>
				<xsl:variable name="tag" select="substring(.,1,3)"/>
				<xsl:if test="$tag-mapping/Tags/tag[@name = $tag and @type='element']">
					<xsl:element name="{$tag-mapping/Tags/tag[@name = $tag and @type='element']/@element}"><xsl:copy-of select="substring-before(substring-after(.,'&gt;'),$tag-mapping/Tags/tag[@name = $tag and @type='element']/@closing)"/></xsl:element>
				</xsl:if>
			</xsl:matching-substring>
			<xsl:non-matching-substring>
				<xsl:variable name="text1" select="."/>
				<xsl:analyze-string select="$text1" regex="(&lt;[BR]+&gt;)">
					<xsl:matching-substring>
						<xsl:variable name="tag" select="."/>
						<xsl:if test="$tag-mapping/Tags/tag[@name = $tag and @type='empty']">
							<xsl:element name="{$tag-mapping/Tags/tag[@name = $tag and @type='empty']/@element}"></xsl:element>
						</xsl:if>
					</xsl:matching-substring>
					<xsl:non-matching-substring>
						<xsl:variable name="data" select="."/>
						<!-- Symbols Çø are not avilable in phase 2 mapping-->
						<xsl:analyze-string select="$data" regex="(&lt;[PESZ]&gt;)([a-z0-9A-ZÇæºΩø¿¡¬√ƒ≈Ψ@*#⊕ó∇σ])(&lt;/[PESZ]&gt;)">
							<xsl:matching-substring>
								<xsl:variable name="tag" select="."/>
								<xsl:if test="$tag-mapping/Tags/tag[@name = $tag and @type='element']">
									<xsl:variable name="tag-name" select="$tag-mapping/Tags/tag[@name = $tag and @type='element']/@element"/>
									<xsl:element name="{$tag-name}"><xsl:value-of select="$tag-mapping/Tags/tag[@name = $tag and @type='element']/@text"/></xsl:element>
								</xsl:if>
							</xsl:matching-substring>
							<xsl:non-matching-substring>
								<xsl:copy-of select="."/>
							</xsl:non-matching-substring>
						</xsl:analyze-string>
					</xsl:non-matching-substring>
				</xsl:analyze-string>
			</xsl:non-matching-substring>
		</xsl:analyze-string>
	</xsl:function>
	
</xsl:stylesheet>