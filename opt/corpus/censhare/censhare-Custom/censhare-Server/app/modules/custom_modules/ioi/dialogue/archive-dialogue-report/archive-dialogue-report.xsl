<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  exclude-result-prefixes="cs">

  <!-- Transformation that creates new asset from body XML of REST post method -->

  <!-- output -->
  <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>

  <!-- parameters -->
  <xsl:param name="data"/>
  
  <xsl:variable name="inparams">
  	<xsl:copy-of select="$data" />
  </xsl:variable>

  <!-- variables -->
  <xsl:variable name="vfs">
    <cs:command name="com.censhare.api.io.CreateVirtualFileSystem"/>
  </xsl:variable>
  
  <xsl:param name="report-name" select="$inparams/asset/@name"/>
  <xsl:param name="report-domain" select="$inparams/asset/@domain"/>
  <xsl:param name="report-domain2" select="$inparams/asset/@domain2"/>
  <xsl:param name="report-url" select="$inparams/asset/storage_item/@url"/>
  
  
  <!-- root match -->
  <xsl:template match="/">
  	<xsl:variable name="asset-xml" select="asset/cs:child-rel()[@key='user.']/cs:asset()[@censhare:asset.name=$report-name]"/>
  	
  	<xsl:variable name="report-assetid">
  		<xsl:value-of select="$asset-xml/@id"/>
  	</xsl:variable>
    <result>
      <xsl:choose>
        <!-- create new asset -->
        <xsl:when test="$data and name($data)='asset'">
          
          <xsl:choose>
          	<xsl:when test="$asset-xml and name($asset-xml)='asset'">
          		<cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="checkout-asset">
					<cs:param name="source">
					  <xsl:copy-of select="$asset-xml"/>
					</cs:param>
				</cs:command>
				<xsl:variable name="replaced-report">
					<asset>
						<xsl:copy-of select="$checkout-asset/@*"/>
						<xsl:attribute name="domain" select="$report-domain"/>
						<xsl:attribute name="domain2" select="$report-domain2"/>
						<xsl:copy-of select="$checkout-asset/node() except $checkout-asset/storage_item[@key='master'] except $checkout-asset/storage_item[@key='preview'] except $checkout-asset/storage_item[@key='thumbnail']"/>
						<storage_item key="master" element_idx="0" url="{$report-url}" mimetype="application/pdf"/>
					</asset>
				</xsl:variable>
				<cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="checkin-asset">
        			<cs:param name="source">
        				<xsl:apply-templates select="$replaced-report" mode="asset"/>
        			</cs:param>
        		</cs:command>
        		<xsl:copy-of select="$checkin-asset"/>
          	 </xsl:when>
          	 <xsl:otherwise>
          	 	 <cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="newAssetXml">
					<cs:param name="source">
					  <xsl:apply-templates select="$data" mode="asset"/>
					</cs:param>
				  </cs:command>
				  <xsl:copy-of select="$newAssetXml"/>
          	 </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <!-- no asset element in $data - return error -->
        <xsl:otherwise>
          <error message="No asset XML given">
            <xsl:copy-of select="$data"/>
          </error>
        </xsl:otherwise>
      </xsl:choose>
    </result>
  </xsl:template>
  <!-- storage_item match -->
  <xsl:template match="storage_item[@key='master']" priority="2" mode="asset">
    <storage_item>
      <xsl:copy-of select="@*"/>
      <xsl:if test="@url">
        <xsl:variable name="resultFile" select="concat($vfs, 'result.pdf')"/>
        <cs:command name="com.censhare.api.io.Copy">
          <cs:param name="source" select="@url"/>
          <cs:param name="dest" select="$resultFile"/>
        </cs:command>
        <xsl:attribute name="corpus:asset-temp-file-url" select="$resultFile"/>
      </xsl:if>
    </storage_item>
  </xsl:template>
  
  <!-- copy all other elements -->
  <xsl:template match="@*|node()" mode="asset">
    <xsl:copy><xsl:apply-templates select="@*|node()" mode="asset"/></xsl:copy>
  </xsl:template>
             
</xsl:stylesheet>
