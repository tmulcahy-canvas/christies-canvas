<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet exclude-result-prefixes="xs html censhare cs io ioi" version="2.0" 
    xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" 
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" 
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
    xmlns:html="http://www.w3.org/TR/REC-html40" 
    xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" 
    xmlns:ioi="http://www.iointegration.com" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output encoding="utf-8" indent="yes" method="xml" version="1.0"/>

	<xsl:param name="censhare:command"/>


	<xsl:template match="asset">
		<xsl:variable name="child" select="./cs:child-rel()[@key='*']/.[@type='picture.']" />
		<xsl:variable name="updateAsset">
			<asset>			
				<xsl:copy-of select="./@*" />
				<xsl:copy-of select="./node() except asset_feature[@feature='canvas:jdeproperty.imageid.internal']" />
				<xsl:for-each select="./cs:child-rel()[@key='*']/.[@type='picture.']">
					<asset_feature feature="canvas:jdeproperty.imageid.internal" value_string="{@id}"/>
				</xsl:for-each>
			</asset>
		</xsl:variable>
		<xsl:copy-of select="cs:update($updateAsset)" />
	</xsl:template>
	
	<xsl:function name="cs:update">
		<xsl:param name="asset-xml"/>
		<cs:command name="com.censhare.api.assetmanagement.Update" returning="asset">
			<cs:param name="source" select="$asset-xml"/>
		</cs:command>
		<xsl:copy-of select="$asset"/>
	</xsl:function>
</xsl:stylesheet>
