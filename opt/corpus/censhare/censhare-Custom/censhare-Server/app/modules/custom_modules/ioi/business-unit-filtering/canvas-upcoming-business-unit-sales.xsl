<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
  	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  	xmlns:xs="http://www.w3.org/2001/XMLSchema"
  	xmlns:html="http://www.w3.org/TR/REC-html40" 
  	xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" 
  	xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  	xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
  	xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io"
  	xmlns:ioi="http://www.iointegration.com"
  	exclude-result-prefixes="xs html censhare cs io ioi">
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>

	<xsl:variable name="businessUnit" select="string-join(for $x in cs:get-asset(cs:master-data('party')[@id = system-property('censhare:party-id')]/@party_asset_id)/asset_feature[@feature='canvas:bu.filter.hvl']/@value_key return tokenize($x, '\.')[last()], ', ')" />
	<xsl:template match="/">
		<xsl:message> ----- XSLT EXEC ----- <xsl:value-of select="$businessUnit"/> </xsl:message>
		<query>
			<condition name="canvas:jdesale.saledepartment" op="IN" sepchar="," value="{$businessUnit}"/>
			<condition name="canvas:jdesale.saledate" value2="n+120.00.0000-00:00:00" op2="&lt;=" value="n+00.00.0000-00:00:00" op="&gt;="/>
			<sortorders>
				<grouping mode="none"/>
				<order ascending="true" by="canvas:jdesale.saledate"/>
			</sortorders>
		</query>		
	</xsl:template>
</xsl:stylesheet>