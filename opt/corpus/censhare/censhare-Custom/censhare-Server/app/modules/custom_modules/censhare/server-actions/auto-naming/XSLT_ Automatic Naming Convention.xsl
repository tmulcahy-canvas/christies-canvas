<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:my="http://ns.censhare.de/functions"
    exclude-result-prefixes="io cs censhare">
    
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=lib:assetmanagement/storage/master/file" />
    <!--<xsl:variable name="VERBOSE" select="true()" />-->
    
    <xsl:template match="/">
        
        <xsl:variable name="asset" select="asset"/>
        
        <xsl:choose>
            <!--Issue Asset Start-->
            <xsl:when test="($asset/@type = 'issue.')">
                
                <!-- for multiple sections -->
                <xsl:for-each select="$asset/cs:child-rel()/cs:asset()[@censhare:asset.type = 'issue.section.']">
                    
                    <xsl:variable name="primary-flag-count" select="count(./asset_feature[@feature = 'canvas:primarysale' and @value_long = '1'])"/>
                    <xsl:variable name="secCount" select="count($asset/cs:child-rel()/cs:asset()[@censhare:asset.type = 'issue.section.'])"/>
                    <xsl:if test="$primary-flag-count &lt; 2">
                        <xsl:variable name="section-asset" select="."/>
                        <xsl:variable name="min-paging" select="min($asset/asset_element[exists(@paging)]/@paging)/xs:integer(.)"/>
                        <xsl:variable name="idx" select="$asset/asset_element[@paging = $min-paging]/@idx"/>
                        <xsl:variable name="first-placed-section-asset-id" select="$asset/child_asset_element_rel[@parent_idx = $idx]/@child_asset"/>
                        <xsl:variable name="count-section" select="count($asset/cs:child-rel()[@key = 'target.']/cs:asset()[@censhare:asset.type = 'issue.section.'])"/>
                        <xsl:variable name="first-section-asset" select="if ($first-placed-section-asset-id) then cs:get-asset($first-placed-section-asset-id) else if ($count-section = 1) then $asset/cs:child-rel()[@key = 'target.']/cs:asset()[@censhare:asset.type = 'issue.section.'] else ()"/>

                        <xsl:variable name="section-asset-with-primary-flag">
                                <xsl:if test="./asset_feature[@feature = 'canvas:primarysale']/@value_long = '1'">
                                    <xsl:copy-of select="."/>
                                </xsl:if>
                        </xsl:variable>
                        
                        <xsl:variable name="sale-section-asset">
                            <xsl:choose>
                                <xsl:when test="$section-asset-with-primary-flag/asset">
                                    <xsl:copy-of select="$section-asset-with-primary-flag"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:copy-of select="$first-section-asset"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        
                        <xsl:variable name="sale-asset" select="$sale-section-asset/asset/cs:child-rel()[@key = 'user.']/cs:asset()[@censhare:asset.type = 'sale.']"/>
                        <xsl:variable name="issue-asset-name" select="concat(if ($sale-asset/asset_feature[@feature = 'canvas:jdesale.salesite'][1]/@value_key) then ($sale-asset/asset_feature[@feature = 'canvas:jdesale.salesite'][1]/@value_key) else ('XXX'), if ($sale-asset/asset_feature[@feature = 'canvas:jdesale.salenumber'][1]/@value_long) then ($sale-asset/asset_feature[@feature = 'canvas:jdesale.salenumber'][1]/@value_long) else ('00000'), '_Catalogue')"/>
                        
                        <xsl:variable name="issue-asset-xml" as="element(asset)">
                            <asset>
                                <xsl:copy-of select="$asset/@* except $asset/@name"/>
                                <xsl:attribute name="name" select="$issue-asset-name"/>
                                <xsl:copy-of select="$asset/*"/>
                            </asset>
                        </xsl:variable>
                        
                        <!-- Updation of section-->
                        <xsl:variable name="sale-asset1" select="$section-asset/cs:child-rel()[@key = 'user.']/.[starts-with(@type, 'sale.')][1]"/>
                        <xsl:variable name="section-asset-xml" select="cs:get-asset($section-asset/@id)"/>
                        <xsl:variable name="location" select="$sale-asset1/asset_feature[@feature = 'canvas:jdesale.salesite']/@value_key"/>
                        <xsl:variable name="sale-number" select="$sale-asset1/asset_feature[@feature = 'canvas:jdesale.salenumber']/@value_long"/>
                        <xsl:choose>
                            <xsl:when test="exists($section-asset-xml)">
                                <xsl:sequence select="my:section-replace-name($section-asset-xml, $location, $sale-number)"/>
                            </xsl:when>
                        </xsl:choose>
                        
                        <!-- Updation of layouts within section-->
                        <xsl:for-each select="$section-asset/cs:child-rel()[@key = 'target.'or @key = 'user.']/.[starts-with(@type, 'layout.')]">
                            <xsl:variable name="layout-asset" select="."/>
                            <xsl:variable name="section-asset1" select="$section-asset"/>
                            <xsl:variable name="sale-asset2" select="$section-asset1/cs:child-rel()[@key = 'user.']/.[starts-with(@type, 'sale.')][1]"/>
                            <xsl:variable name="page-type" select="./asset_feature[@feature = 'canvas:pagetype']/@value_key"/>
                            <xsl:variable name="layout-xml" select="cs:get-asset(@id)"/>
                            <xsl:variable name="location" select="$sale-asset2/asset_feature[@feature = 'canvas:jdesale.salesite']/@value_key"/>
                            <xsl:variable name="sale-number" select="$sale-asset2/asset_feature[@feature = 'canvas:jdesale.salenumber']/@value_long"/>
                            <xsl:choose>
                                <xsl:when test="exists($layout-xml)">
                                    <xsl:sequence select="my:layout-replace-name($layout-xml, $location, $sale-number, $page-type)"/>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:for-each>
                        
                        <!-- Updation of root catalogue-->
                        <xsl:choose>
                            <xsl:when test="./asset_feature[@feature = 'canvas:primarysale']/@value_long = '1' and xs:string($secCount) &gt; '1'">
                                <xsl:sequence select="my:update-issue-asset($issue-asset-xml, $secCount, $primary-flag-count)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:sequence select="my:update-issue-asset($issue-asset-xml, $secCount, $primary-flag-count)"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:if>
                    <!--- end -->
                </xsl:for-each>
            </xsl:when>
            <!--End-->
            
            <!--Section Asset Start-->
            <xsl:when test="($asset/@type = 'issue.section.')">
                <xsl:variable name ="selectedSection" select="asset"/>
                <!--<xsl:variable name="layout-asset" select="$asset/cs:child-rel()[@key = 'target.']/.[starts-with(@type, 'layout.')][1]"/>-->
                <xsl:variable name="sale-asset" select="$asset/cs:child-rel()[@key = 'user.']/.[starts-with(@type, 'sale.')][1]"/>
                <xsl:variable name="location" select="$sale-asset/asset_feature[@feature = 'canvas:jdesale.salesite']/@value_key"/>
                <xsl:variable name="sale-number" select="$sale-asset/asset_feature[@feature = 'canvas:jdesale.salenumber']/@value_long"/>
                <xsl:variable name="section-asset-name" select="concat(if ($location != '') then $location else 'XXX', if ($sale-number != '') then $sale-number else '00000', '_Section')"/>
                <xsl:variable name="issue-asset" select="$asset/cs:parent-rel()[@key = 'target.']/.[starts-with(@type, 'issue.')][1]"/>
                <xsl:variable name="section-asset-xml" as="element(asset)">
                    <asset>
                        <xsl:copy-of select="$asset/@* except $asset/@name"/>
                        <xsl:attribute name="name" select="$section-asset-name"/>
                        <xsl:copy-of select="$asset/*"/>
                    </asset>
                </xsl:variable>
                
                <xsl:for-each select="$selectedSection/cs:child-rel()[@key = 'user.'or @key = 'target.']/.[starts-with(@type, 'layout.')]">
                    <xsl:variable name="layouts-xml" select="cs:get-asset(@id)"/>
                    <xsl:variable name="page-type" select="./asset_feature[@feature = 'canvas:pagetype']/@value_key"/>
                    <xsl:variable name="location" select="$sale-asset/asset_feature[@feature = 'canvas:jdesale.salesite']/@value_key"/>
                    <xsl:variable name="sale-number" select="$sale-asset/asset_feature[@feature = 'canvas:jdesale.salenumber']/@value_long"/>
                    <xsl:choose>
                        <xsl:when test="exists($layouts-xml)">
                            <xsl:sequence select="my:layout-replace-name($layouts-xml, $location, $sale-number, $page-type)"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:sequence select="my:update-asset($section-asset-xml)"/>
            </xsl:when>
            <!--End-->
            
            <!--Layout Asset Start-->
            <xsl:when test="($asset/@type = 'layout.')">
                <xsl:variable name="layout-asset" select="asset"/>
                <xsl:variable name="section-asset" select="$layout-asset/cs:parent-rel()[@key = 'target.' or @key = 'user.']/.[starts-with(@type, 'issue.section.')][1]"/>
                <xsl:variable name="sale-asset" select="$section-asset/cs:child-rel()[@key = 'user.']/.[starts-with(@type, 'sale.')][1]"/>
                <xsl:variable name="location" select="$sale-asset/asset_feature[@feature = 'canvas:jdesale.salesite']/@value_key"/>
                <xsl:variable name="sale-number" select="$sale-asset/asset_feature[@feature = 'canvas:jdesale.salenumber']/@value_long"/>
                <xsl:variable name="page-type" select="$layout-asset/asset_feature[@feature = 'canvas:pagetype']/@value_key"/>
                <xsl:sequence select="my:layout-replace-name($layout-asset, $location, $sale-number, $page-type)"/>
            </xsl:when>
            <!--End-->
            
        </xsl:choose>
    </xsl:template>
    
    <xsl:function name="my:section-replace-name">
        <xsl:param name="layout-asset"/>
        <xsl:param name="location"/>
        <xsl:param name="sale-number"/>
        <xsl:variable name="asset-name"
            select="
            concat(if ($location != '') then
            $location
            else
            'XXX', if ($sale-number != '') then
            $sale-number
            else
            '00000', '_Section')"/>
        <xsl:variable name="section-new-asset" as="element(asset)">
            <asset>
                <xsl:copy-of select="$layout-asset/@* except $layout-asset/@name"/>
                <xsl:attribute name="name" select="$asset-name"/>
                <xsl:copy-of select="$layout-asset/*"/>
            </asset>
        </xsl:variable>
        <xsl:sequence select="my:update-asset($section-new-asset)"/>
    </xsl:function>
    
    <xsl:function name="my:layout-replace-name">
        <xsl:param name="layout-asset"/>
        <xsl:param name="location"/>
        <xsl:param name="sale-number"/>
        <xsl:param name="page-type"/>
        <xsl:variable name="section" select="$layout-asset/asset_feature[@feature = 'canvas:pagetype']/@value_key"/>
        <xsl:variable name="pages" select="distinct-values($layout-asset/asset_element[@asset_format]/@paging)"/>
        <xsl:variable name="user-suffix" select="$layout-asset/asset_feature[@feature = 'canvas:user-suffix']/@value_string"/>
        <xsl:variable name="asset-name" select="
            concat(if ($location != '') then
            $location
            else
            'XXX', if ($sale-number != '') then
            concat($sale-number, '_')
            else
            '00000_', if ($page-type != '') then
            concat($page-type, '_')
            else('XXX_')
            , if (exists($pages)) then
            (concat(if (string-length(string(xs:long(min($pages)))) = 1) then
            (concat('000', xs:long(min($pages))))
            else
            (if (string-length(string(xs:long(min($pages)))) = 2) then
            (concat('00', xs:long(min($pages))))
            else
            (if (string-length(string(xs:long(min($pages)))) = 3) then
            (concat('0', xs:long(min($pages))))
            else
            ('0000'))), '_', if (string-length(string(xs:long(max($pages)))) = 1) then
            (concat('000', xs:long(max($pages))))
            else
            (if (string-length(string(xs:long(max($pages)))) = 2) then
            (concat('00', xs:long(max($pages))))
            else
            (if (string-length(string(xs:long(max($pages)))) = 3) then
            (concat('0', xs:long(max($pages))))
            else
            ('0000')))))
            else
            ('0000'), if ($user-suffix != '') then
            concat('_', $user-suffix)
            else
            ())"/>
        
        <xsl:variable name="checkout-layout-asset">
            <xsl:copy-of select="cs:ASSET_checkout($layout-asset)"/>
        </xsl:variable>
        <xsl:variable name="updated-asset-xml">
            <asset>
                <xsl:copy-of select="$checkout-layout-asset/asset/@*"/>
                <xsl:attribute name="name" select="$asset-name"/>
                <xsl:copy-of select="$checkout-layout-asset/asset/*"/>
            </asset>
        </xsl:variable>
        <xsl:copy-of select="cs:ASSET_checkin($updated-asset-xml)"/>
        
    </xsl:function>
    
    <xsl:function name="my:format-page">
        <xsl:param name="number" as="xs:long"/>
        <xsl:value-of
            select="
            if (string-length(string($number)) = 1) then
            concat('000', string($number))
            else
            if (string-length(string($number)) = 2) then
            concat('00', string($number))
            else
            if (string-length(string($number)) = 3) then
            concat('0', string($number))
            else
            $number"
        />
    </xsl:function>
    
    <xsl:function name="my:update-asset" as="element(asset)">
        <xsl:param name="new-asset" as="element(asset)"/>
        <xsl:variable name="result">
            <cs:command name="com.censhare.api.assetmanagement.Update">
                <cs:param name="source" select="$new-asset"/>
            </cs:command>
        </xsl:variable>
        <xsl:sequence select="$result/asset"/>
    </xsl:function>
    
    <xsl:function name="my:update-issue-asset">
        <xsl:param name="new-asset"/>
        <xsl:param name="secCount"/>
        <xsl:param name="primary-flag-count"/>
        <xsl:choose>
            <xsl:when test="$secCount &gt; 1 and $primary-flag-count = 1">
                <xsl:variable name="result">
                    <cs:command name="com.censhare.api.assetmanagement.Update">
                        <cs:param name="source" select="$new-asset"/>
                    </cs:command>
                </xsl:variable>
                <xsl:sequence select="$result/asset"/>
            </xsl:when>
            <xsl:when test="$secCount &gt; 1 and $primary-flag-count = 0"/>
            <xsl:otherwise>
                <xsl:variable name="result">
                    <cs:command name="com.censhare.api.assetmanagement.Update">
                        <cs:param name="source" select="$new-asset"/>
                    </cs:command>
                </xsl:variable>
                <xsl:sequence select="$result/asset"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
</xsl:stylesheet>
