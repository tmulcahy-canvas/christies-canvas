function controller($scope, csApiSession) {
    $scope.$watchGroup(
        [
            'asset.traits.product.includeEstimates.value[0].value',
            'asset.traits.product.approvedForDotcom.value[0].value',

        ],
        function (newVal, oldVal) {
            // console.log('NEW VAL');
            // console.log(newVal);
            // console.log('OLD VAL');
            // console.log(newVal);


            $scope.key = 'ioi:approver-from-sale';
            $scope.UserId = csApiSession.session.getUserId();
            $scope.assetId = $scope.asset.traits.ids.id.value[0].value;

            csApiSession.transformation($scope.key, {
                contextAsset: $scope.assetId
            }).then(
                (result) => {
                    $scope.approverId = parseInt(result.asset.approver.value);
                    // console.log(`Approver ID ${$scope.approverId}`);

                    if ($scope.UserId == $scope.approverId) {
                        // console.log('THIS IS TRUE');
                        $scope.visible = true;
                    } else {
                      // console.log('This is False');
                        $scope.visible = false;
                    }

                    if (newVal[0] === true && newVal[1] === true && $scope.visible === true ) {
                        // console.log('THIS IS TRUE AND THIRD FIELD SHOULD SHOW');
                        $scope.showSendUpdate = true;
                    } else {
                        // console.log('THIS IS FALSE AND SHOULD NOT LOG');
                        $scope.showSendUpdate = false;
                    }
                });
        }, true);
}