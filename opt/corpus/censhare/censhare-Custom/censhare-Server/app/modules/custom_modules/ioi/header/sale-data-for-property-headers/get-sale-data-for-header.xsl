<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet exclude-result-prefixes="xs html censhare cs io ioi" version="2.0" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:ioi="http://www.iointegration.com" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" indent="yes" method="xml" version="1.0"/>
    <xsl:param name="censhare:command"/>
    <xsl:template match="asset">
    <!-- nh <xsl:message> ***** XSLT EXECUTED ***** </xsl:message> nh -->
        <xsl:variable name="saleData">
            <root>
                <xsl:apply-templates select="./cs:parent-rel()[@key='*']/.[@type='sale.']" mode="gather"/>
            </root>
        </xsl:variable>
        <!-- nh <xsl:message> ***** SALE DATA FOR PROPERTY HEADER ***** <xsl:copy-of select="$saleData"/></xsl:message> nh -->
        <xsl:copy-of select="$saleData"/>
    </xsl:template>
    <xsl:template match="asset" mode="gather">
        <xsl:variable name="saleSite" select="./asset_feature[@feature='canvas:jdesale.salesite']/@value_key" />
        <xsl:variable name="saleNumber" select="./asset_feature[@feature='canvas:jdesale.salenumber']/@value_long" />
        <asset>
        	<saleSite value="{$saleSite}"/>
        	<saleNumber value="{$saleNumber}"/>
        </asset>
    </xsl:template>
</xsl:stylesheet>
