<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
    xmlns:my="http://www.censhare.com/xml/3.0.0/my-functions" 
    xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
    exclude-result-prefixes="#all" version="2.0">
    
    <xsl:strip-space elements="*"/>
    
	<xsl:include href="censhare:///service/assets/asset;censhare:resource-key=censhare:xml2icml-main-v2.9/storage/master/file"/>
    
    <xsl:template match="/">
        <xsl:apply-templates select="node()"/>
    </xsl:template>
    
</xsl:stylesheet>