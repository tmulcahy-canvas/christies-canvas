<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:canvas="http://integration.christies.com/Interfaces/DotCom/Notification"
    exclude-result-prefixes="#all">
    
    <!-- Transformation that creates new asset from body XML of REST post method -->
    
    <!-- output -->
    <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
    
    <!-- parameters -->
    <!--<xsl:param name="data" select="doc('censhare:///service/assets/asset;censhare:resource-key=canvas:SVCOutBoundChartOfArtExtract/storage/master/file')"/>-->
    
    <xsl:template match="/">


        <xsl:param name="data">
        <xsl:variable name="r1">
            <xsl:value-of select="asset//asset_feature[@feature='censhare:resource-key']/@value_string"/>
        </xsl:variable>
        
        <xsl:message>resource key: <xsl:value-of select="$r1"/></xsl:message>
        
        <xsl:variable name="r2">
            <xsl:value-of select="concat('censhare:///service/assets/asset;censhare:resource-key=', $r1, '/storage/master/file')"/>
        </xsl:variable>
        
        <xsl:copy-of select="doc($r2)"/>
        </xsl:param>
        
        
        <xsl:message> CoA: Source data: <xsl:copy-of select="$data"/></xsl:message>
        
        <xsl:variable name="error-flag">
            <xsl:value-of select="cs:error-checking($data)"/>
        </xsl:variable>
        
        <xsl:message>
            error flag: <xsl:value-of select="$error-flag"/>
        </xsl:message>
        
        <xsl:choose>
            <xsl:when test="$error-flag = 'false'">
                <cs:command name="com.censhare.api.transformation.XslTransformation" returning="output-xml">
                    <cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:create-xml-structure-taxonomy2/storage/master/file'"/>
                    <cs:param name="source" select="$data"/>
                </cs:command>
                
                
                <xsl:message>output file:<xsl:copy-of select="$output-xml"/></xsl:message>
                
                
                <cs:command name="com.censhare.api.transformation.XslTransformation" returning="created-asset-xml">
                    <cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:sdi_processor-for-toxonomy2/storage/master/file'"/>
                    <cs:param name="source" select="$output-xml"/>
                </cs:command>
                
                <xsl:if test="not(empty($created-asset-xml))">
                    <xsl:variable name="rest-response">
                        <SVCOutBoundChartOfArtExtract xmlns="http://integration.christies.com/Interfaces/DotCom/Notification">
                            <xsl:copy-of select="$data//canvas:Trace"/>
                            <status value="success"></status>
                            <!--<status>Success</status>
                            <statusmessage>
                                <xsl:value-of select="for $x in $created-asset-xml/result/asset return $x/@id"/>
                            </statusmessage>-->
                            <xsl:copy-of select="$data//canvas:ChartOfArt"/>
                        </SVCOutBoundChartOfArtExtract>
                    </xsl:variable>
                    <xsl:copy-of select="$rest-response"/>
                </xsl:if>
            </xsl:when>
            
            <xsl:otherwise>
                <xsl:variable name="rest-response">
                    <SVCOutBoundChartOfArtExtract xmlns="http://integration.christies.com/Interfaces/DotCom/Notification">
                        <xsl:copy-of select="$data//canvas:Trace"/>
                        <status value="error" message_id="{$data//canvas:TraceStatusID}" message="{$error-flag}"></status>
                        <!--<status>Error</status>
                        <statusmessage>
                            <xsl:value-of select="$error-flag"/>
                        </statusmessage>-->
                        <xsl:copy-of select="$data//canvas:ChartOfArt"/>
                    </SVCOutBoundChartOfArtExtract>
                </xsl:variable>
                <xsl:copy-of select="$rest-response"/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:function name="cs:error-checking">
        <xsl:param name="data"/>
        <xsl:variable name="coa-attribute-ids" select="$data//canvas:COAAttributeID"/>
        <xsl:variable name="id-extern-flag" select="distinct-values(for $x in $coa-attribute-ids return if (normalize-space($x/text()) = '') then false() else true())"/>
        <xsl:message>
            CoA: ID extern: <xsl:value-of select="$id-extern-flag"/>
        </xsl:message>
        <xsl:variable name="retrun-message">
            <xsl:if test="count($id-extern-flag) &gt; 1 or $id-extern-flag = false()">
                <xsl:text>CoA: Some COAAttributeID elements must be required.</xsl:text>
            </xsl:if>

						<!--Commented by FAC 24-02-18-->
            <!--<xsl:variable name="parent-coa-attribute-ids" select="$data//canvas:ParentCOAAttributeID[normalize-space(text())]"/>
            <xsl:variable name="parent-asset-flag" select="distinct-values(for $y in $parent-coa-attribute-ids return if (count(cs:asset()[@censhare:asset.id_extern = normalize-space($y/text())]) = 1) then true() else false())"/>
            
            <xsl:message>
                CoA: parent asset flag : <xsl:value-of select="$parent-asset-flag"/>
            </xsl:message>
            <xsl:if test="count($parent-asset-flag) &gt; 1 or $parent-asset-flag = false()">
                <xsl:text>CoA: Some ParentCOAAttributeID elements have wrong value. The wrong values are: </xsl:text>
                <xsl:variable name="wrong-external-id-of-parent-asset" select="distinct-values(for $y in $parent-coa-attribute-ids return if (count(cs:asset()[@censhare:asset.id_extern = normalize-space($y/text())]) = 1) then '' else $y)"/>
                <xsl:value-of select="$wrong-external-id-of-parent-asset"/>
            </xsl:if>-->
            
        </xsl:variable>
        <xsl:message>
            return message: <xsl:value-of select="$retrun-message"/>, 
            <xsl:value-of select="if ($retrun-message != '') then $retrun-message else 'false'"/>
        </xsl:message>
        <xsl:value-of select="if ($retrun-message != '') then $retrun-message else 'false'"/>
        
    </xsl:function>
</xsl:stylesheet>
