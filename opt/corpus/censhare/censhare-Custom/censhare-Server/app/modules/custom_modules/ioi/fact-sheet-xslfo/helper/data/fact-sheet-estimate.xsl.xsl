<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ioi="http://www.iointegration.com">  
	
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:param name="system" />
  	<xsl:param name="censhare:command-xml" />
	
	<xsl:variable name="number-of-estimates-map">
		<map>
			<map-entry key="CKS" value="3"/>
			<map-entry key="KEN" value="3"/>
			<map-entry key="NYC" value="3"/>
			<map-entry key="HGK" value="2"/>
			<map-entry key="PAR" value="3"/>
			<map-entry key="AMS" value="2"/>
			<map-entry key="GNV" value="3"/>
			<map-entry key="ZUR" value="2"/>
			<map-entry key="MIL" value="3"/>
			<map-entry key="SHA" value="2"/>
			<map-entry key="MUM" value="3"/>
			<map-entry key="ECO" value="3"/>
			<map-entry key="ELSE" value="3"/>
		</map>
	</xsl:variable>
	
	<xsl:template match="asset">

		<!-- Aggregate Content -->
		<xsl:variable name="propertyInformation"/>
		<cs:command name="com.censhare.api.transformation.XslTransformation" returning="propertyInformation">
			<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=ioi:copy_canvas:layout-transformations.content-aggregator/storage/master/file'"/>
			<cs:param name="source" select="."/>
		</cs:command>

		<!-- rg <xsl:message> ##### R.G Property Info / <xsl:copy-of select="$propertyInformation" /></xsl:message> rg -->

		<xsl:variable name="site-code" select="$propertyInformation/property/sale_site/@code"/>
		<xsl:variable name="count" select="if(exists($number-of-estimates-map/map/map-entry[@key = $propertyInformation/property/sale_site/@code])) then $number-of-estimates-map/map/map-entry[@key = $propertyInformation/property/sale_site/@code]/@value else '3'"/>
		<!-- rg <xsl:message> ##### R.G Site Code count / <xsl:value-of select="$count" /></xsl:message> rg -->
		<!-- cc - modify the returned property data to contain count.  Count in the content-aggregator pulls this data from the section asset (layout)
				  ... we will use a map to determine how many estimates will be used.- cc -->
		<xsl:variable name="property">
			<property>
  				<!-- rg <sale_site code="{$site-code}"/> rg -->

  				<xsl:choose>
  					<xsl:when test="$site-code = '' or $site-code = null">
  						<xsl:message> ###  IOI Empty Site Code, assigned ELSE. </xsl:message>
  						<sale_site code="ELSE"/>
  					</xsl:when>
  					<xsl:otherwise>
  						<xsl:message> ###  IOI Site Code Found, continuing... </xsl:message>
  						<sale_site code="{$site-code}"/>
  					</xsl:otherwise>
  				</xsl:choose>
  				<estimates count="{$count}" lines="{$count}">
  					<xsl:copy-of select="$propertyInformation/property/estimates/node()"/>
  				</estimates>
			</property>
		</xsl:variable>
		
		<!-- rg <xsl:message> ##### R.G Property ? / <xsl:copy-of select="$property" /></xsl:message> rg -->
		<!-- 
        	Get Template according to object format of property

        	cc: not using censhare's templates as we are only concered with estimation data.
        -->
		<xsl:variable name="template">
			<property type="general">
				<cs-estimates/>
			</property>
		</xsl:variable>

		<!-- rg <xsl:message> ##### R.G Template / <xsl:copy-of select="$template" /></xsl:message> rg -->

		<!-- Transform Template into Standard Structure using aggregated content-->
		<xsl:variable name="tempStructure"/>
		<cs:command name="com.censhare.api.transformation.XslTransformation" returning="tempStructure">
			<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:layout-transformations.content-transformer/storage/master/file'"/>
			<cs:param name="source" select="$template"/>
			<cs:param name="xsl-parameters">
				<cs:param name="propertyContent" select="$property/property"/>
			</cs:param>
		</cs:command>

		<!-- rg <xsl:message>   ###  FACT SHEET ESTIMATE <xsl:copy-of select="$tempStructure"/></xsl:message> rg -->

		<!-- rg <xsl:message> ##### R.G Temp Structure / <xsl:copy-of select="$tempStructure" /></xsl:message> rg -->

	  <!-- cc -<xsl:copy-of select="$tempStructure"/> we used to return whole thing.  Now one line is returned
	   for all estimates- cc -->

		<xsl:variable name="estimates">
			<estimates>
				<xsl:if test="$tempStructure/content/property/estimate-row[@position=&apos;first&apos;]/estimate/text() != ''">
					<estimate>
						<xsl:value-of select="$tempStructure/content/property/estimate-row[@position=&apos;first&apos;]/estimate/text()"/>
					</estimate>
				</xsl:if>
				<xsl:if test="$tempStructure/content/property/estimate-row[@position=&apos;second&apos;]/estimate/text() != ''">
					<estimate>
						<xsl:value-of select="$tempStructure/content/property/estimate-row[@position=&apos;second&apos;]/estimate/text()"/>
					</estimate>
				</xsl:if>
				<xsl:if test="$tempStructure/content/property/estimate-row[@position=&apos;third&apos;]/estimate/text() != ''">
					<estimate>
						<xsl:value-of select="$tempStructure/content/property/estimate-row[@position=&apos;third&apos;]/estimate/text()"/>
					</estimate>
				</xsl:if>
			</estimates>
		</xsl:variable>

		<!-- rg <xsl:message> ##### R.G The Estimates / <xsl:copy-of select="$estimates" /></xsl:message> rg -->

		<xsl:variable name="estimate-string" select="string-join($estimates/estimates/estimate/text(), ' | ')"/>

		<!-- rg <xsl:message>   ###   ESTIMATE STR - <xsl:value-of select="$estimate-string"/></xsl:message> rg -->

		<content estimate="{$estimate-string}">
		</content>

	</xsl:template>

</xsl:stylesheet>
