<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:html="http://www.w3.org/TR/REC-html40" 
  xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" 
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
  xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io"
  xmlns:ioi="http://www.iointegration.com"
  exclude-result-prefixes="xs html censhare cs io ioi">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
  <xsl:param name="censhare:command-xml"/>
  <xsl:param name="lookup"/>
  <xsl:param name="email"/>
  <xsl:param name="displayname"/>
  <xsl:param name="login"/>
  <xsl:param name="userid"/>

	<xsl:template match="/">
  		<!--<xsl:message>
			This is the value of party
			<xsl:copy-of select="cs:master-data('party')"/>
		</xsl:message>
		-->
  	<xsl:variable name="party-table">
  		<xsl:choose>
  				<xsl:when test="$lookup != ''"><!-- default for legacy -->
					<xsl:copy-of select="cs:master-data('party')[lower-case(@email)=lower-case($lookup)]"/>
				</xsl:when>
				<xsl:when test="$email != ''"><!-- email -->
					<xsl:copy-of select="cs:master-data('party')[lower-case(@email)=lower-case($email)]"/>
				</xsl:when>
				<xsl:when test="$displayname != ''"><!-- displayname -->
					<xsl:copy-of select="cs:master-data('party')[lower-case(@display_name)=lower-case($displayname)]"/>
				</xsl:when>
				<xsl:when test="$login != ''"><!-- login -->
					<xsl:copy-of select="cs:master-data('party')[lower-case(@login)=lower-case($login)]"/>
				</xsl:when>
				<xsl:when test="$userid != ''"><!-- userid -->
					<xsl:copy-of select="cs:master-data('party')[lower-case(@asset_id)=lower-case($userid)]"/>
				</xsl:when>
		</xsl:choose>
	</xsl:variable>
		<!-- cc - 

				cs:master-data() can be filtered inline just as you can with XPath
				
				cs:master-data('party')[@name='test']  <- for example

				Injecting a param (ex: $name) should work as well -> cs:master-data('party')[@name=$name]


		 - cc -->

		 <!-- cc - the party table can be returned in full and sorted on as the below example - cc -->
		<xsl:copy-of select="$party-table"/>
  </xsl:template>
</xsl:stylesheet>