<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
  xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
  xmlns:csc="http://www.censhare.com/censhare-custom"
  exclude-result-prefixes="#all">

	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:elementtofeaturegeneratorfastlane/storage/master/file"/>

	<xsl:param name="data"/>
	<xsl:output indent="yes" omit-xml-declaration="yes"/>

	<!-- asset match -->
	<xsl:template match="/">

<xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<StartOfRESTProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message>

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
			<xsl:copy-of select="$data" />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

		<xsl:variable name="inXML">
			<xsl:copy-of copy-namespaces="no" select="$data" />
		</xsl:variable>

		<!-- Process the XML from Biztalk and store the output into a variable -->
		<xsl:variable name="outXML">
			<xsl:if test="$inXML/ns0:CANVASObjectDataRequest_FastLane">
				<xsl:copy>
					<xsl:apply-templates select="$inXML/ns0:CANVASObjectDataRequest_FastLane/Item"/>
				</xsl:copy>
			</xsl:if>
		</xsl:variable>

		<ns0:CANVASObjectDataResponse_FastLane xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/ObjectDataResponse_FastLane">
				<Item>
					<xsl:copy-of select="$inXML/*/Item/szUniqueKeyIDString_UKIDSZ" copy-namespaces="no"/>
					<xsl:copy-of select="$outXML"  copy-namespaces="no" />
					<xsl:copy-of select="$inXML/*/Item/* except ($inXML/ns0:CANVASObjectDataRequest_FastLane/Item/szUniqueKeyIDString_UKIDSZ)"  copy-namespaces="no" />
				</Item>
		</ns0:CANVASObjectDataResponse_FastLane>

<xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<EndOfRESTProcessing/>
		<xsl:value-of select="'**************************'" />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message>


		<!--xsl:value-of select="'&#xA;'" />
		<xsl:copy-of select="$outXML" />
		<xsl:value-of select="'&#xA;'" />
		<xsl:value-of select="'&#xA;'" /-->

		<!-- ################ UNCOMMENT BELOW -->
		
		<!--xsl:variable name="responseXML">
			<xsl:element name="ns0:CANVASObjectDataResponse" namespace="http://integration.christies.com/Interfaces/CANVAS/ObjectDataResponse">
				<xsl:element name="Item">
					<xsl:copy-of select="$inXML/ns0:CANVASObjectDataRequest/Item/szUniqueKeyIDString_UKIDSZ" />
					<xsl:copy-of select="$outXML" />
					<xsl:copy-of select="$inXML/ns0:CANVASObjectDataRequest/Item/* except ($inXML/ns0:CANVASObjectDataRequest/Item/szUniqueKeyIDString_UKIDSZ)" />
				</xsl:element>
			</xsl:element>
		</xsl:variable>

		<xsl:copy-of copy-namespaces="no" select="$responseXML" /-->

			<!--ns0:CANVASObjectDataResponse namespace="http://integration.christies.com/Interfaces/CANVAS/ObjectDataResponse">
			<xsl:variable name="output1">
			<xsl:element name="ns0:CANVASObjectDataResponse" namespace="http://integration.christies.com/Interfaces/CANVAS/ObjectDataResponse">
				<Item>
					<xsl:copy-of select="$inXML/ns0:CANVASObjectDataRequest/Item/szUniqueKeyIDString_UKIDSZ" />
					<xsl:copy-of select="$outXML" />
					<xsl:copy-of select="$inXML/ns0:CANVASObjectDataRequest/Item/* except ($inXML/ns0:CANVASObjectDataRequest/Item/szUniqueKeyIDString_UKIDSZ)" />
				</Item>
			</xsl:element>
			</xsl:variable>
			
        <xsl:element name="ns0:CANVASObjectDataResponse" namespace="http://integration.christies.com/Interfaces/CANVAS/ObjectDataResponse">
        <xsl:copy-of select="$output1/*/Item" copy-namespaces="no" exclude-result-prefixes="#all"/>
        </xsl:element-->



			<!--/ns0:CANVASObjectDataResponse-->


		
		<!-- ################ UNCOMMENT ABOVE -->

	</xsl:template>

	<!--xsl:template match="ns0:CANVASObjectDataRequest">
		<xsl:copy>
			<xsl:apply-templates select="Item"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="Item">
		<xsl:copy>
			<xsl:for-each select="*">
				<xsl:choose>
					<xsl:when test="local-name(.)='szUniqueKeyIDString_UKIDSZ'">
						<xsl:copy-of select="."/>
						<xsl:comment>Added by censhare upon success</xsl:comment>
						<status value="success"></status>
						<xsl:comment>End of success message</xsl:comment>						
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="."/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</xsl:copy>
	</xsl:template-->

</xsl:stylesheet>
