<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:canvas="censhare.com"
    xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/ObjectDataRequest"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
		xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
		xmlns:csc="http://www.censhare.com/censhare-custom"
		xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
		exclude-result-prefixes="#all">

	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvasmetadatavalidations/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:propertysalemappings/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvasrelateimagestoproperty/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvaspropertytaxonomymappings/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvaspropertycheckandupdate/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvasinitialchecks/storage/master/file"/>

	<xsl:strip-space elements="*"/>

		<xsl:character-map name="brackets">
			<xsl:output-character character="&lt;" string="&lt;"/>
			<xsl:output-character character=">" string="&gt;"/>
		</xsl:character-map>

	<xsl:template match="/">
		<xsl:apply-templates select="ns0:CANVASObjectDataRequest"/>
	</xsl:template>

	<xsl:template match="ns0:CANVASObjectDataRequest/Item">
	
<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<StartBeforeInitialChecks />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->
	
		<xsl:variable name="currentItemXML" select="." />
		<xsl:variable name="genericConfigXMLAssetExisting" select="cs:get-asset-resources(asset)[@censhare:resource-key='christies:jdeinterfacebiztalkcanvaselementtofeaturemapping']" />
		<xsl:variable name="genericErrorWarningXMLAsset" select="cs:get-asset-resources(asset)[@censhare:resource-key='christies:biztalkcanvasloghandle']" />

		<xsl:variable name="elementToFeaturesConfExisting" select="cs:get-asset($genericConfigXMLAssetExisting/@asset_id)" />
		<xsl:variable name="errorWarningConf" select="cs:get-asset($genericErrorWarningXMLAsset/@asset_id)" />


				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************configXMLExisting START'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

		<xsl:variable name="configXMLExisting">
			<xsl:if test="starts-with($elementToFeaturesConfExisting/@type, 'module.config.')">
				<xsl:variable name="storage" select="$elementToFeaturesConfExisting/storage_item[@key='master'][1]"/>
				<!--xsl:if test="exists($storage)"-->
				<xsl:if test="$storage">
					<xsl:copy-of select="doc($storage/@url)"/>
				</xsl:if>
			</xsl:if>
		</xsl:variable>

		<xsl:variable name="logConfigXML">
			<xsl:if test="starts-with($errorWarningConf/@type, 'module.config.')">
				<xsl:variable name="storage" select="$errorWarningConf/storage_item[@key='master'][1]"/>
				<!--xsl:if test="exists($storage)"-->
				<xsl:if test="$storage">
					<xsl:copy-of select="doc($storage/@url)"/>
				</xsl:if>
			</xsl:if>
		</xsl:variable>

		<xsl:variable name="shortItemNumber" select="./ShortItemNumber" />
		<xsl:variable name="itemVersion" select="./ItemVersion" />
		<xsl:variable name="propertyIDExt" select="concat('property:',./ShortItemNumber,./ItemVersion)" />

		<!-- ADDING VALUE KEY DYNAMICALLY - Check for Currency code value existence -->
		<xsl:variable name="configXML">
			<xsl:choose>
				<xsl:when test="normalize-space($currentItemXML/CurrencyCode)">
					<xsl:copy-of select="canvas:currencyMetadataGenerator(normalize-space($currentItemXML/CurrencyCode),$configXMLExisting,$elementToFeaturesConfExisting)" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="$configXMLExisting" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************configXMLExisting END'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

		
		<!-- ADDING VALUE KEY DYNAMICALLY - Check for Symbol code value existence -->
		<!--xsl:variable name="symbolcodeUpdated">
		<xsl:if test="normalize-space($currentItemXML/SymbolCode)">
			<xsl:copy-of select="canvas:featureValuesUpdatesInMasterData(normalize-space(name($currentItemXML/SymbolCode)),normalize-space($currentItemXML/SymbolCode),$configXML)" />
		</xsl:if>
		</xsl:variable-->
		<!-- ADDING VALUE KEY DYNAMICALLY - Check for Symbol code value existence -->
		<!--xsl:variable name="symbolcodeUpdated">
			<xsl:if test="$currentItemXML/SymbolCode">
				<xsl:for-each select="$currentItemXML/SymbolCode">
					<xsl:copy-of select="canvas:featureValuesUpdatesInMasterData(name(.),.,$configXML)" />
				</xsl:for-each>
			</xsl:if>
		</xsl:variable-->

		<!-- Generate Asset Name -->
		<xsl:variable name="objectFormatValue" select="$currentItemXML/ObjectFormat" />
		<xsl:variable name="propertyAssetName">
			<xsl:choose>
				<xsl:when test="$objectFormatValue = '2' or $objectFormatValue = '7' or $objectFormatValue = '4'">
					<xsl:choose>
						<xsl:when test="($currentItemXML/FST)[normalize-space($currentItemXML/FST)]">
							<xsl:value-of select="substring(canvas:tagsCanvasMapping($currentItemXML/FST), 1, 200)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat('_',$propertyIDExt)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$objectFormatValue = '3'">
					<xsl:choose>
						<xsl:when test="($currentItemXML/AD)[normalize-space($currentItemXML/AD)]">
							<xsl:value-of select="substring(canvas:tagsCanvasMapping($currentItemXML/AD), 1, 200)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat('_',$propertyIDExt)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$objectFormatValue = '5'">
					<xsl:choose>
						<xsl:when test="($currentItemXML/DT1)[normalize-space($currentItemXML/DT1)]">
							<xsl:value-of select="substring(canvas:tagsCanvasMapping($currentItemXML/DT1), 1, 200)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat('_',$propertyIDExt)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$objectFormatValue = '6'">
					<xsl:choose>
						<xsl:when test="($currentItemXML/TIT)[normalize-space($currentItemXML/TIT)]">
							<xsl:value-of select="substring(canvas:tagsCanvasMapping($currentItemXML/TIT), 1, 200)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat('_',$propertyIDExt)"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat('_',$propertyIDExt)"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!-- Set Language of the Property -->
		<xsl:variable name="basePropertyLanguage">
			<xsl:choose>
				<xsl:when test="normalize-space($currentItemXML/BaseLanguage) and ($currentItemXML/BaseLanguage = '0')">
					<xsl:choose>
						<xsl:when test="normalize-space($currentItemXML/LanguagePreference)">
							<xsl:value-of select="normalize-space($currentItemXML/LanguagePreference)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'en'"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'en'"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<EndBeforeInitialChecks />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->


		<!-- INITIAL CHECKINGS FOR THE INPUT AND STATIC XMLS -->
		<xsl:variable name="initialCheckErrors">

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************initialCheckings START'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message>

			<xsl:copy-of select="canvas:initialCheckings($currentItemXML,$configXML)" />

				<xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************initialCheckings END'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

		</xsl:variable>
		
		<xsl:choose>
		<xsl:when test="not(normalize-space($initialCheckErrors))">

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************propertyAsset CHECK START'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

			<xsl:variable name="propertyAsset" select="cs:asset()[@censhare:asset.id_extern=$propertyIDExt and @censhare:asset.type='product.' and @censhare:asset.currversion=0]" />

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************propertyAsset CHECK END'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->


				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************PROPERTY ASSET'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:copy-of select="$propertyAsset" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

		<xsl:choose>
			<!--xsl:when test="exists($propertyAsset)"-->
			<xsl:when test="($propertyAsset)">
				<!--xsl:variable name="propertyAsset" select="cs:asset()[@censhare:asset.id_extern=$propertyIDExt and @censhare:asset.type='product.' and @censhare:asset.currversion=0]" /-->

					<!-- CHECK IF THE ASSET IS ALREADY CHECKED OUT BY ANY USER -->
					<xsl:choose>
						<xsl:when test="$propertyAsset/@checked_out_by">
							<xsl:comment>Added by censhare upon errors</xsl:comment>
							<xsl:variable name="errorid" select="'E007'" /> 
							<status value="error" message_id="{$errorid}" message="{$logConfigXML/notificationlog/error/code[@id=$errorid]}"></status>
							<xsl:value-of select="'&#xA;'" />
							<xsl:comment>End of error message</xsl:comment>
						</xsl:when>
						<xsl:otherwise>

					<!--xsl:variable name="propertyTransmissionDate" select="substring-before($propertyAsset/asset_feature[@feature='canvas:jdeproperty.transmissiondate']/@value_timestamp,'T')" /-->
					<!--xsl:variable name="propertyTransmissionTime" select="substring-after($propertyAsset/asset_feature[@feature='canvas:jdeproperty.transmissiontime']/@value_timestamp,'T')" /-->
					<xsl:variable name="existingPropertyTransmissionDate" select="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.transmissiondate']/@value_timestamp" />
					<xsl:variable name="propertyTransmissionDate" select="cs:format-date(xs:dateTime($existingPropertyTransmissionDate),'short','none')" />

					<xsl:variable name="existingPropertyTransmissionTime" select="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.transmissiontime']/@value_timestamp" />
					<xsl:variable name="propertyTransmissionTime" select="cs:format-date(xs:dateTime($existingPropertyTransmissionTime),'none','short')" />

					<!--xsl:variable name="currentTransmmissionDate" select="substring-before(canvas:dateFormatChange($currentItemXML/TransmissionDate),'T')" /-->
					<xsl:variable name="TransmissionDateValue" select="concat(substring(normalize-space($currentItemXML/TransmissionDate), 5, 4), '-', substring(normalize-space($currentItemXML/TransmissionDate), 1, 2), '-', substring(normalize-space($currentItemXML/TransmissionDate), 3, 2),'T00:00:00Z')"/>
					<xsl:variable name="currentTransmmissionDate" select="cs:format-date(xs:dateTime($TransmissionDateValue),'short','none')" />

					<xsl:variable name="itemXMLTransmissionTime" select="normalize-space($currentItemXML/TransmissionTime)" />
					<xsl:variable name="formattedCurrentTransmmissionTime">
						<xsl:choose>
							<xsl:when test="matches($itemXMLTransmissionTime, '^(\d{6})$')">
								<xsl:value-of select="concat('0000-00-00T', substring($itemXMLTransmissionTime, 1, 2), ':', substring($itemXMLTransmissionTime, 3, 2), ':', substring($itemXMLTransmissionTime, 5, 2),'Z')"/>
							</xsl:when>
							<xsl:when test="matches($itemXMLTransmissionTime, '^(\d{5})$')">
								<xsl:value-of select="concat('0000-00-00T',0, substring($itemXMLTransmissionTime, 1, 1), ':', substring($itemXMLTransmissionTime, 2, 2), ':', substring($itemXMLTransmissionTime, 4, 2),'Z')"/>
							</xsl:when>
							<xsl:when test="matches($itemXMLTransmissionTime, '^(\d{4})$')">
								<xsl:value-of select="concat('0000-00-00T', substring($itemXMLTransmissionTime, 1, 2), ':', substring($itemXMLTransmissionTime, 3, 2), ':00Z')"/>
							</xsl:when>
							<xsl:when test="not($itemXMLTransmissionTime)">
								<xsl:value-of select="''"/>
							</xsl:when>
						</xsl:choose>
					</xsl:variable>
					<xsl:variable name="currentTransmmissionTime" select="cs:format-date(xs:dateTime($formattedCurrentTransmmissionTime),'none','short')" />

				<xsl:variable name="existDatePart" select="substring-before($existingPropertyTransmissionDate,'T')" />
				<xsl:variable name="existTimePart" select="substring-after($existingPropertyTransmissionTime,'T')" />
				<xsl:variable name="existingPropDateTime" select="concat($existDatePart,'T',$existTimePart)" />
				
				<xsl:variable name="currentDatePart" select="substring-before($TransmissionDateValue,'T')" />
				<xsl:variable name="currentTimePart" select="substring-after($formattedCurrentTransmmissionTime,'T')" />
				<xsl:variable name="currentPropDateTime" select="concat($currentDatePart,'T',$currentTimePart)" />

						<xsl:choose>
							<!--xsl:when test="($propertyTransmissionDate &lt; $currentTransmmissionDate) or (($propertyTransmissionDate = $currentTransmmissionDate) and ($propertyTransmissionTime &lt; $currentTransmmissionTime))"-->

							<xsl:when test="$existingPropDateTime &lt; $currentPropDateTime">

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************updatedAssetInformations START'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

				<xsl:variable name="updatedAssetInformations" select="canvas:checkAndUpdateAssets($propertyAsset,$configXML,$currentItemXML,$basePropertyLanguage)" />

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************updatedAssetInformations END'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

				<xsl:variable name="NewupdatedAssetInformations">
					<xsl:element name="asset">
						<xsl:copy-of select="$updatedAssetInformations"/>
						<xsl:for-each select="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.imageid.internal']">
							<excludeElements childAsset="{./@value_string}" key="user." />
						</xsl:for-each>
					<excludeElements feature="canvas:lotnumberandsuffix"/>
					</xsl:element>	
				</xsl:variable>

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************NewupdatedAssetInformations'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:copy-of select="$NewupdatedAssetInformations" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

	  			<xsl:variable name="warningMessages">
	  				<xsl:for-each select="$NewupdatedAssetInformations/asset/warningElements">
						<xsl:variable name="warningid" select="./@id" />
						<status value="warning" message_id="{$warningid}" message="{$logConfigXML/notificationlog/warning/code[@id=$warningid]}"><xsl:value-of select="." /></status>
						<xsl:value-of select="'&#xA;'" />
					</xsl:for-each>
	  			</xsl:variable>

				<xsl:variable name="assetXML">
					<xsl:for-each select="$NewupdatedAssetInformations/asset/* except ($NewupdatedAssetInformations/asset/excludeElements,$NewupdatedAssetInformations/asset/warningElements)">
						<xsl:copy-of select="."/>
					</xsl:for-each>
				</xsl:variable>

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************UPDATED PROPERTY ASSET'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:copy-of select="$assetXML" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

				<!--xsl:variable name="propertyAssetUpdated" select="cs:asset('sql=true')[@censhare:asset.id_extern=$propertyIDExt and @censhare:asset.type='product.' and @censhare:asset.currversion=0]" /-->
				<xsl:variable name="propertyAssetUpdated" select="$propertyAsset" />

				<xsl:variable name="filteredAssetXML">
					
					<xsl:choose>
					<!--xsl:when test="$propertyAssetUpdated//asset_feature/asset_feature"-->
					<xsl:when test="$propertyAssetUpdated//*">

					<!-- ############ NEED TO CONFIGURE THIS (FEATURES HAVING PARENT FEATURES)-->
					<xsl:for-each select="$propertyAssetUpdated//asset_feature/asset_feature">

						<xsl:variable name="CurrentFeature" select="../@feature"/>
						<xsl:variable name="CurrentChildFeature" select="@feature"/>

						<asset_feature feature="{$CurrentFeature}">

						<xsl:variable name="preresult">
							<xsl:variable name="parentExcludeFeature" select="$NewupdatedAssetInformations/asset/excludeElements[@parentFeature=$CurrentFeature and @feature=$CurrentChildFeature]"/>

							<xsl:if test="$parentExcludeFeature">
								<xsl:for-each select="$parentExcludeFeature">
										<xsl:if test=".[@parentFeature=$CurrentFeature and @feature=$CurrentChildFeature]">
											<xsl:copy-of select="$propertyAssetUpdated//asset_feature[@feature=$CurrentFeature]/asset_feature[@feature=$CurrentChildFeature]"/>
										</xsl:if>
								</xsl:for-each>
							</xsl:if>
						</xsl:variable>

							<xsl:if test="not($preresult/*)">
								<xsl:copy-of select="." />
							</xsl:if>
						</asset_feature>

					</xsl:for-each>

					<!-- ############ -->
						<xsl:for-each select="$propertyAssetUpdated/asset_feature except ($propertyAssetUpdated/asset_feature[@feature='canvas:jdeproperty.coaattributeid'] | $propertyAsset/asset_feature[@feature='canvas:jdeproperty.imageid.internal'] | $propertyAssetUpdated/asset_feature[@feature='canvas:jdeproperty.coaattributeid'] | $propertyAssetUpdated/asset_feature[asset_feature])">
						<xsl:variable name="existingFeatureID" select="./@feature" />
						<xsl:variable name="existingLanguage" select="./@language" />
						<xsl:variable name="preresult">
						<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements">
							<xsl:choose>
							<xsl:when test="normalize-space(./@language)">
								<xsl:if test="./@feature = $existingFeatureID and ./@language = $existingLanguage">
									<xsl:value-of select="$existingFeatureID" />
								</xsl:if>
							</xsl:when>
							<xsl:when test="./@feature = $existingFeatureID">
								<xsl:value-of select="$existingFeatureID" />
							</xsl:when>
							</xsl:choose>
						</xsl:for-each>
						</xsl:variable>
						
						<xsl:if test="not(normalize-space($preresult))">
							<xsl:copy-of select="." />
						</xsl:if>
					</xsl:for-each>

								<!--xsl:for-each select="$propertyAssetUpdated/asset_feature[@language] except ($propertyAssetUpdated/asset_feature[@feature='canvas:jdeproperty.coaattributeid'] | $propertyAsset/asset_feature[@feature='canvas:jdeproperty.imageid.internal'] | $propertyAssetUpdated/asset_feature[@feature='canvas:jdeproperty.coaattributeid'] | $propertyAssetUpdated/asset_feature[asset_feature])">
									<xsl:copy-of select="." />
								</xsl:for-each-->

					<!-- ############ -->
					<xsl:for-each select="$propertyAssetUpdated/child_asset_rel">
						<xsl:variable name="existingParentID" select="./@parent_asset" />
						<xsl:variable name="existingFeatureID" select="./@child_asset" />
						<xsl:variable name="existingChildAssetKey" select="./@key" />
						<xsl:variable name="preresult">
						<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements">
							<!--xsl:if test="(./@childAsset = $existingFeatureID and ./@key=$existingChildAssetKey) or (./@child_asset = $existingFeatureID and ./@key=$existingChildAssetKey)"-->
							<xsl:if test="./@childAsset = $existingFeatureID and ./@key=$existingChildAssetKey">
								<xsl:copy-of select="." />
							</xsl:if>
						</xsl:for-each>
							<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements/child_asset_rel">
								<xsl:if test="./@child_asset = $existingFeatureID and ./@parent_asset = $existingParentID and ./@key=$existingChildAssetKey">
									<xsl:copy-of select="." />
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						
						<xsl:if test="not($preresult/*)">
							<xsl:copy-of select="." />
						</xsl:if>
					</xsl:for-each>

					<!-- ############ -->
					<xsl:for-each select="$propertyAssetUpdated/asset_element">
							<xsl:copy-of select="." />
					</xsl:for-each>


					<!-- ############ -->
					<xsl:for-each select="$propertyAssetUpdated/child_asset_element_rel">
						<xsl:variable name="existingParentID" select="./@parent_asset" />
						<xsl:variable name="existingFeatureID" select="./@child_asset" />
						<xsl:variable name="existingChildAssetKey" select="./@key" />
						<xsl:variable name="preresult">
						<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements">
							<xsl:if test="./@childAsset = $existingFeatureID and ./@key=$existingChildAssetKey">
								<!--xsl:value-of select="$existingFeatureID" /-->
								<xsl:copy-of select="." />
							</xsl:if>
						</xsl:for-each>
							<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements/child_asset_element_rel">
								<xsl:if test="./@child_asset = $existingFeatureID and ./@parent_asset = $existingParentID and ./@key=$existingChildAssetKey">
									<xsl:copy-of select="." />
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						
						<xsl:if test="not($preresult/*)">
							<xsl:copy-of select="." />
						</xsl:if>
					</xsl:for-each>

					<!-- ############ -->
					<xsl:for-each select="$propertyAssetUpdated/parent_asset_element_rel">
						<xsl:variable name="existingchildID" select="./@child_asset" />
						<xsl:variable name="existingFeatureID" select="./@parent_asset" />
						<xsl:variable name="existingParentAssetKey" select="./@key" />
						<xsl:variable name="preresult">
							<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements">
								<xsl:if test="./@parentAsset = $existingFeatureID and ./@key=$existingParentAssetKey">
									<xsl:copy-of select="." />
								</xsl:if>
							</xsl:for-each>
							<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements/parent_asset_element_rel">
								<xsl:if test="./@child_asset = $existingchildID and ./@parent_asset = $existingFeatureID and ./@key=$existingParentAssetKey">
									<xsl:copy-of select="." />
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
						<xsl:if test="not($preresult/*)">
							<xsl:copy-of select="." />
						</xsl:if>
					</xsl:for-each>

					<!-- ############ -->
					<!--xsl:for-each select="$propertyAssetUpdated/parent_asset_rel/* except ($propertyAssetUpdated/parent_asset_rel[@parent_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@parent_asset and @child_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@child_asset]/asset_rel_feature)"-->
					<xsl:for-each select="$propertyAssetUpdated/parent_asset_rel">
						<xsl:choose>
							<xsl:when test="./asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix'] or ./asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']">
								<xsl:variable name="preresult">
									<xsl:if test="$propertyAssetUpdated/parent_asset_rel[@parent_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@parent_asset and @child_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@child_asset]">
										<xsl:copy-of select="$propertyAssetUpdated/parent_asset_rel[@parent_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@parent_asset and @child_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@child_asset]" />
									</xsl:if>
								</xsl:variable>
								<xsl:if test="not($preresult/*)">
									<xsl:copy-of select="$propertyAssetUpdated/parent_asset_rel[@parent_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@parent_asset and @child_asset=$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel/@child_asset]" />
								</xsl:if>
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="existingchildID" select="./@child_asset" />
								<xsl:variable name="existingFeatureID" select="./@parent_asset" />
								<xsl:variable name="existingParentAssetKey" select="./@key" />
								<xsl:variable name="preresult">
								<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements">
									<xsl:if test="./@parentAsset = $existingFeatureID and ./@key=$existingParentAssetKey">
										<!--xsl:value-of select="$existingFeatureID" /-->
										<xsl:copy-of select="." />
									</xsl:if>
								</xsl:for-each>
									<xsl:for-each select="$NewupdatedAssetInformations/asset/excludeElements/parent_asset_rel">
										<xsl:if test="./@child_asset = $existingchildID and ./@parent_asset = $existingFeatureID and ./@key=$existingParentAssetKey">
											<xsl:copy-of select="." />
										</xsl:if>
									</xsl:for-each>
								</xsl:variable>
								<xsl:if test="not($preresult/*)">
									<xsl:copy-of select="." />
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>

					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="$propertyAssetUpdated//* except ($propertyAssetUpdated/asset_feature,$propertyAssetUpdated/child_asset_rel,$propertyAssetUpdated/child_asset_element_rel,$propertyAssetUpdated/parent_asset_element_rel,$propertyAssetUpdated/parent_asset_rel)" />
						<!--xsl:copy-of select="." /-->
					</xsl:otherwise>
					</xsl:choose>
					<xsl:copy-of select="$assetXML" />
				<!--xsl:if test="$currentItemXML/LotNumber or $currentItemXML/LotSuffix"-->
								<asset_feature feature="canvas:lotnumberandsuffix" value_string="{$currentItemXML/LotNumber}{$currentItemXML/LotSuffix}" />
							<!--/xsl:if-->

				</xsl:variable>


				<xsl:variable name="concludingXMLNodes">
	  				<xsl:variable name="updateAssetFeatures" select="$filteredAssetXML/*[@feature]"/>

	  				<xsl:copy-of select="$filteredAssetXML/*[not(@feature)]"/>
					<xsl:for-each select="distinct-values($updateAssetFeatures/@feature)">
						<xsl:variable name="currentFeature" select="."/>

						<xsl:choose>
							<xsl:when test=". = 'canvas:jdeproperty.saledate'">
								<asset_feature>
									<xsl:for-each select="$updateAssetFeatures[@feature = $currentFeature]">
										<xsl:copy-of select="./@*"/>
									</xsl:for-each>
								</asset_feature>
							</xsl:when>
							<xsl:when test="$updateAssetFeatures[@feature = $currentFeature]/asset_feature">
								<asset_feature feature="{.}">
									<xsl:for-each select="$updateAssetFeatures[@feature = $currentFeature]/asset_feature">
										<xsl:copy-of select="."/>
									</xsl:for-each>
								</asset_feature>
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="$updateAssetFeatures[@feature=$currentFeature]"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</xsl:variable>


<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
	<xsl:value-of select="'**********concludingXMLNodes****************'" />
	<xsl:copy-of select="$concludingXMLNodes" />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->


						<!-- FINAL UPDATE OF ASSET -->
						<xsl:choose>
							<xsl:when test="$concludingXMLNodes/parent_asset_rel[@key='actual.']">

								<cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="checkoutassetxml">
									<cs:param name="source">
										<asset id="{$propertyAssetUpdated/@id}" currversion="0">
										</asset>
									</cs:param>
								</cs:command>
								<cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="CheckedInAssetXML">
									<cs:param name="source">
										<asset>
											<xsl:copy-of select="$checkoutassetxml/@*"/>
											<xsl:copy-of select="$checkoutassetxml/node()"/>
										</asset>
									</cs:param>
								</cs:command>
								
								<!--xsl:message>
									<xsl:copy-of select="$CheckedInAssetXML" />
									<xsl:copy-of select="$CheckedInAssetXML" />
									<xsl:copy-of select="$CheckedInAssetXML" />
								</xsl:message-->

				<xsl:variable name="propertyAssetCheckedIN" select="cs:asset()[@censhare:asset.id_extern=$propertyIDExt and @censhare:asset.type='product.' and @censhare:asset.currversion=0]" />

								<!--xsl:message>
									<xsl:value-of select="'&#xA;&#xA;'" />
									<xsl:copy-of select="$propertyAssetCheckedIN" />
									<xsl:value-of select="'&#xA;&#xA;'" />
								</xsl:message-->

								<cs:command name="com.censhare.api.assetmanagement.Update" returning="resultAssetXml">
									<cs:param name="source">
										<asset>
											<xsl:copy-of select="$propertyAssetCheckedIN/@*"/>
											<xsl:copy-of select="$NewupdatedAssetInformations/asset/@*"/>
											<xsl:if test="normalize-space($propertyAssetName) and $basePropertyLanguage='en'">
												<xsl:attribute name="name" select="$propertyAssetName" />
												<xsl:attribute name="language" select="$basePropertyLanguage"/>
											</xsl:if>
											<xsl:copy-of select="$concludingXMLNodes"/>
										</asset>
									</cs:param>
								</cs:command>

							</xsl:when>
							<xsl:otherwise>
								<cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="checkoutassetxml">
									<cs:param name="source">
										<asset id="{$propertyAssetUpdated/@id}" currversion="0">
										</asset>
									</cs:param>
								</cs:command>
								
								<cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="CheckedInAssetXML">
									<cs:param name="source">
										<asset>
											<xsl:copy-of select="$checkoutassetxml/@*"/>
											<xsl:copy-of select="$NewupdatedAssetInformations/asset/@*"/>
											<xsl:if test="normalize-space($propertyAssetName) and $basePropertyLanguage='en'">
												<xsl:attribute name="name" select="$propertyAssetName" />
												<xsl:attribute name="language" select="$basePropertyLanguage"/>
											</xsl:if>
											<xsl:copy-of select="$concludingXMLNodes"/>
										</asset>
									</cs:param>
								</cs:command>
								
							</xsl:otherwise>
						</xsl:choose>

								<cs:command name="com.censhare.api.assetmanagement.commit" returning="commitResult">
								</cs:command>
								
								<xsl:choose>
									<xsl:when test="$commitResult = 'false'">
										<xsl:comment>Added by censhare upon errors</xsl:comment>
										<xsl:variable name="errorid" select="'E006'" /> 
										<status value="error" message_id="{$errorid}" message="{$logConfigXML/notificationlog/error/code[@id=$errorid]}"></status>
										<xsl:value-of select="'&#xA;'" />
										<xsl:comment>End of error message</xsl:comment>
									</xsl:when>
									<xsl:when test="normalize-space($warningMessages)">
										<xsl:comment>Added by censhare upon warning</xsl:comment>
										<xsl:copy-of select="$warningMessages" />
										<xsl:comment>End of warning message</xsl:comment>
									</xsl:when>
									<xsl:otherwise>
										<xsl:comment>Added by censhare upon success</xsl:comment>
										<status value="success"></status>
										<xsl:comment>End of success message</xsl:comment>
									</xsl:otherwise>
								</xsl:choose>

					</xsl:when>
					<xsl:otherwise>
						<xsl:comment>Added by censhare upon warning</xsl:comment>
						<status value="warning" message_id="W002" message="{$logConfigXML/notificationlog/warning/code[@id='W002']}"><xsl:value-of select="'TransmissionDate TransmissionTime'" /></status>
						<xsl:comment>End of warning message</xsl:comment>
					</xsl:otherwise>
				</xsl:choose>

			</xsl:otherwise>
			</xsl:choose>

			</xsl:when>
			<xsl:otherwise>
				<!-- create new property -->

				<xsl:variable name="output">
					<xsl:element name="newPropertyAsset">
						<xsl:call-template name="item">
							<xsl:with-param name="mappingElementstoFeatures" select="$configXML"/>
							<xsl:with-param name="propertyIDExt" select="$propertyIDExt" />
							<xsl:with-param name="propertyLanguage" select="$basePropertyLanguage" />
						</xsl:call-template>
					</xsl:element>
				</xsl:variable>

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'************************** output OUTSIDE '" />
		<xsl:copy-of select="$output" />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

	  			<xsl:variable name="warningMessages">
	  				<xsl:for-each select="$output/newPropertyAsset/warningElements">
						<xsl:variable name="warningid" select="./@id" />
						<status value="warning" message_id="{$warningid}" message="{$logConfigXML/notificationlog/warning/code[@id=$warningid]}"><xsl:value-of select="." /></status>
						<xsl:value-of select="'&#xA;'" />
					</xsl:for-each>
	  			</xsl:variable>

			<!-- create new asset -->

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<StartCheckInNewPropertyAsset/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

			<cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="resultAssetXml">
					<cs:param name="source">
						<xsl:element name="asset">
							<xsl:if test="normalize-space($propertyAssetName)">
								<xsl:attribute name="name" select="$propertyAssetName" />
							</xsl:if>
							<xsl:attribute name="type" select="'product.'"/>
							<xsl:attribute name="id_extern" select="$propertyIDExt"/>
							<xsl:attribute name="language" select="$basePropertyLanguage"/>
							<xsl:attribute name="domain" select="'root.christiesinternational.public.'"/>
							<xsl:attribute name="domain2" select="'root.christiesinternational.emeri.'"/>
							<xsl:copy-of select="$output/newPropertyAsset/@*" />

							<!-- Check and Create Metadata for New Property -->
							<asset_element key="actual." idx="0"/>
							<xsl:variable name="assetFeatureElements" select="$output/newPropertyAsset/*[@feature]"/>
							<!--venkat>
							<xsl:copy-of select="$assetFeatureElements" />
							</venkat-->

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'************************** output INSIDE '" />
		<xsl:copy-of select="$output" />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message>


<xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
	<xsl:value-of select="'************************** assetFeatureElements'" />
	<xsl:copy-of select="$assetFeatureElements"/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message>

							<xsl:message>
								<xsl:value-of select="'&#xA;&#xA;'" />
								<xsl:value-of select="'************************** newPropertyAsset'" />
								<xsl:copy-of select="$output/newPropertyAsset/*[not(@feature)] except ($output/newPropertyAsset/warningElements)"/>
								<xsl:value-of select="'&#xA;&#xA;'" />
							</xsl:message-->

	  						<xsl:copy-of select="$output/newPropertyAsset/*[not(@feature)] except ($output/newPropertyAsset/warningElements)"/>

							<xsl:for-each select="distinct-values($assetFeatureElements/@feature)">
								<xsl:variable name="currentFeature" select="."/>
								<xsl:choose>
									<xsl:when test=". = 'canvas:jdeproperty.saledate'">
										<asset_feature>
											<xsl:for-each select="$assetFeatureElements[@feature=$currentFeature]">
												<xsl:copy-of select="@*"/>
											</xsl:for-each>
										</asset_feature>
									</xsl:when>
									
									<xsl:when test="$assetFeatureElements[@feature = $currentFeature]/asset_feature">
										<asset_feature feature="{.}">
										<xsl:for-each select="$assetFeatureElements[@feature = $currentFeature]/asset_feature">
											<xsl:copy-of select="."/>
										</xsl:for-each>
										</asset_feature>
									</xsl:when>

									<xsl:otherwise>
										<xsl:copy-of select="$assetFeatureElements[@feature=$currentFeature]"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>

						</xsl:element>

					</cs:param>
				</cs:command>


<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<xsl:copy-of select="$resultAssetXml" />
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->


<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<EndCheckInNewPropertyAsset/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->
				
				<cs:command name="com.censhare.api.assetmanagement.commit" returning="commitResult">
				</cs:command>
				
				<xsl:choose>
					<xsl:when test="$commitResult = 'false'">
						<xsl:comment>Added by censhare upon errors</xsl:comment>
						<xsl:variable name="errorid" select="'E006'" /> 
						<status value="error" message_id="{$errorid}" message="{$logConfigXML/notificationlog/error/code[@id=$errorid]}"></status>
						<xsl:value-of select="'&#xA;'" />
						<xsl:comment>End of error message</xsl:comment>
					</xsl:when>
					<xsl:when test="normalize-space($warningMessages)">
						<xsl:comment>Added by censhare upon warning</xsl:comment>
						<xsl:copy-of select="$warningMessages" />
						<xsl:comment>End of warning message</xsl:comment>
					</xsl:when>
					<xsl:otherwise>
						<xsl:comment>Added by censhare upon success</xsl:comment>
						<status value="success"></status>
						<xsl:comment>End of success message</xsl:comment>
					</xsl:otherwise>
				</xsl:choose>

			</xsl:otherwise>
		</xsl:choose>

		</xsl:when>
		<xsl:otherwise>
			<xsl:comment>Added by censhare upon errors</xsl:comment>
	  		<xsl:for-each select="$initialCheckErrors/errorElements">
				<xsl:variable name="errorid" select="./@id" />
				<status value="error" message_id="{$errorid}" message="{$logConfigXML/notificationlog/error/code[@id=$errorid]}"><xsl:value-of select="." /></status>
				<xsl:value-of select="'&#xA;'" />
			</xsl:for-each>
			<xsl:comment>End of error message</xsl:comment>
		</xsl:otherwise>

		</xsl:choose>
		
	</xsl:template>

	<xsl:template name="item">
		<xsl:param name="mappingElementstoFeatures"/>
		<xsl:param name="propertyIDExt"/>
		<xsl:param name="propertyLanguage"/>

		<xsl:variable name="currentItem" select="." />

		<!-- Creating Property Variants -->

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<StartVariantPropertyAssetProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

		<!--xsl:variable name="existingPropertyVariants" select="cs:asset()[@censhare:asset.type='product.' and @canvas:jdeproperty.shortitemnumber=$currentItem/ShortItemNumber]" />
		<xsl:if test="exists($existingPropertyVariants)">
			<xsl:for-each select="$existingPropertyVariants">
				<xsl:if test="not(./parent_asset_rel[@key='variant.1.'])">
					<parent_asset_rel parent_asset="{./@id}" key="variant.1."/>
				</xsl:if>
			</xsl:for-each>
		</xsl:if-->

		<xsl:variable name="existingPropertyVariants" select="cs:asset()[@censhare:asset.type='product.' and @canvas:jdeproperty.shortitemnumber=$currentItem/ShortItemNumber]" />
		<xsl:variable name="variantParentAsset">
			<!--xsl:if test="exists($existingPropertyVariants)"-->
			<xsl:if test="($existingPropertyVariants)">
				<xsl:for-each select="$existingPropertyVariants">
					<!--xsl:if test="not(./parent_asset_rel[@key='variant.1.'])"-->
					<xsl:if test="not(./parent_asset_rel[@key='variant.1.']) and ./asset_feature[@feature='canvas:jdeproperty.itemversion' and @value_long!=$currentItem/ItemVersion]">
						<parent_asset_rel parent_asset="{./@id}" key="variant.1."/>
						<xsl:copy-of select="." />
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>


<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<EndVariantPropertyAssetProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->


		<!--xsl:if test="exists(cs:asset()[@censhare:asset.type='product.' and @canvas:jdeproperty.shortitemnumber=$currentItem/ShortItemNumber])">
			<xsl:for-each select="cs:asset()[@censhare:asset.type='product.' and @canvas:jdeproperty.shortitemnumber=$currentItem/ShortItemNumber]">
				<xsl:if test="not(./parent_asset_rel[@key='variant.1.'])">
					<xsl:variable name="parentPropertyAssetID" select="./@id" />
					<parent_asset_rel parent_asset="{$parentPropertyAssetID}" key="variant.1."/>
				</xsl:if>
			</xsl:for-each>
		</xsl:if-->

		<!-- Sale Asset Checking and Creation and Relating -->
		<!--xsl:if test="exists($currentItem/SaleNumber)"-->
		<xsl:if test="($currentItem/SaleNumber)">

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<StartSaleAssetProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

			<xsl:copy-of select="canvas:salepropertyrelationscheckcreate(normalize-space($currentItem/SaleNumber),$mappingElementstoFeatures,.)" />

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<EndSaleAssetProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

		</xsl:if>

		<!-- Primary Image Asset Checking and Creation and Relating -->
		<!--xsl:if test="exists($currentItem/PrimaryID) and normalize-space($currentItem/PrimaryID)"-->
		<xsl:if test="($currentItem/PrimaryID) and normalize-space($currentItem/PrimaryID)">

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<StartPrimaryImageAssetProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

			<xsl:copy-of select="canvas:propertyPrimaryImageMapping(normalize-space($currentItem/PrimaryID),normalize-space($currentItem/SaleNumber))" />

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<EndPrimaryImageAssetProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->


		</xsl:if>

		<!-- Additional Image Asset Checking and Creation and Relating -->
		<!--xsl:if test="exists($currentItem/ImageID) and normalize-space($currentItem/ImageID)"-->
		<!--xsl:if test="($currentItem/ImageID) and normalize-space($currentItem/ImageID)">

			<xsl:variable name="elementName" select="normalize-space(name($currentItem/ImageID))"/>
			<xsl:variable name="uniqueImageIDS">
				<xsl:value-of select="distinct-values(tokenize($currentItem/ImageID, '\s'))"/>
			</xsl:variable>
			<xsl:copy-of select="canvas:propertyAdditionalImageMapping($mappingElementstoFeatures,$elementName,normalize-space($uniqueImageIDS),normalize-space($currentItem/SaleNumber),normalize-space($currentItem/PrimaryID))" />

		</xsl:if-->

		<!-- Additional Image Asset Checking and Creation and Relating -->
		<!--xsl:if test="exists($currentItem/ImageID) and normalize-space($currentItem/ImageID)"-->
		<xsl:variable name="primaryImageIDs" select="concat($currentItem/PrimaryID,' ',$currentItem/ImageID)" />
		<!--xsl:if test="($currentItem/ImageID) and normalize-space($currentItem/ImageID)"-->
		<xsl:if test="($primaryImageIDs) and normalize-space($primaryImageIDs)">
			<xsl:variable name="uniqueImageIDS">
				<xsl:value-of select="distinct-values(tokenize(normalize-space($primaryImageIDs), '\s'))"/>
			</xsl:variable>
			<xsl:variable name="elementName" select="normalize-space(name($currentItem/ImageID))"/>
			<!--xsl:copy-of select="canvas:propertyAdditionalImageMapping($mappingElementstoFeatures,$elementName,normalize-space($currentItem/ImageID),normalize-space($currentItem/SaleNumber),normalize-space($currentItem/PrimaryID))" /-->
			<xsl:copy-of select="canvas:propertyAdditionalImageMapping($mappingElementstoFeatures,$elementName,normalize-space($uniqueImageIDS),normalize-space($currentItem/SaleNumber),normalize-space($currentItem/PrimaryID))" />
		</xsl:if>


		<!-- Taxonomy Asset Checking and Creation and Relating -->
		<!--xsl:if test="exists($currentItem/CoAAttributeID)"-->
		<xsl:if test="($currentItem/CoAAttributeID)">

<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<StartCOAAttributeProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->

			<xsl:variable name="elementName" select="normalize-space(name($currentItem/CoAAttributeID))"/>
			<xsl:for-each select="$currentItem/CoAAttributeID/ID">
				<xsl:copy-of select="canvas:propertytaxonomymapping(normalize-space(.),$mappingElementstoFeatures,$elementName)" />
			</xsl:for-each>

</xsl:if>

		<!-- Creating metadata mappings and updating values accordingly -->

		<!--xsl:if test="$currentItemXML/LotNumber or $currentItemXML/LotSuffix"-->
		<asset_feature feature="canvas:lotnumberandsuffix" value_string="{$currentItem/LotNumber}{$currentItem/LotSuffix}" />
		<!--/xsl:if-->

		<!-- Store Transmission Date for used in multiple places - VEC 01-24-2019 -->
		<xsl:variable name="DateTransmission" select="concat(substring(normalize-space($currentItem/TransmissionDate), 5, 4), '-', substring(normalize-space($currentItem/TransmissionDate), 1, 2), '-', substring(normalize-space($currentItem/TransmissionDate), 3, 2))"/>

		<!--xsl:for-each select="*[normalize-space()] except (szUniqueKeyIDString_UKIDSZ,LotNumber,LotSuffix,CoAAttributeID,ImageID)"-->
		<!--xsl:for-each select="*[normalize-space()] except (szUniqueKeyIDString_UKIDSZ,LotNumber,LotSuffix,CoAAttributeID)"--> <!-- UPDATED BY VEC FOR LOT NUMBER and LOT SUFFIX -->
		<xsl:for-each select="*[normalize-space()] except (szUniqueKeyIDString_UKIDSZ,CoAAttributeID)">
			<xsl:variable name="elementName" select="normalize-space(name(.))"/>
			<xsl:variable name="confMappingFeature" select="$mappingElementstoFeatures//*[local-name()=$elementName]" />

				<xsl:choose>
					<xsl:when test="$confMappingFeature/textFileConfs/textFileRequired='Yes'">

						<xsl:variable name="textProcessingResults" select="canvas:lengthValidationFunc(canvas:tagsCanvasMapping(.), local-name(.), $confMappingFeature/textFileConfs/characterLimit, $confMappingFeature/textFileConfs/fileType/text(), $confMappingFeature/textFileConfs/fileName/text(), $propertyIDExt, $currentItem/ShortItemNumber,$propertyLanguage,normalize-space($currentItem/ItemVersion), $variantParentAsset/asset)" />
						
						<!--xsl:message>
							<xsl:value-of select="'&#xA;&#xA;&#xA; TTTTTTTTTTTTT '"/>
							<xsl:copy-of select="$textProcessingResults//textContent/text()"></xsl:copy-of>
							<xsl:value-of select="'&#xA;&#xA;'"/>
							<xsl:copy-of select="$textProcessingResults//asset"></xsl:copy-of>
							<xsl:value-of select="'&#xA;&#xA;'"/>
							<xsl:value-of select="$textProcessingResults//asset/@id" />
							<xsl:value-of select="'&#xA;&#xA;&#xA;'"/>
						</xsl:message-->

						<asset_feature feature="{$confMappingFeature/id}" language="{normalize-space($propertyLanguage)}">
							<!--xsl:attribute name="{$confMappingFeature/valueType}" select="{$textProcessingResults//textContent/text()}"/-->
							<xsl:attribute name="{$confMappingFeature/valueType}">
								<xsl:copy-of select="$textProcessingResults//textContent/text()"></xsl:copy-of>
							</xsl:attribute>
						</asset_feature>

						<!--xsl:variable name="textAssetID" select="(cs:asset('sql=true')[@censhare:asset.id_extern=concat($propertyIDExt,$elementName,$propertyLanguage) and @censhare:asset.currversion=0])/@id"/-->
						<xsl:variable name="textAssetID" select="$textProcessingResults//asset/@id"/>

						<xsl:if test="$textAssetID">
							<child_asset_rel key="{$confMappingFeature/textFileConfs/relationType/text()}" child_asset="{$textAssetID}" />
						</xsl:if>

					</xsl:when>
					<!--xsl:when test="exists($confMappingFeature/parentFeature)"-->
					<xsl:when test="($confMappingFeature/parentFeature)">
						<asset_feature feature="{$confMappingFeature/parentFeature}">
							<asset_feature feature="{$confMappingFeature/id}">
								<xsl:attribute name="{$confMappingFeature/valueType}" select="normalize-space(canvas:numberFormatChange(.))"/>
							</asset_feature>
						</asset_feature>
					</xsl:when>
					<xsl:when test="($confMappingFeature/valueType = 'value_timestamp') or ($confMappingFeature/valueType = 'value_timestamp2')">
						<xsl:choose>
						<xsl:when test="($elementName='TransmissionDate')">
							<xsl:variable name="TransmissionDateValue" select="concat($DateTransmission,'T00:00:00Z')"/>
							<asset_feature feature="{$confMappingFeature/id}">
								<xsl:attribute name="{$confMappingFeature/valueType}" select="$TransmissionDateValue"/>
							</asset_feature>
						</xsl:when>
						<xsl:when test="($elementName='TransmissionTime')">
							<!-- VEC 01-24-2019-->
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:setDateforTimeElements(.,$DateTransmission)"/>
								</asset_feature>
						</xsl:when>
						<xsl:when test="($elementName='TimeLastUpdated')">
							<!-- VEC 01-24-2019-->
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:setDateforTimeElements(.,$DateTransmission)"/>
								</asset_feature>
						</xsl:when>
						<xsl:otherwise>
							<asset_feature feature="{$confMappingFeature/id}">
								<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:dateFormatChange(.)"/>
							</asset_feature>
						</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$confMappingFeature/valueType = 'value_long'">
						<xsl:choose>
							<xsl:when test="$confMappingFeature/languagePreference = 'Yes'">
								<asset_feature feature="{$confMappingFeature/id}" language="{$propertyLanguage}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:numberFormatChange(.)"/>
								</asset_feature>
							</xsl:when>
							<xsl:otherwise>
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:numberFormatChange(.)"/>
								</asset_feature>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="actualString" select="canvas:tagsCanvasMapping(normalize-space(.))"/>
						<xsl:variable name="truncatedString" select="substring($actualString, 1, 4000)"/>
						
						<xsl:if test="(string-length($actualString) > 4000)">
							<warningElements id="W003"><xsl:value-of select="$elementName"/></warningElements>
						</xsl:if>
						
						<xsl:choose>
							<xsl:when test="$confMappingFeature/languagePreference = 'Yes'">
								<asset_feature feature="{$confMappingFeature/id}" language="{$propertyLanguage}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="$truncatedString"/>
								</asset_feature>
							</xsl:when>
							<xsl:otherwise>
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="$truncatedString"/>
								</asset_feature>
							</xsl:otherwise>
						</xsl:choose>
						<!--xsl:choose>
						<xsl:when test="$confMappingFeature/languagePreference = 'Yes'">
							<asset_feature feature="{$confMappingFeature/id}" language="{$propertyLanguage}">
								<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:tagsCanvasMapping(normalize-space(.))"/>
							</asset_feature>
						</xsl:when>
						<xsl:otherwise>
							<asset_feature feature="{$confMappingFeature/id}">
								<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:tagsCanvasMapping(normalize-space(.))"/>
							</asset_feature>
						</xsl:otherwise>
						</xsl:choose-->
					</xsl:otherwise>
				</xsl:choose>
		</xsl:for-each>


		<!-- including variant asset relation-->
		<xsl:if test="$variantParentAsset">
			<!--xsl:message>
				<xsl:value-of select="'&#xA;&#xA;&#xA;'" /-->
					<xsl:copy-of select="$variantParentAsset/parent_asset_rel" />
				<!--xsl:value-of select="'&#xA;&#xA;&#xA;'" />
					<xsl:copy-of select="$variantParentAsset/asset" />
				<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
			</xsl:message-->
		</xsl:if>


<!--xsl:message>
	<xsl:value-of select="'&#xA;&#xA;'" />
		<xsl:value-of select="'**************************'" />
		<EndOtherMetadataProcessing/>
	<xsl:value-of select="'&#xA;&#xA;'" />
</xsl:message-->


	</xsl:template>

	<xsl:template match="excludeElements">
	</xsl:template>
	<xsl:template match="warningElements">
	</xsl:template>
	
	


</xsl:stylesheet>
