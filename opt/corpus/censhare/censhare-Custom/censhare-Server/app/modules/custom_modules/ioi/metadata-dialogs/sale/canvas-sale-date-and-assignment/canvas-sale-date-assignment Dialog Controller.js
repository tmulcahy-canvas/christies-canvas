function controller($scope, csApiSession) {
  
  $scope.party  = [];
  
  csApiSession.masterdata.lookup({
      lookup: [{
          table: 'party'
      }]
  }).then(function (result) {
      // console.log('RESULT');
      // console.log(result.records.record);
      for(var r in result.records.record){
        var record = result.records.record[r];
        // console.log(record);
        $scope.party.push({id: record.id.toString(),
                          displayName:record.display_name
        });
      }
      
      // console.log($scope.party);
  });
  
}