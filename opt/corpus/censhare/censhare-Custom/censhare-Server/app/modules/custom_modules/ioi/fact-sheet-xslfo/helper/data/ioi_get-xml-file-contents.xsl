<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" 
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
    xmlns:xe="http://www.censhare.com/xml/3.0.0/xmleditor" 
    xmlns:my="http://www.censhare.com" 
    xmlns:ioi="http://www.iointegration.com" 
    exclude-result-prefixes="#all">

    <xsl:function name="ioi:get-xml-file-contents">
        <xsl:param name="read-xml-rel-path"/>
        
        <xsl:variable name="path">
        	<xsl:choose>
        		<xsl:when test="not(starts-with($read-xml-rel-path, 'censhare:///'))">
        			<!-- cc - get the runtime - cc -->
        			<xsl:variable name="config"/>
        			<cs:command name="com.censhare.api.io.GetFilesystemRef" returning="config">
          			<cs:param name="name" select="'config-runtime'" />
        			</cs:command>
        			<xsl:value-of select="concat($config, $read-xml-rel-path)"/>
        		</xsl:when>
        		<xsl:otherwise>
        			<xsl:value-of select="$read-xml-rel-path"/>
        		</xsl:otherwise>
        	</xsl:choose>
        </xsl:variable>
        <!-- cc - get the file - cc -->
        <xsl:variable name="content"/>
        <cs:command name="com.censhare.api.io.ReadXML" returning="content">
          <cs:param name="source" select="$path"/>
        </cs:command>
				
        <xsl:copy-of select="$content"/>

    </xsl:function>

</xsl:stylesheet>
