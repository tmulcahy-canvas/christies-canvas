function controller($scope) {
  //dialogue, requires false values rather then default null, so if there are no values set default
  //Approval Permissions  
  if ( $scope.asset.traits.ioi$dialogue.DialogCanApprove.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanApprove.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanReject.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanReject.value[0].value = true;
  
  //Download Permissions
  if ( $scope.asset.traits.ioi$dialogue.DialogDownload.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogDownload.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDownloadOriginalPDF.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDownloadOriginalPDF.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeHistoric.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeHistoric.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogMaxDownload.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogMaxDownload.value[0].value = 300;
  
  //Tools Permissions
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUsePenTool.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUsePenTool.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseEditBoxTool.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseEditBoxTool.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseDensitometerTool.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseDensitometerTool.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseMeasureTool.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseMeasureTool.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseZoomTool.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseZoomTool.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseDiffAlignement.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseDiffAlignement.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSaveDiffAlignment.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSaveDiffAlignment.value[0].value = true;
  
  //User Rights
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeMenuBar.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeMenuBar.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDoGamutCheck.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDoGamutCheck.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDoInkConsumption.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDoInkConsumption.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanConfigureICC.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanConfigureICC.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanRunCalibration.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanRunCalibration.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanPrint.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanPrint.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseZoomPixel.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseZoomPixel.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogMaxZoomLimit.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogMaxZoomLimit.value[0].value = 1000;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanChooseJpegQuality.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanChooseJpegQuality.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDoDifferences.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDoDifferences.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCreateSession.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCreateSession.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanJoinSession.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanJoinSession.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDoRotations.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDoRotations.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanConfigurePreferences.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanConfigurePreferences.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeDocumentInfo.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeDocumentInfo.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeExitButton.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeExitButton.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeICCConfiguration.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeICCConfiguration.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeMemoryUsage.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeMemoryUsage.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeNavigator.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeNavigator.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSwitchDocument.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSwitchDocument.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseMirrorMode.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseMirrorMode.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseMoireReduction.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseMoireReduction.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanChooseOverlay.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanChooseOverlay.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeOPIComments.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeOPIComments.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseUniformityCheck.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseUniformityCheck.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogClosedLoopBlockDocumentOnFail.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogClosedLoopBlockDocumentOnFail.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUsePaperSimulation.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUsePaperSimulation.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseLocalCache.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseLocalCache.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogLocalCacheGenerateAll.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogLocalCacheGenerateAll.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogLocalCacheGenerateNumber.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogLocalCacheGenerateNumber.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanChooseLayers.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanChooseLayers.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanChooseSeparation.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanChooseSeparation.value[0].value = true;
  
  
  //Note Options
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCreateNote.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCreateNote.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeNoteReport.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeNoteReport.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCreateTextNote.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCreateTextNote.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCreateCheckableNote.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCreateCheckableNote.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanModifyNoteEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanModifyNoteEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanModifyNoteValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanModifyNoteValue.value[0].value = "self";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDeleteNoteEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDeleteNoteEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDeleteNoteValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDeleteNoteValue.value[0].value = "self";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeNoteEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeNoteEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeNoteValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeNoteValue.value[0].value = "all";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCheckNotesEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCheckNotesEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogcanCheckNotesValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogcanCheckNotesValue.value[0].value = "all";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeCommentEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeCommentEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeCommentValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeCommentValue.value[0].value = "all";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanModifyCommentEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanModifyCommentEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanModifyCommentValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanModifyCommentValue.value[0].value = "self";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDeleteCommentEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDeleteCommentEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanDeleteCommentValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanDeleteCommentValue.value[0].value = "self";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCollapseNoteEnabled.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCollapseNoteEnabled.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCollapseNoteValue.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCollapseNoteValue.value[0].value = "all";
  if ( $scope.asset.traits.ioi$dialogue.DialogCanChangeNoteSizeMode.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanChangeNoteSizeMode.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanHideAll.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanHideAll.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanHideShapeOverlay.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanHideShapeOverlay.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanHideNotesOverlay.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanHideNotesOverlay.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanMoveNoteEditorPosition.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanMoveNoteEditorPosition.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanCreateComment.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanCreateComment.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanSeeExpandAllNotes.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanSeeExpandAllNotes.value[0].value = true;
  
  //View Options
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseSpreadView.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseSpreadView.value[0].value = false;
  if ( $scope.asset.traits.ioi$dialogue.DialogSpreadModeActivated.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogSpreadModeActivated.value[0].value = false;
  if ( $scope.asset.traits.ioi$dialogue.DialogSpreadAcrossMultipage.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogSpreadAcrossMultipage.value[0].value = false;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseCornerZoom.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseCornerZoom.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseFullscreen.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseFullscreen.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseWindowMode.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseWindowMode.value[0].value = false;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanUseCropTool.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanUseCropTool.value[0].value = true;
  if ( $scope.asset.traits.ioi$dialogue.DialogCanChangeUserColor.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogCanChangeUserColor.value[0].value =false;
  if ( $scope.asset.traits.ioi$dialogue.DialogdisplayBoxes.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialogdisplayBoxes.value[0].value = false;
  if ( $scope.asset.traits.ioi$dialogue.DialoglastReverseMode.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialoglastReverseMode.value[0].value = 0;
  if ( $scope.asset.traits.ioi$dialogue.DialoglastCropMode.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialoglastCropMode.value[0].value = 0;
  if ( $scope.asset.traits.ioi$dialogue.DialoglastSeparatedMode.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.DialoglastSeparatedMode.value[0].value = 0;
  
  //User Preferences
  if ( $scope.asset.traits.ioi$dialogue.Usercolor.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.Usercolor.value[0].value = "#FF0000";
  if ( $scope.asset.traits.ioi$dialogue.Userunitlength.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.Userunitlength.value[0].value = "mm";
  if ( $scope.asset.traits.ioi$dialogue.Userunitres.value[0].value === undefined) $scope.asset.traits.ioi$dialogue.Userunitres.value[0].value = "dpi";
}