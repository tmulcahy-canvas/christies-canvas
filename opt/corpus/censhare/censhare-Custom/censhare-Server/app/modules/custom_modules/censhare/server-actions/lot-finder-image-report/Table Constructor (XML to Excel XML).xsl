<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:x="urn:schemas-microsoft-com:office:excel"
    xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    exclude-result-prefixes="cs"
    xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
    >
    <xsl:output method="xml" omit-xml-declaration="no" indent="yes" encoding="UTF-8" />
    <xsl:param name="transform" />
    
    <!-- input asset -->
    <xsl:template match="/asset">
        <xsl:variable name="data">
            <xsl:variable name="transformAssetId" select="if ($transform/@useparent = 'no') then (@id) else (./cs:parent-rel()[@key='user.']/@id)" />
            <xsl:copy-of select="doc(concat('censhare:///service/assets/asset/id/', $transformAssetId, '/transform;key=', $transform/@report))" />
        </xsl:variable>
        <xsl:apply-templates select="$data/*"/>
    </xsl:template>
    
    
    <!-- report data -->
    <xsl:template match="/report">
        <xsl:processing-instruction name="mso-application">
      <xsl:text>progid="Excel.Sheet"</xsl:text>
    </xsl:processing-instruction>
        
        <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
            <xsl:call-template name="DocumentProperties"/>
            <xsl:call-template name="OfficeDocumentSettings"/>
            <xsl:call-template name="ExcelWorkbook"/>
            <xsl:call-template name="Styles"/>
            <xsl:call-template name="Worksheet"/>       
        </Workbook>
    </xsl:template>
    
    
    <xsl:template name="DocumentProperties">
        <!--<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
      <LastAuthor>NAME</LastAuthor>
      <Created>2013-01-30T10:33:36Z</Created>
      <Version>12.0</Version>
    </DocumentProperties>-->
    </xsl:template>
    
    <xsl:template name="OfficeDocumentSettings">
        <!--<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
      <AllowPNG/>
    </OfficeDocumentSettings>-->
    </xsl:template>
    
    <xsl:template name="ExcelWorkbook">
        <!--<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
      <WindowHeight>20260</WindowHeight>
      <WindowWidth>29600</WindowWidth>
      <WindowTopX>-20</WindowTopX>
      <WindowTopY>-20</WindowTopY>
      <Date1904/>
      <ProtectStructure>False</ProtectStructure>
      <ProtectWindows>False</ProtectWindows>
    </ExcelWorkbook>-->
    </xsl:template>
    
    
    <xsl:template name="Styles">
        <Styles>
            <Style ss:ID="s00">
                <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:WrapText="1"/>
                <Font ss:Size="12.0" ss:Color="#000000"/>
            </Style>
            <!-- Ü1 -->
            <Style ss:ID="s01">
                <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:WrapText="0"/>
                <Font ss:Size="16.0" ss:Color="#808080" ss:Bold="1"/>
            </Style>
            <!-- Ü2 -->
            <Style ss:ID="s02">
                <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:WrapText="0"/>
                <Font ss:Size="12.0" ss:Color="#808080" ss:Bold="1"/>
            </Style>
            <!-- Kopf-Zellen -->
            <Style ss:ID="s10">
                <Alignment ss:Horizontal="Center" ss:Vertical="Bottom" ss:WrapText="1"/>
                <Font ss:Size="10.0" ss:Color="#000000" ss:Bold="1"/>
            </Style>
            <!-- Standard-Zelle -->
            <Style ss:ID="s20">
                <Alignment ss:Horizontal="Left" ss:Vertical="Bottom" ss:WrapText="1"/>
                <Borders>
                    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                </Borders>
            </Style>
            <!-- Ganzzahl -->
            <Style ss:ID="s21">
                <Alignment ss:Horizontal="Right" ss:Vertical="Bottom" ss:WrapText="1"/>
                <Borders>
                    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                </Borders>
                <NumberFormat ss:Format="0"/>
            </Style>
            <!-- Gleitkommazahl -->
            <Style ss:ID="s22">
                <Alignment ss:Horizontal="Right" ss:Vertical="Bottom" ss:WrapText="1"/>
                <Borders>
                    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
                        ss:Color="#000000"/>
                </Borders>
                <NumberFormat ss:Format="Fixed"/>
            </Style>
        </Styles>
    </xsl:template>
    
    <xsl:template name="Worksheet">
        <Worksheet ss:Name="Tabelle1">
            <Table ss:DefaultColumnWidth="80" ss:DefaultRowHeight="15">
                <xsl:apply-templates select="report-definition/columns"/>
                
                <xsl:apply-templates select="report-definition/head"/>
                <xsl:variable name="column-head">
                    <xsl:apply-templates select="report-definition/columns" mode="head"/>
                </xsl:variable>
                <xsl:if test="not(data/group)">
                    <xsl:copy-of select="$column-head"/>
                </xsl:if>
                
                <xsl:apply-templates select="data/*">
                    <xsl:sort order="ascending" select="format-number(@sorting, '0000')" />
                    <xsl:with-param name="columns" select="report-definition/columns/column"/>
                    <xsl:with-param name="column-head" select="$column-head"/>
                </xsl:apply-templates>
            </Table>
        </Worksheet>
    </xsl:template>
    
    <xsl:template match="head">
        <xsl:apply-templates select="header">
            <xsl:sort select="@level"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="header">
        <Row>
            <Cell ss:StyleID="s{format-number(@level, '00')}">
                <Data ss:Type="String">
                    <xsl:value-of select="."/>
                </Data>
            </Cell>
        </Row>
    </xsl:template>
    
    <xsl:template match="columns">
        <xsl:apply-templates select="column">
            <xsl:sort order="ascending" select="format-number(@number, '0000')" />
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="column">
        <Column ss:StyleID="s00" ss:AutoFitWidth="1" />
    </xsl:template>
    
    <xsl:template match="columns" mode="head">
        <Row>
            <xsl:for-each select="column">
                <xsl:sort order="ascending" select="format-number(@number, '0000')" />
                <Cell ss:StyleID="s10">
                    <Data ss:Type="String">
                        <xsl:value-of select="@caption"/>
                    </Data>
                </Cell>
            </xsl:for-each>
        </Row>
    </xsl:template>
    
    <xsl:template match="data/group">
        <xsl:param name="columns" as="element()*"/>
        <xsl:param name="column-head"/>
        <Row>
            <Cell ss:StyleID="s00"/>
        </Row>
        <Row>
            <Cell ss:StyleID="s02">
                <Data ss:Type="String">
                    <xsl:value-of select="@caption"/>
                </Data>
            </Cell>
        </Row>
        <xsl:copy-of select="$column-head"/>
        <xsl:apply-templates select="item">
            <xsl:sort order="ascending" select="format-number(@sorting, '0000')" />
            <xsl:with-param name="columns" select="$columns"/>
        </xsl:apply-templates>
    </xsl:template>
    
    
    <xsl:template match="item">
        <xsl:param name="columns" as="element()*"/>
        <Row>
            <xsl:variable name="item" select="."/>
            <xsl:for-each select="$columns">
                <xsl:sort order="ascending" select="format-number(@number, '0000')" />
                <Cell>
                    <xsl:choose>
                        <xsl:when test="@data-type = 'long'">
                            <xsl:attribute name="ss:StyleID">s21</xsl:attribute>
                        </xsl:when>
                        <xsl:when test="@data-type = 'double'">
                            <xsl:attribute name="ss:StyleID">s22</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:attribute name="ss:StyleID">s20</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:variable name="value" select="normalize-space($item/@*[local-name() = current()/@attribute])"/>
                    <Data>
                        <xsl:choose>
                            <xsl:when test="(@data-type = 'long') and (matches($value, '\d+'))">
                                <xsl:attribute name="ss:Type">Number</xsl:attribute>
                                <xsl:value-of select="$value"/>
                            </xsl:when>
                            <xsl:when test="(@data-type = 'double') and (matches($value, '\d+(\.\d+)?'))">
                                <xsl:attribute name="ss:Type">Number</xsl:attribute>
                                <xsl:value-of select="$value"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:attribute name="ss:Type">String</xsl:attribute>
                                <xsl:value-of select="$value"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </Data>
                </Cell>
            </xsl:for-each>
        </Row>
    </xsl:template>
    
    
    
    
    
    
</xsl:stylesheet>
