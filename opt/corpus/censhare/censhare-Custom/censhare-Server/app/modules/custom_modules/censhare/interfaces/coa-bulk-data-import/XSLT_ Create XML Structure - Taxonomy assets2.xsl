<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io"  xmlns:canvas="http://integration.christies.com/Interfaces/DotCom/Notification" xmlns:corpus="http://www.censhare.com/corpus" exclude-result-prefixes="#all">
    
    <xsl:output method="xml" indent="yes" />
    <xsl:strip-space elements="*" />
    
    <xsl:variable name="VERBOSE" select="true()" />
    
    <xsl:template match="/canvas:SVCOutBoundChartOfArtExtract">
        <xsl:variable name="assets">
         <xsl:apply-templates select="canvas:ChartOfArt/canvas:COAAttribute"/>
        </xsl:variable>
        <assets>
            <xsl:copy-of select="$assets/asset"/>
        </assets>
    </xsl:template>    
    
    <xsl:variable name="trace-status-id" select="/canvas:SVCOutBoundChartOfArtExtract/canvas:Trace/canvas:TraceStatusID"/>
    
    <xsl:template match="canvas:COAAttribute">
        
        <!-- 21-06-18 commented and modified by FAC-->
        <!--<xsl:variable name="id-extern" select="canvas:COAAttributeID"/>-->
        <!-- 21-06-18 commented and modified by FAC-->
        <xsl:variable name="id-extern" select="concat('taxonomy:',canvas:COAAttributeID)"/>
        
        <xsl:variable name="deleted" select="normalize-space(canvas:Deleted)"/>
        <xsl:variable name="asset-exist" select="cs:asset('sql=true')[@censhare:asset.id_extern = $id-extern]"/>
        
        <xsl:variable name="attributetype-feature-value" select="concat(normalize-space(canvas:COAAttributeTypeID/text()),'-',normalize-space(canvas:COAAttributeType/text()))"/>
        <xsl:variable name="alternatevalue-feature-value" select="lower-case(replace(normalize-space(canvas:COAAttributeAlternateValue/text()),' ','-'))"/>
        
        <xsl:variable name="attributetype-feature" select="cs:master-data('feature')[@key = 'canvas:coa.attributetype']"/>
        <xsl:variable name="alternatevalue-feature" select="cs:master-data('feature')[@key = 'canvas:coa.alternatevalue']"/>
        
        <xsl:variable name="attributetype-feature-value-key" select="cs:master-data('feature_value')[@feature = 'canvas:coa.attributetype' and @value_key = substring-before($attributetype-feature-value,'-')]"/>
        <xsl:variable name="alternatevalue-feature-value-key" select="cs:master-data('feature_value')[@feature = 'canvas:coa.alternatevalue' and @value_key = $alternatevalue-feature-value]"/>
        <xsl:variable name="date-time" select="canvas:DateModified"/>
        
        
        <xsl:variable name="masterdataXml">
            <xsl:if test="empty($attributetype-feature-value-key)">
              <feature>
                <xsl:copy-of select="$attributetype-feature/@*"/>
                <feature_value feature="canvas:coa.attributetype" domain="root." domain2="root." is_hierarchical="0" value_key="{substring-before($attributetype-feature-value,'-')}" name="{substring-after($attributetype-feature-value,'-')}" name_de="{substring-after($attributetype-feature-value,'-')}" sorting="0" enabled="1"/>
            </feature>
            </xsl:if>
            <xsl:if test="empty($alternatevalue-feature-value-key) and $alternatevalue-feature-value != ''">
                <feature>
                    <xsl:copy-of select="$alternatevalue-feature/@*"/>
                    <feature_value feature="canvas:coa.alternatevalue" domain="root." domain2="root." is_hierarchical="0" value_key="{$alternatevalue-feature-value}" name="{normalize-space(canvas:COAAttributeAlternateValue/text())}" name_de="{normalize-space(canvas:COAAttributeAlternateValue/text())}" sorting="0" enabled="1"/>
                </feature>
            </xsl:if>
        </xsl:variable>
        
        <!-- Create master-data -->
        <xsl:variable name="command-xml">
            <cmd>
                <xml-info title="my-create-master-data" locale="__ALL" />
                <cmd-info name="admin.export_import.xml-data-import" />
                <commands currentstep="0">
                    <command method="importXml" scriptlet="modules.admin.export_import.XmlDataImport" target="ScriptletManager" />
                </commands>
                <content>
                    <xsl:copy-of select="$masterdataXml" />
                </content>
                <param to-do="both" />
            </cmd>
        </xsl:variable>
            
        <cs:command name="com.censhare.api.ExecuteCommand.execute">
            <cs:param name="command" select="$command-xml" />
        </cs:command>
        
        <asset name="{normalize-space(canvas:COAAttributeValue)}" id_extern="{$id-extern}" type="taxonomy." domain="root.christiesinternational.public." domain2="root.christiesinternational.emeri." application="default" modified_date="{current-dateTime()}">
        <!--<asset name="{normalize-space(canvas:COAAttributeValue)}" id_extern="{$id-extern}" type="taxonomy." domain="root." domain2="root." application="default" modified_date="{current-dateTime()}">-->
            <xsl:if test="$deleted = 'true'">
                <xsl:attribute name="deletion" select="4"/>
            </xsl:if>
            <feature key="canvas:coa.attributetype" value="{substring-before($attributetype-feature-value,'-')}"/>
            <feature key="canvas:coa.attributeid" value="{$id-extern}"/>
            <feature key="canvas:coa.datemodified" value="{$date-time}"/>
            <xsl:if test="exists($trace-status-id)">
                <feature key="canvas:coa.tracestatusid" value="{$trace-status-id}"/>    
            </xsl:if>
            <xsl:if test="$alternatevalue-feature-value != ''">
             <feature key="canvas:coa.alternatevalue" value="{$alternatevalue-feature-value}"/>
            </xsl:if>
            <xsl:if test="not(empty($asset-exist))">
                <xsl:if test="not(exists(canvas:COAAttributeAlternateValue))">
                    <xsl:copy-of select="$asset-exist//asset_feature[@feature='canvas:coa.alternatevalue']"/>
                </xsl:if>
                <xsl:if test="not(exists(canvas:COAAttributeTypeID))">
                    <xsl:copy-of select="$asset-exist//asset_feature[@feature='canvas:coa.attributetype']"/>
                </xsl:if>
            </xsl:if>
            
            
            
            
            
            <!--Added by FAC 24-02-18-->
            <xsl:if test="exists(canvas:ParentCOAAttributeID)">
             <xsl:choose>
                 <xsl:when test="contains(canvas:ParentCOAAttributeID/text(), ' ')">
                 <xsl:message>msg 1:</xsl:message>
                     <xsl:for-each select="tokenize(canvas:ParentCOAAttributeID/text(), ' ')">
                         <!-- 21-06-18 -->
                         <!--<xsl:variable name="parenv" select="."/>-->
                         <!-- 21-06-18 -->
                         <xsl:variable name="parenv" select="concat('taxonomy:',.)"/>
                         <xsl:message>msg 2: <xsl:value-of select="$parenv"/></xsl:message>
                         
                         <!-- 21-06-18 -->
                         <!--<xsl:variable name="parent-taxonomy-asset" select="if (normalize-space($parenv) != '') then cs:asset('sql=true')[@censhare:asset.id_extern = normalize-space($parenv)]/@id else false()"/>-->
                         <!-- 21-06-18 -->
                         <xsl:variable name="parent-taxonomy-asset" select="if (normalize-space($parenv) != '') then cs:asset('sql=true')[@censhare:asset.id_extern = normalize-space($parenv)]/@id else false()"/>
                         
                          <xsl:message>Parent taxonomy asset: <xsl:value-of select="$parent-taxonomy-asset"/></xsl:message>
                           <xsl:message> asset exit: <xsl:value-of select="$asset-exist/@id"/> parent asset <xsl:value-of select="$parent-taxonomy-asset"/></xsl:message>
                         <xsl:if test="$parent-taxonomy-asset != false()">


                         <!-- start here -->
                        <xsl:if test="not($asset-exist//parent_asset_rel[@parent_asset=$parent-taxonomy-asset])">
		                        <feature key="canvas:coa.parentattributeid" value="{normalize-space($parenv)}"/>
                            <parent_asset_rel key="user.taxonomy." parent_asset="{distinct-values($parent-taxonomy-asset)}"/>
                        </xsl:if> 
                         <!-- end here -->                         
                         
                         
                             <!--<feature key="canvas:coa.parentattributeid" value="{normalize-space($parenv)}"/>
                             
                             <parent_asset_rel key="user.taxonomy." parent_asset="{distinct-values($parent-taxonomy-asset)}"/>-->
                         
                         </xsl:if>
                     </xsl:for-each>   
                 </xsl:when>
                 <xsl:otherwise>
                 <xsl:message>msg 1:</xsl:message>
                     <!-- 21-06-18 -->
                     <!--<xsl:variable name="parent-taxonomy-asset" select="if (exists(canvas:ParentCOAAttributeID) and normalize-space(canvas:ParentCOAAttributeID/text()) != '') then cs:asset('sql=true')[@censhare:asset.id_extern = normalize-space(canvas:ParentCOAAttributeID/text())]/@id else false()"/>-->
                     <!-- 21-06-18 -->
                     <xsl:variable name="parent-taxonomy-asset" select="if (exists(canvas:ParentCOAAttributeID) and normalize-space(canvas:ParentCOAAttributeID/text()) != '') then cs:asset('sql=true')[@censhare:asset.id_extern = normalize-space(concat('taxonomy:',canvas:ParentCOAAttributeID/text()))]/@id else false()"/>
                     
                     <xsl:if test="$parent-taxonomy-asset != false()">
                     
                     <!-- start here-->
                     <xsl:message> asset exit: <xsl:value-of select="$asset-exist/@id"/> parent asset <xsl:value-of select="$parent-taxonomy-asset"/></xsl:message>
                     
                     <xsl:if test="not($asset-exist//parent_asset_rel[@parent_asset=$parent-taxonomy-asset])">
    
                     <!-- 21-06-18 -->    
                     <!--<feature key="canvas:coa.parentattributeid" value="{normalize-space(canvas:ParentCOAAttributeID/text())}"/>-->
                     <!-- 21-06-18 -->    
                         <feature key="canvas:coa.parentattributeid" value="{normalize-space(concat('taxonomy:',canvas:ParentCOAAttributeID/text()))}"/>   
                     <parent_asset_rel key="user.taxonomy." parent_asset="{distinct-values($parent-taxonomy-asset)}"/>
                     
                     </xsl:if>    
                     
                     
                     <!-- end here-->
                     
                         <!--<feature key="canvas:coa.parentattributeid" value="{normalize-space(canvas:ParentCOAAttributeID/text())}"/>
                         <parent_asset_rel key="user.taxonomy." parent_asset="{distinct-values($parent-taxonomy-asset)}"/>-->
                     
                     
                     </xsl:if>
                     <xsl:if test="(exists(canvas:ParentCOAAttributeID) and normalize-space(canvas:ParentCOAAttributeID/text()) = '')">
                         <ParentCOAAttributeID value="true"/>
                     </xsl:if>
                 </xsl:otherwise>
             </xsl:choose>                
            </xsl:if>
            
            <!--<xsl:variable name="parent-taxonomy-asset" select="if (exists(canvas:ParentCOAAttributeID) and normalize-space(canvas:ParentCOAAttributeID/text()) != '') then cs:asset('sql=true')[@censhare:asset.id_extern = normalize-space(canvas:ParentCOAAttributeID/text())]/@id else false()"/>
            <xsl:if test="$parent-taxonomy-asset != false()">
                <feature key="canvas:coa.parentattributeid" value="{normalize-space(canvas:ParentCOAAttributeID/text())}"/>
                <parent_asset_rel key="user.taxonomy." parent_asset="{distinct-values($parent-taxonomy-asset)}"/>
            </xsl:if>
            <xsl:if test="(exists(canvas:ParentCOAAttributeID) and normalize-space(canvas:ParentCOAAttributeID/text()) = '')">
                <ParentCOAAttributeID value="true"/>
            </xsl:if>-->
        </asset>
    </xsl:template>    
    
</xsl:stylesheet>