<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
 xmlns:canvas="censhare.com"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
 xmlns:xi="http://www.w3.org/2001/XInclude"
 xmlns:xs="http://www.w3.org/2001/XMLSchema"
 xmlns:my="http://www.censhare.com/my"
 xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
 exclude-result-prefixes="#all">


<!-- ################### -->
	<xsl:function name="canvas:propertytaxonomymapping">
        <xsl:param name="elementID" />
        <xsl:param name="mappingElementstoFeatures" />
        <xsl:param name="elementName" />
		
		<xsl:variable name="taxonomyAssetXML" select="cs:asset()[@censhare:asset.type='taxonomy.' and @censhare:asset.id_extern=concat('taxonomy:',$elementID) and @censhare:asset.currversion=0]" />
				<xsl:choose>
					<xsl:when test="$taxonomyAssetXML">
						<xsl:copy-of select="canvas:taxonomyassetcreaterelations($taxonomyAssetXML/@id,$mappingElementstoFeatures,$elementName)" />
					</xsl:when>
					<xsl:otherwise>
						<warningElements id="W001"><xsl:value-of select="$elementID"/></warningElements>
					</xsl:otherwise>
				</xsl:choose>
	</xsl:function>


<!-- ################### -->
	<xsl:function name="canvas:taxonomyassetcreaterelations">
		<xsl:param name="taxonomyAssetID"/>
        <xsl:param name="mappingElementstoFeatures" />
        <xsl:param name="elementName" />
		
		<xsl:variable name="currentElement" select="$mappingElementstoFeatures//*[local-name()=$elementName]"/>
		<asset_feature>
			<xsl:attribute name="feature" select="normalize-space($currentElement/id)"/>
			<xsl:attribute name="{$currentElement/valueType}" select="normalize-space($taxonomyAssetID)"/>
		</asset_feature>
	</xsl:function>

</xsl:stylesheet>
