<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format"
xmlns:xi="http://www.w3.org/2001/XInclude"
version="1.0">
<xsl:output method="xml" indent="yes" omit-xml-declaration="no" encoding="UTF-8"/>

<xsl:template match="/">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="simple" page-width="1500px" page-height="1500px" >
			<fo:region-body display-align="center" background-color="#808080"/>
			</fo:simple-page-master>
		</fo:layout-master-set>
		<fo:page-sequence master-reference="simple">
			<fo:flow flow-name="xsl-region-body">
			<xsl:apply-templates/>
			</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>
  <xsl:template match="id">
    <!--fo:block text-align="center" vertical-align="center" font-weight="bold" font-size="14pt" color="white"><xsl:value-of select="."/></fo:block-->
    <fo:block text-align="center" font-weight="bold" font-size="14pt" color="white"><xsl:value-of select="."/></fo:block>
  </xsl:template>
</xsl:stylesheet>
