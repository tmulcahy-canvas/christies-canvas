<?xml version="1.0" encoding="UTF-8"?>

<!-- 
#########################################################
censhare standard content Layout Transformation

Version 1.0 - Mai 14th 2013 
#########################################################
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:fn="http://www.w3.org/2005/xpath-functions" 
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:map="http://ns.censhare.de/mapping"
  xmlns:temp="http://ns.censhare.de/elements"
  xmlns:func="http://ns.censhare.de/functions"
  exclude-result-prefixes="xs xd fn xi cs map temp func"
  version="2.0">
  
  <xsl:output indent="no" method="xml" omit-xml-declaration="no" encoding="UTF-8"/>
<!--  <xsl:strip-space elements=""/>-->
  <xsl:preserve-space elements="* temp:CSR"/>
  


<!-- ############################################################################################ -->
  <!-- include xslt modules -->
  <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=canvas:xml2icml-basic/storage/master/file"/>
  <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=censhare:xml2icml-formats/storage/master/file"/>
  <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=censhare:xml2icml-structure/storage/master/file"/>
  
  <!-- ############################################################################################ -->
  

  
</xsl:stylesheet>
  
