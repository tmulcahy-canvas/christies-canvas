<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" 
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
    xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:my="http://ns.censhare.de/functions" exclude-result-prefixes="io cs censhare xs my">
    
    <xsl:template match="asset">
        <xsl:variable name="sale-asset" select="."/>
        <xsl:variable name="JDE-Sale-Number" select="$sale-asset/asset_feature[@feature='canvas:jdesale.salenumber']/@value_long"/>
        <xsl:variable name="products-info-of-sale-asset">
            <xsl:for-each select="$sale-asset/cs:child-rel()[@key='target.']/cs:asset()[@censhare:asset.type='product.']/cs:child-rel()[@key='*']/cs:asset()[@censhare:asset.type='picture.*']">
                <xsl:variable name="current-image" select="."/>
                <image id="{$current-image/@id}"/>
            </xsl:for-each>
            <xsl:for-each select="$sale-asset/cs:child-rel()[@key='target.']/cs:asset()[@censhare:asset.type='picture.*']"> <!--Planning-->
                <xsl:variable name="current-image" select="."/>
                <image id="{$current-image/@id}"/>
            </xsl:for-each>
            <xsl:for-each select="$sale-asset/cs:child-rel()[@key='user.']/cs:asset()[@censhare:asset.type='picture.*']"> <!--Assignment-->
                <xsl:variable name="current-image" select="."/>
                <image id="{$current-image/@id}"/>
            </xsl:for-each>
            <xsl:for-each select="$sale-asset/cs:child-rel()[@key='user.main-picture.']/cs:asset()[@censhare:asset.type='picture.*']"> <!--Primarry Image-->
                <xsl:variable name="current-image" select="."/>
                <image id="{$current-image/@id}"/>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="ParentSectionAsset" select="$sale-asset/cs:parent-rel()[@key='user.']/cs:asset()[@censhare:asset.type='issue.section.']"/>
        <xsl:variable name="LayoutAssets" select="$ParentSectionAsset/cs:child-rel()[@key='target.']/cs:asset()[@censhare:asset.type='layout.']"/>
        
        <xsl:variable name="products-info-of-layouts-asset">
            <xsl:for-each select="$LayoutAssets">
                <xsl:variable name="current-layout" select="."/>
                <xsl:variable name="LayoutAssetsPageType" select="$current-layout/asset_feature[@feature='canvas:pagetype']/@value_key"/>
                
                <xsl:if test="$LayoutAssetsPageType = 'PGS'">
                    <xsl:for-each select="$current-layout/cs:child-rel()[@key='actual.']/cs:asset()[@censhare:asset.type='picture.*']">
                        <xsl:variable name="current-image" select="."/>
                        <image id="{$current-image/@id}"/>
                    </xsl:for-each>
                </xsl:if>
                <xsl:if test="$LayoutAssetsPageType = 'COV' or $LayoutAssetsPageType = 'IFC' or $LayoutAssetsPageType = 'OTHER' or $LayoutAssetsPageType = 'POS'">
                    <xsl:for-each select="$current-layout/cs:child-rel()[@key='user.' or @key='target.']/cs:asset()[@censhare:asset.type='picture.*']">
                        <xsl:variable name="current-image" select="."/>
                        <image id="{$current-image/@id}"/>
                    </xsl:for-each>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="all-images">
            <xsl:copy-of select="$products-info-of-sale-asset"/>
            <xsl:copy-of select="$products-info-of-layouts-asset"/>
        </xsl:variable>
        
        <xsl:variable name="unique-images-id" select="distinct-values(for $x in $all-images/image return $x/@id)"/>
        
        <xsl:for-each select="$unique-images-id">
            <xsl:variable name="current-asset" select="cs:get-asset(.)"/>
            <xsl:if test="not($current-asset/cs:parent-rel()[@key='user.']/cs:asset()[@censhare:asset.type='sale.' and (@censhare:asset.name='UNI9000' or @censhare:asset.name='UNI9001') and (@canvas:jdesale.salenumber='9000' or @canvas:jdesale.salenumber='9001')])">
                <!--<xsl:if test="not($current-asset/cs:parent-rel()[@key='*']/cs:asset()[@censhare:asset.type='product.']/cs:parent-rel()[@key='user.']/cs:asset()[@censhare:asset.type='sale.' and (@chrisus:sale.number='9000' or @chrisus:sale.number='9001')])">-->
                <xsl:variable name="AlreadyMLSaleNumber" select="for $x in $current-asset/asset_feature[@feature='canvas:ml.salenumber'] return $x/@value_long"/>
                <xsl:variable name="matched" select="index-of($AlreadyMLSaleNumber,$JDE-Sale-Number)" as="xs:integer*"/>
                <xsl:if test="not($matched)">
                    <xsl:copy-of select="my:update-image-asset($current-asset,$JDE-Sale-Number)"/>
                </xsl:if>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:function name="my:update-image-asset">
        <xsl:param name="image-asset"/>
        <xsl:param name="JDESaleNumber"/>
        <xsl:variable name="image-new-asset" as="element(asset)">
            <asset>
                <xsl:copy-of select="$image-asset/@*"/>
                <asset_feature feature="canvas:ml.salenumber" value_long="{$JDESaleNumber}"/>
                <xsl:copy-of select="$image-asset/*"/>
            </asset>
        </xsl:variable>
        <xsl:sequence select="my:update-asset($image-new-asset)"/>
    </xsl:function>
    <xsl:function name="my:update-asset" as="element(asset)">
        <xsl:param name="new-asset" as="element(asset)"/>
        <xsl:variable name="result">
            <cs:command name="com.censhare.api.assetmanagement.Update">
                <cs:param name="source" select="$new-asset"/>
            </cs:command>
        </xsl:variable>
        <xsl:sequence select="$result/asset"/>
    </xsl:function>
    
</xsl:stylesheet>
