<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">
	<xsl:template match="/">
		<query type="asset" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus">
			<condition name="censhare:asset.type" value="product.*"/>
			<condition name="censhare:asset.modified_date" value="n-07.00.0000-00:00:00" op="&gt;"/>
			<sortorders>
				<order ascending="true"/>
			</sortorders>
		</query>
	</xsl:template>
</xsl:stylesheet>