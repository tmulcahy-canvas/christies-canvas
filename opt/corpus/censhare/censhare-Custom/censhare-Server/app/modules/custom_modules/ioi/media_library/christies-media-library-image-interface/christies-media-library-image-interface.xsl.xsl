<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:sd="http://www.censhare.com/xml/3.0.0/censhare-solution-development"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:Composite="http://ns.exiftool.ca/Composite/1.0/"
  xmlns:JFIF="http://ns.exiftool.ca/JFIF/JFIF/1.0/"
  xmlns:File="http://ns.exiftool.ca/File/1.0/"
  xmlns:IFD0="http://ns.exiftool.ca/EXIF/IFD0/1.0/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  exclude-result-prefixes="#all"
  >
  
  <!-- source files structure transformed into xml -->
  <!--
        <filelist>
            <fileGroup full-path="file:file:///Users/shailendrasrivastava/workspaces/develop/censhare-product/censhare-Server/work/interfaces/scriba/scriba-to-censhare/in/SSR-001/" rel-path="file:SSR-001/">
                <data/>
                <file filesystem="interfaces" rel-name="39219-SSR-001-Article-001.xml" url="file:scriba/scriba-to-censhare/work/39219-SSR-001-Article-001.xml"/>
                <file filesystem="interfaces" rel-name="SITE Car.xml" url="file:scriba/scriba-to-censhare/work/SITE Car.xml"/>
                <file filesystem="interfaces" rel-name="TOC.xml" url="file:scriba/scriba-to-censhare/work/TOC.xml"/>
            </fileGroup>
            <fileGroup full-path="file:file:///Users/shailendrasrivastava/workspaces/develop/censhare-product/censhare-Server/work/interfaces/scriba/scriba-to-censhare/in/SSR-002/" rel-path="file:SSR-002/">
                <data/>
                <file filesystem="interfaces" rel-name="39425-SSR-002-Article-002.xml" url="file:scriba/scriba-to-censhare/work/39425-SSR-002-Article-002.xml"/>
            </fileGroup>
        </filelist>
        -->
  <xsl:param name="censhare:command-xml"/>
  
  <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=sd:function-library/storage/master/file"/>
  
  <xsl:template match="/">
    <result>
      <xsl:apply-templates select="$censhare:command-xml//filelist/*"/>
    </result>
  </xsl:template>
  
  <xsl:output method="xml" indent="yes" omit-xml-declaration="no" encoding="UTF-8"/>
  
  <xsl:template match="file">
    <xsl:variable name="process-user" select="'102'"/>
    
    <xsl:variable name="master" select="concat('censhare:///service/filesystem/', @filesystem, '/', substring-after(@url, 'file:'))"/>
    
    <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
    <cs:command name="com.censhare.api.io.Copy">
      <cs:param name="source" select="$master"/>
      <!--<cs:param name="dest" select="concat($out, sd:get-file-name-without-suffix(@url), '.', sd:get-file-name-suffix(@url))"/>-->
      <cs:param name="dest" select="concat($out, sd:get-file-name-without-suffix(@url), '.', 'eps')"/>
    </cs:command>
    <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="outDir">
      <cs:param name="id" select="$out"/>
    </cs:command>
    
    <xsl:if test="lower-case(sd:get-file-name-suffix(@url)) = 'jpg' or lower-case(sd:get-file-name-suffix(@url)) = 'jpeg' or lower-case(sd:get-file-name-suffix(@url)) = 'png' or lower-case(sd:get-file-name-suffix(@url)) = 'tif' or lower-case(sd:get-file-name-suffix(@url)) = 'tiff' or lower-case(sd:get-file-name-suffix(@url)) = 'eps' or lower-case(sd:get-file-name-suffix(@url)) = ''">
      <!-- image might has a suffix -->
      <!--<xsl:variable name="unique-id-of-the-image" select="substring(sd:get-file-name-without-suffix(@url), 1, 8)"/>-->
      <xsl:variable name="unique-id-of-the-image" select="sd:get-file-name-without-suffix(@url)"/>
      <xsl:message>unique-id-of-the-image:<xsl:value-of select="$unique-id-of-the-image"/></xsl:message>
      <xsl:variable name="existing-asset" select="(cs:asset()[@chrisus:ml.name = $unique-id-of-the-image])[1]"/>
      <xsl:variable name="production-status" select="$existing-asset/asset_feature[@feature='chrisus:ml.production-status']/@value_key"/>
      <xsl:variable name="wf-id" select="'35000'"/>
      <xsl:variable name="wf-step" select="$production-status"/>
      
      <xsl:choose>
        <xsl:when test="exists($existing-asset)">
          <xsl:variable name="updated-asset-xml">
            <asset>
              <xsl:copy-of select="$existing-asset/@*"/>
              <xsl:attribute name="modified_by" select="$process-user"/>
              <xsl:attribute name="modified_date" select="current-dateTime()"/>
              <xsl:attribute name="wf_id" select="$wf-id"/>
              <xsl:attribute name="wf_step" select="$wf-step"/>
              <xsl:copy-of select="$existing-asset/node() except ($existing-asset/storage_item)"/>
              <xsl:if test="not(ends-with(@url, '/'))">
                <xsl:if test="not($existing-asset/asset_element[@key='actual.' and @idx='0'])">
                  <asset_element key="actual." idx="0"/>
                </xsl:if>
                <!--<storage_item key="master" mimetype="{sd:getMimetype(@url)}" element_idx="0" corpus:asset-temp-file-url="{concat($out, sd:get-file-name-without-suffix(@url), '.', sd:get-file-name-suffix(@url))}"/>-->
                <!-- Set all file's extension as .eps -->
                <storage_item key="master" mimetype="{sd:get-mimetype(@url)}" element_idx="0" corpus:asset-temp-file-url="{concat($out, sd:get-file-name-without-suffix(@url), '.', 'eps')}"/>
              </xsl:if>
            </asset>
          </xsl:variable>
          <xsl:sequence select="sd:update($updated-asset-xml)"/>
          <!--
          <cs:command name="com.censhare.api.assetmanagement.Update" returning="asset-xml">
            <cs:param name="source" select="$updated-asset-xml"/>
          </cs:command>
          <xsl:message>
            <xsl:text>&#xA;-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-&#xA;</xsl:text>
            <xsl:text>Result of the function sd:update:&#xA;</xsl:text>
            <xsl:text>Selected asset </xsl:text>
            <xsl:value-of select="@id"/>
            <xsl:text> has been updated &#xA;</xsl:text>
            <xsl:text>&#xA;-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-</xsl:text>
          </xsl:message>
          -->
        </xsl:when>
        <!-- Don't create new assets if existing placeholder for the image doesn't exist-->
        
        <xsl:otherwise>
          <xsl:message>Christies ML image interface: Image <xsl:value-of select="sd:getFileName(@url)"/> can't be ingested as there is no existing placehoder asset for the image in the server</xsl:message>
          <xsl:variable name="new-asset-xml">
            <asset name="{sd:getFileName(@url)}" type=""/>
          </xsl:variable>
          <xsl:sequence select="sd:checkin-new($new-asset-xml)"/>
          <!--
          <cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="asset-xml">
            <cs:param name="source" select="$new-asset-xml"/>
          </cs:command>
          <xsl:message>
            <xsl:text>&#xA;-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-&#xA;</xsl:text>
            <xsl:text>Result of the function sd:checkin-new:&#xA;</xsl:text>
            <xsl:text>New asset created/checked-in&#xA;</xsl:text>
            <xsl:text>&#xA;-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-</xsl:text>
          </xsl:message>
          -->
        </xsl:otherwise>
        
      </xsl:choose>
    </xsl:if>
    
  </xsl:template>
  
  <xsl:function name="sd:get-file-name-without-suffix">
    <xsl:param name="filePath"/>
    <xsl:variable name="fileName" select="sd:getFileName($filePath)"/>
    <xsl:value-of select="substring-before($fileName, '.')"/>
    <!--
    <xsl:variable name="fileNameSuffix" select="sd:get-file-name-suffix($filePath)"/>
    <xsl:choose>
      <xsl:when test="string-length($fileNameSuffix) &gt; 0">
        <xsl:value-of select="substring-before($fileName, concat('.', $fileNameSuffix))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$fileName"/>
      </xsl:otherwise>
    </xsl:choose>
    -->
  </xsl:function>
  
  <xsl:function name="sd:get-file-name-suffix">
    <xsl:param name="filePath"/>
    <xsl:value-of select="tokenize($filePath, '\.')[last()]"/>
  </xsl:function>
  
  <xsl:function name="sd:get-mimetype">
    <xsl:param name="filePath"/>
    <xsl:variable name="suffix" select="sd:get-file-name-suffix(sd:getFileName($filePath))"/>
    <xsl:choose>
      <xsl:when test="string-length($suffix) &gt; 0">                
        <xsl:variable name="mimetype" select="(cs:master-data('mimetype_alt')[@extension=concat('.', lower-case($suffix))]/@mimetype)[1]"/>
        <xsl:value-of select="if (string-length($mimetype) &gt; 0) then $mimetype else 'application/x-unknown'"/>
      </xsl:when>
      <xsl:otherwise>
        <!-- TODO AR: can we use the MIMETypeDetector as a probably better alternative? -->
        <xsl:value-of select="'application/x-unknown'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
</xsl:stylesheet>

