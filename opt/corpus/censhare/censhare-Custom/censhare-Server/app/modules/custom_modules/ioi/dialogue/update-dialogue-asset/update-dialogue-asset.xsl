<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:html="http://www.w3.org/TR/REC-html40" 
  xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" 
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
  xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io"
  xmlns:ioi="http://www.iointegration.com"
  exclude-result-prefixes="xs html censhare cs io ioi">
  <xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>

  <xsl:param name="censhare:command-xml"/>
  
  <!-- post data is XML sent from script and comes in as param data -->
  <xsl:param name="data"  />
  
  <!--
	<?xml version="1.0" encoding="utf-8"?>
 	<post_data>
		<approval></approval>
		<user></user>
		<page></page>
		<userid></userid>
		<partyid></partyid>
		<page></page>
	</post_data>

	 -->
  <!-- copy incoming xml "data" as xml to variable -->
  <xsl:variable name="inparams">
  	<xsl:copy-of select="$data" />
  </xsl:variable>

  <!-- Now create param from xml data.
  		Thoughts: Could just write loop and make param from node name and node value making it dynamic.
  		Could also check if data (inparams) is valid and then either take post or get -->
  <xsl:param name="approval" select="$inparams/post_data/approval"/>
  <xsl:param name="user" select="$inparams/post_data/user"/>
  <xsl:param name="page" select="$inparams/post_data/page"/>
  <xsl:param name="comment" select="$inparams/post_data/comment"/>
  <xsl:param name="pagenumber" select="$inparams/post_data/page"/>
  <xsl:param name="partyid" select="$inparams/post_data/partyid"/>
  <xsl:param name="userid" select="$inparams/post_data/userid"/>
 
  <xsl:template match="/">
  	<!-- cc - cs:get-asset() -> Takes an asset id as a param and returns the asset xml - cc -->
  	<xsl:variable name="asset-xml" select="asset"/>
  	<!-- cc - create updated asset xml - cc -->
  	<xsl:variable name="updated-asset-xml">
  		<asset>
  			<xsl:copy-of select="$asset-xml/@*"/>
  			<xsl:copy-of select="$asset-xml/node()"/>
  			<asset_feature feature="ioi:dialogue.approvalgroup" >
				<xsl:when test="$approval != ''">
					<asset_feature feature="ioi:dialogue.approval" value_string="{$approval}" />
				</xsl:when>
				<xsl:when test="$comment != ''">
					<asset_feature feature="ioi:dialogue.approvalcomment" value_string="{$comment}" />
				</xsl:when>
				<xsl:when test="$user != ''">
					<asset_feature feature="ioi:dialogue.approvaluser" value_string="{$user}" />
				</xsl:when>
				<xsl:when test="$pagenumber != ''">
					<asset_feature feature="ioi:dialogue.approvalpagenumber" value_long="{$pagenumber}" />
				</xsl:when>
				<xsl:when test="$partyid != ''">
					<asset_feature feature="ioi:dialogue.approvalpartyid" value_long="{$partyid}" />
				</xsl:when>
				<xsl:when test="$userid != ''">
					<asset_feature feature="ioi:dialogue.approvaluserid" value_long="{$userid}" />
				</xsl:when>
			</asset_feature>
  		</asset>
  	</xsl:variable>

  	<xsl:variable name="updated" select="cs:update($updated-asset-xml)"/>

  </xsl:template>

  <!-- cc - common asset-management api functions - cc -->
  <xsl:function name="cs:checkin-new">
    <xsl:param name="asset-xml"/>
    <cs:command name="com.censhare.api.assetmanagement.CheckInNew">
      <cs:param name="source" select="$asset-xml"/>
    </cs:command>
  </xsl:function>
  <xsl:function name="cs:update">
    <xsl:param name="asset-xml"/>
    <cs:command name="com.censhare.api.assetmanagement.Update">
      <cs:param name="source" select="$asset-xml"/>
    </cs:command>
  </xsl:function>
  
  <xsl:function name="cs:delete">
    <xsl:param name="asset-xml"/>
    <cs:command name="com.censhare.api.assetmanagement.Delete">
      <cs:param name="source" select="$asset-xml"/>
      <cs:param name="state" select="'physical'"/>
    </cs:command>
  </xsl:function>


</xsl:stylesheet>