<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:censhare="http://www.censhare.com"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:sd="http://www.censhare.com/xml/3.0.0/censhare-solution-development"
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" exclude-result-prefixes="#all">
    
    <xsl:variable name="dateTime" select="concat(format-date(current-date(), '[Y0001][M01][D01]'), format-time(current-time(),'[H01][m01][s01]'))"/>
    <xsl:variable name="sale-asset" select="asset" />
    <xsl:variable name="sale-asset-id" select="asset/@id" />
    <xsl:variable name="external-id" select="asset/@id_extern" />
    <xsl:variable name="images" select="$sale-asset/cs:child-rel()[@key='*']/cs:asset()[@censhare:asset.type='picture.*']"/>
    <xsl:variable name="PropertyAssetIDs" select="string-join($sale-asset/cs:child-rel()[@key='target.']/cs:asset()[@censhare:asset.type='product.']/@id, ',')"/>
    
    <!-- Product/Image information of Sale-->
    <xsl:variable name="products-info-of-sale-asset">
        <xsl:for-each select="$sale-asset/cs:child-rel()[@key='target.']/cs:asset()[@censhare:asset.type='product.']">
            <xsl:variable name="current-product" select="."/>
            <xsl:choose>
               <xsl:when test="$current-product/cs:child-rel()[@key='*']/cs:asset()[@censhare:asset.type='picture.*']">
                   
                    <!-- Handle Primary relation images -->
                    <xsl:for-each select="$current-product/cs:child-rel()[@key='user.main-picture.']/cs:asset()[@censhare:asset.type='picture.*']">
                     <xsl:variable name="current-image" select="."/>
                     <xsl:variable name="parentSaleAsset" select="$current-image/cs:parent-rel()[@key='user.']/cs:asset()[@censhare:asset.type='sale.']"/>
                        <xsl:if test="$current-image[not(asset_feature[@feature='censhare:resource-key']/@value_asset_key='canvas:primary-image-placeholder')]">
                            <xsl:sequence select="cs:image-process-within-lot($current-product, $current-image, $parentSaleAsset, 'Y')"/>
                        </xsl:if>
                    </xsl:for-each>
                   
                    <!-- Handle Additional relation images -->
                    <xsl:for-each select="$current-product/cs:child-rel()[@key='user.']/cs:asset()[@censhare:asset.type='picture.*']">
                      <xsl:variable name="current-image" select="."/>
                      <xsl:variable name="parentSaleAsset" select="$current-image/cs:parent-rel()[@key='user.']/cs:asset()[@censhare:asset.type='sale.']"/>
                        <xsl:if test="$current-image[not(asset_feature[@feature='censhare:resource-key']/@value_asset_key='canvas:primary-image-placeholder')]">
                            <xsl:sequence select="cs:image-process-within-lot($current-product, $current-image, $parentSaleAsset, 'N')"></xsl:sequence>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <!-- Handle Lots without having any images -->
                    <xsl:if test="$current-product/asset_feature[@feature='canvas:lotnumberandsuffix']/@value_string">
                        <content lot="{$current-product/asset_feature[@feature='canvas:lotnumberandsuffix']/@value_string}"/>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
       
        <xsl:for-each select="$sale-asset/cs:child-rel()[@key='*']/cs:asset()[@censhare:asset.type='picture.*']">
          <xsl:variable name="current-image" select="."/>
          <xsl:variable name="parentSaleAsset" select="$current-image/cs:parent-rel()[@key='user.']/cs:asset()[@censhare:asset.type='sale.']"/>
          <xsl:variable name="countparentRel" select="count($current-image/parent_asset_rel)"/>
          <xsl:variable name="imageAssetParentIDs" select="string-join($current-image/parent_asset_rel/@parent_asset, ',')"/>
          <xsl:variable name="currentImageAssetProductIDs" select="string-join($current-image/cs:parent-rel()[@key='*']/cs:asset()[@censhare:asset.type='product.']/@id, ',')"/>

          <xsl:choose>
            <xsl:when test="$countparentRel gt 1">
              <!-- Property -->
                <xsl:choose>
                  <xsl:when test="$current-image/cs:parent-rel()[@key='*']/cs:asset()[@censhare:asset.type='product.']">
                     <xsl:for-each select="$current-image/cs:parent-rel()[@key='*']/cs:asset()[@censhare:asset.type='product.']">
                      <xsl:variable name="current-product" select="."/>
                      <xsl:variable name="saleAssetIDs" select="string-join($current-product/cs:parent-rel()[@key='target.']/cs:asset()[@censhare:asset.type='sale.']/@id, ',')"/>
                      <xsl:choose>
                       <xsl:when test="contains($saleAssetIDs, $sale-asset-id)"/>
                       <xsl:otherwise>
                         <xsl:choose>
                            <xsl:when test="contains($imageAssetParentIDs, $sale-asset-id)">
                             <xsl:variable name="propertyExits">
                              <xsl:for-each select="tokenize($currentImageAssetProductIDs, ',')">
                              <xsl:variable name="propertyId" select="."/>
                               <xsl:choose>
                                <xsl:when test="contains($PropertyAssetIDs, $propertyId)">
                                  <xsl:value-of select="' true'"/> 
                                </xsl:when>
                                <xsl:otherwise>
                                 <xsl:value-of select="' false'"/> 
                                </xsl:otherwise>
                               </xsl:choose>
                              </xsl:for-each>
                             </xsl:variable>
                             <xsl:if test="not(contains($propertyExits, 'true'))">
                               <xsl:sequence select="cs:image-process-without-lot($current-image, $parentSaleAsset)"/>
                             </xsl:if>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:sequence select="cs:image-process-without-lot($current-image, $parentSaleAsset)"/>
                            </xsl:otherwise>
                         </xsl:choose>
                       </xsl:otherwise>
                      </xsl:choose>
                    </xsl:for-each>                        
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:sequence select="cs:image-process-without-lot($current-image, $parentSaleAsset)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:sequence select="cs:image-process-without-lot($current-image, $parentSaleAsset)"/>
              </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
    
    <!-- All the conent of Sale and Layout -->
    <xsl:variable name="all-product">
        <xsl:copy-of select="$products-info-of-sale-asset"/>
    </xsl:variable>
    
    <!-- Remove duplicate content -->
    <xsl:variable name="all-the-content">
        <xsl:for-each select="$all-product/content[not(@newid = preceding-sibling::content/@newid)]">
            <xsl:sort select="number(@lot)" data-type="number"/>
            <xsl:copy-of select="."/>    
        </xsl:for-each>
    </xsl:variable>
    
    <!-- Remove duplicate content ATK -->
    <xsl:variable name="removed-duplicate-content">
        <xsl:for-each select="$all-the-content/content">
          <xsl:choose>
          <xsl:when test=".[@name = following-sibling::content/@name ] or .[@name = preceding-sibling::content/@name ]  and .[@lotandsuffix = 'null'] and .[@primary = 'N']">
           <xsl:copy-of select="."/>
          </xsl:when>
            <xsl:when test=".[@name = following-sibling::content/@name ] and .[@name = preceding-sibling::content/@name ]  and .[@lotandsuffix = following-sibling::content/@lotandsuffix] and .[@lotandsuffix = preceding-sibling::content/@lotandsuffix] and .[@primary = 'N']"/>
            <xsl:when test=".[not(@name)] and .[@lotnumber =./following-sibling::content/@lotnumber] "/>
            <xsl:otherwise>
               <xsl:copy-of select="."/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
   
    
    <!-- Make a group with same number of lot -->
    <xsl:variable name="group-with-same-lot">
        <xsl:for-each-group select="$removed-duplicate-content/content[@lot != 'null']" group-adjacent="@lot">
            <contents>
                <xsl:copy-of select="current-group()"/>
            </contents>
        </xsl:for-each-group>
        <xsl:for-each-group select="$removed-duplicate-content/content[@lot = 'null']" group-adjacent="@lot">
            <contents>
                <xsl:copy-of select="current-group()"/>
            </contents>
        </xsl:for-each-group>
    </xsl:variable>
    
    <!-- In lot the newest ‘Primary’ image come first-->
    <xsl:variable name="new-primary-image-come-first">
        <xsl:for-each select="$group-with-same-lot/contents">
            <xsl:variable name="newgrup">
                <xsl:copy-of select="."/>
            </xsl:variable>
            <contents>
                <xsl:for-each select="$newgrup/contents/content[@primary='Y']">
                    <xsl:sort select="number(@id)" data-type="number" order="descending"/>
                    <xsl:copy-of select="."/>
                </xsl:for-each>
                <xsl:for-each select="$newgrup/contents/content[@primary!='Y' or not(@primary)]">
                    <xsl:copy-of select="."/>
                </xsl:for-each>
            </contents>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:template match="asset">
        <xsl:variable name="asset" select="."/>
        <xsl:variable name="asset-id" select="@id"/>
        <xsl:variable name="asset-name" select="@name"/>
        <xsl:variable name="salenumber" select="$asset/asset_feature[@feature='canvas:jdesale.salenumber']/@value_long"/>
        <xsl:variable name="salesite" select="$asset/asset_feature[@feature='canvas:jdesale.salesite']/@value_key"/>
        <!--<xsl:copy-of select="$lot-assets"/>-->
        <xsl:variable name="out"/>
        <!-- Open VirtualFileSystem from this command -->
        <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
        
        <xsl:variable name="csv-report-file">
            <report title="CSV Report file">
                <report-definition>
                    <head>
                        <header level="1">CIMD Image Report : <xsl:value-of select="$salesite"/> - <xsl:value-of select="$salenumber"/></header>
                    </head>
                    <columns>
                        <column caption="ML Image Name" attribute="image"/>
                        <column caption="Lot Number &amp; Suffix" attribute="lot"/>
                        <column caption="Primary Image" attribute="primary"/>
                        <column caption="ML Origin Location" attribute="ml"/>
                        <column caption="ML Production Status" attribute="workflow_step"/>
                    </columns>
                </report-definition>
                <data>
                    <xsl:for-each select="$new-primary-image-come-first/contents">
                        <xsl:for-each select="content">
                            <xsl:variable name="current-position" select="position() - 1" as="xs:integer"/>
                            <xsl:variable name="position" select="concat('a', $current-position)"/>
                            <xsl:variable name="image" select="@name"/>
                            <!--For lot 4 digit-->
                            <xsl:variable name="lot4">
                                <xsl:choose>
                                    <xsl:when test="string-length(@lot) = 1">
                                        <xsl:value-of select="concat('000', @lot)"/>
                                    </xsl:when>
                                    <xsl:when test="string-length(@lot) = 2">
                                        <xsl:value-of select="concat('00', @lot)"/>
                                    </xsl:when>
                                    <xsl:when test="string-length(@lot) = 3">
                                        <xsl:value-of select="concat('0', @lot)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="@lot"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="lot">
                                <xsl:choose>
                                    <xsl:when test="position() = 1">
                                        <xsl:value-of select="normalize-space($lot4)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:variable name="exact-position" select="substring-after($position, 'a')" as="xs:integer"/>
                                        <xsl:variable name="modify-position" select="$exact-position" as="xs:integer"/>
                                        <xsl:value-of select="if (string-length($position) = 2) then concat(normalize-space($lot4), '_', '00', $modify-position) else if (string-length($position) = 3) then concat(normalize-space($lot4), '_', '0', $modify-position) else concat(normalize-space($lot4), '_', $modify-position)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <!--<xsl:variable name="lot" select="if (string-length($position) = 2) then concat(lot, '_', '00', substring-after($position, 'a')) else if (string-length($position) = 3) then concat(lot, '_', '0', substring-after($position, 'a')) else concat(lot, '_', substring-after($position, 'a'))"/>-->
                            <xsl:variable name="primary" select="@primary"/>
                            <xsl:variable name="ml" select="@ml"/>
                            <xsl:variable name="workflow_step" select="@wf-step"/>
                            <item image="{$image}" lot="{if (contains($lot, 'null') or starts-with($lot,'_'))  then () else ($salesite,'_',$salenumber,'_',$lot)}" primary="{$primary}" ml="{$ml}" workflow_step="{$workflow_step}"/>
                        </xsl:for-each>
                    </xsl:for-each>
                </data>
            </report>
        </xsl:variable>
        
        <xsl:variable name="file-name">
            <xsl:sequence select="concat(sd:clean-filename(concat($external-id, '_', $dateTime)), '.xml')"/>
        </xsl:variable>
        
        <cs:command name="com.censhare.api.transformation.XslTransformation"  returning="processed-asset-xml">
            <cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=report:table-builder-xml-to-excel-xml/storage/master/file'"/>
            <cs:param name="source" select="$csv-report-file"/>
            <cs:param name="dest" select="concat($out, $file-name)"/>
            <cs:param name="output">
                <output indent="yes"/>
            </cs:param>
        </cs:command>
        
        <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="outDir">
            <cs:param name="id" select="$out"/>
        </cs:command>
        
        <cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="resultVariable">
            <cs:param name="source">
                <asset name="{substring-before($file-name, '.')}" type="text.">
                    <asset_element key="actual." idx="0"/>
                    <storage_item element_idx="0" key="master" mimetype="text/xml" corpus:asset-temp-file-url="{concat($out, $file-name)}"/>
                    <parent_asset_rel parent_asset="{$asset-id}" key="user."/>
                </asset>
            </cs:param>
        </cs:command>
        
    </xsl:template>
    
    <xsl:function name="cs:image-process-within-lot">
        <xsl:param name="current-product"/>
        <xsl:param name="current-image"/>
        <xsl:param name="parentSaleAsset"/>
        <xsl:param name="primary"/>
        <xsl:variable name="current-product-id" select="$current-product/@id"/>
        <xsl:variable name="current-asset-id" select="$current-image/@id"/>
        <xsl:variable name="ml" select="$current-image/asset_feature[@feature='canvas:ml.originlocation']/@value_string"/> <!--Asia-->
        <xsl:variable name="productionstatus_key" select="$current-image/asset_feature[@feature='canvas:ml.productionstatus']/@value_key"/> <!--40-->
        <xsl:variable name="wf-step-name" select="cs:master-data('feature_value')[@value_key = $productionstatus_key  and @feature='canvas:ml.productionstatus']/@name"/> <!--Passed4Print-->
        
        <xsl:choose>
          <xsl:when test="$parentSaleAsset[asset_feature[@feature='canvas:jdesale.salenumber']/@value_long=9000 or asset_feature[@feature='canvas:jdesale.salenumber']/@value_long=9001]">
            <content lotnumber="{$current-product/parent_asset_rel/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long}" lotsuffix="{$current-product/parent_asset_rel/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string}" lotandsuffix="{concat($current-product/parent_asset_rel/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long, $current-product/parent_asset_rel/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string)}"/>
          </xsl:when>
          <xsl:otherwise>
        <content name="{if (contains($current-image/@name, '.')) then substring-before($current-image/@name, '.') else $current-image/@name}"
            lot="{$current-product[child_asset_rel[@child_asset = $current-asset-id]]/asset_feature[@feature='canvas:lotnumberandsuffix']/@value_string}"
            primary="{if ($primary='Y') then 'Y' else 'N'}"
            ml="{$ml}" 
            wf-step="{$wf-step-name}"
            type="picture"
            id="{$current-asset-id}"
            newid="{concat($current-asset-id,$current-product-id)}"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="cs:image-process-without-lot">
        <xsl:param name="current-image"/>
        <xsl:param name="parentSaleAsset"/>
        <xsl:variable name="ml" select="$current-image/asset_feature[@feature='canvas:ml.originlocation']/@value_string"/> <!--Asia-->
        <xsl:variable name="productionstatus_key" select="$current-image/asset_feature[@feature='canvas:ml.productionstatus']/@value_key"/> <!--40-->
        <xsl:variable name="wf-step-name" select="cs:master-data('feature_value')[@value_key = $productionstatus_key  and @feature='canvas:ml.productionstatus']/@name"/> <!--Passed4Print-->
        <xsl:choose>
          <xsl:when test="$parentSaleAsset[asset_feature[@feature='canvas:jdesale.salenumber']/@value_long=9000 or asset_feature[@feature='canvas:jdesale.salenumber']/@value_long=9001]"/>
            <xsl:otherwise>
             <content name="{if (contains($current-image/@name, '.')) then substring-before($current-image/@name, '.') else $current-image/@name}"
            primary="N"
            ml="{$ml}" 
            wf-step="{$wf-step-name}"
            lot="null"
            id="{$current-image/@id}"
            newid="{$current-image/@id}"/> 
            </xsl:otherwise>
            </xsl:choose>
    </xsl:function>
    
    <xsl:function name="sd:clean-filename" as="xs:string">
        <xsl:param name="filename"/>
        <!-- replacing any of {' ', '?', ''', ':', '\', '/', '~', '"'} with an underscore -->
        <xsl:value-of select="lower-case(translate(translate(translate(translate(translate(translate(translate(translate(translate(translate($filename,'?','_'),&quot;'&quot;,''),'&quot;', ''),':','_'),'\','_'),'/',''),'~',''), ' ', '_'), '-', '_'), ',', ''))"/>
    </xsl:function>
    
</xsl:stylesheet>