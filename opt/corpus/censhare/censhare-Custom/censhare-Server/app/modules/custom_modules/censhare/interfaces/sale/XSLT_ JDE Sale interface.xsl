<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/SaleHeaderDataResponse"
    exclude-result-prefixes="#all">
    
    <!-- Transformation that creates new asset from body XML of REST post method -->
    
    <!-- output -->
    <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
    
    <!-- parameters -->
    <xsl:param name="data"/>
    
    <xsl:template match="/">
        
        <xsl:message> JDE Sale Interface: Source data: <xsl:copy-of select="$data"/></xsl:message>
        
        <xsl:variable name="error-flag">
            <xsl:value-of select="cs:error-checking($data)"/>
        </xsl:variable>
        
        <xsl:message>
            error flag: <xsl:value-of select="$error-flag"/>
        </xsl:message>
        
        <xsl:choose>
            <xsl:when test="$error-flag = 'false'">
                <cs:command name="com.censhare.api.transformation.XslTransformation" returning="output-xml">
                    <cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:create-xml-structure-sale/storage/master/file'"/>
                    <cs:param name="source" select="$data"/>
                </cs:command>
                
                <cs:command name="com.censhare.api.transformation.XslTransformation" returning="created-asset-xml">
                    <cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=canvas:sdi_processor-sale/storage/master/file'"/>
                    <cs:param name="source" select="$output-xml"/>
                </cs:command>
                <xsl:if test="not(empty($created-asset-xml))">
                    <xsl:variable name="rest-response">
                        <ns0:CANVASSaleHeaderDataResponse xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/SaleHeaderDataResponse">
                            <Item>
                                <xsl:copy-of select="$data//UniqueIdentifier" copy-namespaces="no"/>
                                <status value="success"></status>
                                <xsl:copy-of select="$data//Item/node() except $data//UniqueIdentifier" copy-namespaces="no"/>
                            </Item>
                        </ns0:CANVASSaleHeaderDataResponse>
                    </xsl:variable>
                    <xsl:copy-of select="$rest-response"/>
                </xsl:if>
            </xsl:when>
            
            <xsl:otherwise>
                <xsl:variable name="rest-response">
                    <ns0:CANVASSaleHeaderDataResponse xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/SaleHeaderDataResponse">
                        <Item>
                            <xsl:copy-of select="$data//UniqueIdentifier"  copy-namespaces="no"/>
                            <status value="error" message_id="{$data//UniqueIdentifier}" message="{$error-flag}"></status>
                            <xsl:copy-of select="$data//Item/node() except $data//UniqueIdentifier"  copy-namespaces="no"/>
                        </Item>
                    </ns0:CANVASSaleHeaderDataResponse>
                </xsl:variable>
                <xsl:copy-of select="$rest-response"/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:function name="cs:error-checking">
        <xsl:param name="data"/>
        <xsl:variable name="sale-attribute-ids" select="$data//SaleNumber"/>
        <xsl:variable name="id-extern-flag" select="if (normalize-space($sale-attribute-ids/text()) = '') then false() else true()"/>
        <xsl:message>
            Sale: ID extern: <xsl:value-of select="$id-extern-flag"/>
        </xsl:message>
        <xsl:variable name="retrun-message">
            <xsl:if test="count($id-extern-flag) &gt; 1 or $id-extern-flag = false()">
                <xsl:text>Sale: Value in SaleNumber element is required.</xsl:text>
            </xsl:if>
        </xsl:variable>
        <xsl:message>
            return message: <xsl:value-of select="$retrun-message"/>, 
            <xsl:value-of select="if ($retrun-message != '') then $retrun-message else 'false'"/>
        </xsl:message>
        <xsl:value-of select="if ($retrun-message != '') then $retrun-message else 'false'"/>
        
    </xsl:function>
</xsl:stylesheet>