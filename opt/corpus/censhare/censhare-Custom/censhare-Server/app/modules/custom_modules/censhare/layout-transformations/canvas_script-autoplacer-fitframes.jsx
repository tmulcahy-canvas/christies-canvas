﻿// ---- autoplacer fitframes ---------------------
// fit image frames and text frames to their content
// the script is read by the autoplacer renderer command XSLT
// which will insert specific configuration parts

﻿// ---- configuration ---------------------
// object styles text frames will be filtered for
const targetTextObjectStyles = ["Lot Text"];

// configuration, will be filled by XSLT with array of box IDs
const config = {
  imageBoxes: [],
  textBoxes: []
}
// --------------------------------------------

// ---- global variables ---------------------
var targetDocument;
// --------------------------------------------

try {
  // get document 
  var censhareTargetDocumentIndex = app.scriptArgs.getValue("censhareTargetDocumentIndex"); 
  var censhareTargetDocument = app.scriptArgs.getValue("censhareTargetDocument"); 
  if (censhareTargetDocument != "") {
    targetDocument = app.documents.itemByID(parseInt(censhareTargetDocument)); 
  } else {
    targetDocument = app.documents.item(0);
  }

  // fit images
  for (var i=0; i<config.imageBoxes.length; i++) {
    var imageBox = targetDocument.rectangles.itemByID(config.imageBoxes[i]);
    if (isImageBox(imageBox)) {
      // Remove any existing frame fitting options (since otherwise adjustment of frame borders will scale the image)
      imageBox.clearFrameFittingOptions();
      // Scale image proportionally into the frame
      imageBox.fit(FitOptions.PROPORTIONALLY);
      // Finally adjust the borders to the size of the image to remove empty (white) space
      fitImageMinimize(imageBox);
    }
  }

  // find object styles the text frames will be filtered for
  var textStyles = new Array();
  for (var i=0; i<targetTextObjectStyles.length; i++) {
    var targetOS = targetDocument.objectStyles.itemByName(targetTextObjectStyles[i]);
    if (!targetOS.isValid) continue;    
    textStyles.push(targetOS);
  }  
  // fit text frames
  for (var i=0; i<config.textBoxes.length; i++) {
    var textBox = targetDocument.rectangles.itemByID(config.textBoxes[i]);
    if (matchesObjectStyles(textBox, textStyles)) {
      fitTextframeMinimize(textBox);
    }
  }
  
  

} catch(e) {
  // output error messages
  var message = "Error: " + e.description + " (line " + e.line + ")";
  // alert(message);
  app.scriptArgs.setValue("censhareResult", message);
} finally {
}


// ------------------------------------------------


// loop through sourceobjects, get objects with desired applied object style
function getObjectsByStyle(objectstyle, sourceobjects) {
  var objects = new Array();
  sourceobjects = sourceobjects || targetDocument.allPageItems;
  for (var i=0; i<sourceobjects.length; i++) {
    if (sourceobjects[i].hasOwnProperty('appliedObjectStyle') && (sourceobjects[i].appliedObjectStyle == objectstyle)) {
      objects.push(sourceobjects[i]);
    }
  }
  return objects;
}

// test if an object is an image frame
function isImageBox(rectangle) {
  if (!rectangle.hasOwnProperty('graphics')) {
    return false;
  }
  if (!rectangle.graphics.firstItem().isValid) {
    return false;
  }  
  return true;
}

function matchesObjectStyles(object, objectStyles) {
  if (!object.hasOwnProperty('appliedObjectStyle')) return false;
  for (var i=0; i<objectStyles.length; i++) {
    if (object.appliedObjectStyle == objectStyles[i]) return true;
  }
  return false;  
}

// fit frame to image content, by only reducing sizefor every direction
function fitImageMinimize(rectangle) {
  var graphic = rectangle.graphics.firstItem();
  
  var frameBounds = rectangle.geometricBounds;
  var graphicBounds = graphic.geometricBounds;

  if (frameBounds[0] < graphicBounds[0]) frameBounds[0] = graphicBounds[0];
  if (frameBounds[1] < graphicBounds[1]) frameBounds[1] = graphicBounds[1];
  if (frameBounds[2] > graphicBounds[2]) frameBounds[2] = graphicBounds[2];
  if (frameBounds[3] > graphicBounds[3]) frameBounds[3] = graphicBounds[3];

  rectangle.geometricBounds = frameBounds;
}

// fit textframe to content
function fitTextframeMinimize(textframe) {
  if (!textframe.hasOwnProperty('parentStory')) return;
  if (textframe.nextTextFrame && textframe.nextTextFrame.isValid) return;
  if (textframe.overflows) return;
  textframe.fit(FitOptions.FRAME_TO_CONTENT);
}
