<?xml version="1.0" encoding="UTF-8"?>

<!-- 
#########################################################
censhare standard content Layout Transformation

Version 1.0 - Mai 14th 2013 
#########################################################
-->

<xsl:stylesheet exclude-result-prefixes="xs xd fn xi cs map temp func" version="2.0" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:func="http://ns.censhare.de/functions" xmlns:map="http://ns.censhare.de/mapping" xmlns:temp="http://ns.censhare.de/elements" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!-- format mappings              -->
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	
	<xd:doc>
		<xd:desc>
			<xd:p>Mapping of element names to InDesign paragraph styles</xd:p>
			<xd:p>
				<xd:i>If not used, include some empty element</xd:i>
			</xd:p>
			<xd:p>Every entry consists of an <xd:b>&lt;cen-map-entry&gt;</xd:b> element with following attributes:</xd:p>
			<xd:ul>
				<xd:li><xd:b>element</xd:b>: name of the element, or part of hierarchy in the form: ancestor/parent/child</xd:li>
				<xd:li><xd:b>stylename</xd:b>: name of InDesign paragraph style</xd:li>
				<xd:li><xd:b>stylegroup</xd:b>: name of style group, leave empty or omit if style not part of a group</xd:li>
				<xd:li><xd:b>add-charstyle</xd:b>: If not empty, CSMapping will additionally be applied to the matching element. Default: No character style applied</xd:li>
				<xd:li><xd:b>attr-name</xd:b>: name of an attribute which will be also matched</xd:li>
				<xd:li><xd:b>attr-value</xd:b>: value of the attribute (attr-name) which must be matching</xd:li>
			</xd:ul>
		</xd:desc>
	</xd:doc>
	
	<xsl:variable name="PSMapping">
		<!-- Removed default mapping. Christies only -->
		<map:entry attr-name="position" attr-value="first" element="estimate-row" stylegroup="" stylename="Price 1"/>
		<map:entry attr-name="position" attr-value="second" element="estimate-row" stylegroup="" stylename="Price 2"/>
		<map:entry attr-name="position" attr-value="third" element="estimate-row" stylegroup="" stylename="Price 3"/>
		<map:entry add-charstyle="true"  element="inline-quantity" stylegroup="" stylename="Other Details"/>
		<map:entry add-charstyle="true"  element="inline-quantitydesc" stylegroup="" stylename="Other Details"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="prelot-paragraph" element="paragraph" stylegroup="" stylename="Pre Lot Text"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="prelot-paragraph-SL" element="paragraph" stylegroup="" stylename="Pre Lot Text-SL"/>
		<map:entry add-charstyle="true" element="lot" stylename="Lot Number"/>
		<map:entry add-charstyle="true" element="lot-number-only"  stylename="Number"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="firstline-paragraph" element="paragraph" stylegroup="" stylename="First Line"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="firstline-paragraph-SL" element="paragraph" stylegroup="" stylename="First Line-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="makerdate-paragraph" element="paragraph" stylegroup="" stylename="Maker &amp; Date"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="makerdate-paragraph-SL" element="paragraph" stylegroup="" stylename="Maker &amp; Date-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="artistdate-paragraph" element="paragraph" stylegroup="" stylename="Artist &amp; Date"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="artistdate-paragraph-SL" element="paragraph" stylegroup="" stylename="Artist &amp; Date-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="title-paragraph" element="paragraph" stylegroup="" stylename="Title"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="title-paragraph-SL" element="paragraph" stylegroup="" stylename="Title-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="details-paragraph" element="paragraph" stylegroup="" stylename="Details"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="details-paragraph-SL" element="paragraph" stylegroup="" stylename="Details-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="signature-paragraph" element="paragraph" stylegroup="" stylename="Signature"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="signature-paragraph-SL" element="paragraph" stylegroup="" stylename="Signature-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="certificate-paragraph" element="paragraph" stylegroup="" stylename="Certificate"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="certificate-paragraph-SL" element="paragraph" stylegroup="" stylename="Certificate-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="picturemedium-paragraph" element="paragraph" stylegroup="" stylename="Picture Medium"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="picturemedium-paragraph-SL" element="paragraph" stylegroup="" stylename="Picture Medium-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="picturesize-paragraph" element="paragraph" stylegroup="" stylename="Picture Size"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="picturesize-paragraph-SL" element="paragraph" stylegroup="" stylename="Picture Size-SL"/>
		<!--<map:entry add-charstyle="true"  element="sizes" stylegroup="" stylename="Size"/>-->
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="size-expert" element="paragraph/E" stylegroup="" stylename="Size"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="size-paragraph" element="paragraph" stylegroup="" stylename="Size"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="size-paragraph-SL" element="paragraph" stylegroup="" stylename="Size-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="weight-paragraph" element="paragraph" stylegroup="" stylename="Weight"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="weight-paragraph-SL" element="paragraph" stylegroup="" stylename="Weight-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="marked-paragraph" element="paragraph" stylegroup="" stylename="Marked"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="marked-paragraph-SL" element="paragraph" stylegroup="" stylename="Marked-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="otherdetails-paragraph" element="paragraph" stylegroup="" stylename="Other Details"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="otherdetails-paragraph-SL" element="paragraph" stylegroup="" stylename="Other Details-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="provenance-subheadline-1" element="subheadline-1" stylegroup="" stylename="Notes"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="provenance-subheadline-1-SL" element="subheadline-1" stylegroup="" stylename="Notes-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="provenance-paragraph" element="paragraph" stylegroup="" stylename="Notes Text"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="provenance-paragraph-SL" element="paragraph" stylegroup="" stylename="Notes Text-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="exhibited-subheadline-1" element="subheadline-1" stylegroup="" stylename="Notes"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="exhibited-subheadline-1-SL" element="subheadline-1" stylegroup="" stylename="Notes-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="exhibited-paragraph" element="paragraph" stylegroup="" stylename="Notes Text"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="exhibited-paragraph-SL" element="paragraph" stylegroup="" stylename="Notes Text-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="literature-subheadline-1" element="subheadline-1" stylegroup="" stylename="Notes"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="literature-subheadline-1-SL" element="subheadline-1" stylegroup="" stylename="Notes-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="literature-paragraph" element="paragraph" stylegroup="" stylename="Notes Text"/>		
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="literature-paragraph-SL" element="paragraph" stylegroup="" stylename="Notes Text-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="engraved-subheadline-1" element="subheadline-1" stylegroup="" stylename="Notes"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="engraved-subheadline-1-SL" element="subheadline-1" stylegroup="" stylename="Notes-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="engraved-paragraph" element="paragraph" stylegroup="" stylename="Notes Text"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="engraved-paragraph-SL" element="paragraph" stylegroup="" stylename="Notes Text-SL"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="cataloguenotes-paragraph" element="paragraph" stylegroup="" stylename="Catalogue Notes"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="cataloguenotes-subheadline-1" element="subheadline-1" stylegroup="" stylename="Notes"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="cataloguenotes-paragraph-SL" element="paragraph" stylegroup="" stylename="Catalogue Notes"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="postlot-paragraph" element="paragraph" stylegroup="" stylename="Post Lot Text"/>
		<map:entry add-charstyle="true" attr-name="text-type" attr-value="postlot-paragraph-SL" element="paragraph" stylegroup="" stylename="Post Lot Text-SL"/>
	</xsl:variable>
	
	<xd:doc>
		<xd:desc>
			<xd:p>Mapping of element names to InDesign character styles</xd:p>
			<xd:p>
				<xd:i>If not used, include some empty element</xd:i>
			</xd:p>
			<xd:p>Every entry consists of an <xd:b>&lt;cen-map-entry&gt;</xd:b> element with following attributes:</xd:p>
			<xd:ul>
				<xd:li><xd:b>element</xd:b>: name of the element, or part of hierarchy in the form: ancestor/parent/child</xd:li>
				<xd:li><xd:b>stylename</xd:b>: name of InDesign paragraph style</xd:li>
				<xd:li><xd:b>stylegroup</xd:b>: name of style group, leave empty or omit if style not part of a group</xd:li>
				<xd:li><xd:b>attr-name</xd:b>: name of an attribute which will be also matched</xd:li>
				<xd:li><xd:b>attr-value</xd:b>: value of the attribute (attr-name) which must be matching</xd:li>
			</xd:ul>
		</xd:desc>
	</xd:doc>
	
	<xsl:variable name="CSMapping">
		<!-- Removed default mapping. Christies only -->
		<map:entry  element="inline-quantity" stylegroup="Quantity" stylename="Quantity"/>
		<map:entry  element="inline-quantitydesc" stylegroup="Quantity Description" stylename="Quantity Description"/>
		<map:entry attr-name="text-type" attr-value="quantitydesc-p" element="paragraph/inline-quantitydesc/p" stylegroup="Quantity Description" stylename="Quantity Description&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="quantitydesc-P" element="paragraph/inline-quantitydesc/P" stylegroup="Quantity Description" stylename="Quantity Description&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="quantitydesc-e" element="paragraph/inline-quantitydesc/e" stylegroup="Quantity Description" stylename="Quantity Description&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="quantitydesc-E" element="paragraph/inline-quantitydesc/E" stylegroup="Quantity Description" stylename="Quantity Description&lt;e&gt;"/>
		
		<map:entry attr-name="position" attr-value="first" element="estimate-row/estimate" stylegroup="Price 1" stylename="Price 1"/>
		<map:entry attr-name="position" attr-value="second" element="estimate-row/estimate" stylegroup="Price 2" stylename="Price 2"/>
		<map:entry attr-name="position" attr-value="third" element="estimate-row/estimate" stylegroup="Price 3" stylename="Price 3"/>
		<map:entry attr-name="text-type" attr-value="prelot-paragraph" element="paragraph" stylegroup="Pre Lot Text" stylename="Pre Lot Text"/>
		<map:entry attr-name="text-type" attr-value="prelot-bold" element="paragraph/bold" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-italic" element="paragraph/italic" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-bold-italic" element="paragraph/bold-italic" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-sc" element="paragraph/sc" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-sup" element="paragraph/sup" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-sub" element="paragraph/sub" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-p" element="paragraph/i" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-P" element="paragraph/P" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;p&gt;"/>
		<!--map:entry attr-name="text-type" attr-value="prelot-E" element="paragraph/E" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;p&gt;"/-->
		<map:entry attr-name="text-type" attr-value="prelot-e" element="paragraph/i" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-E" element="paragraph/E" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-s" element="paragraph/i" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-z" element="paragraph/i" stylegroup="Pre Lot Text" stylename="Pre Lot Text&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-paragraph-SL" element="paragraph" stylegroup="Pre Lot Text" stylename="Pre Lot Text-SL"/>
		<map:entry attr-name="text-type" attr-value="prelot-SL-b" element="paragraph/b" stylegroup="Pre Lot Text" stylename="Pre Lot Text-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="prelot-SL-e" element="paragraph/e" stylegroup="Pre Lot Text" stylename="Pre Lot Text-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="firstline-paragraph" element="paragraph" stylegroup="First Line" stylename="First Line"/>
		<map:entry attr-name="text-type" attr-value="firstline-bold" element="paragraph/bold" stylegroup="First Line" stylename="First Line&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-italic" element="paragraph/italic" stylegroup="First Line" stylename="First Line&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-bold-italic" element="paragraph/bold-italic" stylegroup="First Line" stylename="First Line&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-sc" element="paragraph/sc" stylegroup="First Line" stylename="First Line&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-sup" element="paragraph/sup" stylegroup="First Line" stylename="First Line&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-sub" element="paragraph/sub" stylegroup="First Line" stylename="First Line&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-p" element="paragraph/i" stylegroup="First Line" stylename="First Line&lt;p&gt;"/>
		<!--map:entry attr-name="text-type" attr-value="firstline-P" element="paragraph/P" stylegroup="First Line" stylename="First Line&lt;e&gt;"/-->
		<map:entry attr-name="text-type" attr-value="firstline-P" element="paragraph/P" stylegroup="First Line" stylename="First Line&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-e" element="paragraph/i" stylegroup="First Line" stylename="First Line&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-E" element="paragraph/E" stylegroup="First Line" stylename="First Line&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-s" element="paragraph/i" stylegroup="First Line" stylename="First Line&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-z" element="paragraph/i" stylegroup="First Line" stylename="First Line&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-paragraph-SL" element="paragraph" stylegroup="First Line" stylename="First Line-SL"/>
		<map:entry attr-name="text-type" attr-value="firstline-SL-b" element="paragraph/b" stylegroup="First Line" stylename="First Line-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="firstline-SL-e" element="paragraph/e" stylegroup="First Line" stylename="First Line-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="makerdate-paragraph" element="paragraph" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date"/>
		<map:entry attr-name="text-type" attr-value="makerdate-bold" element="paragraph/bold" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-italic" element="paragraph/italic" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-bold-italic" element="paragraph/bold-italic" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-sc" element="paragraph/sc" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-sup" element="paragraph/sup" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-sub" element="paragraph/sub" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-p" element="paragraph/i" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-P" element="paragraph/P" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-e" element="paragraph/i" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-E" element="paragraph/E" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-s" element="paragraph/i" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-z" element="paragraph/i" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-paragraph-SL" element="paragraph" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date-SL"/>
		<map:entry attr-name="text-type" attr-value="makerdate-SL-b" element="paragraph/b" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="makerdate-SL-e" element="paragraph/e" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="artistdate-paragraph" element="paragraph" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date"/>
		<map:entry attr-name="text-type" attr-value="artistdate-bold" element="paragraph/bold" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-italic" element="paragraph/italic" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-bold-italic" element="paragraph/bold-italic" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-sc" element="paragraph/sc" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-sup" element="paragraph/sup" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-sub" element="paragraph/sub" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-p" element="paragraph/i" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-P" element="paragraph/P" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-e" element="paragraph/i" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-E" element="paragraph/E" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-s" element="paragraph/i" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-z" element="paragraph/i" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-paragraph-SL" element="paragraph" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date-SL"/>
		<map:entry attr-name="text-type" attr-value="artistdate-SL-b" element="paragraph/b" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="artistdate-SL-e" element="paragraph/e" stylegroup="Artist &amp; Date" stylename="Artist &amp; Date-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="title-paragraph" element="paragraph" stylegroup="Title" stylename="Title"/>
		<map:entry attr-name="text-type" attr-value="title-bold" element="paragraph/bold" stylegroup="Title" stylename="Title&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-italic" element="paragraph/italic" stylegroup="Title" stylename="Title&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-bold-italic" element="paragraph/bold-italic" stylegroup="Title" stylename="Title&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-sc" element="paragraph/sc" stylegroup="Title" stylename="Title&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-sup" element="paragraph/sup" stylegroup="Title" stylename="Title&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-su " element="paragraph/sub" stylegroup="Title" stylename="Title&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-p" element="paragraph/i" stylegroup="Title" stylename="Title&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-P" element="paragraph/P" stylegroup="Title" stylename="Title&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-e" element="paragraph/i" stylegroup="Title" stylename="Title&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-E" element="paragraph/E" stylegroup="Title" stylename="Title&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-s" element="paragraph/i" stylegroup="Title" stylename="Title&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-z" element="paragraph/i" stylegroup="Title" stylename="Title&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-paragraph-SL" element="paragraph" stylegroup="Title" stylename="Title-SL"/>
		<map:entry attr-name="text-type" attr-value="title-SL-b" element="paragraph/b" stylegroup="Title" stylename="Title-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="title-SL-e" element="paragraph/e" stylegroup="Title" stylename="Title-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="details-paragraph" element="paragraph" stylegroup="Details" stylename="Details"/>
		<map:entry attr-name="text-type" attr-value="details-bold" element="paragraph/bold" stylegroup="Details" stylename="Details&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-italic" element="paragraph/italic" stylegroup="Details" stylename="Details&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-bold-italic" element="paragraph/bold-italic" stylegroup="Details" stylename="Details&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-sc" element="paragraph/sc" stylegroup="Details" stylename="Details&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-sup" element="paragraph/sup" stylegroup="Details" stylename="Details&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-sub" element="paragraph/sub" stylegroup="Details" stylename="Details&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-p" element="paragraph/i" stylegroup="Details" stylename="Details&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-P" element="paragraph/P" stylegroup="Details" stylename="Details&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-e" element="paragraph/i" stylegroup="Details" stylename="Details&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-E" element="paragraph/E" stylegroup="Details" stylename="Details&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-s" element="paragraph/i" stylegroup="Details" stylename="Details&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-z" element="paragraph/i" stylegroup="Details" stylename="Details&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-paragraph-SL" element="paragraph" stylegroup="Details" stylename="Details-SL"/>
		<map:entry attr-name="text-type" attr-value="details-SL-b" element="paragraph/b" stylegroup="Details" stylename="Details-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="details-SL-e" element="paragraph/e" stylegroup="Details" stylename="Details-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="signature-paragraph" element="paragraph" stylegroup="Signature" stylename="Signature"/>
		<map:entry attr-name="text-type" attr-value="signature-bold" element="paragraph/bold" stylegroup="Signature" stylename="Signature&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-italic" element="paragraph/italic" stylegroup="Signature" stylename="Signature&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-bold-italic" element="paragraph/bold-italic" stylegroup="Signature" stylename="Signature&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-sc" element="paragraph/sc" stylegroup="Signature" stylename="Signature&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-sup" element="paragraph/sup" stylegroup="Signature" stylename="Signature&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-sub" element="paragraph/sub" stylegroup="Signature" stylename="Signature&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-p" element="paragraph/i" stylegroup="Signature" stylename="Signature&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-P" element="paragraph/P" stylegroup="Signature" stylename="Signature&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-e" element="paragraph/i" stylegroup="Signature" stylename="Signature&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-E" element="paragraph/E" stylegroup="Signature" stylename="Signature&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-s" element="paragraph/i" stylegroup="Signature" stylename="Signature&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-z" element="paragraph/i" stylegroup="Signature" stylename="Signature&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-paragraph-SL" element="paragraph" stylegroup="Signature" stylename="Signature-SL"/>
		<map:entry attr-name="text-type" attr-value="signature-SL-b" element="paragraph/b" stylegroup="Signature" stylename="Signature-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="signature-SL-e" element="paragraph/e" stylegroup="Signature" stylename="Signature-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="certificate-paragraph" element="paragraph" stylegroup="Certificate" stylename="Certificate"/>
		<map:entry attr-name="text-type" attr-value="certificate-bold" element="paragraph/bold" stylegroup="Certificate" stylename="Certificate&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-italic" element="paragraph/italic" stylegroup="Certificate" stylename="Certificate&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-bold-italic" element="paragraph/bold-italic" stylegroup="Certificate" stylename="Certificate&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-sc" element="paragraph/sc" stylegroup="Certificate" stylename="Certificate&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-sup" element="paragraph/sup" stylegroup="Certificate" stylename="Certificate&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-sub" element="paragraph/sub" stylegroup="Certificate" stylename="Certificate&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-p" element="paragraph/i" stylegroup="Certificate" stylename="Certificate&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-P" element="paragraph/P" stylegroup="Certificate" stylename="Certificate&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-e" element="paragraph/i" stylegroup="Certificate" stylename="Certificate&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-E" element="paragraph/E" stylegroup="Certificate" stylename="Certificate&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-s" element="paragraph/i" stylegroup="Certificate" stylename="Certificate&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-z" element="paragraph/i" stylegroup="Certificate" stylename="Certificate&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-paragraph-SL" element="paragraph" stylegroup="Certificate" stylename="Certificate-SL"/>
		<map:entry attr-name="text-type" attr-value="certificate-SL-b" element="paragraph/b" stylegroup="Certificate" stylename="Certificate-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="certificate-SL-e" element="paragraph/e" stylegroup="Certificate" stylename="Certificate-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="picturemedium-paragraph" element="paragraph" stylegroup="Picture Medium" stylename="Picture Medium"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-bold" element="paragraph/bold" stylegroup="Picture Medium" stylename="Picture Medium&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-italic" element="paragraph/italic" stylegroup="Picture Medium" stylename="Picture Medium&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-bold-italic" element="paragraph/bold-italic" stylegroup="Picture Medium" stylename="Picture Medium&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-sc" element="paragraph/sc" stylegroup="Picture Medium" stylename="Picture Medium&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-sup" element="paragraph/sup" stylegroup="Picture Medium" stylename="Picture Medium&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-sub" element="paragraph/sub" stylegroup="Picture Medium" stylename="Picture Medium&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-p" element="paragraph/i" stylegroup="Picture Medium" stylename="Picture Medium&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-P" element="paragraph/P" stylegroup="Picture Medium" stylename="Picture Medium&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-e" element="paragraph/i" stylegroup="Picture Medium" stylename="Picture Medium&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-E" element="paragraph/E" stylegroup="Picture Medium" stylename="Picture Medium&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-s" element="paragraph/i" stylegroup="Picture Medium" stylename="Picture Medium&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-z" element="paragraph/i" stylegroup="Picture Medium" stylename="Picture Medium&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-paragraph-SL" element="paragraph" stylegroup="Picture Medium" stylename="Picture Medium-SL"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-SL-b" element="paragraph/b" stylegroup="Picture Medium" stylename="Picture Medium-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturemedium-SL-e" element="paragraph/e" stylegroup="Picture Medium" stylename="Picture Medium-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="picturesize-paragraph" element="paragraph" stylegroup="Picture Size" stylename="Picture Size"/>
		<map:entry attr-name="text-type" attr-value="picturesize-bold" element="paragraph/bold" stylegroup="Picture Size" stylename="Picture Size&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-italic" element="paragraph/italic" stylegroup="Picture Size" stylename="Picture Size&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-bold-italic" element="paragraph/bold-italic" stylegroup="Picture Size" stylename="Picture Size&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-sc" element="paragraph/sc" stylegroup="Picture Size" stylename="Picture Size&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-sup" element="paragraph/sup" stylegroup="Picture Size" stylename="Picture Size&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-sub" element="paragraph/sub" stylegroup="Picture Size" stylename="Picture Size&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-p" element="paragraph/i" stylegroup="Picture Size" stylename="Picture Size&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-P" element="paragraph/P" stylegroup="Picture Size" stylename="Picture Size&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-e" element="paragraph/i" stylegroup="Picture Size" stylename="Picture Size&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-E" element="paragraph/E" stylegroup="Picture Size" stylename="Picture Size&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-s" element="paragraph/i" stylegroup="Picture Size" stylename="Picture Size&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-z" element="paragraph/i" stylegroup="Picture Size" stylename="Picture Size&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-paragraph-SL" element="paragraph" stylegroup="Picture Size" stylename="Picture Size-SL"/>
		<map:entry attr-name="text-type" attr-value="picturesize-SL-b" element="paragraph/b" stylegroup="Picture Size" stylename="Picture Size-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="picturesize-SL-e" element="paragraph/e" stylegroup="Picture Size" stylename="Picture Size-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="size-expert" element="paragraph/E" stylegroup="Size" stylename="Size&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-paragraph" element="paragraph" stylegroup="Size" stylename="Size"/>
		<map:entry attr-name="text-type" attr-value="size-b" element="paragraph/b" stylegroup="Size" stylename="Size&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-i" element="paragraph/i" stylegroup="Size" stylename="Size&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-bi" element="paragraph/bi" stylegroup="Size" stylename="Size&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-sc" element="paragraph/sc" stylegroup="Size" stylename="Size&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-sup" element="paragraph/sup" stylegroup="Size" stylename="Size&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-inf" element="paragraph/sub" stylegroup="Size" stylename="Size&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-p" element="paragraph/i" stylegroup="Size" stylename="Size&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-P" element="paragraph/P" stylegroup="Size" stylename="Size&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-e" element="paragraph/i" stylegroup="Size" stylename="Size&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-E" element="paragraph/E" stylegroup="Size" stylename="Size&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-s" element="paragraph/i" stylegroup="Size" stylename="Size&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-z" element="paragraph/i" stylegroup="Size" stylename="Size&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-paragraph-SL" element="paragraph" stylegroup="Size" stylename="Size-SL"/>
		<map:entry attr-name="text-type" attr-value="size-SL-b" element="paragraph/b" stylegroup="Size" stylename="Size-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="size-SL-e" element="paragraph/e" stylegroup="Size" stylename="Size-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="weight-paragraph" element="paragraph" stylegroup="Weight" stylename="Weight"/>
		<map:entry attr-name="text-type" attr-value="weight-bold" element="paragraph/bold" stylegroup="Weight" stylename="Weight&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-italic" element="paragraph/italic" stylegroup="Weight" stylename="Weight&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-bold-italic" element="paragraph/bold-italic" stylegroup="Weight" stylename="Weight&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-sc" element="paragraph/sc" stylegroup="Weight" stylename="Weight&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-sup" element="paragraph/sup" stylegroup="Weight" stylename="Weight&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-sub" element="paragraph/sub" stylegroup="Weight" stylename="Weight&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-p" element="paragraph/i" stylegroup="Weight" stylename="Weight&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-P" element="paragraph/P" stylegroup="Weight" stylename="Weight&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-e" element="paragraph/i" stylegroup="Weight" stylename="Weight&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-E" element="paragraph/E" stylegroup="Weight" stylename="Weight&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-s" element="paragraph/i" stylegroup="Weight" stylename="Weight&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-z" element="paragraph/i" stylegroup="Weight" stylename="Weight&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-paragraph-SL" element="paragraph" stylegroup="Weight" stylename="Weight-SL"/>
		<map:entry attr-name="text-type" attr-value="weight-SL-b" element="paragraph/b" stylegroup="Weight" stylename="Weight-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="weight-SL-e" element="paragraph/e" stylegroup="Weight" stylename="Weight-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="marked-paragraph" element="paragraph" stylegroup="Marked" stylename="Marked"/>
		<map:entry attr-name="text-type" attr-value="marked-bold" element="paragraph/bold" stylegroup="Marked" stylename="Marked&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-italic" element="paragraph/italic" stylegroup="Marked" stylename="Marked&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-bold-italic" element="paragraph/bold-italic" stylegroup="Marked" stylename="Marked&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-sc" element="paragraph/sc" stylegroup="Marked" stylename="Marked&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-sup" element="paragraph/sup" stylegroup="Marked" stylename="Marked&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-sub" element="paragraph/sub" stylegroup="Marked" stylename="Marked&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-p" element="paragraph/i" stylegroup="Marked" stylename="Marked&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-P" element="paragraph/P" stylegroup="Marked" stylename="Marked&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-e" element="paragraph/i" stylegroup="Marked" stylename="Marked&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-E" element="paragraph/E" stylegroup="Marked" stylename="Marked&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-s" element="paragraph/i" stylegroup="Marked" stylename="Marked&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-z" element="paragraph/i" stylegroup="Marked" stylename="Marked&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-paragraph-SL" element="paragraph" stylegroup="Marked" stylename="Marked-SL"/>
		<map:entry attr-name="text-type" attr-value="marked-SL-b" element="paragraph/b" stylegroup="Marked" stylename="Marked-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="marked-SL-e" element="paragraph/e" stylegroup="Marked" stylename="Marked-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="otherdetails-paragraph" element="paragraph" stylegroup="Other Details" stylename="Other Details"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-bold" element="paragraph/bold" stylegroup="Other Details" stylename="Other Details&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-italic" element="paragraph/italic" stylegroup="Other Details" stylename="Other Details&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-bold-italic" element="paragraph/bold-italic" stylegroup="Other Details" stylename="Other Details&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-sc" element="paragraph/sc" stylegroup="Other Details" stylename="Other Details&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-sup" element="paragraph/sup" stylegroup="Other Details" stylename="Other Details&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-sub" element="paragraph/sub" stylegroup="Other Details" stylename="Other Details&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-p" element="paragraph/i" stylegroup="Other Details" stylename="Other Details&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-P" element="paragraph/P" stylegroup="Other Details" stylename="Other Details&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-e" element="paragraph/i" stylegroup="Other Details" stylename="Other Details&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-E" element="paragraph/E" stylegroup="Other Details" stylename="Other Details&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-s" element="paragraph/i" stylegroup="Other Details" stylename="Other Details&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-z" element="paragraph/i" stylegroup="Other Details" stylename="Other Details&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-paragraph-SL" element="paragraph" stylegroup="Other Details" stylename="Other Details-SL"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-SL-b" element="paragraph/b" stylegroup="Other Details" stylename="Other Details-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="otherdetails-SL-e" element="paragraph/e" stylegroup="Other Details" stylename="Other Details-SL&lt;e&gt;"/>

		<map:entry attr-name="text-type" attr-value="provenance-subheadline-1" element="subheadline-1" stylegroup="Notes" stylename="Notes"/>
		<map:entry attr-name="text-type" attr-value="provenance-subheadline-1-SL" element="subheadline-1" stylegroup="Notes" stylename="Notes-SL"/>
		<map:entry attr-name="text-type" attr-value="provenance-paragraph" element="paragraph" stylegroup="Notes Text" stylename="Notes Text"/>
		<map:entry attr-name="text-type" attr-value="provenance-bold" element="paragraph/bold" stylegroup="Notes Text" stylename="Notes Text&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-italic" element="paragraph/italic" stylegroup="Notes Text" stylename="Notes Text&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-bold-italic" element="paragraph/bold-italic" stylegroup="Notes Text" stylename="Notes Text&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-sc" element="paragraph/sc" stylegroup="Notes Text" stylename="Notes Text&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-sup" element="paragraph/sup" stylegroup="Notes Text" stylename="Notes Text&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-sub" element="paragraph/sub" stylegroup="Notes Text" stylename="Notes Text&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-p" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-P" element="paragraph/P" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-e" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-E" element="paragraph/E" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-s" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-z" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-paragraph-SL" element="paragraph" stylegroup="Notes Text" stylename="Notes Text-SL"/>
		<map:entry attr-name="text-type" attr-value="provenance-SL-b" element="paragraph/b" stylegroup="Notes Text" stylename="Notes Text-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="provenance-SL-e" element="paragraph/e" stylegroup="Notes Text" stylename="Notes Text-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="exhibited-subheadline-1" element="subheadline-1" stylegroup="Notes" stylename="Notes"/>
		<map:entry attr-name="text-type" attr-value="exhibited-subheadline-1-SL" element="subheadline-1" stylegroup="Notes" stylename="Notes-SL"/>
		<map:entry attr-name="text-type" attr-value="exhibited-paragraph" element="paragraph" stylegroup="Notes Text" stylename="Notes Text"/>
		<map:entry attr-name="text-type" attr-value="exhibited-bold" element="paragraph/bold" stylegroup="Notes Text" stylename="Notes Text&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-italic" element="paragraph/italic" stylegroup="Notes Text" stylename="Notes Text&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-bold-italic" element="paragraph/bold-italic" stylegroup="Notes Text" stylename="Notes Text&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-sc" element="paragraph/sc" stylegroup="Notes Text" stylename="Notes Text&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-sup" element="paragraph/sup" stylegroup="Notes Text" stylename="Notes Text&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-sub" element="paragraph/sub" stylegroup="Notes Text" stylename="Notes Text&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-p" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-P" element="paragraph/P" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-e" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-E" element="paragraph/E" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-s" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-z" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-paragraph-SL" element="paragraph" stylegroup="Notes Text" stylename="Notes Text-SL"/>
		<map:entry attr-name="text-type" attr-value="exhibited-SL-b" element="paragraph/b" stylegroup="Notes Text" stylename="Notes Text-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="exhibited-SL-e" element="paragraph/e" stylegroup="Notes Text" stylename="Notes Text-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="literature-subheadline-1" element="subheadline-1" stylegroup="Notes" stylename="Notes"/>
		<map:entry attr-name="text-type" attr-value="literature-subheadline-1-SL" element="subheadline-1" stylegroup="Notes" stylename="Notes-SL"/>
		<map:entry attr-name="text-type" attr-value="literature-paragraph" element="paragraph" stylegroup="Notes Text" stylename="Notes Text"/>
		<map:entry attr-name="text-type" attr-value="literature-bold" element="paragraph/bold" stylegroup="Notes Text" stylename="Notes Text&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-italic" element="paragraph/italic" stylegroup="Notes Text" stylename="Notes Text&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-bold-italic" element="paragraph/bold-italic" stylegroup="Notes Text" stylename="Notes Text&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-sc" element="paragraph/sc" stylegroup="Notes Text" stylename="Notes Text&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-sup" element="paragraph/sup" stylegroup="Notes Text" stylename="Notes Text&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-sub" element="paragraph/sub" stylegroup="Notes Text" stylename="Notes Text&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-p" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-P" element="paragraph/P" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-E" element="paragraph/E" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-e" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-s" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-z" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-paragraph-SL" element="paragraph" stylegroup="Notes Text" stylename="Notes Text-SL"/>
		<map:entry attr-name="text-type" attr-value="literature-SL-b" element="paragraph/b" stylegroup="Notes Text" stylename="Notes Text-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="literature-SL-e" element="paragraph/e" stylegroup="Notes Text" stylename="Notes Text-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="engraved-subheadline-1" element="subheadline-1" stylegroup="Notes" stylename="Notes"/>
		<map:entry attr-name="text-type" attr-value="engraved-subheadline-1-SL" element="subheadline-1" stylegroup="Notes" stylename="Notes-SL"/>
		<map:entry attr-name="text-type" attr-value="engraved-paragraph" element="paragraph" stylegroup="Notes Text" stylename="Notes Text"/>
		<map:entry attr-name="text-type" attr-value="engraved-bold" element="paragraph/bold" stylegroup="Notes Text" stylename="Notes Text&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-italic" element="paragraph/italic" stylegroup="Notes Text" stylename="Notes Text&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-bold-italic" element="paragraph/bold-italic" stylegroup="Notes Text" stylename="Notes Text&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-sc" element="paragraph/sc" stylegroup="Notes Text" stylename="Notes Text&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-sup" element="paragraph/sup" stylegroup="Notes Text" stylename="Notes Text&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-sub" element="paragraph/sub" stylegroup="Notes Text" stylename="Notes Text&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-p" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-P" element="paragraph/P" stylegroup="Notes Text" stylename="Notes Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-e" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-E" element="paragraph/E" stylegroup="Notes Text" stylename="Notes Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-s" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-z" element="paragraph/i" stylegroup="Notes Text" stylename="Notes Text&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-paragraph-SL" element="paragraph" stylegroup="Notes Text" stylename="Notes Text-SL"/>
		<map:entry attr-name="text-type" attr-value="engraved-SL-b" element="paragraph/b" stylegroup="Notes Text" stylename="Notes Text-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="engraved-SL-e" element="paragraph/e" stylegroup="Notes Text" stylename="Notes Text-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="cataloguenotes-paragraph" element="paragraph" stylegroup="Catalogue Notes" stylename="Catalogue Notes"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-bold" element="paragraph/bold" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-italic" element="paragraph/italic" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-bold-italic" element="paragraph/bold-italic" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-sc" element="paragraph/sc" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-sup" element="paragraph/sup" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-sub" element="paragraph/sub" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-p" element="paragraph/i" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-P" element="paragraph/P" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-e" element="paragraph/i" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-E" element="paragraph/E" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-s" element="paragraph/i" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-z" element="paragraph/i" stylegroup="Catalogue Notes" stylename="Catalogue Notes&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-paragraph-SL" element="paragraph" stylegroup="Catalogue Notes" stylename="Catalogue Notes-SL"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-SL-b" element="paragraph/b" stylegroup="Catalogue Notes" stylename="Catalogue Notes-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="cataloguenotes-SL-e" element="paragraph/e" stylegroup="Catalogue Notes" stylename="Catalogue Notes-SL&lt;e&gt;"/>
		
		<map:entry attr-name="text-type" attr-value="postlot-paragraph" element="paragraph" stylegroup="Post Lot Text" stylename="Post Lot Text"/>
		<map:entry attr-name="text-type" attr-value="postlot-bold" element="paragraph/bold" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-italic" element="paragraph/italic" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;i&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-bold-italic" element="paragraph/bold-italic" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;bi&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-sc" element="paragraph/sc" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;sc&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-sup" element="paragraph/sup" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;sup&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-sub" element="paragraph/sub" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;sub&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-p" element="paragraph/i" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-P" element="paragraph/P" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;p&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-e" element="paragraph/i" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-E" element="paragraph/E" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;e&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-s" element="paragraph/i" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;s&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-z" element="paragraph/i" stylegroup="Post Lot Text" stylename="Post Lot Text&lt;z&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-paragraph-SL" element="paragraph" stylegroup="Post Lot Text" stylename="Post Lot Text-SL"/>
		<map:entry attr-name="text-type" attr-value="postlot-SL-b" element="paragraph/b" stylegroup="Post Lot Text" stylename="Post Lot Text-SL&lt;b&gt;"/>
		<map:entry attr-name="text-type" attr-value="postlot-SL-e" element="paragraph/e" stylegroup="Post Lot Text" stylename="Post Lot Text-SL&lt;e&gt;"/>
		
		<map:entry element="lot/number" stylegroup="Lot Number" stylename="Lot Number"/>
		<map:entry element="lot/symbol" stylegroup="Lot Number" stylename="Lot Number"/>
		<map:entry element="lot-number-only/number" stylegroup="Lot Number" stylename="Number"/>
		<map:entry element="lot/symbol/P" stylegroup="Lot Number" stylename="Lot Number&lt;p&gt;"/>
		<map:entry element="lot/symbol/p" stylegroup="Lot Number" stylename="Lot Number&lt;p&gt;"/>
		<map:entry element="lot/symbol/E" stylegroup="Lot Number" stylename="Lot Number&lt;e&gt;"/>
		<map:entry element="lot/symbol/e" stylegroup="Lot Number" stylename="Lot Number&lt;e&gt;"/>
		<map:entry element="lot/symbol/S" stylegroup="Lot Number" stylename="Lot Number&lt;s&gt;"/>
		<map:entry element="lot/symbol/s" stylegroup="Lot Number" stylename="Lot Number&lt;s&gt;"/>
		<map:entry element="lot/symbol/Z" stylegroup="Lot Number" stylename="Lot Number&lt;z&gt;"/>
		<map:entry element="lot/symbol/z" stylegroup="Lot Number" stylename="Lot Number&lt;z&gt;"/>
		<map:entry element="lot/suffix" stylegroup="Lot Number" stylename="Lot Number"/>
		<map:entry element="lot-number-only/suffix" stylegroup="Lot Number" stylename="Number"/>
	</xsl:variable>

	<xd:doc>
		<xd:desc>
			<xd:p>Mapping of element names to RexExp replacemnts</xd:p>
			<xd:p>
				<xd:i>If not used, include some empty element</xd:i>
			</xd:p>
			<xd:p>Unlike <xd:ref name="PSMapping" type="variable"/> and <xd:ref name="CSMapping" type="variable"/> children are also matching, so replacements are applied to all subelements, which don’t match another entry</xd:p>
			<xd:p>Every entry consists of an <xd:b>&lt;cen-map-entry&gt;</xd:b> element with following attributes:</xd:p>
			<xd:ul>
				<xd:li><xd:b>element</xd:b>: name of the element, or part of hierarchy in the form: ancestor/parent/child</xd:li>
				<xd:li><xd:b>search</xd:b>: RegEx for search</xd:li>
				<xd:li><xd:b>replace</xd:b>: RegEx for replacement</xd:li>
				<xd:li><xd:b>flags</xd:b>: optional RegEx flags (f.e.: "i" for case-insensitive search)</xd:li>
			</xd:ul>
			<xd:p><xd:b>IMPORTANT:</xd:b> mask { and } as {{ and }}</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:variable name="Replacements">
		<map:entry/>
	</xsl:variable>



	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
	<!-- end of format mappings       -->
	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

</xsl:stylesheet>
