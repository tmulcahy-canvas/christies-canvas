function controller($scope, csApiSession) {
  // set xslt options and mode = asset!
  $scope.xsltOptions = {
    mappingFile: 'modules/ioi/christies/feature_to_text_mapping/mapping.xml',
    mode: 'asset'
  };

  // default languages if there are none.
  // if($scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata === undefined) {
  //   $scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata = {
  //     cmd : {
  //       settings: {
  //         language: [{value: 'en'}, {value: 'CT'}]
  //       }
  //     }
  //   };
  // }
  
  // if($scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd === undefined) {
  //   $scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd = {
  //     settings: {
  //       language: [{value: 'en'}, {value: 'de'}]
  //     }
  //   };
  // }

  console.log($scope.asset.traits.canvas.userSelectedProperties);
  
  if($scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata === undefined) {
    $scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata = {
      cmd: {
        settings: {}
      }
    };
  }
  console.log($scope.asset.traits.canvas.userSelectedProperties);

  if($scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.language === undefined) {
    $scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.language = [{value: 'en'}, {value: 'CT'}];
  }
  console.log($scope.asset.traits.canvas.userSelectedProperties);
  
  // null out value so server recognizes a change.
  $scope.asset.traits.canvas.userSelectedProperties.value[0].value = '';
  
  // instantiate languages array on scope
  $scope.languages = [{language:"CS", languageName:"Chinese Simplified"}, {language:"CT", languageName:"Chinese Traditional"}, {language:"en", languageName:"English"}];
  $scope.assetId = $scope.asset.traits.ids.id.value[0].value;


  
  // populate languages from cachetable
  // csApiSession.masterdata.lookup({
  //     lookup: [
  //         {
  //             table: 'language_def'
  //         }
  //     ]
  // }).then(function(result) {
  //     if (result && result.records && result.records.record) {
  //         result.records.record.forEach(function(record) {
  //             $scope.languages.push({ language: record.id, languageName: record.name});
  //         });
  //     }
  // });
  // var transformationResource = "ioi:canvas.grab-languages-widget";
  // csApiSession.transformation(transformationResource, {assetid: $scope.asset.traits.ids.id.value[0]}).then(function (result) {
  //     console.log('#### TRANS_RESULT', result);
  // });

  // var params = {};
  // if ($scope.assetId) {
  //     params.contextAsset = $scope.assetId;
  // }

  // console.log(params);
  // console.log('#### CALL TRANS', transformationResource, params);
  // csApiSession.transformation(transformationResource, params).then(function (result) {
      // console.log('#### TRANS_RESULT', result);


      // console.log('IS ARRAY' , angular.isArray(result.thelangs))
       // populate languages from values from grab-languages xslt
  //       if (angular.isArray(result.thelangs))
  //       {   
  //           result.thelangs.forEach(function(thelangs){
  //               $scope.languages.push({language: thelangs.key, languageName: thelangs.name});
  //           });
  //       } else
  //       {   
  //           $scope.languages.push({language: result.thelangs.key, languageName: result.thelangs.name});
  //           // console.log($scope.languages);
  //       }

  // });


  // populate languages from values from grab-languages xslt
  // if (angular.isArray(langObject))
  // {   
  //     langObject.forEach(function(thelangs){
  //         $scope.languages.push({language: thelangs.key, languageName: thelangs.name});

  //     });
  // } else
  // {   
  //     $scope.languages.push({language: $scope.cmd.langs.data.thelangs.key, languageName: $scope.cmd.langs.data.thelangs.name});
  //     console.log($scope.languages);
  // }
  
  $scope.logScope = function() {
    console.log($scope);
  };
}