<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet exclude-result-prefixes="xs html censhare cs io ioi" version="2.0" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:ioi="http://www.iointegration.com" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- ioi:JR - Creates an asset reference for all product asset types to the master product category - ioi:JR -->
	<xsl:output encoding="utf-8" indent="yes" method="xml" version="1.0"/>
	<xsl:param name="censhare:command"/>
	<xsl:template match="asset">
		<xsl:variable name="all-product-category" select="cs:asset()[@censhare:resource-key = 'ioi:product-category-relation-all']/@id"/>
		<xsl:choose>
			<xsl:when test="exists(./asset_feature[@feature='censhare:product.category'])">
						<xsl:message>THIS ASSET IS CHECKED OUT PLEASE CHECK BACK IN AND RUN ACTION AGAIN</xsl:message>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="./@checked_out_by">
						<xsl:message>THIS ASSET IS CHECKED OUT PLEASE CHECK BACK IN AND RUN ACTION AGAIN</xsl:message>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="checkedOut" select="cs:checkout(.)" />
						<xsl:copy-of select="$checkedOut" />
						<xsl:variable name="asset-to-update">
							<asset>
								<xsl:copy-of select="$checkedOut/@*"/>
								<xsl:copy-of select="$checkedOut/node()"/>
								<asset_feature feature="censhare:product.category" value_asset_id="{$all-product-category}"/>
							</asset>
						</xsl:variable>
						<xsl:copy-of select="cs:checkin($asset-to-update)"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:function name="cs:checkin">
		<xsl:param name="asset-xml"/>
		<cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="asset">
			<cs:param name="source" select="$asset-xml"/>
		</cs:command>
		<xsl:copy-of select="$asset"/>
	</xsl:function>
	<xsl:function name="cs:checkout">
		<xsl:param name="asset-xml"/>
		<cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="asset">
			<cs:param name="source" select="$asset-xml"/>
		</cs:command>
		<xsl:copy-of select="$asset"/>
	</xsl:function>
</xsl:stylesheet>