<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ioi="http://www.iointegration.com" xmlns:xe="http://www.censhare.com/xml/3.0.0/xmleditor">  
	
	<xsl:output method="xml" version="1.0" encoding="utf-8" indent="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:param name="system" />
  	<xsl:param name="censhare:command-xml" />
  	<xsl:param name="partID" />
  	<xsl:param name="mappingFile"/>
  	<xsl:param name="clientType"/>
 
  	<xsl:variable name="log-prefix" select="'   ###   camtest: '"/>
	<!-- cc - key is used in the process property text feature data to transfer the chosen data to the backend.  We translate this key to remove
              characters from the string to provide a propery key to the model.  Key needs to be changed in both places for matching
              routine.
    - cc -->
    <xsl:variable name="translate-from-key" select="':.-_ '"/>
	
	<xsl:template match="/">

		<xsl:variable name="config"/>
		<cs:command name="com.censhare.api.io.GetFilesystemRef" returning="config">
			<cs:param name="name" select="'config-runtime'" />
		</cs:command>

		<xsl:variable name="content"/>
		<cs:command name="com.censhare.api.io.ReadXML" returning="content">
			<cs:param name="source" select="concat($config, $mappingFile)"/>
		</cs:command>
	
		<xe:part id="{$partID}" transactional="true">
			<xe:group align="down">
				<xe:label value="Select data to display on PDF." style="label-title" weight-x="1"/>
				<xsl:for-each-group select="$content/config/*/mappings/mapping[@user-selected='1']" group-by="@key">    					
					<xsl:variable name="default-val" select="if(exists(./@is-default) and ./@is-default = '1') then 1 else 0"/>
					<xsl:variable name="label" select="./@label" />
					<xsl:choose>
						<xsl:when test="exists(./@is-default) and ./@is-default = '1'">
							<xe:checkbox label="{$label}" label-style="label-default" source="{concat('settings.excludes.@', translate($label, $translate-from-key, ''))}" checked-value="true" unchecked-value="false" defaultvalue="true"/>
						</xsl:when>
						<xsl:otherwise>
							<xe:checkbox label="{$label}" label-style="label-default" source="{concat('settings.excludes.@', translate($label, $translate-from-key, ''))}" checked-value="true" unchecked-value="false"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each-group>
			</xe:group>
		</xe:part>
	</xsl:template>

</xsl:stylesheet>
