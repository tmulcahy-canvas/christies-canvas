<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:canvas="censhare.com"
    xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/ObjectDataRequest"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
		xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
		xmlns:csc="http://www.censhare.com/censhare-custom"
		xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
		exclude-result-prefixes="#all">

	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvasmetadatavalidations/storage/master/file"/>

	<xsl:output indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<!-- =========================== -->
	<!-- Function for date/time data type validation  -->
	<!-- =========================== -->
	<xsl:function name="canvas:dateTimeDataTypeValidate">
		<xsl:param name="dateString" as="node()"/>
		<xsl:choose>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{4})\.(\d{2})\.(\d{2})$')">
<!--				<xsl:value-of
					select="concat(substring($dateString, 1, 4), '-', substring($dateString, 6, 2), '-', concat(substring($dateString, 9, 2), 'T00:00:00Z'))"
				/>-->
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{4})/(\d{2})/(\d{2})$')">
<!--				<xsl:value-of
					select="concat(substring($dateString, 1, 4), '-', substring($dateString, 6, 2), '-', substring($dateString, 9, 2), 'T00:00:00Z')"
				/>-->
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{8})$')">
<!--				<xsl:value-of
					select="concat(substring($dateString, 1, 4), '-', substring($dateString, 5, 2), '-', substring($dateString, 7, 2), 'T00:00:00Z')"
				/>-->
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{6})$')">
<!--				<xsl:value-of
					select="concat('20', substring($dateString, 1, 2), '-', substring($dateString, 5, 2), '-', substring($dateString, 3, 2), 'T00:00:00Z')"
				/>-->
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{5})$')">
<!--				<xsl:value-of
					select="concat('0000-00-00T', 0, substring($dateString, 1, 1), ':', substring($dateString, 2, 2), ':', substring($dateString, 4, 2), 'Z')"
				/>-->
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{4})$')">
				<!--<xsl:value-of
					select="concat('0000-00-00T', substring($dateString, 1, 2), ':', substring($dateString, 3, 2), ':00Z')"
				/>-->
			</xsl:when>
			<xsl:otherwise>
				<errorElements id="E004"><xsl:value-of select="local-name($dateString)"/></errorElements>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

	<xsl:function name="canvas:initialCheckings">
		<xsl:param name="currentItemXML"/>
		<xsl:param name="mappingElementstoFeatures"/>

		<!--xsl:for-each select="$currentItemXML/* except ($currentItemXML/szUniqueKeyIDString_UKIDSZ)"-->
		<xsl:for-each select="$currentItemXML/* except ($currentItemXML/szUniqueKeyIDString_UKIDSZ,$currentItemXML/Symbol,$currentItemXML/SymbolCode,$currentItemXML/SymbolDescription,$currentItemXML/SymbolConverted)">
			<xsl:variable name="currentElementName" select="local-name(.)"/>
			<xsl:variable name="confMappingFeature" select="$mappingElementstoFeatures//*[local-name()=$currentElementName]" />

			<!--===================================-->
			<!--Check if element is appearing more than one time-->
			<!--===================================-->
			<xsl:if test="count($currentItemXML/*[local-name() = $currentElementName]) &gt; 1">
				<xsl:text>&#10;</xsl:text>
				<errorElements id="E001"><xsl:value-of select="$currentElementName"/></errorElements>
			</xsl:if>

			<!--===================================-->
			<!--Check if elements "SaleDateFrom" is greater than "SaleDateTo"-->
			<!--===================================-->
			<xsl:if
				test="local-name() = 'SaleDateFrom' and ((following-sibling::SaleDateTo) or (preceding-sibling::SaleDateTo)) and (canvas:dateFormatChange(.) &gt; canvas:dateFormatChange(../SaleDateTo))">
				<xsl:text>&#10;</xsl:text>
				<errorElements id="E002"><xsl:value-of select="$currentElementName"/></errorElements>
			</xsl:if>
			<xsl:choose>
				<xsl:when test="$confMappingFeature">
					<xsl:choose>
						<!--===================================-->
						<!--Checking elementswith valueType 'value_timestamp' & value_timestamp2-->
						<!--===================================-->
						<xsl:when test="normalize-space(.) and (($confMappingFeature/valueType = 'value_timestamp') or ($confMappingFeature/valueType = 'value_timestamp2'))">
							<xsl:copy-of select="canvas:dateTimeDataTypeValidate(.)"/>
						</xsl:when>

						<!--===================================-->
						<!--Checking elementswith valueType 'value_key'-->
						<!--===================================-->
						<xsl:when test="normalize-space(.) and ($confMappingFeature/valueType = 'value_key')">
							<xsl:variable name="featureID" select="normalize-space($confMappingFeature/id)" />
							<xsl:variable name="currentFeatureValue" select="normalize-space(.)" />
							<xsl:if test="not($currentElementName = 'SymbolCode')">
								<xsl:if test="not(cs:master-data('feature_value')[@feature=$featureID and @value_key=$currentFeatureValue])">
									<xsl:text>&#10;</xsl:text>
									<errorElements id="E005"><xsl:value-of select="$currentElementName"/></errorElements>
								</xsl:if>
							</xsl:if>
						</xsl:when>

						<!--===================================-->
						<!--Checking elementswith valueType 'value_long'-->
						<!--===================================-->
						<xsl:when test="normalize-space(.) and ($confMappingFeature/valueType = 'value_long')">
							<xsl:if test="not(matches(., '^[0-9\.,]+$'))">
								<xsl:text>&#10;</xsl:text>
								<errorElements id="E004"><xsl:value-of select="$currentElementName"/></errorElements>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise/>
					</xsl:choose>
				</xsl:when>

				<!--===================================-->
				<!--Checking and throwing the error if element is not available in static xml -->
				<!--===================================-->
				<xsl:when test="not($confMappingFeature)">
					<xsl:text>&#10;</xsl:text>
					<errorElements id="E003"><xsl:value-of select="$currentElementName"/></errorElements>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:function>

	
</xsl:stylesheet>
