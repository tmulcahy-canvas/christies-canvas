function controller($scope, $compile, csApiSession, commandData) {

    if (commandData && commandData.cmd) {
        $scope.xsltOptions = {
            mappingFile: commandData.cmd.ui_settings.path
        };
        // the cmd property should always be on the scope
        $scope.cmd = commandData.cmd;

        // var langObject = $scope.cmd.langs.data.thelangs;
        $scope.language = [{language:"CS", languageName:"Chinese Simplified"}, {language:"CT", languageName:"Chinese Traditional"}, {language:"en", languageName:"English"}];

        // console.log(commandData.cmd.ui_settings.path);
        // console.log(commandData.cmd);
        // console.log($scope);
        
      
        // $scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd = {
        //   settings: {
        //     language: [{value: 'en'}, {value: 'en'}]
        //   }
        // };
        // console.log("It Ran?");
        
        // csApiSession.masterdata.lookup({
        //     lookup: [
        //         {
        //             table: 'language_def'
        //         }
        //     ]
        // }).then(function(result) {
        //     if (result && result.records && result.records.record) {
        //         result.records.record.forEach(function(record) {
        //             $scope.languages.push({ language: record.id, languageName: record.name});
        //         });
        //     }
        // });

        // if (angular.isArray(langObject))
        // {   
        //     langObject.forEach(function(thelangs){
        //         $scope.languages.push({language: thelangs.key, languageName: thelangs.name});

        //     });
        // } else
        // {   
        //     $scope.languages.push({language: $scope.cmd.langs.data.thelangs.key, languageName: $scope.cmd.langs.data.thelangs.name});
        //     console.log($scope.languages);
        // }
    }
}
