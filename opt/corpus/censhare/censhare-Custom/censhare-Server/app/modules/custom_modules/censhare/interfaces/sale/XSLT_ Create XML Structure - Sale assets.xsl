<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io"
    xmlns:corpus="http://www.censhare.com/corpus" exclude-result-prefixes="#all">
    
    <xsl:output method="xml" indent="yes" />
    <xsl:strip-space elements="*" />
    
    <!--canvas:jdesale.auctioneer
    canvas:jdesale.costcentercode
    canvas:jdesale.salecurrencycode
    canvas:jdesale.saledate
    canvas:jdesale.saledepartment
    canvas:jdesale.salelocation
    canvas:jdesale.multidepartment
    canvas:jdesale.salenumber (External ID)
    canvas:jdesale.numberofsessions
    canvas:jdesale.salestarttime
    canvas:jdesale.salesite
    canvas:jdesale.salestatus
    canvas:jdesale.saletitle (Asset name)
    canvas:jdesale.saletype
    canvas:jdesale.winesale
    canvas.jdesale.uniqueidentifier-->
    
    <xsl:variable name="VERBOSE" select="true()" />
    
    <xsl:template match="/ns0:CANVASSaleHeaderDataRequest">
        <xsl:variable name="assets">
            <xsl:apply-templates select="Item"/>
        </xsl:variable>
        <assets>
            <xsl:copy-of select="$assets/asset"/>
        </assets>
    </xsl:template>    
    
    <xsl:template match="Item">
    	<!-- added on 21 May 2018 #3873464 -->
        <xsl:variable name="sale-asset-name" select="concat(normalize-space(SaleSite),normalize-space(SaleNumber))"/>
        <!-- END -->
        <!-- 21-06-18 -->
        <!--<xsl:variable name="id-extern" select="SaleNumber"/>-->
        <!-- 21-06-18 -->
        <xsl:variable name="id-extern" select="concat('sale:',SaleNumber)"/>
        
        <xsl:variable name="asset-exist" select="cs:asset('sql=true')[@censhare:asset.id_extern = $id-extern]"/>
        
        <xsl:variable name="salestatus-feature-value" select="normalize-space(SaleStatus/text())"/>
        
        <xsl:variable name="salestatus-feature" select="cs:master-data('feature')[@key = 'canvas:jdesale.salestatus']"/>
        
        <xsl:variable name="salestatus-feature-value-key" select="cs:master-data('feature_value')[@feature = 'canvas:jdesale.salestatus' and @value_key = $salestatus-feature-value]"/>
        
        <!-- 1 dec 2017 -->
        <xsl:variable name="costcentercode-feature-value" select="normalize-space(CostCenterCode/text())"/>
        
        <xsl:variable name="costcentercode-feature" select="cs:master-data('feature')[@key = 'canvas:jdesale.costcentercode']"/>
        
        <xsl:variable name="costcentercode-feature-value-key" select="cs:master-data('feature_value')[@feature = 'canvas:jdesale.costcentercode' and @value_key = $costcentercode-feature-value]"/>
        <!-- end -->
        
        <xsl:variable name="masterdataXml">
            <xsl:if test="empty($salestatus-feature-value-key) and $salestatus-feature-value !=''">
                <feature>
                    <xsl:copy-of select="$salestatus-feature/@*"/>
                    <feature_value feature="canvas:jdesale.salestatus" domain="root." domain2="root." is_hierarchical="0" value_key="{$salestatus-feature-value}" name="{$salestatus-feature-value}" name_de="{$salestatus-feature-value}" sorting="0" enabled="1"/>
                </feature>
            </xsl:if> 
            
            <!-- 1 dec 2017 -->
            <xsl:if test="empty($costcentercode-feature-value-key) and $costcentercode-feature-value !=''">
                <feature>
                    <xsl:copy-of select="$costcentercode-feature/@*"/>
                    <feature_value feature="canvas:jdesale.costcentercode" domain="root." domain2="root." is_hierarchical="0" value_key="{$costcentercode-feature-value}" name="{$costcentercode-feature-value}" name_de="{$costcentercode-feature-value}" sorting="0" enabled="1"/>
                </feature>
            </xsl:if>
            <!-- end -->
            
        </xsl:variable>
        
        <!-- Create master-data -->
        <xsl:variable name="command-xml">
            <cmd>
                <xml-info title="my-create-master-data" locale="__ALL" />
                <cmd-info name="admin.export_import.xml-data-import" />
                <commands currentstep="0">
                    <command method="importXml" scriptlet="modules.admin.export_import.XmlDataImport" target="ScriptletManager" />
                </commands>
                <content>
                    <xsl:copy-of select="$masterdataXml" />
                </content>
                <param to-do="both" />
            </cmd>
        </xsl:variable>
        
        <cs:command name="com.censhare.api.ExecuteCommand.execute">
            <cs:param name="command" select="$command-xml" />
        </cs:command>
        
        <asset name="{$sale-asset-name}" id_extern="{$id-extern}" type="sale." domain="root.christiesinternational.public." domain2="root.christiesinternational.emeri." application="default" modified_date="{current-dateTime()}">
            <xsl:if test="normalize-space(UniqueIdentifier) != ''">
                <feature key="canvas:jdesale.uniqueidentifier" value="{normalize-space(UniqueIdentifier)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(AuctioneerDesc) != ''">
                <feature key="canvas:jdesale.auctioneer" value="{normalize-space(AuctioneerDesc)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(CostCenterCode) != ''">
                <feature key="canvas:jdesale.costcentercode" value="{normalize-space(CostCenterCode)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(SaleCurrencyCode) != ''">
                <feature key="canvas:jdesale.salecurrencycode" value="{normalize-space(SaleCurrencyCode)}"/>    
            </xsl:if>
            <xsl:if test="(normalize-space(SaleDateFrom) !='') and (normalize-space(SaleDateTo) !='')">
                <feature key="canvas:jdesale.saledate" value="{normalize-space(SaleDateFrom)}" value2="{normalize-space(SaleDateTo)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(SaleDepartment) != ''">
                <feature key="canvas:jdesale.saledepartment" value="{normalize-space(SaleDepartment)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(Location) !=''">
                <feature key="canvas:jdesale.salelocation" value="{normalize-space(Location)}"/>
            </xsl:if>
            <xsl:if test="normalize-space(MultiDepartment) != ''">
                <feature key="canvas:jdesale.multidepartment" value="{normalize-space(MultiDepartment)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(SaleNumber) != ''">
                <feature key="canvas:jdesale.salenumber" value="{normalize-space(SaleNumber)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(NumberOfSessions) !=''">
                <feature key="canvas:jdesale.numberofsessions" value="{normalize-space(NumberOfSessions)}"/>    
            </xsl:if>
            <!-- This feature has been commented on 15 Dec 2017 by atk, as per ticket ID 3647423 -->
            <!--<xsl:if test="normalize-space(SaleStartTime) != ''">
                <feature key="canvas:jdesale.salestarttime" value="{normalize-space(SaleStartTime)}"/>    
            </xsl:if>-->
            <xsl:if test="normalize-space(SaleSite) != ''">
                <feature key="canvas:jdesale.salesite" value="{normalize-space(SaleSite)}"/>    
            </xsl:if> 
            <xsl:if test="normalize-space(SaleTitle) !=''">
                <feature key="canvas:jdesale.saletitle" value="{normalize-space(SaleTitle)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(SaleType) != ''">
                <feature key="canvas:jdesale.saletype" value="{normalize-space(SaleType)}"/>    
            </xsl:if>
            <xsl:if test="normalize-space(WineSale) != ''">
                <feature key="canvas:jdesale.winesale" value="{normalize-space(WineSale)}"/>    
            </xsl:if>
            <xsl:if test="$salestatus-feature-value !=''">
                <feature key="canvas:jdesale.salestatus" value="{$salestatus-feature-value}"/>    
            </xsl:if>
            <xsl:if test="not(empty($asset-exist))">
                <xsl:if test="not(exists(SaleStatus))">
                    <xsl:copy-of select="$asset-exist//asset_feature[@feature='canvas:jdesale.salestatus']"/>
                </xsl:if>
            </xsl:if>
        </asset>
    </xsl:template>    
    
</xsl:stylesheet>