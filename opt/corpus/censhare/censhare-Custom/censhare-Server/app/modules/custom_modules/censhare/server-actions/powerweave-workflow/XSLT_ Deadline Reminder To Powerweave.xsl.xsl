<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:func="http://ns.censhare.de/functions"
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
    version="2.0">
    <!-- day light saving start and end date getting from the asset file-->
    <xsl:variable name="getDaylightsavingURL" select="cs:asset()[@censhare:resource-key='christie:daylightsaving']/storage_item[@key='master' and @mimetype='text/xml']/@url"/>
    <xsl:variable name="DaylightSavingStartEndDate">
        <xsl:copy-of select="doc($getDaylightsavingURL)"/>
    </xsl:variable>
    <xsl:function name="func:DaylightSavingBST">
        <xsl:param name="deadline_actual_date"/>
        <xsl:for-each select="$DaylightSavingStartEndDate/Daylightsaving/BST/date">
            <xsl:variable name="start-date" select="@start-date"/>
            <xsl:variable name="end-date" select="@end-date"/>
            <xsl:if test="xs:dateTime($start-date) &lt;= xs:dateTime($deadline_actual_date) and xs:dateTime($end-date) &gt;= xs:dateTime($deadline_actual_date)">
                <xsl:text>available</xsl:text>
            </xsl:if>
        </xsl:for-each>
    </xsl:function>
    <xsl:function name="func:DaylightSavingEDT">
        <xsl:param name="deadline_actual_date"/>
        <xsl:for-each select="$DaylightSavingStartEndDate/Daylightsaving/EDT/date">
            <xsl:variable name="start-date" select="@start-date"/>
            <xsl:variable name="end-date" select="@end-date"/>
            <xsl:if test="xs:dateTime($start-date) &lt;= xs:dateTime($deadline_actual_date) and xs:dateTime($end-date) &gt;= xs:dateTime($deadline_actual_date)">
                <xsl:text>available</xsl:text>
            </xsl:if>
        </xsl:for-each>
    </xsl:function>
    <xsl:template match="asset[starts-with(./@type, 'layout.')]">
        <xsl:variable name="layout-asset" select="."/>
        <xsl:variable name="assignee" select="cs:master-data('party')[@id = $layout-asset/@wf_target]"/>
        <xsl:message>TOPOWERWAVE ----</xsl:message>
        <xsl:choose>
            <!-- Empfänger ist eine einzelne Person -->
            <xsl:when test="$assignee/@isgroup = 0">
                <xsl:variable name="assignee-email" select="$assignee/@email"/>
                <xsl:if test="not(empty($assignee-email))">
                    <xsl:call-template name="send-email">
                        <xsl:with-param name="assignee-email" select="$assignee-email"/>
                        <xsl:with-param name="layout-asset" select="$layout-asset"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:when>
            <!-- Empfänger ist eine Benutzergruppe -->
            <xsl:otherwise>
                <xsl:variable name="assignee-emails" select="for $x in cs:master-data('party_rel')[@parent_id = $assignee/@id]/@child_id return cs:master-data('party')[@id = $x]/@email"/>
                <xsl:for-each select="$assignee-emails">
                    <xsl:variable name="assignee-email" select="."/>
                    <xsl:if test="not(empty($assignee-email))">
                        <xsl:call-template name="send-email">
                            <xsl:with-param name="assignee-email" select="$assignee-email"/>
                            <xsl:with-param name="layout-asset" select="$layout-asset"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="send-email">
        <xsl:param name="assignee-email"/>
        <xsl:param name="layout-asset"/>
        
        <xsl:variable name="accountName" select="'christies'"/>
        <xsl:variable name="workflow-target" select="if ($layout-asset/@wf_target) then (cs:master-data('party')[@id =$layout-asset/@wf_target]/@display_name) else ()"/>
        <xsl:variable name="modified-by" select="if ($layout-asset/@modified_by) then (cs:master-data('party')[@id = $layout-asset/@modified_by]/@display_name) else ()"/>
        <xsl:variable name="subject" select="$layout-asset/@name"/>
        <xsl:variable name="source">
            <mails account-name="{$accountName}">
                <mail subject="{$subject}" sender-address="" replyto-address="">
                    <recipient type="to" address="{$assignee-email}"/>
                    <multipart-body>
                        <content mimetype="text/html" transfer-charset="UTF-8">
                            <html>
                                <body>
                                    <b><xsl:value-of select="concat('A task has been allocated: ', $layout-asset/@name)"/></b>
                                    <br/>
                                    <xsl:value-of select="concat('Allocated by: ', $modified-by)"/>
                                    <br/>
                                    <xsl:value-of select="concat('Allocated to: ', $workflow-target)"/>
                                    <br/>
                                    <!--<xsl:value-of select="concat('Required back by: ', substring(string(xs:date($layout-asset/@deadline_actual)) , 9, 2), '/', substring(string(xs:date($layout-asset/@deadline_actual)) , 6, 2), '/', substring(string(xs:date($layout-asset/@deadline_actual)) , 1, 4), ', ', (if (xs:integer(substring(string($layout-asset/@deadline_actual), 12, 2)) &gt; 12) then concat(xs:integer(substring(string($layout-asset/@deadline_actual), 12, 2)) -12, ':', (substring(string($layout-asset/@deadline_actual), 15, 2)), ' PM') else concat(xs:integer(substring(string($layout-asset/@deadline_actual), 12, 2)), ':', (substring(string($layout-asset/@deadline_actual), 15, 2)), ' AM')), ' GMT')"/>-->
                                    <!--start for tieckt id #3333860-->
                                    <xsl:variable name="deadline_actual" as="xs:dateTime" select="$layout-asset/@deadline_actual"/>
                                    <xsl:variable name="ist-timestamp" select="xs:dateTime($deadline_actual) + (xs:dayTimeDuration('PT5H30M'))"/>
                                    <xsl:variable name="it" select="format-dateTime(xs:dateTime($ist-timestamp), '[D01]/[M01]/[Y0001] [h01]:[m01] [PN, 2-2]')"/>
                                    <xsl:variable name="isDaylightSavingBST" select="func:DaylightSavingBST($deadline_actual)"/>
                                    <xsl:variable name="isDaylightSavingEDT" select="func:DaylightSavingEDT($deadline_actual)"/>
                                    <xsl:text>Required back by: </xsl:text><br/>
                                    <xsl:choose>
                                        <!--British Summer Time -->
                                        <xsl:when test="contains($isDaylightSavingBST, 'available')">
                                            <xsl:variable name="bst-timestamp" select="xs:dateTime($deadline_actual) + (xs:dayTimeDuration('PT1H'))"/>
                                            <xsl:variable name="bst" select="format-dateTime(xs:dateTime($bst-timestamp), '[D01]/[M01]/[Y0001] [h01]:[m01] [PN, 2-2]')"/>
                                            <xsl:value-of select="$bst"/> (GMST/BST)<br/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:variable name="bst-timestamp" select="xs:dateTime($deadline_actual)"/>
                                            <xsl:variable name="bst" select="format-dateTime(xs:dateTime($bst-timestamp), '[D01]/[M01]/[Y0001] [h01]:[m01] [PN, 2-2]')"/>
                                            <xsl:value-of select="$bst"/> (GMT/BT)<br/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <!--Eastern Daylight Time--> 
                                    <xsl:choose>
                                        <xsl:when test="contains($isDaylightSavingEDT, 'available')">
                                            <!--Summer-->
                                            <xsl:variable name="est-timestamp" select="xs:dateTime($deadline_actual) + (xs:dayTimeDuration('-PT4H'))"/>
                                            <xsl:variable name="est" select="format-dateTime(xs:dateTime($est-timestamp), '[D01]/[M01]/[Y0001] [h01]:[m01] [PN, 2-2]')"/>
                                            <xsl:value-of select="$est"/> (EDT)<br/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <!--Winter-->
                                            <xsl:variable name="est-timestamp" select="xs:dateTime($deadline_actual) + (xs:dayTimeDuration('-PT5H'))"/>
                                            <xsl:variable name="est" select="format-dateTime(xs:dateTime($est-timestamp), '[D01]/[M01]/[Y0001] [h01]:[m01] [PN, 2-2]')"/>
                                            <xsl:value-of select="$est"/> (ET/EST)<br/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:value-of select="$it"/> (IT/IST)
                                    <!--end for tieckt id #3333860-->
                                    <br/>
                                </body>
                            </html>
                        </content>
                    </multipart-body>
                </mail>
            </mails>
        </xsl:variable>
        
        <cs:command name="com.censhare.api.Mail.SendMail">
            <cs:param name="source">
                <xsl:copy-of select="$source"/>
            </cs:param>
        </cs:command>
    </xsl:template>
</xsl:stylesheet>
