<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:ioi="http://www.iointegration.com">  

    <!-- cc - key is used in the auto UI generation to transfer the chosen data to the backend.  We translate this key to remove
              characters from the string to provide a propery key to the model.  Key needs to be changed in both places for matching
              routine.
    - cc -->
    <xsl:variable name="translate-from-key" select="':.-_ '"/>

    <xsl:template match="feature">
        <xsl:param name="asset-xml"/>
        <xsl:variable name="feature" select="."/>

        <xsl:if test="$feature/@user-selected = '0' or ($feature/@user-selected = '1' and exists($excludes/excludes/exclude[@value = translate($feature/@label, $translate-from-key, '')]))">
            <xsl:if test="ioi:perform-evaluation($feature, $asset-xml) = 'true'">
                    <xsl:choose>
                        <xsl:when test="exists(value/article)">
                            <xsl:choose>
                                <xsl:when test="value/article/content/text/paragraph/error_processing_article">
                                    <!-- rg <fo:block keep-together="1"> rg -->
                                    <fo:block>    
                                        <xsl:variable name="style" select="./@style"/>
                                        <!-- rg <xsl:message>   ###   FEATURE - <xsl:copy-of select="$feature"/></xsl:message> rg -->
                                        <!-- rg <xsl:message>   ###   FEATURE STYLE STYLE - <xsl:value-of select="$style"/></xsl:message> rg -->
                                        <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $style]"/>
                                        <xsl:apply-templates select="value/article/content/text/paragraph/error_processing_article" />
                                    </fo:block>
                                </xsl:when>
                                <xsl:when test="value/article/content/text/paragraph/text() != ''">
                                    <fo:block>
                                        <xsl:variable name="style" select="./@style"/>
                                       <!-- nh  <xsl:message>   ###   FEATURE - <xsl:copy-of select="$feature"/></xsl:message>
                                        <xsl:message>   ###   FEATURE STYLE STYLE - <xsl:value-of select="$style"/></xsl:message> nh -->
                                        <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $style]"/>
                                        <xsl:apply-templates select="value/article/content"/>
                                    </fo:block>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="value/text() != ''">
                                <!-- rg <fo:block keep-together="1"> rg -->
                                <fo:block>
                                    <xsl:variable name="style" select="./@style"/>
                                    <!-- rg <xsl:message>   ###   FEATURE - <xsl:copy-of select="$feature"/></xsl:message> rg -->
                                    <!-- rg <xsl:message>   ###   FEATURE STYLE - <xsl:value-of select="$style"/></xsl:message> rg -->
                                    <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $style]"/>
                                    <xsl:value-of select="value/text()"/>
                                </fo:block>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template match="feature" mode="converted">
        <xsl:param name="asset-xml"/>
        <xsl:variable name="feature" select="."/>
        <xsl:if test="$feature/@user-selected = '0' or ($feature/@user-selected = '1' and exists($excludes/excludes/exclude[@value = translate($feature/@label, $translate-from-key, '')]))">
            <xsl:if test="ioi:perform-evaluation($feature, $asset-xml) = 'true'">
                <fo:inline>
                  <xsl:value-of select="$feature/value/article/content/text/paragraph/text()"/>
                </fo:inline>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:template match="feature[value[@language='en']]" mode="language-header">
        <xsl:param name="asset-xml"/>

        <xsl:variable name="feature" select="."/>

        <xsl:if test="$feature/@user-selected = '0' or ($feature/@user-selected = '1' and exists($excludes/excludes/exclude[@value = translate($feature/@label, $translate-from-key, '')]))">
            <xsl:if test="ioi:perform-evaluation($feature, $asset-xml) = 'true'">
                    <xsl:choose>
                        <xsl:when test="exists(value[@language='en']/article)">
                            <xsl:choose>
                                <xsl:when test="value[@language='en']/article/content/text/paragraph/error_processing_article">
                                    <fo:block>    
                                        <xsl:variable name="style" select="./@style"/>
                                        <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $style]"/>
                                        <xsl:apply-templates select="value[@language='en']/article/content/text/paragraph/error_processing_article"/>
                                    </fo:block>
                                </xsl:when>
                                <xsl:when test="value[@language='en']/article/content/text/paragraph/text() != ''">
                                    <fo:block>
                                        <xsl:variable name="style" select="./@style"/>
                                        <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $style]"/>
                                        <xsl:apply-templates select="value[@language='en']/article/content"/>
                                    </fo:block>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="value[@language='en']/text() != ''">
                                <fo:block>
                                    <xsl:variable name="style" select="./@style"/>
                                    <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $style]"/>
                                    <xsl:value-of select="value[@language='en']/text()"/>
                                </fo:block>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
            </xsl:if>
        </xsl:if>

    </xsl:template>

    <xsl:template match="feature" mode="table-row">
        <xsl:param name="asset-xml"/>
        <xsl:variable name="feature" select="."/>
        <xsl:if test="$feature/@user-selected = '0' or ($feature/@user-selected = '1' and exists($excludes/excludes/exclude[@value = translate($feature/@label, $translate-from-key, '')]))">
            <xsl:if test="ioi:perform-evaluation($feature, $asset-xml) = 'true' and ( exists(value/article) or value/text() != '' )">
                <xsl:variable name="cells">
                    <cells>
                        <xsl:for-each select="./value">
                            <xsl:variable name="language" select="./@language"/>
                            <fo:table-cell>
                                <fo:block padding-before="0.75em">
                                    <xsl:choose>
                                        <xsl:when  test="./@language = 'CT' or ./@language = 'CS'">
                                            <xsl:variable name="style" select="concat($feature/@style, '-chinese')"/>
                                            <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $style]"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:apply-templates select="$templates/xsl:stylesheet/xsl:template[@name = $feature/@style]"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:variable name="content">
                                        <content>
                                            <xsl:choose>
                                                <xsl:when test="exists(article)">
                                                    <xsl:apply-templates select="article/content">
                                                        <xsl:with-param name="language" select="$language" tunnel="yes"/>
                                                    </xsl:apply-templates>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:if test="value/text() != ''">
                                                        <xsl:value-of select="text()"/>
                                                    </xsl:if>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </content>
                                    </xsl:variable>
                                    <xsl:for-each select="$content/content/fo:block">
                                        <xsl:if test="./node()">
                                            <xsl:copy-of select="."/>
                                        </xsl:if>
                                    </xsl:for-each>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:for-each>
                    </cells>
                </xsl:variable>
                <xsl:if test="count($cells/cells/fo:table-cell/fo:block/fo:block/node()) gt 1">
                    <fo:table-row>
                        <xsl:copy-of select="$cells/cells/fo:table-cell[1]"/>
                        <fo:table-cell><fo:block></fo:block></fo:table-cell>
                        <xsl:copy-of select="$cells/cells/fo:table-cell[2]"/>
                    </fo:table-row>
                </xsl:if>
            </xsl:if>
        </xsl:if>
    </xsl:template>

    <xsl:function name="ioi:perform-evaluation">
        <xsl:param name="feature"/>
        <xsl:param name="asset-xml"/>

        <xsl:choose>
            <xsl:when test="$feature/@use-boolean = '0'">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="return"/>
                <cs:command name="com.censhare.api.ioiXPathEvaluate.evaluate" returning="return">
                  <cs:param name="xpath" select="substring-after($feature/@bool-expr, ':')"/>
                  <cs:param name="asset-xml" select="$asset-xml"/>
                </cs:command>
                <xsl:value-of select="$return"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>