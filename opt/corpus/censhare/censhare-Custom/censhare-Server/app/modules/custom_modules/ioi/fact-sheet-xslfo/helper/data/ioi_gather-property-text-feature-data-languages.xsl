<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:xe="http://www.censhare.com/xml/3.0.0/xmleditor"
  xmlns:my="http://www.censhare.com"
  xmlns:ioi="http://www.iointegration.com"
  exclude-result-prefixes="#all">
  <xsl:output method="xml" indent="yes" omit-xml-declaration="no" encoding="UTF-8"/>

  <xsl:param name="asset-id" select="'182761'"/>
  <xsl:param name="languages" select="('en', 'de')"/>
  <xsl:param name="mode" select="'none'"/>

  <xsl:template match="/">

    <xsl:choose>
      <xsl:when test="$asset-id != ''">
        <xsl:variable name="config"/>
        <cs:command name="com.censhare.api.io.GetFilesystemRef" returning="config">
          <cs:param name="name" select="'config-runtime'" />
        </cs:command>

        <xsl:variable name="content"/>
        <cs:command name="com.censhare.api.io.ReadXML" returning="content">
          <cs:param name="source" select="concat($config, 'modules/ioi/christies/feature_to_text_mapping/mapping-language.xml')"/>
        </cs:command>
        
        <xsl:apply-templates select="cs:asset()[@censhare:asset.id = $asset-id]">
          <xsl:with-param name="content" select="$content"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <data>
          <xsl:message>   ###   GATHER FEATURE TEXT ASSETS FAILED - No asset id was received...</xsl:message>
          <error msg="No asset id was received..."/>
        </data>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <xsl:template match="asset">
    <xsl:param name="content"/>

    <xsl:variable name="asset-xml" select="."/>

    <data assetid="{$asset-xml/@id}">
      <xsl:for-each select="$content/config/*/mappings/mapping">
        <xsl:variable name="feature-key" select="./@key"/>
        <xsl:variable name="child-name" select="./@child-name"/>
        <xsl:variable name="child-type" select="./@type"/>
        <xsl:variable name="rel" select="./@rel"/>
        <xsl:variable name="use-text-asset" select="./@text-asset"/>
        <xsl:variable name="user-selected" select="./@user-selected"/>
        <xsl:variable name="label" select="./@label"/>
        <feature use-boolean="{./@use-boolean}" bool-expr="{./@bool-expr}" style="{./@style}" key="{$feature-key}" name="{if($child-name != '') then $child-name else cs:master-data('feature')[@key=$feature-key]/@name}" position="{ancestor::*[parent::*[name() = 'config']]/name()}" user-selected="{$user-selected}" label="{$label}">
          <xsl:for-each select="tokenize($languages, ',')">
            <xsl:variable name="language" select="."/>
            <xsl:message> ##### R.G Language Test! <xsl:value-of select="." /></xsl:message>
            <value language="{$language}">
              <!-- cc - test if configuration calls for text asset to be used - cc -->
              <xsl:choose>
                <xsl:when test="$use-text-asset = '1'">
                  <xsl:variable name="child-text-asset-xml" select="$asset-xml/cs:child-rel()[@key=$rel]/.[@type=$child-type and @language=$language]"/>
                  <!-- cc - Still need to test if asset exists and failover to feature data if it does not - cc -->
                  <xsl:choose>
                    <xsl:when test="$child-text-asset-xml/storage_item[@key='master'][1]">
                      <xsl:attribute name="fromFile" select="'1'"/>
                      <xsl:variable name="article" select="$child-text-asset-xml/storage_item[@key='master'][1]"/>
                      <xsl:variable name="fs" select="$child-text-asset-xml/storage_item[@key='master'][1]/@filesys_name"/>
                      <xsl:variable name="updated-article">
                        <xsl:choose>
                          <xsl:when test="$mode='none'">
                            <cs:command name="com.censhare.api.christiesPrepareArticle.prepare">
                              <cs:param name="article" select="$article"/>
                              <cs:param name="fs" select="$fs"/>
                              <cs:param name="id" select="$child-text-asset-xml/@id"/>
                              <cs:param name="replaceLotSymbols" select="'false'"/>
                              <cs:param name="addLineBreaks" select="'true'"/>
                            </cs:command>
                          </xsl:when>
                          <xsl:when test="$mode='excel'">
                            <cs:command name="com.censhare.api.christiesPrepareArticle.prepareForExcel">
                              <cs:param name="article" select="$article"/>
                              <cs:param name="fs" select="$fs"/>
                              <cs:param name="id" select="$child-text-asset-xml/@id"/>
                              <cs:param name="replaceLotSymbols" select="'true'"/>
                              <cs:param name="addLineBreaks" select="'true'"/>
                            </cs:command>
                          </xsl:when>
                        </xsl:choose>
                      </xsl:variable>
                      <xsl:copy-of select="$updated-article"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:message>   ###   FAILING USING TEXT ASSET</xsl:message>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:message> ##### R.G IN otherwise</xsl:message>
                  <xsl:attribute name="fromFile" select="'0'"/>
                  <xsl:variable name="updated-article">
                    <xsl:choose>
                      <xsl:when test="$mode='none'">
                        <xsl:message> ##### R.G In mode None</xsl:message>
                        <cs:command name="com.censhare.api.christiesPrepareArticle.prepareFromString" returning="the-feat">
                          <cs:param name="textString" select="ioi:pull-feature-data($asset-xml, $feature-key, $language)"/>
                          <cs:param name="replaceLotSymbols" select="'false'"/>
                          <cs:param name="addLineBreaks" select="'true'"/>
                        </cs:command>
                        <xsl:copy-of select="$the-feat"/>
                      </xsl:when>
                      <xsl:when test="$mode='excel'">
                        <xsl:message> ##### R.G In Mode Excelt</xsl:message>
                        <cs:command name="com.censhare.api.christiesPrepareArticle.prepareForExcelFromString" returning="the-feat">
                          <cs:param name="textString" select="ioi:pull-feature-data($asset-xml, $feature-key, $language)"/>
                          <cs:param name="replaceLotSymbols" select="'true'"/>
                          <cs:param name="addLineBreaks" select="'true'"/>
                        </cs:command>
                        <xsl:copy-of select="$the-feat"/>
                      </xsl:when>
                    </xsl:choose>
                  </xsl:variable>
                  <xsl:copy-of select="$updated-article"/>
                </xsl:otherwise>
              </xsl:choose>
            </value>
          </xsl:for-each>
        </feature>
      </xsl:for-each>
    </data>

  </xsl:template>

  <xsl:function name="ioi:pull-feature-data">
    <xsl:param name="asset-xml"/>
    <xsl:param name="key"/>
    <xsl:param name="language"/>
    <xsl:message> ##### R.G IOI PUll Feature Data Key: <xsl:value-of select="$key" /></xsl:message>
    <xsl:message> ##### R.G IOI Language: <xsl:value-of select="$language" /></xsl:message>
    <!-- cc - Test if evaluation is necessary or not - cc -->
    <xsl:choose>
      <xsl:when test="starts-with($key, ':')">
        <xsl:variable name="return"/>
        <cs:command name="com.censhare.api.ioiXPathEvaluate.evaluate" returning="return">
          <cs:param name="xpath" select="substring-after($key, ':')"/>
          <cs:param name="asset-xml" select="$asset-xml"/>
          <cs:param name="language" select="$language"/>
        </cs:command>
        <xsl:value-of select="$return"/>
        <xsl:message> ##### R.G PUll Feature Data return value <xsl:value-of select="$return" /></xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="cs:getFeatureValue($asset-xml/asset_feature[@feature=$key and @language=$language])"/>
      </xsl:otherwise>
    </xsl:choose>

  </xsl:function>

  <!-- Get localized value of given feature element -->
  <xsl:function name="cs:getFeatureValue">
    <xsl:param name="featureElement" as="element(asset_feature)" />
    <xsl:variable name="type" select="cs:cachelookup('feature', '@key', $featureElement/@feature)/@value_type" />
    <xsl:choose>
      <!-- 0: No value -->
      <xsl:when test="$type='0'">
        <xsl:value-of select="''" />
      </xsl:when>
      <!-- 1: Hierarchical attribute (string) -->
      <xsl:when test="$type='1'">
        <xsl:value-of select="cs:cachelookup('feature_value', '@feature', $featureElement/@feature, '@value_key', $featureElement/@value_key)/@name" />
      </xsl:when>
      <!-- 2: Enumeration (string) -->
      <xsl:when test="$type='2'">
        <xsl:value-of select="cs:cachelookup('feature_value', '@feature', $featureElement/@feature, '@value_key', $featureElement/@value_key)/@name" />
      </xsl:when>
      <!-- 3: Integer (long) -->
      <xsl:when test="$type='3'">
        <xsl:value-of select="string-join((cs:format-number($featureElement/@value_long, '#,###'), cs:getFeatureUnitString($featureElement)), ' ')" />
      </xsl:when>
      <!-- 4: String -->
      <xsl:when test="$type='4'">
        <xsl:choose>
          <!-- censhare URL -->
          <xsl:when test="$featureElement/@feature='censhare:url'">
            <a class="asset-info-link" target="_blank">
              <xsl:attribute name="href" select="$featureElement/@value_string" />
              <xsl:value-of select="$featureElement/@value_string" />
            </a>
          </xsl:when>
          <!-- String -->
          <xsl:otherwise>
            <xsl:value-of select="$featureElement/@value_string" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- 5: Timestamp -->
      <xsl:when test="$type='5'">
        <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'relative-short', 'short')" />
      </xsl:when>
      <!-- 6: Boolean -->
      <xsl:when test="$type='6'">
        <xsl:value-of select="if ($featureElement/@value_long='1') then '&#x2611;' else '&#x2610;'" />
      </xsl:when>
      <!-- 7: Double -->
      <xsl:when test="$type='7'">
        <xsl:value-of select="string-join((cs:format-number($featureElement/@value_double, '#,###.###'), cs:getFeatureUnitString($featureElement)), ' ')" />
      </xsl:when>
      <!-- 8: Integer pair -->
      <xsl:when test="$type='8'">
        <xsl:value-of select="string-join((concat(cs:format-number($featureElement/@value_long, '#,###'), '-', cs:format-number($featureElement/@value_long2, '#,###')), cs:getFeatureUnitString($featureElement)), ' ')" />
      </xsl:when>
      <!-- 9: Timestamp pair -->
      <xsl:when test="$type='9'">
        <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'relative-short', 'short'), '-', cs:format-date($featureElement/@value_timestamp2, 'relative-short', 'short'))" />
      </xsl:when>
      <!-- 10: Asset reference -->
      <xsl:when test="$type='10'">
        <xsl:variable name="refAsset" select="cs:get-asset($featureElement/@value_asset_id)" />
        <xsl:choose>
          <xsl:when test="exists($refAsset)">
            <xsl:value-of select="if ($refAsset/asset_feature[@feature='censhare:name' and @language='en']) then $refAsset/asset_feature[@feature='censhare:name' and @language='en']/@value_string else $refAsset/@name" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('ID: ', $featureElement/@value_asset_id)" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <!-- 11: XML -->
      <xsl:when test="$type='11'">
        <xsl:value-of select="''" />
      </xsl:when>
      <!-- 12: Double pair -->
      <xsl:when test="$type='12'">
        <xsl:value-of select="string-join((concat(cs:format-number($featureElement/@value_double, '#,###.###'), '-', cs:format-number($featureElement/@value_double2, '#,###.###')), cs:getFeatureUnitString($featureElement)), ' ')" />
      </xsl:when>
      <!-- 13: Date -->
      <xsl:when test="$type='13'">
        <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'relative-short', 'none')" />
      </xsl:when>
      <!-- 14: Date pair -->
      <xsl:when test="$type='14'">
        <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'relative-short', 'none'), '-', cs:format-date($featureElement/@value_timestamp2, 'relative-short', 'none'))" />
      </xsl:when>
      <!-- 15: Time -->
      <xsl:when test="$type='15'">
        <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'none', 'short')" />
      </xsl:when>
      <!-- 16: Time pair -->
      <xsl:when test="$type='16'">
        <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'none', 'short'), '-', cs:format-date($featureElement/@value_timestamp2, 'none', 'short'))" />
      </xsl:when>
      <!-- 17: Year -->
      <xsl:when test="$type='17'">
        <xsl:value-of select="year-from-dateTime($featureElement/@value_timestamp)" />
      </xsl:when>
      <!-- 18: Year pair -->
      <xsl:when test="$type='18'">
        <xsl:value-of select="concat(year-from-dateTime($featureElement/@value_timestamp), '-', year-from-dateTime($featureElement/@value_timestamp2))" />
      </xsl:when>
      <!-- 19: Year/month -->
      <xsl:when test="$type='19'">
        <xsl:value-of select="concat(cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), ' ', year-from-dateTime($featureElement/@value_timestamp))" />
      </xsl:when>
      <!-- 20: Year/month pair -->
      <xsl:when test="$type='20'">
        <xsl:value-of select="concat(cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), ' ', year-from-dateTime($featureElement/@value_timestamp), '-', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)), ' ', year-from-dateTime($featureElement/@value_timestamp2))" />
      </xsl:when>
      <!-- 21: Month -->
      <xsl:when test="$type='21'">
        <xsl:value-of select="cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp))" />
      </xsl:when>
      <!-- 22: Month pair -->
      <xsl:when test="$type='22'">
        <xsl:value-of select="concat(cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), '-', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)))" />
      </xsl:when>
      <!-- 23: Month/day -->
      <xsl:when test="$type='23'">
        <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '. ', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)))" />
      </xsl:when>
      <!-- 24: Month/day pair -->
      <xsl:when test="$type='24'">
        <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '. ', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), '-', day-from-dateTime($featureElement/@value_timestamp2), '. ', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)))" />
      </xsl:when>
      <!-- 25: Day -->
      <xsl:when test="$type='25'">
        <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '.')" />
      </xsl:when>
      <!-- 26: Day pair -->
      <xsl:when test="$type='26'">
        <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '.-', day-from-dateTime($featureElement/@value_timestamp2), '.')" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="''"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  <!-- Get localized value of given feature element -->
  <xsl:function name="cs:getFeatureUnitString" as="xs:string">
    <xsl:param name="featureElement" as="element(asset_feature)" />
    <xsl:value-of select="if ($featureElement/@value_unit) then cs:cachelookup('unit_set_rel', '@unit', $featureElement/@value_unit)/@unit else ()" />
  </xsl:function>
  <!-- Get localized name of given month (integer) -->
  <xsl:function name="cs:getMonthName" as="xs:string">
    <xsl:param name="month" as="xs:integer" />
    <xsl:choose>
      <xsl:when test="$month=1">${january}</xsl:when>
      <xsl:when test="$month=2">${february}</xsl:when>
      <xsl:when test="$month=3">${march}</xsl:when>
      <xsl:when test="$month=4">${april}</xsl:when>
      <xsl:when test="$month=5">${may}</xsl:when>
      <xsl:when test="$month=6">${june}</xsl:when>
      <xsl:when test="$month=7">${july}</xsl:when>
      <xsl:when test="$month=8">${august}</xsl:when>
      <xsl:when test="$month=9">${september}</xsl:when>
      <xsl:when test="$month=10">${october}</xsl:when>
      <xsl:when test="$month=11">${november}</xsl:when>
      <xsl:when test="$month=12">${december}</xsl:when>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>
