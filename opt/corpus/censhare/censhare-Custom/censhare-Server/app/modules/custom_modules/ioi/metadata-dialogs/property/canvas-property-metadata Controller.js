function controller($scope, csApiSession, pageInstance) {

	$scope.estimates = [];

	let params = {
		contextAsset: pageInstance.getPathContextReader().getValue().get('id')
	};

	console.log("Property Meta Data")
	csApiSession.transformation('ioi:fact-sheet-estimate', params).then(result => {
		// console.log(result);
		var estimateString = result.estimate;
		// console.log(estimateString);
		var estimateArr = estimateString.split(" | ");
		for(i = 0; i < estimateArr.length; i++) {
	    	var currEstimate = estimateArr[i];
	    	var estObj = {};
	    	estObj.label = i === 0 ? 'First Estimate' : i === 1 ? 'Second Estimate' : i === 2 ? 'Third Estimate' : 'Estimate';
	    	estObj.value = currEstimate
	    	// console.log(estObj);
	    	$scope.estimates.push(estObj);
	    }
		// for(var est in result.value]) {

		// 	var curEst = result.property['estimate-row'][est];

		// 	var estObj = {};
		// 	estObj.label = curEst.position.charAt(0).toUpperCase() + curEst.position.substring(1) + " Estimate";

		// 	estObj.value = curEst.estimate['$t'];

		// 	$scope.estimates.push(estObj);
		// 	console.log("IN FOR EACH PROPERTY")
		// 	console.log(estObj);

		// }
	});

	currentAssetDetails(pageInstance.getPathContextReader().getValue().get('id'));

	function currentAssetDetails(assetId){
        csApiSession.asset.get(Number(assetId)).then((result) => {
            $scope.data = result.container[0].asset;
            // console.log($scope.data);
            if ($scope.data.traits.type.type == 'product.') {
                var props = propertyDetailsToDisplay($scope.data);
            }
        });   
    }

    function propertyDetailsToDisplay(assetInfo){
	    // console.log($scope);

	    var regex = /[0-9]{1,10}[1]/;

	    var rels = assetInfo.relations;

	    for (var i = rels.length - 1; i >= 0; i--) {
	        if (rels[i].direction == 'parent') {
	            var refAsset = rels[i].ref_asset.toString();
	            $scope.parentId = refAsset.match(/[0-9]{1,10}/);
	            parentSaleAssetDetails($scope.parentId[0]);
	        }
	    }
	}

	function parentSaleAssetDetails(assetId) {
		csApiSession.asset.get(Number(assetId)).then((result) => {
		    // values from parent sale
		    if (result.container[0].asset.traits.type.type == 'sale.') {
		    	$scope.parentData = result.container[0].asset;
		    	// console.log($scope.parentData);
		    } else {
		    	console.log('No parent sale for property');
		    }
		});
	}

}