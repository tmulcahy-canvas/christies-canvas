<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:my="http://www.censhare.com/xml/3.0.0/xpath-functions/my" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:ioi="http://www.iointegration.com">
    <!-- cc - Fixed Values - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:lot-fact-sheet-xslfo.fixed-values/storage/master/file"/>
    <!-- cc- Attribute Sets - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:lot-fact-sheet-xslfo.attribute-sets/storage/master/file"/>
    <!-- cc - Standard Article Templates - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:standard-article-templates/storage/master/file"/>
    <!-- cc - Property Text Handling Templates - cc -->
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=ioi:process-property-text-feature-data/storage/master/file"/>
    <xsl:param name="languages"/>
    <xsl:param name="use-external-condition"/>
    <xsl:param name="use-image-status"/>
    <xsl:param name="excludes"/>
    <!-- cc - String for calling text extraction routine - cc -->
    <xsl:variable name="text-extraction-url" select="'censhare:///service/assets/asset;censhare:resource-key=ioi:gather-property-text-feature-data/transform;key=ioi:gather-property-text-feature-data;mode=none;asset-id='"/>
    <!-- cc - GLOBAL FACT SHEET VARIABLES - cc -->
    <xsl:variable name="lot-fact-sheet-footer" select="'Other fees apply in addition to the hammer price- see the Conditions of Sale at the back of the Sale Catalogue.'"/>
    <!-- cc - templates stored so attribute sets can be applied dynamically - cc -->
    <xsl:variable name="templates">
        <xsl:variable name="content"/>
        <cs:command name="com.censhare.api.io.ReadXML" returning="content">
            <cs:param name="source" select="cs:asset()[@censhare:resource-key = 'ioi:lot-fact-sheet-xslfo.attribute-sets']/storage_item[@key='master'][1]"/>
        </cs:command>
        <xsl:copy-of select="$content"/>
        <!-- nh <xsl:message>### NH - CONTENT - <xsl:copy-of select="$content"/></xsl:message> nh -->
    </xsl:variable>
    <xsl:template match="/" priority="1.0">
        <!-- nh <xsl:message>##### R.G use Image STatus /
            <xsl:copy-of select="$use-image-status" />/
            <xsl:value-of select="$use-image-status" />
        </xsl:message> nh -->
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xsl:use-attribute-sets="root">
            <xsl:call-template name="make-layout-master-set"/>
            <fo:page-sequence master-reference="all-pages">
                <fo:static-content flow-name="page-footer">
                    <fo:block space-after.conditionality="retain" space-after="{$page-footer-margin}" xsl:use-attribute-sets="page-footer">
                        <xsl:if test="$page-number-print-in-footer = 'true'">
                            <xsl:value-of select="$lot-fact-sheet-footer"/>
                        </xsl:if>
                    </fo:block>
                </fo:static-content>
                <xsl:variable name="rels" select="asset/cs:child-rel()[@key='target.' or @key='user.']/.[@type='product.']"/>
                <!-- cc -<xsl:variable name="rels">
                                    <assets>
                                        <xsl:for-each select="asset/child_asset_rel">
                                            <xsl:sort-by/>- cc -->
                <!-- cc - cs:get-asset()[@censhare:asset.id = @child_asset- cc -->
                <!-- cc -</xsl:for-each>
                                    </assets>
                                </xsl:variable>- cc -->
                <fo:flow flow-name="xsl-region-body">
                    <xsl:choose>
                        <xsl:when test="$rels">
                            <xsl:apply-templates select="$rels" mode="format-to-sale-sheet">
                                <xsl:sort select="parent_asset_rel/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long" data-type="number"/>
                                <xsl:with-param name="count" select="count($rels)"/>
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <fo:block>No properties found.</fo:block>
                        </xsl:otherwise>
                    </xsl:choose>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="asset" mode="format-to-sale-sheet">
        <xsl:param name="count"/>
        <!-- nh <xsl:message>### NH -   SALE FACT SHEET - Processing property <xsl:value-of select="concat(position(), ' of ', $count, '.')"/></xsl:message> nh -->
        <xsl:variable name="lot-facts">
            <result>
                <xsl:apply-templates select="."/>
            </result>
        </xsl:variable>
        <xsl:copy-of select="$lot-facts/result/fo:flow/node()"/>
        <fo:block page-break-after="always"/>
    </xsl:template>
    <xsl:template name="make-layout-master-set">
        <fo:layout-master-set>
            <fo:simple-page-master master-name="all-pages" xsl:use-attribute-sets="page">
                <fo:region-body margin-top="{$page-margin-top}" margin-right="{$page-margin-right}" margin-bottom="25mm" margin-left="{$page-margin-left}" column-count="{$column-count}" column-gap="{$column-gap}"/>
                <!--  $writing-mode = 'lr-tb'  -->
                <fo:region-before region-name="page-header" extent="{$page-margin-top}" display-align="before"/>
                <fo:region-after region-name="page-footer" extent="{$page-margin-bottom}" display-align="after"/>
                <fo:region-start extent="{$page-margin-left}"/>
                <fo:region-end extent="{$page-margin-bottom}"/>
            </fo:simple-page-master>
        </fo:layout-master-set>
    </xsl:template>
    <xsl:template match="asset">
        <xsl:variable name="asset-xml" select="." />
        <!-- cc - Asset Feature Data Variables - cc -->
        <xsl:variable name="extracted-feature-data" select="doc(concat($text-extraction-url, ./@id))" />
        <xsl:message>----- EXTRACTED FEATURE DATA -----
            <xsl:copy-of select="$extracted-feature-data" />
        </xsl:message>
        <!-- cc -<fo:static-content flow-name="page-header">
                            <fo:block space-before.conditionality="retain" space-before="{$page-header-margin}" xsl:use-attribute-sets="page-header">
                              <xsl:value-of select="concat($jde-location, $header-delimiter, $jde-item-number)"/>
                            </fo:block>
                          </fo:static-content>- cc -->
        <fo:flow flow-name="xsl-region-body">
            <!-- nh <xsl:message> ##### R.G In the XSL Region Body</xsl:message> nh -->
            <!-- cc - LOGO - cc -->
            <fo:block span="all">
                <xsl:apply-templates select="cs:asset()[@censhare:resource-key = 'ioi:chr-logo-for-xslfo-png']" mode="process-logo"/>
            </fo:block>
            <!-- cc - Header Section - cc -->
            <fo:block span="all">
                <xsl:apply-templates select="$extracted-feature-data/data/feature[@position = 'header']">
                    <xsl:with-param name="asset-xml" select="$asset-xml"/>
                </xsl:apply-templates>
            </fo:block>
            <!-- cc - MAIN IMAGE - cc -->
            <fo:block xsl:use-attribute-sets="lot-sheet-image" span="all">
                <xsl:variable name="image-status-from-command" select="$use-image-status" as="xs:string"/>
                <xsl:variable name="img-status" select="if($image-status-from-command = '') 
                    then 
                        asset_feature[@feature='canvas:image-status']/@value_long
                    else 
                        $image-status-from-command" as="xs:string"/>
                <xsl:variable name="asset-id-max" select="round(max(./child_asset_rel[@key='user.main-picture.']/@child_asset))" />
                <!-- nh <xsl:message>   ### SALE FACT SHEET - ASSET ID MAX - <xsl:value-of select="$asset-id-max"/></xsl:message> nh -->
                <xsl:if test="$asset-id-max">
                    <xsl:apply-templates select="cs:asset()[@censhare:asset.id=$asset-id-max]" mode="process-image">
                        <xsl:with-param name="image-status" select="$img-status"/>
                    </xsl:apply-templates>
                </xsl:if>
            </fo:block>
            <!-- cc - First Section - cc -->
            <xsl:if test="count($extracted-feature-data/data/feature[@position = 'first']) gt 0">
                <fo:block span="all" margin-bottom="4.25pt">
                    <!-- cc -   table added to emulate cols - cc -->
                    <fo:table>
                        <fo:table-column column-width="48%"/>
                        <fo:table-column column-width="52%"/>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell>
                                    <fo:block>
                                        <xsl:apply-templates select="$extracted-feature-data/data/feature[@position = 'first']">
                                            <xsl:with-param name="asset-xml" select="$asset-xml"/>
                                        </xsl:apply-templates>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell>
                                    <fo:block></fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <!-- cc -<fo:leader leader-pattern="rule" leader-length="100%" rule-style="solid" rule-thickness="2pt"/>removed WF175766 - cc -->
                </fo:block>
            </xsl:if>

            <!-- nh Lot Symbol Converted nh -->
            <fo:block>
                <xsl:apply-templates select="$extracted-feature-data/data/feature[@position = 'converted']" mode="converted">
                    <xsl:with-param name="asset-xml" select="$asset-xml"/>
                </xsl:apply-templates>
            </fo:block>
            
            <!-- cc - Second Section - cc -->
            <fo:block>
                <xsl:apply-templates select="$extracted-feature-data/data/feature[@position = 'second']">
                    <xsl:with-param name="asset-xml" select="$asset-xml"/>
                </xsl:apply-templates>
            </fo:block>
        </fo:flow>
    </xsl:template>
    <xsl:template match="asset" mode="process-image">
        <xsl:param name="image-status"/>
        <xsl:variable name="wf-id" select="@wf_id"/>
        <xsl:variable name="wf-step" select="@wf_step"/>
        <xsl:variable name="wf-step-name" select="cs:master-data('workflow_step')[@wf_id = $wf-id and @wf_step = $wf-step]/@name" />
        <!-- nh <xsl:message>   WF STEP NAME - <xsl:value-of select="$wf-step-name"/>   SHOW STATUS - <xsl:value-of select="$image-status"/></xsl:message> nh -->
        <!-- rg <xsl:message> ##### R.G Image Status / <xsl:value-of select="$image-status" /></xsl:message> rg -->
        <xsl:choose>
            <!-- cc - canvas:image-status - cc -->
            <xsl:when test="$image-status = '1' or $image-status = 'true'">
                <xsl:choose>
                    <xsl:when test="$wf-step-name = 'Passed4Print'">
                        <fo:external-graphic xsl:use-attribute-sets="main-image-green">
                            <xsl:attribute name="src">
                                <xsl:text>url('</xsl:text>
                                <xsl:value-of select="storage_item[@key='preview']/@url"/>
                                <xsl:text>')</xsl:text>
                            </xsl:attribute>
                        </fo:external-graphic>
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:external-graphic xsl:use-attribute-sets="main-image-red">
                            <xsl:attribute name="src">
                                <xsl:text>url('</xsl:text>
                                <xsl:value-of select="storage_item[@key='preview']/@url"/>
                                <xsl:text>')</xsl:text>
                            </xsl:attribute>
                        </fo:external-graphic>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <fo:external-graphic xsl:use-attribute-sets="main-image">
                    <xsl:attribute name="src">
                        <xsl:text>url('</xsl:text>
                        <xsl:value-of select="storage_item[@key='preview']/@url"/>
                        <xsl:text>')</xsl:text>
                    </xsl:attribute>
                </fo:external-graphic>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="asset" mode="process-logo">
        <fo:external-graphic xsl:use-attribute-sets="lot-sheet-logo">
            <xsl:attribute name="src">
                <xsl:text>url('</xsl:text>
                <xsl:value-of select="storage_item[@key='preview']/@url"/>
                <xsl:text>')</xsl:text>
            </xsl:attribute>
        </fo:external-graphic>
    </xsl:template>
</xsl:stylesheet>
