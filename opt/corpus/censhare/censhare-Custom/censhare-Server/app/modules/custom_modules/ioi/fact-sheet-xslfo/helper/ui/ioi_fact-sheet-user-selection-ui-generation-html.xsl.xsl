<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:ioi="http://www.iointegration.com" xmlns:xe="http://www.censhare.com/xml/3.0.0/xmleditor">  
	<xsl:output method="html" version="1.0" encoding="utf-8" indent="yes"/>
	<xsl:strip-space elements="*"/>

	<xsl:param name="mappingFile"/>
	<!-- mode -> asset / action -->
	<xsl:param name="mode"/>

	<!-- cc - key is used in the process property text feature data to transfer the chosen data to the backend.  We translate this key to remove
              characters from the string to provide a propery key to the model.  Key needs to be changed in both places for matching
              routine.
    - cc -->
    <xsl:variable name="translate-from-key" select="':.-_ '"/>
	
	<xsl:template match="/">


		<!-- nh <xsl:message>  GENERATING HTML....</xsl:message> nh -->

		<xsl:variable name="config"/>
		<cs:command name="com.censhare.api.io.GetFilesystemRef" returning="config">
			<cs:param name="name" select="'config-runtime'" />
		</cs:command>

		<xsl:variable name="content"/>
		<cs:command name="com.censhare.api.io.ReadXML" returning="content">
			<cs:param name="source" select="concat($config, $mappingFile)"/>
		</cs:command>

		<div>
			<xsl:for-each-group select="$content/config/*/mappings/mapping[@user-selected='1']" group-by="@key">
				<!-- nh <xsl:message>   ### MAPPING FILE - <xsl:copy-of select="$content"/></xsl:message>  nh -->
				
				<xsl:variable name="mapping-node" select="."/>
				<!-- nh <xsl:message>   ### MAPPING NODE - <xsl:copy-of select="$mapping-node"/></xsl:message> nh -->
				<xsl:variable name="label" select="$mapping-node/@label" as="xs:string"/>
				<xsl:variable name="default" select="if($mapping-node/@is-default) then $mapping-node/@is-default else false()" as="xs:boolean"/>
				<xsl:variable name="label-to-key" select="translate(./@label, $translate-from-key, '')"/>
				<xsl:variable name="model-path" select="concat(if($mode = 'asset') then 'asset.traits.canvas.userSelectedProperties.value[0].xmldata.' else '', 'cmd.settings.excludes.', $label-to-key)"/>

				<!-- nh <xsl:message>   ### NH MODEL PATH - <xsl:value-of select="$model-path" /></xsl:message> nh -->

				<xsl:if test="$label != ''">
					<div style="display:block;">
						<cs-checkbox label="{$label}" ng-model="{$model-path}">
							<xsl:if test="$default">
								<xsl:attribute name="ng-init" select="concat($model-path, '=true')"/>
							</xsl:if>
						</cs-checkbox>
					</div>
				</xsl:if>
			
			</xsl:for-each-group>
		</div>

	</xsl:template>

</xsl:stylesheet>