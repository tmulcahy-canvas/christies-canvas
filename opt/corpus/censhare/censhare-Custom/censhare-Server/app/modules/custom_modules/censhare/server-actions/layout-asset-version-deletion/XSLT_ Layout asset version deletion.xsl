<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/SaleHeaderDataResponse"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="#all">
    <!-- output -->
    <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        
        <xsl:variable name="layout-assets">
            <query type="asset">
                <condition name="censhare:asset.wf_id" value="25000"/>
                <condition name="censhare:function.workflow-step" value="40"/>
                <condition name="censhare:asset.modified_date" op="&gt;" value="n-14.00.0000-00:00:00"/>
            </query>
        </xsl:variable>
        
        <xsl:variable name="container-layout-assets">
            <xsl:copy-of select="cs:asset($layout-assets)"/>
        </xsl:variable>
        
        <xsl:for-each select="$container-layout-assets/asset">
            
            <xsl:variable name="current-asst-id" select="@id"/>
            
            <xsl:variable name="print-ready-asst-versions" select="string-join(cs:get-asset-versions($current-asst-id)[@wf_step='40']/@version, ',')"/>
            <xsl:variable name="print-ready-max-asst-versions" select="max(cs:get-asset-versions($current-asst-id)[@wf_step='40']/@version)"/>
            
            <xsl:variable name="with-powerweave-asst-versions" select="string-join(cs:get-asset-versions($current-asst-id)[@wf_step='4']/@version, ',')"/>
            <xsl:variable name="with-powerweave-max-asst-versions" select="max(cs:get-asset-versions($current-asst-id)[@wf_step='4']/@version)"/>
            
            <xsl:variable name="return-to-christies-asst-versions" select="string-join(cs:get-asset-versions($current-asst-id)[@wf_step='6']/@version, ',')"/>
            <xsl:variable name="return-to-christies-max-asst-versions" select="max(cs:get-asset-versions($current-asst-id)[@wf_step='6']/@version)"/>
            
            <xsl:variable name="pre-proof-asst-versions" select="string-join(cs:get-asset-versions($current-asst-id)[@wf_step='10']/@version, ',')"/>
            <xsl:variable name="pre-proof-max-asst-versions" select="max(cs:get-asset-versions($current-asst-id)[@wf_step='10']/@version)"/>
            
            <xsl:variable name="on-proof-asst-versions" select="string-join(cs:get-asset-versions($current-asst-id)[@wf_step='20']/@version, ',')"/>
            <xsl:variable name="on-proof-max-asst-versions" select="max(cs:get-asset-versions($current-asst-id)[@wf_step='20']/@version)"/>
            
            <xsl:variable name="on-screen-asst-versions" select="string-join(cs:get-asset-versions($current-asst-id)[@wf_step='30']/@version, ',')"/>
            <xsl:variable name="on-screen-max-asst-versions" select="max(cs:get-asset-versions($current-asst-id)[@wf_step='30']/@version)"/>
            
            <xsl:choose>
                <xsl:when test="$print-ready-max-asst-versions > 0">
                    <xsl:if test="$print-ready-max-asst-versions > 0">
                        <xsl:sequence select="cs:max-asset-versions($print-ready-asst-versions, $current-asst-id, 'print-ready')"/>
                    </xsl:if>
                    
                    <xsl:if test="$with-powerweave-max-asst-versions > 0">
                        <xsl:sequence select="cs:max-asset-versions($with-powerweave-asst-versions, $current-asst-id, 'with-powerweave')"/>
                    </xsl:if>
                    
                    <xsl:if test="$return-to-christies-max-asst-versions > 0">
                        <xsl:sequence select="cs:max-asset-versions($return-to-christies-asst-versions, $current-asst-id, 'return-to-christies')"/>
                    </xsl:if>
                    
                    <xsl:if test="$pre-proof-max-asst-versions > 0">
                        <xsl:sequence select="cs:max-asset-versions($pre-proof-asst-versions, $current-asst-id, 'pre-proof')"/>
                    </xsl:if>
                    
                    <xsl:if test="$on-proof-max-asst-versions > 0">
                        <xsl:sequence select="cs:max-asset-versions($on-proof-asst-versions, $current-asst-id, 'on-proof')"/>
                    </xsl:if>
                    
                    <xsl:if test="$on-screen-max-asst-versions > 0">
                        <xsl:sequence select="cs:max-asset-versions($on-screen-asst-versions, $current-asst-id, 'on-screen')"/>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:function name="cs:max-asset-versions">
        <xsl:param name="on-asst-versions"/>
        <xsl:param name="current-asst-id"/>
        <xsl:param name="current-asst-wf"/>

        <xsl:for-each select="tokenize($on-asst-versions, ',')">
            <xsl:variable name="current-version" select="number(.)"/>
            <xsl:choose>
                <xsl:when test="($current-asst-wf='print-ready') and (position()=last() or position()= last()-1 or position()= last()-2)"/>
                
                <xsl:when test="position()=1 and $current-asst-wf!='print-ready'"/>
                <xsl:otherwise>
                    <xsl:variable name="get-asset" select="cs:get-asset($current-asst-id, $current-version)"/>
                    <xsl:sequence select="cs:update($get-asset)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:function>
    
    <xsl:function name="cs:update">
        <xsl:param name="get-asset"/>
        <cs:command name="com.censhare.api.assetmanagement.Update" returning="resultAssetXml">
            <cs:param name="source">
                <asset>
                    <xsl:copy-of select="$get-asset/@*"/>
                    <xsl:attribute name="deletion" select="1"/>
                    <xsl:copy-of select="$get-asset/node()"/>
                </asset>
            </cs:param>
        </cs:command>
    </xsl:function>
</xsl:stylesheet>