<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
    xmlns:func="http://ns.censhare.de/functions" exclude-result-prefixes="#all">
   
    <xsl:template match="/">
        <xsl:variable name="asset-xml" select="asset"/>
        <xsl:choose>
            <xsl:when test="starts-with($asset-xml/@type,'issue.')">
                <xsl:variable name="all-layout-asset-within-issue-asset" select="$asset-xml/cs:child-rel()[@key='target.']/.[starts-with(@type,'layout.')]"/>
                <xsl:for-each select="$all-layout-asset-within-issue-asset">
                    <xsl:variable name="current-layout-asset" select="current()"/>
                    <xsl:sequence select="cs:create-group($current-layout-asset)"/>
                </xsl:for-each>
            </xsl:when>
            <xsl:when test="starts-with($asset-xml/@type,'layout.')">
                <xsl:sequence select="cs:create-group($asset-xml)"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
   
    <xsl:function name="cs:create-group">
        <xsl:param name="asset-xml"/>
        <!--xsl:copy-of select="$asset-xml"/-->
        <xsl:variable name="group-asset-exist" select="$asset-xml/cs:child-rel()[@key='user.']/.[starts-with(@type,'group.')]"/>
       
        <!--Sale asset-->
        <xsl:variable name="sale-asset" select="$asset-xml/cs:parent-rel()[@key='target.']/.[starts-with(@type,'issue')]/cs:child-rel()[@key='user.']/.[starts-with(@type,'sale.')]"/>
       
        <!--All layout asset-->
        <xsl:variable name="all-layout-asset-within-issue-asset" select="$asset-xml/cs:parent-rel()[@key='target.']/.[starts-with(@type,'issue.')]/./cs:child-rel()[@key='target.']/.[starts-with(@type,'layout.')]"/>
       
        <!-- All properties assset placed in layout-->
        <xsl:variable name="layout-lots-assets">
            <xsl:for-each select="$all-layout-asset-within-issue-asset">
                <xsl:variable name="current-layout-asset" select="current()"/>
                <xsl:value-of select="$current-layout-asset/cs:child-rel()[@key='actual.']/.[starts-with(@type,'product.')]/@id"/>
            </xsl:for-each>
        </xsl:variable>
       
        <!--All property asset associated with sale asset-->
        <xsl:variable name="lot-assets" select="$sale-asset/cs:child-rel()[@key='target.']/.[starts-with(@type,'product.')]/@id"/>
       
        <!-- All image assset placed in layout-->
        <xsl:variable name="layout-actual-image-assets">
            <xsl:for-each select="$all-layout-asset-within-issue-asset">
                <xsl:variable name="current-layout-asset" select="current()"/>
                <xsl:value-of select="$current-layout-asset/cs:child-rel()[@key='actual.']/.[starts-with(@type,'picture.')]/@id"/>
            </xsl:for-each>
        </xsl:variable>
       
        <!-- All primary image assets that are related with property - on sale asset-->
        <xsl:variable name="lot-assets-primary-images" select="$sale-asset/cs:child-rel()[@key='target.']/.[starts-with(@type,'product.')]/cs:child-rel()[@key='user.main-picture.']/@id"/>
        <!-- All image assets related to sale asset-->
        <xsl:variable name="images-direct-related-to-sale" select="$sale-asset/cs:child-rel()[@key='*']/.[starts-with(@type,'picture.')]/@id"/>
       <!-- Updated by ATK 27092018 3760794 -->
        <xsl:variable name="all-unplaced-assets-id" select="distinct-values(($lot-assets, $lot-assets-primary-images, $images-direct-related-to-sale))"/>
       
        <xsl:choose>
            <xsl:when test="empty($group-asset-exist)">
                <xsl:variable name="group-asset-xml">
                    <!--any properties that belong to a catalogue’s sale that have not been placed-->
                    <asset name="Unplaced Lot assets" type="group.">
                    <!-- Updated by ATK 27092018 3760794 -->
                      <xsl:for-each select="$all-unplaced-assets-id">
                            <xsl:variable name="asset-id" select="."/>
                            <xsl:choose>
                                <xsl:when test="contains($layout-actual-image-assets, $asset-id) or contains ($layout-lots-assets, $asset-id)"/>
                                <xsl:otherwise>
                                    <child_asset_rel key="user." child_asset="{$asset-id}"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                        <parent_asset_rel key="user." parent_asset="{$asset-xml/@id}"/>
                    </asset>
                </xsl:variable>
                <!--xsl:copy-of select="$group-asset-xml"/-->
                <xsl:sequence select="cs:checkin-new($group-asset-xml)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="group-asset-xml">
                    <asset>
                        <xsl:copy-of select="$group-asset-exist/@*"/>
                        <!--any properties that belong to a catalogue’s sale that have not been placed-->
                       <!-- Updated by ATK 27092018 3760794 -->
                        <xsl:for-each select="$all-unplaced-assets-id">
                            <xsl:variable name="asset-id" select="."/>
                            <xsl:choose>
                                <xsl:when test="contains($layout-actual-image-assets, $asset-id) or contains ($layout-lots-assets, $asset-id)"/>
                                <xsl:otherwise>
                                    <child_asset_rel key="user." child_asset="{$asset-id}"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                        <parent_asset_rel key="user." parent_asset="{$asset-xml/@id}"/>
                    </asset>
                </xsl:variable>
                <xsl:sequence select="cs:asset-update($group-asset-xml)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
   
    <xsl:function name="cs:checkin-new">
        <xsl:param name="asset-xml" />
        <cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="processed-asset-xml">
            <cs:param name="source" select="$asset-xml" />
        </cs:command>
        <xsl:copy-of select="$processed-asset-xml" />
    </xsl:function>
    <xsl:function name="cs:asset-update">
        <xsl:param name="asset-xml"/>
        <cs:command name="com.censhare.api.assetmanagement.Update" returning="resultxml">
            <cs:param name="source" select="$asset-xml" />
        </cs:command>
        <xsl:copy-of select="$resultxml" />
    </xsl:function>
   
</xsl:stylesheet>
