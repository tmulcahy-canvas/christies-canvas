function controller($scope) {
    var regex = /\B[A-Z]\B/g
    // (\S[A-Z])
    // var estimateregex = /(\SEstimate)/
        // This contains the labels for each object.  The index in array orders the UI.  Add additional attributes here to be included.  If the attribute is not included it will default to 99.
        var attributes = ['Pre Lot Text', 'Estimates', 'Provenance', 'Exhibited', 'Literature', 'Catalogue Notes', 'Post Lot Text', 'External Condition', 'Primary ID'];
    $scope.$watchGroup(
        [
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.CatalogueNotes',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.Estimates',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.Exhibited',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.FirstEstimate',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.Literature',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.PostLotText',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.PreLotText',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.PrimaryID',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.Provenance',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.SecondEstimate',
            'asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes.ThirdEstimate',

        ],
        function (newVal, oldVal) {
            $scope.newExcludes = [];
    		var dasDummyDang = [];
            var theExcludes = $scope.asset.traits.canvas.userSelectedProperties.value[0].xmldata.cmd.settings.excludes;
            for (var key in theExcludes) {
                var keyString = key;
                var theMatches = keyString.match(regex);
                for (var char in theMatches) {
                    var firstHalf = keyString.slice(0, keyString.indexOf(theMatches[char]))
                    var secondHalf = keyString.slice(keyString.indexOf(theMatches[char]));
                    keyString = firstHalf + " " + secondHalf;
                }

	                dasDummyDang.push({
	                	label: keyString,
	                	value: theExcludes[key],
	                	sort: attributes.includes(keyString) ? attributes.indexOf(keyString) : 99
	                });
            }

            $scope.newExcludes = dasDummyDang;
            console.log($scope.newExcludes);

        }, true);

    
}