<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:canvas="censhare.com"
    xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/ObjectDataRequest"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
		xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
		xmlns:csc="http://www.censhare.com/censhare-custom"
		xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
		exclude-result-prefixes="#all">

    <xsl:output indent="yes" omit-xml-declaration="yes"/>

    <xsl:strip-space elements="*"/>

    <!-- =========================== -->
    <!-- Property Sale Mappings  -->
    <!-- =========================== -->
	<xsl:function name="canvas:salepropertyrelationscheckupdateremove">
		<xsl:param name="propertyAsset" />
		<xsl:param name="mappingElementstoFeatures" />
		<xsl:param name="currentItemXML" />

		<xsl:choose>
			<xsl:when test="normalize-space($currentItemXML/SaleNumber)">

				<xsl:variable name="elementName" select="normalize-space(local-name($currentItemXML/SaleNumber))"/>
				<!--xsl:variable name="propertySaleNumber" select="normalize-space($propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber']/@value_long)"/-->
				
				<xsl:variable name="propertySaleFeature" select="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber']/@value_long"/>

				<xsl:choose>
					<xsl:when test="$propertySaleFeature">
						<xsl:choose>
							<xsl:when test="normalize-space($currentItemXML/SaleNumber) != normalize-space($propertySaleFeature)">
								<xsl:copy-of select="canvas:salepropertyrelationscheckcreate(normalize-space($currentItemXML/SaleNumber),$mappingElementstoFeatures,$currentItemXML)" />
								<xsl:copy-of select="canvas:propertysalelayoutremoverelation($propertySaleFeature,$propertyAsset)" />
								<xsl:copy-of select="canvas:salepropertyremoverelation($propertySaleFeature,$propertyAsset/@id)" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:variable name="saleAssetXML" select="cs:asset()[@censhare:asset.id_extern=concat('sale:',$currentItemXML/SaleNumber) and @censhare:asset.currversion=0]" />
								<xsl:choose>
									<xsl:when test="exists($currentItemXML/ItemIntention)">
										<!--xsl:copy-of select="canvas:propertyitemintentiondefinitions(normalize-space($currentItemXML/ItemIntention),$propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber']/@value_long,$propertyAsset)" /-->

										<xsl:variable name="itemIntentionValue" select="normalize-space($currentItemXML/ItemIntention)"/>
										<!--xsl:param name="saleID" />
										<xsl:param name="propertyID" /-->
	
										<xsl:choose>
											<xsl:when test="$itemIntentionValue eq 'X' or $itemIntentionValue eq 'UNO' or $itemIntentionValue eq 'XAR' or $itemIntentionValue eq 'XPS'">
												<xsl:attribute name="wf_id" select="'20000'"/>
												<xsl:attribute name="wf_step" select="'902'"/>
												<xsl:copy-of select="canvas:salepropertyremoverelation($currentItemXML/SaleNumber,$propertyAsset/@id)" />
												<xsl:copy-of select="canvas:propertysalelayoutremoverelation($currentItemXML/SaleNumber,$propertyAsset)" />
											</xsl:when>
											<xsl:when test="$itemIntentionValue eq 'WDR'">
												<xsl:attribute name="wf_id" select="'20000'"/>
												<xsl:attribute name="wf_step" select="'999'"/>
	
												<xsl:if test="normalize-space($currentItemXML/LotNumber) or normalize-space($currentItemXML/LotSuffix)">
													<excludeElements>
														<parent_asset_rel parent_asset="{$saleAssetXML/@id}" child_asset="{$propertyAsset/@id}" key="target." />
													</excludeElements>
													<parent_asset_rel parent_asset="{$saleAssetXML/@id}" child_asset="{$propertyAsset/@id}" key="target.">
														<xsl:choose>
															<xsl:when test="$currentItemXML/LotNumber">
															    <asset_rel_feature feature="canvas:jdeproperty.lotnumber" value_long="{$currentItemXML/LotNumber}"/>
															</xsl:when>
															<xsl:when test="$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long">
															    <asset_rel_feature feature="canvas:jdeproperty.lotnumber" value_long="{$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long}"/>
															</xsl:when>
															<xsl:otherwise></xsl:otherwise>
														</xsl:choose>
														<xsl:choose>
															<xsl:when test="$currentItemXML/LotSuffix">
															    <asset_rel_feature feature="canvas:jdeproperty.lotsuffix" value_string="{$currentItemXML/LotSuffix}"/>
															</xsl:when>
															<xsl:when test="$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string">
															    <asset_rel_feature feature="canvas:jdeproperty.lotsuffix" value_string="{$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string}"/>
															</xsl:when>
															<xsl:otherwise></xsl:otherwise>
														</xsl:choose>
													</parent_asset_rel>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:attribute name="wf_id" select="'20000'"/>
												<xsl:attribute name="wf_step" select="''"/>
		
												<xsl:if test="normalize-space($currentItemXML/LotNumber) or normalize-space($currentItemXML/LotSuffix)">
													<excludeElements>
														<parent_asset_rel parent_asset="{$saleAssetXML/@id}" child_asset="{$propertyAsset/@id}" key="target." />
													</excludeElements>
													<parent_asset_rel parent_asset="{$saleAssetXML/@id}" child_asset="{$propertyAsset/@id}" key="target.">
														<xsl:choose>
															<xsl:when test="$currentItemXML/LotNumber">
															    <asset_rel_feature feature="canvas:jdeproperty.lotnumber" value_long="{$currentItemXML/LotNumber}"/>
															</xsl:when>
															<xsl:when test="$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long">
															    <asset_rel_feature feature="canvas:jdeproperty.lotnumber" value_long="{$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long}"/>
															</xsl:when>
															<xsl:otherwise></xsl:otherwise>
														</xsl:choose>
														<xsl:choose>
															<xsl:when test="$currentItemXML/LotSuffix">
															    <asset_rel_feature feature="canvas:jdeproperty.lotsuffix" value_string="{$currentItemXML/LotSuffix}"/>
															</xsl:when>
															<xsl:when test="$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string">
															    <asset_rel_feature feature="canvas:jdeproperty.lotsuffix" value_string="{$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string}"/>
															</xsl:when>
															<xsl:otherwise></xsl:otherwise>
														</xsl:choose>
													</parent_asset_rel>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:when>
									<xsl:otherwise>
										<xsl:if test="normalize-space($currentItemXML/LotNumber) or normalize-space($currentItemXML/LotSuffix)">
											<excludeElements>
												<parent_asset_rel parent_asset="{$saleAssetXML/@id}" child_asset="{$propertyAsset/@id}" key="target." />
											</excludeElements>
											<parent_asset_rel parent_asset="{$saleAssetXML/@id}" child_asset="{$propertyAsset/@id}" key="target.">
												<xsl:choose>
													<xsl:when test="$currentItemXML/LotNumber">
													    <asset_rel_feature feature="canvas:jdeproperty.lotnumber" value_long="{$currentItemXML/LotNumber}"/>
													</xsl:when>
													<xsl:when test="$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long">
													    <asset_rel_feature feature="canvas:jdeproperty.lotnumber" value_long="{$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotnumber']/@value_long}"/>
													</xsl:when>
													<xsl:otherwise></xsl:otherwise>
												</xsl:choose>
												<xsl:choose>
													<xsl:when test="$currentItemXML/LotSuffix">
													    <asset_rel_feature feature="canvas:jdeproperty.lotsuffix" value_string="{$currentItemXML/LotSuffix}"/>
													</xsl:when>
													<xsl:when test="$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string">
													    <asset_rel_feature feature="canvas:jdeproperty.lotsuffix" value_string="{$propertyAsset//parent_asset_rel[@parent_asset=$saleAssetXML/@id and @child_asset=$propertyAsset/@id and @key='target.']/asset_rel_feature[@feature='canvas:jdeproperty.lotsuffix']/@value_string}"/>
													</xsl:when>
													<xsl:otherwise></xsl:otherwise>
												</xsl:choose>
											</parent_asset_rel>
										</xsl:if>
									</xsl:otherwise>
								</xsl:choose>

							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:copy-of select="canvas:salepropertyrelationscheckcreate(normalize-space($currentItemXML/SaleNumber),$mappingElementstoFeatures,$currentItemXML)" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="exists($currentItemXML/ItemIntention)">
						<xsl:variable name="itemIntentionValue" select="normalize-space($currentItemXML/ItemIntention)"/>
						<xsl:if test="$itemIntentionValue eq 'X' or $itemIntentionValue eq 'UNO' or $itemIntentionValue eq 'XAR' or $itemIntentionValue eq 'XPS'">
							<xsl:attribute name="wf_id" select="'20000'"/>
							<xsl:attribute name="wf_step" select="'902'"/>
							<xsl:copy-of select="canvas:salepropertyremoverelation($propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber'],$propertyAsset/@id)" />
							<xsl:copy-of select="canvas:propertysalelayoutremoverelation($propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber'],$propertyAsset)" />
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="exists($propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber'])">
							<xsl:variable name="elementName" select="normalize-space(name($currentItemXML/SaleNumber))"/>
							<xsl:attribute name="wf_id" select="'20000'"/>
							<xsl:attribute name="wf_step" select="'901'"/>
							<excludeElements feature="'canvas:jdeproperty.salenumber'" />
							<xsl:copy-of select="canvas:propertysalelayoutremoverelation($propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber']/@value_long,$propertyAsset)" />
							<xsl:copy-of select="canvas:salepropertyremoverelation($propertyAsset/asset_feature[@feature='canvas:jdeproperty.salenumber']/@value_long,$propertyAsset/@id)" />
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				
			</xsl:otherwise>
		</xsl:choose>
	
	</xsl:function>

	
<!-- ############# -->
	<xsl:function name="canvas:salepropertyrelationscheckcreate">
		<xsl:param name="saleNumber" />
		<xsl:param name="mappingElementstoFeatures" />
		<xsl:param name="currentItemXML" />
		
		<xsl:variable name="currentSaleAssetXML" select="cs:asset()[@censhare:asset.id_extern=concat('sale:',$saleNumber) and @censhare:asset.currversion=0]"/>
		
			<xsl:choose>
				<xsl:when test="normalize-space($saleNumber)">

					<xsl:variable name="saleAssetID">
						<xsl:choose>
							<xsl:when test = "not($currentSaleAssetXML)">
								<xsl:value-of select="canvas:salepropertyrelations($saleNumber,normalize-space($currentItemXML/SaleSite))" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$currentSaleAssetXML/@id" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

					<xsl:if test="normalize-space($saleAssetID)">

						<xsl:attribute name="wf_id" select="'20000'"/>
						<xsl:attribute name="wf_step" select="''"/>
						<parent_asset_rel parent_asset="{$saleAssetID}" key="target.">
							<xsl:if test="exists($currentItemXML/LotNumber)">
								<asset_rel_feature>
									<xsl:attribute name="feature" select="normalize-space($mappingElementstoFeatures//LotNumber/id)"/>
									<xsl:attribute name="{$mappingElementstoFeatures//LotNumber/valueType}" select="normalize-space($currentItemXML/LotNumber)"/>
								</asset_rel_feature>
							</xsl:if>
							<xsl:if test="exists($currentItemXML/LotSuffix)">
								<asset_rel_feature>
									<xsl:attribute name="feature" select="normalize-space($mappingElementstoFeatures//LotSuffix/id)"/>
									<xsl:attribute name="{$mappingElementstoFeatures//LotSuffix/valueType}" select="normalize-space($currentItemXML/LotSuffix)"/>
								</asset_rel_feature>
							</xsl:if>
						</parent_asset_rel>

					</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="wf_id" select="'20000'"/>
					<xsl:attribute name="wf_step" select="'901'"/>
				</xsl:otherwise>
			</xsl:choose>
	</xsl:function>


<!-- ################ -->
    <xsl:function name="canvas:salepropertyrelations">
        <xsl:param name="saleNumber"/>
        <xsl:param name="saleSite"/>

        <xsl:choose>
					<xsl:when test="$saleNumber">
						<cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="resultAssetXml">
							<cs:param name="source">
								<asset name="{$saleSite}{$saleNumber}" domain="root.christiesinternational.public." domain2="root.christiesinternational.emeri." type="sale." id_extern="sale:{$saleNumber}">
								</asset>
							</cs:param>
						</cs:command>
						<xsl:value-of select="$resultAssetXml/@id" />
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
        </xsl:choose>

    </xsl:function>


<!-- #################### -->
	<xsl:function name="canvas:propertysalelotdefinitions">
		<xsl:param name="lotInfo" />
		<xsl:param name="lotMappingID" />
		<xsl:param name="lotMappingValueType" />
		<xsl:param name="propertyAssetXML" />
		<xsl:param name="saleAssetXML" />
		
		<xsl:choose>
			<xsl:when test="normalize-space($propertyAssetXML/parent_asset_rel[@parent_asset=$saleAssetXML/@id]/asset_rel_feature[@feature=$lotMappingID]/@*[local-name()=$lotMappingValueType])">
				<xsl:if test="normalize-space($lotInfo) != $propertyAssetXML/parent_asset_rel[@parent_asset=$saleAssetXML/@id]/asset_rel_feature[@feature=$lotMappingID]/@*[local-name()=$lotMappingValueType]">
					<xsl:copy-of select="canvas:salepropertyupdatefeaturerelations($saleAssetXML,$propertyAssetXML/@id,normalize-space($lotMappingID),$lotMappingValueType,normalize-space($lotInfo))" />
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="canvas:salepropertyupdatefeaturerelations($saleAssetXML,$propertyAssetXML/@id,normalize-space($lotMappingID),$lotMappingValueType,normalize-space($lotInfo))" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:function>


<!-- ################# -->
	<xsl:function name="canvas:propertysalelayoutremoverelation">
		<xsl:param name="saleID" />
		<xsl:param name="propertyAssetXML" />
		
		<xsl:variable name="saleAssetXML" select="cs:asset()[@censhare:asset.id_extern=concat('sale:',$saleID)]" />
		<xsl:variable name="propertyID" select="$propertyAssetXML/@id" />

		<xsl:for-each select="$saleAssetXML/cs:parent-rel()[@key='user.']">
			<xsl:if test="@type='issue.section.'">
				<xsl:variable name="parentAssetXML" select="." />

				<xsl:if test="exists($parentAssetXML/cs:child-rel()[@key='user.'])">
					<xsl:for-each select="$parentAssetXML/cs:child-rel()[@key='user.']">
						<xsl:if test="@type='layout.'">
							<xsl:variable name="layoutAssetXML" select="." />

								<xsl:if test="$layoutAssetXML/child_asset_rel[@child_asset=$propertyID and @key='actual.']">

									<excludeElements>
										<parent_asset_rel parent_asset="{$layoutAssetXML/@id}" child_asset="{$propertyID}" key="actual." />
									</excludeElements>
									<excludeElements>
										<parent_asset_element_rel parent_asset="{$layoutAssetXML/@id}" child_asset="{$propertyID}" key="actual." />
									</excludeElements>
									
									<xsl:for-each select="$propertyAssetXML/parent_asset_rel[@parent_asset=$layoutAssetXML/@id and @key='actual.']">
										<parent_asset_rel>
											<xsl:copy-of select="./@*"/>
											<xsl:attribute name="iscancellation" select="'1'"/>
										</parent_asset_rel>
									</xsl:for-each>
									<xsl:for-each select="$propertyAssetXML/parent_asset_element_rel[@parent_asset=$layoutAssetXML/@id and @key='actual.']">
										<parent_asset_element_rel>
											<xsl:copy-of select="./@*"/>
											<xsl:attribute name="iscancellation" select="'1'"/>
										</parent_asset_element_rel>
									</xsl:for-each>

									<!--cs:command name="com.censhare.api.assetmanagement.Update" returning="updatedXML">
										<cs:param name="source">
											<asset id="{$layoutAssetXML/@id}">
												<xsl:copy-of select="$layoutAssetXML/@*"/>
												<xsl:copy-of select="$layoutAssetXML/node() except ($layoutAssetXML/child_asset_rel[@child_asset=$propertyID and @key='actual.'],$layoutAssetXML/child_asset_element_rel[@child_asset=$propertyID and @key='actual.'])"/>

												<xsl:for-each select="$layoutAssetXML/child_asset_rel[@child_asset=$propertyID and @key='actual.']">
													<xsl:element name="child_asset_rel">
						        				        <xsl:copy-of select="./@*"/>
														<xsl:attribute name="iscancellation" select="'1'"/>
													</xsl:element>
												</xsl:for-each>
												<xsl:for-each select="$layoutAssetXML/child_asset_element_rel[@child_asset=$propertyID and @key='actual.']">
													<xsl:element name="child_asset_element_rel">
						        				        <xsl:copy-of select="./@*"/>
														<xsl:attribute name="iscancellation" select="'1'"/>
													</xsl:element>
												</xsl:for-each>

											</asset>
										</cs:param>
									</cs:command-->  
									
								</xsl:if>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>

			</xsl:if>
		</xsl:for-each>

	</xsl:function>

<!-- ################################## -->
	<xsl:function name="canvas:propertyitemintentiondefinitions">
		<xsl:param name="itemIntentionValue" />
		<xsl:param name="saleID" />
		<xsl:param name="propertyAssetXML" />
		
		<xsl:variable name="propertyID" select="$propertyAssetXML/@id"/>

		<xsl:choose>
			<xsl:when test="$itemIntentionValue eq 'X' or $itemIntentionValue eq 'UNO' or $itemIntentionValue eq 'XAR' or $itemIntentionValue eq 'XPS'">
				<xsl:attribute name="wf_id" select="'20000'"/>
				<xsl:attribute name="wf_step" select="'902'"/>
				<xsl:copy-of select="canvas:propertysalelayoutremoverelation($saleID,$propertyAssetXML)" />
				<xsl:copy-of select="canvas:salepropertyremoverelation($saleID,$propertyID)" />
			</xsl:when>
			<xsl:when test="$itemIntentionValue eq 'WDR'">
				<xsl:attribute name="wf_id" select="'20000'"/>
				<xsl:attribute name="wf_step" select="'999'"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="wf_id" select="'20000'"/>
				<xsl:attribute name="wf_step" select="''"/>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:function>


<!-- ################################## -->
	<xsl:function name="canvas:salepropertyremoverelation">
		<xsl:param name="saleID" />
		<xsl:param name="propertyID" />
		
		<xsl:variable name="saleAssetXML" select="cs:asset()[@censhare:asset.id_extern=concat('sale:',$saleID)]" />

		<excludeElements>
			<parent_asset_rel parent_asset="{$saleAssetXML/@id}" child_asset="{$propertyID}" key="target." />
		</excludeElements>
		<excludeElements feature="'canvas:jdeproperty.salenumber'" />

						<!--xsl:message>
							<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
							<xsl:copy-of select="$saleAssetXML" />
							<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
						</xsl:message>

		<cs:command name="com.censhare.api.assetmanagement.Update" returning="saleUpdateXML">
			<cs:param name="source">
				<asset id="{$saleAssetXML/@id}">
					<xsl:copy-of select="$saleAssetXML/@*"/>
					<xsl:copy-of select="$saleAssetXML/node() except (($saleAssetXML/parent_asset_rel[@child_asset=$propertyID and @key='target.']/asset_rel_feature), ($saleAssetXML/child_asset_rel[@key='target.' and @child_asset=$propertyID]))"/>
				</asset>
			</cs:param>
		</cs:command-->  

	</xsl:function>


<!-- ################################## -->
	<xsl:function name="canvas:salepropertyupdatefeaturerelations">
		<xsl:param name="saleAssetXML" />
		<xsl:param name="propertyID" />
		<xsl:param name="featureID" />
		<xsl:param name="featureType" />
		<xsl:param name="featureValue" />

		<!--xsl:variable name="saleAssetXMLUpdated" select="(cs:asset('sql=true')[@censhare:asset.id_extern=$saleAssetXML/@id_extern])[1]" /-->
		<!--xsl:variable name="saleAssetXMLUpdated" select="(cs:asset()[@censhare:asset.id_extern=$saleAssetXML/@id_extern])[1]" /-->
		
		<!--xsl:variable name="saleAssetXML" select="$saleID/@id" /-->

				<!--xsl:message>
						<xsl:value-of select="'&#xA;''&#xA;''&#xA;'" />
					<salepropertyupdatefeaturerelations>
						<xsl:copy-of select="$saleAssetXML/child_asset_rel[@child_asset=$propertyID and @key='target.']/asset_rel_feature[@feature=$featureID]"/>
					</salepropertyupdatefeaturerelations>
						<xsl:value-of select="'&#xA;''&#xA;''&#xA;'" />
				</xsl:message-->

				<!--xsl:message>
						<xsl:value-of select="'&#xA;''&#xA;''&#xA;'" /-->
		
		<!--cs:command name="com.censhare.api.assetmanagement.Update" returning="saleUpdateXML">
			<cs:param name="source">
				<asset id="{$saleAssetXML/@id}">
					<xsl:copy-of select="$saleAssetXML/@*"/>
					<xsl:copy-of select="$saleAssetXML/node() except ($saleAssetXML/child_asset_rel[@child_asset=$propertyID and @key='target.'])"/>
					

					<child_asset_rel child_asset="{$propertyID}" key='target.'>

					<xsl:choose>

						<xsl:when test="$saleAssetXMLUpdated/child_asset_rel[@child_asset=$propertyID and @key='target.']/asset_rel_feature[@feature=$featureID]">

						<xsl:for-each select="$saleAssetXMLUpdated/child_asset_rel[@child_asset=$propertyID and @key='target.']/asset_rel_feature">

							<xsl:choose>
								<xsl:when test="$featureID = ./@feature">
									<asset_rel_feature feature="{$featureID}">
										<xsl:attribute name="{$featureType}">
											<xsl:value-of select="$featureValue" />
										</xsl:attribute>
									</asset_rel_feature>
								</xsl:when>
								<xsl:when test="$featureID != ./@feature">
									<xsl:copy-of select="." />
								</xsl:when>
							</xsl:choose>

						</xsl:for-each>

						</xsl:when>
						
						<xsl:otherwise>

									<asset_rel_feature feature="{$featureID}">
										<xsl:attribute name="{$featureType}">
											<xsl:value-of select="$featureValue" />
										</xsl:attribute>
									</asset_rel_feature>
						<xsl:for-each select="$saleAssetXMLUpdated/child_asset_rel[@child_asset=$propertyID and @key='target.']/asset_rel_feature">
							<xsl:copy-of select="." />
						</xsl:for-each>
						
						</xsl:otherwise>

					</xsl:choose>


					</child_asset_rel>

				</asset>
			</cs:param>
		</cs:command-->
						<!--xsl:value-of select="'&#xA;''&#xA;''&#xA;'" />
				</xsl:message-->

<!--xsl:message>
<saleUpdateXML>
	<xsl:copy-of select="$saleUpdateXML" />
</saleUpdateXML>
</xsl:message-->

	</xsl:function>


</xsl:stylesheet>
