<?xml version="1.0" encoding="UTF-8"?>

<!-- 
#########################################################
censhare standard content Layout Transformation

Version 1.0 - Mai 14th 2013 
#########################################################
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:fn="http://www.w3.org/2005/xpath-functions" 
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:map="http://ns.censhare.de/mapping"
  xmlns:temp="http://ns.censhare.de/elements"
  xmlns:func="http://ns.censhare.de/functions"
  exclude-result-prefixes="xs xd fn xi cs map temp func"
  version="2.0">
  
  
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- format mappings              -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  
  <xd:doc>
    <xd:desc>
      <xd:p>Mapping of element names to InDesign paragraph styles</xd:p>
      <xd:p><xd:i>If not used, include some empty element</xd:i></xd:p>
      <xd:p>Every entry consists of an <xd:b>&lt;cen-map-entry&gt;</xd:b> element with following attributes:</xd:p>
      <xd:ul>
        <xd:li><xd:b>element</xd:b>: name of the element, or part of hierarchy in the form: ancestor/parent/child</xd:li>
        <xd:li><xd:b>stylename</xd:b>: name of InDesign paragraph style</xd:li>
        <xd:li><xd:b>stylegroup</xd:b>: name of style group, leave empty or omit if style not part of a group</xd:li>
        <xd:li><xd:b>add-charstyle</xd:b>: If not empty, CSMapping will additionally be applied to the matching element. Default: No character style applied</xd:li>
        <xd:li><xd:b>attr-name</xd:b>: name of an attribute which will be also matched</xd:li>
        <xd:li><xd:b>attr-value</xd:b>: value of the attribute (attr-name) which must be matching</xd:li>      
      </xd:ul>
    </xd:desc>
  </xd:doc>
  <xsl:variable name="PSMapping">
    <!-- basic elements -->
    <map:entry element="strapline" stylegroup="" stylename="strapline" />
    <map:entry element="title" stylegroup="" stylename="Title" />
    <map:entry element="firstline" stylegroup="" stylename="First Line" />
    <map:entry element="subtitle" stylegroup="" stylename="subtitle" />
    <map:entry element="paragraph" stylegroup="" stylename="paragraph" />
    <map:entry element="hdivider" stylegroup="" stylename="hdivider" />
    <map:entry element="intro" stylegroup="" stylename="intro" />
    <map:entry element="subheadline-1" stylegroup="" stylename="subheadline-1" />
    <map:entry element="subheadline-2" stylegroup="" stylename="subheadline-2" />
    <map:entry element="subheadline-3" stylegroup="" stylename="subheadline-3" />
    <map:entry element="pre" stylegroup="" stylename="pre" />
    <map:entry element="enumeration/item/paragraph" stylegroup="" stylename="enumeration" />
    <map:entry element="enumeration/item/enumeration/item/paragraph" stylegroup="" stylename="enumeration level2" />
    <map:entry element="bullet-list/item/enumeration/item/paragraph" stylegroup="" stylename="enumeration level2" />
    <map:entry element="bullet-list/item/paragraph" stylegroup="" stylename="bullet-list" />
    <map:entry element="bullet-list/item/bullet-list/item/paragraph" stylegroup="" stylename="bullet-list level2" />
    <map:entry element="enumeration/item/bullet-list/item/paragraph" stylegroup="" stylename="bullet-list level2" />
    
    <!-- tables -->
    <map:entry element="table/row/cell/paragraph" stylegroup="table" stylename="paragraph" />
    <map:entry element="table/row/cell/subheadline-1" stylegroup="table" stylename="paragraph" />
    <map:entry element="table/row/cell/subheadline-2" stylegroup="table" stylename="paragraph" />
    <map:entry element="table/row/cell/subheadline-3" stylegroup="table" stylename="paragraph" />
    <map:entry element="table/row/cell/enumeration/item/paragraph" stylegroup="table" stylename="enumeration" />
    <map:entry element="table/row/cell/bullet-list/item/paragraph" stylegroup="table" stylename="bullet-list" />
    <map:entry element="table/table-caption" stylegroup="table" stylename="caption" />

    <!-- imagebox -->
    <map:entry element="image-box/caption/paragraph" stylegroup="image-caption" stylename="caption" />
    <map:entry element="image-box/caption/enumeration/item/paragraph" stylegroup="image-caption" stylename="enumeration" />
    <map:entry element="image-box/caption/bullet-list/item/paragraph" stylegroup="image-caption" stylename="bullet-list" />
    
    <!-- other -->
    <map:entry element="question/paragraph" stylegroup="interview" stylename="question" />
    <map:entry element="answer/paragraph" stylegroup="interview" stylename="answer" />
    <map:entry element="callout/paragraph" stylegroup="callout" stylename="paragraph" />
    <map:entry element="group/paragraph" stylegroup="group" stylename="paragraph" />
    
    
    <!-- Christie's Phase 2 Mapping -->
    <map:entry element="prelot/content/paragraph" stylegroup="" stylename="Pre Lot Text"/>
    <map:entry element="subheadline-1" stylegroup="" stylename="Notes" attr-name="type" attr-value="provenance"/>
    <map:entry element="paragraph" stylegroup="" stylename="Notes Text" attr-name="type" attr-value="provenance"/>
    <map:entry element="subheadline-1" stylegroup="" stylename="Notes" attr-name="type" attr-value="exhibited"/>
    <map:entry element="paragraph" stylegroup="" stylename="Notes Text" attr-name="type" attr-value="exhibited"/>
    <map:entry element="subheadline-1" stylegroup="" stylename="Notes" attr-name="type" attr-value="literature"/>
    <map:entry element="paragraph" stylegroup="" stylename="Notes Text" attr-name="type" attr-value="literature"/>
    <map:entry element="subheadline-1" stylegroup="" stylename="Notes" attr-name="type" attr-value="engraved"/>
    <map:entry element="paragraph" stylegroup="" stylename="Notes Text" attr-name="type" attr-value="engraved"/>
    <map:entry element="paragraph" stylegroup="" stylename="Footnotes" attr-type="type" attr-value="footnotes"/>
    <map:entry element="paragraph" stylegroup="" stylename="Post Lot Text" attr-name="type" attr-value="postlot"/>
    <map:entry element="paragraph" stylegroup="" stylename="Heading" attr-name="type" attr-value="heading"/>
    <map:entry element="paragraph" stylegroup="" stylename="Subheading" attr-name="type" attr-value="subheding"/>
    
    <!-- Christie's Phase 2 Mapping Scott -->
    <map:entry element="prelot" stylegroup="" stylename="Pre Lot Text"/>
    <map:entry element="lot_number" stylegroup="" stylename="Lot Number"/>
    <map:entry element="makerdate" stylegroup="" stylename="Maker &amp; Date"/>
    <map:entry element="artistdate" stylegroup="" stylename="Artist &amp; Date"/>
    <map:entry element="details" stylegroup="" stylename="Details"/>
    <map:entry element="size" stylegroup="" stylename="Size"/>
    <map:entry element="marked" stylegroup="" stylename="Marked"/>
    <map:entry element="otherdetails" stylegroup="" stylename="Other Details"/>
    <map:entry element="quantity" stylegroup="" stylename="Quantity"/>
    <map:entry element="estimates" stylegroup="" stylename="Price 3"/>
    <map:entry element="provenance" stylegroup="" stylename="paragraph"/>
    <map:entry element="exhibited" stylegroup="" stylename="paragraph"/>
    <map:entry element="literature" stylegroup="" stylename="paragraph"/>
    <map:entry element="engraved" stylegroup="" stylename="paragraph"/>
    <map:entry element="cataloguenotes" stylegroup="" stylename="Cat Text"/>
    <map:entry element="postlottext" stylegroup="" stylename="Post Lot Text"/>
    <map:entry element="signature" stylegroup="" stylename="Signature"/>
    <map:entry element="certificate" stylegroup="" stylename="Certificate"/>
    <map:entry element="picturemedium" stylegroup="" stylename="Picture Medium"/>
    <map:entry element="picturesize" stylegroup="" stylename="Picture Size"/>
    <map:entry element="quantitydesc" stylegroup="" stylename="Quantity Description"/>
    <map:entry element="weight" stylegroup="" stylename="Weight"/>
    <map:entry element="symbolconverted" stylegroup="" stylename="Lot Number"/>
    
  </xsl:variable>

  <xd:doc>
    <xd:desc>
      <xd:p>Mapping of element names to InDesign character styles</xd:p>
      <xd:p><xd:i>If not used, include some empty element</xd:i></xd:p>
      <xd:p>Every entry consists of an <xd:b>&lt;cen-map-entry&gt;</xd:b> element with following attributes:</xd:p>
      <xd:ul>
        <xd:li><xd:b>element</xd:b>: name of the element, or part of hierarchy in the form: ancestor/parent/child</xd:li>
        <xd:li><xd:b>stylename</xd:b>: name of InDesign paragraph style</xd:li>
        <xd:li><xd:b>stylegroup</xd:b>: name of style group, leave empty or omit if style not part of a group</xd:li>
        <xd:li><xd:b>attr-name</xd:b>: name of an attribute which will be also matched</xd:li>
        <xd:li><xd:b>attr-value</xd:b>: value of the attribute (attr-name) which must be matching</xd:li>      
      </xd:ul>
    </xd:desc>
  </xd:doc>
  <xsl:variable name="CSMapping">
    <map:entry element="bold" stylegroup="" stylename="bold" />
    <map:entry element="italic" stylegroup="" stylename="italic" />
    <map:entry element="bold-italic" stylegroup="" stylename="bold-italic" />
    <map:entry element="marker" attr-name="semantics" attr-value="emphasized" stylegroup="" stylename="em" />
    <map:entry element="marker" attr-name="semantics" attr-value="strong" stylegroup="" stylename="strong" />
    <map:entry element="marker" attr-name="semantics" attr-value="small" stylegroup="" stylename="small" />
    <map:entry element="person" stylegroup="interview" stylename="person" />
    <map:entry element="underline" stylegroup="" stylename="underline" />
    <map:entry element="sub" stylegroup="" stylename="sub" />
    <map:entry element="sup" stylegroup="" stylename="sup" />
    <map:entry element="link" stylegroup="" stylename="link" />
    
    <!-- Christie's Phase 2 Mapping -->
    <map:entry element="estimates" stylegroup="Price 3" stylename="Price 3"/>
    <map:entry element="lot_symbol" stylegroup="" stylename="Lot Symbol"/>
    <map:entry element="lot_symbol_z" stylegroup="Lot Number" stylename="Lot Number&lt;z&gt;"/>
    <map:entry element="lot_symbol_s" stylegroup="Lot Number" stylename="Lot Number&lt;s&gt;"/>
    <map:entry element="lot_number" stylegroup="Lot Number" stylename="Lot Number"/>
    <map:entry element="lot_suffix" stylegroup="" stylename="Lot Number"/>
    <map:entry element="quantity" stylegroup="" stylename="Quantity" />
    <map:entry element="pad" stylegroup="" stylename="Pad" />
    <map:entry element="prelot" stylegroup="Pre Lot Text" stylename="Pre Lot Text" />
    <map:entry element="makerdate" stylegroup="Maker &amp; Date" stylename="Maker &amp; Date"/>
    <map:entry element="marked" stylegroup="Marked" stylename="Marked"/>
    <map:entry element="artistdate" stylegroup="Artis &amp; Date" stylename="Artist &amp; Date"/>
    <map:entry element="title" stylegroup="Title" stylename="Title" />
    <map:entry element="details" stylegroup="Details" stylename="Details" />
    <map:entry element="signature" stylegroup="Signature" stylename="Signature" />
    <map:entry element="certificate" stylegroup="Certificate" stylename="Certificate" />
    <map:entry element="otherdetails" stylegroup="Other Details" stylename="Other Details"/>
    <map:entry element="picturemedium" stylegroup="Picture Medium" stylename="Picture Medium"/>
    <map:entry element="picturesize" stylegroup="Picture Size" stylename="Picture Size"/>
    <map:entry element="quantity" stylegroup="Quantity" stylename="Quantity"/>
    <map:entry element="quantitydesc" stylegroup="Quantity Description" stylename="Quantity Description"/>
    <map:entry element="size" stylegroup="Size" stylename="Size"/>
    <map:entry element="symbolconverted" stylegroup="Lot Number" stylename="Lot Number"/>
    <map:entry element="weight" stylegroup="Weight" stylename="Weight"/>
    
  </xsl:variable>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Mapping of element names to RexExp replacemnts</xd:p>
      <xd:p><xd:i>If not used, include some empty element</xd:i></xd:p>
      <xd:p>Unlike <xd:ref name="PSMapping" type="variable"/> and <xd:ref name="CSMapping" type="variable"/> children are also matching, so replacements are applied to all subelements, which don’t match another entry</xd:p>
      <xd:p>Every entry consists of an <xd:b>&lt;cen-map-entry&gt;</xd:b> element with following attributes:</xd:p>
      <xd:ul>
        <xd:li><xd:b>element</xd:b>: name of the element, or part of hierarchy in the form: ancestor/parent/child</xd:li>
        <xd:li><xd:b>search</xd:b>: RegEx for search</xd:li>
        <xd:li><xd:b>replace</xd:b>: RegEx for replacement</xd:li>
        <xd:li><xd:b>flags</xd:b>: optional RegEx flags (f.e.: "i" for case-insensitive search)</xd:li>
      </xd:ul>
      <xd:p><xd:b>IMPORTANT:</xd:b> mask { and } as  {{ and }}</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:variable name="Replacements">
    <map:entry/>
  </xsl:variable>
  
  
  
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  <!-- end of format mappings       -->
  <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
  
</xsl:stylesheet>
