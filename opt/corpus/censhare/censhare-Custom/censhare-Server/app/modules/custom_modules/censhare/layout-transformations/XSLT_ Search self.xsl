<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:my="http://www.censhare.com/my">

  <xsl:param name="transform"/>

  <!-- Defines a search asset with a result of the given asset (self) -->
  <xsl:template match="asset">
    <assets>
      <!-- debug -->
      <xsl:message><xsl:value-of select="concat('call ''censhare:search-self'' with asset ''', @name, ''' (ID: ', @id, ')')"/></xsl:message>
      <!-- search -->
      <xsl:copy-of select="."/>
    </assets>
  </xsl:template>

</xsl:stylesheet>
