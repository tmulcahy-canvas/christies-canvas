<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">

  <!-- Search from the current asset all child user. related text assets sorted by name -->
  <xsl:template match="/">
    <query>
      <and>
        <condition name="censhare:asset.type" value="text.*"/>
        <relation direction="parent" type="user.*">
          <target>
            <and>
              <condition name="censhare:asset.id" op="=" value="{asset/@id}"/>
            </and>
          </target>
        </relation>
      </and>
      <sortorders>
        <grouping mode="none"/>
        <order ascending="true" by="censhare:asset.name"/>
      </sortorders>
    </query>
  </xsl:template>

</xsl:stylesheet>
