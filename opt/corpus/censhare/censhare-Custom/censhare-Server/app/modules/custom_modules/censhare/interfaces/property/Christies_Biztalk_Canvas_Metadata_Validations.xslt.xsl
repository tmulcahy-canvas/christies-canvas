<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:canvas="censhare.com"
    xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/ObjectDataRequest"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
		xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
		xmlns:csc="http://www.censhare.com/censhare-custom"
		xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
		xmlns:xmlutildom="java:com.censhare.support.xml.CXmlUtilDOM"
		xmlns:xmlutil="java:com.censhare.support.xml.CXmlUtil"
		exclude-result-prefixes="#all">

    <!--xsl:output indent="yes" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/-->

		<xsl:character-map name="brackets">
			<xsl:output-character character="&lt;" string="&lt;"/>
			<xsl:output-character character=">" string="&gt;"/>
		</xsl:character-map>

    <!-- =========================== -->
    <!-- Functions for Validations  -->
    <!-- =========================== -->

    <xsl:function name="canvas:featureValuesUpdatesInMasterData">
		<xsl:param name="elementName"/>
		<xsl:param name="featureValue"/>
		<xsl:param name="mappingElementstoFeatures"/>

		<xsl:variable name="featureID" select="normalize-space($mappingElementstoFeatures//*[local-name()=$elementName]/id)" />

		<xsl:if test="not(cs:master-data('feature_value')[@feature=$featureID and @value_key=$featureValue])">
			<xsl:variable name="masterdataXml">
				<feature_value feature="{$mappingElementstoFeatures//*[local-name()=$elementName]/id}" domain="root." domain2="root." is_hierarchical="0" name="{$featureValue}" sorting="0" enabled="1">
					<xsl:attribute name="{$mappingElementstoFeatures//*[local-name()=$elementName]/valueType}" select="$featureValue"/>
				</feature_value>
			</xsl:variable>

			<!-- Create master-data -->
			<xsl:variable name="command-xml">
				<cmd>
					<xml-info title="my-create-master-data" locale="__ALL" />
					<cmd-info name="admin.export_import.xml-data-import" />
					<commands currentstep="0">
						<command method="importXml" scriptlet="modules.admin.export_import.XmlDataImport" target="ScriptletManager" />
					</commands>
					<content>
						<xsl:copy-of select="$masterdataXml" />
					</content>
					<param to-do="both" />
				</cmd>
			</xsl:variable>

			<cs:command name="com.censhare.api.ExecuteCommand.execute">
				<cs:param name="command" select="$command-xml" />
			</cs:command>

		</xsl:if>

	</xsl:function>


	<!-- ############### -->
	<xsl:function name="canvas:currencyMetadataGenerator">
		<xsl:param name="currencyCode" />
		<xsl:param name="configXML" />
		<xsl:param name="configXMLAssetXML" />
		
		<xsl:variable name="estimatecurrencyunit" select="concat('canvas:jdeproperty.estimatecurrency',$currencyCode)" />
		<xsl:variable name="estimatehighunit" select="concat('canvas:jdeproperty.estimatehigh',$currencyCode)" />
		<xsl:variable name="estimatelowunit" select="concat('canvas:jdeproperty.estimatelow',$currencyCode)" />
		<xsl:variable name="parentestimatecurrency" select="concat('canvas:jdeproperty.estimate',$currencyCode)" />

		<xsl:variable name="featureID" select="'canvas:jdeproperty.propertycurrencycode'"/>
		<xsl:variable name="featureValue" select="$currencyCode"/>

		<xsl:choose>	
			<xsl:when test="not(cs:master-data('feature_value')[@feature=$featureID and @value_key=$featureValue])">
				<xsl:variable name="masterdataXml">
					<feature_value feature="{$featureID}" domain="root." domain2="root." is_hierarchical="0" name="{$featureValue}" sorting="0" enabled="1">
						<xsl:attribute name="value_key" select="$featureValue"/>
					</feature_value>
					<feature language_type="0" domain="root." domain2="root." enabled="1" isassetinfo="2" issearchable="1" key="{$estimatecurrencyunit}" name="Estimate Currency {$currencyCode}" sorting="1" target_object_type="asset" trait="product" trait_property="estimateCurrency{$currencyCode}" trait_property_top_level="1" type="censhare:asset-feature" user_modifiable="1" value_type="4"/>
					<feature language_type="0" domain="root." domain2="root." enabled="1" isassetinfo="2" issearchable="1" key="{$estimatehighunit}" name="Estimate High {$currencyCode}" sorting="1"  target_object_type="asset" trait="product" trait_property="estimatehigh{$currencyCode}" trait_property_top_level="1" type="censhare:asset-feature" user_modifiable="1" value_type="3"/>
					<feature language_type="0" domain="root." domain2="root." enabled="1" isassetinfo="2" issearchable="1" key="{$estimatelowunit}" name="Estimate Low {$currencyCode}" sorting="1" target_object_type="asset" trait="product" trait_property="estimateLow{$currencyCode}" trait_property_top_level="1" type="censhare:asset-feature" user_modifiable="1" value_type="3"/>
					<feature language_type="0" key="{$parentestimatecurrency}" type="censhare:asset-feature" domain="root." domain2="root." name="Estimate {$currencyCode}" value_type="0" issearchable="1" sorting="1" enabled="1" target_object_type="asset" trait="product" trait_property="estimate{$currencyCode}" trait_property_top_level="1" user_modifiable="1">
						<feature_rel parent_feature="{$parentestimatecurrency}" child_feature="{$estimatecurrencyunit}" enabled="1" />
						<feature_rel parent_feature="{$parentestimatecurrency}" child_feature="{$estimatehighunit}" enabled="1" />
						<feature_rel parent_feature="{$parentestimatecurrency}" child_feature="{$estimatelowunit}" enabled="1" />
					</feature>
				</xsl:variable>

				<!-- Create master-data -->
				<xsl:variable name="command-xml">
					<cmd>
						<xml-info title="my-create-master-data" locale="__ALL" />
						<cmd-info name="admin.export_import.xml-data-import" />
						<commands currentstep="0">
							<command method="importXml" scriptlet="modules.admin.export_import.XmlDataImport" target="ScriptletManager" />
						</commands>
						<content>
							<xsl:copy-of select="$masterdataXml" />
						</content>
						<param to-do="both" />
					</cmd>
				</xsl:variable>

				<cs:command name="com.censhare.api.ExecuteCommand.execute">
					<cs:param name="command" select="$command-xml" />
				</cs:command>
			
				<xsl:copy-of select="canvas:updateXMLConfigFile($currencyCode,$configXML,$configXMLAssetXML,$estimatecurrencyunit,$estimatehighunit,$estimatelowunit,$parentestimatecurrency)"/>
			
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="$configXML" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:function>
	

	<!-- ############### -->
	<xsl:function name="canvas:updateXMLConfigFile">
		<xsl:param name="currencyCode" />
		<xsl:param name="configXML" />
		<xsl:param name="configXMLAssetXML" />
		<xsl:param name="estimatecurrencyunit" />
		<xsl:param name="estimatehighunit" />
		<xsl:param name="estimatelowunit" />
		<xsl:param name="parentestimatecurrency" />

		<xsl:variable name="textContent">
		<MetadataConfigs><Item>
			<xsl:copy-of copy-namespaces="no" select="$configXML/MetadataConfigs/Item/*"/>
			<xsl:element name="EstimateCurrency{$currencyCode}">
				<id><xsl:value-of select="$estimatecurrencyunit" /></id>
				<valueType>value_string</valueType>
				<parentFeature><xsl:value-of select="$parentestimatecurrency" /></parentFeature>
			</xsl:element>
			<xsl:element name="EstimateHigh{$currencyCode}">
				<id><xsl:value-of select="$estimatehighunit" /></id>
				<valueType>value_long</valueType>
				<parentFeature><xsl:value-of select="$parentestimatecurrency" /></parentFeature>
			</xsl:element>
			<xsl:element name="EstimateLow{$currencyCode}">
				<id><xsl:value-of select="$estimatelowunit" /></id>
				<valueType>value_long</valueType>
				<parentFeature><xsl:value-of select="$parentestimatecurrency" /></parentFeature>
			</xsl:element>
		</Item></MetadataConfigs>
		</xsl:variable>

		<!--xsl:variable name="configXMLAsset" select="cs:get-asset-resources(asset)[@censhare:resource-key='christies:jdeinterfacebiztalkcanvaselementtofeaturemapping']" />
		<xsl:variable name="configXMLAssetXML" select="cs:get-asset($configXMLAsset/@asset_id)" /-->

		<cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
		<xsl:variable name="textFileName" select="concat($out, concat('genericConfigXMLFile','.xml'))"/>

		<cs:command name="com.censhare.api.io.WriteXML">
			<cs:param name="source" select="$textContent"/>
			<cs:param name="dest" select="$textFileName"/>
			<cs:param name="output">
				<output indent="yes"/>
			</cs:param>
		</cs:command>

		<!--cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="checkoutassetxml">
			<cs:param name="source">
				<asset id="{$configXMLAssetXML/@id}" currversion="0">
				</asset>
			</cs:param>
		</cs:command-->

		<!--cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="NewModell">
			<cs:param name="source"-->
		<cs:command name="com.censhare.api.assetmanagement.Update" returning="resultAssetXml">
			<cs:param name="source">
				<asset>
					<xsl:copy-of select="$configXMLAssetXML/@*" />
					<!--xsl:copy-of select="$configXMLAssetXML/node() except ($configXMLAssetXML/storage_item[@key='master'])" /-->
					<xsl:copy-of select="$configXMLAssetXML/node() except ($configXMLAssetXML/storage_item[@key='master' and @asset_id=$configXMLAssetXML/@id],$configXMLAssetXML/asset_element[@key='actual.' and @asset_id=$configXMLAssetXML/@id])" />
					<storage_item filesys_name="assets_emeri" key="master" mimetype="text/xml" element_idx="0" corpus:asset-temp-file-url="{$textFileName}"/>
					<asset_element key="actual." idx="0"/>
				</asset>
			</cs:param>
		</cs:command>
		
		<xsl:copy-of select="$textContent" />

    <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="outDir">
      <cs:param name="id" select="$out"/>
    </cs:command>


	</xsl:function>


	<!-- ############### -->
	<xsl:function name="canvas:tagsCanvasMapping">
		<xsl:param name="inputContent" />
		
		<!--replacing "bi" into "bold-italic" tag-->
		<xsl:variable name="replacebiInContent">
			<xsl:copy-of select="replace(replace($inputContent, '&lt;bi&gt;', '&lt;bold-italic&gt;'), '&lt;/bi&gt;', '&lt;/bold-italic&gt;')"></xsl:copy-of>
		</xsl:variable>
		<xsl:variable name="replaceBIInContent">
			<xsl:copy-of select="replace(replace($replacebiInContent, '&lt;BI&gt;', '&lt;bold-italic&gt;'), '&lt;/BI&gt;', '&lt;/bold-italic&gt;')"></xsl:copy-of>
		</xsl:variable>
		<xsl:variable name="replacebandiInContent">
			<xsl:copy-of select="replace(replace($replaceBIInContent, '&lt;b&gt;&lt;i&gt;', '&lt;bold-italic&gt;'), '&lt;/i&gt;&lt;/b&gt;', '&lt;/bold-italic&gt;')"></xsl:copy-of>
		</xsl:variable>
		<xsl:variable name="replaceBandIInContent">
			<xsl:copy-of select="replace(replace($replacebandiInContent, '&lt;B&gt;&lt;I&gt;', '&lt;bold-italic&gt;'), '&lt;/I&gt;&lt;/B&gt;', '&lt;/bold-italic&gt;')"></xsl:copy-of>
		</xsl:variable>
		<xsl:variable name="replaceiandbInContent">
			<xsl:copy-of select="replace(replace($replaceBandIInContent, '&lt;i&gt;&lt;b&gt;', '&lt;bold-italic&gt;'), '&lt;/b&gt;&lt;/i&gt;', '&lt;/bold-italic&gt;')"></xsl:copy-of>
		</xsl:variable>
		<xsl:variable name="replaceIandBInContent">
			<xsl:copy-of select="replace(replace($replaceiandbInContent, '&lt;I&gt;&lt;B&gt;', '&lt;bold-italic&gt;'), '&lt;/B&gt;&lt;/I&gt;', '&lt;/bold-italic&gt;')"></xsl:copy-of>
		</xsl:variable>
		<!--replacing "b" into "bold" tag-->
		<xsl:variable name="replacebInContent">
			<xsl:copy-of select="replace(replace($replaceIandBInContent, '&lt;b&gt;', '&lt;bold&gt;'), '&lt;/b&gt;', '&lt;/bold&gt;')"/>
		</xsl:variable>
		<xsl:variable name="replaceBInContent">
			<xsl:copy-of select="replace(replace($replacebInContent, '&lt;B&gt;', '&lt;bold&gt;'), '&lt;/B&gt;', '&lt;/bold&gt;')"/>
		</xsl:variable>
		<!--replacing "i" into "italic" tag-->
		<xsl:variable name="replaceiInContent">
			<xsl:copy-of select="replace(replace($replaceBInContent, '&lt;i&gt;', '&lt;italic&gt;'), '&lt;/i&gt;', '&lt;/italic&gt;')"/>
		</xsl:variable>
		<xsl:variable name="replaceIInContent">
			<xsl:copy-of select="replace(replace($replaceiInContent, '&lt;I&gt;', '&lt;italic&gt;'), '&lt;/I&gt;', '&lt;/italic&gt;')"/>
		</xsl:variable>
		<!--replacing "u" into "underline" tag-->
		<xsl:variable name="replaceulInContent">
			<xsl:copy-of select="replace(replace($replaceIInContent, '&lt;u&gt;', '&lt;underline&gt;'), '&lt;/u&gt;', '&lt;/underline&gt;')"/>
		</xsl:variable>
		<xsl:variable name="replaceULInContent">
			<xsl:copy-of select="replace(replace($replaceulInContent, '&lt;U&gt;', '&lt;underline&gt;'), '&lt;/U&gt;', '&lt;/underline&gt;')"/>
		</xsl:variable>
		<!--xsl:copy-of select="$contentUnderline"/-->
		<xsl:variable name="replaceAMPinContent">
			<xsl:copy-of select="replace($replaceULInContent, '&amp;amp;', '&amp;')"/>
		</xsl:variable>
		<xsl:variable name="replaceBRinContent">
			<xsl:copy-of select="replace($replaceAMPinContent, '&lt;BR&gt;', '&lt;br/&gt;')"/>
		</xsl:variable>
		<xsl:variable name="tagsConvertedContent">
			<xsl:copy-of select="replace($replaceBRinContent, '&lt;br&gt;', '&lt;br/&gt;')"/>
		</xsl:variable>
		<xsl:copy-of select="$tagsConvertedContent"/>
		
		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
			<xsl:value-of select="$inputContent" />
			<xsl:value-of select="'&#xA;'" />
			<xsl:copy-of select="$tagsConvertedContent"/>
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
		</xsl:message-->
		

						<!--xsl:variable name="textContent">
							<content><xsl:copy-of select="$tagsConvertedContent" /></content>
						</xsl:variable>

								<cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
								<xsl:variable name="textFileName" select="concat($out,'formulatingTextContent.xml')"/>

								<cs:command name="com.censhare.api.io.WriteXML">
									<cs:param name="source" select="$textContent"/>
									<cs:param name="dest" select="$textFileName"/>
									<cs:param name="output">
										<output indent="yes" use-character-maps="brackets"/>
									</cs:param>
								</cs:command>

								<cs:command name="com.censhare.api.io.ReadXML" returning="updatedcontent">
									<cs:param name="source" select="$textFileName"/>
								</cs:command>
								
								<xsl:copy-of select="$updatedcontent/node()" />

    <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="outDir">
      <cs:param name="id" select="$out"/>
    </cs:command-->

								<!--xsl:message>
									<xsl:value-of select="'&#xA;&#xA;#############&#xA;'" />
									<xsl:copy-of select="$updatedcontent/node()" />
									<xsl:value-of select="'&#xA;&#xA;#############&#xA;'" />
								</xsl:message-->


	</xsl:function>


	<!-- VEC 01-24-2019-->
	<!-- =========================== -->
	<!-- Setting Time with Transmission Date to the censhare DateTime Format -->
	<!-- =========================== -->
	<xsl:function name="canvas:setDateforTimeElements">
		<xsl:param name="dateContent"/>
		<xsl:param name="DateTransmission"/>

								<xsl:variable name="dateTimeValue">
									<xsl:choose>
										<xsl:when test="matches(normalize-space($dateContent), '^(\d{6})$')">
											<xsl:value-of select="concat($DateTransmission,'T', substring($dateContent, 1, 2), ':', substring($dateContent, 3, 2), ':', substring($dateContent, 5, 2),'Z')"/>
										</xsl:when>
										<xsl:when test="matches(normalize-space(.), '^(\d{5})$')">
											<xsl:value-of select="concat($DateTransmission,'T',0, substring($dateContent, 1, 1), ':', substring($dateContent, 2, 2), ':', substring($dateContent, 4, 2),'Z')"/>
										</xsl:when>
										<xsl:when test="matches(normalize-space(.), '^(\d{4})$')">
											<xsl:value-of select="concat($DateTransmission,'T', substring($dateContent, 1, 2), ':', substring($dateContent, 3, 2), ':00Z')"/>
										</xsl:when>
										<xsl:when test="not(normalize-space(.))">
											<xsl:value-of select="''"/>
										</xsl:when>
									</xsl:choose>
								</xsl:variable>

		<xsl:value-of select="$dateTimeValue" />

	</xsl:function>


	<!-- =========================== -->
	<!-- Function for date formate conversion into censhare format  -->
	<!-- =========================== -->
	<xsl:function name="canvas:dateFormatChange">
		<xsl:param name="dateString"/>

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; ######################### DATE FORMAT CHANGE'"/>
			<xsl:value-of select="$dateString"/>
			<xsl:value-of select="'&#xA;&#xA;#########################&#xA;'"/>
		</xsl:message-->

		<xsl:choose>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{4})\.(\d{2})\.(\d{2})$')">
				<xsl:value-of select="concat(substring($dateString, 1, 4), '-', substring($dateString, 6, 2), '-', concat(substring($dateString, 9, 2),'T00:00:00Z'))"/>
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{4})/(\d{2})/(\d{2})$')">
				<xsl:value-of select="concat(substring($dateString, 1, 4), '-', substring($dateString, 6, 2), '-', substring($dateString, 9, 2),'T00:00:00Z')"/>
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{8})$')">
				<xsl:value-of select="concat(substring($dateString, 1, 4), '-', substring($dateString, 5, 2), '-', substring($dateString, 7, 2),'T00:00:00Z')"/>
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{6})$')">
				<xsl:value-of select="concat('20', substring($dateString, 1, 2), '-', substring($dateString, 5, 2), '-', substring($dateString, 3, 2),'T00:00:00Z')"/>
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{5})$')">
				<xsl:value-of select="concat('0000-00-00T',0, substring($dateString, 1, 1), ':', substring($dateString, 2, 2), ':', substring($dateString, 4, 2),'Z')"/>
			</xsl:when>
			<xsl:when test="matches(normalize-space($dateString), '^(\d{4})$')">
				<xsl:value-of select="concat('0000-00-00T', substring($dateString, 1, 2), ':', substring($dateString, 3, 2), ':00Z')"/>
			</xsl:when>
			<xsl:when test="not(normalize-space($dateString))">
				<xsl:value-of select="''"/>
			</xsl:when>
		</xsl:choose>
	</xsl:function>

	<!-- =========================== -->
	<!-- Function for date formate conversion into censhare format  -->
	<!-- =========================== -->
	<xsl:function name="canvas:numberFormatChange">
		<xsl:param name="numberToBeFormatted"/>
		<!--xsl:value-of select="translate($numberToBeFormatted, ',', '')"/-->
		<!--xsl:value-of select="$numberToBeFormatted"/-->

        <xsl:choose>
            <xsl:when test="contains($numberToBeFormatted, '.')">
                <xsl:value-of
                    select="substring-before(translate($numberToBeFormatted, ',', ''), '.')"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="translate($numberToBeFormatted, ',', '')"/>
            </xsl:otherwise>
        </xsl:choose>
	</xsl:function>

	<!-- ############## -->	
    <xsl:function name="canvas:textFileContentCheck">
		<xsl:param name="contentValue" as="xs:string"/>
		<xsl:param name="elementName" as="xs:string"/>
		<xsl:param name="characterLimit" as="xs:integer"/>
		<xsl:param name="fileType" as="xs:string"/>
		<xsl:param name="fileName" as="xs:string"/>
		<xsl:param name="propertyIDExt" as="xs:string"/>
		<xsl:param name="propertySINo" as="xs:string"/>
		<xsl:param name="langPref" as="xs:string"/>
	    <xsl:param name="newItemVersionNumber"/>
	    <xsl:param name="mappingElementstoFeatures"/>
        <xsl:param name="propertyAsset"/>
        <xsl:param name="propertyAssetVariantXML"/>

		<xsl:variable name="textIDExtern" select="concat('text:',$propertyIDExt,$elementName,$langPref)" />
		<xsl:variable name="existingTextAsset" select="cs:asset()[@censhare:asset.id_extern=$textIDExtern and @censhare:asset.type=$fileType and @censhare:asset.language=$langPref and @censhare:asset.currversion=0]" />
		<xsl:variable name="confMappingFeature" select="$mappingElementstoFeatures//*[local-name()=$elementName]" />

    	<xsl:choose>
			<xsl:when test="$existingTextAsset and not($existingTextAsset/@checked_out_by)">

		<!--xsl:message>
        <xsl:value-of select="'&#xA;&#xA;&#xA;'" />
        <xsl:value-of select="$contentValue" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$elementName" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$characterLimit" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$fileType" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$fileName" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$propertyIDExt" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$propertySINo" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$langPref" />
        <xsl:value-of select="'&#xA;'" />
        <xsl:value-of select="$newItemVersionNumber" />
        <xsl:value-of select="'&#xA;&#xA;&#xA;'" />
        </xsl:message-->
						
				<xsl:variable name="truncatedString" select="substring($contentValue, 1, $characterLimit)"/>
				<xsl:variable name="actualString" select="$contentValue"/>
				

				<!--xsl:message>
					<xsl:value-of select="'&#xA;actualString '" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:copy-of select="$actualString" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:value-of select="'&#xA;'" />
				</xsl:message>

				<xsl:variable name="actualStringParsed" select="xmlutil:parse(concat('&lt;article&gt;',$actualString,'&lt;/article&gt;'))" />
				<xsl:message>
					<xsl:value-of select="'&#xA;'" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:value-of select="'&#xA;actualStringParsed&#xA;'" />
					<xsl:copy-of select="$actualStringParsed" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:value-of select="'&#xA;'" />
				</xsl:message-->
				

						<xsl:variable name="textContent">
							<article><content><title><xsl:value-of select="$elementName" /></title><text><paragraph><xsl:copy-of select="$actualString" /></paragraph></text></content></article>
						</xsl:variable>

								<cs:command name="com.censhare.api.io.ReadXML" returning="storedcontent">
									<cs:param name="source" select="$existingTextAsset/storage_item[@key='master'][1]"/>
								</cs:command>

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;textContent SHA-1&#xA;'" />
					<xsl:copy-of select="cs:hash($textContent,'SHA-1')" />
					<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
					<xsl:copy-of select="cs:hash($textContent,'MD5')" />
					<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
					<xsl:value-of select="$existingTextAsset/storage_item[@key='master']/@hashcode" />
					<xsl:value-of select="'&#xA;&#xA;storedcontent &#xA;'" />
					<xsl:copy-of select="$storedcontent" />
					<xsl:value-of select="'&#xA;&#xA;current textContent&#xA;'" />
					<xsl:copy-of select="$textContent" />
					<xsl:value-of select="'&#xA;&#xA;storedcontent SHA-1 &#xA;'" />
					<xsl:copy-of select="cs:hash($storedcontent,'SHA-1')" />
					<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
				</xsl:message-->


						<xsl:variable name="textContentOnlyForHashing">&lt;article&gt;&lt;content&gt;&lt;title&gt;<xsl:value-of select="$elementName" />&lt;/title&gt;&lt;text&gt;&lt;paragraph&gt;<xsl:copy-of select="$actualString" />&lt;/paragraph&gt;&lt;/text&gt;&lt;/content&gt;&lt;/article&gt;</xsl:variable>

<!-- Only for creating hash and comparing -->
		<xsl:variable name="replaceBRinTextContent">
			<xsl:copy-of select="replace($textContentOnlyForHashing, '&lt;BR/&gt;', '&lt;br&gt;&lt;/br&gt;')"/>
		</xsl:variable>
		<xsl:variable name="finalTextContent">
			<xsl:copy-of select="replace($replaceBRinTextContent, '&lt;br/&gt;', '&lt;br&gt;&lt;/br&gt;')"/>
		</xsl:variable>

		<xsl:variable name="storedContentHash" select="cs:hash(string-join(csc:xml2string($storedcontent),''))" />
		<xsl:variable name="textContentHash" select="cs:hash(string-join(csc:xml2string($finalTextContent),''))" />

				<!--xsl:message>

					<xsl:value-of select="'&#xA;&#xA;textContent &#xA;'" />
					<xsl:copy-of select="$textContentOnlyForHashing" />
					<xsl:value-of select="'&#xA;replaceBRinTextContent &#xA;'" />
					<xsl:copy-of select="$replaceBRinTextContent" />
					<xsl:value-of select="'&#xA;finalTextContent &#xA;'" />
					<xsl:copy-of select="$finalTextContent" />
					<xsl:value-of select="'&#xA;'" />

					<xsl:value-of select="'&#xA;&#xA;Stored Content Hash &#xA;'" />
					<xsl:variable name="functionreturntext" select="cs:hash(string-join(csc:xml2string($storedcontent),''))" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:copy-of select="$functionreturntext" />
					<xsl:value-of select="'&#xA;Text Content Hash &#xA;'" />
					<xsl:variable name="functionreturntextContent" select="cs:hash(string-join(csc:xml2string($finalTextContent),''))" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:copy-of select="$functionreturntextContent" />
					<xsl:value-of select="'&#xA;'" />

					<xsl:choose>
					<xsl:when test="$functionreturntext = $functionreturntextContent">
						<xsl:value-of select="'when YES'" />
					</xsl:when>
					<xsl:when test="not($functionreturntext = $functionreturntextContent)">
						<xsl:value-of select="'when NO'" />
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="'&#xA;&#xA;&#xA;'" />

				</xsl:message-->

					<!--xsl:value-of select="'&#xA;stored content xml util dom&#xA;'" />
					<xsl:variable name="functionreturntext" select="xmlutildom:toString($storedcontent)" />
					<xsl:copy-of select="xmlutildom:toString($storedcontent)" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:copy-of select="cs:hash($functionreturntext)" />
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:variable name="functionreturntextContent" select="xmlutildom:toString($textContent)" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:copy-of select="xmlutildom:toString($textContent)" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:copy-of select="cs:hash($functionreturntextContent)" />
					<xsl:value-of select="'&#xA;&#xA;&#xA;'" /-->

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;textContent SHA-1 call function &#xA;'" />
					<xsl:variable name="functionreturntextContent" select="csc:xml2string($textContent)" />
					<xsl:copy-of select="$functionreturntextContent" />
					<xsl:value-of select="'&#xA;'" />
					<xsl:value-of select="'&#xA;text content xml util dom&#xA;'" />
					<xsl:variable name="functionreturntextContent" select="xmlutildom:toString($textContent)" />
					<xsl:copy-of select="cs:hash($functionreturntextContent,'SHA-1')" />
					<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
				</xsl:message-->


						<xsl:choose>
							<!--xsl:when test="cs:hash($textContent,'SHA-1') != cs:hash($storedcontent,'SHA-1')"-->
							<xsl:when test="$storedContentHash != $textContentHash">
								<textDefinitions>
					<textContent><xsl:value-of select="$truncatedString"/></textContent>

								<cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
								<xsl:variable name="textFileName" select="concat($out,concat($elementName,$propertyIDExt,'.xml'))"/>

								<cs:command name="com.censhare.api.io.WriteXML">
									<cs:param name="source" select="$textContent"/>
									<cs:param name="dest" select="$textFileName"/>
									<cs:param name="output">
										<output indent="no" use-character-maps="brackets"/>
									</cs:param>
								</cs:command>

								<cs:command name="com.censhare.api.assetmanagement.CheckOut" returning="checkoutassetxml">
									<cs:param name="source">
										<asset id="{$existingTextAsset/@id}" currversion="0">
										</asset>
									</cs:param>
								</cs:command>

								<cs:command name="com.censhare.api.assetmanagement.CheckIn" returning="CheckedInAssetXML">
								<cs:param name="source">
										<asset>
											<xsl:copy-of select="$checkoutassetxml/@*" />
											<!--xsl:copy-of select="$existingTextAsset/node() except ($existingTextAsset/storage_item[@key='master' and @asset_id=$existingTextAsset/@id],$existingTextAsset/asset_element[@key='actual.' and @asset_id=$existingTextAsset/@id])" /-->
											<xsl:copy-of select="$existingTextAsset/node() except ($existingTextAsset/storage_item[@key='master' and @asset_id=$existingTextAsset/@id],$existingTextAsset/asset_element[@key='actual.' and @asset_id=$existingTextAsset/@id],$existingTextAsset/storage_item[@key='text-preview' and @asset_id=$existingTextAsset/@id])" />
											<storage_item key="master" mimetype="text/xml" filesys_name="assets_emeri" element_idx="0" corpus:asset-temp-file-url="{$textFileName}"/>
											<asset_element key="actual." idx="0"/>
										</asset>
									</cs:param>
								</cs:command>

					<xsl:copy-of select="$CheckedInAssetXML" />

    <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="outDir">
      <cs:param name="id" select="$out"/>
    </cs:command>
					
								</textDefinitions>
							</xsl:when>
							<xsl:otherwise>
								<textDefinitions><textContent><xsl:value-of select="$truncatedString"/></textContent></textDefinitions>
							</xsl:otherwise>
						</xsl:choose>
								<!--xsl:if test="not($existingTextAsset/parent_asset_rel[@parent_asset=$propertyAsset/@id])">
									<child_asset_rel child_asset="{$existingTextAsset/@id}"  key="user."/>
								</xsl:if-->

					</xsl:when>
    		<xsl:when test="$existingTextAsset and $existingTextAsset/@checked_out_by">
    		<!--xsl:message><xsl:value-of select="'&#xA;&#xA;INISDE TEXT CHECKEDOUT&#xA;&#xA;'" /></xsl:message-->
    			<warning>
    				<warningElements id="W004"><xsl:value-of select="$elementName"/></warningElements>
    			</warning>
    		</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$contentValue != ''">
							<xsl:copy-of select="canvas:lengthValidationFunc($contentValue,$elementName,$characterLimit,$fileType,$fileName,$propertyIDExt,$propertySINo,$langPref,$newItemVersionNumber,$propertyAssetVariantXML)" />
						</xsl:if>
						<!--xsl:variable name="textProcessingResults" select="canvas:lengthValidationFunc($contentValue,$elementName,$characterLimit,$fileType,$fileName,$propertyIDExt,$propertySINo,$langPref,$newItemVersionNumber,$propertyAssetVariantXML)" />
						
						<asset_feature feature="{$confMappingFeature/id}" language="{$langPref}">
							<xsl:attribute name="{$confMappingFeature/valueType}">
								<xsl:copy-of select="$textProcessingResults//textContent/text()"></xsl:copy-of>
							</xsl:attribute>
						</asset_feature>
						
						<xsl:variable name="textAssetID" select="$textProcessingResults//asset/@id"/>

						<xsl:if test="$textAssetID">
							<child_asset_rel key="user." child_asset="{$textAssetID}" />
						</xsl:if-->
						
						<!-- ##########  OLD IMplementation-->

						<!--asset_feature language="{$langPref}" feature="{$confMappingFeature/id}">
							<xsl:attribute name="{$confMappingFeature/valueType}">
		                        <xsl:value-of select="$truncatedString"/>
	                        </xsl:attribute>
                        </asset_feature>
                        <excludeElements feature="{$confMappingFeature/id}" language="{$langPref}" /-->
						
						<!--asset_feature language="{$langPref}" feature="{$confMappingFeature/id}">
							<xsl:attribute name="{$confMappingFeature/valueType}">
								<xsl:copy-of select="canvas:lengthValidationFunc($contentValue,$elementName,$characterLimit,$fileType,$fileName,$propertyIDExt,$propertySINo,$langPref,$newItemVersionNumber,$propertyAssetVariantXML)" />
							</xsl:attribute>
						</asset_feature>

						<xsl:variable name="textAssetsRel" select="concat($propertyAsset/@id_extern,$elementName,$langPref)" />
						<xsl:variable name="textAssetXML" select="(cs:asset('sql=true')[@censhare:asset.id_extern=$textAssetsRel])[1]" />
						<xsl:if test="$textAssetXML/@id !=''">
							<child_asset_rel child_asset="{$textAssetXML/@id}" key="user." />
						</xsl:if-->

					</xsl:otherwise>
    		
		</xsl:choose>

	</xsl:function>
	

	<!-- String Length Validations -->	
    <xsl:function name="canvas:lengthValidationFunc">
        <xsl:param name="contentValue" as="xs:string"/>
        <xsl:param name="elementName" as="xs:string"/>
        <xsl:param name="characterLimit" as="xs:integer"/>
        <xsl:param name="fileType" as="xs:string"/>
        <xsl:param name="fileName" as="xs:string"/>
        <xsl:param name="propertyIDExt" as="xs:string"/>
        <xsl:param name="propertySINo"/>
        <xsl:param name="langPref" as="xs:string"/>
        <xsl:param name="newItemVersionNumber"/>
        <xsl:param name="propertyAssetVariantXML"/>

                        <xsl:variable name="truncatedString" select="substring($contentValue, 1, $characterLimit)"/>
                        <xsl:variable name="actualString" select="$contentValue"/>

    	<textDefinitios>
    	<textContent><xsl:value-of select="$truncatedString"/></textContent>

						<xsl:variable name="textIDExt" select="concat('text:',$propertyIDExt,$elementName,$langPref)" />

						<xsl:if test="not(cs:asset()[@censhare:asset.id_extern=$textIDExt and @censhare:asset.type=$fileType and @censhare:asset.currversion=0 and @censhare:asset.language=$langPref])">
							<xsl:copy-of select="canvas:checkAndCreateTextAssets($actualString,$elementName,$fileType,$fileName,$propertyIDExt,$propertySINo,$langPref,$newItemVersionNumber,$propertyAssetVariantXML)" />
						</xsl:if>
    	</textDefinitios>

						<!--xsl:variable name="idExternValue" select="concat($propertyIDExt,$langPref)" /-->

    </xsl:function>

	<!-- ################## -->
	<xsl:function name="canvas:checkAndCreateTextAssets">
        <xsl:param name="actualString" as="xs:string"/>
        <xsl:param name="elementName" as="xs:string"/>
        <xsl:param name="fileType" as="xs:string"/>
        <xsl:param name="fileName" as="xs:string"/>
        <xsl:param name="propertyIDExt" as="xs:string"/>
        <xsl:param name="propertySINo" as="xs:string"/>
        <xsl:param name="langPref" as="xs:string"/>
        <xsl:param name="newItemVersionNumber"/>
        <xsl:param name="propertyAssetVariantXML"/>

			<xsl:variable name="fileNameLangBased">
				<xsl:choose>
					<xsl:when test="$langPref != 'en'">
						<xsl:value-of select="concat($fileName,' ',$langPref)"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$fileName"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<xsl:variable name="textContent"><article><content><title><xsl:value-of select="$elementName" /></title><text><paragraph><xsl:copy-of select="$actualString" /></paragraph></text></content></article></xsl:variable>

			<cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
			<xsl:variable name="textFileName" select="concat($out, concat($elementName,$propertyIDExt,'.xml'))"/>

			<cs:command name="com.censhare.api.io.WriteXML">
				<cs:param name="source" select="$textContent"/>
				<cs:param name="dest" select="$textFileName"/>
				<cs:param name="output">
					<output indent="no" use-character-maps="brackets"/>
				</cs:param>
			</cs:command>

				<cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="resultAssetXml">
					<cs:param name="source">
						<!--asset application="contenteditor" name="{$fileName}" domain="root.christiesinternational.public." domain2="root.christiesinternational.emeri." type="{$fileType}" id_extern="{$propertyIDExt}{$elementName}{$langPref}" language="{$langPref}"-->
						<asset application="contenteditor" name="{$fileNameLangBased}" domain="root.christiesinternational.public." domain2="root.christiesinternational.emeri." type="{$fileType}" id_extern="text:{$propertyIDExt}{$elementName}{$langPref}" language="{$langPref}">
							<!--storage_item filesys_name="assets" key="master" mimetype="text/xml" element_idx="0" corpus:asset-temp-file-url="{$textFileName}"/-->
							
							<storage_item key="master" mimetype="text/xml" filesys_name="assets_emeri" element_idx="0" corpus:asset-temp-file-url="{$textFileName}"/>
							<asset_element key="actual." idx="0"/>
							<asset_feature feature="censhare:content-editor.config" value_asset_key_ref="censhare:content-editor-scs-text-v3" value_long="1" value_string2="censhare:resource-key"/>
							<asset_feature feature="censhare:input-form" value_asset_id="14792" value_long2="0"/>

								<!-- ############### handling different languages -->
								<xsl:if test="cs:asset()[@censhare:asset.id_extern=$propertyIDExt and @censhare:asset.type='product.' and @censhare:asset.currversion=0]">
									<xsl:variable name="propertyAssetXML" select="cs:asset()[@censhare:asset.id_extern=$propertyIDExt and @censhare:asset.type='product.' and @censhare:asset.currversion=0]" />
									
										<xsl:for-each select="$propertyAssetXML/cs:child-rel()[@key='user.']">
											<xsl:if test="./@type = $fileType">

											<xsl:choose>
												<xsl:when test="./@type = $fileType and ./@language='en'">
													<parent_asset_rel parent_asset="{./@id}" key="variant.1."/>
												</xsl:when>
												<xsl:when test="(./@type = $fileType and @language='CS') or (./@type = $fileType and @language='CT')">
												</xsl:when>
												<xsl:otherwise>
												</xsl:otherwise>
											</xsl:choose>
											</xsl:if>
										</xsl:for-each>
										
										<xsl:if test="$langPref='en'">
										<xsl:for-each select="$propertyAssetXML/cs:child-rel()[@key='user.']">
											<xsl:if test="./@type = $fileType  and not(./@language='en')">
													<child_asset_rel child_asset="{./@id}" key="variant.1."/>
											</xsl:if>
										</xsl:for-each>
										</xsl:if>
										
								</xsl:if>

							<!-- Handling variants -->
							<!--xsl:variable name="existingPropertyVariants" select="cs:asset()[@censhare:asset.type='product.' and @canvas:jdeproperty.shortitemnumber=$propertySINo]" />
							<xsl:if test="$existingPropertyVariants">
								<xsl:for-each select="$existingPropertyVariants">
									<xsl:if test="(./asset_feature[@feature='canvas:jdeproperty.itemversion' and @value_long != $newItemVersionNumber]) and not(./parent_asset_rel[@key='variant.1.'])">
										<xsl:for-each select="./cs:child-rel()[@key='user.']">
											<xsl:if test="./@type = $fileType">
												<parent_asset_rel parent_asset="{./@id}" key="variant.1."/>
											</xsl:if>
										</xsl:for-each>
									</xsl:if>
								</xsl:for-each>
							</xsl:if-->

								<xsl:if test="$propertyAssetVariantXML">
									<xsl:for-each select="$propertyAssetVariantXML/cs:child-rel()[@key='user.']">
										<xsl:if test="./@type = $fileType and ./@language=$langPref">
											<parent_asset_rel parent_asset="{./@id}" key="variant.1."/>
										</xsl:if>
									</xsl:for-each>
								</xsl:if>

						</asset>
					</cs:param>
				</cs:command>

			<xsl:copy-of select="$resultAssetXml" />

    <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="outDir">
      <cs:param name="id" select="$out"/>
    </cs:command>


	</xsl:function>



	<xsl:function name="csc:xml2string">
		<xsl:param name="source"/>
		<xsl:apply-templates select="$source" mode="code"/>
	</xsl:function>

	<xsl:template match="*" priority="2" mode="code">
		<xsl:value-of select="concat('&lt;', local-name(.))"/>
		<xsl:apply-templates select="@*" mode="#current"/>
		<xsl:text>&gt;</xsl:text>
		<xsl:apply-templates select="*|text()" mode="#current"/>
		<xsl:value-of select="concat('&lt;/', local-name(.), '&gt;')"/>
	</xsl:template>

	<xsl:template match="@*" priority="1" mode="code">
		<xsl:value-of select="concat(local-name(), '=', .)"/>
	</xsl:template>



</xsl:stylesheet>
