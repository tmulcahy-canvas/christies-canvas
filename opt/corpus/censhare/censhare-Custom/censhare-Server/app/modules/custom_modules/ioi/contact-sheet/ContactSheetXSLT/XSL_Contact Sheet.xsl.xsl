<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:censhare="http://www.censhare.com/my"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">

	<xsl:param name="censhare:command-xml" />

  	<xsl:template match="/">

  		<cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>

		<xsl:variable name="content">
			<xsl:element name="content">
				<data>
					<xsl:apply-templates select="$censhare:command-xml/assets/asset">
						<xsl:with-param name="out" select="$out"/>
					</xsl:apply-templates>
				</data>
			</xsl:element>
		</xsl:variable>

		<xsl:message>   ###   CONTENT - <xsl:copy-of select="$content"/></xsl:message>
		<!-- rg <xsl:message> ##### R.G CONTENT / <xsl:copy-of select="$content" /></xsl:message> rg -->
		<xsl:variable name="contentXML">
			<content>
				<xsl:for-each select="$content/content/data/images/image">
					<image>
						<xsl:copy-of select="./@*"/>
						<xsl:attribute name="id" select="position()"/>
						<xsl:copy-of select="./node()"/>
					</image>
				</xsl:for-each>
			</content>
		</xsl:variable>

		<xsl:if test="exists($contentXML)">
			<cs:command name="com.censhare.api.transformation.FopTransformation" returning="pdfresult">
				<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=xslfo:contact_sheet/storage/master/file'"/>
				<cs:param name="source" select="$contentXML"/>
				<cs:param name="dest" select="concat($out, 'Product_Sheet.pdf')"/>
				<cs:param name="xsl-parameters">
					<cs:param name= "asset" select="asset"/>
					<cs:param name="target-resolution" select="300"/>
				</cs:param>
			</cs:command>

  			<download><file filesys_name="{substring-before(substring-after($out, 'censhare:///service/filesystem/'), '/')}" relpath="{concat('file:', substring-after($out, 'censhare:///service/filesystem/temp/'), 'Product_Sheet.pdf')}" target_name="{concat('Contact Sheet - ', $censhare:command-xml/assets/asset[1]/@name, '.pdf')}"/></download>

			<!-- cc -<xsl:variable name="path" select="concat($out, 'Product_Sheet.pdf')"/>

			<xsl:variable name="resultAssetXml"/>
			<cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="resultAssetXml">
				<cs:param name="source">
					<asset name="Contact Sheet.pdf" currversion="0" type="picture.">
						<storage_item key="master" element_idx="0" filesys_name="assets" mimetype="application/pdf" corpus:asset-temp-file-url="{$path}"/>
						<asset_element key="actual."/>
					</asset>
				</cs:param>
			</cs:command>- cc -->

		</xsl:if>
	</xsl:template>

	<xsl:template match="asset[@type='sale.']">
       	<xsl:param name="out"/>
   		<!-- rg <data> rg -->
	     	<xsl:apply-templates select="./cs:child-rel()[@key='*']">
				<xsl:with-param name="sale-asset" select="."/>
				<xsl:with-param name="out" select="$out"/>
	     	</xsl:apply-templates>
	    <!-- rg </data> rg -->
	</xsl:template>

	<xsl:template match="asset[@type='group.']">
       	<xsl:param name="out"/>
   		<!-- rg <data> rg -->
	     	<xsl:apply-templates select="./cs:child-rel()[@key='*']">
				<xsl:with-param name="out" select="$out"/>
	     	</xsl:apply-templates>
	    <!-- rg </data> rg -->
	</xsl:template>

	<xsl:template match="asset[@type='product.']">
		<xsl:param name="sale-asset"/>
		<xsl:param name="out"/>
		<images>
			<xsl:for-each select="./cs:child-rel()[@key='user.main-picture.' or @key='user.']">
				<!-- rg <xsl:message> ##### R.G Curr asset please / <xsl:copy-of select="." /></xsl:message> rg -->
				<xsl:variable name="wf-id" select="./@wf_id"/>
			    <xsl:variable name="wf-step-id" select="./@wf_step"/>
			    <xsl:variable name="wf-step-name" select="cs:master-data('workflow_step')[@wf_id = $wf-id and @wf_step = $wf-step-id]/@name"/>
				<xsl:if test="./@type = 'picture.' and not($wf-step-name = 'Awaiting Image')">
					<xsl:variable name="main-image" select="."/>

					
					<xsl:message> ***** Main Image ***** <xsl:copy-of select="$main-image"/></xsl:message>
					<xsl:if test="if(string-length($main-image/@name) lt 9) then true() else false()">
					    <cs:command name="com.censhare.api.transformation.BarcodeTransformation">
							<cs:param name="source" select="string(concat('*', $main-image/@name, '*'))"/>
							<cs:param name="dest" select="concat($out,./@id,'.png')"/>
							<cs:param name="barcode-transformation">
								<barcode-transformation type="code39" file-format="png"
									height="15" module-width="0" margin="4"
									orientation="0" font="Courier" font-size=".25"
									qr-error-corr="" dpi="300" antialias="true"/>
							</cs:param>
						</cs:command>
					</xsl:if>
					<xsl:element name="image">
						<xsl:element name="url">
							<!-- R.G Modified by R.G-->
							<!-- rg <xsl:value-of select="if(not($wf-step-name = 'Awaiting Image')) then $main-image/storage_item[@key='preview']/@url else ''"/> rg -->
							<xsl:value-of select="$main-image/storage_item[@key='preview']/@url"/>
						</xsl:element>
						<!-- cc -<xsl:element name="id">
							<xsl:value-of select="./asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string"/>
						</xsl:element>- cc -->
						<!-- <xsl:element name="asset_type">
							<xsl:value-of select="cs:cachelookup('asset_typedef', '@asset_type', ./@type)/@name"/>
						</xsl:element>
						<xsl:element name="name">
							<xsl:value-of select="./@name"/>
						</xsl:element> -->
						<data value="{$main-image/asset_feature[@feature='canvas:ml.originlocation']/@value_string}"/>
						<data value="{$sale-asset/@name}"/>
						<xsl:element name="barcode">
							<xsl:if test="if(string-length($main-image/@name) lt 9) then true() else false()">
								<xsl:value-of select="concat($out,./@id,'.png')"/>
							</xsl:if>
						</xsl:element>
						<data value="{$main-image/@name}"/>
					</xsl:element>
				</xsl:if>
			</xsl:for-each>
		</images>
	</xsl:template>

</xsl:stylesheet>
