<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" 
  xmlns:censhare="http://www.censhare.com/xml/3.0.0/corpus" 
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:math="ext://java.lang.Math"
  exclude-result-prefixes="xs corpus censhare cs math" version="1.0">

  <xsl:variable name="chart-type" select="'pie'" as="xs:string"/>
  <xsl:variable name="chart-legend" select="false()" as="xs:boolean"/>
  <xsl:variable name="chart-options" select="'labelType: percent;'" as="xs:string"/>

  <xsl:variable name="child-no-wf-assigned-label" select="'No workflow assigned'" as="xs:string"/>

  <xsl:variable name="child-asset-type" select="'product.'" as="xs:string"/>
  <xsl:variable name="sub-child-asset-type" select="'picture.'" as="xs:string"/>
  <xsl:variable name="rel-key" select="'user.main-picture.'" as="xs:string"/>

  <xsl:variable name="total-sale-no-main-image-label" select="'Sales with no main image'" as="xs:string"/>

  <xsl:variable name="other-wf-step" select="'Other Workflow'"/>
  <xsl:variable name="wf-name-list" select="('Passed4Print', 'Image Editing', 'Ready2Proof', 'Awaiting Approval')"/>
  <xsl:variable name="workflow-mapping">
    <mappings>
      <mapping wf-name="Passed4Print" color="#6ae3a7"/>
      <mapping wf-name="Image Editing" color="#ff4e4f"/>
      <mapping wf-name="Ready2Proof" color="#ff8080"/>
      <mapping wf-name="Awaiting Approval" color="#ffcccc"/>
      <mapping wf-name="Other Workflow" color="#69bee6"/>
    </mappings>
  </xsl:variable>

  <xsl:variable name="wf" select="'Image'"/>
  <xsl:variable name="wfs" select="'Awaiting Image'"/>
  <xsl:variable name="wfid" select="cs:master-data('workflow')[@name=$wf]/@id"/>
  <xsl:variable name="wfsid" select="cs:master-data('workflow_step')[@wf_id=$wfid and @name=$wfs]/@wf_step"/>

  <xsl:template match="asset">

    <xsl:variable name="children-init">
      <child-assets>
        <!-- cc - changed to apply recursive search - cc -->
        <xsl:apply-templates select="./cs:child-rel()" mode="gather-children"/>
        <!-- cc -<xsl:apply-templates select="./cs:child-rel()[@key='*']/.[@type=$child-asset-type]/cs:child-rel()[@key=$rel-key]/.[@type=$sub-child-asset-type]" mode="gather-children"/>- cc -->
      </child-assets>
    </xsl:variable>

    <xsl:variable name="children">
      <child-assets>
        <xsl:for-each-group select="$children-init/child-assets/child-asset" group-by="@asset_id">
          <xsl:copy-of select="."/>
        </xsl:for-each-group>
      </child-assets>
    </xsl:variable>

    <xsl:variable name="total-main-image-count" select="count($children/child-assets/child-asset)"/>
    <xsl:variable name="total-main-image-with-workflow-count" select="count($children/child-assets/child-asset[@wf-step!='null'])"/>
    <xsl:variable name="total-sale-count" select="count(./cs:child-rel()[@key='*']/.[@type=$child-asset-type])" />
    <!-- cc -<xsl:variable name="total-sale-no-main-image" select="($total-sale-count - $total-main-image-count)" />- cc -->

    <xsl:variable name="title" select="'Properties with main image.'" as="xs:string"/>
    <xsl:variable name="sub-title" select="concat($total-main-image-with-workflow-count, ' of ', $total-main-image-count, ' have a workflow step assigned')" as="xs:string"/>

    <result>
      <xsl:if test="$title">
        <title>
          <xsl:value-of select="$title"/>
        </title>
      </xsl:if>
      <subTitle>
        <xsl:value-of select="$sub-title"/>
      </subTitle>
      <type>pie</type>
      <legend censhare:_annotation.datatype="boolean">false</legend>
      <labelType censhare:_annotation.datatype="string">value</labelType>
      <showToolTipPercent censhare:_annotation.datatype="boolean">true</showToolTipPercent>
      <valueFormat censhare:_annotation.datatype="string">d3.format('.0f')</valueFormat>
      <noData censhare:_annotation.datatype="string">There is no data to display</noData>
      <data censhare:_annotation.arraygroup="true">
        <xsl:for-each-group select="$children/child-assets/child-asset" group-by="@wf-step">
          <values>
            <key><xsl:value-of select="@wf-step-name"/></key>
            <xsl:if test="exists($workflow-mapping/mappings/mapping[@wf-name = current-group()[1]/@wf-step-name]/@color)">
              <color><xsl:value-of select="$workflow-mapping/mappings/mapping[@wf-name = current-group()[1]/@wf-step-name]/@color"/></color>
            </xsl:if>
            <value censhare:_annotation.datatype="number"><xsl:value-of select="count(current-group())"/></value>
            <query>
              <ids censhare:_annotation.datatype="string"><xsl:value-of select="string-join(current-group()/@asset_id, ',')"/></ids>
            </query>
          </values>
        </xsl:for-each-group>
        <!-- cc -<values>
          <key><xsl:value-of select="$total-sale-no-main-image-label"/></key>
          <value censhare:_annotation.datatype="number"><xsl:value-of select="$total-sale-no-main-image"/></value>
        </values>- cc -->
      </data>
    </result>
  </xsl:template>
  r
  <!-- cc - template applied to all children of contextual asset of a specific type
            Specific business logic can be applied here...Currently this will return
            a node called child-asset and an attribute containing the name of the workflow step currently
            held by the child asset.  If no workflow is assigned those assets will be returned as a category
            and displayed seperately as a pie.
  - cc -->
  <!-- cc - changed to be image.* template - cc -->
  <xsl:template match="asset[starts-with(@type, $sub-child-asset-type)]" mode="gather-children">
    <xsl:variable name="wf-id" select="./@wf_id"/>
    <xsl:variable name="wf-name" select="cs:master-data('workflow')[@id=$wf-id]/@name"/>
    <xsl:variable name="wf-step-id" select="./@wf_step"/>
    <xsl:variable name="wf-step-name" select="cs:master-data('workflow_step')[@wf_id = $wf-id and @wf_step = $wf-step-id]/@name"/>
    <xsl:choose>
      <!-- cc - changed to ensure only wf_steps that show are the ones apart of the above list - cc -->
      <xsl:when test="$wf-step-name = $wf-name-list">
        <xsl:message>   ####  WF PIE - <xsl:value-of select="$wf-step-name"/> - <xsl:value-of select="$wf-step-name"/> - <xsl:value-of select="$wf-name-list"/></xsl:message>
        <child-asset asset_id="{@id}" wf="{$wf-name}" wf-step="{$wf-step-id}" wf-step-name="{$wf-step-name}"/>  <!-- cc - {if($wf-step-name = $wf-name-list) then $wf-step-name else $other-wf-step} - cc -->
      </xsl:when>
      <xsl:when test="$wf-step-name = 'Awaiting Image'">
      	<!-- R.G Edit to Exclude the images that are set to WF Step Awaiting Image.
      		This should do nothing -->
      		<!-- R.G <xsl:message> ##### R.G Awaiting Image Exclude</xsl:message> R.G-->
      </xsl:when>
      <xsl:when test="$wf-id">
        <xsl:message>   ####  OTHER PIE - <xsl:value-of select="$wf-step-name"/> - <xsl:value-of select="$wf-step-name"/> - <xsl:value-of select="$wf-name-list"/></xsl:message>
        <child-asset asset_id="{@id}" wf="{$wf-name}" wf-step="other" wf-step-name="{$other-wf-step}"/>
      </xsl:when>
      <xsl:otherwise>
        <child-asset asset_id="{@id}" wf="null" wf-step="null" wf-step-name="{$child-no-wf-assigned-label}"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- cc - added template to recursively search asset tree for images - cc -->
  <xsl:template match="asset[not(starts-with(@type, $sub-child-asset-type))]" mode="gather-children">
    <xsl:apply-templates select="./cs:child-rel()" mode="gather-children"/>
  </xsl:template>

</xsl:stylesheet>
