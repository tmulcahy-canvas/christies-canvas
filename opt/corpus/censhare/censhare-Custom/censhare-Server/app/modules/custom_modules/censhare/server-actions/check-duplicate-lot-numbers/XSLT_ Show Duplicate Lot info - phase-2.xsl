<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io"
    xmlns:sd="http://www.censhare.com/SolutionDevelopment" exclude-result-prefixes="#all">
    
    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>
    <xsl:param name="asset-ids"/>
    
    <xsl:template match="/asset">
        <xsl:variable name="sale-name" select="@name"/>
        <xsl:variable name="lot-asset-mapping">
            <lot-assets sale-name="{@name}">
                <xsl:for-each select="cs:child-rel()[@key='target.']/.[starts-with(@type,'product.')]">
                    <xsl:sort select="number(asset_feature[@feature='canvas:lotnumberandsuffix']/@value_string)"/>
                    <lot-asset lot-name="{@name}" stock-number="{asset_feature[@feature='canvas:jdeproperty.identifier2nditem']/@value_string}" asset-id="{@id}" lot-number="{asset_feature[@feature='canvas:lotnumberandsuffix']/@value_string}" created="{@creation_date}" modified="{@modified_date}"/>
                </xsl:for-each>
            </lot-assets>
        </xsl:variable>
        <xsl:variable name="lot-number" select="distinct-values(cs:child-rel()[@key='target.']/.[starts-with(@type,'product.')]/asset_feature[@feature='canvas:lotnumberandsuffix']/@value_string)"/>
        
        
        <xsl:variable name="duplicate-lots">
            <xsl:for-each select="$lot-number">
                <xsl:sort select="number(.)"/>
                <xsl:variable name="current-number" select="."/>
                <xsl:variable name="lot-detail" select="$lot-asset-mapping/lot-assets/lot-asset[@lot-number = $current-number]"/>
                <xsl:if test="count($lot-detail) &gt; 1">
                    <xsl:copy-of select="$lot-detail"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:sequence select="sd:create-html($duplicate-lots,$sale-name)"/>
        
    </xsl:template>
    
    <xsl:function name="sd:create-html">
        <xsl:param name="duplicate-lots"/>
        <xsl:param name="sale-name"/>
        
        <html>
            <head>
                <style>
                    .asset-info-label {
                    color:#707070;
                    white-space:nowrap;
                    margin-right: 9px;
                    text-align: right;
                    }
                    .asset-info-label-inline {
                    color:#707070;
                    white-space:nowrap;
                    text-align: right;
                    }
                    .asset-info-value {
                    text-align: left;
                    margin-right: 9px;
                    }
                    .asset-info-value2 {
                    color:#707070;
                    }
                    .asset-info-table {
                    white-space:normal;
                    padding: 5px;
                    font: Arial;
                    font-size: 11pt;
                    }
                    .asset-ref-table {
                    white-space:normal;
                    padding: 0px;
                    }
                    .asset-info-title {
                    font-weight: bold;
                    font-size: 110%;
                    }
                    .asset-info-bold {
                    font-weight: bold;
                    }
                    .asset-info-link {
                    color:#0080FF;
                    }
                    .admin-help-link {
                    color:#5882FA;
                    }
                    .admin-quick-help-text {
                    word-wrap:break-word;
                    white-space:normal;
                    }
                </style>
            </head>
            <body>
                <table class="asset-info-table" cellpadding="2" valign="top">
                    <thead>
                        <tr>
                            <td colspan="6" class="asset-info-label" align="center"><xsl:value-of select="$sale-name"/></td>
                        </tr>
                        <tr>
                            <td class="asset-info-label"  align="left">Property Name</td>
                            <td class="asset-info-label" align="left">Item Number 2nd</td>
                            <td class="asset-info-label" align="left">Asset ID</td>
                            <td class="asset-info-label" align="left">Lot Number and Lot Suffix</td>
                            <td class="asset-info-label" align="left">Date Created</td>
                            <td class="asset-info-label" align="left">Date last Modified</td>
                        </tr>
                    </thead>
                    <xsl:variable name="rows">
                        <xsl:choose>
                            <xsl:when test="exists($duplicate-lots/lot-asset)">
                                <xsl:for-each select="$duplicate-lots/lot-asset">
                                    <tr>
                                        <td class="asset-info-value" align="left"><xsl:value-of select="@lot-name"/></td>
                                        <td class="asset-info-value" align="left"><xsl:value-of select="@stock-number"/></td>
                                        <td class="asset-info-value" align="left"><xsl:value-of select="@asset-id"/></td>
                                        <td class="asset-info-value" align="left"><xsl:value-of select="@lot-number"/></td>
                                        <td class="asset-info-value" align="left"><xsl:value-of select="cs:format-date(xs:dateTime(@created))"/></td>
                                        <td class="asset-info-value" align="left"><xsl:value-of select="cs:format-date(xs:dateTime(@modified))"/></td>
                                    </tr>
                                </xsl:for-each>
                            </xsl:when>
                            <xsl:otherwise>
                                <tr>
                                    <td colspan="6" class="asset-info-value" align="center">There are no duplicate lot numbers in this sale.</td>
                                </tr>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:for-each select="$rows/tr">
                        <xsl:variable name="current" select="current()"/>
                        <tr>
                            <xsl:if test="position() mod 2 != 0">
                                <xsl:attribute name="bgcolor">#DADADA</xsl:attribute>
                            </xsl:if>
                            <xsl:copy-of select="$current/*"/>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:function>
    
</xsl:stylesheet>
