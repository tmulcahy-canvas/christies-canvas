<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:censhare="java:modules.conversion.XSLExtensionFunctions" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:sd="http://www.censhare.com/xml/3.0.0/censhare-solution-development" xmlns:cen-func="http://ns.censhare.de/functions" xmlns:ioi="http://www.iointegration.com" xmlns:java="http://www.java.com/" exclude-result-prefixes="#all">
    <xsl:include href="censhare:///service/assets/asset;censhare:resource-key=sd:function-library/storage/master/file"/>
    <xsl:output method="xml" indent="yes" omit-xml-declaration="no" encoding="UTF-8"/>

    <xsl:param name="censhare:command-xml"/>


    <xsl:variable name="uiLocale" select="($censhare:command-xml/system/@locale, 'en')[1]" />
    <xsl:variable name="log-prefix" select="'   ###  ML Interface - '"/>
    <xsl:variable name="process-user" select="'102'"/>

    <!-- cc - updated first domain to new Public domain - cc -->
    <xsl:variable name="first-domain" select="'root.christiesinternational.public.'"/>
    <!-- cc - created sale-domain2 for all sale assets will now fall under this second domain - cc -->
    <xsl:variable name="sale-domain2" select="'root.christiesinternational.emeri.'"/>

    <!-- cc - change-type mapping - cc -->
    <xsl:variable name="change-type-map">
        <change-types>
            <change-type name="new" type="new"/>
            <change-type name="replace" type="replace"/>
            <change-type name="remove" type="remove"/>
            <change-type name="update-metadata" type="update"/>
            <change-type name="archive" type="Archived Image"/>
        </change-types>
    </xsl:variable>

    <xsl:variable name="sale-location-map">
        <sale-locations>
            <sale-location name="Amsterdam" code="AMS"/>
            <sale-location name="King Street" code="CKS"/>
            <sale-location name="Christies Private Sales" code="CPS"/>
            <sale-location name="Dubai" code="DUB"/>
            <sale-location name="Geneva Ecommerce" code="ECO"/>
            <sale-location name="Geneva" code="GNV"/>
            <sale-location name="Hong Kong" code="HGK"/>
            <sale-location name="South Kensington" code="KEN"/>
            <sale-location name="New York" code="NYC"/>
            <sale-location name="New York Wine" code="NYW"/>
            <sale-location name="Paris" code="PAR"/>
            <sale-location name="Shanghai, China" code="SHA"/>
            <sale-location name="Stockholm" code="STK"/>
            <sale-location name="Zurich" code="ZUR"/>
            <sale-location name="PS King Street" code="CPSCKS"/>
            <sale-location name="PS New York" code="CPSNYC"/>
        </sale-locations>
    </xsl:variable>

    <!-- cc - Updated domain mappings to map to new Xinet_Origin_Location property in sidecar xml - cc -->
    <xsl:variable name="domain-mapping">
        <domains>
            <domain name="EMERI" key="root.christiesinternational.emeri."/>
            <domain name="AMERICAS" key="root.christiesinternational.americas."/>
            <domain name="ASIA" key="root.christiesinternational.asia."/>
        </domains>
    </xsl:variable>

    <xsl:variable name="workflow-steps">
        <workflow id="35000">
            <step name="Image_Editing" id="10" production-status="Image_Editing"/>
            <step name="Ready2Proof" id="20" production-status="Ready2Proof"/>
            <step name="Awaiting_Approval" id="30" production-status="Awaiting_Approval"/>
            <step name="Passed4Print" id="40" production-status="Passed4Print"/>
            <step name="Error_on_Update" id="50" production-status="Error_on_Update"/>
            <step name="Archived Image" id="70" production-status="Archived"/>
            <step name="Deleted from ML" id="80" production-status="Deleted"/>
            <step name="Awaiting Image" id="90" production-status="Awaiting Image"/>
        </workflow>
    </xsl:variable>

    <!-- cc - dummy var to satisfy function param requirements - cc -->
    <xsl:variable name="dummy" select="''"/>

    <xsl:template match="/">

        <!-- cc - START - cc -->
        <xsl:variable name="startTime" select="current-dateTime()"/>
        <xsl:message>
            <xsl:value-of select="concat($log-prefix, ' ML Start        - ', format-dateTime($startTime, '[Y0001]/[M01]/[D01]-[H01]:[m01]:[s01].[f1]'))"/>  
        </xsl:message>

        <!-- cc - process sidecar - cc -->
        <result>
            <xsl:apply-templates select="$censhare:command-xml//filelist/*"/>
        </result>

        <!-- cc - END - cc -->
        <xsl:variable name="endTime" select="current-dateTime()"/>
        <xsl:message>
            <xsl:value-of select="concat($log-prefix, ' ML END          - ', format-dateTime($endTime, '[Y0001]/[M01]/[D01]-[H01]:[m01]:[s01].[f1]'))"/>
        </xsl:message>
        <xsl:variable name="elapsed" select="(xs:dateTime($endTime) - xs:dateTime($startTime))"/>
        <xsl:message>
            <xsl:value-of select="concat($log-prefix, ' ML DURATION     - ', seconds-from-duration($elapsed), ' (s)')"/>
        </xsl:message>
    
    </xsl:template>

    <xsl:template match="file">
        <xsl:variable name="inputFileXml">
            <cs:command name="com.censhare.api.io.ReadXML">
                <cs:param name="source" select="concat('censhare:///service/filesystem/', @filesystem, '/', replace(@url, 'file:', ''))"/>
            </cs:command>
        </xsl:variable>
        <xsl:variable name="url" select="@url"/>
        <!-- cc -<xsl:message>Input file content:
            <xsl:copy-of select="$inputFileXml"/>
        </xsl:message>- cc -->
        <xsl:apply-templates select="$inputFileXml/FileSpecification"/>
    </xsl:template>

    <xsl:template match="FileSpecification" priority="2.0">
        <!-- cc - extracting data from sidecar - cc -->
        <xsl:variable name="asset-name-or-barcode" select="sd:getFileNameWithoutSuffix(Filename/Property[@Name='filename']/@Value)"/>
        <xsl:variable name="asset-master-storage-item-file-name" select="Filename/Property[@Name='fpofilename']/@Value"/>
        <xsl:variable name="asset-name-or-barcode-length" select="string-length(Filename/Property[@Name='filename']/@Value)"/>
        <xsl:variable name="sale-number" select="normalize-space(ImageMetadata/Property[@Name='Sale Number']/@Value)"/>
        <xsl:variable name="existing-sale-asset" select="(cs:asset()[@censhare:asset.id_extern = concat('sale:', $sale-number)])[1]"/>
        <xsl:variable name="unique-id" select="normalize-space(ImageMetadata/Property[@Name='Censhare_Unique_ID']/@Value)"/>
        <xsl:variable name="origin-location" select="normalize-space(ImageMetadata/Property[@Name='Xinet_Origin_Location']/@Value)"/>
        <xsl:variable name="sale-location" select="normalize-space(lower-case(ImageMetadata/Property[@Name='Sale Site']/@Value))"/>
        <xsl:variable name="change-type" select="ChangeType/Property[@Name='ChangeType']/@Value"/>
        <xsl:variable name="production-status" select="ImageMetadata/Property[@Name='Production Status']/@Value"/>

        <!-- cc - mapped varaiables - cc -->
        <xsl:variable name="second-domain" select="$domain-mapping/domains/domain[@name = $origin-location]/@key"/>
        <xsl:variable name="change-type-mapped" select="$change-type-map/change-types/change-type[@type = $change-type]/@name"/>
        <xsl:variable name="sale-location-code" select="cs:master-data('feature_value')[@feature='canvas:ml.sale.location' and lower-case(@name) = $sale-location]/@value_key"/>
        <xsl:variable name="workflow-step" select="$workflow-steps/workflow/step[lower-case(@production-status)=lower-case($production-status)]/@id"/>

        <!-- cc - Craft asset_feature structure for create/update routines (update compares values no longer replacing) - cc -->
        <xsl:variable name="image-asset-features">
            <features>
                <!-- To be set on image asset -->
                <xsl:if test="ChangeType/Property[@Name='ChangeType']/@Value != ''">
                    <asset_feature feature="canvas:ml.changetype" value_string="{ChangeType/Property[@Name='ChangeType']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="sd:getFileNameWithoutSuffix(Filename/Property[@Name='filename']/@Value) != ''">
                    <asset_feature feature="canvas:ml.name" value_string="{sd:getFileNameWithoutSuffix(Filename/Property[@Name='filename']/@Value)}" language="en"/>
                </xsl:if>
                <xsl:if test="ImageMetadata/Property[@Name='Photographers Name']/@Value != ''">
                    <asset_feature feature="canvas:ml.photographer" value_string="{ImageMetadata/Property[@Name='Photographers Name']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="ImageMetadata/Property[@Name='Photographers Name']/@Value != ''">
                    <asset_feature feature="censhare:asset.author" value_string="{ImageMetadata/Property[@Name='Photographers Name']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="ImageMetadata/Property[@Name='Principal Contact Email']/@Value != ''">
                    <asset_feature feature="canvas:ml.departmentcontactemail" value_string="{ImageMetadata/Property[@Name='Principal Contact Email']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="ImageMetadata/Property[@Name='Sub-Category']/@Value != ''">
                    <asset_feature feature="canvas:ml.subcategory" value_string="{ImageMetadata/Property[@Name='Sub-Category']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="ImageMetadata/Property[@Name='Keywords']/@Value != ''">
                    <asset_feature feature="censhare:asset.keywords" value_string="{ImageMetadata/Property[@Name='Keywords']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="ImageMetadata/Property[@Name='Xinet_Path']/@Value != ''">
                    <asset_feature feature="canvas:ml.highresfilepath" value_string="{ImageMetadata/Property[@Name='Xinet_Path']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="$workflow-steps/workflow/step[lower-case(@production-status)=lower-case($production-status)]/@id != ''">
                    <asset_feature feature="canvas:ml.productionstatus" value_key="{$workflow-steps/workflow/step[lower-case(@production-status)=lower-case($production-status)]/@id}"/>
                </xsl:if>
                <xsl:if test="$origin-location != ''">
                    <!-- cc - adding value $origin-location to feature canvas:ml.origin-location - cc -->
                    <asset_feature feature="canvas:ml.originlocation" value_string="{$origin-location}" language="en"/>
                </xsl:if>
            </features>
        </xsl:variable>

        <xsl:variable name="image-and-sale-asset-features">
            <features>
                <!-- To be set on both image and sale asset -->
                <xsl:if test="ImageMetadata/Property[@Name='Sale Number']/@Value != ''">
                    <asset_feature feature="canvas:ml.salenumber" value_long="{ImageMetadata/Property[@Name='Sale Number']/@Value}" language="en"/>
                </xsl:if>
                <xsl:if test="$sale-location-code != ''">
                    <asset_feature feature="canvas:ml.sale.location" value_key="{$sale-location-code}"/>
                </xsl:if>
                <!-- Only process correct date format -->
                <!-- :matches('03/25/18 00:00:00', '^\d{2}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}') -->
                <!-- :matches('2016/04/18 00:00:00', '^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}') -->
                <xsl:variable name="production-deadline" select="normalize-space(ImageMetadata/Property[@Name='Production Deadline']/@Value)"/>
                <xsl:if test="matches($production-deadline, '^\d{2}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}') or matches($production-deadline, '^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}')">
                    <asset_feature feature="canvas:ml.productiondeadline" value_timestamp="{sd:create-timestamp($production-deadline)}"/>
                </xsl:if>
                <xsl:variable name="sign-off-deadline" select="normalize-space(ImageMetadata/Property[@Name='Sign-Off Deadline']/@Value)"/>
                <xsl:if test="matches($sign-off-deadline, '^\d{2}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}') or matches($sign-off-deadline, '^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}')">
                    <asset_feature feature="canvas:ml.signoffdeadline" value_timestamp="{sd:create-timestamp($sign-off-deadline)}"/>
                </xsl:if>
            </features>
        </xsl:variable>

        <!-- cc - change to query for JDE images if no image found - cc -->
        <xsl:variable name="existing-image-asset">
            <xsl:message><xsl:value-of select="concat($log-prefix, ' - Existing Image Asset Routine')"/></xsl:message>
            <xsl:variable name="image-by-unique-id" select="(cs:asset()[@censhare:asset.id_extern = concat('image:', $unique-id)])[1]"/>
            <xsl:choose>
                <xsl:when test="exists($image-by-unique-id)">
                    <xsl:message><xsl:value-of select="concat($log-prefix, ' - Existing Image Asset Routine - Image Found')"/></xsl:message>
                    <xsl:sequence select="$image-by-unique-id"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message><xsl:value-of select="concat($log-prefix, ' - Existing Image Asset Routine - Image not found...Fall over to JDE')"/></xsl:message>
                    <xsl:variable name="image-by-jde" select="(cs:asset()[@censhare:asset.id_extern = concat('image:', $asset-name-or-barcode)])[1]"/>
                    <xsl:choose>
                        <xsl:when test="exists($image-by-jde)">
                            <xsl:message><xsl:value-of select="concat($log-prefix, ' - Existing Image Asset Routine - Image Found JDE - ')"/></xsl:message>
                            <xsl:sequence select="$image-by-jde"/>
                        </xsl:when>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:message><xsl:value-of select="concat($log-prefix, ' - Existing Image Asset Routine - Result image - ', $existing-image-asset/asset/@id)"/></xsl:message>

        <xsl:choose>
            <!-- cc - ARCHIVE ROUTINE - cc -->
            <xsl:when test="$change-type-mapped = 'archive'">
                <xsl:message>
                    <xsl:value-of select="concat($log-prefix, 'Performing Archive Routine')"/>
                </xsl:message>
                <xsl:sequence select="ioi:archive-routine($existing-image-asset)"/>
            </xsl:when>
            <!-- cc - ADD/REPLACE ROUTINE - cc -->
            <xsl:when test="$change-type-mapped = 'new' or $change-type-mapped = 'replace'">
                <xsl:message>
                    <xsl:value-of select="concat($log-prefix, 'Performing Add/Replace Routine')"/>
                </xsl:message>
                <xsl:sequence select="ioi:add-replace-routine($change-type-mapped, $unique-id, $existing-sale-asset,
                                            $existing-image-asset, $sale-number, $sale-location-code, $second-domain,
                                            $asset-name-or-barcode, $image-and-sale-asset-features, $image-asset-features,
                                            $asset-name-or-barcode-length, $workflow-step, $asset-master-storage-item-file-name)"/>
            </xsl:when>
            <!-- cc - UPDATE METADATA ROUTINE - cc -->
            <xsl:when test="$change-type-mapped = 'update-metadata'">
                <xsl:message>
                    <xsl:value-of select="concat($log-prefix, 'Performing Update Metadata Routine')"/>
                </xsl:message>
                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $dummy, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $dummy, $dummy, $unique-id)"/>
            </xsl:when>
            <!-- cc - DELETE ROUTINE - cc -->
            <xsl:when test="$change-type-mapped = 'remove'">
                <xsl:message>
                    <xsl:value-of select="concat($log-prefix, 'Performing Delete Routine')"/>
                </xsl:message>
                <xsl:sequence select="ioi:delete-routine($existing-image-asset, $workflow-step)"/>
            </xsl:when>
        </xsl:choose>

    </xsl:template>

    <!-- cc - template to iterate existing lot assets and generate parent rel node - cc -->
    <xsl:template match="asset" mode="relate-existing-lot-asset">
        <xsl:param name="perform-child-rel-test"/>
        <parent_asset_rel key="user." parent_asset="{./@id}"> <!-- cc - user.main-picture - cc -->
            <xsl:choose>
                <xsl:when test="$perform-child-rel-test">
                    <xsl:if test="not(exists(./child_asset_rel[@key='user.main-picture.']))">
                        <asset_rel_feature feature="canvas:jdeproperty.primaryid" value_string="1"/>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <asset_rel_feature feature="canvas:jdeproperty.primaryid" value_string="1"/>
                </xsl:otherwise>
            </xsl:choose>
        </parent_asset_rel>
    </xsl:template>

    <!-- cc - template to iterate exist lot assets related with user relation and generate parent rel node - cc -->
    <xsl:template match="asset" mode="relate-existing-lot-asset-related-with-user-relation">
        <parent_asset_rel key="user." parent_asset="{./@id}"/>
    </xsl:template>

    <!-- cc - function to handle add/replace routine - cc -->
    <xsl:function name="ioi:add-replace-routine">
        <xsl:param name="change-type"/>
        <xsl:param name="unique-id"/>
        <xsl:param name="existing-sale-asset"/>
        <xsl:param name="existing-image-asset"/>
        <xsl:param name="sale-number"/>
        <xsl:param name="sale-location-code"/>
        <xsl:param name="second-domain"/>
        <xsl:param name="asset-name-or-barcode"/>
        <xsl:param name="image-and-sale-asset-features"/>
        <xsl:param name="image-asset-features"/>
        <xsl:param name="asset-name-or-barcode-length"/>
        <xsl:param name="workflow-step"/>
        <xsl:param name="asset-master-storage-item-file-name"/>
        <!-- cc - splitting $file-url into two seperate variables - cc -->
        <xsl:variable name="base-file-url" select="'censhare:///service/filesystem/interfaces/censhare_ML_interface/ML_export_emeri/images_in/source_zip/'"/>
        <xsl:variable name="file-name" select="concat($asset-name-or-barcode, '.eps')"/>
        <xsl:variable name="file-exists" select="ioi:check-for-file($base-file-url, $asset-master-storage-item-file-name)"/>
        <!-- cc -<xsl:message>
            <xsl:value-of select="concat($log-prefix, 'Base File Url - ', $base-file-url)"/>
        </xsl:message>
        <xsl:message>
            <xsl:value-of select="concat($log-prefix, 'File Name - ', $asset-master-storage-item-file-name)"/>
        </xsl:message>
        <xsl:message>
            <xsl:value-of select="concat($log-prefix, 'File Exists - ')"/>
            <xsl:copy-of select="$file-exists"/>
        </xsl:message>- cc -->
        <!-- Existing duplicate image asset with differnt external id but same barcode -->
        <xsl:variable name="duplicate-image-asset" select="(cs:asset()[@canvas:ml.name = $asset-name-or-barcode])[1]"/>
        <xsl:variable name="image-physical-file-exists" select="$existing-image-asset/asset/storage_item[@key='master']"/>
        <!-- Is the primary image relation between lot and image asset is created by user or not? -->
        <xsl:variable name="relation-created-by-interface" select="($duplicate-image-asset/cs:parent-rel()[@key='user.main-picture.']/child_asset_rel[@key='user.main-picture.']/asset_rel_feature[@feature=''])[1]"/><!-- canvas:jde.lot-primary-image-relation -->
        <xsl:variable name="existing-lot-asset-related-with-user-relation">
            <xsl:choose>
                <xsl:when test="$asset-name-or-barcode-length &gt;= 8">
                    <!-- cc - Equals 8 - cc -->
                    <xsl:variable name="barcode-with-first-8-digits" select="substring($asset-name-or-barcode, 1, 8)"/>
                    <xsl:copy-of select="(cs:asset()[@canvas:jdeproperty.primaryid = $barcode-with-first-8-digits or @canvas:jdeproperty.imageid like concat('*', $barcode-with-first-8-digits,'*')])"/>
                </xsl:when>
                <xsl:otherwise>
                    <!-- cc - Less than 8 - cc -->
                    <xsl:copy-of select="(cs:asset()[@canvas:jdeproperty.imageid = $asset-name-or-barcode])"/><!-- canvas:jde.image-id -->
                </xsl:otherwise>
                <!-- cc - select="(cs:asset()[@canvas:jdeproperty.imageid like concat($asset-name-or-barcode,'*')])"/> - cc -->
            </xsl:choose>
        </xsl:variable>
        <!-- cc - removed [1] to accept multiple matches - cc -->
        <xsl:variable name="existing-lot-asset">
            <xsl:choose>
                <xsl:when test="$asset-name-or-barcode-length gt 8">
                    <xsl:variable name="barcode-with-first-8-digits" select="substring($asset-name-or-barcode, 1, 8)"/>
                    <xsl:copy-of select="(cs:asset()[@canvas:jdeproperty.imageid like concat($barcode-with-first-8-digits,'*')])"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:copy-of select="(cs:asset()[@canvas:jde.primary-image-id = $asset-name-or-barcode])"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="master-storage-item">
            <storage_item key="master" element_idx="0" corpus:asset-temp-file-url="{concat($base-file-url, $asset-master-storage-item-file-name)}" mimetype="{$file-exists/@mimetype}" />
        </xsl:variable>
        <!-- cc - determine if a unique id was passed from ML - cc -->
        <xsl:choose>
            <!-- cc - when no unique id was passed - cc -->
            <xsl:when test="not($unique-id)">
                <xsl:message>
                    <xsl:value-of select="concat($log-prefix, 'No Unique ID Found...Create without!')"/>
                </xsl:message>
                <xsl:sequence select="ioi:create-with-no-unique-id($existing-sale-asset, 
                                            $existing-lot-asset,
                                            $existing-lot-asset-related-with-user-relation,
                                            $sale-number, $sale-location-code, 
                                            $first-domain,
                                            $second-domain, 
                                            $sale-domain2, 
                                            $image-asset-features,
                                            $image-and-sale-asset-features, 
                                            $asset-name-or-barcode, 
                                            $master-storage-item, $workflow-step)"/>
            </xsl:when>
            <!-- cc - unique id was found - cc -->
            <xsl:otherwise>
                <xsl:message>
                    <xsl:value-of select="concat($log-prefix, 'Unique ID Found!')"/>
                </xsl:message>
                <xsl:sequence select="ioi:add-replace($existing-image-asset, 
                                            $existing-lot-asset, 
                                            $asset-name-or-barcode-length, 
                                            $duplicate-image-asset, 
                                            $asset-name-or-barcode, 
                                            $unique-id, $first-domain, 
                                            $second-domain,
                                            $image-asset-features, 
                                            $image-and-sale-asset-features, 
                                            $relation-created-by-interface,
                                            $existing-lot-asset-related-with-user-relation, 
                                            $existing-sale-asset,
                                            $sale-number, 
                                            $sale-domain2, 
                                            $sale-location-code, 
                                            $image-physical-file-exists,
                                            $workflow-step, 
                                            $master-storage-item)"/>
                <!-- cc - follow change type? seems like add and replace should do the same checks. - cc -->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- cc - function to create assets when no unique id is found - cc -->
    <xsl:function name="ioi:create-with-no-unique-id">
        <xsl:param name="existing-sale-asset"/>
        <xsl:param name="existing-lot-asset"/>
        <xsl:param name="existing-lot-asset-related-with-user-relation"/>
        <xsl:param name="sale-number"/>
        <xsl:param name="sale-location-code"/>
        <xsl:param name="first-domain"/>
        <xsl:param name="second-domain"/>
        <xsl:param name="sale-domain2"/>
        <xsl:param name="image-asset-features"/>
        <xsl:param name="image-and-sale-asset-features"/>
        <xsl:param name="asset-name-or-barcode"/>
        <xsl:param name="master-storage-item"/>
        <xsl:param name="workflow-step"/>
        <!-- Always create a new asset when source XML contains no unique id (Censhare_Unique_ID) -->
        <xsl:choose>
            <!-- cc - No existing sale asset and sale number exists - cc -->
            <xsl:when test="not($existing-sale-asset) and $sale-number">
                <!-- Create sale asset first -->
                <xsl:variable name="new-sale-asset">
                    <xsl:sequence select="ioi:create-sale-asset($sale-location-code, $sale-number, $first-domain,
                                                $sale-domain2, $image-and-sale-asset-features)"/>
                </xsl:variable>
                <xsl:variable name="new-image-asset">
                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain,
                                                $existing-lot-asset, $new-sale-asset, $image-asset-features,
                                                $image-and-sale-asset-features,
                                                $dummy, $master-storage-item, $dummy, $workflow-step)"/>
                </xsl:variable>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="new-image-asset">
                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain,
                                                $existing-lot-asset, $existing-sale-asset, $image-asset-features,
                                                $image-and-sale-asset-features,
                                                $existing-lot-asset-related-with-user-relation, $master-storage-item, $dummy, $workflow-step)"/>
                </xsl:variable>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- cc - function to perform add/replace - cc -->
    <xsl:function name="ioi:add-replace">
        <xsl:param name="existing-image-asset"/>
        <xsl:param name="existing-lot-asset"/>
        <xsl:param name="asset-name-or-barcode-length"/>
        <xsl:param name="duplicate-image-asset"/>
        <xsl:param name="asset-name-or-barcode"/>
        <xsl:param name="unique-id"/>
        <xsl:param name="first-domain"/>
        <xsl:param name="second-domain"/>
        <xsl:param name="image-asset-features"/>
        <xsl:param name="image-and-sale-asset-features"/>
        <xsl:param name="relation-created-by-interface"/>
        <xsl:param name="existing-lot-asset-related-with-user-relation"/>
        <xsl:param name="existing-sale-asset"/>
        <xsl:param name="sale-number"/>
        <xsl:param name="sale-domain2"/>
        <xsl:param name="sale-location-code"/>
        <xsl:param name="image-physical-file-exists"/>
        <xsl:param name="workflow-step"/>
        <xsl:param name="master-storage-item"/>
        <xsl:message><xsl:value-of select="concat($log-prefix, ' Existing Image - ', $existing-image-asset/asset/@id)"/></xsl:message>
        <xsl:choose>
            <xsl:when test="not($existing-image-asset/asset/@id)">
                <xsl:message><xsl:value-of select="concat($log-prefix, 'Existing image asset missing')"/></xsl:message>
                <xsl:choose>
                    <!-- First condition -->
                    <xsl:when test="$asset-name-or-barcode-length = 8">
                        <!-- cc -<xsl:message>### ML - Existing Lot Asset-
                            <xsl:copy-of select="$existing-lot-asset"/>-
                            <xsl:value-of select="$existing-lot-asset"/>
                        </xsl:message>- cc -->
                        <xsl:choose>
                            <xsl:when test="$existing-lot-asset != ''">
                                <!-- Primary Image relation between lot and image asset -->
                                <xsl:choose>
                                    <xsl:when test="not($duplicate-image-asset)">
                                        <xsl:variable name="new-image-asset-created">
                                            <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $existing-lot-asset,
                                                                        $dummy, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                        </xsl:variable>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <!-- 5. Update lot asset here for handling of different relations -->
                                        <xsl:choose>
                                            <xsl:when test="$duplicate-image-asset and not($relation-created-by-interface)">
                                                <!-- existing Primary Image relation stays untouched, and the new image is related with a “user.” relation to the Lot Asset.  -->
                                                <xsl:variable name="new-image-asset-created">
                                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $existing-lot-asset,
                                                                                $dummy, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                                </xsl:variable>
                                            </xsl:when>
                                            <xsl:when test="$duplicate-image-asset != '' and $relation-created-by-interface">
                                                <!-- New image should get the “Primary Image” relation between image and lot asset. The relation “Primary Image” to the former Image Asset should be removed, and this former Image Asset will get a “user.” relation to the lot -->
                                                <xsl:variable name="new-image-asset-created">
                                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $existing-lot-asset,
                                                                                $dummy, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                                </xsl:variable>
                                                <!-- Update duplicate asset's relation with lot asset. Remove “Primary Image” relation and set "Assignment relation"-->
                                                <xsl:sequence select="sd:update-duplicate-image-asset($duplicate-image-asset,$existing-lot-asset)"/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="$existing-lot-asset-related-with-user-relation != ''">
                                <!-- cc -<xsl:message>### ML - Existing Lot Asset rel with user-
                                    <xsl:copy-of select="$existing-lot-asset-related-with-user-relation"/>-
                                    <xsl:value-of select="$existing-lot-asset-related-with-user-relation"/>
                                </xsl:message>- cc -->
                                <!-- Additional Image relation between lot and image asset -->
                                <xsl:variable name="new-image-asset-created">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $dummy,
                                                                $dummy, $image-asset-features, $image-and-sale-asset-features, $existing-lot-asset-related-with-user-relation, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:when>
                            <xsl:when test="$existing-sale-asset/@id != '' or $existing-sale-asset/asset/@id != '' or exists($existing-sale-asset)">
                                <!-- cc -<xsl:message>### ML - Existing Sale Asset-
                                    <xsl:copy-of select="$existing-sale-asset"/>-
                                    <xsl:value-of select="$existing-sale-asset"/>
                                </xsl:message>- cc -->
                                <!-- user relation between sale and image asset-->
                                <xsl:variable name="new-image-asset-created">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $dummy,
                                                                $existing-sale-asset, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:when>
                            <!-- Create new sale asset and attach the image with user relation -->
                            <xsl:when test="not($existing-sale-asset) and $sale-number != ''">
                                <xsl:message><xsl:value-of select="$log-prefix"/> No Sale and Sale Num not null</xsl:message>
                                <!-- Create sale asset first -->
                                <xsl:variable name="new-sale-asset">
                                    <xsl:sequence select="ioi:create-sale-asset($sale-location-code, $sale-number, $first-domain,
                                                                $sale-domain2, $image-and-sale-asset-features)"/>
                                </xsl:variable>
                                <xsl:variable name="new-image-asset">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain,
                                                                $dummy, $new-sale-asset, $image-asset-features,
                                                                $image-and-sale-asset-features,
                                                                $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:when>
                            <!-- 4. If sale number is empty just create the image asset without any relation -->
                            <xsl:otherwise>
                                <xsl:message><xsl:value-of select="$log-prefix"/> Just create image with no rel</xsl:message>
                                <xsl:variable name="new-image-asset-created">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $dummy,
                                                                                $dummy, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- Second condition -->
                    <xsl:when test="$asset-name-or-barcode-length gt 8">
                        <!-- cc -<xsl:variable name="barcode-with-first-8-digits" select="substring($asset-name-or-barcode, 1, 8)"/>
                        <xsl:variable name="existing-lot-asset" select="(cs:asset()[@canvas:jdeproperty.imageid like concat('*',$barcode-with-first-8-digits,'*')])[1]"/>- cc -->
                        <xsl:choose>
                            <xsl:when test="$existing-lot-asset">
                                <xsl:message><xsl:value-of select="$log-prefix"/>CHOOSE CONDITION 1</xsl:message>
                                <!-- user relation between lot and image asset -->
                                <xsl:choose>
                                    <xsl:when test="not($duplicate-image-asset)">
                                        <xsl:variable name="new-image-asset-created">
                                            <xsl:message><xsl:value-of select="$log-prefix"/>NOT DUPE IMAGE</xsl:message>
                                            <xsl:message><xsl:value-of select="$log-prefix"/>EXISTING LOT ASSET - <xsl:copy-of select="$existing-lot-asset"/></xsl:message>
                                            <xsl:message><xsl:value-of select="$log-prefix"/>EXISTING SALE ASSET - <xsl:copy-of select="$existing-sale-asset"/></xsl:message>
                                            <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, 
                                                                                        $second-domain, $existing-lot-asset,
                                                                                        $existing-sale-asset, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                        </xsl:variable>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:message><xsl:value-of select="$log-prefix"/>IS DUPE IMAGE</xsl:message>
                                        <!-- cc -<xsl:message>Relation-created-by-interface - <xsl:copy-of select="$relation-created-by-interface"/></xsl:message>
                                        <xsl:message>Dupe Image - <xsl:copy-of select="$duplicate-image-asset"/></xsl:message>
                                        <xsl:message>Values to test - <xsl:value-of select="exists($duplicate-image-asset//@id)"/> - <xsl:value-of select="not(exists($relation-created-by-interface//@id))"/></xsl:message>- cc -->
                                        <!-- 5. Update lot asset here for handling of different relations -->
                                        <xsl:choose>
                                            <xsl:when test="exists($duplicate-image-asset//@id) and not(exists($relation-created-by-interface//@id))">
                                                <xsl:message><xsl:value-of select="$log-prefix"/>NO REL CREATED BY INTERFACE</xsl:message>
                                                <!-- existing Primary Image relation stays untouched, and the new image is related with a “user.” relation to the Lot Asset.  -->
                                                <xsl:variable name="new-image-asset-created">
                                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $existing-lot-asset,
                                                                                                $existing-sale-asset, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                                </xsl:variable>
                                            </xsl:when>
                                            <xsl:when test="exists($duplicate-image-asset//@id) and exists($relation-created-by-interface//@id)">
                                                <xsl:message><xsl:value-of select="$log-prefix"/>REL CREATED BY INTERFACE</xsl:message>
                                                <!-- New image should get the “Primary Image” relation between image and lot asset. The relation “Primary Image” to the former Image Asset should be removed, and this former Image Asset will get a “user.” relation to the lot -->
                                                <xsl:variable name="new-image-asset-created">
                                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $existing-lot-asset,
                                                                                                $existing-sale-asset, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                                </xsl:variable>
                                                <!-- Update duplicate asset's relation with lot asset. Remove “Primary Image” relation and set "Assignment relation"-->
                                                <xsl:sequence select="sd:update-duplicate-image-asset($duplicate-image-asset,$existing-lot-asset)"/>
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="$existing-sale-asset/@id != '' or $existing-sale-asset/asset/@id != ''">
                                <xsl:message><xsl:value-of select="$log-prefix"/>CHOOSE CONDITION 2</xsl:message>
                                <!-- user relation between sale and image asset-->
                                <xsl:variable name="new-image-asset-created">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $dummy,
                                                                $existing-sale-asset, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:when>
                            <!-- Create new sale asset and attach the image with user relation -->
                            <xsl:when test="not($existing-sale-asset) and $sale-number != ''">
                                <xsl:message><xsl:value-of select="$log-prefix"/>CHOOSE CONDITION 3</xsl:message>
                                <!-- Create sale asset first -->
                                <xsl:variable name="new-sale-asset">
                                    <xsl:sequence select="ioi:create-sale-asset($sale-location-code, $sale-number, $first-domain,
                                                                $sale-domain2, $image-and-sale-asset-features)"/>
                                </xsl:variable>
                                <xsl:variable name="new-image-asset">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain,
                                                                $dummy, $new-sale-asset, $image-asset-features,
                                                                $image-and-sale-asset-features,
                                                                $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:when>
                            <!-- 4. If sale number is empty just create the image asset without any relation -->
                            <xsl:otherwise>
                                <xsl:message><xsl:value-of select="$log-prefix"/>CHOOSE Otherwise</xsl:message>
                                <xsl:variable name="new-image-asset-created">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $dummy,
                                                                $dummy, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- Third condition -->
                    <xsl:when test="$asset-name-or-barcode-length != 8">
                        <xsl:choose>
                            <xsl:when test="$existing-sale-asset/@id != '' or $existing-sale-asset/asset/@id != ''">
                                <!-- user relation between sale and image asset-->
                                <xsl:variable name="new-image-asset-created">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $dummy,
                                                                $existing-sale-asset, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:when>
                            <!-- Create new sale asset and attach the image with user relation -->
                            <xsl:when test="not($existing-sale-asset) and $sale-number != ''">
                                <!-- Create sale asset first -->
                                <xsl:variable name="new-sale-asset">
                                    <xsl:sequence select="ioi:create-sale-asset($sale-location-code, $sale-number, $first-domain,
                                                                $sale-domain2, $image-and-sale-asset-features)"/>
                                </xsl:variable>
                                <xsl:variable name="new-image-asset">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain,
                                                                $dummy, $new-sale-asset, $image-asset-features,
                                                                $image-and-sale-asset-features,
                                                                $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:when>
                            <!-- 4. If sale number is empty just create the image asset without any relation -->
                            <xsl:otherwise>
                                <xsl:variable name="new-image-asset-created">
                                    <xsl:sequence select="ioi:create-image-asset($asset-name-or-barcode, $first-domain, $second-domain, $dummy,
                                                                $dummy, $image-asset-features, $image-and-sale-asset-features, $dummy, $master-storage-item, $unique-id, $workflow-step)"/>
                                </xsl:variable>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <!-- Update asset -->
            <xsl:otherwise>
                <xsl:message><xsl:value-of select="concat($log-prefix, 'Existing image asset found!')"/></xsl:message>
                <!-- Todo -->
                <xsl:choose>
                    <!-- First condition -->
                    <xsl:when test="$asset-name-or-barcode-length = 8">
                        <xsl:choose>
                            <xsl:when test="$existing-lot-asset != ''">
                                <!-- cc -<xsl:message>### ML - Existing Lot Asset-
                                    <xsl:copy-of select="$existing-lot-asset"/>-
                                    <xsl:value-of select="$existing-lot-asset"/>
                                </xsl:message>- cc -->
                                <!-- Primary Image relation between lot and image asset -->
                                <xsl:choose>
                                    <xsl:when test="not($duplicate-image-asset)">
                                        <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step,(), $master-storage-item, $unique-id)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <!-- 5. Update lot asset here for handling of different relations -->
                                        <xsl:choose>
                                            <xsl:when test="$duplicate-image-asset != '' and not($relation-created-by-interface)">
                                                <!-- existing Primary Image relation stays untouched, and the new image is related with a "user." relation to the Lot Asset.-->
                                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                                <!-- Comment 001 -->
                                            </xsl:when>
                                            <xsl:when test="$duplicate-image-asset != '' and $relation-created-by-interface">
                                                <!-- New image should get the "Primary Image" relation between image and lot asset. The relation "Primary Image" to the former Image Asset should be removed, and this former Image Asset will get a "user." relation to the lot -->
                                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                                <!-- Comment 005 -->
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="$existing-lot-asset-related-with-user-relation != ''">
                                <!-- cc -<xsl:message>### ML - Existing Lot Asset related with user relation-
                                    <xsl:copy-of select="$existing-lot-asset-related-with-user-relation"/>-
                                    <xsl:value-of select="$existing-lot-asset-related-with-user-relation"/>
                                </xsl:message>- cc -->
                                <!-- Additional Image relation between lot and image asset -->
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $existing-lot-asset-related-with-user-relation, $master-storage-item, $unique-id)"/>
                            </xsl:when>
                            <xsl:when test="$existing-sale-asset/@id != '' or $existing-sale-asset/asset/@id != ''">
                                <!-- user relation between sale and image asset-->
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $existing-sale-asset, $master-storage-item, $unique-id)"/>
                                <!-- Comment 002 -->
                            </xsl:when>
                            <!-- Create new sale asset and attach the image with user relation -->
                            <xsl:when test="($existing-sale-asset/@id = '' or $existing-sale-asset/asset/@id = '') and $sale-number != ''">
                                <xsl:message><xsl:value-of select="concat($log-prefix, 'NO Existing SALE Asset')"/></xsl:message>
                                <!-- Create sale asset first -->
                                <xsl:variable name="new-sale-asset">
                                    <xsl:sequence select="ioi:create-sale-asset($sale-location-code, $sale-number, $first-domain,
                                                                $sale-domain2, $image-and-sale-asset-features)"/>
                                </xsl:variable>
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $new-sale-asset, $master-storage-item, $unique-id)"/>
                                <!-- Comment 003 -->
                            </xsl:when>
                            <!-- 4. If sale number is empty just create the image asset without any relation -->
                            <xsl:otherwise>
                                <xsl:message><xsl:value-of select="concat($log-prefix, 'Otherwise just create')"/></xsl:message>
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                <!-- Comment 004 -->
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- Second condition -->
                    <xsl:when test="$asset-name-or-barcode-length gt 8">
                        <xsl:message><xsl:value-of select="concat($log-prefix, 'The asset barcode length is greater than 8')"/></xsl:message>
                        <!-- cc -<xsl:variable name="barcode-with-first-8-digits" select="substring($asset-name-or-barcode, 1, 8)"/>
                        <xsl:variable name="existing-lot-asset" select="(cs:asset()[@canvas:jdeproperty.imageid like concat('*',$barcode-with-first-8-digits,'*')])[1]"/>- cc -->
                        <xsl:choose>
                            <xsl:when test="$existing-lot-asset != ''">
                                <xsl:message><xsl:value-of select="concat($log-prefix, 'There was an existing lot asset')"/></xsl:message>
                                <!-- user relation between lot and image asset -->
                                <xsl:choose>
                                    <xsl:when test="not($duplicate-image-asset)">
                                        <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                        <!-- Comment 001 -->
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <!-- 5. Update lot asset here for handling of different relations -->
                                        <xsl:choose>
                                            <xsl:when test="$duplicate-image-asset != '' and not($relation-created-by-interface)">
                                                <!-- existing Primary Image relation stays untouched, and the new image is related with a "user." relation to the Lot Asset.-->
                                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                                <!-- Comment 001 -->
                                            </xsl:when>
                                            <xsl:when test="$duplicate-image-asset != '' and $relation-created-by-interface">
                                                <!-- New image should get the "Primary Image" relation between image and lot asset. The relation "Primary Image" to the former Image Asset should be removed, and this former Image Asset will get a "user." relation to the lot -->
                                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                                <!-- Comment 001 -->
                                            </xsl:when>
                                        </xsl:choose>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="exists($existing-sale-asset)">
                                <xsl:message><xsl:value-of select="concat($log-prefix, 'There was an existing sale asset')"/></xsl:message>
                                <!-- user relation between sale and image asset-->
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $existing-sale-asset, $master-storage-item, $unique-id)"/>
                                <!-- Comment 002 -->
                            </xsl:when>
                            <!-- Create new sale asset and attach the image with user relation -->
                            <xsl:when test="not($existing-sale-asset) and $sale-number != ''">
                                <xsl:message><xsl:value-of select="concat($log-prefix, 'There was not existing sale asset but a sale number was found')"/></xsl:message>
                                <!-- Create sale asset first -->
                                <xsl:variable name="new-sale-asset">
                                    <xsl:sequence select="ioi:create-sale-asset($sale-location-code, $sale-number, $first-domain,
                                                                $sale-domain2, $image-and-sale-asset-features)"/>
                                </xsl:variable>
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $new-sale-asset/asset, $master-storage-item, $unique-id)"/>
                                <!-- Comment 003 -->
                            </xsl:when>
                            <!-- 4. If sale number is empty just create the image asset without any relation -->
                            <xsl:otherwise>
                                <xsl:message><xsl:value-of select="concat($log-prefix, 'WHY ARE WE HERE?!')"/> - <xsl:copy-of select="$existing-sale-asset"/> - <xsl:value-of select="$sale-number"/> - <xsl:copy-of select="$existing-lot-asset"/></xsl:message>
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                <!-- Comment 004 -->
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- Third condition -->
                    <xsl:when test="$asset-name-or-barcode-length != 8">
                        <xsl:choose>
                            <xsl:when test="$existing-sale-asset/@id != '' or $existing-sale-asset/asset/@id != ''">
                                <!-- user relation between sale and image asset-->
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $existing-sale-asset, $master-storage-item, $unique-id)"/>
                                <!-- Comment 002 -->
                            </xsl:when>
                            <!-- Create new sale asset and attach the image with user relation -->
                            <xsl:when test="not($existing-sale-asset) and $sale-number != ''">
                                <!-- Create sale asset first -->
                                <xsl:variable name="new-sale-asset">
                                    <xsl:sequence select="ioi:create-sale-asset($sale-location-code, $sale-number, $first-domain,
                                                                $sale-domain2, $image-and-sale-asset-features)"/>
                                </xsl:variable>
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $new-sale-asset/asset, $master-storage-item, $unique-id)"/>
                                <!-- Comment 003 -->
                            </xsl:when>
                            <!-- 4. If sale number is empty just create the image asset without any relation -->
                            <xsl:otherwise>
                                <xsl:sequence select="sd:update-image-asset($existing-image-asset, $image-physical-file-exists, $image-asset-features, $image-and-sale-asset-features, $workflow-step, (), $master-storage-item, $unique-id)"/>
                                <!-- Comment 004 -->
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- cc - function to handle archive routine - cc -->
    <xsl:function name="ioi:archive-routine">
        <xsl:param name="image-asset"/>
        <xsl:message>
            <xsl:value-of select="concat('   ###   Archiving Asset [', $image-asset/@name, ' - ', $image-asset/@id, ']')"/>
        </xsl:message>
        <xsl:variable name="checked-out-image-asset">
            <xsl:copy-of select="sd:checkout($image-asset)"/>
        </xsl:variable>
        <xsl:variable name="updated-image-asset-xml">
            <asset>
                <xsl:copy-of select="$checked-out-image-asset/asset/@*"/>
                <xsl:attribute name="wf_id" select="'35000'"/>
                <xsl:attribute name="wf_step" select="$workflow-steps/workflow[@id='35000']/step[@name='Archived Image']/@id"/>
                <xsl:attribute name="storage_state" select="'1'"/>
                <xsl:copy-of select="$checked-out-image-asset/asset/node()"/>
            </asset>
        </xsl:variable>
        <xsl:sequence select="sd:checkin($updated-image-asset-xml)"/>
    </xsl:function>

    <!-- cc - function to handle deletion routine - cc -->
    <xsl:function name="ioi:delete-routine">

        <xsl:param name="image-asset"/>
        <xsl:param name="wf-step-id"/>

        <!-- cc -<xsl:message>###  ML -
            <xsl:copy-of select="$image-asset"/>
        </xsl:message>- cc -->

        <xsl:variable name="placed" select="if(count($image-asset/parent_asset_rel[starts-with(@key, 'actual.') and @parent_currversion='0']) gt 0) then true() else false()"/>

        <xsl:variable name="checked-out-image-asset">
            <xsl:copy-of select="sd:checkout($image-asset)"/>
        </xsl:variable>

        <xsl:variable name="updated-image-asset-xml">
            <asset>
                <xsl:copy-of select="$checked-out-image-asset/asset/@*"/>
                <xsl:if test="$placed">
                    <xsl:attribute name="wf_id" select="'35000'"/>
                    <xsl:attribute name="wf_step" select="$wf-step-id"/>
                </xsl:if>
                <xsl:attribute name="deletion" select="1"/>
                <xsl:copy-of select="$checked-out-image-asset/asset/node() except $checked-out-image-asset/asset/parent_asset_element_rel[@key='actual.']"/>
                <xsl:for-each select="$checked-out-image-asset/asset/parent_asset_element_rel[@key='actual.']">
                    <parent_asset_element_rel>
                        <xsl:copy-of select="./@*"/>
                        <xsl:attribute name="iscancellation" select="'1'"/>
                    </parent_asset_element_rel>
                </xsl:for-each>
            </asset>
        </xsl:variable>

        <xsl:sequence select="sd:checkin($updated-image-asset-xml)"/>

    </xsl:function>

    <!-- cc - function to create new image assets - cc -->
    <xsl:function name="ioi:create-image-asset">
        <xsl:param name="asset-name-or-barcode"/>
        <xsl:param name="first-domain"/>
        <xsl:param name="second-domain"/>
        <xsl:param name="existing-lot-asset"/>
        <xsl:param name="new-sale-asset"/>
        <xsl:param name="image-asset-features"/>
        <xsl:param name="image-and-sale-asset-features"/>
        <xsl:param name="existing-lot-asset-related-with-user-relation"/>
        <xsl:param name="master-storage-item"/>
        <xsl:param name="unique-id"/>
        <xsl:param name="workflow-step"/>
        <xsl:variable name="asset-for-primary-image-routine" select="$new-sale-asset/asset/cs:child-rel()[@key='*']/.[starts-with(@type, 'picture.') and @wf_step = $workflow-steps/workflow/step[@name='Awaiting Image']/@id and @name = $asset-name-or-barcode and not(exists(storage_item[@key='master']))]"/>
        <xsl:choose>
            <!-- cc - check for primary image. - cc -->
            <xsl:when test="exists($asset-for-primary-image-routine)">
                <xsl:sequence select="ioi:primary-image-routine($asset-name-or-barcode, $first-domain, $second-domain, $existing-lot-asset, $new-sale-asset, $image-asset-features, $image-and-sale-asset-features, $existing-lot-asset-related-with-user-relation, $master-storage-item,$asset-for-primary-image-routine, $dummy, $unique-id)"/>
            </xsl:when>
            <!-- cc - No Primary Image...create. - cc -->
            <xsl:otherwise>
                <!-- cc -<xsl:message>### ML - NO PRIMARY IMAGE - sale asset -
                    <xsl:copy-of select="$new-sale-asset"/>
                </xsl:message>- cc -->
                <xsl:variable name="new-image-asset-xml">
                    <asset name="{$asset-name-or-barcode}" type="picture." domain="{$first-domain}" domain2="{$second-domain}" wf_id="35000" wf_step="{$workflow-step}" modified_by="{$process-user}" created_by="{$process-user}">
                        <xsl:if test="$unique-id and $unique-id != ''">
                            <xsl:attribute name="id_extern" select="concat('image:', $unique-id)"/>
                        </xsl:if>
                        <xsl:if test="$master-storage-item">
                            <xsl:copy-of select="$master-storage-item"/>
                            <asset_element idx="0" key="actual."/>
                        </xsl:if>
                        <xsl:if test="$existing-lot-asset">
                            <!-- cc - iterate assets in query and relate each one - cc -->
                            <xsl:apply-templates select="$existing-lot-asset" mode="relate-existing-lot-asset">
                                <!-- cc - check if existing child_asset_rel with key = user.main-picture. exists to assign feature on rel - cc -->
                                <xsl:with-param name="perform-child-rel-test" select="false()"/>
                            </xsl:apply-templates>
                        </xsl:if>
                        <xsl:if test="$existing-lot-asset-related-with-user-relation">
                            <!-- cc - iterate assets in query and relate each one - cc -->
                            <xsl:apply-templates select="$existing-lot-asset-related-with-user-relation" mode="relate-existing-lot-asset-related-with-user-relation"/>
                        </xsl:if>
                        <xsl:if test="($new-sale-asset/@id != '' or $new-sale-asset/asset/@id != '') and $existing-lot-asset = ''">
                            <parent_asset_rel key="user." parent_asset="{if($new-sale-asset/@id) then $new-sale-asset/@id else $new-sale-asset/asset/@id}"/>
                        </xsl:if>
                        <xsl:copy-of select="$image-asset-features/features/asset_feature"/>
                        <xsl:copy-of select="$image-and-sale-asset-features/features/asset_feature"/>
                    </asset>
                </xsl:variable>
                <xsl:sequence select="sd:checkin-new($new-image-asset-xml)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- cc - primary image update - cc -->
    <xsl:function name="ioi:primary-image-routine">
        <xsl:param name="asset-name-or-barcode"/>
        <xsl:param name="first-domain"/>
        <xsl:param name="second-domain"/>
        <xsl:param name="existing-lot-asset"/>
        <xsl:param name="new-sale-asset"/>
        <xsl:param name="image-asset-features"/>
        <xsl:param name="image-and-sale-asset-features"/>
        <xsl:param name="existing-lot-asset-related-with-user-relation"/>
        <xsl:param name="master-storage-item"/>
        <xsl:param name="asset-for-primary-image-routine"/>
        <xsl:param name="workflow-step"/>
        <xsl:param name="unique-id"/>
        <xsl:sequence select="sd:update-image-asset($asset-for-primary-image-routine, $dummy, $image-asset-features, $image-and-sale-asset-features, $workflow-step, $dummy, $master-storage-item, $unique-id)"/>
    </xsl:function>

    <!-- cc - function to create new sale assets - cc -->
    <xsl:function name="ioi:create-sale-asset">
        <xsl:param name="sale-location-code"/>
        <xsl:param name="sale-number"/>
        <xsl:param name="first-domain"/>
        <xsl:param name="sale-domain2"/>
        <xsl:param name="image-and-sale-asset-features"/>
        <!-- cc - craft sale asset xml and check in - cc -->
        <xsl:variable name="new-sale-asset-xml">
            <asset name="{concat($sale-location-code, $sale-number)}" type="sale." id_extern="{concat('sale:', $sale-number)}" domain="{$first-domain}" domain2="{$sale-domain2}">
                <xsl:copy-of select="$image-and-sale-asset-features/features/asset_feature"/>
            </asset>
        </xsl:variable>
        <xsl:sequence select="sd:checkin-new($new-sale-asset-xml)"/>
    </xsl:function>

    <!-- cc - function returning boolean on whether a file exists- cc -->
    <xsl:function name="ioi:check-for-file">
        <xsl:param name="file-url"/>
        <xsl:param name="file-name"/>
        <xsl:variable name="file-list">
            <cs:command name="com.censhare.api.io.List">
                <cs:param name="source" select="$file-url"/>
            </cs:command>
            <!-- cc - <cs:param name="filter" select="'*.jpg'"/>- cc -->
        </xsl:variable>
        <xsl:variable name="existing-file" select="$file-list/list/item[@name = $file-name and @type = 'file']"/>
        <file>
            <xsl:choose>
                <xsl:when test="exists($existing-file)">
                    <xsl:copy-of select="$existing-file/@*"/>
                    <xsl:attribute name="exists" select="'1'"/>
                    <xsl:attribute name="mimetype" select="ioi:get-mimetype-from-extension($file-name)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:attribute name="exists" select="'0'"/>
                </xsl:otherwise>
            </xsl:choose>
        </file>
    </xsl:function>

    <xsl:function name="ioi:get-mimetype-from-extension">
        <xsl:param name="file-name"/>
        <xsl:variable name="extension" select="concat('.', tokenize($file-name, '\.')[last()])"/>
        <xsl:value-of select="cs:master-data('mimetype')[@extension=$extension]/@mimetype"/>
    </xsl:function>
    <xsl:function name="cs:delete">
        <xsl:param name="assetXml"/>
        <cs:command name="com.censhare.api.assetmanagement.Delete">
            <cs:param name="source" select="$assetXml"/>
            <cs:param name="state" select="'physical'"/>
        </cs:command>
    </xsl:function>

    <xsl:function name="sd:create-timestamp">
        <xsl:param name="source"/>
        <!-- :matches('03/25/18 00:00:00', '^\d{2}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}') -->
        <xsl:if test="matches($source, '^\d{2}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}')">
            <xsl:variable name="date" select="substring($source, 4, 2)"/>
            <xsl:variable name="month" select="substring($source, 1, 2)"/>
            <xsl:variable name="year" select="concat('20', substring($source, 7, 2))"/>
            <xsl:variable name="timestring" select="substring($source, 10)"/>
            <xsl:variable name="datestring" select="concat($year, '-', $month, '-', $date)"/>
            <xsl:variable name="datetime" select="xs:dateTime(concat($datestring, 'T', $timestring))"/>
            <!-- formatting -->
            <xsl:sequence select="cs:format-date($datetime, 'dd.MM.yyyy-HH:mm:ss Z')"/>
        </xsl:if>
        <!-- :matches('2016/04/18 00:00:00', '^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}') -->
        <xsl:if test="matches($source, '^\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}')">
            <xsl:variable name="date" select="substring($source, 9, 2)"/>
            <xsl:variable name="month" select="substring($source, 6, 2)"/>
            <xsl:variable name="year" select="substring($source, 1, 4)"/>
            <xsl:variable name="timestring" select="substring($source, 12)"/>
            <xsl:variable name="datestring" select="concat($year, '-', $month, '-', $date)"/>
            <xsl:variable name="datetime" select="xs:dateTime(concat($datestring, 'T', $timestring))"/>
            <!-- formatting -->
            <xsl:sequence select="cs:format-date($datetime, 'dd.MM.yyyy-HH:mm:ss Z')"/>
        </xsl:if>
    </xsl:function>

    <xsl:function name="cen-func:make-timestamp">
        <xsl:param name="input"/>
        <!-- date part -->
        <!-- save <xsl:variable name="datestring" select=" if (fn:matches($input, '^\d{8}$')) then fn:replace($input, '(\d{4})(\d{2})(\d{2})', '$1-$2-$3') else ()"/>-->
        <xsl:variable name="datestring" select="if (fn:matches($input, '^\d{8}.*$')) then concat(substring($input,1,4), '-', substring($input,5,2), '-', substring($input,7,2)) else if (fn:matches($input, '^\d{2}.\d{2}.\d{4}$')) then concat(substring($input,7,4), '-', substring($input,4,2), '-', substring($input,1,2)) else ()"/>
        <!-- time part -->
        <xsl:variable name="timestring" select="'00:00:00'"/>
        <xsl:variable name="datetime" select="xs:dateTime(concat($datestring, 'T', $timestring))"/>
        <!-- formatting -->
        <xsl:sequence select="cs:format-date($datetime, 'dd.MM.yyyy-HH:mm:ss Z')"/>
    </xsl:function>

    <xsl:function name="sd:update-image-asset">
        <xsl:param name="existing-image-asset"/>
        <xsl:param name="image-physical-file-exists"/>
        <xsl:param name="image-asset-features"/>
        <xsl:param name="image-and-sale-asset-features"/>
        <xsl:param name="workflow-step"/>
        <xsl:param name="parent-asset-id"/>
        <xsl:param name="master-storage-item"/>
        <xsl:param name="unique-id"/>
        <!-- cc -<xsl:message>### ML - Parent Asset Id -
            <xsl:copy-of select="$parent-asset-id"/>-
            <xsl:value-of select="$parent-asset-id"/>
        </xsl:message>- cc -->
        <xsl:variable name="asset-feature-reset-starts-with" select="'canvas:ml.'"/>
        <xsl:variable name="asset-feature-reset-sequence" select="('censhare:asset.author', 'censhare:asset.keywords', 'canvas:sale.location')"/>
        <xsl:variable name="checked-out-image-asset">
            <xsl:copy-of select="sd:checkout($existing-image-asset)"/>
        </xsl:variable>
        <xsl:message><xsl:value-of select="concat($log-prefix, 'Image Physical File Exists - ')"/><xsl:copy-of select="$image-physical-file-exists"/></xsl:message>
        <xsl:variable name="updated-image-asset-xml">
            <asset>
                <xsl:copy-of select="$checked-out-image-asset/asset/@*"/>
                <xsl:attribute name="name" select="$image-asset-features/features/asset_feature[@feature='canvas:ml.name']/@value_string"/>
                <xsl:attribute name="modified_date" select="current-dateTime()"/>
                <xsl:attribute name="wf_step" select="$workflow-step"/>  <!-- cc - if ($image-physical-file-exists) then $workflow-step else '50' - cc -->
                <xsl:attribute name="id_extern" select="concat('image:', $unique-id)"/>
                <!-- cc - updated to use reset variables- cc -->
                <!-- jr - removed and replaced next 4 lines below per CAN-586 instructions - jr -->
                <!-- <xsl:copy-of select="$checked-out-image-asset/asset/node() 
                                            except ($checked-out-image-asset/asset/asset_feature[starts-with(@feature, $asset-feature-reset-starts-with)] |
                                            $checked-out-image-asset/asset/asset_feature[@feature = $asset-feature-reset-sequence] |
                                            $checked-out-image-asset/asset/storage_item[@key='master'])"/> -->
                <xsl:copy-of select="$checked-out-image-asset/asset/node() 
                                            except ($checked-out-image-asset/asset/asset_feature[starts-with(@feature, $asset-feature-reset-starts-with)] |
                                            $checked-out-image-asset/asset/asset_feature[@feature = $asset-feature-reset-sequence] |
                                            $checked-out-image-asset/asset/storage_item | $checked-out-image-asset/asset/asset_element)"/>
                <xsl:choose>
                    <xsl:when test="$master-storage-item">
                        <xsl:copy-of select="$master-storage-item"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="$checked-out-image-asset/asset/storage_item[@key='master']"/>
                    </xsl:otherwise>
                </xsl:choose>
                <asset_element idx="0" key="actual."/>
                <!-- cc - combine features to iterate and determine if adding / updating in new version / skipping - cc -->
                <xsl:variable name="combined-features">
                    <features>
                        <xsl:copy-of select="$image-asset-features/features/asset_feature"/>
                        <xsl:copy-of select="$image-and-sale-asset-features/features/asset_feature"/>
                    </features>
                </xsl:variable>
                <!-- cc - perform update routine instead of replace - cc -->
                <xsl:for-each select="$combined-features/features/asset_feature">
                    <xsl:variable name="new-feature-key" select="@feature"/>
                    <xsl:variable name="new-feature-value" select="cs:getFeatureValue(.)"/>
                    <xsl:variable name="existing-feature" select="$checked-out-image-asset/asset/asset_feature[@feature = $new-feature-key]"/>
                    <xsl:variable name="existing-feature-value" select="cs:getFeatureValue($existing-feature)"/>
                    <xsl:choose>
                        <xsl:when test="exists($existing-feature)">
                            <xsl:choose>
                                <xsl:when test="$existing-feature-value = $new-feature-value">
                                    <xsl:copy-of select="$existing-feature"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:if test="$new-feature-value != ''">
                                        <xsl:copy-of select="."/>
                                    </xsl:if>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="$new-feature-value != ''">
                                <xsl:copy-of select="."/>
                            </xsl:if>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:if test="not(empty($parent-asset-id))">
                    <!-- cc - we do not want to add any relations if the image is already assigned to a property. - cc -->
                    <xsl:if test="not(exists($checked-out-image-asset/asset/cs:parent-rel()[@key='*']/.[@type='product.']))">
                        <!-- cc - modified to iterate through ids passed and create a rel for each one. - cc -->
                        <xsl:for-each select="$parent-asset-id">
                            <xsl:variable name="par-id">
                                <xsl:choose>
                                    <xsl:when test="./asset/@id != ''">
                                        <xsl:value-of select="$parent-asset-id/asset/@id"/>
                                    </xsl:when>
                                    <xsl:when test="./@id != ''">
                                        <xsl:value-of select="$parent-asset-id/@id"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="."/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:if test="$par-id != '' and not(exists($checked-out-image-asset/asset/parent_asset_rel[@parent_asset = $par-id]))">
                                <parent_asset_rel parent_asset="{$par-id}" key="user."/>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:if>
            </asset>
        </xsl:variable>
        <!-- cc -<xsl:message>### ML - UPDATE IMAGE - <xsl:copy-of select="$updated-image-asset-xml"/></xsl:message>- cc -->
        <xsl:sequence select="sd:checkin($updated-image-asset-xml)"/>
    </xsl:function>

    <xsl:function name="sd:update-duplicate-image-asset">
        <xsl:param name="asset-xml"/>
        <xsl:param name="existing-lot-asset"/>
        <xsl:variable name="checked-out-xml">
            <xsl:copy-of select="sd:checkout($asset-xml)"/>
        </xsl:variable>
        <xsl:variable name="duplicate-image-asset-xml">
            <asset>
                <xsl:copy-of select="$checked-out-xml/asset/@*"/>
                <xsl:attribute name="modified_date" select="current-dateTime()"/>
                <xsl:copy-of select="$checked-out-xml/asset/node()"/>
                <parent_asset_rel key="user." parent_asset="{$existing-lot-asset/@id}"/>
            </asset>
        </xsl:variable>
        <xsl:sequence select="sd:checkin($duplicate-image-asset-xml)"/>
    </xsl:function>

    <!-- Get localized value of given feature element -->
    <xsl:function name="cs:getFeatureValue">
        <xsl:param name="featureElement" as="element(asset_feature)" />
        <xsl:variable name="type" select="cs:cachelookup('feature', '@key', $featureElement/@feature)/@value_type" />
        <xsl:choose>
            <!-- 0: No value -->
            <xsl:when test="$type='0'">
                <xsl:value-of select="''" />
            </xsl:when>
            <!-- 1: Hierarchical attribute (string) -->
            <xsl:when test="$type='1'">
                <xsl:value-of select="cs:cachelookup('feature_value', '@feature', $featureElement/@feature, '@value_key', $featureElement/@value_key)/@name" />
            </xsl:when>
            <!-- 2: Enumeration (string) -->
            <xsl:when test="$type='2'">
                <xsl:value-of select="cs:cachelookup('feature_value', '@feature', $featureElement/@feature, '@value_key', $featureElement/@value_key)/@name" />
            </xsl:when>
            <!-- 3: Integer (long) -->
            <xsl:when test="$type='3'">
                <xsl:value-of select="string-join((cs:format-number($featureElement/@value_long, '#,###'), cs:getFeatureUnitString($featureElement)), ' ')" />
            </xsl:when>
            <!-- 4: String -->
            <xsl:when test="$type='4'">
                <xsl:choose>
                    <!-- censhare URL -->
                    <xsl:when test="$featureElement/@feature='censhare:url'">
                        <a class="asset-info-link" target="_blank">
                            <xsl:attribute name="href" select="$featureElement/@value_string" />
                            <xsl:value-of select="$featureElement/@value_string" />
                        </a>
                    </xsl:when>
                    <!-- String -->
                    <xsl:otherwise>
                        <xsl:value-of select="$featureElement/@value_string" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <!-- 5: Timestamp -->
            <xsl:when test="$type='5'">
                <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'relative-short', 'short')" />
            </xsl:when>
            <!-- 6: Boolean -->
            <xsl:when test="$type='6'">
                <xsl:value-of select="if ($featureElement/@value_long='1') then '&#x2611;' else '&#x2610;'" />
            </xsl:when>
            <!-- 7: Double -->
            <xsl:when test="$type='7'">
                <xsl:value-of select="string-join((cs:format-number($featureElement/@value_double, '#,###.###'), cs:getFeatureUnitString($featureElement)), ' ')" />
            </xsl:when>
            <!-- 8: Integer pair -->
            <xsl:when test="$type='8'">
                <xsl:value-of select="string-join((concat(cs:format-number($featureElement/@value_long, '#,###'), '-', cs:format-number($featureElement/@value_long2, '#,###')), cs:getFeatureUnitString($featureElement)), ' ')" />
            </xsl:when>
            <!-- 9: Timestamp pair -->
            <xsl:when test="$type='9'">
                <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'relative-short', 'short'), '-', cs:format-date($featureElement/@value_timestamp2, 'relative-short', 'short'))" />
            </xsl:when>
            <!-- 10: Asset reference -->
            <xsl:when test="$type='10'">
                <xsl:variable name="refAsset" select="cs:get-asset($featureElement/@value_asset_id)" />
                <xsl:choose>
                    <xsl:when test="exists($refAsset)">
                        <xsl:value-of select="if ($refAsset/asset_feature[@feature='censhare:name' and @language=$uiLocale]) then $refAsset/asset_feature[@feature='censhare:name' and @language=$uiLocale]/@value_string else $refAsset/@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat('ID: ', $featureElement/@value_asset_id)" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <!-- 11: XML -->
            <xsl:when test="$type='11'">
                <xsl:value-of select="''" />
            </xsl:when>
            <!-- 12: Double pair -->
            <xsl:when test="$type='12'">
                <xsl:value-of select="string-join((concat(cs:format-number($featureElement/@value_double, '#,###.###'), '-', cs:format-number($featureElement/@value_double2, '#,###.###')), cs:getFeatureUnitString($featureElement)), ' ')" />
            </xsl:when>
            <!-- 13: Date -->
            <xsl:when test="$type='13'">
                <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'relative-short', 'none')" />
            </xsl:when>
            <!-- 14: Date pair -->
            <xsl:when test="$type='14'">
                <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'relative-short', 'none'), '-', cs:format-date($featureElement/@value_timestamp2, 'relative-short', 'none'))" />
            </xsl:when>
            <!-- 15: Time -->
            <xsl:when test="$type='15'">
                <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'none', 'short')" />
            </xsl:when>
            <!-- 16: Time pair -->
            <xsl:when test="$type='16'">
                <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'none', 'short'), '-', cs:format-date($featureElement/@value_timestamp2, 'none', 'short'))" />
            </xsl:when>
            <!-- 17: Year -->
            <xsl:when test="$type='17'">
                <xsl:value-of select="year-from-dateTime($featureElement/@value_timestamp)" />
            </xsl:when>
            <!-- 18: Year pair -->
            <xsl:when test="$type='18'">
                <xsl:value-of select="concat(year-from-dateTime($featureElement/@value_timestamp), '-', year-from-dateTime($featureElement/@value_timestamp2))" />
            </xsl:when>
            <!-- 19: Year/month -->
            <xsl:when test="$type='19'">
                <xsl:value-of select="concat(cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), ' ', year-from-dateTime($featureElement/@value_timestamp))" />
            </xsl:when>
            <!-- 20: Year/month pair -->
            <xsl:when test="$type='20'">
                <xsl:value-of select="concat(cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), ' ', year-from-dateTime($featureElement/@value_timestamp), '-', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)), ' ', year-from-dateTime($featureElement/@value_timestamp2))" />
            </xsl:when>
            <!-- 21: Month -->
            <xsl:when test="$type='21'">
                <xsl:value-of select="cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp))" />
            </xsl:when>
            <!-- 22: Month pair -->
            <xsl:when test="$type='22'">
                <xsl:value-of select="concat(cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), '-', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)))" />
            </xsl:when>
            <!-- 23: Month/day -->
            <xsl:when test="$type='23'">
                <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '. ', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)))" />
            </xsl:when>
            <!-- 24: Month/day pair -->
            <xsl:when test="$type='24'">
                <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '. ', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), '-', day-from-dateTime($featureElement/@value_timestamp2), '. ', cs:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)))" />
            </xsl:when>
            <!-- 25: Day -->
            <xsl:when test="$type='25'">
                <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '.')" />
            </xsl:when>
            <!-- 26: Day pair -->
            <xsl:when test="$type='26'">
                <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '.-', day-from-dateTime($featureElement/@value_timestamp2), '.')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'${none}'" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- Get localized value of given feature element -->
    <xsl:function name="cs:getFeatureUnitString" as="xs:string">
        <xsl:param name="featureElement" as="element(asset_feature)" />
        <xsl:value-of select="if ($featureElement/@value_unit) then cs:cachelookup('unit_set_rel', '@unit', $featureElement/@value_unit)/@unit else ()" />
    </xsl:function>

    <!-- Get localized name of given month (integer) -->
    <xsl:function name="cs:getMonthName" as="xs:string">
        <xsl:param name="month" as="xs:integer" />
        <xsl:choose>
            <xsl:when test="$month=1">${january}</xsl:when>
            <xsl:when test="$month=2">${february}</xsl:when>
            <xsl:when test="$month=3">${march}</xsl:when>
            <xsl:when test="$month=4">${april}</xsl:when>
            <xsl:when test="$month=5">${may}</xsl:when>
            <xsl:when test="$month=6">${june}</xsl:when>
            <xsl:when test="$month=7">${july}</xsl:when>
            <xsl:when test="$month=8">${august}</xsl:when>
            <xsl:when test="$month=9">${september}</xsl:when>
            <xsl:when test="$month=10">${october}</xsl:when>
            <xsl:when test="$month=11">${november}</xsl:when>
            <xsl:when test="$month=12">${december}</xsl:when>
        </xsl:choose>
    </xsl:function>

</xsl:stylesheet>