<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:my="http://www.censhare.com/xml/3.0.0/xpath-functions/my"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:html="http://www.w3.org/1999/xhtml">
    
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:variable name="column" select="3"/>

    <xsl:template match="/">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="my-page" page-height="297mm" page-width="210mm" margin-top="5mm" margin-bottom="5mm" margin-left="10mm" margin-right="10mm">
                    <fo:region-body margin-top="5mm" margin-bottom="5mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="my-page">
                <fo:flow flow-name="xsl-region-body" font-size=".75em"> 
                    <xsl:apply-templates/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="content">
        <xsl:message>   ###   CONTENT - <xsl:copy-of select="."/></xsl:message>
        <fo:block keep-with-previous="always">
            <fo:table table-layout="fixed" width="100%"  border-collapse="separate">
                <fo:table-body>
                    <xsl:apply-templates select="image[(position() -1 = 0) or ((position() - 1) mod $column = 0)]" mode="row"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="image" mode="row">
        <xsl:variable name="nbr" select="@id" as="xs:integer"/>
        <fo:table-row>
            <xsl:apply-templates select="."/>
            <xsl:if test="(($nbr - 1 = 0) ) or (($nbr -1) mod $column = 0)">
                <xsl:variable name="addtlCells" select="$column - 1"/>
                <xsl:variable name="numberOfSibs" select="count(following-sibling::image[@id le ($nbr + $addtlCells)])"/>
                <xsl:variable name="difference" select="$addtlCells - $numberOfSibs"/>
                <xsl:for-each select="following-sibling::image[@id le $nbr + $addtlCells]">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
                <xsl:if test="$difference gt 0">
                    <xsl:call-template name="empty">
                        <xsl:with-param name="i" select="1"/>
                        <xsl:with-param name="count" select="$difference"/>
                    </xsl:call-template>
                </xsl:if>
            </xsl:if>
        </fo:table-row>
    </xsl:template>

    <xsl:template name="empty">
        <xsl:param name="i"/>
        <xsl:param name="count"/>

        <xsl:if test="$i le $count">
            <fo:table-cell display-align="center">
                <fo:block/>
            </fo:table-cell>
            <xsl:call-template name="empty">
                <xsl:with-param name="i" select="$i + 1"/>
                <xsl:with-param name="count" select="$count"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template match="image">
        <!-- Riz Test -->
                
        <fo:table-cell display-align="center">
            
            <fo:block keep-together.within-page="always" float="left" padding-bottom="25pt">
                <fo:block text-align="center"> <!--   line-stacking-strategy="max-height" -->
                    <xsl:if test="not(url='')">
                    <fo:external-graphic 
                        height="50mm" 
                        width="50mm"
                        content-width="scale-down-to-fit"
                        content-height="scale-down-to-fit" 
                        scaling="uniform" 
                        src="url('{url}')"/>
                    </xsl:if>
                </fo:block>
                <xsl:apply-templates select="id|barcode|data"/>

            </fo:block>
            
        </fo:table-cell>
    </xsl:template>

    <xsl:template match="id">
        <fo:block text-align="center" font-weight="bold" font-size="10pt"><xsl:value-of select="."/></fo:block>
    </xsl:template>

    <xsl:template match="data">
        <fo:block text-align="center" font-size="10pt">
            <xsl:value-of select="./@value"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="barcode">
        <fo:block text-align="center">
            <fo:external-graphic
                width="300px"
                src="url('{./text()}')"/>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>

               <!--  height="100px"
                width="300px"
                content-width="scale-down-to-fit"
                content-height="scale-down-to-fit" 
                scaling="uniform"  -->


