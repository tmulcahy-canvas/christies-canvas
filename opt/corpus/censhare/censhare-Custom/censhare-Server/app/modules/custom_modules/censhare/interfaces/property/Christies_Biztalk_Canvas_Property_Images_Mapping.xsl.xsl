<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
 xmlns:canvas="censhare.com"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
 xmlns:xi="http://www.w3.org/2001/XInclude"
 xmlns:xs="http://www.w3.org/2001/XMLSchema"
 xmlns:my="http://www.censhare.com/my"
 xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">


<!-- ####################### -->	
	<xsl:function name="canvas:propertyPrimaryImageMapping">
		<xsl:param name="elementID" as="xs:string"/>
		<xsl:param name="saleNumber" as="xs:string"/>

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
			<xsl:value-of select="concat('elementid - ',$elementID)" />
			<xsl:value-of select="'&#xA;'" />
			<xsl:value-of select="concat('salenumber - ',$saleNumber)" />
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
		</xsl:message-->


		<xsl:variable name="existingPrimaryImageID" select="(cs:asset()[@censhare:asset.name=$elementID and @censhare:asset.type='picture.' and @censhare:asset.currversion=0])[1]/@id"/>
		<!--xsl:variable name="existingPrimaryImageID" select="(cs:asset()[@censhare:asset.name=$elementID)[0]/@id"/-->
		
		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
			<xsl:value-of select="concat('existingPrimaryImageID - ',$existingPrimaryImageID)" />
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
		</xsl:message-->

			<xsl:choose>
				<xsl:when test="not($existingPrimaryImageID) and ($elementID != '')">
					
				<xsl:variable name="fopInput">
					<xsl:element name="id">
						<xsl:value-of select="$elementID"/>
					</xsl:element>
				</xsl:variable>
				<cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out"/>
				
				<cs:command name="com.censhare.api.transformation.FopTransformation" returning="imagemaster">
					<cs:param name="stylesheet" select="'censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvasnewimagestoragecreation/storage/master/file'"/>
					<cs:param name="source" select="$fopInput"/>
					<cs:param name="dest" select="concat($out, $elementID, 'ImageStorageItem')"/>
					<cs:param name="xsl-parameters">
						<cs:param name= "asset" select="asset"/>
						<cs:param name="target-resolution" select="300"/>
					</cs:param>
				</cs:command>
				
				<xsl:variable name="imageVFSURL" select="concat($out, $elementID,'ImageStorageItem')"/>
				
				<cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="primaryImageResultAssetXml">
					<cs:param name="source">
						<asset name="{$elementID}" domain="root.christiesinternational.public." domain2="root.christiesinternational.emeri." type="picture." id_extern="image:{$elementID}" wf_id="35000" wf_step="5">
						        <storage_item key="master" element_idx="0" mimetype="application/pdf" filesys_name="assets" corpus:asset-temp-file-url="{$imageVFSURL}"/>
							<asset_element key="actual."/>
						</asset>
					</cs:param>
				</cs:command>
				
			<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
			<xsl:value-of select="'primaryImageResultAssetXml - &#xA;'" />
			<xsl:copy-of select="$primaryImageResultAssetXml" />
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
			</xsl:message-->


		<xsl:variable name="PrimaryAssetID" select="$primaryImageResultAssetXml/@id" />

    <cs:command name="com.censhare.api.io.CloseVirtualFileSystem" returning="outDir">
      <cs:param name="id" select="$out"/>
    </cs:command>

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
			<xsl:value-of select="concat('PrimaryAssetID - ',$PrimaryAssetID)" />
			<xsl:value-of select="'&#xA;&#xA;&#xA;'" />
		</xsl:message-->

					<xsl:if test="$PrimaryAssetID != ''">
						<xsl:element name="child_asset_rel">
							<xsl:attribute name="child_asset" select="$PrimaryAssetID"/>
							<xsl:attribute name="key" select="'user.main-picture.'"/>
						</xsl:element>
						<xsl:element name="child_asset_rel">
							<xsl:attribute name="child_asset" select="$PrimaryAssetID"/>
							<xsl:attribute name="key" select="'user.'"/>
						</xsl:element>
						<asset_feature feature="canvas:jdeproperty.imageid.internal" value_string="{$PrimaryAssetID}" />
						<xsl:copy-of select="canvas:removeExistingRelationsToSale($PrimaryAssetID,$saleNumber)"/>
					</xsl:if>

		</xsl:when>
		<xsl:otherwise>
			<!--xsl:variable name="PrimaryAssetID" select="cs:asset()[@censhare:asset.name=$elementID and @censhare:asset.type='picture.' and @censhare:asset.currversion=0]/@id" />

			<xsl:if test="$PrimaryAssetID != ''">
				<xsl:element name="child_asset_rel">
					<xsl:attribute name="child_asset" select="$PrimaryAssetID"/>
					<xsl:attribute name="key" select="'user.main-picture.'"/>
				</xsl:element>
				<xsl:element name="child_asset_rel">
					<xsl:attribute name="child_asset" select="$PrimaryAssetID"/>
					<xsl:attribute name="key" select="'user.'"/>
				</xsl:element>
				<xsl:copy-of select="canvas:removeExistingRelationsToSale($PrimaryAssetID,$saleNumber)"/>
			</xsl:if-->

				<xsl:variable name="existingPrimaryImageXMLs" select="cs:asset()[@censhare:asset.name=$elementID and @censhare:asset.type='picture.' and @censhare:asset.currversion=0]"/>
				<xsl:variable name="highestPrimaryID">
					<xsl:value-of select="substring-before(string(max($existingPrimaryImageXMLs/@id)),'.')" />
				</xsl:variable>

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA; existingPrimaryImageXMLs '" />
					<xsl:copy-of select="$existingPrimaryImageXMLs" />
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:copy-of select="$existingPrimaryImageXMLs[@id = $highestPrimaryID]" />
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:copy-of select="$existingPrimaryImageXMLs[@id != $highestPrimaryID]" />
				</xsl:message-->

				<xsl:if test="$existingPrimaryImageXMLs">
					<xsl:element name="child_asset_rel">
						<xsl:attribute name="child_asset" select="$highestPrimaryID"/>
						<xsl:attribute name="key" select="'user.main-picture.'"/>
					</xsl:element>
					<!--xsl:element name="child_asset_rel">
						<xsl:attribute name="child_asset" select="$highestPrimaryID"/>
						<xsl:attribute name="key" select="'user.'"/>
					</xsl:element-->
					<xsl:copy-of select="canvas:removeExistingRelationsToSale($highestPrimaryID,$saleNumber)"/>

					<!--xsl:for-each select="$existingPrimaryImageXMLs[@id!=$highestPrimaryID]">
						<xsl:element name="child_asset_rel">
							<xsl:attribute name="child_asset" select="./@id"/>
							<xsl:attribute name="key" select="'user.'"/>
						</xsl:element>
						<asset_feature feature="canvas:jdeproperty.imageid.internal" value_string="{./@id}" />
						<xsl:copy-of select="canvas:removeExistingRelationsToSale(./@id,$saleNumber)"/>
					</xsl:for-each-->

				</xsl:if>


		</xsl:otherwise>

		</xsl:choose>

	</xsl:function>



<!-- #################### -->
	<xsl:function name="canvas:propertyAdditionalImageMapping">
		<xsl:param name="mappingElementstoFeatures" as="node()"/>
        <xsl:param name="elementName" as="xs:string"/>
        <xsl:param name="elementID" as="xs:string"/>
        <xsl:param name="saleNumber" as="xs:string"/>
        <xsl:param name="primaryID" as="xs:string"/>
        
     <xsl:variable name="confMappingFeature" select="$mappingElementstoFeatures//*[local-name()=$elementName]" />

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; elementID '"/>
					<xsl:value-of select="$elementID" />
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->

		<!--xsl:variable name="additionalImageInfo"-->
			<xsl:for-each select="tokenize($elementID,' ')" >
				<xsl:variable name="imageID" select="." />

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; imageID '"/>
					<xsl:value-of select="$imageID" />
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->

<!-- vec updated 18 May 2018 -->
				<!--xsl:if test="normalize-space($imageID) != normalize-space($primaryID)"-->
					<xsl:variable name="addtionalImageID" select="cs:asset()[@censhare:asset.name=normalize-space($imageID) and @censhare:asset.currversion=0]/@id" />

					<xsl:variable name="addtionalImageAssetXML">
						<!-- do not execute again for the primary id which is already done -->
						<xsl:if test="normalize-space($imageID) != normalize-space($primaryID)">
						<xsl:if test="not($addtionalImageID)">
						<cs:command name="com.censhare.api.assetmanagement.CheckInNew" returning="addtionalImageResultAssetXML">
							<cs:param name="source">
								<asset name="{$imageID}" domain="root.christiesinternational.public." domain2="root.christiesinternational.emeri." type="picture." id_extern="image:{$imageID}" wf_id="35000" wf_step="5">
								</asset>
							</cs:param>
						</cs:command>
						<xsl:copy-of select="$addtionalImageResultAssetXML" />
						</xsl:if>
						</xsl:if>
					</xsl:variable>

					<xsl:variable name="searchImagesContext" select="concat($imageID,'*')" />

					<xsl:variable name="additonalImageAssets">
						<xsl:choose>
							<xsl:when test="$addtionalImageAssetXML">
								<xsl:copy-of select="$addtionalImageAssetXML" />
								<xsl:copy-of select="cs:asset()[@censhare:asset.name like $searchImagesContext and @censhare:asset.type = 'picture.' and @censhare:asset.currversion=0]" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:copy-of select="cs:asset()[@censhare:asset.name like $searchImagesContext and @censhare:asset.type = 'picture.' and @censhare:asset.currversion=0]" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; additonalImageAssets '"/>
					<xsl:copy-of select="$additonalImageAssets" />
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; OUTSIDE &#xA;'"/>
					<xsl:value-of select="./@name" />
			<xsl:value-of select="'&#xA;'"/>
			<xsl:value-of select="$imageID"/>
			<xsl:value-of select="'&#xA;'"/>
			<xsl:value-of select="$primaryID"/>
			<xsl:value-of select="'&#xA;'"/>
							<child_asset_rel child_asset="{./@id}" key="user." />
			<xsl:value-of select="'&#xA;'"/>
							<asset_feature feature="canvas:jdeproperty.imageid.internal" value_string="{./@id}" />
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->
		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; INSIDE &#xA;'"/>
					<xsl:value-of select="./@name" />
			<xsl:value-of select="'&#xA;'"/>
			<xsl:value-of select="$imageID"/>
			<xsl:value-of select="'&#xA;'"/>
			<xsl:value-of select="$primaryID"/>
			<xsl:value-of select="'&#xA;'"/>
							<child_asset_rel child_asset="{./@id}" key="user." />
			<xsl:value-of select="'&#xA;'"/>
							<asset_feature feature="canvas:jdeproperty.imageid.internal" value_string="{./@id}" />
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->

					<xsl:if test="$additonalImageAssets/asset">
						<xsl:for-each select="$additonalImageAssets/asset">

							<!-- check primary so that this is not related again as this is done as a part of the primary image compilation -->
							<!--xsl:if test="$primaryID != ./@name"-->
								<child_asset_rel child_asset="{./@id}" key="user." />
								<asset_feature feature="canvas:jdeproperty.imageid.internal" value_string="{./@id}" />
							<xsl:copy-of select="canvas:removeExistingRelationsToSale(./@id,$saleNumber)"/>
							<!--/xsl:if-->

						</xsl:for-each>
					</xsl:if>

				<!--/xsl:if-->

			</xsl:for-each>
		<!--/xsl:variable-->

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; additionalImageInfo'"/>
			<xsl:copy-of select="$additionalImageInfo"/>
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->

	</xsl:function>


<!-- #################### REMOVE IMAGE SALE MAPPINGS-->
	<xsl:function name="canvas:removeExistingRelationsToSale">
        <xsl:param name="elementID" />
        <xsl:param name="saleNumber" />

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; elementID '"/>
			<xsl:value-of select="$elementID"/>
			<xsl:value-of select="'&#xA;&#xA; saleNumber '"/>
			<xsl:value-of select="$saleNumber"/>
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->


        <!--xsl:variable name="saleAssetXML" select="(cs:asset()[@censhare:asset.id_extern=$saleNumber])[1]" />
        <xsl:variable name="imageAssetID" select="(cs:asset('sql=true')[@censhare:asset.name=$elementID])[1]/@id" />

		<xsl:choose>
        	<xsl:when test="exists($saleAssetXML/child_asset_rel[@child_asset=$imageAssetID])">
				<cs:command name="com.censhare.api.assetmanagement.Update" returning="saleUpdateXML">
					<cs:param name="source">
						<asset id="{$saleAssetXML/@id}">
							<xsl:copy-of select="$saleAssetXML/@*"/>
							<xsl:copy-of select="$saleAssetXML/node() except ($saleAssetXML/child_asset_rel[@child_asset=$imageAssetID])"/>
						</asset>
					</cs:param>
				</cs:command>
        	</xsl:when>
        	<xsl:otherwise>
        	</xsl:otherwise>
		</xsl:choose-->

        <!--xsl:variable name="saleAssetXML" select="(cs:asset('sql=true')[@censhare:asset.id_extern=$saleNumber])[1]" /-->
				<xsl:variable name="saleAssetXML" select="cs:asset()[@censhare:asset.id_extern=concat('sale:',$saleNumber) and @censhare:asset.currversion=0]" />
        <!--xsl:variable name="imageAssetID" select="(cs:asset('sql=true')[@censhare:asset.name=$elementID])[1]/@id" /-->
        <!--xsl:variable name="imageAssetXML" select="(cs:asset()[@censhare:asset.name=$elementID and @censhare:asset.type='picture.' and @censhare:asset.currversion=0])[1]" /-->
        <xsl:variable name="imageAssetXML" select="cs:asset()[@censhare:asset.id=$elementID and @censhare:asset.type='picture.' and @censhare:asset.currversion=0]" />

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA; saleAssetXML '"/>
			<xsl:copy-of select="$saleAssetXML"/>
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message>
		<xsl:message>
			<xsl:value-of select="'&#xA;&#xA; imageAssetXML '"/>
			<xsl:copy-of select="$imageAssetXML"/>
			<xsl:value-of select="'&#xA;&#xA;'"/>
		</xsl:message-->

		<xsl:choose>
        	<xsl:when test="exists($imageAssetXML/parent_asset_rel[@parent_asset=$saleAssetXML/@id])">
				<cs:command name="com.censhare.api.assetmanagement.Update" returning="saleUpdateXML">
					<cs:param name="source">
						<asset id="{$imageAssetXML/@id}">
							<xsl:copy-of select="$imageAssetXML/@*"/>
							<xsl:copy-of select="$imageAssetXML/node() except ($imageAssetXML/parent_asset_rel[@parent_asset=$saleAssetXML/@id])"/>
						</asset>
					</cs:param>
				</cs:command>
        	</xsl:when>
        	<xsl:otherwise>
        	</xsl:otherwise>
		</xsl:choose>



	</xsl:function>


<!-- #################### -->
	<xsl:function name="canvas:imageCheckAndMap">
		<xsl:param name="propertyAsset" as="node()"/>
		<xsl:param name="mappingElementstoFeatures" as="node()"/>
		<xsl:param name="currentItemXML" as="node()"/>
		<xsl:param name="elementID" as="xs:string"/>
	
			<!--xsl:message>
				<xsl:value-of select="'&#xA;&#xA;imageCheckAndMap'"/>
				<xsl:copy-of select="$propertyAsset"/>
				<xsl:value-of select="'&#xA;&#xA;'"/>
				<xsl:copy-of select="$mappingElementstoFeatures"/>
				<xsl:value-of select="'&#xA;&#xA;'"/>
				<xsl:copy-of select="$currentItemXML"/>
				<xsl:value-of select="'&#xA;&#xA;'"/>
				<xsl:value-of select="$elementID"/>
				<xsl:value-of select="'&#xA;&#xA;'"/>
			</xsl:message-->

		<!--xsl:if test="exists($propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid'])"-->
		<xsl:if test="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string">
			<xsl:variable name="existingPrimaryImageID" select="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string" />

			<!--xsl:message>
				<xsl:value-of select="'&#xA;&#xA;existingPrimaryImageID - '"/>
				<xsl:value-of select="$existingPrimaryImageID"/>
				<xsl:value-of select="'&#xA;&#xA;'"/>
			</xsl:message-->

			<!--xsl:variable name="imageAssetXML" select="cs:asset()[@censhare:asset.name=$existingPrimaryImageID]" /--><!-- venkat -->
			<!-- changed by venkat 07th Aug 2018 -->
			<!--xsl:variable name="imageAssetXML" select="cs:asset()[@censhare:asset.id=$propertyAsset/child_asset_rel[@key='user.main-picture.']/@child_asset]" /-->
			<xsl:variable name="imageAssetID" select="cs:asset()[@censhare:asset.name=$existingPrimaryImageID]/parent_asset_rel[@key='user.main-picture.' and @parent_asset=$propertyAsset/@id]/@child_asset" />
			<xsl:variable name="imageAssetXML" select="cs:asset()[@censhare:asset.id=$imageAssetID]" />
				
			<!--xsl:message>
				<xsl:value-of select="'&#xA;&#xA; imageAsset - '"/>
				<xsl:value-of select="$imageAssetID"/>
				<xsl:value-of select="'&#xA;&#xA;'"/>
				<xsl:copy-of select="$imageAssetXML"/>
				<xsl:value-of select="'&#xA;&#xA;'"/>
			</xsl:message-->

			<!--excludeElements><xsl:text>child_asset_rel[@child_asset='</xsl:text><xsl:value-of select="$imageAssetXML/@id" /><xsl:text>']</xsl:text></excludeElements-->

			<excludeElements childAsset="{$imageAssetXML/@id}" key="user.main-picture." />

			
			<xsl:variable name="saleAssetXML" select="cs:asset()[@censhare:asset.id_extern=concat('sale:',normalize-space($currentItemXML/SaleNumber))]" />
			
			<xsl:choose>
				<xsl:when test="$saleAssetXML/cs:child-rel()[@key='target.']">
					<xsl:variable name="childAssetXMLs" select="$saleAssetXML/cs:child-rel()[@key='target.']" />
						<xsl:if test="count($childAssetXMLs/@type='product.' and $childAssetXMLs/child_asset_rel[@key='user.main-picture.' and @child_asset=$imageAssetXML/@id]) > 1">
							<xsl:variable name="propertyXMLs">
								<xsl:for-each select="$childAssetXMLs/@type='product.' and $childAssetXMLs/child_asset_rel[@key='user.main-picture.' and @child_asset=$imageAssetXML/@id]">
									<xsl:copy-of select="." />
								</xsl:for-each>
							</xsl:variable>
							<xsl:if test="not(count($propertyXMLs/parent_asset_element_rel[@child_asset=./@id and key='actual.']) > 1)">
								<xsl:copy-of select="canvas:primaryImageLayoutRemoveRelation($saleAssetXML,$imageAssetXML)" />
							</xsl:if>
						</xsl:if>
				</xsl:when>
				<xsl:otherwise>
					<xsl:copy-of select="canvas:primaryImageLayoutRemoveRelation($saleAssetXML,$imageAssetXML)" />
				</xsl:otherwise>
			</xsl:choose>

			<!--xsl:copy-of select="canvas:removeExistingRelationsToSale($elementID,normalize-space($currentItemXML/SaleNumber))"/-->
			<xsl:variable name="removedSaleRelationsXML">
				<!--xsl:copy-of select="canvas:removeExistingRelationsToSale($elementID,normalize-space($currentItemXML/SaleNumber))"/-->
				<xsl:copy-of select="canvas:removeExistingRelationsToSale($imageAssetXML/@id,normalize-space($currentItemXML/SaleNumber))"/>
			</xsl:variable>

		</xsl:if>
		
	</xsl:function>


<!-- #################### -->
	<xsl:function name="canvas:primaryImageLayoutRemoveRelation">
		<xsl:param name="saleAssetXML"/>
		<xsl:param name="imageAssetXML"/>
		<!--xsl:param name="elementID"/-->
		
		<xsl:for-each select="$saleAssetXML/cs:parent-rel()[@key='user.']">
			<xsl:if test="@type='issue.section.'">
				<xsl:variable name="parentAssetXML" select="." />

				<xsl:if test="exists($parentAssetXML/cs:child-rel()[@key='user.'])">
					<xsl:for-each select="$parentAssetXML/cs:child-rel()[@key='user.']">
						<xsl:if test="@type='layout.'">
							<xsl:variable name="layoutAssetXML" select="." />

								<xsl:if test="$layoutAssetXML/child_asset_rel[@child_asset=$imageAssetXML/@id and @key='actual.']">

									<cs:command name="com.censhare.api.assetmanagement.Update" returning="updatedXML">
										<cs:param name="source">
											<asset id="{$imageAssetXML/@id}">
												<xsl:copy-of select="$imageAssetXML/@*"/>
												<xsl:copy-of select="$imageAssetXML/node() except ($imageAssetXML/parent_asset_rel[@parent_asset=$layoutAssetXML/@id and @key='actual.'],$imageAssetXML/parent_asset_element_rel[@parent_asset=$layoutAssetXML/@id and @key='actual.'])"/>

												<xsl:for-each select="$imageAssetXML/parent_asset_rel[@parent_asset=$layoutAssetXML/@id and @key='actual.']">
													<parent_asset_rel>
						        				        <xsl:copy-of select="./@*"/>
														<xsl:attribute name="iscancellation" select="'1'"/>
													</parent_asset_rel>
												</xsl:for-each>
												<xsl:for-each select="$imageAssetXML/parent_asset_element_rel[@parent_asset=$layoutAssetXML/@id and @key='actual.']">
													<parent_asset_element_rel>
						        				        <xsl:copy-of select="./@*"/>
														<xsl:attribute name="iscancellation" select="'1'"/>
													</parent_asset_element_rel>
												</xsl:for-each>
											</asset>
										</cs:param>
									</cs:command>  
									
								</xsl:if>

						</xsl:if>
					</xsl:for-each>
				</xsl:if>

			</xsl:if>
		</xsl:for-each>

	</xsl:function>


			
</xsl:stylesheet>
