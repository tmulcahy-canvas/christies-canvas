<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:my="http://www.censhare.com/xml/3.0.0/xpath-functions/my"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xi="http://www.w3.org/2001/XInclude"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:html="http://www.w3.org/1999/xhtml">

  <!-- process common attributes and children -->
  <xsl:template name="process-common-attributes-and-children">
    <xsl:call-template name="process-common-attributes"/>
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template name="process-common-attributes">
    <xsl:attribute name="role">
      <xsl:value-of select="local-name()"/>
    </xsl:attribute>
    <xsl:choose>
      <xsl:when test="@xml:lang">
        <xsl:attribute name="xml:lang">
          <xsl:value-of select="@xml:lang"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:when test="@lang">
        <xsl:attribute name="xml:lang">
          <xsl:value-of select="@lang"/>
        </xsl:attribute>
      </xsl:when>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="@id">
        <xsl:attribute name="id">
          <xsl:value-of select="@id"/>
        </xsl:attribute>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="@align">
      <xsl:attribute name="align">
        <xsl:value-of select="@align"/>
      </xsl:attribute>
    </xsl:if>
  </xsl:template>
  <!-- Ignore Website, App and interactive Contents -->
  <xsl:template match="website|app|video|audio|print"/>
  <!-- Includes -->
  <xsl:template match="xi:include">
    <xsl:choose>
      <xsl:when test="cs:override">
        <i><xsl:apply-templates select="cs:override"/></i>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="doc(@href)/*"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- Block-level -->
  <xsl:template match="title">
    <fo:block xsl:use-attribute-sets="article-titles">
      <xsl:choose>
        <xsl:when test="text() = 'PRO'">
          PROVENANCE
        </xsl:when>
        <xsl:when test="text() = 'ENGR'">
          ENGRAVED
        </xsl:when>
        <xsl:when test="text() = 'LIT'">
          LITERATURE
        </xsl:when>
        <xsl:when test="text() = 'EXH'">
          EXHIBITED
        </xsl:when>
        <xsl:when test="text() = 'EXC'">
          EXTERNAL CONDITION
        </xsl:when>
        <!-- cc - commented out....NO titles should filter through unless it is the four above - cc -->
        <!-- cc -<xsl:otherwise>
          <xsl:call-template name="process-common-attributes-and-children"/>
        </xsl:otherwise>- cc -->
      </xsl:choose>
    </fo:block>
  </xsl:template>
  <xsl:template match="subtitle">
    <fo:block xsl:use-attribute-sets="h2">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="hdivider">
    <fo:block xsl:use-attribute-sets="hr">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="strapline">
    <fo:block xsl:use-attribute-sets="hr">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="subheadline-1">
    <fo:block xsl:use-attribute-sets="h2">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="subheadline-2">
    <fo:block xsl:use-attribute-sets="h3">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="callout | callout-box">
    <fo:block xsl:use-attribute-sets="h4">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="paragraph">
    <xsl:choose>
      <xsl:when test="if(ancestor::content/title/text() = 'EXC' or ancestor::content/title/text() ='PRO' or ancestor::content/title/text() = 'LIT' or ancestor::content/title/text() ='EXH') then true else false">
        <fo:block use-attribute-sets="light-small-paragraph">
          <xsl:call-template name="process-common-attributes-and-children"/>
        </fo:block>
      </xsl:when>
      <xsl:otherwise>
        <fo:block>
          <xsl:call-template name="process-common-attributes-and-children"/>
        </fo:block>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template match="P">
    <fo:inline xsl:use-attribute-sets="P">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="E">
    <fo:inline xsl:use-attribute-sets="E">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="Z">
    <fo:inline xsl:use-attribute-sets="Z">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="pre">
    <fo:block xsl:use-attribute-sets="pre">
      <xsl:value-of select="."/>
    </fo:block>
  </xsl:template>
  <xsl:template match="center">
    <fo:block text-align="center">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="bullet-list">
    <fo:list-block xsl:use-attribute-sets="ul">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:list-block>
  </xsl:template>
  <xsl:template match="item/bullet-list" priority="0.5">
    <fo:list-block xsl:use-attribute-sets="ul-nested">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:list-block>
  </xsl:template>
  <xsl:template match="enumeration">
    <fo:list-block xsl:use-attribute-sets="ol">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:list-block>
  </xsl:template>
  <xsl:template match="item/enumeration" priority="0.5">
    <fo:list-block xsl:use-attribute-sets="ol-nested">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:list-block>
  </xsl:template>
  <xsl:template match="bullet-list/item">
    <fo:list-item xsl:use-attribute-sets="ul-li">
      <xsl:call-template name="process-ul-li"/>
    </fo:list-item>
  </xsl:template>
  <xsl:template name="process-ul-li">
    <xsl:call-template name="process-common-attributes"/>
    <fo:list-item-label end-indent="label-end()" text-align="end" wrap-option="no-wrap">
      <fo:block>
        <xsl:variable name="depth" select="count(ancestor::bullet-list)"/>
        <xsl:choose>
          <xsl:when test="$depth = 1">
            <fo:inline xsl:use-attribute-sets="ul-label-1">
              <xsl:value-of select="$ul-label-1"/>
            </fo:inline>
          </xsl:when>
          <xsl:when test="$depth = 2">
            <fo:inline xsl:use-attribute-sets="ul-label-2">
              <xsl:value-of select="$ul-label-2"/>
            </fo:inline>
          </xsl:when>
          <xsl:otherwise>
            <fo:inline xsl:use-attribute-sets="ul-label-3">
              <xsl:value-of select="$ul-label-3"/>
            </fo:inline>
          </xsl:otherwise>
        </xsl:choose>
      </fo:block>
    </fo:list-item-label>
    <fo:list-item-body start-indent="body-start()">
      <fo:block>
        <xsl:apply-templates/>
      </fo:block>
    </fo:list-item-body>
  </xsl:template>
  <xsl:template match="enumeration/item">
    <fo:list-item xsl:use-attribute-sets="ol-li">
      <xsl:call-template name="process-ol-li"/>
    </fo:list-item>
  </xsl:template>
  <xsl:template name="process-ol-li">
    <xsl:call-template name="process-common-attributes"/>
    <fo:list-item-label end-indent="label-end()" text-align="end" wrap-option="no-wrap">
      <fo:block>
        <xsl:variable name="depth" select="count(ancestor::enumeration)"/>
        <xsl:choose>
          <xsl:when test="$depth = 1">
            <fo:inline xsl:use-attribute-sets="ol-label-1">
              <xsl:value-of select="concat(count(preceding-sibling::item)+1, '. ')"/>
            </fo:inline>
          </xsl:when>
          <xsl:when test="$depth = 2">
            <fo:inline xsl:use-attribute-sets="ol-label-2">
              <xsl:value-of select="concat(count(ancestor::item[1]/preceding-sibling::item)+1, '.' ,count(preceding-sibling::item)+1, '. ')"/>
            </fo:inline>
          </xsl:when>
          <xsl:otherwise>
            <fo:inline xsl:use-attribute-sets="ol-label-3">
              <xsl:value-of select="concat(count(ancestor::item[2]/preceding-sibling::item)+1, '.', count(ancestor::item[1]/preceding-sibling::item)+1, '.' ,count(preceding-sibling::item)+1, '. ')"/>
            </fo:inline>
          </xsl:otherwise>
        </xsl:choose>
      </fo:block>
    </fo:list-item-label>
    <fo:list-item-body start-indent="body-start()">
      <fo:block>
        <xsl:apply-templates/>
      </fo:block>
    </fo:list-item-body>
  </xsl:template>
  <xsl:template match="table">
    <fo:table xsl:use-attribute-sets="table">
      <xsl:call-template name="process-table"/>
    </fo:table>
  </xsl:template>
  <xsl:template name="process-table">
    <xsl:call-template name="process-common-attributes"/>
    
    <xsl:variable name="parent_colspan">
         <xsl:value-of select="if (number(ancestor::cell/@colspan) gt 1) then number(ancestor::cell/@colspan) else 1"/>
    </xsl:variable>
    
    <xsl:variable name="colspans">
          <xsl:value-of select="sum(descendant::row[1]/cell/@colspan)"/>
    </xsl:variable>
    
    <xsl:variable name="maxwidth">
          <xsl:value-of select="($column_content_width div (count(descendant::row[1]/cell[empty(@colspan)]) + $colspans))"/>
    </xsl:variable>
    
    <xsl:variable name="parent_maxwidth">
          <xsl:value-of select="number((($maxwidth * $parent_colspan) div (count(descendant::row[1]/cell[empty(@colspan)]) + $colspans)) -3)"/>
    </xsl:variable>    
    
    <xsl:for-each select="colspec/col">
      <fo:table-column xsl:use-attribute-sets="table-column">
        <xsl:attribute name="column-width" select="concat(if (ancestor::cell) then $parent_maxwidth else $maxwidth ,'mm')"/>
      </fo:table-column>
    </xsl:for-each>
    <fo:table-body xsl:use-attribute-sets="tbody">
      <xsl:apply-templates select="row"/>
    </fo:table-body>
  </xsl:template>
  <xsl:template match="table-caption">
    <fo:table-caption xsl:use-attribute-sets="table-caption">
      <xsl:call-template name="process-common-attributes"/>
      <fo:block>
        <xsl:apply-templates/>
      </fo:block>
    </fo:table-caption>
  </xsl:template>
  <xsl:template name="process-table-rowgroup">
    <xsl:if test="ancestor::table[1]/@rules = 'groups'">
      <xsl:attribute name="border">1px solid</xsl:attribute>
    </xsl:if>
    <xsl:call-template name="process-common-attributes-and-children"/>
  </xsl:template>
  <xsl:template match="row">
    <fo:table-row xsl:use-attribute-sets="tr">
      <xsl:call-template name="process-table-row"/>
    </fo:table-row>
  </xsl:template>
  <xsl:template match="row[parent::table and not(cell)]">
    <!-- rg <fo:table-row xsl:use-attribute-sets="tr" keep-with-next="always"> rg -->
    <fo:table-row xsl:use-attribute-sets="tr">
      <xsl:call-template name="process-table-row"/>
    </fo:table-row>
  </xsl:template>
  <xsl:template name="process-table-row">
    <xsl:if test="ancestor::table[1]/@rules = 'rows'">
      <xsl:attribute name="border">1px solid</xsl:attribute>
    </xsl:if>
    <xsl:call-template name="process-common-attributes-and-children"/>
  </xsl:template>
  <xsl:template match="cell">
    <fo:table-cell xsl:use-attribute-sets="td">
      <xsl:call-template name="process-table-cell"/>
    </fo:table-cell>
  </xsl:template>
  <xsl:template name="process-table-cell">
    <xsl:if test="@colspan">
      <xsl:attribute name="number-columns-spanned">
        <xsl:value-of select="@colspan"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:if test="@rowspan">
      <xsl:attribute name="number-rows-spanned">
        <xsl:value-of select="@rowspan"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:call-template name="process-common-attributes"/>
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="footnote">
   <xsl:call-template name="indexed_footnote"/>
  </xsl:template>
  <xsl:template name="indexed_footnote">
   <fo:footnote>
    <fo:inline xsl:use-attribute-sets="footnote">
    <xsl:value-of select="count(preceding::footnote)+1"/></fo:inline>
      <fo:footnote-body>
       <fo:block xsl:use-attribute-sets="p">
            <fo:inline xsl:use-attribute-sets="footnote"><xsl:value-of select="count(preceding::footnote)+1"/></fo:inline>
              <xsl:value-of select="."/>
         </fo:block>
      </fo:footnote-body>
    </fo:footnote>
  </xsl:template>
  <xsl:template match="error_processing_article">
    <fo:block xsl:use-attribute-sets="error-processing-article">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:block>
  </xsl:template>
  <xsl:template match="bold">
    <fo:inline xsl:use-attribute-sets="b">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="bold-italic">
  	<xsl:param name="language" tunnel="yes"/>
    <xsl:message>   ###  CT HIT HERE -<xsl:value-of select="$language"/></xsl:message>
  	<xsl:choose>
  		<xsl:when test="$language = 'CT' or $language = 'CS'">
		    <fo:inline xsl:use-attribute-sets="strong-em-chinese">
		      <xsl:call-template name="process-common-attributes-and-children"/>
		    </fo:inline>
  		</xsl:when>
  		<xsl:otherwise>
		    <fo:inline xsl:use-attribute-sets="strong-em">
		      <xsl:call-template name="process-common-attributes-and-children"/>
		    </fo:inline>
		</xsl:otherwise>
	</xsl:choose>
  </xsl:template>
  <xsl:template match="strong">
    <fo:inline xsl:use-attribute-sets="strong">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="italic | intro | caption">
    <fo:inline xsl:use-attribute-sets="i">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="em">
    <fo:inline xsl:use-attribute-sets="em">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="big">
    <fo:inline xsl:use-attribute-sets="big">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="small">
    <fo:inline xsl:use-attribute-sets="small">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="sub">
    <fo:inline xsl:use-attribute-sets="sub">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="sup">
    <fo:inline xsl:use-attribute-sets="sup">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
  <xsl:template match="underline">
    <fo:inline xsl:use-attribute-sets="u">
      <xsl:call-template name="process-common-attributes-and-children"/>
    </fo:inline>
  </xsl:template>
   <xsl:template match="new-page">
    <fo:block page-break-after="always"/>
  </xsl:template>
  <xsl:template match="group">
    <fo:block>
      <xsl:if test="@align">
        <xsl:attribute name="text-align">
          <xsl:value-of select="@align"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>
  <xsl:template match="image-box | video-box">
    <xsl:if test="@align">
      <fo:block>
        <xsl:attribute name="text-align">
          <xsl:value-of select="@align"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </fo:block>
    </xsl:if>
  </xsl:template>
  <xsl:template match="image | poster | icon">
    <xsl:choose>
      <xsl:when test="ancestor::cell">
        <fo:external-graphic xsl:use-attribute-sets="img-inline">
          <xsl:variable name="colspans" select="sum(ancestor::row/cell/@colspan)"/>
          <xsl:variable name="maxwidth">
            <xsl:value-of select="($column_content_width div (count(ancestor::row/cell[empty(@colspan)]) + $colspans))"/>
          </xsl:variable>
          <xsl:attribute name="width" select="concat($maxwidth -3,'mm')"/>
          <xsl:call-template name="process-img"/>
        </fo:external-graphic>
      </xsl:when>
      <xsl:otherwise>
        <fo:external-graphic xsl:use-attribute-sets="img">
          <xsl:call-template name="process-img"/>
        </fo:external-graphic>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="process-img">
    <xsl:attribute name="src">
      <xsl:text>url('</xsl:text><xsl:value-of select="concat(@xlink:href, '/storage/preview/file')"/><xsl:text>')</xsl:text>
    </xsl:attribute>
    <xsl:if test="@alt">
      <xsl:attribute name="role">
        <xsl:value-of select="@alt"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:call-template name="process-common-attributes"/>
  </xsl:template>
  <xsl:template match="link">
    <fo:basic-link xsl:use-attribute-sets="a-link">
      <xsl:choose>
        <xsl:when test="@url!=''">
          <xsl:attribute name="external-destination" select="@url"/>
          <xsl:call-template name="process-a-link"/>
        </xsl:when>
        <xsl:when test="@xlink:href">
          <xsl:attribute name="internal-destination" select="@xlink:href"/>
          <xsl:call-template name="process-a-link"/>
        </xsl:when>
      </xsl:choose>
    </fo:basic-link>
  </xsl:template>
  <xsl:template match="anchor">
    <fo:basic-link xsl:use-attribute-sets="a-link">
      <xsl:attribute name="internal-destination" select="@key"/>
      <xsl:call-template name="process-a-link"/>
    </fo:basic-link>
  </xsl:template>
  <xsl:template name="process-a-link">
    <xsl:call-template name="process-common-attributes"/>
    <xsl:if test="@title">
      <xsl:attribute name="role">
        <xsl:value-of select="@title"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>
