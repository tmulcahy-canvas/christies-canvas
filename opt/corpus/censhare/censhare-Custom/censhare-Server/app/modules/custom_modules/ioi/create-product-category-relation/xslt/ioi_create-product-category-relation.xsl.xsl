<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:xs="http://www.w3.org/2001/XMLSchema">  
  <xsl:output method="xml" indent="yes" />
  <xsl:strip-space elements="*" />

  <xsl:param name="censhare:command-xml" />

  <xsl:variable name="parent-key-map">
    <mapping>
      <map keye="ioi:product-category-ken"  value="KEN"/>
      <map key="ioi:product-category-hgk"  value="HGK"/>
      <map key="ioi:product-category-mum"  value="MUM"/>
      <map key="ioi:product-category-cpsgnv"  value="CPSGNV"/>
      <map key="ioi:product-category-sha"  value="SHA"/>
      <map key="ioi:product-category-gnv"  value="GNV"/>
      <map key="ioi:product-category-cpskcs"  value="CPSKCS"/>
      <map key="ioi:product-category-stk"  value="STK"/>
      <map key="ioi:product-category-dub"  value="DUB"/>
      <map key="ioi:product-category-cps"  value="CPS"/>
      <map key="ioi:product-category-cks"  value="CKS"/>
      <map key="ioi:product-category-nyw"  value="NYW"/>
      <map key="ioi:product-category-nyc"  value="NYC"/>
      <map key="ioi:product-category-cpsny"  value="CPSNY"/>
      <map key="ioi:product-category-ams"  value="AMS"/>
      <map key="ioi:product-category-mil"  value="MIL"/>
      <map key="ioi:product-category-cpsnyc"  value="CPSNYC"/>
      <map key="ioi:product-category-zur"  value="ZUR"/>
      <map key="ioi:product-category-par"  value="PAR"/>
      <map key="ioi:product-category-eco"  value="ECO"/>
    </mapping>
  </xsl:variable>

  <xsl:template match="asset"> 
    <xsl:variable name="parent-indicator" select="asset_feature[@feature='canvas:jdeproperty.salesite']/@value_key"/>
    <xsl:if test="$parent-indicator != ''">
    	<xsl:variable name="parent-id" select="cs:asset()[@censhare:resource-key = $parent-key-map/mapping/map[@value = $parent-indicator]/@key]/@id"/>
    	<xsl:variable name="updated">
        <xsl:variable name="to-update">
            <asset>
              <xsl:copy-of select="./@*"/>
              <xsl:copy-of select="./node() except ./asset_feature[@feature='censhare:product.category']"/>
              <asset_feature feature="censhare:product.category" value_asset_id="{$parent-id}"/>
            </asset>
        </xsl:variable>
        <xsl:copy-of select="cs:update($to-update)"/>
      </xsl:variable>
    </xsl:if>
  </xsl:template>

  <xsl:function name="cs:update">
    <xsl:param name="asset-xml" />
    <cs:command name="com.censhare.api.assetmanagement.Update">
      <cs:param name="source" select="$asset-xml" />
    </cs:command>
  </xsl:function>
  

</xsl:stylesheet>
