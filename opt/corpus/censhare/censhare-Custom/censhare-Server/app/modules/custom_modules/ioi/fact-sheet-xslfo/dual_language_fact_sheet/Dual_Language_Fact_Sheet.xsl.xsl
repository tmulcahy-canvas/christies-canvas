<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
  xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
  xmlns:xe="http://www.censhare.com/xml/3.0.0/xmleditor"
  xmlns:my="http://www.censhare.com">
  <xsl:output method="xml" indent="yes" omit-xml-declaration="no" encoding="UTF-8"/>

  <xsl:param name="transform"/>
  
  <!-- variables -->
  <xsl:variable name="language" select="$transform/@language"/>
  <xsl:variable name="mode" select="if ($transform) then $transform/@mode else ''"/>
  <xsl:variable name="app" select="if ($transform) then $transform/@app else 'none'"/>
  <xsl:variable name="scale" select="if ($transform) then $transform/@scale else 1.0"/>
    <xsl:variable name="urlPrefix" select="if ($mode = 'browser') then '/ws/rest/service/' else if ($mode='cs5') then '/censhare5/client/rest/service/' else '/service/'"/>
  <xsl:variable name="rootAsset" select="asset[1]"/>

  <!-- root match -->
  <xsl:template match="/">

    <xsl:variable name="ext-condition" select="if(exists($rootAsset/asset_feature[@feature=&apos;canvas:external-condition&apos;]/@value_long)) then $rootAsset/asset_feature[@feature=&apos;canvas:external-condition&apos;]/@value_long else '0'"/>
    <xsl:variable name="img-condition" select="if(exists($rootAsset/asset_feature[@feature=&apos;canvas:image-status&apos;]/@value_long)) then $rootAsset/asset_feature[@feature=&apos;canvas:image-status&apos;]/@value_long else '0'"/>
    <xsl:variable name="languages" select="string-join($rootAsset/asset_feature[@feature='canvas:user-selected-properties']/xmldata/cmd/settings/language/@value, ',')"/>
    <xsl:variable name="excludes">
      <excludes>
        <xsl:for-each select="$rootAsset/asset_feature[@feature='canvas:user-selected-properties']/xmldata/cmd/settings/excludes/@*">
          <xsl:if test=". = '1' or . = 'true'">
            <exclude value="{local-name()}"/>
          </xsl:if>
        </xsl:for-each>
      </excludes>
    </xsl:variable>

    <cs:command name="com.censhare.api.io.CreateVirtualFileSystem" returning="out">
    </cs:command>
    
  	<xsl:variable name="dest" select="concat($out, &apos;temp-preview.pdf&apos;)"/>

    <cs:command name="com.censhare.api.transformation.FopTransformation">
      <cs:param name="stylesheet" select="&apos;censhare:///service/assets/asset;censhare:resource-key=ioi:dual-language-sale-fact-sheet-xslfo/storage/master/file&apos;"/>
      <cs:param name="source" select="$rootAsset"/>
      <cs:param name="dest" select="concat($out, &apos;temp-preview.pdf&apos;)"/>
      <cs:param name="xsl-parameters">
        <cs:param name="languages" select="$languages"/>
        <cs:param name="use-external-condition" select="$ext-condition"/>
        <cs:param name="use-image-status" select="$img-condition"/>
        <cs:param name="excludes" select="$excludes"/>
      </cs:param>
    </cs:command>

    <xsl:variable name="pdf-legal-path" select="concat($out, 'temp-preview.pdf')"/>

    <xsl:variable name="renderResult">
      <cs:command name="com.censhare.api.pdf.CombinePDF">
        <cs:param name="dest" select="$pdf-legal-path"/>
        <cs:param name="sources">
          <source href="{$dest}"/>
          <source href="censhare:///service/assets/asset;censhare:resource-key=christies:legal-page/storage/master/file"/>
        </cs:param>
      </cs:command>
    </xsl:variable>

    <!-- Return the result (file locator) -->
    <cs:output href="$pdf-legal-path" media-type="&apos;application/pdf&apos;"/>

  </xsl:template>

</xsl:stylesheet>
    