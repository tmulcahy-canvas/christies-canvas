<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet exclude-result-prefixes="#all" version="2.0" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:my="http://www.censhare.com/xml/3.0.0/my-functions" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="canvas_transformation.include.xsl" use-when="system-property('xsl:vendor')!='censhare'"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=canvas:transformation.include/storage/master/file" use-when="system-property('xsl:vendor')='censhare'"/>

	<xsl:output indent="no"/>

	<xsl:strip-space elements="*"/>
	
	<xsl:preserve-space elements="paragraph italic sup sub bold bold-italic"/>

	<!-- 
		Predefined for development only. Will be replaced during execution by parent transformation.
		@target-asset-id  : Asset ID of the layout the property will be placed on
	-->
	<xsl:param as="element(transform)" name="params">
		<transform target-asset-id="220370"/>
	</xsl:param>
	
	<xsl:param name="output-mode" select="'default'"/>

	<!-- ID of layout asset the property asset will be placed in -->
	<xsl:variable as="xs:long?" name="target_asset_id" select="$params/@target-asset-id"/>
	<xsl:variable as="element(asset)" name="property_asset" select="/asset"/>
	<xsl:variable as="element(asset)?" name="layout_asset" select="if ($target_asset_id) then cs:get-asset($target_asset_id) else ()"/>
	<xsl:variable as="element(asset)?" name="section_asset" select="if ($layout_asset) then ($layout_asset/cs:parent-rel()[@key='target.']/cs:asset()[@censhare:asset.type='issue.section.'])[1] else ()"/>
	<xsl:variable as="element(asset)?" name="sale_asset" select="$property_asset/cs:parent-rel()[@key='target.']/cs:asset()[@censhare:asset.type='sale.']"/>

	<xsl:variable name="propertyTextAssetMap">
		<entry asset_type="text.lotdetails." desc="Lot Details" element_name="details" jde_entity="DT1"/>
		<entry asset_type="text.provenance." desc="Provenance" element_name="provenance" jde_entity="PRO"/>
		<entry asset_type="text.literature." desc="Literature" element_name="literature" jde_entity="LIT"/>
		<entry asset_type="text.exhibited." desc="Exhibited" element_name="exhibited" jde_entity="EXH"/>
		<entry asset_type="text.engraved." desc="Engraved" element_name="engraved" jde_entity="ENG"/>
		<entry asset_type="text.prelot." desc="Prelot Text" element_name="prelot" jde_entity="PRE"/>
		<entry asset_type="text.postlot." desc="Postlot Text" element_name="postlot" jde_entity="PST"/>
		<!--<entry asset_type="text.cattext." desc="Catalogue Text" element_name="cattext" jde_entity="DSC"/>cataloguenotes-->
		<entry asset_type="text.cattext." desc="Catalogue Text" element_name="cataloguenotes" jde_entity="DSC"/>
		<entry asset_type="text.footnotes." desc="Footnotes" element_name="footnotes" jde_entity="CAT"/>
		<entry asset_type="text.pslongdescription." element_name="pslongdescription" desc="Private Sale Long Description" jde_entity="PSD"/>
		<entry asset_type="text.firstline." desc="First Line" element_name="firstline" jde_entity="1ST"/>
		<entry asset_type="text." desc="Lot Card Text Overwrite" jde_entity="LCO"/>
		<entry asset_type="text." desc="Subheading" jde_entity="SHD"/>
		<entry asset_type="text.title." desc="Title" element_name="title" jde_entity="TIT"/>
		<entry asset_type="text.weight." desc="Weight" element_name="weight" jde_entity="WGH"/>
		<entry asset_type="text.makeranddate." desc="Maker &amp; Date" element_name="makeranddate" jde_entity="M&amp;D"/>
		<entry asset_type="text.artistanddate." desc="Artist &amp; Date" element_name="artistanddate" jde_entity="A&amp;D"/>
		<entry asset_type="text.size." desc="Size" element_name="size" jde_entity="SZE"/>
		<entry asset_type="text.marked." desc="Marked" element_name="marked" jde_entity="MRK"/>
		<entry asset_type="text." desc="Index" jde_entity="IDX"/>
		<entry asset_type="text.picturemedium." desc="Picture Medium" element_name="picturemedium" jde_entity="PMD"/>
		<entry asset_type="text.picturesize." desc="Picture Size" element_name="picturesize" jde_entity="PSZ"/>
		<entry asset_type="text." desc="Quantity Description" jde_entity="QTY"/>
		<entry asset_type="text." desc="Certificate" jde_entity="CRT"/>
		<entry asset_type="text." desc="Heading" jde_entity="HDG"/>
		<entry asset_type="text.signature." desc="Signature" element_name="signature" jde_entity="SIG"/>
		<entry asset_type="text.certificate." desc="Certificate" element_name="certificate" jde_entity=""/>
		<entry asset_type="text.otherdetails." element_name="otherdetails" desc="Other Details" jde_entity="ODT"/>
		<entry asset_type="text.externalcondition." element_name="externalcondition" desc="External Condition" jde_entity="EXC"/>
		<entry asset_type="text.usernotes." element_name="usernotes" desc="User Notes" jde_entity="USN"/>
	</xsl:variable>

	<xsl:variable name="textHeadingMap">
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="AMS" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="CKS" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="DUB" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="GNV" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="HGK" target_language="en"/>
		<!-- Target language changed to en as JDE application can't handle it, fr, de languages and all users are using English languages fields in JDE to store and send data to Canvas (censhare) -->
		<entry engraved="ENGRAVED:" exhibited="ESPOSIZIONI:" literature="BIBLIOGRAFIA:" provenance="PROVENIENZA:" sale_site="MIL" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="NYC" target_language="en"/>
		<!-- Target language changed to en as JDE application can't handle it, fr, de languages and all users are using English languages fields in JDE to store and send data to Canvas (censhare) -->
		<entry engraved="ENGRAVED:" exhibited="EXPOSITION:" literature="BIBLIOGRAPHIE:" provenance="PROVENANCE:" sale_site="PAR" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="SHA" target_language="en"/>
		<!-- Target language changed to en as JDE application can't handle it, fr, de languages and all users are using English languages fields in JDE to store and send data to Canvas (censhare) -->
		<entry engraved="ENGRAVED:" exhibited="AUSSTELLUNG:" literature="LITERATUR:" provenance="PROVENIENZ:" sale_site="ZUR" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="ECO" target_language="en"/>
		<entry engraved="ENGRAVED:" exhibited="EXHIBITED:" literature="LITERATURE:" provenance="PROVENANCE:" sale_site="MUM" target_language="en"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="AMS" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="CKS" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="DUB" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="GNV" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="HGK" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="MIL" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="NYC" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="PAR" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="SHA" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="ZUR" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="ECO" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="MUM" target_language="CS"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="AMS" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="CKS" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="DUB" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="GNV" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="HGK" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="MIL" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="NYC" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="PAR" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="SHA" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="ZUR" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="ECO" target_language="CT"/>
		<entry engraved="展览" exhibited="展覽:" literature="出版:" provenance="來源:" sale_site="MUM" target_language="CT"/>		
	</xsl:variable>

	<xsl:variable name="metadataElementMap">
		<entry element_name="maker-and-date" feature_key="canvas:jde.maker-and-date" value_type="value_string"/>
		<entry element_name="artist-and-date" feature_key="canvas:jde.artist-and-date" value_type="value_string"/>
		<entry element_name="artistdate" feature_key="canvas:jdeproperty.artistdate" value_type="value_string"/>
		<entry element_name="quantity" feature_key="canvas:jde.quantity" value_type="value_long"/>
		<entry element_name="pad" feature_key="canvas:jde.pad" value_type="value_long"/>
		<entry element_name="objectformat" feature_key="canvas:jdeproperty.objectformat" value_type="value_key"/>
		<entry element_name="title" feature_key="canvas:jdeproperty.title" value_type="value_string"/>
		<entry element_name="firstline" feature_key="canvas:jdeproperty.firstline" value_type="value_string"/>
		<entry element_name="prelot" feature_key="canvas:jdeproperty.prelottext" value_type="value_string"/>
		<entry element_name="makerdate" feature_key="canvas:jdeproperty.makerdate" value_type="value_string"/>
		<entry element_name="details" feature_key="canvas:jdeproperty.details" value_type="value_string"/>
		<entry element_name="size" feature_key="canvas:jdeproperty.size" value_type="value_string"/>
		<entry element_name="index" feature_key="canvas:jdeproperty.index" value_type="value_string"/>
		<entry element_name="quantity" feature_key="canvas:jdeproperty.quantity" value_type="value_long"/>
		<entry element_name="quantitydesc" feature_key="canvas:jdeproperty.quantitydesc" value_type="value_string"/>
		<entry element_name="cataloguewinecasequantity" feature_key="canvas:jdeproperty.cataloguewinecasequantity" value_type="value_long"/>
		<entry element_name="cataloguewinequantity" feature_key="canvas:jdeproperty.cataloguewinequantity" value_type="value_long"/>

		<entry element_name="estimateonrequest" feature_key="canvas:jdeproperty.estimateonrequest" kind="estimate" value_type="value_long"/>
		<entry element_name="estimatecurrency" feature_key="canvas:jdeproperty.estimatecurrency" kind="estimate" value_type="value_string"/>
		<entry element_name="estimatelow" feature_key="canvas:jdeproperty.estimatelow" kind="estimate" value_type="value_long"/>
		<entry element_name="estimatehigh" feature_key="canvas:jdeproperty.estimatehigh" kind="estimate" value_type="value_long"/>

		<entry element_name="estimatecurrencysale" feature_key="canvas:jdeproperty.estimatecurrencysale" kind="estimate" value_type="value_string"/>
		<entry element_name="estimatelowsale" feature_key="canvas:jdeproperty.estimatelowsale" kind="estimate" value_type="value_long"/>
		<entry element_name="estimatehighsale" feature_key="canvas:jdeproperty.estimatehighsale" kind="estimate" value_type="value_long"/>

		<entry element_name="estimateusd" feature_key="canvas:jdeproperty.estimateusd" kind="estimate" value_type="value_long"/>
		<entry element_name="estimateeur" feature_key="canvas:jdeproperty.estimateeur" kind="estimate" value_type="value_long"/>
		<entry element_name="estimategbp" feature_key="canvas:jdeproperty.estimategbp" kind="estimate" value_type="value_long"/>
		<entry element_name="estimatehkd" feature_key="canvas:jdeproperty.estimatehkd" kind="estimate" value_type="value_long"/>

		<entry element_name="estimatecurrencyusd" feature_key="canvas:jdeproperty.estimatecurrencyusd" kind="estimate" value_type="value_string"/>
		<entry element_name="estimatehighusd" feature_key="canvas:jdeproperty.estimatehighusd" kind="estimate" value_type="value_long"/>
		<entry element_name="estimatelowusd" feature_key="canvas:jdeproperty.estimatelowusd" kind="estimate" value_type="value_long"/>

		<entry element_name="estimatecurrencyeur" feature_key="canvas:jdeproperty.estimatecurrencyeur" kind="estimate" value_type="value_string"/>
		<entry element_name="estimatehigheur" feature_key="canvas:jdeproperty.estimatehigheur" kind="estimate" value_type="value_long"/>
		<entry element_name="estimateloweur" feature_key="canvas:jdeproperty.estimateloweur" kind="estimate" value_type="value_long"/>

		<entry element_name="estimatecurrencygbp" feature_key="canvas:jdeproperty.estimatecurrencygbp" kind="estimate" value_type="value_string"/>
		<entry element_name="estimatehighgbp" feature_key="canvas:jdeproperty.estimatehighgbp" kind="estimate" value_type="value_long"/>
		<entry element_name="estimatelowgbp" feature_key="canvas:jdeproperty.estimatelowgbp" kind="estimate" value_type="value_long"/>

		<entry element_name="estimatecurrencyhkd" feature_key="canvas:jdeproperty.estimatecurrencyhkd" kind="estimate" value_type="value_string"/>
		<entry element_name="estimatehighhkd" feature_key="canvas:jdeproperty.estimatehighhkd" kind="estimate" value_type="value_long"/>
		<entry element_name="estimatelowhkd" feature_key="canvas:jdeproperty.estimatelowhkd" kind="estimate" value_type="value_long"/>

		<entry element_name="parcelnumber" feature_key="canvas:jdeproperty.parcelnumber" value_type="value_long"/>
		<entry element_name="parcelsequence" feature_key="canvas:jdeproperty.parcelsequence" value_type="value_long"/>
		<entry element_name="lotsuffix" feature_key="canvas:jdeproperty.lotsuffix" value_type="value_string"/>
		<entry element_name="weight" feature_key="canvas:jdeproperty.weight" value_type="value_string"/>
		<entry element_name="marked" feature_key="canvas:jdeproperty.marked" value_type="value_string"/>
		<entry element_name="otherdetails" feature_key="canvas:jdeproperty.otherdetails" value_type="value_string"/>
		<entry element_name="provenance" feature_key="canvas:jdeproperty.provenance" value_type="value_string"/>		
		<entry element_name="exhibited" feature_key="canvas:jdeproperty.exhibited" value_type="value_string"/>
		<entry element_name="postlottext" feature_key="canvas:jdeproperty.postlottext" value_type="value_string"/>
		<entry element_name="cataloguenotes" feature_key="canvas:jdeproperty.cataloguenotes" value_type="value_string"/>
		<entry element_name="literature" feature_key="canvas:jdeproperty.literature" value_type="value_string"/>
		<entry element_name="engraved" feature_key="canvas:jdeproperty.engraved" value_type="value_string" get_heading="true" text_type="text.engraved."/>
		<entry element_name="certificate" feature_key="canvas:jdeproperty.certificate" value_type="value_string"/>
		<entry element_name="picturemedium" feature_key="canvas:jdeproperty.picturemedium" value_type="value_string"/>
		<entry element_name="picturesize" feature_key="canvas:jdeproperty.picturesize" value_type="value_string"/>
		<entry element_name="signature" feature_key="canvas:jdeproperty.signature" value_type="value_string"/>
		<entry element_name="symbolconverted" feature_key="canvas:jdeproperty.symbolconverted" value_type="value_string"/>
	</xsl:variable>

	<xsl:variable name="numberOfEstimatesMapping">
		<entry number="1" value_key="one-estimate"/>
		<entry number="2" value_key="two-estimates"/>
		<entry number="3" value_key="three-estimates"/>
	</xsl:variable>

	<xsl:template match="asset">
		<property>
			<!-- language handling -->
			<language_handling>
				<!-- Add fallback to 'english-only' if no explicit second language handling is present -->
				<xsl:attribute name="mode" select="if (exists($section_asset/asset_feature[@feature = 'canvas:secondlanguagehandling']/@value_key)) then $section_asset/asset_feature[@feature = 'canvas:secondlanguagehandling']/@value_key else 'english-only'"/>
			</language_handling>
			<alternate_text_colour>
				<xsl:attribute name="mode" select="$property_asset/asset_feature[@feature = 'canvas:customtextcolor']/@value_key"/>
			</alternate_text_colour>

			<!-- get sale site from property's parent SALE asset -->
			<!-- currently in use: "Sale Site (Property)", has to be verified and maybe changed to "Sale Site (Sale)" -->
			<xsl:variable as="xs:string?" name="sale_site_code" select="$sale_asset/asset_feature[@feature = 'canvas:jdesale.salesite']/@value_key"/>
			<sale_site code="{$sale_site_code}" name="{if ($sale_site_code) then cs:localized-master-data('en', 'feature_value')[@feature = 'canvas:jdesale.salesite' and @value_key = $sale_site_code]/@name else ()}"/>

			<!-- lot data -->
			<xsl:comment>lot data</xsl:comment>
			<xsl:apply-templates mode="get-lot-data" select="."/>
			
			<xsl:if test="$output-mode = 'default'">
				<!-- estimates -->
				<xsl:comment>estimates</xsl:comment>
				<xsl:variable as="xs:string?" name="number_of_estimates" select="$numberOfEstimatesMapping/entry[@value_key = $section_asset/asset_feature[@feature = 'canvas:numberofestimates']/@value_key]/@number"/>
				<xsl:variable as="xs:string?" name="estimate_lines" select="$section_asset/asset_feature[@feature = 'canvas:estimatelines']/@value_key"/>
				<estimates count="{if ($number_of_estimates) then $number_of_estimates else 0}" lines="{if ($estimate_lines) then $estimate_lines else 0}">
					<xsl:apply-templates mode="get-estimates" select="$property_asset"/>
				</estimates>
				
				<!-- Gather information from metadata of property asset -->
				<xsl:comment>meta of property asset</xsl:comment>
				<xsl:apply-templates mode="get-metadata" select="$property_asset">
					<xsl:with-param name="sale_site_code" select="$sale_site_code" tunnel="yes"/>
				</xsl:apply-templates>
				
				<!-- Gather information from related text assets of property asset -->
				<xsl:apply-templates mode="get-text-assets" select="$property_asset">
					<xsl:with-param name="sale_site_code" select="$sale_site_code" tunnel="yes"/>
				</xsl:apply-templates>	
			</xsl:if>			
		</property>

	</xsl:template>

	<xsl:template match="asset" mode="get-metadata">
		<xsl:apply-templates mode="#current" select="asset_feature[starts-with(@feature, 'canvas:')]"/>
	</xsl:template>

	<xsl:template match="asset_feature" mode="get-estimates">
		<xsl:variable as="element(entry)?" name="mapEntry" select="$metadataElementMap/entry[@feature_key = current()/@feature and @kind = 'estimate']"/>

		<xsl:if test="$mapEntry">
			<xsl:element name="{$mapEntry/@element_name}">
				<xsl:value-of select="@*[local-name() = $mapEntry/@value_type]"/>
				<xsl:apply-templates mode="#current" select="asset_feature"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template match="asset" mode="get-lot-data">
		<xsl:variable as="element(asset)" name="property_asset" select="."/>
		<xsl:variable name="symbolconverted" select="$property_asset/asset_feature[@feature = 'canvas:jdeproperty.symbolconverted']/@value_string"/>
		<lot>
			<xsl:attribute name="number" select="$property_asset/parent_asset_rel[@key = 'target.' and @parent_asset = $sale_asset/@id]/asset_rel_feature[@feature = 'canvas:jdeproperty.lotnumber']/@value_long"/>
			<xsl:attribute name="symbolcode" select="$property_asset/asset_feature[@feature = 'canvas:jdeproperty.symbolcode']/@value_string"/>
			<!-- commented by APA (24.10.18) Ticket id: 4050966 <xsl:attribute name="symbolconverted" select="$property_asset/asset_feature[@feature = 'canvas:jdeproperty.symbolconverted']/@value_string"/>-->
			<xsl:attribute name="symbolconverted" select="for $x in $symbolconverted return replace(replace($x, '&#x3c3;', '&lt;S&gt;σ&lt;/S&gt;'), '&#x2207;', '&lt;S&gt;∇&lt;/S&gt;')"/>
			<xsl:attribute name="symboldescription" select="$property_asset/asset_feature[@feature = 'canvas:jdeproperty.symboldescription']/@value_string"/>
			<xsl:attribute name="suffix" select="$property_asset/parent_asset_rel[@key = 'target.' and @parent_asset = $sale_asset/@id]/asset_rel_feature[@feature = 'canvas:jdeproperty.lotsuffix']/@value_string"/>
		</lot>
	</xsl:template>

	<xsl:template match="asset_feature[not(contains(@feature, 'lotsymbol.'))]" mode="get-metadata">
		<xsl:param name="sale_site_code" tunnel="yes"/>
		
		<xsl:variable name="mapEntry" select="$metadataElementMap/entry[@feature_key = current()/@feature]"/>
		<xsl:choose>
			<xsl:when test="$mapEntry">
				<metadata>
					<xsl:attribute name="type" select="$mapEntry/@element_name"/>
					<xsl:attribute name="value" select="@*[local-name() = $mapEntry/@value_type]"/>
					<xsl:copy-of select="@language"/>
					<xsl:if test="xs:boolean($mapEntry/@get_heading) and exists($mapEntry/@text_type)">
						<xsl:attribute name="heading" select="my:getTextHeading($sale_site_code, @language, $mapEntry/@text_type)"/>
					</xsl:if>
				</metadata>
			</xsl:when>
		</xsl:choose>

	</xsl:template>

	<!-- Resolve master text assets in all available languages (EN, CS, CT) -->
	<xsl:template match="asset" mode="get-text-assets">
		<xsl:variable as="element(asset)" name="property_asset" select="."/>
		<xsl:variable as="element(asset)*" name="master_text_assets" select="for $child-asset in $property_asset/cs:child-rel()[@key = 'user.']/cs:asset()[@censhare:asset.type = 'text.*']return if ($child-asset/parent_asset_rel[@key = 'variant.1.']) then () else $child-asset"/>
		<xsl:variable as="element(asset)*" name="language_variant_assets" select="($master_text_assets/@id)/cs:child-rel()[@key = 'variant.1.']/cs:asset()[@censhare:asset.language = 'CS' or @censhare:asset.language = 'CT']"/>

		<xsl:variable name="resolved_text_assets">
			<xsl:apply-templates mode="resolve-user-generated-variants" select="$master_text_assets"/>
			<xsl:apply-templates mode="resolve-user-generated-variants" select="$language_variant_assets"/>
		</xsl:variable>

		<xsl:apply-templates mode="get-content" select="$resolved_text_assets"/>
	</xsl:template>

	<!-- Resolve user generated variants -->
	<xsl:template match="asset" mode="resolve-user-generated-variants">
		<xsl:variable as="element(asset)" name="text_asset" select="."/>
		<xsl:variable as="xs:string" name="asset_language" select="$text_asset/@language"/>

		<xsl:variable as="element(asset)?" name="variant_asset" select="$text_asset/cs:child-rel()[@key = 'variant.1.']/cs:asset()[@censhare:asset.language = $asset_language]"/>
		<xsl:choose>
			<xsl:when test="$variant_asset">
				<xsl:copy-of select="$variant_asset"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="$text_asset"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!-- Read storage items and prepend headings for selected text asset types -->
	<xsl:template match="asset" mode="get-content">
		<xsl:param as="xs:string?" name="sale_site_code" tunnel="yes"/>

		<xsl:variable as="element(asset)" name="text_asset" select="."/>
		<xsl:variable name="content" select="doc(concat('censhare:///service/assets/asset/id/', @id, '/storage/master/file'))/article/content/text"/>
		<xsl:variable as="xs:string*" name="text_element_name" select="$propertyTextAssetMap/entry[@asset_type=$text_asset/@type]/@element_name"/>
	<xsl:if test="string-length($content) gt 0 and exactly-one($text_element_name)">
			<text>
				<!--<xsl:variable as="xs:string*" name="text_element_name" select="$propertyTextAssetMap/entry[@asset_type=$text_asset/@type]/@element_name"/>-->
				<xsl:attribute name="type" select="if (exactly-one($text_element_name)) then $text_element_name else 'undefined'"/>
				<xsl:if test="count($text_element_name) != 1">
					<xsl:attribute name="text-asset-type" select="$text_asset/@type"/>
				</xsl:if>
				<xsl:variable name="text_asset_language" select="$text_asset/@language"/>
				<xsl:attribute name="language" select="$text_asset_language"/>
				<xsl:if test="$sale_site_code and $text_asset/@type = ('text.provenance.', 'text.literature.', 'text.exhibited.', 'text.engraved.')">
					<xsl:variable as="xs:string?" name="heading" select="my:getTextHeading($sale_site_code, $text_asset_language, $text_asset/@type)"/>
					<xsl:if test="$heading">
						<subheadline-1>
							<xsl:value-of select="$heading"/>
						</subheadline-1>
					</xsl:if>
				</xsl:if>
				<xsl:apply-templates mode="remove-line-breaks" select="$content/node()"/>
			</text>
		</xsl:if>
	</xsl:template>
	
	<xsl:function name="my:getTextHeading">
		<xsl:param name="sale_site_code" as="xs:string"/>
		<xsl:param name="target_language" as="xs:string"/>
		<xsl:param name="text_type" as="xs:string"/>
		
		<xsl:variable as="element(entry)?" name="map_entry" select="$textHeadingMap/entry[@sale_site = $sale_site_code and @target_language = $target_language]"/>
		<xsl:choose>
			<xsl:when test="$text_type = 'text.provenance.'">
				<xsl:value-of select="$map_entry/@provenance"/>
			</xsl:when>
			<xsl:when test="$text_type = 'text.literature.'">
				<xsl:value-of select="$map_entry/@literature"/>
			</xsl:when>
			<xsl:when test="$text_type = 'text.exhibited.'">
				<xsl:value-of select="$map_entry/@exhibited"/>
			</xsl:when>
			<xsl:when test="$text_type = 'text.engraved.'">
				<xsl:value-of select="$map_entry/@engraved"/>
			</xsl:when>
		</xsl:choose>		
	</xsl:function>

	<!-- Remove manual line breaks and tabs in text content -->
	<xsl:template match="text()" mode="remove-line-breaks" priority="2">
		<xsl:variable name="temp" select="normalize-space(concat('x', replace(., '\n', ''), 'x'))"/>
		<xsl:value-of select="substring($temp, 2, string-length($temp) - 2)"/>
	</xsl:template>

	<xsl:template match="@*|node()" mode="remove-line-breaks">
		<xsl:copy>
			<xsl:apply-templates mode="#current" select="@*|node()"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>