<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
    xmlns:func="http://www.censhare.com/xml/3.0.0/my-functions" 
    xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
    xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
    exclude-result-prefixes="#all" version="2.0">
    
    <xsl:strip-space elements="*"/>
    
    <!-- Declaration of censhare XPath functions to allow external validation -->
	<xsl:function name="cs:escape-html" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="arg" as="xs:string"/>
		<xsl:value-of select="''"/>
	</xsl:function>
	<xsl:function name="cs:asset" as="element(asset)*" override="no" use-when="system-property('xsl:vendor')!='censhare'"/>
	<xsl:function name="cs:asset" as="element(asset)*" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="query" as="item()"/>
	</xsl:function>
	<xsl:function name="cs:resource-asset" as="element(asset)*" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="query" as="item()"/>
	</xsl:function>
	<xsl:function name="cs:get-asset" as="element(asset)?" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="id" as="xs:integer"/>
	</xsl:function>
	<xsl:function name="cs:get-asset" as="element(asset)?" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="id" as="xs:integer"/>
		<xsl:param name="version" as="xs:integer"/>
	</xsl:function>
	<xsl:function name="cs:get-resource-asset" as="element(asset)?" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="resource-key" as="xs:string"/>
	</xsl:function>
	<xsl:function name="cs:parent-rel" as="element(asset)*" override="no" use-when="system-property('xsl:vendor')!='censhare'"/>
	<xsl:function name="cs:child-rel" as="element(asset)*" override="no" use-when="system-property('xsl:vendor')!='censhare'"/>
	<xsl:function name="cs:feature-ref" as="element(asset)*" override="no" use-when="system-property('xsl:vendor')!='censhare'"/>
	<xsl:function name="cs:feature-ref-reverse" as="element(asset)*" override="no" use-when="system-property('xsl:vendor')!='censhare'"/>
	<xsl:function name="cs:master-data" as="element()?" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="table" as="xs:string"/>
	</xsl:function>
	<xsl:function name="cs:format-number" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="number"/>
		<xsl:param name="format" as="xs:string?"/>
		<xsl:value-of select="''"/>
	</xsl:function>
	<xsl:function name="cs:localized-format-number" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="language" as="xs:string"/>
		<xsl:param name="number"/>
		<xsl:param name="format" as="xs:string?"/>
		<xsl:value-of select="''"/>
	</xsl:function>
	<xsl:function name="cs:format-date" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="dateTime" as="xs:dateTime"/>
		<xsl:param name="format" as="xs:string"/>
		<xsl:value-of select="''"/>
	</xsl:function>
	<xsl:function name="cs:localized-format-date" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="language" as="xs:string"/>
		<xsl:param name="dateTime" as="xs:dateTime"/>
		<xsl:param name="format" as="xs:string"/>
		<xsl:value-of select="''"/>
	</xsl:function>
	<xsl:function name="cs:format-date" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="dateTime" as="xs:dateTime"/>
		<xsl:param name="dateFormat" as="xs:string"/>
		<xsl:param name="timeFormat" as="xs:string"/>
		<xsl:value-of select="''"/>
	</xsl:function>
	<xsl:function name="cs:evaluate" as="item()*" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="expression" as="xs:string"/>
	</xsl:function>
	<xsl:function name="cs:encode-base64" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="value" as="xs:string"/>
		<xsl:value-of select="''"/>
	</xsl:function>
	<xsl:function name="cs:decode-base64" as="xs:string" override="no" use-when="system-property('xsl:vendor')!='censhare'">
		<xsl:param name="value" as="xs:string"/>
		<xsl:value-of select="''"/>
	</xsl:function>
</xsl:stylesheet>