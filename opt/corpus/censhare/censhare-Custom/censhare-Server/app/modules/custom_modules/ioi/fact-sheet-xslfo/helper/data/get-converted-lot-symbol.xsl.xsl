<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet exclude-result-prefixes="xs html censhare cs io ioi" version="2.0" xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare" xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus" xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" xmlns:html="http://www.w3.org/TR/REC-html40" xmlns:io="http://www.censhare.com/xml/3.0.0/censhare-io" xmlns:ioi="http://www.iointegration.com" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output encoding="utf-8" indent="yes" method="xml" version="1.0"/>
    <xsl:param name="censhare:command"/>
    <xsl:template match="asset">
        <xsl:variable name="symbols" select="string-join(for $x in ./asset_feature[@feature='canvas:jdeproperty.symbolconverted']/@value_string return $x,',')" />
        <xsl:variable name="return">
            <xsl:for-each select="tokenize($symbols, ',')">
                <cs:command name="com.censhare.api.christiesPrepareArticle.prepareConvertedSymbols">
                    <cs:param name="textString" select="."/>
                </cs:command>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="convertedSymbols" select="string-join(for $x in $return/descendant::text return $x, '')" />
        <content symbol-lotnumber="{$convertedSymbols}"/>
    </xsl:template>
</xsl:stylesheet>