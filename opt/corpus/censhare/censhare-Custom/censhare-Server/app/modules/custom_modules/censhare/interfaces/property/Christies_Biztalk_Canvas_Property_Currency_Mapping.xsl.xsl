<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
 xmlns:canvas="censhare.com"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
 xmlns:xi="http://www.w3.org/2001/XInclude"
 xmlns:xs="http://www.w3.org/2001/XMLSchema"
 xmlns:my="http://www.censhare.com/my"
 xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions">

	<xsl:function name="canvas:propertycurrencymapping">
		<xsl:param name="elementTag" as="xs:string"/>
		<xsl:param name="mappingElementstoFeatures" as="xs:string"/>
		<xsl:variable name="elementName" select="$elementTag//normalize-space(name(.))"/>
		<xsl:variable name="elementValue" select="$elementTag"/>
		
					<xsl:element name="asset_feature">
						<xsl:attribute name="feature" select="normalize-space($mappingElementstoFeatures//*[local-name()=$elementName]/parentFeature)"/>
						<xsl:element name="asset_feature">
							<xsl:attribute name="feature" select="normalize-space($mappingElementstoFeatures//*[local-name()=$elementName]/id)"/>
							<xsl:attribute name="{$mappingElementstoFeatures//*[local-name()=$elementName]/valueType}" select="$elementValue"/>
						</xsl:element>
					</xsl:element>
	</xsl:function>

</xsl:stylesheet>
