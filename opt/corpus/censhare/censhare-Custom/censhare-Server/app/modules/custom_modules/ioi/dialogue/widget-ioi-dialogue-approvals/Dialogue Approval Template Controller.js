function controller($scope,$filter) {
 
   $scope.formatTimeStamp = function(value) {
      return $filter('date')(value, 'MMM d, y h:m a');
   }
}