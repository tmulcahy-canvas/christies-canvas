<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
    xmlns:canvas="censhare.com"
    xmlns:ns0="http://integration.christies.com/Interfaces/CANVAS/ObjectDataRequest"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions" 
		xmlns:censhare="http://www.censhare.com/xml/3.0.0/censhare"
		xmlns:csc="http://www.censhare.com/censhare-custom"
		xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
		exclude-result-prefixes="#all">

	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:propertysalemappings/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvasmetadatavalidations/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvasrelateimagestoproperty/storage/master/file"/>
	<xsl:import href="censhare:///service/assets/asset;censhare:resource-key=christies:biztalkcanvaspropertytaxonomymappings/storage/master/file"/>

	<xsl:output indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>

	<!-- ################## -->
	<xsl:function name="canvas:checkAndUpdateAssets">
		<xsl:param name="propertyAsset" />
		<xsl:param name="mappingElementstoFeatures" />
		<xsl:param name="currentItemXML" />
		<xsl:param name="propertyLanguage"/>

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************INSIDE checkAndUpdateAssets'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

		<xsl:variable name="propertyIDExt" select="$propertyAsset/@id_extern" />

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************existingPropertyVariants START'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->

		<xsl:variable name="existingPropertyVariants" select="cs:asset()[@censhare:asset.type='product.' and @canvas:jdeproperty.shortitemnumber=$currentItemXML/ShortItemNumber]" />
		<xsl:variable name="variantParentAsset">
			<xsl:if test="$existingPropertyVariants">
				<xsl:for-each select="$existingPropertyVariants">
					<xsl:if test="not(./parent_asset_rel[@key='variant.1.']) and ./asset_feature[@feature='canvas:jdeproperty.itemversion' and @value_long!=$currentItemXML/ItemVersion]">
					<variants>
						<parent_asset_rel parent_asset="{./@id}" child_asset="{$propertyAsset/@id}" key="variant.1."/>
						<xsl:copy-of select="." />
					</variants>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:variable>

				<!--xsl:message>
					<xsl:value-of select="'&#xA;&#xA;'" />
					<xsl:value-of select="'**************************existingPropertyVariants END'" />
					<xsl:value-of select="'&#xA;&#xA;'" />
				</xsl:message-->


		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA; ############## variantParentAsset &#xA;'"/>
			<xsl:copy-of select="$variantParentAsset/variants/parent_asset_rel" />
			<xsl:copy-of select="$variantParentAsset/variants/asset"/>
			<xsl:value-of select="'&#xA;&#xA;&#xA;'"/>
		</xsl:message-->

		<!-- Sale Asset Checking and Creation and Relating -->
		<xsl:if test="$currentItemXML/SaleNumber">
			<xsl:copy-of select="canvas:salepropertyrelationscheckupdateremove($propertyAsset,$mappingElementstoFeatures,$currentItemXML)" />
		</xsl:if>

		<!-- Primary Image Asset Checking and Creation and Relating -->
		<xsl:if test="$currentItemXML/PrimaryID">

			<xsl:choose>
			<xsl:when test="(not($propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string) and ($currentItemXML/PrimaryID))">

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA; ############## inside when 1 &#xA;'"/>
			<xsl:value-of select="$currentItemXML/PrimaryID" />
			<xsl:value-of select="'&#xA;&#xA;'"/>
			<xsl:value-of select="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string" />
			<xsl:value-of select="'&#xA;&#xA;&#xA;'"/>
		</xsl:message-->

				<xsl:copy-of select="canvas:propertyPrimaryImageMapping($currentItemXML/PrimaryID,$currentItemXML/SaleNumber)" />
				<xsl:copy-of select="canvas:imageCheckAndMap($propertyAsset,$mappingElementstoFeatures,$currentItemXML,($propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string))" />
			</xsl:when>
			<xsl:when test="($propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string != $currentItemXML/PrimaryID)">

		<!--xsl:message>
			<xsl:value-of select="'&#xA;&#xA;&#xA; ############## inside when 2 &#xA;'"/>
			<xsl:value-of select="$currentItemXML/PrimaryID" />
			<xsl:value-of select="'&#xA;&#xA;'"/>
			<xsl:value-of select="$propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string" />
			<xsl:value-of select="'&#xA;&#xA;&#xA;'"/>
		</xsl:message-->

				<xsl:copy-of select="canvas:propertyPrimaryImageMapping($currentItemXML/PrimaryID,$currentItemXML/SaleNumber)" />
				<xsl:copy-of select="canvas:imageCheckAndMap($propertyAsset,$mappingElementstoFeatures,$currentItemXML,($propertyAsset/asset_feature[@feature='canvas:jdeproperty.primaryid']/@value_string))" />
			</xsl:when>
			<xsl:otherwise>
				<!--xsl:copy-of select="$propertyAsset/child_asset_rel[@key='user.main-picture.']" /-->
				<!--child_asset_rel key="'user.main-picture.'" child_asset="{(cs:asset()[@censhare:asset.name=$currentItemXML/PrimaryID and @censhare:asset.currversion=0])/@id}" /-->
			</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<!-- Additional Image Asset Checking and Creation and Relating -->
		<!--xsl:if test="$currentItemXML/ImageID">
			<xsl:variable name="elementName" select="name($currentItemXML/ImageID)"/>
			<xsl:variable name="uniqueImageIDS">
				<xsl:value-of select="distinct-values(tokenize($currentItemXML/ImageID, '\s'))"/>
			</xsl:variable>
			<xsl:copy-of select="canvas:propertyAdditionalImageMapping($mappingElementstoFeatures,$elementName,$uniqueImageIDS,$currentItemXML/SaleNumber,$currentItemXML/PrimaryID)" />
		</xsl:if-->

		<!-- Additional Image Asset Checking and Creation and Relating -->
		<xsl:variable name="primaryImageIDs" select="concat($currentItemXML/PrimaryID,' ',$currentItemXML/ImageID)" />
		<xsl:if test="($primaryImageIDs) and normalize-space($primaryImageIDs)">
			<xsl:variable name="uniqueImageIDS">
				<xsl:value-of select="distinct-values(tokenize(normalize-space($primaryImageIDs), '\s'))"/>
			</xsl:variable>
			<xsl:variable name="elementName" select="normalize-space(name($currentItemXML/ImageID))"/>
			<xsl:copy-of select="canvas:propertyAdditionalImageMapping($mappingElementstoFeatures,$elementName,normalize-space($uniqueImageIDS),normalize-space($currentItemXML/SaleNumber),normalize-space($currentItemXML/PrimaryID))" />
		</xsl:if>


		<!-- Taxonomy Asset Checking and Creation and Relating -->
		<xsl:if test="$currentItemXML/CoAAttributeID">
			<xsl:variable name="elementName" select="name($currentItemXML/CoAAttributeID)"/>
			<xsl:for-each select="$currentItemXML/CoAAttributeID/ID">
				<xsl:copy-of select="canvas:propertytaxonomymapping(normalize-space(.),$mappingElementstoFeatures,$elementName)" />
			</xsl:for-each>
		</xsl:if>

		<!-- Store Transmission Date for used in multiple places - VEC 01-24-2019 -->
		<xsl:variable name="DateTransmission" select="concat(substring(normalize-space($currentItemXML/TransmissionDate), 5, 4), '-', substring(normalize-space($currentItemXML/TransmissionDate), 1, 2), '-', substring(normalize-space($currentItemXML/TransmissionDate), 3, 2))"/>

		<!-- Creating metadata mappings and updating values accordingly -->
		<!--xsl:for-each select="$currentItemXML/* except (szUniqueKeyIDString_UKIDSZ,LotNumber,LotSuffix,CoAAttributeID)"--> <!-- UPDATED BY VENKAT FOR HANDLING LOT NUMBER and LOT SUFFIC -->
		<xsl:for-each select="$currentItemXML/* except (szUniqueKeyIDString_UKIDSZ,CoAAttributeID)">
			<xsl:variable name="elementName" select="normalize-space(name(.))"/>
			<xsl:variable name="confMappingFeature" select="$mappingElementstoFeatures//*[local-name()=$elementName]" />
				<xsl:choose>
					<xsl:when test="$confMappingFeature/textFileConfs/textFileRequired='Yes'">

						<xsl:variable name="textProcessingResults" select="canvas:textFileContentCheck(canvas:tagsCanvasMapping(.), local-name(.), $confMappingFeature/textFileConfs/characterLimit, $confMappingFeature/textFileConfs/fileType/text(), $confMappingFeature/textFileConfs/fileName/text(), $propertyIDExt, $currentItemXML/ShortItemNumber,$propertyLanguage,$currentItemXML/ItemVersion,$mappingElementstoFeatures,$propertyAsset,'')" />

							<!--xsl:message>
								<xsl:value-of select="'&#xA;&#xA;&#xA;'"/>
								<xsl:value-of select="local-name(.)" />
								<xsl:value-of select="'&#xA;VALUE OF&#xA;'"/>
								<xsl:value-of select="$textProcessingResults"/>
								<xsl:value-of select="'&#xA;COPY OF&#xA;'"/>
								<xsl:copy-of select="$textProcessingResults"/>
								<xsl:value-of select="'&#xA;&#xA;&#xA;'"/>
							</xsl:message-->


						<xsl:choose>
							<xsl:when test="$textProcessingResults/warningElements">
								<!--xsl:message>
								<xsl:value-of select="'&#xA;&#xA;&#xA;'"/-->
								<xsl:copy-of select="$textProcessingResults/warningElements"></xsl:copy-of>
								<!--xsl:value-of select="'&#xA;&#xA;&#xA;'"/>
								</xsl:message-->
							</xsl:when>
							<xsl:otherwise>
						<!--xsl:if test="$textProcessingResults != ''"-->
							<asset_feature feature="{$confMappingFeature/id}" language="{normalize-space($propertyLanguage)}">
								<xsl:attribute name="{$confMappingFeature/valueType}">
									<xsl:copy-of select="$textProcessingResults//textContent/text()"></xsl:copy-of>
								</xsl:attribute>
							</asset_feature>
							<excludeElements feature="{$confMappingFeature/id}" language="{$propertyLanguage}" />
	
							<xsl:variable name="textAssetID" select="$textProcessingResults//asset/@id"/>
							<xsl:if test="$textAssetID">
								<excludeElements childAsset="{$textAssetID}" key="user." />
								<child_asset_rel key="{$confMappingFeature/textFileConfs/relationType/text()}" child_asset="{$textAssetID}" />
							</xsl:if>
						<!--/xsl:if-->
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="exists($confMappingFeature/parentFeature)">
						<xsl:if test="$propertyLanguage='en'">
							<asset_feature feature="{$confMappingFeature/parentFeature}">
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="normalize-space(canvas:numberFormatChange(.))"/>
								</asset_feature>
							</asset_feature>
							<excludeElements parentFeature="{$confMappingFeature/parentFeature}" feature="{$confMappingFeature/id}" />
						</xsl:if>
					</xsl:when>
					<xsl:when test="($confMappingFeature/valueType = 'value_timestamp') or ($confMappingFeature/valueType = 'value_timestamp2')">
						<xsl:if test="$propertyLanguage='en'">
						<xsl:choose>
							<xsl:when test="($elementName='TransmissionDate')">
								<xsl:variable name="TransmissionDateValue" select="concat($DateTransmission,'T00:00:00Z')"/>
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="$TransmissionDateValue"/>
								</asset_feature>
								<excludeElements feature="{$confMappingFeature/id}" />
							</xsl:when>
						<xsl:when test="($elementName='TransmissionTime')">
							<!-- VEC 01-24-2019-->
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:setDateforTimeElements(.,$DateTransmission)"/>
								</asset_feature>
								<excludeElements feature="{$confMappingFeature/id}" />
						</xsl:when>
						<xsl:when test="($elementName='TimeLastUpdated')">
							<!-- VEC 01-24-2019-->
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:setDateforTimeElements(.,$DateTransmission)"/>
								</asset_feature>
								<excludeElements feature="{$confMappingFeature/id}" />
						</xsl:when>
							<xsl:otherwise>
								<asset_feature feature="{$confMappingFeature/id}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:dateFormatChange(.)"/>
								</asset_feature>
								<excludeElements feature="{$confMappingFeature/id}" />
							</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</xsl:when>
					<xsl:when test="$confMappingFeature/valueType = 'value_long'">
						<xsl:if test="$propertyLanguage='en'">
							<xsl:choose>
								<xsl:when test="$confMappingFeature/languagePreference = 'Yes'">
									<asset_feature feature="{$confMappingFeature/id}" language="{$propertyLanguage}">
										<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:numberFormatChange(.)"/>
									</asset_feature>
									<excludeElements feature="{$confMappingFeature/id}" language="{$propertyLanguage}"/>
								</xsl:when>
								<xsl:otherwise>
									<asset_feature feature="{$confMappingFeature/id}">
										<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:numberFormatChange(.)"/>
									</asset_feature>
									<excludeElements feature="{$confMappingFeature/id}" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>

						<xsl:variable name="actualString" select="canvas:tagsCanvasMapping(normalize-space(.))"/>
						<xsl:variable name="truncatedString" select="substring($actualString, 1, 4000)"/>
						
						<xsl:if test="(string-length($actualString) > 4000)">
							<warningElements id="W003"><xsl:value-of select="$elementName"/></warningElements>
						</xsl:if>

						<xsl:choose>
							<xsl:when test="$confMappingFeature/languagePreference = 'Yes'">
								<asset_feature feature="{$confMappingFeature/id}" language="{$propertyLanguage}">
									<xsl:attribute name="{$confMappingFeature/valueType}" select="$truncatedString"/>
								</asset_feature>
								<excludeElements feature="{$confMappingFeature/id}" language="{$propertyLanguage}"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="$propertyLanguage='en'">
									<asset_feature feature="{$confMappingFeature/id}">
										<xsl:attribute name="{$confMappingFeature/valueType}" select="$truncatedString"/>
									</asset_feature>
									<excludeElements feature="{$confMappingFeature/id}"/>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>

						<!--xsl:choose>
						<xsl:when test="$confMappingFeature/languagePreference = 'Yes'">
							<asset_feature feature="{$confMappingFeature/id}" language="{$propertyLanguage}">
								<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:tagsCanvasMapping(normalize-space(.))"/>
							</asset_feature>
							<excludeElements feature="{$confMappingFeature/id}" language="{$propertyLanguage}"/>
						</xsl:when>
						<xsl:otherwise>
							<asset_feature feature="{$confMappingFeature/id}">
								<xsl:attribute name="{$confMappingFeature/valueType}" select="canvas:tagsCanvasMapping(normalize-space(.))"/>
							</asset_feature>
							<excludeElements feature="{$confMappingFeature/id}"/>
						</xsl:otherwise>
						</xsl:choose-->
					</xsl:otherwise>
				</xsl:choose>
		</xsl:for-each>


		<xsl:if test="$variantParentAsset">
			<xsl:copy-of select="$variantParentAsset/variants/parent_asset_rel" />
			<excludeElements>
			<parent_asset_rel parent_asset="{$variantParentAsset/variants/parent_asset_rel/@parent_asset}" child_asset="{$variantParentAsset/variants/parent_asset_rel/@child_asset}" key="{$variantParentAsset/variants/parent_asset_rel/@key}" />
			</excludeElements>
		</xsl:if>

	</xsl:function>

</xsl:stylesheet>

