<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xi="http://www.w3.org/2001/XInclude"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:my="http://www.censhare.com/my"
        xmlns:censhare="java:modules.conversion.XSLExtensionFunctions">
  <xsl:output indent="no" method="xml" omit-xml-declaration="no" encoding="UTF-8"/>
  <xsl:param name="system"/>
  <xsl:param name="transform"/>
  <xsl:variable name="mode" select="if ($transform and $transform/@mode='html-widget') then 'html-widget' else ''"/>
  <xsl:variable name="urlPrefix" select="'censhare:///service/'"/>
  <xsl:variable name="lafBackground" select="if ($system) then $system/system/laf/@background else 'dark'"/>
  <xsl:variable name="lafIconset" select="if ($system) then $system/system/laf/@icon-set else 'default'"/>
  <xsl:variable name="loggedInUserID" select="$system/system/party/@id"/>
  <xsl:variable name="uiLocale" select="$system/system/@locale"/>
  <xsl:variable name="excludeFeatureKeys">
    <feature key="censhare:uuid"/>
    <feature key="censhare:asset.application"/>
    <feature key="censhare:asset.checked_out_by"/>
    <feature key="censhare:asset.checked_out_date"/>
    <feature key="censhare:asset.checked_out_inside_id"/>
    <feature key="censhare:asset.created_by"/>
    <feature key="censhare:asset.creation_date"/>
    <feature key="censhare:asset.currversion"/>
    <feature key="censhare:asset.deadline_actual"/>
    <feature key="censhare:asset.deadline_calc"/>
    <feature key="censhare:asset.deletion"/>
    <feature key="censhare:asset.edit-permission-key"/>
    <feature key="censhare:asset.first_actual_paging"/>
    <feature key="censhare:asset.first_paging"/>
    <feature key="censhare:asset.first_target_paging"/>
    <feature key="censhare:asset.id"/>
    <feature key="censhare:asset.id_extern"/>
    <feature key="censhare:asset.iscancellation"/>
    <feature key="censhare:asset.modified_by"/>
    <feature key="censhare:asset.modified_date"/>
    <feature key="censhare:asset.state"/>
    <feature key="censhare:asset.storage_state"/>
    <feature key="censhare:asset.wf_worst_state_id"/>
    <feature key="censhare:input-form"/>
  </xsl:variable>
  <xsl:variable name="useFeatures">
    <xsl:copy-of select="for $x in cs:cachelookup('feature')/feature return if ($excludeFeatureKeys/feature[@key=$x/@key]) then () else if ($x/@attribute_mapping) then (if (starts-with($x/@attribute_mapping, '@')) then $x else ()) else $x"/>
  </xsl:variable>
  <xsl:template match="/">
    <xsl:variable name="assetid" select="/container/asset/traits/ids/@id"/>
    <!--xsl:variable name="assetid" select="asset/@id"/--> 
    <xsl:if test="$assetid">
    <asset>
      <!-- Info asset -->
      <versions>
        <!-- Get asset versions -->
        <xsl:variable name="assets">
          <xsl:copy-of select="cs:get-asset-versions($assetid)"/>
        </xsl:variable>
        <xsl:variable name="sortedArrayOfAssets">
          <xsl:for-each select="$assets/asset">
            <xsl:sort select="number(@version)" data-type="number"/>
              <xsl:copy-of select="."/>
          </xsl:for-each>
        </xsl:variable>
        
        <xsl:for-each select="$sortedArrayOfAssets/asset">
          <xsl:sort select="number(@version)" data-type="number"/>
          <xsl:variable name="currentAsset" select="."/>
          <xsl:variable name="currentAssetIndex" select="position()"/>
          <xsl:variable name="previousAsset" select="if ($currentAssetIndex>1) then $sortedArrayOfAssets/asset[$currentAssetIndex - 1] else ()"/>
          <version>
            <version>
              <xsl:value-of select="$currentAsset/@version"/>
            </version>
            <previousVersion>
              <xsl:value-of select="$previousAsset/@version"/>
            </previousVersion>
            <assetId>
              <xsl:value-of select="$currentAsset/@id"/>
            </assetId>
            <hashcode>
			  <xsl:value-of select="$currentAsset/storage_item[@key='master']/@hashcode"/>
			</hashcode>
			<url>
			  <xsl:value-of select="$currentAsset/storage_item[@key='master']/@url"/>
			</url>
            <self>
              <xsl:value-of select="concat('asset/id/', $currentAsset/@id, '/version/', $currentAsset/@version)"/>
            </self>
            <modifiedDate>
              <xsl:value-of select="$currentAsset/@modified_date"/>
            </modifiedDate>
            <modifiedBy>
              <xsl:copy-of select="my:getChangedInfo($currentAsset/@modified_by)"/>
            </modifiedBy>
                
            <!-- Info changed attributes (don't show at first version) -->
              <xsl:if test="$previousAsset">
                <xsl:variable name="changedAssetFeatures">
                  <xsl:copy-of select="my:getChangedAssetAttributes($previousAsset, $currentAsset)"/>
                </xsl:variable>
                <xsl:if test="$changedAssetFeatures/feature">
                  <xsl:for-each select="$changedAssetFeatures/feature">
                    <change>
                      <type><xsl:value-of select="if (@key = 'censhare:asset.color') then 'color' else 'attribute'"/></type>
                      <name>
                        <xsl:value-of select="@name"/>
                      </name>
                      <key>
                        <xsl:value-of select="@key"/>
                      </key>
                      <modification>
                        <xsl:value-of select="@modification"/>
                      </modification>
                      <xsl:choose>
                        <xsl:when test="@modification='added'">
                          <currentValue>
                            <xsl:value-of select="@currentValue"/>
                          </currentValue>
                        </xsl:when>
                        <xsl:when test="@modification='removed'">
                          <previousValue>
                            <xsl:value-of select="@previousValue"/>
                          </previousValue>
                        </xsl:when>
                        <xsl:when test="@modification='changed'">
                          <currentValue>
                            <xsl:value-of select="@currentValue"/>
                          </currentValue>
                          <previousValue>
                            <xsl:value-of select="@previousValue"/>
                          </previousValue>
                        </xsl:when>
                      </xsl:choose>
                    </change>
                  </xsl:for-each>
                </xsl:if>
              </xsl:if>
              <!-- Info changed features -->
              <xsl:variable name="changedFeatures">
                <xsl:copy-of select="my:getChangedFeatures($previousAsset, $currentAsset)"/>
              </xsl:variable>
              <xsl:for-each select="$changedFeatures/feature">
                <xsl:variable name="changedFeatureElements">
                  <xsl:copy-of select="my:getChangedFeatureElements($previousAsset, $currentAsset, @key)"/>
                </xsl:variable>
                <xsl:if test="$changedFeatureElements/feature">
                  <xsl:for-each select="$changedFeatureElements/feature">
                    <change>
                      <xsl:choose>
                        <xsl:when test="@key = 'censhare:geo-coord'">
                          <type>coordinate</type>
                        </xsl:when>
                        <xsl:when test="@key = 'censhare:rating'">
                          <type>rating</type>
                        </xsl:when>
                        <xsl:otherwise>
                          <type>feature</type>
                        </xsl:otherwise>
                      </xsl:choose>
                      <name>
                        <xsl:value-of select="@name"/>
                      </name>
                      <key>
                        <xsl:value-of select="@key"/>
                      </key>
                      <modification>
                        <xsl:value-of select="@modification"/>
                      </modification>
                      <xsl:choose>
                        <xsl:when test="@modification='added'">
                          <currentValue>
                            <xsl:value-of select="if (@currentValue!='') then @currentValue else @name"/>
                            <!--No child features for the first version<xsl:copy-of select="my:getChildFeatures(.)"/>-->
                          </currentValue>
                        </xsl:when>
                        <xsl:when test="@modification='removed'">
                          <previousValue>
                            <xsl:value-of select="if (@previousValue!='') then @previousValue else @name"/>
                            <!--No child features for the first version<xsl:copy-of select="my:getChildFeatures(.)"/>-->
                          </previousValue>
                        </xsl:when>
                        <xsl:when test="@modification='modified'">
                          <previousValue>
                            <xsl:value-of select="if (@previousValue!='') then @previousValue else @name"/>
                            <!--No child features for the first version<xsl:copy-of select="my:getChildFeatures(.)"/>-->
                          </previousValue>
                          <currentValue>
                            <xsl:value-of select="@currentValue"/>
                            <!--No child features for the first version<xsl:copy-of select="my:getChildFeatures(.)"/>-->
                          </currentValue>
                        </xsl:when>
                        <xsl:when test="@modification='equals'">
                          <currentValue>
                            <xsl:value-of select="if (@currentValue!='') then @currentValue else @name"/>
                            <!--No child features for the first version<xsl:copy-of select="my:getChildFeatures(.)"/>-->
                          </currentValue>
                        </xsl:when>
                      </xsl:choose>
                    </change>
                  </xsl:for-each>
                </xsl:if>
              </xsl:for-each>
              <!-- Info changed master files -->
             
              <xsl:variable name="changedStorages">
                <xsl:copy-of select="my:getChangedMasterStorages($previousAsset, $currentAsset)"/>
              </xsl:variable>
              <xsl:if test="$changedStorages/storage">
                <xsl:for-each select="$changedStorages/storage">
                  <change>
                    <type>masterFile</type>
                    <!--ToDo: get translated value-->
                    <name>Masterfile</name>
                    <modification>
                      <xsl:value-of select="@modification"/>
                    </modification>
                    <xsl:choose>
                      <xsl:when test="@modification='removed'">
                        <previousValue>
                          <xsl:copy-of select="my:getPreviewElement($previousAsset, storage_item, 200, 200)"/>
                        </previousValue>
                      </xsl:when>
                      <xsl:when test="@modification='added'">
                        <currentValue>
                          <xsl:copy-of select="my:getPreviewElement($currentAsset, storage_item, 200, 200)"/>
                        </currentValue>
                      </xsl:when>
                      <xsl:when test="@modification='modified'">
                        <currentValue>
                          <xsl:copy-of select="my:getPreviewElement($currentAsset, storage_item, 200, 200)"/>
                        </currentValue>
                      </xsl:when>
                    </xsl:choose>
                  </change>
                </xsl:for-each>
              </xsl:if>
          </version>
        </xsl:for-each>
      </versions>
    </asset>
    </xsl:if>
  </xsl:template>

        <!-- Get changed asset attributes -->
        <xsl:function name="my:getChangedAssetAttributes">
          <xsl:param name="previousAsset" as="element(asset)"/>
          <xsl:param name="currentAsset" as="element(asset)"/>
          <xsl:variable name="usedAssetAttributeNames" select="distinct-values((for $x in $previousAsset/@* return node-name($x), for $x in $currentAsset/@* return node-name($x)))"/>
          <xsl:variable name="assetFeatures" select="for $x in $useFeatures/feature return if ($x/@attribute_mapping and $usedAssetAttributeNames=substring($x/@attribute_mapping, 2)) then $x else ()"/>
          <xsl:for-each select="$assetFeatures">
            <xsl:sort order="ascending" select="cs:cachelookup('feature', '@key', .)/@name"/>
            <xsl:variable name="previousValue" select="my:getFeatureAttribute($previousAsset, @attribute_mapping)"/>
            <xsl:variable name="currentValue" select="my:getFeatureAttribute($currentAsset, @attribute_mapping)"/>
            <xsl:choose>
              <xsl:when test="$previousValue and not($currentValue)">
                <!-- Feature removed -->
                <feature key="{@key}" name="{@name}" previousValue="{$previousValue}" attribute_mapping="{@attribute_mapping}" modification="removed"/>
              </xsl:when>
              <xsl:when test="not($previousValue) and $currentValue">
                <!-- Feature added -->
                <feature key="{@key}" name="{@name}" currentValue="{$currentValue}" attribute_mapping="{@attribute_mapping}" modification="added"/>
              </xsl:when>
              <xsl:when test="$previousValue and $currentValue and ($previousValue != $currentValue)">
                <!-- Feature changed -->
                <feature key="{@key}" name="{@name}" previousValue="{$previousValue}" currentValue="{$currentValue}" attribute_mapping="{@attribute_mapping}" modification="changed"/>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </xsl:function>

        <!-- Get changed features of given previousParentNode and given currentParentNode -->
        <xsl:function name="my:getChangedFeatures">
          <xsl:param name="previousParentNode"/>
          <xsl:param name="currentParentNode"/>
          <xsl:variable name="usedAssetFeatureKeys" select="distinct-values(($previousParentNode/asset_feature/@feature, $currentParentNode/asset_feature/@feature))"/>
          <xsl:variable name="assetFeatures"><xsl:copy-of select="for $x in $useFeatures/feature return if ($x/@attribute_mapping) then () else if ($usedAssetFeatureKeys=$x/@key) then $x else ()"/></xsl:variable>
          <xsl:for-each select="$assetFeatures/feature">
            <xsl:sort order="ascending" select="cs:cachelookup('feature', '@key', .)/@name"/>
            <xsl:copy-of select="."/>
          </xsl:for-each>
        </xsl:function>

        <!-- Get changed feature elements -->
        <xsl:function name="my:getChangedFeatureElements">
          <xsl:param name="previousParentNode"/>
          <xsl:param name="currentParentNode"/>
          <xsl:param name="key" as="xs:string"/>
          <xsl:variable name="previousFeatureElements"><xsl:copy-of select="my:getChildFeatureElements($previousParentNode, $key)"/></xsl:variable>
          <xsl:variable name="currentFeatureElements"><xsl:copy-of select="my:getChildFeatureElements($currentParentNode, $key)"/></xsl:variable>
          <xsl:variable name="values" select="distinct-values(($previousFeatureElements/feature/@value, $currentFeatureElements/feature/@value))"/>
          <xsl:for-each select="$values">
            <xsl:sort order="ascending" select="."/>
            <xsl:variable name="value" select="."/>
            <xsl:variable name="previousFeatureElement" select="$previousFeatureElements/feature[@value=$value][1]"/>
            <xsl:variable name="currentFeatureElement" select="$currentFeatureElements/feature[@value=$value][1]"/>
            <xsl:variable name="previousAssetFeature" select="$previousParentNode/asset_feature[@sid=$previousFeatureElement/@sid]"/>
            <xsl:variable name="currentAssetFeature" select="$currentParentNode/asset_feature[@sid=$currentFeatureElement/@sid]"/>
            <xsl:variable name="childFeatureElements">
              <xsl:variable name="changedFeatures"><xsl:copy-of select="my:getChangedFeatures($previousAssetFeature, $currentAssetFeature)"/></xsl:variable>
              <xsl:for-each select="$changedFeatures/feature">
                <xsl:copy-of select="my:getChangedFeatureElements($previousAssetFeature, $currentAssetFeature, @key)"/>
              </xsl:for-each>
            </xsl:variable>
            <xsl:variable name="modification" select="."/>
            <xsl:choose>
              <!-- Check removed -->
              <xsl:when test="$previousFeatureElement and empty($currentFeatureElement)">
                <feature key="{$previousFeatureElement/@key}" name="{$previousFeatureElement/@name}" previousValue="{$value}" previous-sid="{$previousFeatureElement/@sid}" modification="removed">
                  <xsl:copy-of select="$childFeatureElements/feature"/>
                </feature>
              </xsl:when>
              <!-- Check added -->
              <xsl:when test="empty($previousFeatureElement) and $currentFeatureElement">
                <feature key="{$currentFeatureElement/@key}" name="{$currentFeatureElement/@name}" currentValue="{$value}" current-sid="{$currentFeatureElement/@sid}" modification="added">
                  <xsl:copy-of select="$childFeatureElements/feature"/>
                </feature>
              </xsl:when>
              <!-- Check modified -->
              <xsl:when test="$previousFeatureElement and $currentFeatureElement">
                <xsl:if test="$childFeatureElements/feature">
                  <feature key="{$currentFeatureElement/@key}" name="{$currentFeatureElement/@name}" previousValue="{$value}" previous-sid="{$previousFeatureElement/@sid}" currentValue="{$value}" current-sid="{$currentFeatureElement/@sid}" modification="equals">
                    <xsl:copy-of select="$childFeatureElements/feature"/>
                  </feature>
                </xsl:if>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each>
        </xsl:function>

        <!-- Get child feature elements of given parentNode with given key -->
        <xsl:function name="my:getChildFeatureElements">
          <xsl:param name="parentNode"/>
          <xsl:param name="key" as="xs:string"/>
          <xsl:variable name="assetFeatures" select="if ($key) then $parentNode/asset_feature[@feature=$key] else $parentNode/asset_feature"/>
          <xsl:for-each select="$assetFeatures">
            <feature key="{@feature}" name="{my:getFeatureName(.)}" value="{my:getFeatureValue(.)}" sid="{@sid}"/>
          </xsl:for-each>
        </xsl:function>

        <!-- Get changed master storages -->
        <xsl:function name="my:getChangedMasterStorages">
          <xsl:param name="previousAsset" as="element(asset)"/>
          <xsl:param name="currentAsset" as="element(asset)"/>
          <xsl:variable name="previousMasterStorages" select="$previousAsset/storage_item[@key='master']"/>
          <xsl:variable name="currentMasterStorages" select="$currentAsset/storage_item[@key='master']"/>
          <xsl:variable name="removedStorages" select="for $x in $previousMasterStorages return if ($currentMasterStorages[@element_idx=$x/@element_idx]) then () else $x"/>
          <xsl:variable name="addedStorages" select="for $x in $currentMasterStorages return if ($previousMasterStorages[@element_idx=$x/@element_idx]) then () else $x"/>
          <xsl:variable name="modifiedStorages" select="for $x in $currentMasterStorages return if ($previousMasterStorages[@element_idx=$x/@element_idx] and $previousMasterStorages[@element_idx=$x/@element_idx]/@relpath != $x/@relpath) then $x else ()"/>

          <xsl:if test="$removedStorages">
            <xsl:for-each select="$removedStorages">
              <storage modification="removed">
                <xsl:copy-of select="."/>
              </storage>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="$addedStorages">
            <xsl:for-each select="$addedStorages">
              <storage modification="added">
                <xsl:copy-of select="."/>
              </storage>
            </xsl:for-each>
          </xsl:if>
          <xsl:if test="$modifiedStorages">
            <xsl:for-each select="$modifiedStorages">
              <xsl:variable name="currentStorage" select="."/>
              <xsl:variable name="previousStorage" select="$previousMasterStorages[@element_idx=$currentStorage/@element_idx]"/>
              <!-- Check differences of text-previews -->
              <xsl:variable name="currentTextPreviewStorage" select="my:getTextPreviewStorageItem($currentAsset, $currentStorage)"/>
              <xsl:variable name="previousTextPreviewStorage" select="my:getTextPreviewStorageItem($previousAsset, $previousStorage)"/>
              <xsl:choose>
                <xsl:when test="$previousTextPreviewStorage and $currentTextPreviewStorage">
                  <xsl:variable name="currentContent"><xsl:value-of select="doc(concat($urlPrefix, 'assets/asset/id/', $currentTextPreviewStorage/@asset_id, if ($currentAsset/@currversion='0') then '' else concat('/version/', $currentAsset/@version), '/element/actual/', $currentTextPreviewStorage/@element_idx, '/storage/text-preview/file/', tokenize($currentTextPreviewStorage/@relpath,'/')[last()]))"/></xsl:variable>
                  <xsl:variable name="previousContent"><xsl:value-of select="doc(concat($urlPrefix, 'assets/asset/id/', $previousTextPreviewStorage/@asset_id, if ($previousAsset/@currversion='0') then '' else concat('/version/', $previousAsset/@version), '/element/actual/', $previousTextPreviewStorage/@element_idx, '/storage/text-preview/file/', tokenize($previousTextPreviewStorage/@relpath,'/')[last()]))"/></xsl:variable>
                  <xsl:if test="($currentContent/html/body and $previousContent/html/body and ($currentContent/html/body != $previousContent/html/body)) or ($currentContent!=$previousContent)">
                    <storage modification="modified">
                      <xsl:copy-of select="."/>
                    </storage>
                  </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                  <storage modification="modified">
                    <xsl:copy-of select="."/>
                  </storage>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each>
          </xsl:if>
        </xsl:function>

        <!-- Get icon image of given key -->
        <xsl:function name="my:getIconImage">
          <xsl:param name="key"/>
          <imgUrl>
            <xsl:value-of select="concat($urlPrefix, 'resources/icon/', $key, '/iconset/', $lafIconset, '/background/', $lafBackground, '/file')"/>
          </imgUrl>
        </xsl:function>

        <!-- Get preview element of given asset and given masterStorageItem -->
        <xsl:function name="my:getPreviewElement">
          <xsl:param name="asset" as="element(asset)"/>
          <xsl:param name="masterStorageItem" as="element(storage_item)"/>
          <xsl:param name="maxWidth" as="xs:integer"/>
          <xsl:param name="maxHeight" as="xs:integer"/>
          <xsl:variable name="previewStorage" select="my:getAnyPreviewStorageItem($asset, $masterStorageItem)"/>
          <xsl:choose>
            <!-- Check preview storage -->
            <xsl:when test="$previewStorage[@key='thumbnail']">
              <imgUrl>
                <xsl:value-of select="concat($urlPrefix, 'assets/asset/id/', $previewStorage/@asset_id, if ($asset/@currversion='0') then '' else concat('/version/', $asset/@version), '/element/actual/page/0/storage/thumbnail/file/', tokenize($previewStorage/@relpath,'/')[last()])"/>
              </imgUrl>
            </xsl:when>
            <!-- Check text preview storage -->
            <xsl:when test="$previewStorage[@key='text-preview' and @mimetype='application/xhtml+xml']">
              <xsl:copy-of select="doc(concat($urlPrefix, 'assets/asset/id/', $previewStorage/@asset_id, if ($asset/@currversion='0') then '' else concat('/version/', $asset/@version), '/element/actual/', $previewStorage/@element_idx, '/storage/text-preview/file/', tokenize($previewStorage/@relpath,'/')[last()]))/html/body"/>
            </xsl:when>
            <!-- Otherwise relpath output -->
            <xsl:otherwise>
              <xsl:value-of select="$asset/storage_item[@key='master']/@relpath"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:function>

        <!-- Get any (thumbnail and text-preview) storageItem of given asset and given masterStorageItem -->
        <xsl:function name="my:getAnyPreviewStorageItem">
          <xsl:param name="asset" as="element(asset)"/>
          <xsl:param name="masterStorageItem" as="element(storage_item)"/>
          <!-- Check thumbnail storage with same element index -->
          <xsl:variable name="thumbnailStorage" select="$asset/storage_item[@key='thumbnail' and @element_idx=$masterStorageItem/@element_idx][1]"/>
          <xsl:choose>
            <xsl:when test="$thumbnailStorage">
              <xsl:copy-of select="$thumbnailStorage"/>
            </xsl:when>
            <xsl:otherwise>
              <!-- Check text-preview storage with same element index -->
              <xsl:variable name="thumbnailStorage" select="$asset/storage_item[@key='text-preview' and @element_idx=$masterStorageItem/@element_idx][1]"/>
              <xsl:choose>
                <xsl:when test="$thumbnailStorage">
                  <xsl:copy-of select="$thumbnailStorage"/>
                </xsl:when>
                <xsl:otherwise>
                  <!-- Check preview storage of child elements -->
                  <xsl:variable name="firstChildElement" select="for $x in $asset/asset_element return if ($x/@key='actual.' and $x/@parent_idx=$masterStorageItem/@element_idx and empty($asset/asset_element[@key='actual.' and @next_idx=$x/@idx])) then $x else ()"/>
                  <xsl:variable name="thumbnailStorage" select="if ($firstChildElement) then $asset/storage_item[@key='thumbnail' and @element_idx=$firstChildElement/@idx][1] else ()"/>
                  <xsl:choose>
                    <xsl:when test="$thumbnailStorage">
                      <xsl:copy-of select="$thumbnailStorage[1]"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:copy-of select="()"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:function>

        <!-- Get text-preview storageItem of given asset and given masterStorageItem -->
        <xsl:function name="my:getTextPreviewStorageItem">
          <xsl:param name="asset" as="element(asset)"/>
          <xsl:param name="masterStorageItem" as="element(storage_item)"/>
          <xsl:copy-of select="$asset/storage_item[@key='text-preview' and @element_idx=$masterStorageItem/@element_idx][1]"/>
        </xsl:function>

        <!-- Get child features -->
        <xsl:function name="my:getChildFeatures">
          <xsl:param name="featureElement" as="element(feature)"/>
          <xsl:variable name="childFeatureElements" select="$featureElement/feature"/>
          <xsl:if test="$childFeatureElements">
            <xsl:for-each select="$childFeatureElements">
              <table cellpadding="0">
                <tr>
                  <xsl:choose>
                    <xsl:when test="@modification='added'">
                      <td width="20"><xsl:copy-of select="my:getIconImage('add')"/></td>
                      <td class="asset-info-value">
                        <span class="asset-info-label-inline"><xsl:value-of select="@name"/>: </span>
                        <xsl:value-of select="if (@currentValue!='') then @currentValue else @name"/>
                        <xsl:copy-of select="my:getChildFeatures(.)"/>
                      </td>
                    </xsl:when>
                    <xsl:when test="@modification='removed'">
                      <td width="20"><xsl:copy-of select="my:getIconImage('delete')"/></td>
                      <td class="asset-info-value">
                        <span class="asset-info-label-inline"><xsl:value-of select="@name"/>: </span>
                        <xsl:value-of select="if (@previousValue!='') then @previousValue else @name"/>
                        <xsl:copy-of select="my:getChildFeatures(.)"/>
                      </td>
                    </xsl:when>
                    <xsl:when test="@modification='modified'">
                      <td width="20"><xsl:copy-of select="my:getIconImage('edit')"/></td>
                      <td class="asset-info-value">
                        <span class="asset-info-label-inline"><xsl:value-of select="@name"/>: </span>
                        <span style="text-decoration:line-through"><xsl:value-of select="if (@previousValue!='') then @previousValue else @name"/></span>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="@currentValue"/>
                        <xsl:copy-of select="my:getChildFeatures(.)"/>
                      </td>
                  </xsl:when>
                  <xsl:when test="@modification='equals'">
                      <td width="20">•</td>
                      <td class="asset-info-value">
                        <span class="asset-info-label-inline"><xsl:value-of select="@name"/>: </span>
                        <xsl:value-of select="if (@currentValue!='') then @currentValue else @name"/>
                        <xsl:copy-of select="my:getChildFeatures(.)"/>
                      </td>
                    </xsl:when>
                  </xsl:choose>
                </tr>
              </table>
            </xsl:for-each>
          </xsl:if>
        </xsl:function>

        <!-- Creates preview of party of given $by and text of given $modifiedUserID and $modifiedDate -->
        <xsl:function name="my:getChangedInfo">
          <xsl:param name="modifiedUserID"/>
          <xsl:variable name="maxPreviewSize" select="32"/>
          <xsl:variable name="byAssetID" select="cs:cachelookup('party', '@id', $modifiedUserID)/@party_asset_id"/>
          <xsl:variable name="byAsset" select="cs:get-asset($byAssetID, 0, 0)"/>
          <xsl:variable name="thumbnailStorage" select="$byAsset/storage_item[@key='thumbnail'][1]"/>
          <by>
            <xsl:value-of select="if ($modifiedUserID) then (if ($modifiedUserID=$loggedInUserID) then '${myself}' else cs:cachelookup('party', '@id', $modifiedUserID)/@display_name) else '${multiple-values}'"/>
          </by>
          <xsl:choose>
            <xsl:when test="$thumbnailStorage">
                <avatar>
                    <xsl:value-of select="concat($urlPrefix, 'assets/asset/id/', $thumbnailStorage/@asset_id, '/element/actual/', $thumbnailStorage/@element_idx, '/storage/', $thumbnailStorage/@key, '/file/', tokenize($thumbnailStorage/@relpath,'/')[last()])"/>
                </avatar>
            </xsl:when>
            <xsl:otherwise>
              <avatar>
                <xsl:value-of select="if (byAssetID) then concat($urlPrefix, 'assets/asset/id/', $byAssetID, '/version/', $byAsset/@version, '/icon/minsize/48/file') else concat($urlPrefix, 'resources/icon/assettype-person./minsize/48/file')"/>
              </avatar>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:function>

        <!-- Get localized name of given asset feature element -->
        <xsl:function name="my:getFeatureName" as="xs:string">
          <xsl:param name="featureElement" as="element(asset_feature)"/>
          <xsl:value-of select="cs:cachelookup('feature', '@key', $featureElement/@feature)/@name"/>
        </xsl:function>

        <!-- Get localized value of given feature element -->
        <xsl:function name="my:getFeatureAttribute" as="xs:string">
          <xsl:param name="asset" as="element(asset)"/>
          <xsl:param name="attributeMapping" as="xs:string"/>
          <xsl:variable name="name" select="tokenize($attributeMapping, '@')[last()]"/>
          <xsl:variable name="value" select="$asset/attribute()[node-name(.)=$name]"/>
          <xsl:choose>
            <xsl:when test="$name='domain'">
              <xsl:value-of select="my:getLocalizedValueHierarchical($value, 'domain', '@pathid', '/', false())"/>
            </xsl:when>
            <xsl:when test="$name='domain2'">
              <xsl:value-of select="my:getLocalizedValueHierarchical($value, 'domain2', '@pathid', '/', false())"/>
            </xsl:when>
            <xsl:when test="$name='type'">
              <xsl:value-of select="my:getLocalizedValueHierarchical($value, 'asset_typedef', '@asset_type', '/', false())"/>
            </xsl:when>
            <xsl:when test="$name='language'">
              <xsl:value-of select="cs:cachelookup('language_def', '@id', $value)/@name"/>
            </xsl:when>
            <xsl:when test="$name='wf_id'">
              <xsl:value-of select="cs:cachelookup('workflow', '@id', $value)/@description"/>
            </xsl:when>
            <xsl:when test="$name='wf_step'">
              <xsl:variable name="wfID" select="$asset/@wf_id"/>
              <xsl:value-of select="cs:cachelookup('workflow_step', '@wf_id', $wfID, '@wf_step', $value)/@name"/>
            </xsl:when>
            <xsl:when test="$name='wf_target'">
              <xsl:value-of select="cs:cachelookup('party', '@id', $value)/@display_name"/>
            </xsl:when>
            <xsl:when test="$name='non_owner_access'">
            <xsl:choose>
                <xsl:when test="$value='0'">
                  ${unrestricted-access}
                </xsl:when>
                <xsl:when test="$value='1'">
                  ${read-only-access}
                </xsl:when>
                <xsl:when test="$value='2'">
                  ${no-access}
                </xsl:when>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$value"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:function>

        <!-- Get localized value of given feature element -->
        <xsl:function name="my:getFeatureValue">
          <xsl:param name="featureElement" as="element(asset_feature)"/>
          <xsl:variable name="type" select="cs:cachelookup('feature', '@key', $featureElement/@feature)/@value_type"/>
          <xsl:choose>
            <!-- 0: No value -->
            <xsl:when test="$type='0'">
              <xsl:value-of select="''"/>
            </xsl:when>
            <!-- 1: Hierarchical attribute (string) -->
            <xsl:when test="$type='1'">
              <xsl:value-of select="cs:cachelookup('feature_value', '@feature', $featureElement/@feature, '@value_key', $featureElement/@value_key)/@name"/>
            </xsl:when>
            <!-- 2: Enumeration (string) -->
            <xsl:when test="$type='2'">
              <xsl:value-of select="cs:cachelookup('feature_value', '@feature', $featureElement/@feature, '@value_key', $featureElement/@value_key)/@name"/>
            </xsl:when>
            <!-- 3: Integer (long) -->
            <xsl:when test="$type='3'">
              <xsl:choose>
                <xsl:when test="$featureElement/@feature='censhare:owner'">
                  <xsl:value-of select="cs:cachelookup('party', '@id', $featureElement/@value_long)/@display_name"/>
                </xsl:when>
                <!-- String -->
                <xsl:otherwise>
                  <xsl:value-of select="string-join((cs:format-number($featureElement/@value_long, '#,###'), my:getFeatureUnitString($featureElement)), ' ')"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <!-- 4: String -->
            <xsl:when test="$type='4'">
              <xsl:choose>
                <!-- censhare URL -->
                <xsl:when test="$featureElement/@feature='censhare:url'">
                  <a class="asset-info-link" target="_blank">
                    <xsl:attribute name="href" select="$featureElement/@value_string"/>
                    <xsl:value-of select="$featureElement/@value_string"/>
                  </a>
                </xsl:when>
                <!-- String -->
                <xsl:otherwise>
                  <xsl:value-of select="$featureElement/@value_string"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <!-- 5: Timestamp -->
            <xsl:when test="$type='5'">
              <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'relative-short', 'short')"/>
            </xsl:when>
            <!-- 6: Boolean -->
            <xsl:when test="$type='6'">
              <xsl:value-of select="if ($featureElement/@value_long='1') then '${yes}' else '${no}'"/>
            </xsl:when>
            <!-- 7: Double -->
            <xsl:when test="$type='7'">
              <xsl:value-of select="string-join((cs:format-number($featureElement/@value_double, '#,###.###'), my:getFeatureUnitString($featureElement)), ' ')"/>
            </xsl:when>
            <!-- 8: Integer pair -->
            <xsl:when test="$type='8'">
              <xsl:value-of select="string-join((concat(cs:format-number($featureElement/@value_long, '#,###'), '-', cs:format-number($featureElement/@value_long2, '#,###')), my:getFeatureUnitString($featureElement)), ' ')"/>
            </xsl:when>
            <!-- 9: Timestamp pair -->
            <xsl:when test="$type='9'">
              <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'relative-short', 'short'), '-', cs:format-date($featureElement/@value_timestamp2, 'relative-short', 'short'))"/>
            </xsl:when>
            <!-- 10: Asset reference -->
            <xsl:when test="$type='10'">
              <xsl:variable name="refAsset" select="cs:get-asset($featureElement/@value_asset_id)"/>
              <xsl:choose>
                <xsl:when test="exists($refAsset)">
                  <xsl:value-of select="if ($refAsset/asset_feature[@feature='censhare:name' and @language=$uiLocale]) then $refAsset/asset_feature[@feature='censhare:name' and @language=$uiLocale]/@value_string else $refAsset/@name"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="concat('ID: ', @value_asset_id)"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <!-- 11: XML -->
            <xsl:when test="$type='11'">
              <xsl:value-of select="''"/>
            </xsl:when>
            <!-- 12: Double pair -->
            <xsl:when test="$type='12'">
              <xsl:value-of select="string-join((concat(cs:format-number($featureElement/@value_double, '#,###.###'), '-', cs:format-number($featureElement/@value_double2, '#,###.###')), my:getFeatureUnitString($featureElement)), ' ')"/>
            </xsl:when>
            <!-- 13: Date -->
            <xsl:when test="$type='13'">
              <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'relative-short', 'none')"/>
            </xsl:when>
            <!-- 14: Date pair -->
            <xsl:when test="$type='14'">
              <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'relative-short', 'none'), '-', cs:format-date($featureElement/@value_timestamp2, 'relative-short', 'none'))"/>
            </xsl:when>
            <!-- 15: Time -->
            <xsl:when test="$type='15'">
              <xsl:value-of select="cs:format-date($featureElement/@value_timestamp, 'none', 'short')"/>
            </xsl:when>
            <!-- 16: Time pair -->
            <xsl:when test="$type='16'">
              <xsl:value-of select="concat(cs:format-date($featureElement/@value_timestamp, 'none', 'short'), '-', cs:format-date($featureElement/@value_timestamp2, 'none', 'short'))"/>
            </xsl:when>
            <!-- 17: Year -->
            <xsl:when test="$type='17'">
              <xsl:value-of select="year-from-dateTime($featureElement/@value_timestamp)"/>
            </xsl:when>
            <!-- 18: Year pair -->
            <xsl:when test="$type='18'">
              <xsl:value-of select="concat(year-from-dateTime($featureElement/@value_timestamp), '-', year-from-dateTime($featureElement/@value_timestamp2))"/>
            </xsl:when>
            <!-- 19: Year/month -->
            <xsl:when test="$type='19'">
              <xsl:value-of select="concat(my:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), ' ', year-from-dateTime($featureElement/@value_timestamp))"/>
            </xsl:when>
            <!-- 20: Year/month pair -->
            <xsl:when test="$type='20'">
              <xsl:value-of select="concat(my:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), ' ', year-from-dateTime($featureElement/@value_timestamp), '-', my:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)), ' ', year-from-dateTime($featureElement/@value_timestamp2))"/>
            </xsl:when>
            <!-- 21: Month -->
            <xsl:when test="$type='21'">
              <xsl:value-of select="my:getMonthName(month-from-dateTime($featureElement/@value_timestamp))"/>
            </xsl:when>
            <!-- 22: Month pair -->
            <xsl:when test="$type='22'">
              <xsl:value-of select="concat(my:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), '-', my:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)))"/>
            </xsl:when>
            <!-- 23: Month/day -->
            <xsl:when test="$type='23'">
              <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '. ', my:getMonthName(month-from-dateTime($featureElement/@value_timestamp)))"/>
            </xsl:when>
            <!-- 24: Month/day pair -->
            <xsl:when test="$type='24'">
              <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '. ', my:getMonthName(month-from-dateTime($featureElement/@value_timestamp)), '-', day-from-dateTime($featureElement/@value_timestamp2), '. ', my:getMonthName(month-from-dateTime($featureElement/@value_timestamp2)))"/>
            </xsl:when>
            <!-- 25: Day -->
            <xsl:when test="$type='25'">
              <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '.')"/>
            </xsl:when>
            <!-- 26: Day pair -->
            <xsl:when test="$type='26'">
              <xsl:value-of select="concat(day-from-dateTime($featureElement/@value_timestamp), '.-', day-from-dateTime($featureElement/@value_timestamp2), '.')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="'${none}'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:function>

        <!-- Get localized value of given feature element -->
        <xsl:function name="my:getFeatureUnitString" as="xs:string">
          <xsl:param name="featureElement" as="element(asset_feature)"/>
          <xsl:value-of select="if ($featureElement/@value_unit) then cs:cachelookup('unit_set_rel', '@code', $featureElement/@value_unit)/@symbol else ()"/>
        </xsl:function>

        <!-- Get localized name of given month (integer) -->
        <xsl:function name="my:getMonthName" as="xs:string">
          <xsl:param name="month" as="xs:integer"/>
          <xsl:choose>
            <xsl:when test="$month=1">${january}</xsl:when>
            <xsl:when test="$month=2">${february}</xsl:when>
            <xsl:when test="$month=3">${march}</xsl:when>
            <xsl:when test="$month=4">${april}</xsl:when>
            <xsl:when test="$month=5">${may}</xsl:when>
            <xsl:when test="$month=6">${june}</xsl:when>
            <xsl:when test="$month=7">${july}</xsl:when>
            <xsl:when test="$month=8">${august}</xsl:when>
            <xsl:when test="$month=9">${september}</xsl:when>
            <xsl:when test="$month=10">${october}</xsl:when>
            <xsl:when test="$month=11">${november}</xsl:when>
            <xsl:when test="$month=12">${december}</xsl:when>
          </xsl:choose>
        </xsl:function>

        <!-- Get localized name of a given dot separated value -->
        <xsl:function name="my:getLocalizedValueHierarchical" as="xs:string">
          <xsl:param name="value" as="xs:string"/>
          <xsl:param name="cachedTableName" as="xs:string"/>
          <xsl:param name="cachedTableAttribute" as="xs:string"/>
          <xsl:param name="delimiter" as="xs:string"/>
          <xsl:param name="hideRoot" as="xs:boolean"/>
          <xsl:variable name="items" select="tokenize($value, '\.')"/>
          <xsl:variable name="startItem" select="if ($hideRoot) then 2 else 1"/>
          <xsl:value-of select="string-join(for $x in ($startItem to xs:long(count($items))) return cs:cachelookup($cachedTableName, $cachedTableAttribute, concat(string-join(subsequence($items, 1, $x), '.'), '.'))/@name, $delimiter)"/>
        </xsl:function>

</xsl:stylesheet>