import * as censhare from 'censhare';
import * as angular from 'angular';
import * as nvd3Lib from '../../../../3rd-Party/node_modules/nvd3';
import * as d3Lib from '../../../../3rd-Party/node_modules/d3';
import { IWidgetInstance } from '../../../../cs/client/frames/csMainFrame/csDefaultPages/default-pages.types';
import { IPageInstance } from '../../../../cs/client/frames/csMainFrame/csWorkspaceManager/csPageRegistry/page-registry.types';
import { INotify } from '../../../../cs/client/frames/csMainFrame/notifications/csNotify/notify';
import { IcsApiSession } from '../../../../cs/framework/csApi/csApi';
import { IcsNavigationManager } from '../../../../cs/client/frames/csMainFrame/csWorkspaceManager/csNavigationManager/navigation-manager';

const m = censhare.module('ioiPieChartWidget');

const LOG_PREFIX = 'ioi Pie Chart Widget - ';
const COLORS = ['#f0645a', '#28d782', '#69bee6', '#41464b'];
const WIDTH = 370;
const HEIGHT = 300;
const MARGINS = { top: 0, right: 10, bottom: 20, left: 10 }; // ToDo: How is the default top margin calculated?

class ChartOptions {
    height: number;
    width: number;
    showLegend: boolean;
    labelType: string;
    showToolTipPercent: string;
    valueFormat: string;
    noData: string;
    options: ChartOptionsOpts;
}

class ChartOptionsOpts {
    showLabels: boolean;
}

class IoiPieChart implements ng.IDirective {
    static $name: string = 'ioiPieChart';
    static $inject: string[] = ['$timeout', 'csNotify'];

    public restrict: string = 'E';
    public require: string = 'ngModel';
    public replace: boolean = true;
    public template: string = '<svg class="ioi-pie-chart"></svg>';

    private nv = window.nv;
    private d3 = window.d3;

    link = (scope: ng.IScope, element: ng.IAugmentedJQuery, attr: ng.IAttributes, ngModel: any) => {

        let options = new ChartOptions();
        let renderTimeout,
            currentType,
            currentChart;

        options.width = attr.csWidth ? attr.csWidth : WIDTH;
        options.height = attr.csHeight ? attr.csHeight : HEIGHT;
        options.showLegend = attr.showLegend ? attr.showLegend : false;
        options.labelType = attr.labelType ? attr.labelType : 'value';
        options.showToolTipPercent = attr.showToolTipPercent ? attr.showToolTipPercent : true;
        options.valueFormat = attr.valueformat ? attr.valueformat : undefined;
        options.noData = attr.noData ? attr.noData : 'No data found!';
        options.options = {
            showLabels: true
        };

        function preparePieDefaultOptions(opts) {
            angular.extend(opts.options, {
                showLabels: true,
                color: COLORS,
                margin: MARGINS,

            }, angular.copy(opts.options));
        }

        function createPie(node, data, opts, donut, callback) {

            nv.addGraph(() => {
                let chart = nv.models.pieChart()
                    .width(opts.width)
                    .height(opts.height)
                    .x((d) => {
                        return d.key;
                    })
                    .y((d) => {
                        return d.value;
                    })
                    .options(opts.options)
                    .color((d, idx) => {
                        let color = d.color || opts.options.color[idx % opts.options.color.length];
                        return color;
                    })
                    .labelType(opts.labelType)
                    .valueFormat(d3.format(opts.valueFormat))
                    .showTooltipPercent(opts.showToolTipPercent)
                    .donut(donut)
                    .showLegend(false);

                d3.select(node)
                    .datum(data)
                    .transition().duration(1200)
                    .attr('width', opts.width)
                    .attr('height', opts.height)
                    .call(chart);

                return chart;
            }, callback);
        }

        function updateChart(node, data, opts, chart) {
            d3.select(node)
                .datum(data)
                .attr('width', opts.width)
                .attr('height', opts.height)
                .call(chart);
        }

        function clearCurrentChart() {
            if (currentChart) {
                element.children().remove();
                nv.charts = {};
                currentChart = null;
            }
        }

        function createChartCallback(chart) {
            // clearCurrentChart(); // removed, so the charts do not disappear
            currentChart = chart;

            d3.selectAll('.nv-slice')
                .on('click', (elem) => {
                    if (elem.data.query.ids !== undefined) {
                        d3.selectAll('.nvtooltip').remove();
                        scope.$parent.$parent.runQuery(elem.data.query.ids);
                    }
                });
        }

        attr.$observe('type', function (type) {
            if (type) {
                deferredRender();
            }
        });

        attr.$observe('csWidth', function (val: number) {
            if (val && val !== options.width) {
                options.width = val;
                deferredRender();
            }
        });

        attr.$observe('csHeight', function (val: number) {
            if (val && val !== options.height) {
                options.height = val;
                deferredRender();
            }
        });

        attr.$observe('legend', function (val: boolean) {
            if (angular.isDefined(val) && val !== options.showLegend) {
                options.showLegend = val;
                deferredRender();
            }
        });

        function deferredRender() {
            if (renderTimeout) {
                ngModel.$$timeout.cancel(renderTimeout);
            }
            renderTimeout = ngModel.$$timeout(ngModel.$render, 200);
        }

        ngModel.$render = () => {
            let config, data;

            element.css({
                width: options.width + 'px',
                height: options.height + 'px'
            });

            if (ngModel.$viewValue) {
                if (ngModel.$viewValue.hasOwnProperty('data')) {

                    config = ngModel.$viewValue;
                    data = config.data;

                    options.width = config.csWidth || options.width;
                    options.height = config.csHeight || options.height;

                    if (typeof attr.legend === 'undefined') {
                        attr.legend = false;
                    }

                } else {
                    data = ngModel.$viewValue;

                }
            }

            if (data && !!attr.type) {
                preparePieDefaultOptions(options);
                if (currentChart) {
                    updateChart(element[0], data, options, currentChart);
                    return;
                }
                createPie(element[0], data, options, false, createChartCallback);
            }

            ngModel.$viewChangeListeners.push(ngModel.$render);

            scope.$on('$destroy', function () {
                if (renderTimeout) {
                    ngModel.$$timeout.cancel(renderTimeout);
                }

                clearCurrentChart();
                element.empty();
            });
        };
    }
}

class IoiPieChartWidgetHeadless {
    static $name: string = 'ioiPieChartWidgetHeadless';
    static $inject = ['widgetInstance'];
    public someParameters: any;

    constructor(widgetInstance: IWidgetInstance) {

    }
}

class IoiPieChartWidgetController {
    static $name: string = 'ioiPieChartWidgetController';
    static $inject = ['$scope', '$http', 'widgetDataManager', 'widgetInstance', 'pageInstance', 'csApiSession', 'csNavigationManager', 'csNotify', 'csWidgetConfigDialog', 'csQueryBuilder'];

    constructor($scope, $http, widgetDataManager, widgetInstance, pageInstance, csApiSession, csNavigationManager: IcsNavigationManager, csNotify: INotify, csWidgetConfigDialog, csQueryBuilder) {

        let assetID: number;

        if (pageInstance.getPathContextReader() && pageInstance.getPathContextReader().getValue()) {
            assetID = pageInstance.getPathContextReader().getValue().get('id');
        }

        // listen for widget dimensions change
        widgetInstance.getDimensions().registerChangeListenerAndFire(function (newValue) {
            $scope.dimensions = newValue || {};
            setContentSize();
        }, $scope);

        $scope.widgetConfiguration = function () {
            csWidgetConfigDialog(widgetInstance);
        };

        // set content size (calculated by grid size 270x125)
        function setContentSize() {
            let subTitleHeight = $scope.chart && $scope.chart.subTitle ? 14 : 0;
            $scope.contentWidth = ($scope.dimensions.width * (270 + 20)) - 40;                    // Example: 1 width * 290 -40 = 250px
            $scope.contentHeight = ($scope.dimensions.height * (125 + 20)) - 50 - subTitleHeight; // Example: 1 height * 125 -20 = 105px
        }

        // initialize
        $scope.chart = null;
        $scope.dataChecked = false;
        $scope.isGetValues = false;

        // widget configuration button in empty state
        $scope.widgetConfiguration = () => {
            csWidgetConfigDialog(widgetInstance);
        };

        // remove asset configuration
        function removeAssetConfig() {
            widgetInstance.setCounter();
            $scope.config.transformationKey = null;
            widgetInstance.saveConfig($scope.config);
        }

        // take assets and run query for them.
        $scope.runQuery = (ids) => {
            const qb = new csQueryBuilder();
            const and = qb.and();
            and.condition('censhare:asset.id', 'IN', ids);
            qb.order().orderBy('censhare:asset.name');
            let builtQuery = qb.build();
            csNavigationManager.openPage('/search', undefined, true, {
                advancedSearch: {
                    'version': 1,
                    'query': builtQuery
                }
            });
        };

        function getValues() {
            if ($scope.config.transformationKey) {
                $scope.isGetValues = true;
                $scope.dataChecked = false;
                // get REST url
                let url = 'rest/service/assets/asset';
                if (assetID) {
                    url = url + '/id/' + assetID;
                } else {
                    // fallback for application pages
                    url = url + ';censhare:resource-key=' + $scope.config.transformationKey;
                }
                url = csApiSession.session.resolveUrl(url + '/transform;key=' + $scope.config.transformationKey + '/json');

                $http.get(url).then((response) => {
                    let data = response.data;

                    widgetInstance.setTitle(data.title ? data.title : 'ioiPieChartWidget.title');
                    if (data.widgetcount) {
                        widgetInstance.setCounter(data.widgetcount);
                    }


                    $scope.chart = data;
                    $scope.legend = data.legend ? data.legend : false;
                    $scope.dataChecked = (data.data && data.data instanceof Array && data.data.length > 0) || data.noData;
                    $scope.isGetValues = false;
                    setContentSize();
                }).catch((err) => {
                    $scope.chart = null;
                    $scope.dataChecked = false;
                    $scope.isGetValues = false;
                });

            } else {
                $scope.chart = null;
            }
        }
        // handle config change
        function handleConfigChange(config) {
            $scope.config = config;
            if ($scope.config.transformationKey) {
                getValues();
            } else {
                removeAssetConfig();
            }
        }

        widgetInstance.getConfig().registerChangeListenerAndFire(handleConfigChange, $scope);
    }
}

class IoiPieChartWidgetConfigController {
    static $name: string = 'ioiPieChartWidgetConfigController';
    static $inject = ['$scope', 'csApiSession', 'config'];

    constructor($scope, csApiSession, config) {
        $scope.config = config;
        $scope.transformationsLoaded = false;

        csApiSession.resourceassets.lookup({
            usage: 'censhare:chart-data-transformation'
        }).then(function (result) {
            let transformations = [];
            result.resourceAssets.forEach(function (resourceAsset) {
                transformations.push({
                    key: resourceAsset.resourceKey,
                    name: resourceAsset.name
                });
            });
            $scope.transformations = transformations;
            $scope.transformationsLoaded = true;
        });
    }
}

m.directive(IoiPieChart)
    .constant(IoiPieChartWidgetHeadless)
    .controller(IoiPieChartWidgetConfigController)
    .controller(IoiPieChartWidgetController);
