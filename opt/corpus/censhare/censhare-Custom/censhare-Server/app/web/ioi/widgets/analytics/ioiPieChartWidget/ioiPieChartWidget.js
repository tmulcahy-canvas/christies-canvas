define(["require", "exports", "censhare", "angular"], function (require, exports, censhare, angular) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var m = censhare.module('ioiPieChartWidget');
    var LOG_PREFIX = 'ioi Pie Chart Widget - ';
    var COLORS = ['#f0645a', '#28d782', '#69bee6', '#41464b'];
    var WIDTH = 370;
    var HEIGHT = 300;
    var MARGINS = { top: 0, right: 10, bottom: 20, left: 10 };
    var ChartOptions = (function () {
        function ChartOptions() {
        }
        return ChartOptions;
    }());
    var ChartOptionsOpts = (function () {
        function ChartOptionsOpts() {
        }
        return ChartOptionsOpts;
    }());
    var IoiPieChart = (function () {
        function IoiPieChart() {
            this.restrict = 'E';
            this.require = 'ngModel';
            this.replace = true;
            this.template = '<svg class="ioi-pie-chart"></svg>';
            this.nv = window.nv;
            this.d3 = window.d3;
            this.link = function (scope, element, attr, ngModel) {
                var options = new ChartOptions();
                var renderTimeout, currentType, currentChart;
                options.width = attr.csWidth ? attr.csWidth : WIDTH;
                options.height = attr.csHeight ? attr.csHeight : HEIGHT;
                options.showLegend = attr.showLegend ? attr.showLegend : false;
                options.labelType = attr.labelType ? attr.labelType : 'value';
                options.showToolTipPercent = attr.showToolTipPercent ? attr.showToolTipPercent : true;
                options.valueFormat = attr.valueformat ? attr.valueformat : undefined;
                options.noData = attr.noData ? attr.noData : 'No data found!';
                options.options = {
                    showLabels: true
                };
                function preparePieDefaultOptions(opts) {
                    angular.extend(opts.options, {
                        showLabels: true,
                        color: COLORS,
                        margin: MARGINS,
                    }, angular.copy(opts.options));
                }
                function createPie(node, data, opts, donut, callback) {
                    nv.addGraph(function () {
                        var chart = nv.models.pieChart()
                            .width(opts.width)
                            .height(opts.height)
                            .x(function (d) {
                            return d.key;
                        })
                            .y(function (d) {
                            return d.value;
                        })
                            .options(opts.options)
                            .color(function (d, idx) {
                            var color = d.color || opts.options.color[idx % opts.options.color.length];
                            return color;
                        })
                            .labelType(opts.labelType)
                            .valueFormat(d3.format(opts.valueFormat))
                            .showTooltipPercent(opts.showToolTipPercent)
                            .donut(donut)
                            .showLegend(false);
                        d3.select(node)
                            .datum(data)
                            .transition().duration(1200)
                            .attr('width', opts.width)
                            .attr('height', opts.height)
                            .call(chart);
                        return chart;
                    }, callback);
                }
                function updateChart(node, data, opts, chart) {
                    d3.select(node)
                        .datum(data)
                        .attr('width', opts.width)
                        .attr('height', opts.height)
                        .call(chart);
                }
                function clearCurrentChart() {
                    if (currentChart) {
                        element.children().remove();
                        nv.charts = {};
                        currentChart = null;
                    }
                }
                function createChartCallback(chart) {
                    currentChart = chart;
                    d3.selectAll('.nv-slice')
                        .on('click', function (elem) {
                        if (elem.data.query.ids !== undefined) {
                            d3.selectAll('.nvtooltip').remove();
                            scope.$parent.$parent.runQuery(elem.data.query.ids);
                        }
                    });
                }
                attr.$observe('type', function (type) {
                    if (type) {
                        deferredRender();
                    }
                });
                attr.$observe('csWidth', function (val) {
                    if (val && val !== options.width) {
                        options.width = val;
                        deferredRender();
                    }
                });
                attr.$observe('csHeight', function (val) {
                    if (val && val !== options.height) {
                        options.height = val;
                        deferredRender();
                    }
                });
                attr.$observe('legend', function (val) {
                    if (angular.isDefined(val) && val !== options.showLegend) {
                        options.showLegend = val;
                        deferredRender();
                    }
                });
                function deferredRender() {
                    if (renderTimeout) {
                        ngModel.$$timeout.cancel(renderTimeout);
                    }
                    renderTimeout = ngModel.$$timeout(ngModel.$render, 200);
                }
                ngModel.$render = function () {
                    var config, data;
                    element.css({
                        width: options.width + 'px',
                        height: options.height + 'px'
                    });
                    if (ngModel.$viewValue) {
                        if (ngModel.$viewValue.hasOwnProperty('data')) {
                            config = ngModel.$viewValue;
                            data = config.data;
                            options.width = config.csWidth || options.width;
                            options.height = config.csHeight || options.height;
                            if (typeof attr.legend === 'undefined') {
                                attr.legend = false;
                            }
                        }
                        else {
                            data = ngModel.$viewValue;
                        }
                    }
                    if (data && !!attr.type) {
                        preparePieDefaultOptions(options);
                        if (currentChart) {
                            updateChart(element[0], data, options, currentChart);
                            return;
                        }
                        createPie(element[0], data, options, false, createChartCallback);
                    }
                    ngModel.$viewChangeListeners.push(ngModel.$render);
                    scope.$on('$destroy', function () {
                        if (renderTimeout) {
                            ngModel.$$timeout.cancel(renderTimeout);
                        }
                        clearCurrentChart();
                        element.empty();
                    });
                };
            };
        }
        return IoiPieChart;
    }());
    IoiPieChart.$name = 'ioiPieChart';
    IoiPieChart.$inject = ['$timeout', 'csNotify'];
    var IoiPieChartWidgetHeadless = (function () {
        function IoiPieChartWidgetHeadless(widgetInstance) {
        }
        return IoiPieChartWidgetHeadless;
    }());
    IoiPieChartWidgetHeadless.$name = 'ioiPieChartWidgetHeadless';
    IoiPieChartWidgetHeadless.$inject = ['widgetInstance'];
    var IoiPieChartWidgetController = (function () {
        function IoiPieChartWidgetController($scope, $http, widgetDataManager, widgetInstance, pageInstance, csApiSession, csNavigationManager, csNotify, csWidgetConfigDialog, csQueryBuilder) {
            var assetID;
            if (pageInstance.getPathContextReader() && pageInstance.getPathContextReader().getValue()) {
                assetID = pageInstance.getPathContextReader().getValue().get('id');
            }
            widgetInstance.getDimensions().registerChangeListenerAndFire(function (newValue) {
                $scope.dimensions = newValue || {};
                setContentSize();
            }, $scope);
            $scope.widgetConfiguration = function () {
                csWidgetConfigDialog(widgetInstance);
            };
            function setContentSize() {
                var subTitleHeight = $scope.chart && $scope.chart.subTitle ? 14 : 0;
                $scope.contentWidth = ($scope.dimensions.width * (270 + 20)) - 40;
                $scope.contentHeight = ($scope.dimensions.height * (125 + 20)) - 50 - subTitleHeight;
            }
            $scope.chart = null;
            $scope.dataChecked = false;
            $scope.isGetValues = false;
            $scope.widgetConfiguration = function () {
                csWidgetConfigDialog(widgetInstance);
            };
            function removeAssetConfig() {
                widgetInstance.setCounter();
                $scope.config.transformationKey = null;
                widgetInstance.saveConfig($scope.config);
            }
            $scope.runQuery = function (ids) {
                var qb = new csQueryBuilder();
                var and = qb.and();
                and.condition('censhare:asset.id', 'IN', ids);
                qb.order().orderBy('censhare:asset.name');
                var builtQuery = qb.build();
                csNavigationManager.openPage('/search', undefined, true, {
                    advancedSearch: {
                        'version': 1,
                        'query': builtQuery
                    }
                });
            };
            function getValues() {
                if ($scope.config.transformationKey) {
                    $scope.isGetValues = true;
                    $scope.dataChecked = false;
                    var url = 'rest/service/assets/asset';
                    if (assetID) {
                        url = url + '/id/' + assetID;
                    }
                    else {
                        url = url + ';censhare:resource-key=' + $scope.config.transformationKey;
                    }
                    url = csApiSession.session.resolveUrl(url + '/transform;key=' + $scope.config.transformationKey + '/json');
                    $http.get(url).then(function (response) {
                        var data = response.data;
                        widgetInstance.setTitle(data.title ? data.title : 'ioiPieChartWidget.title');
                        if (data.widgetcount) {
                            widgetInstance.setCounter(data.widgetcount);
                        }
                        $scope.chart = data;
                        $scope.legend = data.legend ? data.legend : false;
                        $scope.dataChecked = (data.data && data.data instanceof Array && data.data.length > 0) || data.noData;
                        $scope.isGetValues = false;
                        setContentSize();
                    }).catch(function (err) {
                        $scope.chart = null;
                        $scope.dataChecked = false;
                        $scope.isGetValues = false;
                    });
                }
                else {
                    $scope.chart = null;
                }
            }
            function handleConfigChange(config) {
                $scope.config = config;
                if ($scope.config.transformationKey) {
                    getValues();
                }
                else {
                    removeAssetConfig();
                }
            }
            widgetInstance.getConfig().registerChangeListenerAndFire(handleConfigChange, $scope);
        }
        return IoiPieChartWidgetController;
    }());
    IoiPieChartWidgetController.$name = 'ioiPieChartWidgetController';
    IoiPieChartWidgetController.$inject = ['$scope', '$http', 'widgetDataManager', 'widgetInstance', 'pageInstance', 'csApiSession', 'csNavigationManager', 'csNotify', 'csWidgetConfigDialog', 'csQueryBuilder'];
    var IoiPieChartWidgetConfigController = (function () {
        function IoiPieChartWidgetConfigController($scope, csApiSession, config) {
            $scope.config = config;
            $scope.transformationsLoaded = false;
            csApiSession.resourceassets.lookup({
                usage: 'censhare:chart-data-transformation'
            }).then(function (result) {
                var transformations = [];
                result.resourceAssets.forEach(function (resourceAsset) {
                    transformations.push({
                        key: resourceAsset.resourceKey,
                        name: resourceAsset.name
                    });
                });
                $scope.transformations = transformations;
                $scope.transformationsLoaded = true;
            });
        }
        return IoiPieChartWidgetConfigController;
    }());
    IoiPieChartWidgetConfigController.$name = 'ioiPieChartWidgetConfigController';
    IoiPieChartWidgetConfigController.$inject = ['$scope', 'csApiSession', 'config'];
    m.directive(IoiPieChart)
        .constant(IoiPieChartWidgetHeadless)
        .controller(IoiPieChartWidgetConfigController)
        .controller(IoiPieChartWidgetController);
});
//# sourceMappingURL=ioiPieChartWidget.js.map