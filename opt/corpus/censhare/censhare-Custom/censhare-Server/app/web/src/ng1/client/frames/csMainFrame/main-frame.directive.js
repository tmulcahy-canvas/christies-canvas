define(["require", "exports", "tslib", "@cs/client/base/csActions/csActionFactory", "@cs/client/frames/csMainFrame/csMainFrame", "./main-frame-embedded.directive.html!text", "./main-frame.directive.html!text"], function (require, exports, tslib_1, csActionFactory_1, csMainFrame_1, main_frame_embedded_directive_html_text_1, main_frame_directive_html_text_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var LEFT_NAV_OPENED_KEY = 'com_censhare_LeftNav_is_opened';
    var CREATE_ASSET_DIALOG_CONTEXT = {
        title: 'csCommonTranslations.newAsset',
        defaultTemplate: 'censhare:metadata.default.asset.template',
        tabs: [],
        defaultTab: 0
    };
    var $inject = [
        '$scope', 'csLogger', 'csWorkspaceManager', 'csTypeRegistry', '$location', '$rootScope', 'csConfirmDialog',
        'csAboutDialog', 'csLocalInjector', 'csApiSession', 'csNavigationManagerHolder',
        'csTranslateFilter', 'csUserPreferencesDialog', 'csUserPasswordDialog', 'csKeyboardShortcutsDialog', '$window', 'csBehaviorCollector',
        'csLocalInjectorFactory', 'csAssetCreateDialog', 'csGlobalDialogManagerService', 'csUserPreferencesManager', 'hotkeys', '$cookies', 'csUploadsManager', 'csTranslate'
    ];
    var controller = $inject.concat([function ($scope, csLogger, csWorkspaceManager, csTypeRegistry, $location, $rootScope, csConfirmDialog, csAboutDialog, csLocalInjector, csApiSession, csNavigationManagerHolder, csTranslateFilter, csUserPreferencesDialog, csUserPasswordDialog, csKeyboardShortcutsDialog, $window, csBehaviorCollector, csLocalInjectorFactory, csAssetCreateDialog, csGlobalDialogManagerService, csUserPreferencesManager, hotkeys, $cookies, csUploadsManager, csTranslate) {
            var logger = csLogger.get('csFrameMain');
            var workspaceManager = new csWorkspaceManager({});
            var workspacePromise = workspaceManager.initialize().then(function () {
                return workspaceManager;
            });
            var globalDialogManagerPromise = csGlobalDialogManagerService.getGlobalDialogManager();
            var actions = csActionFactory_1.actionFactory.group();
            var leftActions = csActionFactory_1.actionFactory.group();
            var middleActions = csActionFactory_1.actionFactory.group();
            var userMenu = csActionFactory_1.actionFactory.group().setDropDown(true)
                .setTitle('csCommonTranslations.user')
                .setIcon('cs-icon-user-alt')
                .setViewFactory(new csMainFrame_1.UserViewFactory());
            var rightActions = csActionFactory_1.actionFactory.group();
            var navBarToggleAction = csActionFactory_1.actionFactory.toggleAction(openCloseNavbar)
                .setTitle('csCommonTranslations.showHideLeftNavigation')
                .setIcons(['cs-icon-navpanel-open csHeaderNav__ToggleNavBarIcon', 'cs-icon-navpanel-closed csHeaderNav__ToggleNavBarIcon']);
            var body = document.querySelector('body');
            var pagePathWatcher;
            csUserPreferencesManager.registerChangeListener(function (userPreferences) {
                navBarToggleAction.setActiveValue(userPreferences[LEFT_NAV_OPENED_KEY] !== 'false');
            });
            actions
                .addChildAfter(navBarToggleAction)
                .addChildAfter(leftActions)
                .addChildAfter(middleActions)
                .addChildAfter(rightActions);
            middleActions.addChildAfter(csActionFactory_1.actionFactory.group());
            rightActions.addChildAfter(userMenu);
            fillUserMenu();
            fillGlobalBehaviors();
            var navigationActionGroup = csActionFactory_1.actionFactory.group()
                .addChildAfter(csActionFactory_1.actionFactory.action(function () {
                $window.history.back();
            }).setTitle('csCommonTranslations.back').setIcon('cs-icon-back-alt'))
                .addChildAfter(csActionFactory_1.actionFactory.action(function () {
                $window.history.forward();
            }).setTitle('csCommonTranslations.forward').setIcon('cs-icon-forward-alt'));
            leftActions.addChildAfter(navigationActionGroup, navigationActionGroup);
            csApiSession.permission.checkAssetIndependentPermission('asset_checkin_new').then(function (value) {
                var createAssetPermmission = value && !!value.permission;
                if (createAssetPermmission) {
                    addUploadAndNewFromTemplate();
                }
            });
            var globalActionsDropDown = csActionFactory_1.actionFactory.group().setDropDown(true)
                .setTitle('csCommonTranslations.globalActions')
                .showDropDownIndicator();
            var createActionGroup = csActionFactory_1.actionFactory.group().addChildAfter(globalActionsDropDown);
            leftActions.addChildAfter(createActionGroup);
            csApiSession.session.getUserAssetReader().registerChangeListenerAndFire(function (userAsset) {
                if (userAsset && userAsset.thumbnail && userAsset.thumbnail.url && userAsset.thumbnail.url.indexOf('/icon/') === -1) {
                    userMenu
                        .setIcon(userAsset.thumbnail.url)
                        .setTitle(userAsset.traits.display.name);
                }
                else {
                    userMenu
                        .setIcon('cs-icon-user-alt')
                        .setTitle(userAsset ? userAsset.traits.display.name : 'csCommonTranslations.user');
                }
            }, $scope);
            workspacePromise.then(function (_workspaceManager) {
                var api = {
                    actionGroup: actions,
                    navigationManager: _workspaceManager.getNavigationManager(),
                    pageManager: _workspaceManager.getPageManager(),
                    workspaceManager: _workspaceManager.forPublic
                };
                var navBarApi = {
                    csNavigationAPI: {
                        getNavi: _workspaceManager.getNavi
                    }
                };
                var headerBarApi = {
                    csHeaderAPI: {
                        getActions: function () {
                            return actions;
                        }
                    }
                };
                var loadComponentModule = function (module, component, locals) {
                    return {
                        component: [module, component],
                        locals: tslib_1.__assign({}, api, locals, { csPageManager: workspaceManager })
                    };
                };
                csLocalInjector.add('csWorkspaceManager', _workspaceManager);
                csLocalInjector.add('csNavigationManager', _workspaceManager.getNavigationManager());
                csNavigationManagerHolder.setNavigationManager(_workspaceManager.getNavigationManager());
                logger.fine('csMainFrameController pagePromise resolved');
                $scope.navBar = loadComponentModule('csNavBar', 'csNavBarContainer', navBarApi);
                $scope.header = loadComponentModule('csHeaderNav', 'csHeaderNav', headerBarApi);
                $scope.pinboard = loadComponentModule('csPinboard', 'csPinboard', {});
                workspaceManager.getVisibleInstanceReader().registerChangeListenerAndFire(function pageInstance(visiblePageInstance) {
                    if (visiblePageInstance) {
                        $scope.pageInstance = visiblePageInstance;
                        visiblePageInstance.getTitleReader().registerChangeListenerAndFire(function () {
                            var title = csTranslateFilter(visiblePageInstance.getTitleReader().getValue());
                            $rootScope.pageTitle = title ? title + ' | censhare Web' : 'censhare Web';
                        }, $scope);
                    }
                }, $scope);
                angular.forEach(csTypeRegistry.getType('csClientPlugIn').getImplementations(), function (impl) {
                    csTypeRegistry.getImpl(impl.getName(), 'csClientPlugIn').instantiate(api);
                });
                $scope.ready = true;
            });
            $scope.pagePath = null;
            function routeUpdate() {
                var path = $location.path();
                var query = $location.search();
                workspacePromise.then(function (_workspaceManager) {
                    $scope.pagePath = path;
                    _workspaceManager.pathChanged(path, query);
                });
            }
            routeUpdate();
            $scope.$on('$routeUpdate', routeUpdate);
            pagePathWatcher = $scope.$watch('pagePath', function (n, o) {
                if (n !== o) {
                    $scope.$broadcast('$routeChangeStart');
                }
            });
            $scope.$on('$destroy', function () {
                workspacePromise.then(function (_workspaceManager) {
                    _workspaceManager.destroy();
                });
                pagePathWatcher();
            });
            $scope.$root.$on('csNavBarHide', function (_e, args) {
                if (args.close) {
                    body.classList.add('cs-nav-hidden');
                }
                else {
                    body.classList.remove('cs-nav-hidden');
                }
            });
            function toggleBodyClassesForNavBar(isNavBarVisible) {
                if (isNavBarVisible) {
                    body.classList.remove('cs-nav-toggled');
                    body.classList.add('cs-nav-default');
                }
                else {
                    body.classList.remove('cs-nav-default');
                    body.classList.add('cs-nav-toggled');
                }
            }
            function openCloseNavbar(open) {
                var userPreferences = csUserPreferencesManager.getUserPreferences();
                userPreferences[LEFT_NAV_OPENED_KEY] = open ? 'true' : 'false';
                csUserPreferencesManager.setUserPreferences(userPreferences);
                toggleBodyClassesForNavBar(open);
                $scope.$root.$broadcast('csNavBarToggle');
            }
            hotkeys.add('shift+alt+n', openAssetCreateDialogShortcut);
            function openAssetCreateDialogShortcut(event) {
                event.preventDefault();
                openAssetCreateDialog();
            }
            function addUploadAndNewFromTemplate() {
                createActionGroup.addChildAfter(csActionFactory_1.actionFactory.customCallbackAction(function (action, event) {
                    action.enable();
                    event.currentTarget.blur();
                    openAssetCreateDialog();
                }, new csMainFrame_1.HeaderButtonViewFactory())
                    .setTitle('csCommonTranslations.newAsset')
                    .enableMoreInput(), -1);
            }
            function openAssetCreateDialog() {
                getCreateAssetDialogContext()
                    .then(function (context) {
                    return csAssetCreateDialog.open(globalDialogManagerPromise, context);
                })
                    .then(function (result) {
                    if (result && result.promise) {
                        result.promise.then(function (asset) {
                            if (asset && asset.editPage) {
                                workspaceManager.getNavigationManager().openPage(asset.editPage);
                            }
                        });
                    }
                });
            }
            function getCreateAssetDialogContext() {
                return csApiSession.execute('com.censhare.api.dam.dialog.create.asset.config', {})
                    .then(function (_a) {
                    var list = _a.tabs;
                    var tabs = list.map(function (tab) {
                        return {
                            key: tab
                        };
                    });
                    return Object.assign({}, CREATE_ASSET_DIALOG_CONTEXT, { tabs: tabs });
                });
            }
            function waitForWorkspaceToLoad(f) {
                return function (a) { return workspacePromise.then(function () {
                    f(a);
                }); };
            }
            function workspaceCallbackAction(f) {
                return csActionFactory_1.actionFactory.action(waitForWorkspaceToLoad(f));
            }
            function fillUserMenu() {
                hotkeys.add('shift+mod+p', openPreferencesDialog);
                hotkeys.add('shift+ctrl+p', openPreferencesDialog);
                hotkeys.add('shift+mod+k', openKeyboardShortcutsDialog);
                hotkeys.add('shift+ctrl+k', openKeyboardShortcutsDialog);
                hotkeys.add('shift+mod+a', openAboutDialog);
                hotkeys.add('shift+ctrl+a', openAboutDialog);
                function openPreferencesDialog(event) {
                    event.preventDefault();
                    csUserPreferencesDialog(globalDialogManagerPromise);
                }
                function openKeyboardShortcutsDialog(event) {
                    event.preventDefault();
                    csKeyboardShortcutsDialog.open(globalDialogManagerPromise);
                }
                function openAboutDialog(event) {
                    event.preventDefault();
                    csApiSession.execute('com.censhare.api.dialogs.aboutdialog', {}).then(function (result) {
                        if (result) {
                            csAboutDialog.open(globalDialogManagerPromise, result);
                        }
                    });
                }
                userMenu.addChildAfter(workspaceCallbackAction(function () {
                    var userID = csApiSession.session.getUserAssetReader().getValue().traits.ids.id;
                    if (userID) {
                        workspaceManager.getNavigationManager().openPage('/assetPerson/' + userID);
                    }
                    else {
                        logger.error('Can not show user profile. Can not get the user ID.');
                    }
                }).setTitle('csCommonTranslations.showUserAsset'));
                userMenu.addChildAfter(workspaceCallbackAction(function () { return csUserPreferencesDialog(globalDialogManagerPromise); })
                    .setTitle('csCommonTranslations.preferences').enableMoreInput());
                userMenu.addChildAfter(workspaceCallbackAction(function () {
                    csConfirmDialog(globalDialogManagerPromise, 'csCommonTranslations.resetPreferences', 'csCommonTranslations.resetPreferencesMessage', 'csCommonTranslations.resetPreferencesSubMessage').then(csApiSession.workspace.resetUserPreferences).then(function (defaultVal) {
                        if (defaultVal) {
                            csUserPreferencesManager.setUserPreferences(defaultVal, false);
                        }
                    });
                }).setTitle('csCommonTranslations.resetPreferences').enableMoreInput());
                userMenu.addChildAfter(workspaceCallbackAction(function () {
                    csConfirmDialog(globalDialogManagerPromise, 'csCommonTranslations.resetWorkspace', 'csCommonTranslations.resetWorkspaceMessage', 'csCommonTranslations.resetWorkspaceSubMessage').then(function () { return workspaceManager.reset(); });
                }).setTitle('csCommonTranslations.resetWorkspace').enableMoreInput());
// hiding Change password on user dropdown menu on dashboard top-right                
//                userMenu.addChildAfter(csActionFactory_1.actionFactory.separator());
//                userMenu.addChildAfter(workspaceCallbackAction(function () { return csUserPasswordDialog.open(globalDialogManagerPromise); })
//                    .setTitle('csCommonTranslations.changePassword').enableMoreInput());
                userMenu.addChildAfter(csActionFactory_1.actionFactory.separator());
                userMenu.addChildAfter(workspaceCallbackAction(function () { return csKeyboardShortcutsDialog.open(globalDialogManagerPromise); })
                    .setTitle('csCommonTranslations.keyboardShortcuts').enableMoreInput());
                userMenu.addChildAfter(workspaceCallbackAction(function () {
                    csApiSession.execute('com.censhare.api.dialogs.aboutdialog', {}).then(function (result) {
                        if (result) {
                            csAboutDialog.open(globalDialogManagerPromise, result);
                        }
                    });
                }).setTitle('csCommonTranslations.aboutCenshareWeb').enableMoreInput());
                csApiSession.permission.checkAssetPermission('all').then(function (value) {
                    var pos;
                    var myArray = [];
                    if (value && !!value.permission) {
                        myArray = userMenu.getChildren();
                        for (pos = 0; pos < myArray.length; pos++) {
                            if (myArray[pos].getTitleKey() === 'csCommonTranslations.aboutCenshareWeb') {
                                break;
                            }
                        }
                        userMenu.addChildAfter(workspaceCallbackAction(function () {
                            var query = {
                                condition: { name: 'censhare:resource-key', op: '=', value: 'censhare:system' }
                            };
                            csApiSession.asset.query(query).then(function (data) {
                                if (data && data.container) {
                                    var systemId = data.container[0].asset.traits.ids.id;
                                    if (systemId) {
                                        workspaceManager.getNavigationManager().openPage('/assetModuleSystem/' + systemId);
                                    }
                                    else {
                                        logger.error('Can not open system. Can not get the system asset ID.');
                                    }
                                }
                            });
                        }).setTitle('csCommonTranslations.showSystemAsset'), pos);
                    }
                });
                userMenu.addChildAfter(csActionFactory_1.actionFactory.separator());
                userMenu.addChildAfter(workspaceCallbackAction(function () {
                    var uploadsInProgress = csUploadsManager.getInProgressUploads();
                    if (uploadsInProgress) {
                        csUploadsManager.pauseUploads();
                        var msg = ((uploadsInProgress > 1) ?
                            csTranslate.instant('csUploadsManager.uploadInProgressFiles', { countInProgress: uploadsInProgress }) :
                            csTranslate.instant('csUploadsManager.uploadInProgressFile')) + ' ' +
                            csTranslate.instant('csUploadsManager.chancelUploadMessage');
                        return csConfirmDialog(globalDialogManagerPromise, 'csUploadsManager.uploadInProgress', msg).then(function () {
                            executeLogout();
                        }, function () {
                            csUploadsManager.resumeUploads();
                            return;
                        });
                    }
                    else {
                        executeLogout();
                    }
                    function executeLogout() {
                        if ($cookies.get('CENSHARE_SAML_LOGOUT') === '1') {
                            csApiSession.execute('com.censhare.api.logout.GetSAMLLogout', {}).then(function (result) {
                                return result.URL;
                            }).then(function (logoutUrl) {
                                csApiSession.session.logout().then(function () {
                                    $cookies.remove('CENSHARE_USER_LOGIN', { path: '/' });
                                    $cookies.remove('CENSHARE_SAML_LOGOUT', { path: '/' });
                                    $cookies.remove('CENSHARE_SAML_STANDARD_LOGIN', { path: '/' });
                                    $cookies.remove('JSESSIONID', { path: '/' });
                                    $window.location.reload(false);
                                    $window.location.href = logoutUrl;
                                });
                            });
                        }
                        else if ($cookies.get('CENSHARE_KERBEROS_LOGOUT') === '1') {
                            csApiSession.execute('com.censhare.api.logout.GetKerberosLogout', {}).then(function (result) {
                                return result.URL;
                            }).then(function (logoutUrl) {
                                csApiSession.session.logout().then(function () {
                                    $cookies.remove('CENSHARE_USER_LOGIN', { path: '/' });
                                    $cookies.remove('CENSHARE_KERBEROS_LOGOUT', { path: '/' });
                                    $cookies.remove('JSESSIONID', { path: '/' });
                                    $window.location.reload(false);
                                    $window.location.href = logoutUrl;
                                });
                            });
                        }
                        else {
                            csApiSession.session.logout().then(function () {
                                if ($cookies.get('CENSHARE_SAML_STANDARD_LOGIN') === '1') {
                                    $cookies.remove('CENSHARE_SAML_STANDARD_LOGIN', { path: '/' });
                                    $location.url('/?auth=standard');
                                }
                                else {
                                    $location.url('/');
                                }
                                $window.location.reload(false);
                            });
                        }
                    }
                }).setTitle('csCommonTranslations.logout'));
            }
            function fillGlobalBehaviors() {
                csApiSession.execute('com.censhare.api.behaviors.GlobalBehaviors', 'execute', {}).then(function (result) {
                    var globalBehaviors = [];
                    var behaviorScope;
                    angular.forEach(result.behaviors, function (action) {
                        if (action && action.hasOwnProperty('name')) {
                            globalBehaviors.push(action);
                        }
                    });
                    behaviorScope = $scope.$new();
                    csLocalInjectorFactory.getOrCreate(behaviorScope).add('navigationManager', workspaceManager.getNavigationManager());
                    csBehaviorCollector.convertToActions(globalBehaviors, {}, behaviorScope).then(function (_actions) {
                        angular.forEach(_actions, function (action) {
                            if (action.actionAPI.type === 'divider') {
                                globalActionsDropDown.addChildAfter(csActionFactory_1.actionFactory.separator());
                            }
                            else {
                                var menuAction = csActionFactory_1.actionFactory.action(action.actionAPI.callback)
                                    .setTitle(action.actionAPI.title)
                                    .setIcon(action.actionAPI.icon);
                                globalActionsDropDown.addChildAfter(menuAction);
                                if (action.actionAPI.moreInput) {
                                    menuAction.enableMoreInput();
                                }
                            }
                        });
                    });
                });
            }
        }]);
    exports.csMainFrameDirective = function () { return ({
        controller: controller, template: main_frame_directive_html_text_1.default, restrict: 'E'
    }); };
    exports.csMainFrameEmbeddedDirective = function () { return ({
        controller: controller, template: main_frame_embedded_directive_html_text_1.default, restrict: 'E'
    }); };
});
//# sourceMappingURL=main-frame.directive.js.map 