define(["require", "exports", "./page-border-head.component.html!text", "moment"], function (require, exports, page_border_head_component_html_text_1, moment) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PageHeadComponent = (function () {
        function PageHeadComponent($scope, pageInstance, pageActionsRegistry, csApiSession) {
            var _this = this;
            this.$scope = $scope;
            this.pageInstance = pageInstance;
            this.pageActionsRegistry = pageActionsRegistry;
            this.csApiSession = csApiSession;
            this.checkoutUserListener = function (userName) {
                _this.checkoutUser = {
                    key: 'csCommonTranslations.checkoutOther',
                    params: { userName: userName }
                };
                _this.digest();
            };
            this.titleListener = function (title) {
                _this.title = title;
                _this.digest();
            };
            this.iconListener = function (icon) {
                _this.icon = icon;
                _this.digest();
            };
            this.iconsInfoListener = function (iconsInfo) {
                _this.iconsInfo = iconsInfo;
                _this.digest();
            };
            this.subtitleListener = function (subtitle) {
                _this.subtitle = subtitle;
                _this.digest();
            };
            this.notificationListener = function (notification) {
                _this.notification = notification;
                _this.digest();
            };
            this.$scope.parentName = undefined;
            this.$scope.propertyDetails = undefined;
            this.$scope.lotNumberAndSuffix = undefined;
            this.$scope.data = undefined;
            this.digest = function () {
                if (!_this.$scope.$root.$$phase) {
                    _this.$scope.$digest();
                }
            };
        }
        PageHeadComponent.prototype.$onInit = function () {
            var _this = this;
            this.cssModule = this.pageInstance.getBluePrint().getKind().getModuleName();
            this.assetId = this.pageInstance.getPathContextReader().getValue().get('id');
            if (this.assetId !== undefined) {
                this.dragIface = {
                    onDragStart: function (event) {
                        var assetRef = 'asset/id/' + _this.assetId + '/currversion/0';
                        event.dataTransfer.setData('application/x-censhare-assets', assetRef);
                        return {
                            draggedData: [assetRef],
                            dragIcon: 'file'
                        };
                    }
                };
                this.loadAssetDetails(this.assetId);
            }
            this.pageInstance.getCheckoutUser().registerNativeChangeListenerAndFire(this.checkoutUserListener);
            this.pageInstance.getTitle().registerNativeChangeListenerAndFire(this.titleListener);
            this.pageInstance.getIcon().registerNativeChangeListenerAndFire(this.iconListener);
            this.pageInstance.getIconsInfo().registerNativeChangeListenerAndFire(this.iconsInfoListener);
            this.pageInstance.getSubtitle().registerNativeChangeListenerAndFire(this.subtitleListener);
            this.pageInstance.getNotification().registerNativeChangeListenerAndFire(this.notificationListener);
        };
        PageHeadComponent.prototype.$onDestroy = function () {
            this.pageInstance.getCheckoutUser().unregisterNativeChangeListener(this.checkoutUserListener);
            this.pageInstance.getTitle().unregisterNativeChangeListener(this.titleListener);
            this.pageInstance.getIcon().unregisterNativeChangeListener(this.iconListener);
            this.pageInstance.getIconsInfo().unregisterNativeChangeListener(this.iconsInfoListener);
            this.pageInstance.getSubtitle().unregisterNativeChangeListener(this.subtitleListener);
            this.pageInstance.getNotification().unregisterNativeChangeListener(this.notificationListener);
        };
        PageHeadComponent.prototype.loadAssetDetails = function (assetId) {
            var _this = this;
            var params = {};
            if (assetId && assetId.length > 0) {
                this.csApiSession.asset.get(assetId, params).then(function (result) {
                    _this.$scope.data = result.container[0].asset;
                    if (_this.$scope.data) {
                        if (_this.$scope.data.traits.type.type == 'sale.') {
                            _this.$scope.saleDate = moment(_this.$scope.data.traits.jdesale.saledate.value).format("D MMMM YYYY");
                        }
                        _this.$scope.lotNumberAndSuffix = result.container[0].asset.traits.property.lotnumberandsuffix.value;
                        _this.$scope.propertyDetails = _this.propertyDetailsToDisplay(_this.$scope.data);
                    }
                });
            }
        };
        PageHeadComponent.prototype.parentSaleAssetDetails = function (assetId) {
            var _this = this;
            this.csApiSession.asset.get(Number(assetId)).then(function (result) {
                if (result.container[0].asset.traits.type.type == 'sale.') {
                    _this.$scope.parentData = result.container[0].asset;
                    _this.$scope.saleDate = moment(_this.$scope.parentData.traits.jdesale.saledate.value).format("D MMMM YYYY");
                    _this.$scope.parentName = _this.$scope.parentData.traits.display.name;
                }
                else {
                }
            });
        };
        PageHeadComponent.prototype.propertyDetailsToDisplay = function (assetInfo) {
            var propToPush = [];
            this.$scope.propDetails = [];
            if (assetInfo.relations != undefined) {
                var rels = assetInfo.relations;
                for (var i = rels.length - 1; i >= 0; i--) {
                    if (rels[i].direction == 'parent' && rels[i].ref_type == 'asset_rel_typedef/key/target.') {
                        var refAsset = rels[i].ref_asset.toString();
                        var parentId = refAsset.match(/[0-9]{1,10}/);
                        this.parentSaleAssetDetails(parentId[0]);
                    }
                }
            }
            if (assetInfo.traits.type.type == 'product.') {
                var makerDate = assetInfo.traits.product && assetInfo.traits.product.makerDate && assetInfo.traits.product.makerDate.values ? assetInfo.traits.product.makerDate.values[0].value : undefined;
                var artistDate = assetInfo.traits.canvas && assetInfo.traits.canvas.artistDate && assetInfo.traits.canvas.artistDate.values ? assetInfo.traits.canvas.artistDate.values[0].value : undefined;
                var firstLine = assetInfo.traits.product && assetInfo.traits.product.firstline && assetInfo.traits.product.firstline.values ? assetInfo.traits.product.firstline.values[0].value : undefined;
                var title = assetInfo.traits.product && assetInfo.traits.product.title && assetInfo.traits.product.title.values ? assetInfo.traits.product.title.values[0].value : undefined;
                var details = assetInfo.traits.product && assetInfo.traits.product.details && assetInfo.traits.product.details.values ? assetInfo.traits.product.details.values[0].value : undefined;
                if (firstLine != undefined) {
                    propToPush.push(this.valuesToPush(assetInfo.traits.product.firstline.values));
                    propToPush.push('Property');
                    if (makerDate != undefined) {
                        propToPush.push(this.valuesToPush(assetInfo.traits.product.makerDate.values));
                    }
                }
                else if (artistDate != undefined) {
                    propToPush.push('Property');
                    propToPush.push(this.valuesToPush(assetInfo.traits.canvas.artistDate.values));
                    if (title != undefined) {
                        propToPush.push(this.valuesToPush(assetInfo.traits.product.title.values));
                    }
                }
                else if (title != undefined) {
                    propToPush.push('Property');
                    propToPush.push(this.valuesToPush(assetInfo.traits.product.title.values));
                }
                else if (details != undefined) {
                    propToPush.push('Property');
                    propToPush.push(this.valuesToPush(assetInfo.traits.product.details.values));
                }
                else if (firstLine == undefined && artistDate == undefined && title == undefined && details == undefined) {
                    propToPush.push('Property');
                }
                else {
                }
            }
            for (var i = 0; i < propToPush.length; i++) {
                if (propToPush[i].values != undefined) {
                    this.$scope.propDetails.push(propToPush[i].values[0]);
                }
                else {
                    this.$scope.propDetails.push(propToPush[i]);
                }
            }
            return this.$scope.propDetails;
        };
        PageHeadComponent.prototype.valuesToPush = function (value) {
            var prop = {
                'values': []
            };
            for (var i = 0; i < value.length; i++) {
                if (value[i].language) {
                    if (value[i].language == "en") {
                        prop.values.push(value[i].value);
                        break;
                    }
                }
                else {
                    prop.values.push(value[i].value);
                    break;
                }
            }
            return prop;
        };
        PageHeadComponent.$inject = ['$scope', 'pageInstance', 'pageActionsRegistry', 'csApiSession', 'csServices'];
        return PageHeadComponent;
    }());
    exports.pageHeadComponent = {
        controller: PageHeadComponent,
        template: page_border_head_component_html_text_1.default
    };
});
//# sourceMappingURL=page-border-head.component.js.map 