import {IPageIconInfo, IPageInstance} from '../csWorkspaceManager/csPageRegistry/page-registry.types';
import {IDragIface, IDragIfaceResult} from '@cs/client/DAM/shared/directive/cs-dnd/dnd.types';
import template from './page-border-head.component.html!text';
import {IcsApiSession} from "@cs/framework/csApi/cs-api.model";
import moment = require("moment");

/**
 * Default header controller e.g. for asset editor pages.
 * Used if no custom header controller is defined.
 */
class PageHeadComponent implements ng.IComponentController {
    static $inject = ['$scope', 'pageInstance', 'pageActionsRegistry', 'csApiSession', 'csServices'];

    cssModule: string;
    assetId: string;
    dragIface: IDragIface;

    checkoutUser: any;
    title: string;
    icon: string;
    iconsInfo: IPageIconInfo;
    subtitle: string;
    notification: { locking?: string; count?: number };

    private digest: () => void;


    constructor(private $scope: ng.IScope, private pageInstance: IPageInstance, public pageActionsRegistry: any, private csApiSession: IcsApiSession) {
        this.$scope.parentName = undefined;
        this.$scope.propertyDetails = undefined;
        this.$scope.lotNumberAndSuffix = undefined;
        this.$scope.data = undefined;
        this.digest = () => {
            if (!this.$scope.$root.$$phase) {
                this.$scope.$digest();
            }
        };
    }

    $onInit() {
        this.cssModule = this.pageInstance.getBluePrint().getKind().getModuleName();
        this.assetId = this.pageInstance.getPathContextReader().getValue().get('id');

        if (this.assetId !== undefined) {
            this.dragIface = {
                onDragStart: (event: DragEvent) => {
                    const assetRef = 'asset/id/' + this.assetId + '/currversion/0';
                    event.dataTransfer.setData('application/x-censhare-assets', assetRef);

                    return <IDragIfaceResult>{
                        draggedData: [assetRef],
                        dragIcon: 'file'
                    };
                }
            };
            this.loadAssetDetails(this.assetId);
        }

        this.pageInstance.getCheckoutUser().registerNativeChangeListenerAndFire(this.checkoutUserListener);
        this.pageInstance.getTitle().registerNativeChangeListenerAndFire(this.titleListener);
        this.pageInstance.getIcon().registerNativeChangeListenerAndFire(this.iconListener);
        this.pageInstance.getIconsInfo().registerNativeChangeListenerAndFire(this.iconsInfoListener);
        this.pageInstance.getSubtitle().registerNativeChangeListenerAndFire(this.subtitleListener);
        this.pageInstance.getNotification().registerNativeChangeListenerAndFire(this.notificationListener);
    }

    $onDestroy() {
        this.pageInstance.getCheckoutUser().unregisterNativeChangeListener(this.checkoutUserListener);
        this.pageInstance.getTitle().unregisterNativeChangeListener(this.titleListener);
        this.pageInstance.getIcon().unregisterNativeChangeListener(this.iconListener);
        this.pageInstance.getIconsInfo().unregisterNativeChangeListener(this.iconsInfoListener);
        this.pageInstance.getSubtitle().unregisterNativeChangeListener(this.subtitleListener);
        this.pageInstance.getNotification().unregisterNativeChangeListener(this.notificationListener);
    }

    private checkoutUserListener = (userName: string) => {
        this.checkoutUser = {
            key: 'csCommonTranslations.checkoutOther',
            params: { userName }
        };
        this.digest();
    }

    private titleListener = (title: string) => {
        this.title = title;
        this.digest();
    }

    private iconListener = (icon: string) => {
        this.icon = icon;
        this.digest();
    }

    private iconsInfoListener = (iconsInfo: IPageIconInfo) => {
        this.iconsInfo = iconsInfo;
        this.digest();
    }

    private subtitleListener = (subtitle: string) => {
        this.subtitle = subtitle;
        this.digest();
    }

    private notificationListener = (notification: { locking?: string; count?: number }) => {
        this.notification = notification;
        this.digest();
    }

    public loadAssetDetails(assetId)  {
        // console.log(`Asset Id - ${assetId}`);
        let params = {};
        if (assetId && assetId.length > 0) {
            this.csApiSession.asset.get(assetId, params).then( (result: any) => {
                // console.log("RESULT");
                // console.log(result.container[0]);
                this.$scope.data = result.container[0].asset;
                // console.log('DATA');
                // console.log(this.$scope.data);
                if (this.$scope.data) {
                    if (this.$scope.data.traits.type.type == 'sale.') {
                        this.$scope.saleDate = moment(this.$scope.data.traits.jdesale.saledate.value).format("D MMMM YYYY");
                    }
                    // console.log(this.$scope.saleDate);
                    this.$scope.lotNumberAndSuffix = result.container[0].asset.traits.property.lotnumberandsuffix.value;
                    // console.log("LOT");
                    // console.log(this.$scope.lotNumberAndSuffix);
                    this.$scope.propertyDetails = this.propertyDetailsToDisplay(this.$scope.data);
                    // console.log("PROPERTY DETAILS INSIDE LOAD ASSET DETAILS");
                    // console.log(this.$scope.propertyDetails);
                }
            });
        }
    }

    private parentSaleAssetDetails(assetId){
        this.csApiSession.asset.get(Number(assetId)).then((result) => {
            // console.log("PARENT ASSET RESULT");
            // console.log(result);
            // console.log(result.container[0].asset.traits.type.type);
            // values from parent sale
            if (result.container[0].asset.traits.type.type == 'sale.') {
                this.$scope.parentData = result.container[0].asset;
                // console.log("PARENT DATA");
                this.$scope.saleDate = moment(this.$scope.parentData.traits.jdesale.saledate.value).format("D MMMM YYYY");
                // console.log(this.$scope.parentData);
                this.$scope.parentName = this.$scope.parentData.traits.display.name;
                // console.log(`Parent Asset Name - ${this.$scope.parentName}`);
            } else {

            }
        });
    }

    private propertyDetailsToDisplay(assetInfo) {

        let propToPush = [];
        this.$scope.propDetails = [];
        // console.log("ASSET INFO");
        // console.log(assetInfo);
        if (assetInfo.relations != undefined) {
            let rels = assetInfo.relations;

            for (let i = rels.length - 1; i >= 0; i--) {
                if (rels[i].direction == 'parent'&& rels[i].ref_type == 'asset_rel_typedef/key/target.') {
                    let refAsset = rels[i].ref_asset.toString();
                    let parentId = refAsset.match(/[0-9]{1,10}/);
                    this.parentSaleAssetDetails(parentId[0]);
                }
            }
        }


        if (assetInfo.traits.type.type == 'product.') {
            // console.log('THIS IS A PRODUCT ASSET');
            // console.log(assetInfo);
            let makerDate = assetInfo.traits.product && assetInfo.traits.product.makerDate && assetInfo.traits.product.makerDate.values ? assetInfo.traits.product.makerDate.values[0].value : undefined;
            let artistDate = assetInfo.traits.canvas && assetInfo.traits.canvas.artistDate && assetInfo.traits.canvas.artistDate.values ? assetInfo.traits.canvas.artistDate.values[0].value : undefined;
            // console.log('NO ARTIST DATE');
            // console.log(artistDate);
            let firstLine = assetInfo.traits.product && assetInfo.traits.product.firstline && assetInfo.traits.product.firstline.values ? assetInfo.traits.product.firstline.values[0].value : undefined;
            // console.log('NO AFIRST LINE');
            // console.log(firstLine);
            let title = assetInfo.traits.product && assetInfo.traits.product.title && assetInfo.traits.product.title.values ? assetInfo.traits.product.title.values[0].value : undefined;
            // console.log('NO TITLE');
            // console.log(title);
            let details = assetInfo.traits.product && assetInfo.traits.product.details && assetInfo.traits.product.details.values ? assetInfo.traits.product.details.values[0].value : undefined;
            // console.log('NO DETAILS');
            // console.log(details);

            if(firstLine != undefined){
                // console.log('test1');
                propToPush.push(this.valuesToPush(assetInfo.traits.product.firstline.values));
                propToPush.push('Property');
                if(makerDate != undefined){
                    propToPush.push(this.valuesToPush(assetInfo.traits.product.makerDate.values));
                }
                // console.log(propToPush);
            } else if(artistDate != undefined){
                // console.log('TEST2');
                propToPush.push('Property');
                propToPush.push(this.valuesToPush(assetInfo.traits.canvas.artistDate.values));
                if (title != undefined) {
                    propToPush.push(this.valuesToPush(assetInfo.traits.product.title.values));
                }
            } else if (title != undefined) {
                // console.log('TEST3');
                propToPush.push('Property');
                propToPush.push(this.valuesToPush(assetInfo.traits.product.title.values));
            } else if (details != undefined) {
                // console.log('TEST4');
                propToPush.push('Property');
                propToPush.push(this.valuesToPush(assetInfo.traits.product.details.values));
            } else if (firstLine == undefined  && artistDate == undefined  && title == undefined && details == undefined) {
                // console.log('THIS IS TRUE AND SHOULD BE ON THE ARRAY');
                // console.log(propToPush);
                propToPush.push('Property');
                // console.log(propToPush);
            }else{
                // console.log('else');
            }
        }
        for (let i = 0; i < propToPush.length; i++) {
            if (propToPush[i].values != undefined) {
                // console.log(propToPush[i]);
                this.$scope.propDetails.push(propToPush[i].values[0]);
            } else {
                this.$scope.propDetails.push(propToPush[i]);
            }
        }
        return this.$scope.propDetails;
    }

    private valuesToPush(value) {
        // console.log(value);
        let prop = {
            'values': []
        }
        // console.log(value);
        for (var i = 0; i < value.length; i++) {
            if(value[i].language) {
                if (value[i].language == "en"){
                    prop.values.push(value[i].value);
                    break;
                }
            } else {
                // console.log(value[i]);
                prop.values.push(value[i].value);
                break;
            }
        }
        return prop;

    }
}

export const pageHeadComponent: ng.IComponentOptions = {
    controller: PageHeadComponent,
    template
};