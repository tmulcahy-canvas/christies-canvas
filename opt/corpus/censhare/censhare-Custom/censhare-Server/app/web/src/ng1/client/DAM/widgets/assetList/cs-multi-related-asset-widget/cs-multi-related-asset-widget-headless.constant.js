"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var tslib_1 = require("tslib");
var csActionFactory_1 = require("../../../../base/csActions/csActionFactory");
var CsMultiRelatedAssetWidgetHeadless = (function () {
    function CsMultiRelatedAssetWidgetHeadless(csServices, pageContextProviderPromise, widgetInstance, pageInstance, csAssetUtil, csApiSession, csViewNameResolver, csConfirmDialog, csTranslate, csNotify) {
        var _this = this;
        this.widgetInstance = widgetInstance;
        this.pageInstance = pageInstance;
        this.csAssetUtil = csAssetUtil;
        this.csApiSession = csApiSession;
        this.csViewNameResolver = csViewNameResolver;
        this.csConfirmDialog = csConfirmDialog;
        this.csTranslate = csTranslate;
        this.csNotify = csNotify;
        this.customActions = [];
        this.destroy = function () {
            _this.application.unregisterChangeListener(_this.loadData);
            _this.widgetInstance.getConfig().unregisterChangeListener(_this.invalidateConfigKey);
            _this.result.destroy();
            _this.groups.destroy();
            _this.totalCount.destroy();
            _this.viewLimit.destroy();
            _this.offset.destroy();
            _this.behaviorsObservable.destroy();
            _this.assetDnDEnabledObservable.destroy();
            _this.fileDnDEnabledObservable.destroy();
        };
        this.invalidateConfigKey = function (config) {
            var widgetSize = config.listSize || '3';
            _this.flavor = _this.csViewNameResolver.getFlavorFromRowHeight(widgetSize);
            _this.contextFlavorObservable.setValue(_this.flavor);
            _this.relationGroups = Object.keys(config)
                .filter(function (item) { return item.startsWith('multiRelation') && config[item]; })
                .sort(function (a, b) { return config[a].order - config[b].order; })
                .map(function (key) { return Object.assign({}, config[key], { key: key }); });
            var assetDnDEnabled = _this.relationGroups.find(function (item) { return item.assetDnDEnabled; });
            var fileDnDEnabled = _this.relationGroups.find(function (item) { return item.fileDnDEnabled; });
            _this.assetDnDEnabledObservable.setValue(assetDnDEnabled);
            _this.fileDnDEnabledObservable.setValue(fileDnDEnabled);
            _this.loadData();
        };
        this.loadData = function () {
            var relationGroups = _this.relationGroups.map(function (group) {
                var direction = _this.csAssetUtil.invertDirection(group.direction);
                return Object.assign({}, group, { direction: direction });
            });
            _this.csApiSession.execute('com.censhare.api.dam.masterdata.multirelations', {
                assetId: _this.assetId,
                viewName: _this.csViewNameResolver.getViewFromFlavor(_this.flavor),
                relationGroups: relationGroups
            }).then(function (res) {
                var prevIndex = 0;
                var totalCount = 0;
                var assets = [];
                var groups = [];
                var relations = res.update.find(function (e) { return e.mode === 'ADDITION'; }).groups;
                var groupMap = _this.relationGroups.reduce(function (r, group) {
                    return Object.assign(r, (_a = {}, _a[group.key] = group, _a));
                    var _a;
                }, {});
                relations.forEach(function (group) {
                    var relationGroup = groupMap[group.key];
                    relationGroup.title = group.title;
                    group.showList = relationGroup.showList;
                    group.config = relationGroup;
                    group.startIndex = prevIndex;
                    if (!group.asset && group.subGroups) {
                        group.asset = group.subGroups.reduce(function (r, sg) { return r.concat(sg.asset); }, []);
                    }
                    totalCount += group.asset.length;
                    var subGroups = group.subGroups || [];
                    subGroups.forEach(function (g, idx) {
                        g.startIndex = group.startIndex;
                        if (idx > 0) {
                            g.startIndex = subGroups[idx - 1].startIndex + subGroups[idx - 1].asset.length;
                        }
                        if (!g.title || g.title.toLowerCase() === 'other') {
                            g.title = _this.groupOtherTitle;
                        }
                        g.isSubgroup = true;
                    });
                    group.subgroups = subGroups;
                    groups.push(group);
                    if (relationGroup.showList && group.asset) {
                        prevIndex += group.asset.length;
                        assets.push.apply(assets, group.asset);
                        groups.push.apply(groups, subGroups);
                    }
                });
                _this.showHideCustomActions(totalCount > 0);
                var behaviors = relations.filter(function (group) { return group.behaviors; }).map(function (group) { return group.behaviors[0]; });
                _this.behaviorsObservable.setValue(behaviors);
                _this.widgetInstance.setCounter(totalCount);
                _this.totalCount.setValue(assets.length);
                if (relations.length) {
                    _this.viewLimit.setValue(relations.length * relations[0].limit);
                    _this.offset.setValue(relations[0].offset);
                }
                _this.groups.setValue(groups);
                _this.result.setValue(assets);
                return relations;
            })
                .then(function () { return _this.resetSelectionObservable.notifyWithValue(true); })
                .catch(function (error) {
                _this.csNotify.failure('csMultiRelatedAssetWidget.errorTitle', error.message);
            });
        };
        this.result = csServices.createObservable([]);
        this.groups = csServices.createObservable([]);
        this.totalCount = csServices.createObservable(1);
        this.viewLimit = csServices.createObservable(0);
        this.offset = csServices.createObservable(0);
        this.listFlavorObservable = csServices.createObservable(null);
        this.contextFlavorObservable = csServices.createObservable(null);
        this.behaviorsObservable = csServices.createObservable([]);
        this.assetDnDEnabledObservable = csServices.createObservable(null);
        this.fileDnDEnabledObservable = csServices.createObservable(null);
        this.resetSelectionObservable = csServices.createObservable(false);
        widgetInstance.setTitle('csMultiRelatedAssetWidget.title');
        this.groupOtherTitle = this.csTranslate.instant('csCommonTranslations.other');
        pageContextProviderPromise.then(function (pCP) {
            _this.application = pCP.getLiveApplicationInstance('com.censhare.api.applications.asset.metadata.AssetInfoApplication');
            var data = _this.application.getValue().data;
            _this.application.registerChangeListener(_this.loadData);
            _this.assetId = +data.assetId;
            _this.widgetInstance.getConfig().registerChangeListenerAndFire(_this.invalidateConfigKey);
        });
    }
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "resultReader", {
        get: function () {
            return this.result.reader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "groupsReader", {
        get: function () {
            return this.groups.reader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "totalCountReader", {
        get: function () {
            return this.totalCount.reader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "viewLimitReader", {
        get: function () {
            return this.viewLimit.reader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "offsetReader", {
        get: function () {
            return this.offset.reader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "behaviorInfo", {
        get: function () {
            return this.behaviorsObservable.reader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "assetDnDEnabledReader", {
        get: function () {
            return this.assetDnDEnabledObservable.reader;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CsMultiRelatedAssetWidgetHeadless.prototype, "fileDnDEnabledReader", {
        get: function () {
            return this.fileDnDEnabledObservable.reader;
        },
        enumerable: true,
        configurable: true
    });
    CsMultiRelatedAssetWidgetHeadless.prototype.getAssetId = function () {
        return this.assetId;
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.getRelationGroups = function () {
        return this.relationGroups;
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.getNumberOfAssetsByKey = function (key) {
        var groups = this.groups.getValue();
        var group = groups.find(function (g) { return g.key === key; });
        return group.asset && group.asset.length || 0;
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.isLimitedToCreate = function (key, count) {
        var group = this.relationGroups.find(function (g) { return g.key === key; });
        var current = this.getNumberOfAssetsByKey(key);
        return group.limitRelations && current + count > group.limitNumberOfRelations;
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.changeGroupSelection = function (key) {
        var groups = this.groups.getValue();
        var gIndex = groups.findIndex(function (group) { return group.key === key; });
        var assets = this.result.getValue();
        groups[gIndex].showList = !groups[gIndex].showList;
        this.relationGroups.find(function (group) { return group.key === key; }).showList = groups[gIndex].showList;
        if (groups[gIndex].asset) {
            var shiftLength = groups[gIndex].asset.length;
            if (!groups[gIndex].showList) {
                assets.splice(groups[gIndex].startIndex, shiftLength);
                groups.splice(gIndex + 1, groups[gIndex].subgroups.length);
                for (var i = gIndex + 1; i < groups.length; ++i) {
                    groups[i].startIndex -= shiftLength;
                }
            }
            else {
                assets.splice.apply(assets, [groups[gIndex].startIndex, 0].concat(groups[gIndex].asset));
                for (var i = gIndex + 1; i < groups.length; ++i) {
                    groups[i].startIndex += shiftLength;
                }
                groups.splice.apply(groups, [gIndex + 1, 0].concat(groups[gIndex].subgroups));
                groups[gIndex].subgroups.forEach(function (g, idx, arr) {
                    g.startIndex = groups[gIndex].startIndex;
                    if (idx > 0) {
                        g.startIndex = arr[idx - 1].startIndex + arr[idx - 1].asset.length;
                    }
                    g.isSubgroup = true;
                });
            }
        }
        this.result.setValue(assets.slice());
        this.groups.setValue(groups.slice());
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.createRemoveAllMenuItem = function () {
        var _this = this;
        var deleteAction = csActionFactory_1.actionFactory
            .action(function () { return _this.removeAllRelations(); })
            .setTitle('csRelatedAssetWidget.removeRelationAll');
        this.customActions.push(csActionFactory_1.actionFactory.separator());
        this.customActions.push(deleteAction);
        this.widgetInstance.getOrCreateMenuActions()
            .addChildBefore(this.customActions[0])
            .addChildBefore(this.customActions[1]);
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.showHideCustomActions = function (show) {
        if (show) {
            this.customActions.forEach(function (action) { return action.show(); });
        }
        else {
            this.customActions.forEach(function (action) { return action.hide(); });
        }
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.removeRelationAssets = function (group) {
        var _this = this;
        return this.csApiSession.execute('com.censhare.api.dam.assetmanagement.relation.deleteAll', {
            assetIds: [this.assetId],
            type: group.relationType,
            direction: group.direction
        }).catch(function (error) {
            _this.csNotify.failure('csMultiRelatedAssetWidget.errorTitle', error.message);
        });
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.removeAllRelations = function () {
        var _this = this;
        this.csConfirmDialog(this.pageInstance.getDialogManager(), 'csRelatedAssetWidget.removeRelationAll', {
            key: 'csMultiRelatedAssetWidget.deleteRelationAllConfirmByWidget',
            params: {
                aName: this.pageInstance.getTitle().getValue().replace('csNoTranslate.', ''),
                aId: this.assetId
            }
        }).then(function () { return _this.removeAllRelationsSync(); });
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.removeAllRelationsSync = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _i, _a, group;
            return tslib_1.__generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _i = 0, _a = this.relationGroups;
                        _b.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3, 4];
                        group = _a[_i];
                        return [4, this.removeRelationAssets(group)];
                    case 2:
                        _b.sent();
                        _b.label = 3;
                    case 3:
                        _i++;
                        return [3, 1];
                    case 4: return [2];
                }
            });
        });
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.getAssetsGroups = function (assets) {
        var groupedAssets = new Map();
        var groups = this.groups.getValue();
        assets.forEach(function (asset) {
            var group = groups.find(function (g) { return g.asset && g.asset.includes(asset); });
            if (!group) {
                return;
            }
            if (!groupedAssets.has(group)) {
                groupedAssets.set(group, []);
            }
            groupedAssets.get(group).push(asset);
        });
        return groupedAssets;
    };
    CsMultiRelatedAssetWidgetHeadless.prototype.removeSelectedAssets = function (assets) {
        var _this = this;
        var groupedAssets = this.getAssetsGroups(assets);
        var promises = Array.from(groupedAssets.keys()).map(function (group) {
            var assetIds = groupedAssets.get(group).map(function (asset) { return asset.traits.ids.id; });
            var relation = _this.relationGroups.find(function (r) { return r.key === group.key; });
            return _this.csApiSession.relation.remove(_this.assetId, assetIds, relation.relationType, relation.direction);
        });
        return Promise.all(promises);
    };
    CsMultiRelatedAssetWidgetHeadless.$name = 'csMultiRelatedAssetWidgetHeadless';
    CsMultiRelatedAssetWidgetHeadless.$inject = ['csServices', 'pageContextProviderPromise', 'widgetInstance', 'pageInstance', 'csAssetUtil',
        'csApiSession', 'csViewNameResolver', 'csConfirmDialog', 'csTranslate', 'csNotify'];
    return CsMultiRelatedAssetWidgetHeadless;
}());
exports.CsMultiRelatedAssetWidgetHeadless = CsMultiRelatedAssetWidgetHeadless;
//# sourceMappingURL=cs-multi-related-asset-widget-headless.constant.js.map 