import {IWidgetInstance} from '../../../../frames/csMainFrame/csDefaultPages/default-pages.types';
import {ICommandPromise} from '../../../../../framework/csApi/csCommander/csCommander';
import {
    IBaseContextProvider, IBaseApplication,
    IContextProvider
} from '../../../../frames/csMainFrame/csDefaultPages/csContextProvider/context-providers.types';
import IObservable = csServices.IObservable;
import {IcsAssetUtil} from '@cs/framework/csApplication/csAssetUtil/cs-asset-util.model';
import {IcsApiSession} from '@cs/framework/csApi/cs-api.model';
import {IAction} from '../../../../base/csActions/csActionInterfaces';
import { actionFactory } from '../../../../base/csActions/csActionFactory';
import {IPageInstance} from '@cs/client/frames/csMainFrame/csWorkspaceManager/csPageRegistry/page-registry.types';
import {ICsTranslate} from '@cs/framework/csApplication/csTranslate/translate.provider';
import {INotify} from '@cs/client/frames/csMainFrame/notifications/csNotify/notify';

export class CsMultiRelatedAssetWidgetHeadless {
    public static $name: string = 'csMultiRelatedAssetWidgetHeadless';
    public static $inject = ['csServices', 'pageContextProviderPromise', 'widgetInstance', 'pageInstance', 'csAssetUtil',
        'csApiSession', 'csViewNameResolver', 'csConfirmDialog', 'csTranslate', 'csNotify'];

    private application: csServices.IReader<IBaseApplication>;

    private assetId: number;
    private relationGroups: any[];

    private readonly result: IObservable<any[]>;
    private readonly groups: IObservable<any[]>;
    private readonly totalCount: IObservable<number>;
    private readonly viewLimit: IObservable<number>;
    private readonly offset: IObservable<number>;
    private readonly groupOtherTitle: string;
    private flavor: string;
    private customActions: IAction[] = [];

    public readonly listFlavorObservable: IObservable<any>;
    public readonly contextFlavorObservable: IObservable<any>;
    public readonly behaviorsObservable: IObservable<any[]>;
    public readonly assetDnDEnabledObservable: IObservable<boolean>;
    public readonly fileDnDEnabledObservable: IObservable<boolean>;
    public readonly resetSelectionObservable: IObservable<boolean>;

    constructor(csServices: csServices.IcsServicesService,
                pageContextProviderPromise: ICommandPromise<IBaseContextProvider>,
                private widgetInstance: IWidgetInstance,
                private pageInstance: IPageInstance,
                private csAssetUtil: IcsAssetUtil,
                private csApiSession: IcsApiSession,
                private csViewNameResolver: any,
                private csConfirmDialog: any,
                private csTranslate: ICsTranslate,
                private csNotify: INotify) {

        this.result = csServices.createObservable([]);
        this.groups = csServices.createObservable([]);
        this.totalCount = csServices.createObservable(1);
        this.viewLimit = csServices.createObservable(0);
        this.offset = csServices.createObservable(0);
        this.listFlavorObservable = csServices.createObservable(null);
        this.contextFlavorObservable = csServices.createObservable(null);
        this.behaviorsObservable = csServices.createObservable([]);
        this.assetDnDEnabledObservable = csServices.createObservable(null);
        this.fileDnDEnabledObservable = csServices.createObservable(null);
        this.resetSelectionObservable = csServices.createObservable(false);

        widgetInstance.setTitle('csMultiRelatedAssetWidget.title');
        this.groupOtherTitle = this.csTranslate.instant('csCommonTranslations.other');

        pageContextProviderPromise.then((pCP: IContextProvider) => {
            this.application = pCP.getLiveApplicationInstance('com.censhare.api.applications.asset.metadata.AssetInfoApplication');
            const {data} = this.application.getValue();
            this.application.registerChangeListener(this.loadData);
            this.assetId = +data.assetId;
            //JR removed this in the Javascript. AL removing this in the typescript
            // this.createRemoveAllMenuItem();
            this.widgetInstance.getConfig().registerChangeListenerAndFire(this.invalidateConfigKey);
        });
    }

    public get resultReader() {
        return this.result.reader;
    }
    public get groupsReader() {
        return this.groups.reader;
    }
    public get totalCountReader() {
        return this.totalCount.reader;
    }
    public get viewLimitReader() {
        return this.viewLimit.reader;
    }
    public get offsetReader() {
        return this.offset.reader;
    }

    public get behaviorInfo() {
        return this.behaviorsObservable.reader;
    }

    public get assetDnDEnabledReader() {
        return this.assetDnDEnabledObservable.reader;
    }

    public get fileDnDEnabledReader() {
        return this.fileDnDEnabledObservable.reader;
    }

    public destroy = (): void => {
        this.application.unregisterChangeListener(this.loadData);
        this.widgetInstance.getConfig().unregisterChangeListener(this.invalidateConfigKey);
        this.result.destroy();
        this.groups.destroy();
        this.totalCount.destroy();
        this.viewLimit.destroy();
        this.offset.destroy();
        this.behaviorsObservable.destroy();
        this.assetDnDEnabledObservable.destroy();
        this.fileDnDEnabledObservable.destroy();
    }

    public getAssetId(): number {
        return this.assetId;
    }

    public getRelationGroups(): any[] {
        return this.relationGroups;
    }

    private invalidateConfigKey = (config: any) => {
        const widgetSize = config.listSize || '3';
        this.flavor = this.csViewNameResolver.getFlavorFromRowHeight(widgetSize);
        this.contextFlavorObservable.setValue(this.flavor);

        this.relationGroups = Object.keys(config)
            .filter((item) => item.startsWith('multiRelation') && config[item])
            .sort((a: string, b: string) => config[a].order - config[b].order)
            .map( (key: string) => Object.assign({}, config[key], {key}));

        const assetDnDEnabled = this.relationGroups.find(item => item.assetDnDEnabled);
        const fileDnDEnabled = this.relationGroups.find(item => item.fileDnDEnabled);
        this.assetDnDEnabledObservable.setValue(assetDnDEnabled);
        this.fileDnDEnabledObservable.setValue(fileDnDEnabled);
        this.loadData();
    }

    public getNumberOfAssetsByKey(key: string): number {
        const groups: any[] = this.groups.getValue();
        const group: any = groups.find((g: any) => g.key === key);
        return group.asset && group.asset.length || 0;
    }

    public isLimitedToCreate(key: string, count: number): boolean {
        const group: any = this.relationGroups.find((g: any) => g.key === key);
        const current: number = this.getNumberOfAssetsByKey(key);
        return group.limitRelations && current + count > group.limitNumberOfRelations;
    }

    public changeGroupSelection(key: string) {
        const groups: any[] = this.groups.getValue();
        const gIndex: number = groups.findIndex(group => group.key === key);
        const assets: any[] = this.result.getValue();
        groups[gIndex].showList = !groups[gIndex].showList;
        this.relationGroups.find(group => group.key === key).showList = groups[gIndex].showList;
        if (groups[gIndex].asset) {
            const shiftLength: number = groups[gIndex].asset.length;
            if (!groups[gIndex].showList) {
                assets.splice(groups[gIndex].startIndex, shiftLength);
                groups.splice(gIndex + 1, groups[gIndex].subgroups.length);
                for (let i: number = gIndex + 1; i < groups.length; ++i) {
                    groups[i].startIndex -= shiftLength;
                }
            } else {
                assets.splice(groups[gIndex].startIndex, 0, ...groups[gIndex].asset);
                for (let i: number = gIndex + 1; i < groups.length; ++i) {
                    groups[i].startIndex += shiftLength;
                }
                groups.splice(gIndex + 1, 0, ...groups[gIndex].subgroups);
                groups[gIndex].subgroups.forEach((g: any, idx: number, arr: any[]) => {
                    g.startIndex = groups[gIndex].startIndex;
                    if (idx > 0) {
                        g.startIndex = arr[idx - 1].startIndex + arr[idx - 1].asset.length;
                    }
                    g.isSubgroup = true;
                });
            }
        }
        this.result.setValue([...assets]);
        this.groups.setValue([...groups]);
    }

    private loadData = () => {
        const relationGroups = this.relationGroups.map( (group: any) => {
                const direction = this.csAssetUtil.invertDirection(group.direction);
                return Object.assign({}, group, {direction});
            });
        this.csApiSession.execute('com.censhare.api.dam.masterdata.multirelations', {
            assetId: this.assetId,
            viewName: this.csViewNameResolver.getViewFromFlavor(this.flavor),
            relationGroups
        }).then((res: any) => {
            let prevIndex: number = 0;
            let totalCount: number = 0;
            const assets = [];
            const groups: any[] = [];
            const relations = res.update.find(e => e.mode === 'ADDITION').groups; // if we hve just one group it come as object
            const groupMap: any = this.relationGroups.reduce((r: any, group: any) => Object.assign(r, {[group.key]: group}), {});

            relations.forEach(group => {
                const relationGroup = groupMap[group.key];
                relationGroup.title = group.title;
                group.showList = relationGroup.showList;
                group.config = relationGroup;
                group.startIndex = prevIndex;
                if (!group.asset && group.subGroups) {
                    group.asset = group.subGroups.reduce( (r: any[], sg: any) => r.concat(sg.asset), []);
                }
                totalCount += group.asset.length;

                const subGroups = group.subGroups || [];
                subGroups.forEach((g: any, idx: number) => {
                    g.startIndex = group.startIndex;
                    if (idx > 0) {
                        g.startIndex = subGroups[idx - 1].startIndex + subGroups[idx - 1].asset.length;
                    }
                    if (!g.title || g.title.toLowerCase() === 'other') {
                        g.title = this.groupOtherTitle;
                    }
                    g.isSubgroup = true;
                });

                group.subgroups = subGroups;
                groups.push(group);


                if (relationGroup.showList && group.asset) {
                    prevIndex += group.asset.length;
                    assets.push(...group.asset);
                    groups.push(...subGroups);
                }
            });

            this.showHideCustomActions(totalCount > 0);

            const behaviors = relations.filter( group => group.behaviors).map( group => group.behaviors[0]);
            this.behaviorsObservable.setValue(behaviors);
            this.widgetInstance.setCounter(totalCount);
            this.totalCount.setValue(assets.length);
            if (relations.length) {
                this.viewLimit.setValue(relations.length * relations[0].limit);
                this.offset.setValue(relations[0].offset);
            }
            this.groups.setValue(groups);
            this.result.setValue(assets);
            return relations;
        })
        .then(() => this.resetSelectionObservable.notifyWithValue(true))
        .catch(error => {
            this.csNotify.failure('csMultiRelatedAssetWidget.errorTitle', error.message);
        });
    }

    private createRemoveAllMenuItem(): void {
        const deleteAction: IAction = actionFactory
            .action(() => this.removeAllRelations())
            .setTitle('csRelatedAssetWidget.removeRelationAll');

        this.customActions.push(actionFactory.separator());
        this.customActions.push(deleteAction);

        this.widgetInstance.getOrCreateMenuActions()
            .addChildBefore(this.customActions[0])
            .addChildBefore(this.customActions[1]);
    }

    private showHideCustomActions(show: boolean) {
        if (show) {
            this.customActions.forEach((action: IAction) => action.show());
        } else {
            this.customActions.forEach((action: IAction) => action.hide());
        }
    }

    public removeRelationAssets(group: any) {
        return this.csApiSession.execute('com.censhare.api.dam.assetmanagement.relation.deleteAll', {
            assetIds: [this.assetId],
            type: group.relationType,
            direction: group.direction
        }).catch((error: any) => {
            this.csNotify.failure('csMultiRelatedAssetWidget.errorTitle', error.message);
        });
    }

    private removeAllRelations() {
        this.csConfirmDialog(this.pageInstance.getDialogManager(), 'csRelatedAssetWidget.removeRelationAll',
            {
                key: 'csMultiRelatedAssetWidget.deleteRelationAllConfirmByWidget',
                params: {
                    aName: this.pageInstance.getTitle().getValue().replace('csNoTranslate.', ''),
                    aId: this.assetId
                }
            }).then(() => this.removeAllRelationsSync());
    }

    private async removeAllRelationsSync() { // need to wait before previous relation deleted, otherwise server throws exception "try delete again"
        for (const group of this.relationGroups) {
            await this.removeRelationAssets(group);
        }
    }

    public getAssetsGroups(assets: any[]): Map<any, any[]> {
        const groupedAssets: Map<any, any[]> = new Map<any, any[]>();
        const groups: any[] = this.groups.getValue();
        assets.forEach((asset: any) => {
            const group: any = groups.find((g: any) => g.asset && g.asset.includes(asset));
            if (!group) {
                return;
            }
            if (!groupedAssets.has(group)) {
                groupedAssets.set(group, []);
            }
            groupedAssets.get(group).push(asset);
        });
        return groupedAssets;
    }

    public removeSelectedAssets(assets: any[]): Promise<void[]> {
        const groupedAssets: Map<any, any[]> = this.getAssetsGroups(assets);
        const promises: PromiseLike<void>[] = Array.from(groupedAssets.keys()).map((group: any) => {
            const assetIds = groupedAssets.get(group).map((asset: any) => asset.traits.ids.id);
            const relation = this.relationGroups.find((r: any) => r.key === group.key);
            return this.csApiSession.relation.remove(this.assetId, assetIds, relation.relationType, relation.direction) as PromiseLike<void>;
        });
        return Promise.all(promises);
    }
}